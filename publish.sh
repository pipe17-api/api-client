#!/bin/sh

git_message="Pipe17 OpenAPI Changed (auto generated)"
git_version=$npm_package_version
git_repo=$npm_package_repository_url
git_repo=${git_repo#git+}

git clone --bare $git_repo ./.git

# restore the link between the local repo and its upstream remote repo
git config --local --bool core.bare false
git config --local remote.origin.fetch +refs/heads/*:refs/remotes/origin/*
git fetch origin
git branch -u origin/master master

# reset the index (not the working tree)
git reset HEAD -- .
git add -A
git commit -m "$git_message"
git push

git fetch --tags
git tag -d $git_version
git push origin :$git_version
git tag $git_version
git push --tags
