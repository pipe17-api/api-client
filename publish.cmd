@echo off

set message="Pipe17 OpenAPI Changed (auto generated)"
set version=%npm_package_version%
set git_repo=%npm_package_repository_url%
set git_repo=%git_repo:git+=%

git clone --bare %git_repo% .git

rem restore the link between the local repo and its upstream remote repo
git config --local --bool core.bare false
git config --local remote.origin.fetch +refs/heads/*:refs/remotes/origin/*
git fetch origin
git branch -u origin/master master

rem reset the index (not the working tree)
git reset HEAD -- .
git add -A
git commit -m %message%
git push

git fetch --tags
git tag -d %version%
git push origin :%version%
git tag %version%
git push --tags