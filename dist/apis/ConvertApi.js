"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertApi = void 0;
const runtime = __importStar(require("../runtime"));
const models_1 = require("../models");
/**
 *
 */
class ConvertApi extends runtime.BaseAPI {
    /**
     * Convert a source object to target using specified mapping rules
     */
    convertRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["x-api-key"] = this.configuration.apiKey("x-api-key"); // ApiKeyAuth authentication
            }
            const response = yield this.request({
                path: `/convert`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ConvertRequestToJSON(requestParameters.convertRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConvertResponseFromJSON(jsonValue));
        });
    }
    /**
     * Convert a source object to target using specified mapping rules
     */
    convert(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.convertRaw(requestParameters);
            return yield response.value();
        });
    }
}
exports.ConvertApi = ConvertApi;
