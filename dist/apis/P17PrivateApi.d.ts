/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import * as runtime from '../runtime';
import { ConnectorSetupRequest, ConnectorSetupResponse, DeleteAction, EntityFilterCreateData, EntityFilterCreateResponse, EntityFilterFetchResponse, EntityFilterListResponse, EntityFilterUpdateRequest, EntityFilterUpdateResponse, EntityName, EntitySchemaCreateData, EntitySchemaCreateResponse, EntitySchemaListResponse, ExceptionCreateRequest, ExceptionCreateResponse, ExceptionFetchResponse, ExceptionFilterCreateData, ExceptionFilterCreateResponse, ExceptionFilterFetchResponse, ExceptionFilterListResponse, ExceptionFilterUpdateRequest, ExceptionFilterUpdateResponse, ExceptionStatus, ExceptionType, ExceptionUpdateRequest, ExceptionUpdateResponse, ExceptionsListResponse, IntegrationStateCreateData, IntegrationStateCreateResponse, IntegrationStateFetchResponse, IntegrationStateListResponse, NotImplemented, OrderRoutingCreateData, OrderRoutingCreateResponse, OrderRoutingFetchResponse, OrderRoutingListResponse, OrderRoutingUpdateRequest, OrderRoutingUpdateResponse, Success } from '../models';
export interface P17PrivateApiCreateEntityFilterRequest {
    entityFilterCreateData: EntityFilterCreateData;
}
export interface P17PrivateApiCreateEntitySchemaRequest {
    entitySchemaCreateData: EntitySchemaCreateData;
}
export interface P17PrivateApiCreateExceptionRequest {
    exceptionCreateRequest: ExceptionCreateRequest;
}
export interface P17PrivateApiCreateExceptionFilterRequest {
    exceptionFilterCreateData: ExceptionFilterCreateData;
}
export interface P17PrivateApiCreateOrderRoutingRequest {
    orderRoutingCreateData: OrderRoutingCreateData;
}
export interface P17PrivateApiDeleteEntityFilterRequest {
    filterId: string;
    action?: DeleteAction;
}
export interface P17PrivateApiDeleteEntitySchemaRequest {
    schemaId: string;
    action?: DeleteAction;
}
export interface P17PrivateApiDeleteExceptionRequest {
    exceptionId: string;
    action?: DeleteAction;
}
export interface P17PrivateApiDeleteExceptionFilterRequest {
    filterId: string;
    action?: DeleteAction;
}
export interface P17PrivateApiDeleteOrderRoutingRequest {
    routingId: string;
    action?: DeleteAction;
}
export interface P17PrivateApiFetchEntityFilterRequest {
    filterId: string;
}
export interface P17PrivateApiFetchExceptionRequest {
    exceptionId: string;
}
export interface P17PrivateApiFetchExceptionFilterRequest {
    filterId: string;
}
export interface P17PrivateApiFetchIntegrationStateRequest {
    name: string;
}
export interface P17PrivateApiFetchOrderRoutingRequest {
    routingId: string;
}
export interface P17PrivateApiListEntityFiltersRequest {
    filterId?: Array<string>;
    name?: Array<string>;
    entity?: Array<EntityName>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17PrivateApiListEntitySchemasRequest {
    schemaId?: Array<string>;
    entity?: Array<EntityName>;
    name?: Array<string>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17PrivateApiListExceptionFiltersRequest {
    filterId?: Array<string>;
    exceptionType?: Array<ExceptionType>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17PrivateApiListExceptionsRequest {
    status?: Array<ExceptionStatus>;
    exceptionId?: Array<string>;
    exceptionType?: Array<ExceptionType>;
    entityId?: Array<string>;
    entityType?: Array<EntityName>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17PrivateApiListIntegrationStateRequest {
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17PrivateApiListOrderRoutingsRequest {
    routingId?: Array<string>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17PrivateApiSetupConnectorRequest {
    connectorId: string;
    connectorSetupRequest: ConnectorSetupRequest;
}
export interface P17PrivateApiUpdateEntityFilterRequest {
    filterId: string;
    entityFilterUpdateRequest: EntityFilterUpdateRequest;
}
export interface P17PrivateApiUpdateExceptionRequest {
    exceptionId: string;
    exceptionUpdateRequest: ExceptionUpdateRequest;
}
export interface P17PrivateApiUpdateExceptionFilterRequest {
    filterId: string;
    exceptionFilterUpdateRequest: ExceptionFilterUpdateRequest;
}
export interface P17PrivateApiUpdateOrderRoutingRequest {
    routingId: string;
    orderRoutingUpdateRequest: OrderRoutingUpdateRequest;
}
export interface P17PrivateApiUpsertIntegrationStateRequest {
    integrationStateCreateData: IntegrationStateCreateData;
}
/**
 *
 */
export declare class P17PrivateApi extends runtime.BaseAPI {
    /**
     * Create Entity Filter
     */
    createEntityFilterRaw(requestParameters: P17PrivateApiCreateEntityFilterRequest): Promise<runtime.ApiResponse<EntityFilterCreateResponse>>;
    /**
     * Create Entity Filter
     */
    createEntityFilter(requestParameters: P17PrivateApiCreateEntityFilterRequest): Promise<EntityFilterCreateResponse>;
    /**
     * Create Entity Schema
     */
    createEntitySchemaRaw(requestParameters: P17PrivateApiCreateEntitySchemaRequest): Promise<runtime.ApiResponse<EntitySchemaCreateResponse>>;
    /**
     * Create Entity Schema
     */
    createEntitySchema(requestParameters: P17PrivateApiCreateEntitySchemaRequest): Promise<EntitySchemaCreateResponse>;
    /**
     * Create Exception
     */
    createExceptionRaw(requestParameters: P17PrivateApiCreateExceptionRequest): Promise<runtime.ApiResponse<ExceptionCreateResponse>>;
    /**
     * Create Exception
     */
    createException(requestParameters: P17PrivateApiCreateExceptionRequest): Promise<ExceptionCreateResponse>;
    /**
     * Create Exception Filter
     */
    createExceptionFilterRaw(requestParameters: P17PrivateApiCreateExceptionFilterRequest): Promise<runtime.ApiResponse<ExceptionFilterCreateResponse>>;
    /**
     * Create Exception Filter
     */
    createExceptionFilter(requestParameters: P17PrivateApiCreateExceptionFilterRequest): Promise<ExceptionFilterCreateResponse>;
    /**
     * Create Order Routing
     */
    createOrderRoutingRaw(requestParameters: P17PrivateApiCreateOrderRoutingRequest): Promise<runtime.ApiResponse<OrderRoutingCreateResponse>>;
    /**
     * Create Order Routing
     */
    createOrderRouting(requestParameters: P17PrivateApiCreateOrderRoutingRequest): Promise<OrderRoutingCreateResponse>;
    /**
     * Delete Accounts (Not Implemented)
     */
    deleteAccountsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Accounts (Not Implemented)
     */
    deleteAccounts(): Promise<NotImplemented>;
    /**
     * Delete Apikeys (Not Implemented)
     */
    deleteApikeysRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Apikeys (Not Implemented)
     */
    deleteApikeys(): Promise<NotImplemented>;
    /**
     * Delete Connectors (Not Implemented)
     */
    deleteConnectorsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Connectors (Not Implemented)
     */
    deleteConnectors(): Promise<NotImplemented>;
    /**
     * Delete Entity Filter
     */
    deleteEntityFilterRaw(requestParameters: P17PrivateApiDeleteEntityFilterRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Entity Filter
     */
    deleteEntityFilter(requestParameters: P17PrivateApiDeleteEntityFilterRequest): Promise<Success>;
    /**
     * Delete Entity Filters (Not Implemented)
     */
    deleteEntityFiltersRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Entity Filters (Not Implemented)
     */
    deleteEntityFilters(): Promise<NotImplemented>;
    /**
     * Delete Entity Schema
     */
    deleteEntitySchemaRaw(requestParameters: P17PrivateApiDeleteEntitySchemaRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Entity Schema
     */
    deleteEntitySchema(requestParameters: P17PrivateApiDeleteEntitySchemaRequest): Promise<Success>;
    /**
     * Delete EntitySchemas (Not Implemented)
     */
    deleteEntitySchemasRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete EntitySchemas (Not Implemented)
     */
    deleteEntitySchemas(): Promise<NotImplemented>;
    /**
     * Delete Events (Not Implemented)
     */
    deleteEventsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Events (Not Implemented)
     */
    deleteEvents(): Promise<NotImplemented>;
    /**
     * Delete Exception
     */
    deleteExceptionRaw(requestParameters: P17PrivateApiDeleteExceptionRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Exception
     */
    deleteException(requestParameters: P17PrivateApiDeleteExceptionRequest): Promise<Success>;
    /**
     * Delete Exception Filter
     */
    deleteExceptionFilterRaw(requestParameters: P17PrivateApiDeleteExceptionFilterRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Exception Filter
     */
    deleteExceptionFilter(requestParameters: P17PrivateApiDeleteExceptionFilterRequest): Promise<Success>;
    /**
     * Delete ExceptionFilters (Not Implemented)
     */
    deleteExceptionFiltersRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete ExceptionFilters (Not Implemented)
     */
    deleteExceptionFilters(): Promise<NotImplemented>;
    /**
     * Delete Exceptions (Not Implemented)
     */
    deleteExceptionsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Exceptions (Not Implemented)
     */
    deleteExceptions(): Promise<NotImplemented>;
    /**
     * Delete Integrations (Not Implemented)
     */
    deleteIntegrationsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Integrations (Not Implemented)
     */
    deleteIntegrations(): Promise<NotImplemented>;
    /**
     * Delete Mappings (Not Implemented)
     */
    deleteMappingsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Mappings (Not Implemented)
     */
    deleteMappings(): Promise<NotImplemented>;
    /**
     * Delete Order Routing
     */
    deleteOrderRoutingRaw(requestParameters: P17PrivateApiDeleteOrderRoutingRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Order Routing
     */
    deleteOrderRouting(requestParameters: P17PrivateApiDeleteOrderRoutingRequest): Promise<Success>;
    /**
     * Delete OrderRoutings (Not Implemented)
     */
    deleteOrderRoutingsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete OrderRoutings (Not Implemented)
     */
    deleteOrderRoutings(): Promise<NotImplemented>;
    /**
     * Delete Organizations (Not Implemented)
     */
    deleteOrganizationsRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Organizations (Not Implemented)
     */
    deleteOrganizations(): Promise<NotImplemented>;
    /**
     * Delete Roles (Not Implemented)
     */
    deleteRolesRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Roles (Not Implemented)
     */
    deleteRoles(): Promise<NotImplemented>;
    /**
     * Delete Users (Not Implemented)
     */
    deleteUsersRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Users (Not Implemented)
     */
    deleteUsers(): Promise<NotImplemented>;
    /**
     * Delete Webhooks (Not Implemented)
     */
    deleteWebhooksRaw(): Promise<runtime.ApiResponse<NotImplemented>>;
    /**
     * Delete Webhooks (Not Implemented)
     */
    deleteWebhooks(): Promise<NotImplemented>;
    /**
     * Fetch Entity Filter
     */
    fetchEntityFilterRaw(requestParameters: P17PrivateApiFetchEntityFilterRequest): Promise<runtime.ApiResponse<EntityFilterFetchResponse>>;
    /**
     * Fetch Entity Filter
     */
    fetchEntityFilter(requestParameters: P17PrivateApiFetchEntityFilterRequest): Promise<EntityFilterFetchResponse>;
    /**
     * Fetch Exception
     */
    fetchExceptionRaw(requestParameters: P17PrivateApiFetchExceptionRequest): Promise<runtime.ApiResponse<ExceptionFetchResponse>>;
    /**
     * Fetch Exception
     */
    fetchException(requestParameters: P17PrivateApiFetchExceptionRequest): Promise<ExceptionFetchResponse>;
    /**
     * Fetch Exception Filter
     */
    fetchExceptionFilterRaw(requestParameters: P17PrivateApiFetchExceptionFilterRequest): Promise<runtime.ApiResponse<ExceptionFilterFetchResponse>>;
    /**
     * Fetch Exception Filter
     */
    fetchExceptionFilter(requestParameters: P17PrivateApiFetchExceptionFilterRequest): Promise<ExceptionFilterFetchResponse>;
    /**
     * Fetch Integration State
     */
    fetchIntegrationStateRaw(requestParameters: P17PrivateApiFetchIntegrationStateRequest): Promise<runtime.ApiResponse<IntegrationStateFetchResponse>>;
    /**
     * Fetch Integration State
     */
    fetchIntegrationState(requestParameters: P17PrivateApiFetchIntegrationStateRequest): Promise<IntegrationStateFetchResponse>;
    /**
     * Fetch Order Routing
     */
    fetchOrderRoutingRaw(requestParameters: P17PrivateApiFetchOrderRoutingRequest): Promise<runtime.ApiResponse<OrderRoutingFetchResponse>>;
    /**
     * Fetch Order Routing
     */
    fetchOrderRouting(requestParameters: P17PrivateApiFetchOrderRoutingRequest): Promise<OrderRoutingFetchResponse>;
    /**
     * List Entity Filters
     */
    listEntityFiltersRaw(requestParameters: P17PrivateApiListEntityFiltersRequest): Promise<runtime.ApiResponse<EntityFilterListResponse>>;
    /**
     * List Entity Filters
     */
    listEntityFilters(requestParameters: P17PrivateApiListEntityFiltersRequest): Promise<EntityFilterListResponse>;
    /**
     * List Entity Schemas
     */
    listEntitySchemasRaw(requestParameters: P17PrivateApiListEntitySchemasRequest): Promise<runtime.ApiResponse<EntitySchemaListResponse>>;
    /**
     * List Entity Schemas
     */
    listEntitySchemas(requestParameters: P17PrivateApiListEntitySchemasRequest): Promise<EntitySchemaListResponse>;
    /**
     * List Exception Filters
     */
    listExceptionFiltersRaw(requestParameters: P17PrivateApiListExceptionFiltersRequest): Promise<runtime.ApiResponse<ExceptionFilterListResponse>>;
    /**
     * List Exception Filters
     */
    listExceptionFilters(requestParameters: P17PrivateApiListExceptionFiltersRequest): Promise<ExceptionFilterListResponse>;
    /**
     * List Exceptions
     */
    listExceptionsRaw(requestParameters: P17PrivateApiListExceptionsRequest): Promise<runtime.ApiResponse<ExceptionsListResponse>>;
    /**
     * List Exceptions
     */
    listExceptions(requestParameters: P17PrivateApiListExceptionsRequest): Promise<ExceptionsListResponse>;
    /**
     * List Integrations State
     */
    listIntegrationStateRaw(requestParameters: P17PrivateApiListIntegrationStateRequest): Promise<runtime.ApiResponse<IntegrationStateListResponse>>;
    /**
     * List Integrations State
     */
    listIntegrationState(requestParameters: P17PrivateApiListIntegrationStateRequest): Promise<IntegrationStateListResponse>;
    /**
     * List Order Routings
     */
    listOrderRoutingsRaw(requestParameters: P17PrivateApiListOrderRoutingsRequest): Promise<runtime.ApiResponse<OrderRoutingListResponse>>;
    /**
     * List Order Routings
     */
    listOrderRoutings(requestParameters: P17PrivateApiListOrderRoutingsRequest): Promise<OrderRoutingListResponse>;
    /**
     * Configure private connector settings
     */
    setupConnectorRaw(requestParameters: P17PrivateApiSetupConnectorRequest): Promise<runtime.ApiResponse<ConnectorSetupResponse>>;
    /**
     * Configure private connector settings
     */
    setupConnector(requestParameters: P17PrivateApiSetupConnectorRequest): Promise<ConnectorSetupResponse>;
    /**
     * Update Entity Filter
     */
    updateEntityFilterRaw(requestParameters: P17PrivateApiUpdateEntityFilterRequest): Promise<runtime.ApiResponse<EntityFilterUpdateResponse>>;
    /**
     * Update Entity Filter
     */
    updateEntityFilter(requestParameters: P17PrivateApiUpdateEntityFilterRequest): Promise<EntityFilterUpdateResponse>;
    /**
     * Update Exception
     */
    updateExceptionRaw(requestParameters: P17PrivateApiUpdateExceptionRequest): Promise<runtime.ApiResponse<ExceptionUpdateResponse>>;
    /**
     * Update Exception
     */
    updateException(requestParameters: P17PrivateApiUpdateExceptionRequest): Promise<ExceptionUpdateResponse>;
    /**
     * Update Exception Filter
     */
    updateExceptionFilterRaw(requestParameters: P17PrivateApiUpdateExceptionFilterRequest): Promise<runtime.ApiResponse<ExceptionFilterUpdateResponse>>;
    /**
     * Update Exception Filter
     */
    updateExceptionFilter(requestParameters: P17PrivateApiUpdateExceptionFilterRequest): Promise<ExceptionFilterUpdateResponse>;
    /**
     * Update Order Routing
     */
    updateOrderRoutingRaw(requestParameters: P17PrivateApiUpdateOrderRoutingRequest): Promise<runtime.ApiResponse<OrderRoutingUpdateResponse>>;
    /**
     * Update Order Routing
     */
    updateOrderRouting(requestParameters: P17PrivateApiUpdateOrderRoutingRequest): Promise<OrderRoutingUpdateResponse>;
    /**
     * Create/Update/Delete Integration State
     */
    upsertIntegrationStateRaw(requestParameters: P17PrivateApiUpsertIntegrationStateRequest): Promise<runtime.ApiResponse<IntegrationStateCreateResponse>>;
    /**
     * Create/Update/Delete Integration State
     */
    upsertIntegrationState(requestParameters: P17PrivateApiUpsertIntegrationStateRequest): Promise<IntegrationStateCreateResponse>;
}
