/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import * as runtime from '../runtime';
import { ConvertRequest, ConvertResponse } from '../models';
export interface ConvertApiConvertOperationRequest {
    convertRequest?: ConvertRequest;
}
/**
 *
 */
export declare class ConvertApi extends runtime.BaseAPI {
    /**
     * Convert a source object to target using specified mapping rules
     */
    convertRaw(requestParameters: ConvertApiConvertOperationRequest): Promise<runtime.ApiResponse<ConvertResponse>>;
    /**
     * Convert a source object to target using specified mapping rules
     */
    convert(requestParameters: ConvertApiConvertOperationRequest): Promise<ConvertResponse>;
}
