"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.P17Api = void 0;
const runtime = __importStar(require("../runtime"));
const models_1 = require("../models");
/**
 *
 */
class P17Api extends runtime.BaseAPI {
    /**
     * Convert a source object to target using specified mapping rules
     */
    converterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/converter`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ConverterRequestToJSON(requestParameters.converterRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConverterResponseFromJSON(jsonValue));
        });
    }
    /**
     * Convert a source object to target using specified mapping rules
     */
    converter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.converterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Account
     */
    createAccountRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.accountCreateRequest === null || requestParameters.accountCreateRequest === undefined) {
                throw new runtime.RequiredError('accountCreateRequest', 'Required parameter requestParameters.accountCreateRequest was null or undefined when calling createAccount.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/accounts`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.AccountCreateRequestToJSON(requestParameters.accountCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Account
     */
    createAccount(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createAccountRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create API Key
     */
    createApiKeyRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.apiKeyCreateRequest === null || requestParameters.apiKeyCreateRequest === undefined) {
                throw new runtime.RequiredError('apiKeyCreateRequest', 'Required parameter requestParameters.apiKeyCreateRequest was null or undefined when calling createApiKey.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ApiKeyCreateRequestToJSON(requestParameters.apiKeyCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create API Key
     */
    createApiKey(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createApiKeyRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Arrival Notifications
     */
    createArrivalsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.arrivalCreateData === null || requestParameters.arrivalCreateData === undefined) {
                throw new runtime.RequiredError('arrivalCreateData', 'Required parameter requestParameters.arrivalCreateData was null or undefined when calling createArrivals.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/arrivals`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.arrivalCreateData.map(models_1.ArrivalCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalsCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalsCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Arrival Notifications
     */
    createArrivals(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createArrivalsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Connector
     */
    createConnectorRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.connectorCreateRequest === null || requestParameters.connectorCreateRequest === undefined) {
                throw new runtime.RequiredError('connectorCreateRequest', 'Required parameter requestParameters.connectorCreateRequest was null or undefined when calling createConnector.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ConnectorCreateRequestToJSON(requestParameters.connectorCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Connector
     */
    createConnector(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createConnectorRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create event
     */
    createEventRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.eventCreateData === null || requestParameters.eventCreateData === undefined) {
                throw new runtime.RequiredError('eventCreateData', 'Required parameter requestParameters.eventCreateData was null or undefined when calling createEvent.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.EventCreateDataToJSON(requestParameters.eventCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create event
     */
    createEvent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createEventRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Fulfillments
     */
    createFulfillmentsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.fulfillmentCreateData === null || requestParameters.fulfillmentCreateData === undefined) {
                throw new runtime.RequiredError('fulfillmentCreateData', 'Required parameter requestParameters.fulfillmentCreateData was null or undefined when calling createFulfillments.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/fulfillments`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.fulfillmentCreateData.map(models_1.FulfillmentCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FulfillmentsCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.FulfillmentsCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Fulfillments
     */
    createFulfillments(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createFulfillmentsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Integration
     */
    createIntegrationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.integrationCreateRequest === null || requestParameters.integrationCreateRequest === undefined) {
                throw new runtime.RequiredError('integrationCreateRequest', 'Required parameter requestParameters.integrationCreateRequest was null or undefined when calling createIntegration.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.IntegrationCreateRequestToJSON(requestParameters.integrationCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Integration
     */
    createIntegration(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createIntegrationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Inventory Log Items
     */
    createInventoryRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.inventoryCreateData === null || requestParameters.inventoryCreateData === undefined) {
                throw new runtime.RequiredError('inventoryCreateData', 'Required parameter requestParameters.inventoryCreateData was null or undefined when calling createInventory.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventory`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.inventoryCreateData.map(models_1.InventoryCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Inventory Log Items
     */
    createInventory(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createInventoryRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Inventory Rule
     */
    createInventoryRuleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.inventoryRuleCreateRequest === null || requestParameters.inventoryRuleCreateRequest === undefined) {
                throw new runtime.RequiredError('inventoryRuleCreateRequest', 'Required parameter requestParameters.inventoryRuleCreateRequest was null or undefined when calling createInventoryRule.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventoryrules`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.InventoryRuleCreateRequestToJSON(requestParameters.inventoryRuleCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRuleCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRuleCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Inventory Rule
     */
    createInventoryRule(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createInventoryRuleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create long-running job
     */
    createJobRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.jobCreateData === null || requestParameters.jobCreateData === undefined) {
                throw new runtime.RequiredError('jobCreateData', 'Required parameter requestParameters.jobCreateData was null or undefined when calling createJob.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/jobs`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.JobCreateDataToJSON(requestParameters.jobCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create long-running job
     */
    createJob(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createJobRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Label
     */
    createLabelRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.labelCreateData === null || requestParameters.labelCreateData === undefined) {
                throw new runtime.RequiredError('labelCreateData', 'Required parameter requestParameters.labelCreateData was null or undefined when calling createLabel.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.LabelCreateDataToJSON(requestParameters.labelCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Label
     */
    createLabel(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createLabelRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Location
     */
    createLocationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.locationCreateRequest === null || requestParameters.locationCreateRequest === undefined) {
                throw new runtime.RequiredError('locationCreateRequest', 'Required parameter requestParameters.locationCreateRequest was null or undefined when calling createLocation.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/locations`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.LocationCreateRequestToJSON(requestParameters.locationCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Location
     */
    createLocation(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createLocationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Mapping
     */
    createMappingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.mappingCreateRequest === null || requestParameters.mappingCreateRequest === undefined) {
                throw new runtime.RequiredError('mappingCreateRequest', 'Required parameter requestParameters.mappingCreateRequest was null or undefined when calling createMapping.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/mappings`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.MappingCreateRequestToJSON(requestParameters.mappingCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Mapping
     */
    createMapping(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createMappingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Orders
     */
    createOrdersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orderCreateData === null || requestParameters.orderCreateData === undefined) {
                throw new runtime.RequiredError('orderCreateData', 'Required parameter requestParameters.orderCreateData was null or undefined when calling createOrders.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/orders`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.orderCreateData.map(models_1.OrderCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrdersCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrdersCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Orders
     */
    createOrders(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createOrdersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Organization
     */
    createOrganizationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.organizationCreateRequest === null || requestParameters.organizationCreateRequest === undefined) {
                throw new runtime.RequiredError('organizationCreateRequest', 'Required parameter requestParameters.organizationCreateRequest was null or undefined when calling createOrganization.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/organizations`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.OrganizationCreateRequestToJSON(requestParameters.organizationCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Organization
     */
    createOrganization(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createOrganizationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Products
     */
    createProductsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.productCreateData === null || requestParameters.productCreateData === undefined) {
                throw new runtime.RequiredError('productCreateData', 'Required parameter requestParameters.productCreateData was null or undefined when calling createProducts.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/products`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.productCreateData.map(models_1.ProductCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductsCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductsCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Products
     */
    createProducts(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createProductsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Purchases
     */
    createPurchasesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.purchaseCreateData === null || requestParameters.purchaseCreateData === undefined) {
                throw new runtime.RequiredError('purchaseCreateData', 'Required parameter requestParameters.purchaseCreateData was null or undefined when calling createPurchases.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/purchases`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.purchaseCreateData.map(models_1.PurchaseCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchasesCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchasesCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Purchases
     */
    createPurchases(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createPurchasesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Arrival Receipts
     */
    createReceiptsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.receiptCreateData === null || requestParameters.receiptCreateData === undefined) {
                throw new runtime.RequiredError('receiptCreateData', 'Required parameter requestParameters.receiptCreateData was null or undefined when calling createReceipts.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/receipts`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.receiptCreateData.map(models_1.ReceiptCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReceiptsCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReceiptsCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Arrival Receipts
     */
    createReceipts(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createReceiptsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Refund
     */
    createRefundRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.refundCreateRequest === null || requestParameters.refundCreateRequest === undefined) {
                throw new runtime.RequiredError('refundCreateRequest', 'Required parameter requestParameters.refundCreateRequest was null or undefined when calling createRefund.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/refunds`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.RefundCreateRequestToJSON(requestParameters.refundCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Refund
     */
    createRefund(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createRefundRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Return
     */
    createReturnRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.returnCreateRequest === null || requestParameters.returnCreateRequest === undefined) {
                throw new runtime.RequiredError('returnCreateRequest', 'Required parameter requestParameters.returnCreateRequest was null or undefined when calling createReturn.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/returns`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ReturnCreateRequestToJSON(requestParameters.returnCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Return
     */
    createReturn(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createReturnRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Role
     */
    createRoleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.roleCreateRequest === null || requestParameters.roleCreateRequest === undefined) {
                throw new runtime.RequiredError('roleCreateRequest', 'Required parameter requestParameters.roleCreateRequest was null or undefined when calling createRole.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/roles`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.RoleCreateRequestToJSON(requestParameters.roleCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoleCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoleCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Role
     */
    createRole(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createRoleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Routing
     */
    createRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingCreateRequest === null || requestParameters.routingCreateRequest === undefined) {
                throw new runtime.RequiredError('routingCreateRequest', 'Required parameter requestParameters.routingCreateRequest was null or undefined when calling createRouting.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/routings`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.RoutingCreateRequestToJSON(requestParameters.routingCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Routing
     */
    createRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Shipments
     */
    createShipmentsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.shipmentCreateData === null || requestParameters.shipmentCreateData === undefined) {
                throw new runtime.RequiredError('shipmentCreateData', 'Required parameter requestParameters.shipmentCreateData was null or undefined when calling createShipments.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/shipments`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.shipmentCreateData.map(models_1.ShipmentCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentsCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentsCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Shipments
     */
    createShipments(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createShipmentsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Supplier
     */
    createSupplierRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.supplierCreateRequest === null || requestParameters.supplierCreateRequest === undefined) {
                throw new runtime.RequiredError('supplierCreateRequest', 'Required parameter requestParameters.supplierCreateRequest was null or undefined when calling createSupplier.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/suppliers`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.SupplierCreateRequestToJSON(requestParameters.supplierCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.SupplierCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SupplierCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Supplier
     */
    createSupplier(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createSupplierRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Tracking
     */
    createTrackingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.trackingCreateRequest === null || requestParameters.trackingCreateRequest === undefined) {
                throw new runtime.RequiredError('trackingCreateRequest', 'Required parameter requestParameters.trackingCreateRequest was null or undefined when calling createTracking.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/trackings`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.TrackingCreateRequestToJSON(requestParameters.trackingCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Tracking
     */
    createTracking(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createTrackingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Transfers
     */
    createTransfersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.transferCreateData === null || requestParameters.transferCreateData === undefined) {
                throw new runtime.RequiredError('transferCreateData', 'Required parameter requestParameters.transferCreateData was null or undefined when calling createTransfers.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/transfers`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.transferCreateData.map(models_1.TransferCreateDataToJSON),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransfersCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransfersCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Transfers
     */
    createTransfers(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createTransfersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create User
     */
    createUserRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.userCreateRequest === null || requestParameters.userCreateRequest === undefined) {
                throw new runtime.RequiredError('userCreateRequest', 'Required parameter requestParameters.userCreateRequest was null or undefined when calling createUser.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.UserCreateRequestToJSON(requestParameters.userCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create User
     */
    createUser(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createUserRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Webhook
     */
    createWebhookRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.webhookCreateRequest === null || requestParameters.webhookCreateRequest === undefined) {
                throw new runtime.RequiredError('webhookCreateRequest', 'Required parameter requestParameters.webhookCreateRequest was null or undefined when calling createWebhook.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/webhooks`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.WebhookCreateRequestToJSON(requestParameters.webhookCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhookCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhookCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Webhook
     */
    createWebhook(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createWebhookRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Account
     */
    deleteAccountRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orgKey === null || requestParameters.orgKey === undefined) {
                throw new runtime.RequiredError('orgKey', 'Required parameter requestParameters.orgKey was null or undefined when calling deleteAccount.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/accounts/{OrgKey}`.replace(`{${"OrgKey"}}`, encodeURIComponent(String(requestParameters.orgKey))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Account
     */
    deleteAccount(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteAccountRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete API Key
     */
    deleteApiKeyRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.apiKey === null || requestParameters.apiKey === undefined) {
                throw new runtime.RequiredError('apiKey', 'Required parameter requestParameters.apiKey was null or undefined when calling deleteApiKey.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey/{ApiKey}`.replace(`{${"ApiKey"}}`, encodeURIComponent(String(requestParameters.apiKey))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete API Key
     */
    deleteApiKey(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteApiKeyRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Arrival
     */
    deleteArrivalRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.arrivalId === null || requestParameters.arrivalId === undefined) {
                throw new runtime.RequiredError('arrivalId', 'Required parameter requestParameters.arrivalId was null or undefined when calling deleteArrival.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            if (requestParameters.nested !== undefined) {
                queryParameters['nested'] = requestParameters.nested;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/arrivals/{ArrivalId}`.replace(`{${"ArrivalId"}}`, encodeURIComponent(String(requestParameters.arrivalId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Arrival
     */
    deleteArrival(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteArrivalRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Arrivals
     */
    deleteArrivalsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.arrivalId) {
                queryParameters['arrivalId'] = Array.isArray(requestParameters.arrivalId) ? requestParameters.arrivalId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.arrivalId;
            }
            if (requestParameters.transferId) {
                queryParameters['transferId'] = Array.isArray(requestParameters.transferId) ? requestParameters.transferId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.transferId;
            }
            if (requestParameters.purchaseId) {
                queryParameters['purchaseId'] = Array.isArray(requestParameters.purchaseId) ? requestParameters.purchaseId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.purchaseId;
            }
            if (requestParameters.fulfillmentId) {
                queryParameters['fulfillmentId'] = Array.isArray(requestParameters.fulfillmentId) ? requestParameters.fulfillmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.fulfillmentId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/arrivals`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Arrivals
     */
    deleteArrivals(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteArrivalsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Connector
     */
    deleteConnectorRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.connectorId === null || requestParameters.connectorId === undefined) {
                throw new runtime.RequiredError('connectorId', 'Required parameter requestParameters.connectorId was null or undefined when calling deleteConnector.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors/{ConnectorId}`.replace(`{${"ConnectorId"}}`, encodeURIComponent(String(requestParameters.connectorId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Connector
     */
    deleteConnector(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteConnectorRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Event
     */
    deleteEventRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.eventId === null || requestParameters.eventId === undefined) {
                throw new runtime.RequiredError('eventId', 'Required parameter requestParameters.eventId was null or undefined when calling deleteEvent.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events/{EventId}`.replace(`{${"EventId"}}`, encodeURIComponent(String(requestParameters.eventId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Event
     */
    deleteEvent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteEventRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Integration
     */
    deleteIntegrationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.integrationId === null || requestParameters.integrationId === undefined) {
                throw new runtime.RequiredError('integrationId', 'Required parameter requestParameters.integrationId was null or undefined when calling deleteIntegration.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations/{IntegrationId}`.replace(`{${"IntegrationId"}}`, encodeURIComponent(String(requestParameters.integrationId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Integration
     */
    deleteIntegration(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteIntegrationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Inventory
     */
    deleteInventoriesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.inventoryId) {
                queryParameters['inventoryId'] = Array.isArray(requestParameters.inventoryId) ? requestParameters.inventoryId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.inventoryId;
            }
            if (requestParameters.entityId) {
                queryParameters['entityId'] = Array.isArray(requestParameters.entityId) ? requestParameters.entityId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.entityId;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.sku) {
                queryParameters['sku'] = Array.isArray(requestParameters.sku) ? requestParameters.sku.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.sku;
            }
            if (requestParameters.locationId) {
                queryParameters['locationId'] = Array.isArray(requestParameters.locationId) ? requestParameters.locationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.locationId;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.ledger !== undefined) {
                queryParameters['ledger'] = requestParameters.ledger;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventory`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Inventory
     */
    deleteInventories(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteInventoriesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Inventory
     */
    deleteInventoryRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.inventoryId === null || requestParameters.inventoryId === undefined) {
                throw new runtime.RequiredError('inventoryId', 'Required parameter requestParameters.inventoryId was null or undefined when calling deleteInventory.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventory/{InventoryId}`.replace(`{${"InventoryId"}}`, encodeURIComponent(String(requestParameters.inventoryId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Inventory
     */
    deleteInventory(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteInventoryRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Inventory Rule
     */
    deleteInventoryRuleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.ruleId === null || requestParameters.ruleId === undefined) {
                throw new runtime.RequiredError('ruleId', 'Required parameter requestParameters.ruleId was null or undefined when calling deleteInventoryRule.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventoryrules/{RuleId}`.replace(`{${"RuleId"}}`, encodeURIComponent(String(requestParameters.ruleId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Inventory Rule
     */
    deleteInventoryRule(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteInventoryRuleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple InventoryRules
     */
    deleteInventoryRulesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.ruleId) {
                queryParameters['ruleId'] = Array.isArray(requestParameters.ruleId) ? requestParameters.ruleId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.ruleId;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventoryrules`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRulesDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRulesDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple InventoryRules
     */
    deleteInventoryRules(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteInventoryRulesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete (cancel) long-running job
     */
    deleteJobRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.jobId === null || requestParameters.jobId === undefined) {
                throw new runtime.RequiredError('jobId', 'Required parameter requestParameters.jobId was null or undefined when calling deleteJob.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/jobs/{JobId}`.replace(`{${"JobId"}}`, encodeURIComponent(String(requestParameters.jobId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete (cancel) long-running job
     */
    deleteJob(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteJobRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Jobs
     */
    deleteJobsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.jobId) {
                queryParameters['jobId'] = Array.isArray(requestParameters.jobId) ? requestParameters.jobId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.jobId;
            }
            if (requestParameters.type) {
                queryParameters['type'] = Array.isArray(requestParameters.type) ? requestParameters.type.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.type;
            }
            if (requestParameters.subType) {
                queryParameters['subType'] = Array.isArray(requestParameters.subType) ? requestParameters.subType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.subType;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/jobs`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Jobs
     */
    deleteJobs(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteJobsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Label
     */
    deleteLabelRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.labelId === null || requestParameters.labelId === undefined) {
                throw new runtime.RequiredError('labelId', 'Required parameter requestParameters.labelId was null or undefined when calling deleteLabel.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels/{LabelId}`.replace(`{${"LabelId"}}`, encodeURIComponent(String(requestParameters.labelId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Label
     */
    deleteLabel(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteLabelRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Labels
     */
    deleteLabelsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.labelId) {
                queryParameters['labelId'] = Array.isArray(requestParameters.labelId) ? requestParameters.labelId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.labelId;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Labels
     */
    deleteLabels(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteLabelsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Location
     */
    deleteLocationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.locationId === null || requestParameters.locationId === undefined) {
                throw new runtime.RequiredError('locationId', 'Required parameter requestParameters.locationId was null or undefined when calling deleteLocation.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/locations/{LocationId}`.replace(`{${"LocationId"}}`, encodeURIComponent(String(requestParameters.locationId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Location
     */
    deleteLocation(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteLocationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Locations
     */
    deleteLocationsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.locationId) {
                queryParameters['locationId'] = Array.isArray(requestParameters.locationId) ? requestParameters.locationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.locationId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/locations`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Locations
     */
    deleteLocations(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteLocationsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Mapping
     */
    deleteMappingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.mappingId === null || requestParameters.mappingId === undefined) {
                throw new runtime.RequiredError('mappingId', 'Required parameter requestParameters.mappingId was null or undefined when calling deleteMapping.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/mappings/{MappingId}`.replace(`{${"MappingId"}}`, encodeURIComponent(String(requestParameters.mappingId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Mapping
     */
    deleteMapping(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteMappingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Order
     */
    deleteOrderRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orderId === null || requestParameters.orderId === undefined) {
                throw new runtime.RequiredError('orderId', 'Required parameter requestParameters.orderId was null or undefined when calling deleteOrder.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            if (requestParameters.nested !== undefined) {
                queryParameters['nested'] = requestParameters.nested;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/orders/{OrderId}`.replace(`{${"OrderId"}}`, encodeURIComponent(String(requestParameters.orderId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Order
     */
    deleteOrder(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteOrderRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Orders
     */
    deleteOrdersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.orderId) {
                queryParameters['orderId'] = Array.isArray(requestParameters.orderId) ? requestParameters.orderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.orderSource) {
                queryParameters['orderSource'] = Array.isArray(requestParameters.orderSource) ? requestParameters.orderSource.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderSource;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/orders`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrdersDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrdersDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Orders
     */
    deleteOrders(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteOrdersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Organization
     */
    deleteOrganizationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.organizationId === null || requestParameters.organizationId === undefined) {
                throw new runtime.RequiredError('organizationId', 'Required parameter requestParameters.organizationId was null or undefined when calling deleteOrganization.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/organizations/{OrganizationId}`.replace(`{${"OrganizationId"}}`, encodeURIComponent(String(requestParameters.organizationId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Organization
     */
    deleteOrganization(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteOrganizationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Product
     */
    deleteProductRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.productId === null || requestParameters.productId === undefined) {
                throw new runtime.RequiredError('productId', 'Required parameter requestParameters.productId was null or undefined when calling deleteProduct.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/products/{ProductId}`.replace(`{${"ProductId"}}`, encodeURIComponent(String(requestParameters.productId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Product
     */
    deleteProduct(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteProductRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Products
     */
    deleteProductsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.productId) {
                queryParameters['productId'] = Array.isArray(requestParameters.productId) ? requestParameters.productId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.productId;
            }
            if (requestParameters.extProductId) {
                queryParameters['extProductId'] = Array.isArray(requestParameters.extProductId) ? requestParameters.extProductId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extProductId;
            }
            if (requestParameters.parentExtProductId) {
                queryParameters['parentExtProductId'] = Array.isArray(requestParameters.parentExtProductId) ? requestParameters.parentExtProductId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.parentExtProductId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.types) {
                queryParameters['types'] = Array.isArray(requestParameters.types) ? requestParameters.types.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.types;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/products`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Products
     */
    deleteProducts(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteProductsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Purchase
     */
    deletePurchaseRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.purchaseId === null || requestParameters.purchaseId === undefined) {
                throw new runtime.RequiredError('purchaseId', 'Required parameter requestParameters.purchaseId was null or undefined when calling deletePurchase.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            if (requestParameters.nested !== undefined) {
                queryParameters['nested'] = requestParameters.nested;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/purchases/{PurchaseId}`.replace(`{${"PurchaseId"}}`, encodeURIComponent(String(requestParameters.purchaseId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Purchase
     */
    deletePurchase(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deletePurchaseRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Purchases
     */
    deletePurchasesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.purchaseId) {
                queryParameters['purchaseId'] = Array.isArray(requestParameters.purchaseId) ? requestParameters.purchaseId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.purchaseId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/purchases`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchasesDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchasesDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Purchases
     */
    deletePurchases(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deletePurchasesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Refund
     */
    deleteRefundRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.refundId === null || requestParameters.refundId === undefined) {
                throw new runtime.RequiredError('refundId', 'Required parameter requestParameters.refundId was null or undefined when calling deleteRefund.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/refunds/{RefundId}`.replace(`{${"RefundId"}}`, encodeURIComponent(String(requestParameters.refundId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Refund
     */
    deleteRefund(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteRefundRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Refunds
     */
    deleteRefundsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.refundId) {
                queryParameters['refundId'] = Array.isArray(requestParameters.refundId) ? requestParameters.refundId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.refundId;
            }
            if (requestParameters.extRefundId) {
                queryParameters['extRefundId'] = Array.isArray(requestParameters.extRefundId) ? requestParameters.extRefundId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extRefundId;
            }
            if (requestParameters.extReturnId) {
                queryParameters['extReturnId'] = Array.isArray(requestParameters.extReturnId) ? requestParameters.extReturnId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extReturnId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.type) {
                queryParameters['type'] = Array.isArray(requestParameters.type) ? requestParameters.type.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.type;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/refunds`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Refunds
     */
    deleteRefunds(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteRefundsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Return
     */
    deleteReturnRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.returnId === null || requestParameters.returnId === undefined) {
                throw new runtime.RequiredError('returnId', 'Required parameter requestParameters.returnId was null or undefined when calling deleteReturn.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/returns/{ReturnId}`.replace(`{${"ReturnId"}}`, encodeURIComponent(String(requestParameters.returnId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Return
     */
    deleteReturn(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteReturnRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Returns
     */
    deleteReturnsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.returnId) {
                queryParameters['returnId'] = Array.isArray(requestParameters.returnId) ? requestParameters.returnId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.returnId;
            }
            if (requestParameters.extReturnId) {
                queryParameters['extReturnId'] = Array.isArray(requestParameters.extReturnId) ? requestParameters.extReturnId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extReturnId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/returns`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Returns
     */
    deleteReturns(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteReturnsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Role
     */
    deleteRoleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.roleId === null || requestParameters.roleId === undefined) {
                throw new runtime.RequiredError('roleId', 'Required parameter requestParameters.roleId was null or undefined when calling deleteRole.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/roles/{RoleId}`.replace(`{${"RoleId"}}`, encodeURIComponent(String(requestParameters.roleId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Role
     */
    deleteRole(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteRoleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Routing
     */
    deleteRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingId === null || requestParameters.routingId === undefined) {
                throw new runtime.RequiredError('routingId', 'Required parameter requestParameters.routingId was null or undefined when calling deleteRouting.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/routings/{RoutingId}`.replace(`{${"RoutingId"}}`, encodeURIComponent(String(requestParameters.routingId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Routing
     */
    deleteRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Routings
     */
    deleteRoutingsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.routingId) {
                queryParameters['routingId'] = Array.isArray(requestParameters.routingId) ? requestParameters.routingId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.routingId;
            }
            if (requestParameters.uuid) {
                queryParameters['uuid'] = Array.isArray(requestParameters.uuid) ? requestParameters.uuid.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.uuid;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/routings`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Routings
     */
    deleteRoutings(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteRoutingsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Shipment
     */
    deleteShipmentRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.shipmentId === null || requestParameters.shipmentId === undefined) {
                throw new runtime.RequiredError('shipmentId', 'Required parameter requestParameters.shipmentId was null or undefined when calling deleteShipment.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            if (requestParameters.nested !== undefined) {
                queryParameters['nested'] = requestParameters.nested;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/shipments/{ShipmentId}`.replace(`{${"ShipmentId"}}`, encodeURIComponent(String(requestParameters.shipmentId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Shipment
     */
    deleteShipment(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteShipmentRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Shipments
     */
    deleteShipmentsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.shipmentId) {
                queryParameters['shipmentId'] = Array.isArray(requestParameters.shipmentId) ? requestParameters.shipmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.shipmentId;
            }
            if (requestParameters.orderId) {
                queryParameters['orderId'] = Array.isArray(requestParameters.orderId) ? requestParameters.orderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.orderType) {
                queryParameters['orderType'] = Array.isArray(requestParameters.orderType) ? requestParameters.orderType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderType;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/shipments`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentsDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentsDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Shipments
     */
    deleteShipments(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteShipmentsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Supplier
     */
    deleteSupplierRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.supplierId === null || requestParameters.supplierId === undefined) {
                throw new runtime.RequiredError('supplierId', 'Required parameter requestParameters.supplierId was null or undefined when calling deleteSupplier.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/suppliers/{SupplierId}`.replace(`{${"SupplierId"}}`, encodeURIComponent(String(requestParameters.supplierId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Supplier
     */
    deleteSupplier(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteSupplierRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Suppliers
     */
    deleteSuppliersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.supplierId) {
                queryParameters['supplierId'] = Array.isArray(requestParameters.supplierId) ? requestParameters.supplierId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.supplierId;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/suppliers`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuppliersDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuppliersDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Suppliers
     */
    deleteSuppliers(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteSuppliersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Transfer
     */
    deleteTransferRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.transferId === null || requestParameters.transferId === undefined) {
                throw new runtime.RequiredError('transferId', 'Required parameter requestParameters.transferId was null or undefined when calling deleteTransfer.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            if (requestParameters.nested !== undefined) {
                queryParameters['nested'] = requestParameters.nested;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/transfers/{TransferId}`.replace(`{${"TransferId"}}`, encodeURIComponent(String(requestParameters.transferId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Transfer
     */
    deleteTransfer(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteTransferRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Multiple Transfers
     */
    deleteTransfersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.transferId) {
                queryParameters['transferId'] = Array.isArray(requestParameters.transferId) ? requestParameters.transferId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.transferId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/transfers`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransfersDeleteErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransfersDeleteResponseFromJSON(jsonValue));
        });
    }
    /**
     * Delete Multiple Transfers
     */
    deleteTransfers(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteTransfersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete User
     */
    deleteUserRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.userId === null || requestParameters.userId === undefined) {
                throw new runtime.RequiredError('userId', 'Required parameter requestParameters.userId was null or undefined when calling deleteUser.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users/{UserId}`.replace(`{${"UserId"}}`, encodeURIComponent(String(requestParameters.userId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete User
     */
    deleteUser(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteUserRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Webhook
     */
    deleteWebhookRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.webhookId === null || requestParameters.webhookId === undefined) {
                throw new runtime.RequiredError('webhookId', 'Required parameter requestParameters.webhookId was null or undefined when calling deleteWebhook.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/webhooks/{WebhookId}`.replace(`{${"WebhookId"}}`, encodeURIComponent(String(requestParameters.webhookId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Webhook
     */
    deleteWebhook(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteWebhookRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Download Label Content
     */
    downloadLabelContentRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.labelId === null || requestParameters.labelId === undefined) {
                throw new runtime.RequiredError('labelId', 'Required parameter requestParameters.labelId was null or undefined when calling downloadLabelContent.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels/{LabelId}/download`.replace(`{${"LabelId"}}`, encodeURIComponent(String(requestParameters.labelId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            return new runtime.BlobApiResponse(response);
        });
    }
    /**
     * Download Label Content
     */
    downloadLabelContent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.downloadLabelContentRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Account
     */
    fetchAccountRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orgKey === null || requestParameters.orgKey === undefined) {
                throw new runtime.RequiredError('orgKey', 'Required parameter requestParameters.orgKey was null or undefined when calling fetchAccount.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/accounts/{OrgKey}`.replace(`{${"OrgKey"}}`, encodeURIComponent(String(requestParameters.orgKey))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Account
     */
    fetchAccount(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchAccountRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch API Key Info
     */
    fetchApiKeyRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.apiKey === null || requestParameters.apiKey === undefined) {
                throw new runtime.RequiredError('apiKey', 'Required parameter requestParameters.apiKey was null or undefined when calling fetchApiKey.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey/{ApiKey}`.replace(`{${"ApiKey"}}`, encodeURIComponent(String(requestParameters.apiKey))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch API Key Info
     */
    fetchApiKey(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchApiKeyRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch API Key Info, including the key value
     */
    fetchApiKeyValueRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.apiKey === null || requestParameters.apiKey === undefined) {
                throw new runtime.RequiredError('apiKey', 'Required parameter requestParameters.apiKey was null or undefined when calling fetchApiKeyValue.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey/{ApiKey}/value`.replace(`{${"ApiKey"}}`, encodeURIComponent(String(requestParameters.apiKey))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyValueFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch API Key Info, including the key value
     */
    fetchApiKeyValue(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchApiKeyValueRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Arrival Notification
     */
    fetchArrivalRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.arrivalId === null || requestParameters.arrivalId === undefined) {
                throw new runtime.RequiredError('arrivalId', 'Required parameter requestParameters.arrivalId was null or undefined when calling fetchArrival.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/arrivals/{ArrivalId}`.replace(`{${"ArrivalId"}}`, encodeURIComponent(String(requestParameters.arrivalId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Arrival Notification
     */
    fetchArrival(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchArrivalRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Connector
     */
    fetchConnectorRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.connectorId === null || requestParameters.connectorId === undefined) {
                throw new runtime.RequiredError('connectorId', 'Required parameter requestParameters.connectorId was null or undefined when calling fetchConnector.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors/{ConnectorId}`.replace(`{${"ConnectorId"}}`, encodeURIComponent(String(requestParameters.connectorId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Connector
     */
    fetchConnector(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchConnectorRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Entity Schema
     */
    fetchEntitySchemaRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.schemaId === null || requestParameters.schemaId === undefined) {
                throw new runtime.RequiredError('schemaId', 'Required parameter requestParameters.schemaId was null or undefined when calling fetchEntitySchema.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_schemas/{SchemaId}`.replace(`{${"SchemaId"}}`, encodeURIComponent(String(requestParameters.schemaId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Entity Schema
     */
    fetchEntitySchema(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchEntitySchemaRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Event
     */
    fetchEventRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.eventId === null || requestParameters.eventId === undefined) {
                throw new runtime.RequiredError('eventId', 'Required parameter requestParameters.eventId was null or undefined when calling fetchEvent.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events/{EventId}`.replace(`{${"EventId"}}`, encodeURIComponent(String(requestParameters.eventId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Event
     */
    fetchEvent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchEventRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Fulfillment
     */
    fetchFulfillmentRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.fulfillmentId === null || requestParameters.fulfillmentId === undefined) {
                throw new runtime.RequiredError('fulfillmentId', 'Required parameter requestParameters.fulfillmentId was null or undefined when calling fetchFulfillment.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/fulfillments/{FulfillmentId}`.replace(`{${"FulfillmentId"}}`, encodeURIComponent(String(requestParameters.fulfillmentId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FulfillmentFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.FulfillmentFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Fulfillment
     */
    fetchFulfillment(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchFulfillmentRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Integration
     */
    fetchIntegrationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.integrationId === null || requestParameters.integrationId === undefined) {
                throw new runtime.RequiredError('integrationId', 'Required parameter requestParameters.integrationId was null or undefined when calling fetchIntegration.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations/{IntegrationId}`.replace(`{${"IntegrationId"}}`, encodeURIComponent(String(requestParameters.integrationId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Integration
     */
    fetchIntegration(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchIntegrationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Inventory Log Item
     */
    fetchInventoryRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.inventoryId === null || requestParameters.inventoryId === undefined) {
                throw new runtime.RequiredError('inventoryId', 'Required parameter requestParameters.inventoryId was null or undefined when calling fetchInventory.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventory/{InventoryId}`.replace(`{${"InventoryId"}}`, encodeURIComponent(String(requestParameters.inventoryId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Inventory Log Item
     */
    fetchInventory(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchInventoryRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Inventory Rule
     */
    fetchInventoryRuleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.ruleId === null || requestParameters.ruleId === undefined) {
                throw new runtime.RequiredError('ruleId', 'Required parameter requestParameters.ruleId was null or undefined when calling fetchInventoryRule.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventoryrules/{RuleId}`.replace(`{${"RuleId"}}`, encodeURIComponent(String(requestParameters.ruleId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRuleFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRuleFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Inventory Rule
     */
    fetchInventoryRule(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchInventoryRuleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch long-running job
     */
    fetchJobRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.jobId === null || requestParameters.jobId === undefined) {
                throw new runtime.RequiredError('jobId', 'Required parameter requestParameters.jobId was null or undefined when calling fetchJob.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/jobs/{JobId}`.replace(`{${"JobId"}}`, encodeURIComponent(String(requestParameters.jobId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch long-running job
     */
    fetchJob(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchJobRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch long-running job results
     */
    fetchJobResultsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.jobId === null || requestParameters.jobId === undefined) {
                throw new runtime.RequiredError('jobId', 'Required parameter requestParameters.jobId was null or undefined when calling fetchJobResults.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/jobs/{JobId}/results`.replace(`{${"JobId"}}`, encodeURIComponent(String(requestParameters.jobId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            return new runtime.TextApiResponse(response);
        });
    }
    /**
     * Fetch long-running job results
     */
    fetchJobResults(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchJobResultsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Label
     */
    fetchLabelRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.labelId === null || requestParameters.labelId === undefined) {
                throw new runtime.RequiredError('labelId', 'Required parameter requestParameters.labelId was null or undefined when calling fetchLabel.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels/{LabelId}`.replace(`{${"LabelId"}}`, encodeURIComponent(String(requestParameters.labelId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Label
     */
    fetchLabel(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchLabelRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Location
     */
    fetchLocationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.locationId === null || requestParameters.locationId === undefined) {
                throw new runtime.RequiredError('locationId', 'Required parameter requestParameters.locationId was null or undefined when calling fetchLocation.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/locations/{LocationId}`.replace(`{${"LocationId"}}`, encodeURIComponent(String(requestParameters.locationId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Location
     */
    fetchLocation(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchLocationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Mapping
     */
    fetchMappingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.mappingId === null || requestParameters.mappingId === undefined) {
                throw new runtime.RequiredError('mappingId', 'Required parameter requestParameters.mappingId was null or undefined when calling fetchMapping.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/mappings/{MappingId}`.replace(`{${"MappingId"}}`, encodeURIComponent(String(requestParameters.mappingId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Mapping
     */
    fetchMapping(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchMappingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Order
     */
    fetchOrderRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orderId === null || requestParameters.orderId === undefined) {
                throw new runtime.RequiredError('orderId', 'Required parameter requestParameters.orderId was null or undefined when calling fetchOrder.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/orders/{OrderId}`.replace(`{${"OrderId"}}`, encodeURIComponent(String(requestParameters.orderId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Order
     */
    fetchOrder(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchOrderRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Organization
     */
    fetchOrganizationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.organizationId === null || requestParameters.organizationId === undefined) {
                throw new runtime.RequiredError('organizationId', 'Required parameter requestParameters.organizationId was null or undefined when calling fetchOrganization.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/organizations/{OrganizationId}`.replace(`{${"OrganizationId"}}`, encodeURIComponent(String(requestParameters.organizationId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Organization
     */
    fetchOrganization(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchOrganizationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Retrieve Product
     */
    fetchProductRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.productId === null || requestParameters.productId === undefined) {
                throw new runtime.RequiredError('productId', 'Required parameter requestParameters.productId was null or undefined when calling fetchProduct.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/products/{ProductId}`.replace(`{${"ProductId"}}`, encodeURIComponent(String(requestParameters.productId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Retrieve Product
     */
    fetchProduct(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchProductRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Purchase
     */
    fetchPurchaseRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.purchaseId === null || requestParameters.purchaseId === undefined) {
                throw new runtime.RequiredError('purchaseId', 'Required parameter requestParameters.purchaseId was null or undefined when calling fetchPurchase.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/purchases/{PurchaseId}`.replace(`{${"PurchaseId"}}`, encodeURIComponent(String(requestParameters.purchaseId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchaseFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchaseFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Purchase
     */
    fetchPurchase(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchPurchaseRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Receipt
     */
    fetchReceiptRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.receiptId === null || requestParameters.receiptId === undefined) {
                throw new runtime.RequiredError('receiptId', 'Required parameter requestParameters.receiptId was null or undefined when calling fetchReceipt.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/receipts/{ReceiptId}`.replace(`{${"ReceiptId"}}`, encodeURIComponent(String(requestParameters.receiptId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReceiptFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReceiptFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Receipt
     */
    fetchReceipt(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchReceiptRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Refund
     */
    fetchRefundRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.refundId === null || requestParameters.refundId === undefined) {
                throw new runtime.RequiredError('refundId', 'Required parameter requestParameters.refundId was null or undefined when calling fetchRefund.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/refunds/{RefundId}`.replace(`{${"RefundId"}}`, encodeURIComponent(String(requestParameters.refundId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Refund
     */
    fetchRefund(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchRefundRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Return
     */
    fetchReturnRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.returnId === null || requestParameters.returnId === undefined) {
                throw new runtime.RequiredError('returnId', 'Required parameter requestParameters.returnId was null or undefined when calling fetchReturn.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/returns/{ReturnId}`.replace(`{${"ReturnId"}}`, encodeURIComponent(String(requestParameters.returnId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Return
     */
    fetchReturn(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchReturnRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Role
     */
    fetchRoleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.roleId === null || requestParameters.roleId === undefined) {
                throw new runtime.RequiredError('roleId', 'Required parameter requestParameters.roleId was null or undefined when calling fetchRole.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/roles/{RoleId}`.replace(`{${"RoleId"}}`, encodeURIComponent(String(requestParameters.roleId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoleFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoleFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Role
     */
    fetchRole(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchRoleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Routing
     */
    fetchRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingId === null || requestParameters.routingId === undefined) {
                throw new runtime.RequiredError('routingId', 'Required parameter requestParameters.routingId was null or undefined when calling fetchRouting.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/routings/{RoutingId}`.replace(`{${"RoutingId"}}`, encodeURIComponent(String(requestParameters.routingId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Routing
     */
    fetchRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Shipment
     */
    fetchShipmentRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.shipmentId === null || requestParameters.shipmentId === undefined) {
                throw new runtime.RequiredError('shipmentId', 'Required parameter requestParameters.shipmentId was null or undefined when calling fetchShipment.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/shipments/{ShipmentId}`.replace(`{${"ShipmentId"}}`, encodeURIComponent(String(requestParameters.shipmentId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Shipment
     */
    fetchShipment(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchShipmentRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Supplier
     */
    fetchSupplierRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.supplierId === null || requestParameters.supplierId === undefined) {
                throw new runtime.RequiredError('supplierId', 'Required parameter requestParameters.supplierId was null or undefined when calling fetchSupplier.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/suppliers/{SupplierId}`.replace(`{${"SupplierId"}}`, encodeURIComponent(String(requestParameters.supplierId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.SupplierFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SupplierFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Supplier
     */
    fetchSupplier(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchSupplierRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Tracking
     */
    fetchTrackingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.trackingId === null || requestParameters.trackingId === undefined) {
                throw new runtime.RequiredError('trackingId', 'Required parameter requestParameters.trackingId was null or undefined when calling fetchTracking.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/trackings/{TrackingId}`.replace(`{${"TrackingId"}}`, encodeURIComponent(String(requestParameters.trackingId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Tracking
     */
    fetchTracking(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchTrackingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Transfer
     */
    fetchTransferRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.transferId === null || requestParameters.transferId === undefined) {
                throw new runtime.RequiredError('transferId', 'Required parameter requestParameters.transferId was null or undefined when calling fetchTransfer.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/transfers/{TransferId}`.replace(`{${"TransferId"}}`, encodeURIComponent(String(requestParameters.transferId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransferFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransferFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Transfer
     */
    fetchTransfer(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchTransferRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch User
     */
    fetchUserRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.userId === null || requestParameters.userId === undefined) {
                throw new runtime.RequiredError('userId', 'Required parameter requestParameters.userId was null or undefined when calling fetchUser.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users/{UserId}`.replace(`{${"UserId"}}`, encodeURIComponent(String(requestParameters.userId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch User
     */
    fetchUser(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchUserRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Webhook
     */
    fetchWebhookRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.webhookId === null || requestParameters.webhookId === undefined) {
                throw new runtime.RequiredError('webhookId', 'Required parameter requestParameters.webhookId was null or undefined when calling fetchWebhook.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/webhooks/{WebhookId}`.replace(`{${"WebhookId"}}`, encodeURIComponent(String(requestParameters.webhookId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhookFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhookFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Webhook
     */
    fetchWebhook(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchWebhookRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Accounts
     */
    listAccountsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.orgKey) {
                queryParameters['orgKey'] = Array.isArray(requestParameters.orgKey) ? requestParameters.orgKey.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orgKey;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/accounts`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Accounts
     */
    listAccounts(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listAccountsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List API Keys
     */
    listApiKeysRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.connector) {
                queryParameters['connector'] = Array.isArray(requestParameters.connector) ? requestParameters.connector.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.connector;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeysListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeysListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List API Keys
     */
    listApiKeys(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listApiKeysRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Arrival Notifications
     */
    listArrivalsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.arrivalId) {
                queryParameters['arrivalId'] = Array.isArray(requestParameters.arrivalId) ? requestParameters.arrivalId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.arrivalId;
            }
            if (requestParameters.transferId) {
                queryParameters['transferId'] = Array.isArray(requestParameters.transferId) ? requestParameters.transferId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.transferId;
            }
            if (requestParameters.purchaseId) {
                queryParameters['purchaseId'] = Array.isArray(requestParameters.purchaseId) ? requestParameters.purchaseId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.purchaseId;
            }
            if (requestParameters.fulfillmentId) {
                queryParameters['fulfillmentId'] = Array.isArray(requestParameters.fulfillmentId) ? requestParameters.fulfillmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.fulfillmentId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/arrivals`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Arrival Notifications
     */
    listArrivals(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listArrivalsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Connectors
     */
    listConnectorsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.connectorId) {
                queryParameters['connectorId'] = Array.isArray(requestParameters.connectorId) ? requestParameters.connectorId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.connectorId;
            }
            if (requestParameters.connectorName) {
                queryParameters['connectorName'] = Array.isArray(requestParameters.connectorName) ? requestParameters.connectorName.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.connectorName;
            }
            if (requestParameters.connectorType) {
                queryParameters['connectorType'] = Array.isArray(requestParameters.connectorType) ? requestParameters.connectorType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.connectorType;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Connectors
     */
    listConnectors(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listConnectorsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Events
     */
    listEventsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.eventId) {
                queryParameters['eventId'] = Array.isArray(requestParameters.eventId) ? requestParameters.eventId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.eventId;
            }
            if (requestParameters.source) {
                queryParameters['source'] = Array.isArray(requestParameters.source) ? requestParameters.source.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.source;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.requestId) {
                queryParameters['requestId'] = Array.isArray(requestParameters.requestId) ? requestParameters.requestId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.requestId;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Events
     */
    listEvents(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listEventsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Fulfillments
     */
    listFulfillmentsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.fulfillmentId) {
                queryParameters['fulfillmentId'] = Array.isArray(requestParameters.fulfillmentId) ? requestParameters.fulfillmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.fulfillmentId;
            }
            if (requestParameters.shipmentId) {
                queryParameters['shipmentId'] = Array.isArray(requestParameters.shipmentId) ? requestParameters.shipmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.shipmentId;
            }
            if (requestParameters.extShipmentId) {
                queryParameters['extShipmentId'] = Array.isArray(requestParameters.extShipmentId) ? requestParameters.extShipmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extShipmentId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/fulfillments`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FulfillmentsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.FulfillmentsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Fulfillments
     */
    listFulfillments(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listFulfillmentsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Integrations
     */
    listIntegrationsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.integrationId) {
                queryParameters['integrationId'] = Array.isArray(requestParameters.integrationId) ? requestParameters.integrationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integrationId;
            }
            if (requestParameters.connectorName) {
                queryParameters['connectorName'] = Array.isArray(requestParameters.connectorName) ? requestParameters.connectorName.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.connectorName;
            }
            if (requestParameters.connectorId) {
                queryParameters['connectorId'] = Array.isArray(requestParameters.connectorId) ? requestParameters.connectorId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.connectorId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Integrations
     */
    listIntegrations(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listIntegrationsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Inventory Log Items
     */
    listInventoryRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.inventoryId) {
                queryParameters['inventoryId'] = Array.isArray(requestParameters.inventoryId) ? requestParameters.inventoryId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.inventoryId;
            }
            if (requestParameters.entityId) {
                queryParameters['entityId'] = Array.isArray(requestParameters.entityId) ? requestParameters.entityId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.entityId;
            }
            if (requestParameters.sku) {
                queryParameters['sku'] = Array.isArray(requestParameters.sku) ? requestParameters.sku.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.sku;
            }
            if (requestParameters.locationId) {
                queryParameters['locationId'] = Array.isArray(requestParameters.locationId) ? requestParameters.locationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.locationId;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.ledger !== undefined) {
                queryParameters['ledger'] = requestParameters.ledger;
            }
            if (requestParameters.totals !== undefined) {
                queryParameters['totals'] = requestParameters.totals;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventory`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Inventory Log Items
     */
    listInventory(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listInventoryRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Inventory Rules
     */
    listInventoryRulesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.ruleId) {
                queryParameters['ruleId'] = Array.isArray(requestParameters.ruleId) ? requestParameters.ruleId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.ruleId;
            }
            if (requestParameters.event) {
                queryParameters['event'] = Array.isArray(requestParameters.event) ? requestParameters.event.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.event;
            }
            if (requestParameters.ptype) {
                queryParameters['ptype'] = Array.isArray(requestParameters.ptype) ? requestParameters.ptype.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.ptype;
            }
            if (requestParameters.partner) {
                queryParameters['partner'] = Array.isArray(requestParameters.partner) ? requestParameters.partner.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.partner;
            }
            if (requestParameters.integration) {
                queryParameters['integration'] = Array.isArray(requestParameters.integration) ? requestParameters.integration.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.integration;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventoryrules`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRulesListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRulesListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Inventory Rules
     */
    listInventoryRules(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listInventoryRulesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List long-running jobs
     */
    listJobsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.jobId) {
                queryParameters['jobId'] = Array.isArray(requestParameters.jobId) ? requestParameters.jobId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.jobId;
            }
            if (requestParameters.type) {
                queryParameters['type'] = Array.isArray(requestParameters.type) ? requestParameters.type.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.type;
            }
            if (requestParameters.subType) {
                queryParameters['subType'] = Array.isArray(requestParameters.subType) ? requestParameters.subType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.subType;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/jobs`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.JobsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List long-running jobs
     */
    listJobs(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listJobsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Labels
     */
    listLabelsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.labelId) {
                queryParameters['labelId'] = Array.isArray(requestParameters.labelId) ? requestParameters.labelId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.labelId;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LabelsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Labels
     */
    listLabels(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listLabelsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Locations
     */
    listLocationsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.locationId) {
                queryParameters['locationId'] = Array.isArray(requestParameters.locationId) ? requestParameters.locationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.locationId;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.email !== undefined) {
                queryParameters['email'] = requestParameters.email;
            }
            if (requestParameters.phone !== undefined) {
                queryParameters['phone'] = requestParameters.phone;
            }
            if (requestParameters.extLocationId) {
                queryParameters['extLocationId'] = Array.isArray(requestParameters.extLocationId) ? requestParameters.extLocationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extLocationId;
            }
            if (requestParameters.extIntegrationId) {
                queryParameters['extIntegrationId'] = Array.isArray(requestParameters.extIntegrationId) ? requestParameters.extIntegrationId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extIntegrationId;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/locations`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Locations
     */
    listLocations(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listLocationsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Mappings
     */
    listMappingsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.mappingId) {
                queryParameters['mappingId'] = Array.isArray(requestParameters.mappingId) ? requestParameters.mappingId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.mappingId;
            }
            if (requestParameters.uuid) {
                queryParameters['uuid'] = Array.isArray(requestParameters.uuid) ? requestParameters.uuid.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.uuid;
            }
            if (requestParameters.desc !== undefined) {
                queryParameters['desc'] = requestParameters.desc;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/mappings`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Mappings
     */
    listMappings(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listMappingsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Orders
     */
    listOrdersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.orderId) {
                queryParameters['orderId'] = Array.isArray(requestParameters.orderId) ? requestParameters.orderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.orderSource) {
                queryParameters['orderSource'] = Array.isArray(requestParameters.orderSource) ? requestParameters.orderSource.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderSource;
            }
            if (requestParameters.email !== undefined) {
                queryParameters['email'] = requestParameters.email;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.requireShippingLabels !== undefined) {
                queryParameters['requireShippingLabels'] = requestParameters.requireShippingLabels;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.timestamp !== undefined) {
                queryParameters['timestamp'] = requestParameters.timestamp;
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/orders`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrdersListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrdersListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Orders
     */
    listOrders(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listOrdersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Organizations
     */
    listOrganizationsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.orgKey) {
                queryParameters['orgKey'] = Array.isArray(requestParameters.orgKey) ? requestParameters.orgKey.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orgKey;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.phone !== undefined) {
                queryParameters['phone'] = requestParameters.phone;
            }
            if (requestParameters.email !== undefined) {
                queryParameters['email'] = requestParameters.email;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/organizations`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Organizations
     */
    listOrganizations(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listOrganizationsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Products
     */
    listProductsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.productId) {
                queryParameters['productId'] = Array.isArray(requestParameters.productId) ? requestParameters.productId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.productId;
            }
            if (requestParameters.sku) {
                queryParameters['sku'] = Array.isArray(requestParameters.sku) ? requestParameters.sku.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.sku;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.extProductId) {
                queryParameters['extProductId'] = Array.isArray(requestParameters.extProductId) ? requestParameters.extProductId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extProductId;
            }
            if (requestParameters.parentExtProductId) {
                queryParameters['parentExtProductId'] = Array.isArray(requestParameters.parentExtProductId) ? requestParameters.parentExtProductId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.parentExtProductId;
            }
            if (requestParameters.bundleSKU !== undefined) {
                queryParameters['bundleSKU'] = requestParameters.bundleSKU;
            }
            if (requestParameters.inBundle !== undefined) {
                queryParameters['inBundle'] = requestParameters.inBundle;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.types) {
                queryParameters['types'] = Array.isArray(requestParameters.types) ? requestParameters.types.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.types;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/products`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProducstListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProducstListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Products
     */
    listProducts(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listProductsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Purchases
     */
    listPurchasesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.purchaseId) {
                queryParameters['purchaseId'] = Array.isArray(requestParameters.purchaseId) ? requestParameters.purchaseId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.purchaseId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.timestamp !== undefined) {
                queryParameters['timestamp'] = requestParameters.timestamp;
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/purchases`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchasesListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchasesListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Purchases
     */
    listPurchases(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listPurchasesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Receipts
     */
    listReceiptsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.receiptId) {
                queryParameters['receiptId'] = Array.isArray(requestParameters.receiptId) ? requestParameters.receiptId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.receiptId;
            }
            if (requestParameters.arrivalId) {
                queryParameters['arrivalId'] = Array.isArray(requestParameters.arrivalId) ? requestParameters.arrivalId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.arrivalId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/receipts`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReceiptsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReceiptsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Receipts
     */
    listReceipts(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listReceiptsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Refunds
     */
    listRefundsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.refundId) {
                queryParameters['refundId'] = Array.isArray(requestParameters.refundId) ? requestParameters.refundId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.refundId;
            }
            if (requestParameters.extRefundId) {
                queryParameters['extRefundId'] = Array.isArray(requestParameters.extRefundId) ? requestParameters.extRefundId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extRefundId;
            }
            if (requestParameters.extReturnId) {
                queryParameters['extReturnId'] = Array.isArray(requestParameters.extReturnId) ? requestParameters.extReturnId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extReturnId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.type) {
                queryParameters['type'] = Array.isArray(requestParameters.type) ? requestParameters.type.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.type;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/refunds`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Refunds
     */
    listRefunds(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listRefundsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Returns
     */
    listReturnsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.returnId) {
                queryParameters['returnId'] = Array.isArray(requestParameters.returnId) ? requestParameters.returnId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.returnId;
            }
            if (requestParameters.extReturnId) {
                queryParameters['extReturnId'] = Array.isArray(requestParameters.extReturnId) ? requestParameters.extReturnId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extReturnId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/returns`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Returns
     */
    listReturns(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listReturnsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Roles
     */
    listRolesRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.roleId) {
                queryParameters['roleId'] = Array.isArray(requestParameters.roleId) ? requestParameters.roleId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.roleId;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/roles`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RolesListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RolesListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Roles
     */
    listRoles(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listRolesRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Routings
     */
    listRoutingsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.routingId) {
                queryParameters['routingId'] = Array.isArray(requestParameters.routingId) ? requestParameters.routingId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.routingId;
            }
            if (requestParameters.uuid) {
                queryParameters['uuid'] = Array.isArray(requestParameters.uuid) ? requestParameters.uuid.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.uuid;
            }
            if (requestParameters.desc !== undefined) {
                queryParameters['desc'] = requestParameters.desc;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/routings`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Routings
     */
    listRoutings(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listRoutingsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Shipments
     */
    listShipmentsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.shipmentId) {
                queryParameters['shipmentId'] = Array.isArray(requestParameters.shipmentId) ? requestParameters.shipmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.shipmentId;
            }
            if (requestParameters.orderId) {
                queryParameters['orderId'] = Array.isArray(requestParameters.orderId) ? requestParameters.orderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.orderType) {
                queryParameters['orderType'] = Array.isArray(requestParameters.orderType) ? requestParameters.orderType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.orderType;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/shipments`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Shipments
     */
    listShipments(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listShipmentsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Suppliers
     */
    listSuppliersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.supplierId) {
                queryParameters['supplierId'] = Array.isArray(requestParameters.supplierId) ? requestParameters.supplierId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.supplierId;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/suppliers`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuppliersListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuppliersListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Suppliers
     */
    listSuppliers(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listSuppliersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Trackings
     */
    listTrackingsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.trackingId) {
                queryParameters['trackingId'] = Array.isArray(requestParameters.trackingId) ? requestParameters.trackingId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.trackingId;
            }
            if (requestParameters.trackingNumber) {
                queryParameters['trackingNumber'] = Array.isArray(requestParameters.trackingNumber) ? requestParameters.trackingNumber.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.trackingNumber;
            }
            if (requestParameters.fulfillmentId) {
                queryParameters['fulfillmentId'] = Array.isArray(requestParameters.fulfillmentId) ? requestParameters.fulfillmentId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.fulfillmentId;
            }
            if (requestParameters.shippingCarrier) {
                queryParameters['shippingCarrier'] = Array.isArray(requestParameters.shippingCarrier) ? requestParameters.shippingCarrier.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.shippingCarrier;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/trackings`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Trackings
     */
    listTrackings(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listTrackingsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Transfers
     */
    listTransfersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.transferId) {
                queryParameters['transferId'] = Array.isArray(requestParameters.transferId) ? requestParameters.transferId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.transferId;
            }
            if (requestParameters.extOrderId) {
                queryParameters['extOrderId'] = Array.isArray(requestParameters.extOrderId) ? requestParameters.extOrderId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.extOrderId;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.deleted !== undefined) {
                queryParameters['deleted'] = requestParameters.deleted;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.timestamp !== undefined) {
                queryParameters['timestamp'] = requestParameters.timestamp;
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/transfers`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransfersListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransfersListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Transfers
     */
    listTransfers(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listTransfersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Users
     */
    listUsersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.userId) {
                queryParameters['userId'] = Array.isArray(requestParameters.userId) ? requestParameters.userId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.userId;
            }
            if (requestParameters.name !== undefined) {
                queryParameters['name'] = requestParameters.name;
            }
            if (requestParameters.email !== undefined) {
                queryParameters['email'] = requestParameters.email;
            }
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.organizations !== undefined) {
                queryParameters['organizations'] = requestParameters.organizations;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.UsersListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.UsersListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Users
     */
    listUsers(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listUsersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Webhooks
     */
    listWebhooksRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.webhookId) {
                queryParameters['webhookId'] = Array.isArray(requestParameters.webhookId) ? requestParameters.webhookId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.webhookId;
            }
            if (requestParameters.topic !== undefined) {
                queryParameters['topic'] = requestParameters.topic;
            }
            if (requestParameters.related !== undefined) {
                queryParameters['related'] = requestParameters.related;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/webhooks`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhooksListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhooksListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Webhooks
     */
    listWebhooks(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listWebhooksRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Send verification email to user
     */
    reinviteUserRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.userId === null || requestParameters.userId === undefined) {
                throw new runtime.RequiredError('userId', 'Required parameter requestParameters.userId was null or undefined when calling reinviteUser.');
            }
            if (requestParameters.body === null || requestParameters.body === undefined) {
                throw new runtime.RequiredError('body', 'Required parameter requestParameters.body was null or undefined when calling reinviteUser.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users/{UserId}/reinvite`.replace(`{${"UserId"}}`, encodeURIComponent(String(requestParameters.userId))),
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.body,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Send verification email to user
     */
    reinviteUser(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.reinviteUserRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Replay event entry
     */
    replayEventRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.eventId === null || requestParameters.eventId === undefined) {
                throw new runtime.RequiredError('eventId', 'Required parameter requestParameters.eventId was null or undefined when calling replayEvent.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events/{EventId}/replay`.replace(`{${"EventId"}}`, encodeURIComponent(String(requestParameters.eventId))),
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventReplayErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventReplayResponseFromJSON(jsonValue));
        });
    }
    /**
     * Replay event entry
     */
    replayEvent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.replayEventRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Reset user\'s password
     */
    resetPasswordRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.userId === null || requestParameters.userId === undefined) {
                throw new runtime.RequiredError('userId', 'Required parameter requestParameters.userId was null or undefined when calling resetPassword.');
            }
            if (requestParameters.body === null || requestParameters.body === undefined) {
                throw new runtime.RequiredError('body', 'Required parameter requestParameters.body was null or undefined when calling resetPassword.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users/{UserId}/reset_password`.replace(`{${"UserId"}}`, encodeURIComponent(String(requestParameters.userId))),
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.body,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Reset user\'s password
     */
    resetPassword(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.resetPasswordRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Account
     */
    updateAccountRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.activationKey === null || requestParameters.activationKey === undefined) {
                throw new runtime.RequiredError('activationKey', 'Required parameter requestParameters.activationKey was null or undefined when calling updateAccount.');
            }
            if (requestParameters.accountUpdateRequest === null || requestParameters.accountUpdateRequest === undefined) {
                throw new runtime.RequiredError('accountUpdateRequest', 'Required parameter requestParameters.accountUpdateRequest was null or undefined when calling updateAccount.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/accounts/{ActivationKey}`.replace(`{${"ActivationKey"}}`, encodeURIComponent(String(requestParameters.activationKey))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.AccountUpdateRequestToJSON(requestParameters.accountUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.AccountUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Account
     */
    updateAccount(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateAccountRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update API Key Info
     */
    updateApiKeyRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.apiKey === null || requestParameters.apiKey === undefined) {
                throw new runtime.RequiredError('apiKey', 'Required parameter requestParameters.apiKey was null or undefined when calling updateApiKey.');
            }
            if (requestParameters.apiKeyUpdateRequest === null || requestParameters.apiKeyUpdateRequest === undefined) {
                throw new runtime.RequiredError('apiKeyUpdateRequest', 'Required parameter requestParameters.apiKeyUpdateRequest was null or undefined when calling updateApiKey.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey/{ApiKey}`.replace(`{${"ApiKey"}}`, encodeURIComponent(String(requestParameters.apiKey))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ApiKeyUpdateRequestToJSON(requestParameters.apiKeyUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ApiKeyUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update API Key Info
     */
    updateApiKey(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateApiKeyRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Arrival Notification
     */
    updateArrivalRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.arrivalId === null || requestParameters.arrivalId === undefined) {
                throw new runtime.RequiredError('arrivalId', 'Required parameter requestParameters.arrivalId was null or undefined when calling updateArrival.');
            }
            if (requestParameters.arrivalUpdateRequest === null || requestParameters.arrivalUpdateRequest === undefined) {
                throw new runtime.RequiredError('arrivalUpdateRequest', 'Required parameter requestParameters.arrivalUpdateRequest was null or undefined when calling updateArrival.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/arrivals/{ArrivalId}`.replace(`{${"ArrivalId"}}`, encodeURIComponent(String(requestParameters.arrivalId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ArrivalUpdateRequestToJSON(requestParameters.arrivalUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ArrivalUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Arrival Notification
     */
    updateArrival(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateArrivalRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Connector
     */
    updateConnectorRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.connectorId === null || requestParameters.connectorId === undefined) {
                throw new runtime.RequiredError('connectorId', 'Required parameter requestParameters.connectorId was null or undefined when calling updateConnector.');
            }
            if (requestParameters.connectorUpdateRequest === null || requestParameters.connectorUpdateRequest === undefined) {
                throw new runtime.RequiredError('connectorUpdateRequest', 'Required parameter requestParameters.connectorUpdateRequest was null or undefined when calling updateConnector.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors/{ConnectorId}`.replace(`{${"ConnectorId"}}`, encodeURIComponent(String(requestParameters.connectorId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ConnectorUpdateRequestToJSON(requestParameters.connectorUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Connector
     */
    updateConnector(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateConnectorRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Entity Schema
     */
    updateEntitySchemaRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.schemaId === null || requestParameters.schemaId === undefined) {
                throw new runtime.RequiredError('schemaId', 'Required parameter requestParameters.schemaId was null or undefined when calling updateEntitySchema.');
            }
            if (requestParameters.entitySchemaUpdateRequest === null || requestParameters.entitySchemaUpdateRequest === undefined) {
                throw new runtime.RequiredError('entitySchemaUpdateRequest', 'Required parameter requestParameters.entitySchemaUpdateRequest was null or undefined when calling updateEntitySchema.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_schemas/{SchemaId}`.replace(`{${"SchemaId"}}`, encodeURIComponent(String(requestParameters.schemaId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.EntitySchemaUpdateRequestToJSON(requestParameters.entitySchemaUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Entity Schema
     */
    updateEntitySchema(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateEntitySchemaRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update event entry
     */
    updateEventRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.eventId === null || requestParameters.eventId === undefined) {
                throw new runtime.RequiredError('eventId', 'Required parameter requestParameters.eventId was null or undefined when calling updateEvent.');
            }
            if (requestParameters.eventUpdateRequest === null || requestParameters.eventUpdateRequest === undefined) {
                throw new runtime.RequiredError('eventUpdateRequest', 'Required parameter requestParameters.eventUpdateRequest was null or undefined when calling updateEvent.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events/{EventId}`.replace(`{${"EventId"}}`, encodeURIComponent(String(requestParameters.eventId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.EventUpdateRequestToJSON(requestParameters.eventUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EventUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update event entry
     */
    updateEvent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateEventRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Integration
     */
    updateIntegrationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.integrationId === null || requestParameters.integrationId === undefined) {
                throw new runtime.RequiredError('integrationId', 'Required parameter requestParameters.integrationId was null or undefined when calling updateIntegration.');
            }
            if (requestParameters.integrationUpdateRequest === null || requestParameters.integrationUpdateRequest === undefined) {
                throw new runtime.RequiredError('integrationUpdateRequest', 'Required parameter requestParameters.integrationUpdateRequest was null or undefined when calling updateIntegration.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations/{IntegrationId}`.replace(`{${"IntegrationId"}}`, encodeURIComponent(String(requestParameters.integrationId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.IntegrationUpdateRequestToJSON(requestParameters.integrationUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Integration
     */
    updateIntegration(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateIntegrationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Inventory
     */
    updateInventoryRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.inventoryUpdateRequest === null || requestParameters.inventoryUpdateRequest === undefined) {
                throw new runtime.RequiredError('inventoryUpdateRequest', 'Required parameter requestParameters.inventoryUpdateRequest was null or undefined when calling updateInventory.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventory`,
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.InventoryUpdateRequestToJSON(requestParameters.inventoryUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Inventory
     */
    updateInventory(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateInventoryRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Inventory Rule
     */
    updateInventoryRuleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.ruleId === null || requestParameters.ruleId === undefined) {
                throw new runtime.RequiredError('ruleId', 'Required parameter requestParameters.ruleId was null or undefined when calling updateInventoryRule.');
            }
            if (requestParameters.inventoryRuleUpdateRequest === null || requestParameters.inventoryRuleUpdateRequest === undefined) {
                throw new runtime.RequiredError('inventoryRuleUpdateRequest', 'Required parameter requestParameters.inventoryRuleUpdateRequest was null or undefined when calling updateInventoryRule.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/inventoryrules/{RuleId}`.replace(`{${"RuleId"}}`, encodeURIComponent(String(requestParameters.ruleId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.InventoryRuleUpdateRequestToJSON(requestParameters.inventoryRuleUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRuleUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.InventoryRuleUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Inventory Rule
     */
    updateInventoryRule(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateInventoryRuleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Location
     */
    updateLocationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.locationId === null || requestParameters.locationId === undefined) {
                throw new runtime.RequiredError('locationId', 'Required parameter requestParameters.locationId was null or undefined when calling updateLocation.');
            }
            if (requestParameters.locationUpdateRequest === null || requestParameters.locationUpdateRequest === undefined) {
                throw new runtime.RequiredError('locationUpdateRequest', 'Required parameter requestParameters.locationUpdateRequest was null or undefined when calling updateLocation.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/locations/{LocationId}`.replace(`{${"LocationId"}}`, encodeURIComponent(String(requestParameters.locationId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.LocationUpdateRequestToJSON(requestParameters.locationUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.LocationUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Location
     */
    updateLocation(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateLocationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Mapping
     */
    updateMappingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.mappingId === null || requestParameters.mappingId === undefined) {
                throw new runtime.RequiredError('mappingId', 'Required parameter requestParameters.mappingId was null or undefined when calling updateMapping.');
            }
            if (requestParameters.mappingUpdateRequest === null || requestParameters.mappingUpdateRequest === undefined) {
                throw new runtime.RequiredError('mappingUpdateRequest', 'Required parameter requestParameters.mappingUpdateRequest was null or undefined when calling updateMapping.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/mappings/{MappingId}`.replace(`{${"MappingId"}}`, encodeURIComponent(String(requestParameters.mappingId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.MappingUpdateRequestToJSON(requestParameters.mappingUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.MappingUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Mapping
     */
    updateMapping(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateMappingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Order
     */
    updateOrderRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orderId === null || requestParameters.orderId === undefined) {
                throw new runtime.RequiredError('orderId', 'Required parameter requestParameters.orderId was null or undefined when calling updateOrder.');
            }
            if (requestParameters.orderUpdateRequest === null || requestParameters.orderUpdateRequest === undefined) {
                throw new runtime.RequiredError('orderUpdateRequest', 'Required parameter requestParameters.orderUpdateRequest was null or undefined when calling updateOrder.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/orders/{OrderId}`.replace(`{${"OrderId"}}`, encodeURIComponent(String(requestParameters.orderId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.OrderUpdateRequestToJSON(requestParameters.orderUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Order
     */
    updateOrder(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateOrderRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Organization
     */
    updateOrganizationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.organizationId === null || requestParameters.organizationId === undefined) {
                throw new runtime.RequiredError('organizationId', 'Required parameter requestParameters.organizationId was null or undefined when calling updateOrganization.');
            }
            if (requestParameters.organizationUpdateRequest === null || requestParameters.organizationUpdateRequest === undefined) {
                throw new runtime.RequiredError('organizationUpdateRequest', 'Required parameter requestParameters.organizationUpdateRequest was null or undefined when calling updateOrganization.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/organizations/{OrganizationId}`.replace(`{${"OrganizationId"}}`, encodeURIComponent(String(requestParameters.organizationId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.OrganizationUpdateRequestToJSON(requestParameters.organizationUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrganizationUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Organization
     */
    updateOrganization(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateOrganizationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Product
     */
    updateProductRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.productId === null || requestParameters.productId === undefined) {
                throw new runtime.RequiredError('productId', 'Required parameter requestParameters.productId was null or undefined when calling updateProduct.');
            }
            if (requestParameters.productUpdateRequest === null || requestParameters.productUpdateRequest === undefined) {
                throw new runtime.RequiredError('productUpdateRequest', 'Required parameter requestParameters.productUpdateRequest was null or undefined when calling updateProduct.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/products/{ProductId}`.replace(`{${"ProductId"}}`, encodeURIComponent(String(requestParameters.productId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ProductUpdateRequestToJSON(requestParameters.productUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ProductUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Product
     */
    updateProduct(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateProductRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Purchase
     */
    updatePurchaseRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.purchaseId === null || requestParameters.purchaseId === undefined) {
                throw new runtime.RequiredError('purchaseId', 'Required parameter requestParameters.purchaseId was null or undefined when calling updatePurchase.');
            }
            if (requestParameters.purchaseUpdateRequest === null || requestParameters.purchaseUpdateRequest === undefined) {
                throw new runtime.RequiredError('purchaseUpdateRequest', 'Required parameter requestParameters.purchaseUpdateRequest was null or undefined when calling updatePurchase.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/purchases/{PurchaseId}`.replace(`{${"PurchaseId"}}`, encodeURIComponent(String(requestParameters.purchaseId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.PurchaseUpdateRequestToJSON(requestParameters.purchaseUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchaseUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.PurchaseUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Purchase
     */
    updatePurchase(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updatePurchaseRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Refund
     */
    updateRefundRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.refundId === null || requestParameters.refundId === undefined) {
                throw new runtime.RequiredError('refundId', 'Required parameter requestParameters.refundId was null or undefined when calling updateRefund.');
            }
            if (requestParameters.refundUpdateRequest === null || requestParameters.refundUpdateRequest === undefined) {
                throw new runtime.RequiredError('refundUpdateRequest', 'Required parameter requestParameters.refundUpdateRequest was null or undefined when calling updateRefund.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/refunds/{RefundId}`.replace(`{${"RefundId"}}`, encodeURIComponent(String(requestParameters.refundId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.RefundUpdateRequestToJSON(requestParameters.refundUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RefundUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Refund
     */
    updateRefund(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateRefundRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Return
     */
    updateReturnRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.returnId === null || requestParameters.returnId === undefined) {
                throw new runtime.RequiredError('returnId', 'Required parameter requestParameters.returnId was null or undefined when calling updateReturn.');
            }
            if (requestParameters.returnUpdateRequest === null || requestParameters.returnUpdateRequest === undefined) {
                throw new runtime.RequiredError('returnUpdateRequest', 'Required parameter requestParameters.returnUpdateRequest was null or undefined when calling updateReturn.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/returns/{ReturnId}`.replace(`{${"ReturnId"}}`, encodeURIComponent(String(requestParameters.returnId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ReturnUpdateRequestToJSON(requestParameters.returnUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ReturnUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Return
     */
    updateReturn(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateReturnRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Role
     */
    updateRoleRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.roleId === null || requestParameters.roleId === undefined) {
                throw new runtime.RequiredError('roleId', 'Required parameter requestParameters.roleId was null or undefined when calling updateRole.');
            }
            if (requestParameters.roleUpdateRequest === null || requestParameters.roleUpdateRequest === undefined) {
                throw new runtime.RequiredError('roleUpdateRequest', 'Required parameter requestParameters.roleUpdateRequest was null or undefined when calling updateRole.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/roles/{RoleId}`.replace(`{${"RoleId"}}`, encodeURIComponent(String(requestParameters.roleId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.RoleUpdateRequestToJSON(requestParameters.roleUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoleUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoleUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Role
     */
    updateRole(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateRoleRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Routing
     */
    updateRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingId === null || requestParameters.routingId === undefined) {
                throw new runtime.RequiredError('routingId', 'Required parameter requestParameters.routingId was null or undefined when calling updateRouting.');
            }
            if (requestParameters.routingUpdateRequest === null || requestParameters.routingUpdateRequest === undefined) {
                throw new runtime.RequiredError('routingUpdateRequest', 'Required parameter requestParameters.routingUpdateRequest was null or undefined when calling updateRouting.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/routings/{RoutingId}`.replace(`{${"RoutingId"}}`, encodeURIComponent(String(requestParameters.routingId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.RoutingUpdateRequestToJSON(requestParameters.routingUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.RoutingUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Routing
     */
    updateRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Shipment
     */
    updateShipmentRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.shipmentId === null || requestParameters.shipmentId === undefined) {
                throw new runtime.RequiredError('shipmentId', 'Required parameter requestParameters.shipmentId was null or undefined when calling updateShipment.');
            }
            if (requestParameters.shipmentUpdateRequest === null || requestParameters.shipmentUpdateRequest === undefined) {
                throw new runtime.RequiredError('shipmentUpdateRequest', 'Required parameter requestParameters.shipmentUpdateRequest was null or undefined when calling updateShipment.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/shipments/{ShipmentId}`.replace(`{${"ShipmentId"}}`, encodeURIComponent(String(requestParameters.shipmentId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ShipmentUpdateRequestToJSON(requestParameters.shipmentUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ShipmentUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Shipment
     */
    updateShipment(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateShipmentRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Supplier
     */
    updateSupplierRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.supplierId === null || requestParameters.supplierId === undefined) {
                throw new runtime.RequiredError('supplierId', 'Required parameter requestParameters.supplierId was null or undefined when calling updateSupplier.');
            }
            if (requestParameters.supplierUpdateRequest === null || requestParameters.supplierUpdateRequest === undefined) {
                throw new runtime.RequiredError('supplierUpdateRequest', 'Required parameter requestParameters.supplierUpdateRequest was null or undefined when calling updateSupplier.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/suppliers/{SupplierId}`.replace(`{${"SupplierId"}}`, encodeURIComponent(String(requestParameters.supplierId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.SupplierUpdateRequestToJSON(requestParameters.supplierUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.SupplierUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SupplierUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Supplier
     */
    updateSupplier(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateSupplierRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Tracking
     */
    updateTrackingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.trackingId === null || requestParameters.trackingId === undefined) {
                throw new runtime.RequiredError('trackingId', 'Required parameter requestParameters.trackingId was null or undefined when calling updateTracking.');
            }
            if (requestParameters.trackingUpdateRequest === null || requestParameters.trackingUpdateRequest === undefined) {
                throw new runtime.RequiredError('trackingUpdateRequest', 'Required parameter requestParameters.trackingUpdateRequest was null or undefined when calling updateTracking.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/trackings/{TrackingId}`.replace(`{${"TrackingId"}}`, encodeURIComponent(String(requestParameters.trackingId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.TrackingUpdateRequestToJSON(requestParameters.trackingUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TrackingUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Tracking
     */
    updateTracking(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateTrackingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Transfer
     */
    updateTransferRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.transferId === null || requestParameters.transferId === undefined) {
                throw new runtime.RequiredError('transferId', 'Required parameter requestParameters.transferId was null or undefined when calling updateTransfer.');
            }
            if (requestParameters.transferUpdateRequest === null || requestParameters.transferUpdateRequest === undefined) {
                throw new runtime.RequiredError('transferUpdateRequest', 'Required parameter requestParameters.transferUpdateRequest was null or undefined when calling updateTransfer.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/transfers/{TransferId}`.replace(`{${"TransferId"}}`, encodeURIComponent(String(requestParameters.transferId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.TransferUpdateRequestToJSON(requestParameters.transferUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransferUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.TransferUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Transfer
     */
    updateTransfer(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateTransferRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update User
     */
    updateUserRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.userId === null || requestParameters.userId === undefined) {
                throw new runtime.RequiredError('userId', 'Required parameter requestParameters.userId was null or undefined when calling updateUser.');
            }
            if (requestParameters.userUpdateRequest === null || requestParameters.userUpdateRequest === undefined) {
                throw new runtime.RequiredError('userUpdateRequest', 'Required parameter requestParameters.userUpdateRequest was null or undefined when calling updateUser.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users/{UserId}`.replace(`{${"UserId"}}`, encodeURIComponent(String(requestParameters.userId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.UserUpdateRequestToJSON(requestParameters.userUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.UserUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update User
     */
    updateUser(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateUserRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Webhook
     */
    updateWebhookRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.webhookId === null || requestParameters.webhookId === undefined) {
                throw new runtime.RequiredError('webhookId', 'Required parameter requestParameters.webhookId was null or undefined when calling updateWebhook.');
            }
            if (requestParameters.webhookUpdateRequest === null || requestParameters.webhookUpdateRequest === undefined) {
                throw new runtime.RequiredError('webhookUpdateRequest', 'Required parameter requestParameters.webhookUpdateRequest was null or undefined when calling updateWebhook.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/webhooks/{WebhookId}`.replace(`{${"WebhookId"}}`, encodeURIComponent(String(requestParameters.webhookId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.WebhookUpdateRequestToJSON(requestParameters.webhookUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhookUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.WebhookUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Webhook
     */
    updateWebhook(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateWebhookRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Upgrade Integration
     */
    upgradeIntegrationRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.integrationId === null || requestParameters.integrationId === undefined) {
                throw new runtime.RequiredError('integrationId', 'Required parameter requestParameters.integrationId was null or undefined when calling upgradeIntegration.');
            }
            if (requestParameters.integrationUpgradeRequest === null || requestParameters.integrationUpgradeRequest === undefined) {
                throw new runtime.RequiredError('integrationUpgradeRequest', 'Required parameter requestParameters.integrationUpgradeRequest was null or undefined when calling upgradeIntegration.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations/{IntegrationId}/upgrade`.replace(`{${"IntegrationId"}}`, encodeURIComponent(String(requestParameters.integrationId))),
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.IntegrationUpgradeRequestToJSON(requestParameters.integrationUpgradeRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Upgrade Integration
     */
    upgradeIntegration(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.upgradeIntegrationRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Upload Label Content
     */
    uploadLabelContentRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.labelId === null || requestParameters.labelId === undefined) {
                throw new runtime.RequiredError('labelId', 'Required parameter requestParameters.labelId was null or undefined when calling uploadLabelContent.');
            }
            if (requestParameters.body === null || requestParameters.body === undefined) {
                throw new runtime.RequiredError('body', 'Required parameter requestParameters.body was null or undefined when calling uploadLabelContent.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/octet-stream';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/labels/{LabelId}/upload`.replace(`{${"LabelId"}}`, encodeURIComponent(String(requestParameters.labelId))),
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: requestParameters.body,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Upload Label Content
     */
    uploadLabelContent(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.uploadLabelContentRaw(requestParameters);
            return yield response.value();
        });
    }
}
exports.P17Api = P17Api;
