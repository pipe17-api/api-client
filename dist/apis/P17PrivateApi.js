"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.P17PrivateApi = void 0;
const runtime = __importStar(require("../runtime"));
const models_1 = require("../models");
/**
 *
 */
class P17PrivateApi extends runtime.BaseAPI {
    /**
     * Create Entity Filter
     */
    createEntityFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.entityFilterCreateData === null || requestParameters.entityFilterCreateData === undefined) {
                throw new runtime.RequiredError('entityFilterCreateData', 'Required parameter requestParameters.entityFilterCreateData was null or undefined when calling createEntityFilter.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_filters`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.EntityFilterCreateDataToJSON(requestParameters.entityFilterCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Entity Filter
     */
    createEntityFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createEntityFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Entity Schema
     */
    createEntitySchemaRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.entitySchemaCreateData === null || requestParameters.entitySchemaCreateData === undefined) {
                throw new runtime.RequiredError('entitySchemaCreateData', 'Required parameter requestParameters.entitySchemaCreateData was null or undefined when calling createEntitySchema.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_schemas`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.EntitySchemaCreateDataToJSON(requestParameters.entitySchemaCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Entity Schema
     */
    createEntitySchema(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createEntitySchemaRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Exception
     */
    createExceptionRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.exceptionCreateRequest === null || requestParameters.exceptionCreateRequest === undefined) {
                throw new runtime.RequiredError('exceptionCreateRequest', 'Required parameter requestParameters.exceptionCreateRequest was null or undefined when calling createException.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exceptions`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ExceptionCreateRequestToJSON(requestParameters.exceptionCreateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Exception
     */
    createException(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createExceptionRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Exception Filter
     */
    createExceptionFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.exceptionFilterCreateData === null || requestParameters.exceptionFilterCreateData === undefined) {
                throw new runtime.RequiredError('exceptionFilterCreateData', 'Required parameter requestParameters.exceptionFilterCreateData was null or undefined when calling createExceptionFilter.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exception_filters`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ExceptionFilterCreateDataToJSON(requestParameters.exceptionFilterCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Exception Filter
     */
    createExceptionFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createExceptionFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create Order Routing
     */
    createOrderRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.orderRoutingCreateData === null || requestParameters.orderRoutingCreateData === undefined) {
                throw new runtime.RequiredError('orderRoutingCreateData', 'Required parameter requestParameters.orderRoutingCreateData was null or undefined when calling createOrderRouting.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/order_routings`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.OrderRoutingCreateDataToJSON(requestParameters.orderRoutingCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create Order Routing
     */
    createOrderRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.createOrderRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Accounts (Not Implemented)
     */
    deleteAccountsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/accounts`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Accounts (Not Implemented)
     */
    deleteAccounts() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteAccountsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Apikeys (Not Implemented)
     */
    deleteApikeysRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/apikey`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Apikeys (Not Implemented)
     */
    deleteApikeys() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteApikeysRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Connectors (Not Implemented)
     */
    deleteConnectorsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Connectors (Not Implemented)
     */
    deleteConnectors() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteConnectorsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Entity Filter
     */
    deleteEntityFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.filterId === null || requestParameters.filterId === undefined) {
                throw new runtime.RequiredError('filterId', 'Required parameter requestParameters.filterId was null or undefined when calling deleteEntityFilter.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_filters/{FilterId}`.replace(`{${"FilterId"}}`, encodeURIComponent(String(requestParameters.filterId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Entity Filter
     */
    deleteEntityFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteEntityFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Entity Filters (Not Implemented)
     */
    deleteEntityFiltersRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_filters`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Entity Filters (Not Implemented)
     */
    deleteEntityFilters() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteEntityFiltersRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Entity Schema
     */
    deleteEntitySchemaRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.schemaId === null || requestParameters.schemaId === undefined) {
                throw new runtime.RequiredError('schemaId', 'Required parameter requestParameters.schemaId was null or undefined when calling deleteEntitySchema.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_schemas/{SchemaId}`.replace(`{${"SchemaId"}}`, encodeURIComponent(String(requestParameters.schemaId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Entity Schema
     */
    deleteEntitySchema(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteEntitySchemaRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete EntitySchemas (Not Implemented)
     */
    deleteEntitySchemasRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_schemas`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete EntitySchemas (Not Implemented)
     */
    deleteEntitySchemas() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteEntitySchemasRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Events (Not Implemented)
     */
    deleteEventsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/events`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Events (Not Implemented)
     */
    deleteEvents() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteEventsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Exception
     */
    deleteExceptionRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.exceptionId === null || requestParameters.exceptionId === undefined) {
                throw new runtime.RequiredError('exceptionId', 'Required parameter requestParameters.exceptionId was null or undefined when calling deleteException.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exceptions/{ExceptionId}`.replace(`{${"ExceptionId"}}`, encodeURIComponent(String(requestParameters.exceptionId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Exception
     */
    deleteException(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteExceptionRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete Exception Filter
     */
    deleteExceptionFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.filterId === null || requestParameters.filterId === undefined) {
                throw new runtime.RequiredError('filterId', 'Required parameter requestParameters.filterId was null or undefined when calling deleteExceptionFilter.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exception_filters/{FilterId}`.replace(`{${"FilterId"}}`, encodeURIComponent(String(requestParameters.filterId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Exception Filter
     */
    deleteExceptionFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteExceptionFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete ExceptionFilters (Not Implemented)
     */
    deleteExceptionFiltersRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exception_filters`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete ExceptionFilters (Not Implemented)
     */
    deleteExceptionFilters() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteExceptionFiltersRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Exceptions (Not Implemented)
     */
    deleteExceptionsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exceptions`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Exceptions (Not Implemented)
     */
    deleteExceptions() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteExceptionsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Integrations (Not Implemented)
     */
    deleteIntegrationsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integrations`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Integrations (Not Implemented)
     */
    deleteIntegrations() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteIntegrationsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Mappings (Not Implemented)
     */
    deleteMappingsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/mappings`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Mappings (Not Implemented)
     */
    deleteMappings() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteMappingsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Order Routing
     */
    deleteOrderRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingId === null || requestParameters.routingId === undefined) {
                throw new runtime.RequiredError('routingId', 'Required parameter requestParameters.routingId was null or undefined when calling deleteOrderRouting.');
            }
            const queryParameters = {};
            if (requestParameters.action !== undefined) {
                queryParameters['action'] = requestParameters.action;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/order_routings/{RoutingId}`.replace(`{${"RoutingId"}}`, encodeURIComponent(String(requestParameters.routingId))),
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.SuccessFromJSON(jsonValue));
        });
    }
    /**
     * Delete Order Routing
     */
    deleteOrderRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteOrderRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Delete OrderRoutings (Not Implemented)
     */
    deleteOrderRoutingsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/order_routings`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete OrderRoutings (Not Implemented)
     */
    deleteOrderRoutings() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteOrderRoutingsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Organizations (Not Implemented)
     */
    deleteOrganizationsRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/organizations`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Organizations (Not Implemented)
     */
    deleteOrganizations() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteOrganizationsRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Roles (Not Implemented)
     */
    deleteRolesRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/roles`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Roles (Not Implemented)
     */
    deleteRoles() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteRolesRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Users (Not Implemented)
     */
    deleteUsersRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/users`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Users (Not Implemented)
     */
    deleteUsers() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteUsersRaw();
            return yield response.value();
        });
    }
    /**
     * Delete Webhooks (Not Implemented)
     */
    deleteWebhooksRaw() {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/webhooks`,
                method: 'DELETE',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.FailureFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.NotImplementedFromJSON(jsonValue));
        });
    }
    /**
     * Delete Webhooks (Not Implemented)
     */
    deleteWebhooks() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.deleteWebhooksRaw();
            return yield response.value();
        });
    }
    /**
     * Fetch Entity Filter
     */
    fetchEntityFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.filterId === null || requestParameters.filterId === undefined) {
                throw new runtime.RequiredError('filterId', 'Required parameter requestParameters.filterId was null or undefined when calling fetchEntityFilter.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_filters/{FilterId}`.replace(`{${"FilterId"}}`, encodeURIComponent(String(requestParameters.filterId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Entity Filter
     */
    fetchEntityFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchEntityFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Exception
     */
    fetchExceptionRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.exceptionId === null || requestParameters.exceptionId === undefined) {
                throw new runtime.RequiredError('exceptionId', 'Required parameter requestParameters.exceptionId was null or undefined when calling fetchException.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exceptions/{ExceptionId}`.replace(`{${"ExceptionId"}}`, encodeURIComponent(String(requestParameters.exceptionId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Exception
     */
    fetchException(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchExceptionRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Exception Filter
     */
    fetchExceptionFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.filterId === null || requestParameters.filterId === undefined) {
                throw new runtime.RequiredError('filterId', 'Required parameter requestParameters.filterId was null or undefined when calling fetchExceptionFilter.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exception_filters/{FilterId}`.replace(`{${"FilterId"}}`, encodeURIComponent(String(requestParameters.filterId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Exception Filter
     */
    fetchExceptionFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchExceptionFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Integration State
     */
    fetchIntegrationStateRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.name === null || requestParameters.name === undefined) {
                throw new runtime.RequiredError('name', 'Required parameter requestParameters.name was null or undefined when calling fetchIntegrationState.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integration_state/{Name}`.replace(`{${"Name"}}`, encodeURIComponent(String(requestParameters.name))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationStateFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationStateFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Integration State
     */
    fetchIntegrationState(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchIntegrationStateRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Fetch Order Routing
     */
    fetchOrderRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingId === null || requestParameters.routingId === undefined) {
                throw new runtime.RequiredError('routingId', 'Required parameter requestParameters.routingId was null or undefined when calling fetchOrderRouting.');
            }
            const queryParameters = {};
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/order_routings/{RoutingId}`.replace(`{${"RoutingId"}}`, encodeURIComponent(String(requestParameters.routingId))),
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingFetchErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingFetchResponseFromJSON(jsonValue));
        });
    }
    /**
     * Fetch Order Routing
     */
    fetchOrderRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.fetchOrderRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Entity Filters
     */
    listEntityFiltersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.filterId) {
                queryParameters['filterId'] = Array.isArray(requestParameters.filterId) ? requestParameters.filterId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.filterId;
            }
            if (requestParameters.name) {
                queryParameters['name'] = Array.isArray(requestParameters.name) ? requestParameters.name.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.name;
            }
            if (requestParameters.entity) {
                queryParameters['entity'] = Array.isArray(requestParameters.entity) ? requestParameters.entity.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.entity;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_filters`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Entity Filters
     */
    listEntityFilters(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listEntityFiltersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Entity Schemas
     */
    listEntitySchemasRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.schemaId) {
                queryParameters['schemaId'] = Array.isArray(requestParameters.schemaId) ? requestParameters.schemaId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.schemaId;
            }
            if (requestParameters.entity) {
                queryParameters['entity'] = Array.isArray(requestParameters.entity) ? requestParameters.entity.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.entity;
            }
            if (requestParameters.name) {
                queryParameters['name'] = Array.isArray(requestParameters.name) ? requestParameters.name.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.name;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_schemas`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntitySchemaListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Entity Schemas
     */
    listEntitySchemas(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listEntitySchemasRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Exception Filters
     */
    listExceptionFiltersRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.filterId) {
                queryParameters['filterId'] = Array.isArray(requestParameters.filterId) ? requestParameters.filterId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.filterId;
            }
            if (requestParameters.exceptionType) {
                queryParameters['exceptionType'] = Array.isArray(requestParameters.exceptionType) ? requestParameters.exceptionType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.exceptionType;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exception_filters`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Exception Filters
     */
    listExceptionFilters(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listExceptionFiltersRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Exceptions
     */
    listExceptionsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.status) {
                queryParameters['status'] = Array.isArray(requestParameters.status) ? requestParameters.status.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.status;
            }
            if (requestParameters.exceptionId) {
                queryParameters['exceptionId'] = Array.isArray(requestParameters.exceptionId) ? requestParameters.exceptionId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.exceptionId;
            }
            if (requestParameters.exceptionType) {
                queryParameters['exceptionType'] = Array.isArray(requestParameters.exceptionType) ? requestParameters.exceptionType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.exceptionType;
            }
            if (requestParameters.entityId) {
                queryParameters['entityId'] = Array.isArray(requestParameters.entityId) ? requestParameters.entityId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.entityId;
            }
            if (requestParameters.entityType) {
                queryParameters['entityType'] = Array.isArray(requestParameters.entityType) ? requestParameters.entityType.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.entityType;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exceptions`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionsListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionsListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Exceptions
     */
    listExceptions(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listExceptionsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Integrations State
     */
    listIntegrationStateRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integration_state`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationStateListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationStateListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Integrations State
     */
    listIntegrationState(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listIntegrationStateRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * List Order Routings
     */
    listOrderRoutingsRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const queryParameters = {};
            if (requestParameters.routingId) {
                queryParameters['routingId'] = Array.isArray(requestParameters.routingId) ? requestParameters.routingId.join(runtime.COLLECTION_FORMATS["csv"]) : requestParameters.routingId;
            }
            if (requestParameters.since !== undefined) {
                queryParameters['since'] = new Date(requestParameters.since).toISOString();
            }
            if (requestParameters.until !== undefined) {
                queryParameters['until'] = new Date(requestParameters.until).toISOString();
            }
            if (requestParameters.updatedSince !== undefined) {
                queryParameters['updatedSince'] = new Date(requestParameters.updatedSince).toISOString();
            }
            if (requestParameters.updatedUntil !== undefined) {
                queryParameters['updatedUntil'] = new Date(requestParameters.updatedUntil).toISOString();
            }
            if (requestParameters.skip !== undefined) {
                queryParameters['skip'] = requestParameters.skip;
            }
            if (requestParameters.count !== undefined) {
                queryParameters['count'] = requestParameters.count;
            }
            if (requestParameters.order !== undefined) {
                queryParameters['order'] = requestParameters.order;
            }
            if (requestParameters.keys !== undefined) {
                queryParameters['keys'] = requestParameters.keys;
            }
            const headerParameters = {};
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/order_routings`,
                method: 'GET',
                headers: headerParameters,
                query: queryParameters,
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingListErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingListResponseFromJSON(jsonValue));
        });
    }
    /**
     * List Order Routings
     */
    listOrderRoutings(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.listOrderRoutingsRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Configure private connector settings
     */
    setupConnectorRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.connectorId === null || requestParameters.connectorId === undefined) {
                throw new runtime.RequiredError('connectorId', 'Required parameter requestParameters.connectorId was null or undefined when calling setupConnector.');
            }
            if (requestParameters.connectorSetupRequest === null || requestParameters.connectorSetupRequest === undefined) {
                throw new runtime.RequiredError('connectorSetupRequest', 'Required parameter requestParameters.connectorSetupRequest was null or undefined when calling setupConnector.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/connectors/{ConnectorId}/setup`.replace(`{${"ConnectorId"}}`, encodeURIComponent(String(requestParameters.connectorId))),
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ConnectorSetupRequestToJSON(requestParameters.connectorSetupRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorSetupErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ConnectorSetupResponseFromJSON(jsonValue));
        });
    }
    /**
     * Configure private connector settings
     */
    setupConnector(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.setupConnectorRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Entity Filter
     */
    updateEntityFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.filterId === null || requestParameters.filterId === undefined) {
                throw new runtime.RequiredError('filterId', 'Required parameter requestParameters.filterId was null or undefined when calling updateEntityFilter.');
            }
            if (requestParameters.entityFilterUpdateRequest === null || requestParameters.entityFilterUpdateRequest === undefined) {
                throw new runtime.RequiredError('entityFilterUpdateRequest', 'Required parameter requestParameters.entityFilterUpdateRequest was null or undefined when calling updateEntityFilter.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/entity_filters/{FilterId}`.replace(`{${"FilterId"}}`, encodeURIComponent(String(requestParameters.filterId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.EntityFilterUpdateRequestToJSON(requestParameters.entityFilterUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.EntityFilterUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Entity Filter
     */
    updateEntityFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateEntityFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Exception
     */
    updateExceptionRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.exceptionId === null || requestParameters.exceptionId === undefined) {
                throw new runtime.RequiredError('exceptionId', 'Required parameter requestParameters.exceptionId was null or undefined when calling updateException.');
            }
            if (requestParameters.exceptionUpdateRequest === null || requestParameters.exceptionUpdateRequest === undefined) {
                throw new runtime.RequiredError('exceptionUpdateRequest', 'Required parameter requestParameters.exceptionUpdateRequest was null or undefined when calling updateException.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exceptions/{ExceptionId}`.replace(`{${"ExceptionId"}}`, encodeURIComponent(String(requestParameters.exceptionId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ExceptionUpdateRequestToJSON(requestParameters.exceptionUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Exception
     */
    updateException(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateExceptionRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Exception Filter
     */
    updateExceptionFilterRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.filterId === null || requestParameters.filterId === undefined) {
                throw new runtime.RequiredError('filterId', 'Required parameter requestParameters.filterId was null or undefined when calling updateExceptionFilter.');
            }
            if (requestParameters.exceptionFilterUpdateRequest === null || requestParameters.exceptionFilterUpdateRequest === undefined) {
                throw new runtime.RequiredError('exceptionFilterUpdateRequest', 'Required parameter requestParameters.exceptionFilterUpdateRequest was null or undefined when calling updateExceptionFilter.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/exception_filters/{FilterId}`.replace(`{${"FilterId"}}`, encodeURIComponent(String(requestParameters.filterId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.ExceptionFilterUpdateRequestToJSON(requestParameters.exceptionFilterUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.ExceptionFilterUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Exception Filter
     */
    updateExceptionFilter(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateExceptionFilterRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Update Order Routing
     */
    updateOrderRoutingRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.routingId === null || requestParameters.routingId === undefined) {
                throw new runtime.RequiredError('routingId', 'Required parameter requestParameters.routingId was null or undefined when calling updateOrderRouting.');
            }
            if (requestParameters.orderRoutingUpdateRequest === null || requestParameters.orderRoutingUpdateRequest === undefined) {
                throw new runtime.RequiredError('orderRoutingUpdateRequest', 'Required parameter requestParameters.orderRoutingUpdateRequest was null or undefined when calling updateOrderRouting.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/order_routings/{RoutingId}`.replace(`{${"RoutingId"}}`, encodeURIComponent(String(requestParameters.routingId))),
                method: 'PUT',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.OrderRoutingUpdateRequestToJSON(requestParameters.orderRoutingUpdateRequest),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingUpdateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.OrderRoutingUpdateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Update Order Routing
     */
    updateOrderRouting(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.updateOrderRoutingRaw(requestParameters);
            return yield response.value();
        });
    }
    /**
     * Create/Update/Delete Integration State
     */
    upsertIntegrationStateRaw(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            if (requestParameters.integrationStateCreateData === null || requestParameters.integrationStateCreateData === undefined) {
                throw new runtime.RequiredError('integrationStateCreateData', 'Required parameter requestParameters.integrationStateCreateData was null or undefined when calling upsertIntegrationState.');
            }
            const queryParameters = {};
            const headerParameters = {};
            headerParameters['Content-Type'] = 'application/json';
            if (this.configuration && this.configuration.apiKey) {
                headerParameters["X-Pipe17-Key"] = this.configuration.apiKey("X-Pipe17-Key"); // Pipe17KeyAuth authentication
            }
            const response = yield this.request({
                path: `/integration_state`,
                method: 'POST',
                headers: headerParameters,
                query: queryParameters,
                body: models_1.IntegrationStateCreateDataToJSON(requestParameters.integrationStateCreateData),
            });
            if (!response.ok) {
                const resp = new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationStateCreateErrorFromJSON(jsonValue));
                const failure = yield resp.value();
                if (!failure.code) {
                    failure.code = response.status;
                }
                throw new runtime.P17Failure(failure);
            }
            return new runtime.JSONApiResponse(response, (jsonValue) => models_1.IntegrationStateCreateResponseFromJSON(jsonValue));
        });
    }
    /**
     * Create/Update/Delete Integration State
     */
    upsertIntegrationState(requestParameters) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield this.upsertIntegrationStateRaw(requestParameters);
            return yield response.value();
        });
    }
}
exports.P17PrivateApi = P17PrivateApi;
