/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import Blob from 'fetch-blob';
import * as runtime from '../runtime';
import { AccountCreateRequest, AccountCreateResponse, AccountFetchResponse, AccountStatus, AccountUpdateRequest, AccountUpdateResponse, AccountsListResponse, ApiKeyCreateRequest, ApiKeyCreateResponse, ApiKeyFetchResponse, ApiKeyUpdateRequest, ApiKeyUpdateResponse, ApiKeyValueFetchResponse, ApiKeysListResponse, ArrivalCreateData, ArrivalFetchResponse, ArrivalStatus, ArrivalUpdateRequest, ArrivalUpdateResponse, ArrivalsCreateResponse, ArrivalsDeleteResponse, ArrivalsListResponse, ConnectorCreateRequest, ConnectorCreateResponse, ConnectorFetchResponse, ConnectorType, ConnectorUpdateRequest, ConnectorUpdateResponse, ConnectorsListResponse, ConverterRequest, ConverterResponse, DeleteAction, EntitySchemaFetchResponse, EntitySchemaUpdateRequest, EntitySchemaUpdateResponse, EventCreateData, EventCreateResponse, EventFetchResponse, EventReplayResponse, EventRequestSource, EventRequestStatus, EventSource, EventType, EventUpdateRequest, EventUpdateResponse, EventsListResponse, FulfillmentCreateData, FulfillmentFetchResponse, FulfillmentsCreateResponse, FulfillmentsListResponse, IntegrationCreateRequest, IntegrationCreateResponse, IntegrationFetchResponse, IntegrationStatus, IntegrationUpdateRequest, IntegrationUpdateResponse, IntegrationUpgradeRequest, IntegrationsListResponse, InventoryCreateData, InventoryCreateResponse, InventoryDeleteResponse, InventoryFetchResponse, InventoryListResponse, InventoryRuleCreateRequest, InventoryRuleCreateResponse, InventoryRuleFetchResponse, InventoryRuleUpdateRequest, InventoryRuleUpdateResponse, InventoryRulesDeleteResponse, InventoryRulesListResponse, InventoryUpdateRequest, InventoryUpdateResponse, JobCreateData, JobCreateResponse, JobFetchResponse, JobStatus, JobSubType, JobType, JobsDeleteResponse, JobsListResponse, LabelCreateData, LabelCreateResponse, LabelFetchResponse, LabelsDeleteResponse, LabelsListResponse, ListDeleteAction, LocationCreateRequest, LocationCreateResponse, LocationFetchResponse, LocationStatus, LocationUpdateRequest, LocationUpdateResponse, LocationsDeleteResponse, LocationsListResponse, MappingCreateRequest, MappingCreateResponse, MappingFetchResponse, MappingUpdateRequest, MappingUpdateResponse, MappingsListResponse, OrderCreateData, OrderFetchResponse, OrderStatus, OrderUpdateRequest, OrderUpdateResponse, OrdersCreateResponse, OrdersDeleteResponse, OrdersListResponse, OrganizationCreateRequest, OrganizationCreateResponse, OrganizationFetchResponse, OrganizationStatus, OrganizationUpdateRequest, OrganizationUpdateResponse, OrganizationsListResponse, ProducstListResponse, ProductCreateData, ProductFetchResponse, ProductStatus, ProductType, ProductUpdateRequest, ProductUpdateResponse, ProductsCreateResponse, ProductsDeleteResponse, PurchaseCreateData, PurchaseFetchResponse, PurchaseStatus, PurchaseUpdateRequest, PurchaseUpdateResponse, PurchasesCreateResponse, PurchasesDeleteResponse, PurchasesListResponse, ReceiptCreateData, ReceiptFetchResponse, ReceiptsCreateResponse, ReceiptsListResponse, RefundCreateRequest, RefundCreateResponse, RefundFetchResponse, RefundStatus, RefundType, RefundUpdateRequest, RefundUpdateResponse, RefundsDeleteResponse, RefundsListResponse, ReturnCreateRequest, ReturnCreateResponse, ReturnFetchResponse, ReturnStatus, ReturnUpdateRequest, ReturnUpdateResponse, ReturnsDeleteResponse, ReturnsListResponse, RoleCreateRequest, RoleCreateResponse, RoleFetchResponse, RoleStatus, RoleUpdateRequest, RoleUpdateResponse, RolesListResponse, RoutingCreateRequest, RoutingCreateResponse, RoutingFetchResponse, RoutingUpdateRequest, RoutingUpdateResponse, RoutingsDeleteResponse, RoutingsListResponse, ShipmentCreateData, ShipmentFetchResponse, ShipmentOrderType, ShipmentStatus, ShipmentUpdateRequest, ShipmentUpdateResponse, ShipmentsCreateResponse, ShipmentsDeleteResponse, ShipmentsListResponse, Success, SupplierCreateRequest, SupplierCreateResponse, SupplierFetchResponse, SupplierUpdateRequest, SupplierUpdateResponse, SuppliersDeleteResponse, SuppliersListResponse, TrackingCreateRequest, TrackingCreateResponse, TrackingFetchResponse, TrackingListResponse, TrackingStatus, TrackingUpdateRequest, TrackingUpdateResponse, TransferCreateData, TransferFetchResponse, TransferStatus, TransferUpdateRequest, TransferUpdateResponse, TransfersCreateResponse, TransfersDeleteResponse, TransfersListResponse, UserCreateRequest, UserCreateResponse, UserFetchResponse, UserStatus, UserUpdateRequest, UserUpdateResponse, UsersListResponse, WebhookCreateRequest, WebhookCreateResponse, WebhookFetchResponse, WebhookTopics, WebhookUpdateRequest, WebhookUpdateResponse, WebhooksListResponse } from '../models';
export interface P17ApiConverterOperationRequest {
    converterRequest?: ConverterRequest;
}
export interface P17ApiCreateAccountRequest {
    accountCreateRequest: AccountCreateRequest;
}
export interface P17ApiCreateApiKeyRequest {
    apiKeyCreateRequest: ApiKeyCreateRequest;
}
export interface P17ApiCreateArrivalsRequest {
    arrivalCreateData: Array<ArrivalCreateData>;
}
export interface P17ApiCreateConnectorRequest {
    connectorCreateRequest: ConnectorCreateRequest;
}
export interface P17ApiCreateEventRequest {
    eventCreateData: EventCreateData;
}
export interface P17ApiCreateFulfillmentsRequest {
    fulfillmentCreateData: Array<FulfillmentCreateData>;
}
export interface P17ApiCreateIntegrationRequest {
    integrationCreateRequest: IntegrationCreateRequest;
}
export interface P17ApiCreateInventoryRequest {
    inventoryCreateData: Array<InventoryCreateData>;
}
export interface P17ApiCreateInventoryRuleRequest {
    inventoryRuleCreateRequest: InventoryRuleCreateRequest;
}
export interface P17ApiCreateJobRequest {
    jobCreateData: JobCreateData;
}
export interface P17ApiCreateLabelRequest {
    labelCreateData: LabelCreateData;
}
export interface P17ApiCreateLocationRequest {
    locationCreateRequest: LocationCreateRequest;
}
export interface P17ApiCreateMappingRequest {
    mappingCreateRequest: MappingCreateRequest;
}
export interface P17ApiCreateOrdersRequest {
    orderCreateData: Array<OrderCreateData>;
}
export interface P17ApiCreateOrganizationRequest {
    organizationCreateRequest: OrganizationCreateRequest;
}
export interface P17ApiCreateProductsRequest {
    productCreateData: Array<ProductCreateData>;
}
export interface P17ApiCreatePurchasesRequest {
    purchaseCreateData: Array<PurchaseCreateData>;
}
export interface P17ApiCreateReceiptsRequest {
    receiptCreateData: Array<ReceiptCreateData>;
}
export interface P17ApiCreateRefundRequest {
    refundCreateRequest: RefundCreateRequest;
}
export interface P17ApiCreateReturnRequest {
    returnCreateRequest: ReturnCreateRequest;
}
export interface P17ApiCreateRoleRequest {
    roleCreateRequest: RoleCreateRequest;
}
export interface P17ApiCreateRoutingRequest {
    routingCreateRequest: RoutingCreateRequest;
}
export interface P17ApiCreateShipmentsRequest {
    shipmentCreateData: Array<ShipmentCreateData>;
}
export interface P17ApiCreateSupplierRequest {
    supplierCreateRequest: SupplierCreateRequest;
}
export interface P17ApiCreateTrackingRequest {
    trackingCreateRequest: TrackingCreateRequest;
}
export interface P17ApiCreateTransfersRequest {
    transferCreateData: Array<TransferCreateData>;
}
export interface P17ApiCreateUserRequest {
    userCreateRequest: UserCreateRequest;
}
export interface P17ApiCreateWebhookRequest {
    webhookCreateRequest: WebhookCreateRequest;
}
export interface P17ApiDeleteAccountRequest {
    orgKey: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteApiKeyRequest {
    apiKey: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteArrivalRequest {
    arrivalId: string;
    action?: DeleteAction;
    nested?: boolean;
}
export interface P17ApiDeleteArrivalsRequest {
    arrivalId?: Array<string>;
    transferId?: Array<string>;
    purchaseId?: Array<string>;
    fulfillmentId?: Array<string>;
    extOrderId?: Array<string>;
    integration?: Array<string>;
    status?: Array<ArrivalStatus>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteConnectorRequest {
    connectorId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteEventRequest {
    eventId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteIntegrationRequest {
    integrationId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteInventoriesRequest {
    inventoryId?: Array<string>;
    entityId?: Array<string>;
    deleted?: boolean;
    sku?: Array<string>;
    locationId?: Array<string>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    ledger?: boolean;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteInventoryRequest {
    inventoryId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteInventoryRuleRequest {
    ruleId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteInventoryRulesRequest {
    ruleId?: Array<string>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteJobRequest {
    jobId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteJobsRequest {
    jobId?: Array<string>;
    type?: Array<JobType>;
    subType?: Array<JobSubType>;
    status?: Array<JobStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteLabelRequest {
    labelId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteLabelsRequest {
    labelId?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteLocationRequest {
    locationId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteLocationsRequest {
    locationId?: Array<string>;
    status?: Array<LocationStatus>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteMappingRequest {
    mappingId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteOrderRequest {
    orderId: string;
    action?: DeleteAction;
    nested?: boolean;
}
export interface P17ApiDeleteOrdersRequest {
    orderId?: Array<string>;
    extOrderId?: Array<string>;
    orderSource?: Array<string>;
    status?: Array<OrderStatus>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteOrganizationRequest {
    organizationId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteProductRequest {
    productId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteProductsRequest {
    productId?: Array<string>;
    extProductId?: Array<string>;
    parentExtProductId?: Array<string>;
    status?: Array<ProductStatus>;
    types?: Array<ProductType>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeletePurchaseRequest {
    purchaseId: string;
    action?: DeleteAction;
    nested?: boolean;
}
export interface P17ApiDeletePurchasesRequest {
    purchaseId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<PurchaseStatus>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteRefundRequest {
    refundId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteRefundsRequest {
    refundId?: Array<string>;
    extRefundId?: Array<string>;
    extReturnId?: Array<string>;
    extOrderId?: Array<string>;
    type?: Array<RefundType>;
    status?: Array<RefundStatus>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteReturnRequest {
    returnId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteReturnsRequest {
    returnId?: Array<string>;
    extReturnId?: Array<string>;
    status?: Array<ReturnStatus>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteRoleRequest {
    roleId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteRoutingRequest {
    routingId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteRoutingsRequest {
    routingId?: Array<string>;
    uuid?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteShipmentRequest {
    shipmentId: string;
    action?: DeleteAction;
    nested?: boolean;
}
export interface P17ApiDeleteShipmentsRequest {
    shipmentId?: Array<string>;
    orderId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<ShipmentStatus>;
    orderType?: Array<ShipmentOrderType>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteSupplierRequest {
    supplierId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteSuppliersRequest {
    supplierId?: Array<string>;
    name?: string;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteTransferRequest {
    transferId: string;
    action?: DeleteAction;
    nested?: boolean;
}
export interface P17ApiDeleteTransfersRequest {
    transferId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<TransferStatus>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    action?: ListDeleteAction;
}
export interface P17ApiDeleteUserRequest {
    userId: string;
    action?: DeleteAction;
}
export interface P17ApiDeleteWebhookRequest {
    webhookId: string;
    action?: DeleteAction;
}
export interface P17ApiDownloadLabelContentRequest {
    labelId: string;
}
export interface P17ApiFetchAccountRequest {
    orgKey: string;
}
export interface P17ApiFetchApiKeyRequest {
    apiKey: string;
}
export interface P17ApiFetchApiKeyValueRequest {
    apiKey: string;
}
export interface P17ApiFetchArrivalRequest {
    arrivalId: string;
}
export interface P17ApiFetchConnectorRequest {
    connectorId: string;
}
export interface P17ApiFetchEntitySchemaRequest {
    schemaId: string;
}
export interface P17ApiFetchEventRequest {
    eventId: string;
}
export interface P17ApiFetchFulfillmentRequest {
    fulfillmentId: string;
}
export interface P17ApiFetchIntegrationRequest {
    integrationId: string;
}
export interface P17ApiFetchInventoryRequest {
    inventoryId: string;
}
export interface P17ApiFetchInventoryRuleRequest {
    ruleId: string;
}
export interface P17ApiFetchJobRequest {
    jobId: string;
}
export interface P17ApiFetchJobResultsRequest {
    jobId: string;
}
export interface P17ApiFetchLabelRequest {
    labelId: string;
}
export interface P17ApiFetchLocationRequest {
    locationId: string;
}
export interface P17ApiFetchMappingRequest {
    mappingId: string;
}
export interface P17ApiFetchOrderRequest {
    orderId: string;
}
export interface P17ApiFetchOrganizationRequest {
    organizationId: string;
}
export interface P17ApiFetchProductRequest {
    productId: string;
}
export interface P17ApiFetchPurchaseRequest {
    purchaseId: string;
}
export interface P17ApiFetchReceiptRequest {
    receiptId: string;
}
export interface P17ApiFetchRefundRequest {
    refundId: string;
}
export interface P17ApiFetchReturnRequest {
    returnId: string;
}
export interface P17ApiFetchRoleRequest {
    roleId: string;
}
export interface P17ApiFetchRoutingRequest {
    routingId: string;
}
export interface P17ApiFetchShipmentRequest {
    shipmentId: string;
}
export interface P17ApiFetchSupplierRequest {
    supplierId: string;
}
export interface P17ApiFetchTrackingRequest {
    trackingId: string;
}
export interface P17ApiFetchTransferRequest {
    transferId: string;
}
export interface P17ApiFetchUserRequest {
    userId: string;
}
export interface P17ApiFetchWebhookRequest {
    webhookId: string;
}
export interface P17ApiListAccountsRequest {
    orgKey?: Array<string>;
    status?: Array<AccountStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListApiKeysRequest {
    connector?: Array<string>;
    integration?: Array<string>;
    name?: string;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListArrivalsRequest {
    arrivalId?: Array<string>;
    transferId?: Array<string>;
    purchaseId?: Array<string>;
    fulfillmentId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<ArrivalStatus>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListConnectorsRequest {
    connectorId?: Array<string>;
    connectorName?: Array<string>;
    connectorType?: Array<ConnectorType>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListEventsRequest {
    eventId?: Array<string>;
    source?: Array<EventRequestSource>;
    status?: Array<EventRequestStatus>;
    requestId?: Array<string>;
    since?: Date;
    until?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListFulfillmentsRequest {
    fulfillmentId?: Array<string>;
    shipmentId?: Array<string>;
    extShipmentId?: Array<string>;
    extOrderId?: Array<string>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListIntegrationsRequest {
    integrationId?: Array<string>;
    connectorName?: Array<string>;
    connectorId?: Array<string>;
    status?: Array<IntegrationStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListInventoryRequest {
    inventoryId?: Array<string>;
    entityId?: Array<string>;
    sku?: Array<string>;
    locationId?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    ledger?: boolean;
    totals?: boolean;
    order?: string;
    keys?: string;
}
export interface P17ApiListInventoryRulesRequest {
    ruleId?: Array<string>;
    event?: Array<EventType>;
    ptype?: Array<EventSource>;
    partner?: Array<string>;
    integration?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListJobsRequest {
    jobId?: Array<string>;
    type?: Array<JobType>;
    subType?: Array<JobSubType>;
    status?: Array<JobStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListLabelsRequest {
    labelId?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListLocationsRequest {
    locationId?: Array<string>;
    name?: string;
    status?: Array<LocationStatus>;
    email?: string;
    phone?: string;
    extLocationId?: Array<string>;
    extIntegrationId?: Array<string>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListMappingsRequest {
    mappingId?: Array<string>;
    uuid?: Array<string>;
    desc?: string;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListOrdersRequest {
    orderId?: Array<string>;
    extOrderId?: Array<string>;
    orderSource?: Array<string>;
    email?: string;
    status?: Array<OrderStatus>;
    requireShippingLabels?: boolean;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    timestamp?: boolean;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListOrganizationsRequest {
    orgKey?: Array<string>;
    name?: string;
    phone?: string;
    email?: string;
    status?: Array<OrganizationStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListProductsRequest {
    productId?: Array<string>;
    sku?: Array<string>;
    name?: string;
    extProductId?: Array<string>;
    parentExtProductId?: Array<string>;
    bundleSKU?: string;
    inBundle?: string;
    status?: Array<ProductStatus>;
    types?: Array<ProductType>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListPurchasesRequest {
    purchaseId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<PurchaseStatus>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    timestamp?: boolean;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListReceiptsRequest {
    receiptId?: Array<string>;
    arrivalId?: Array<string>;
    extOrderId?: Array<string>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListRefundsRequest {
    refundId?: Array<string>;
    extRefundId?: Array<string>;
    extReturnId?: Array<string>;
    extOrderId?: Array<string>;
    type?: Array<RefundType>;
    status?: Array<RefundStatus>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListReturnsRequest {
    returnId?: Array<string>;
    extReturnId?: Array<string>;
    status?: Array<ReturnStatus>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListRolesRequest {
    roleId?: Array<string>;
    name?: string;
    status?: Array<RoleStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListRoutingsRequest {
    routingId?: Array<string>;
    uuid?: Array<string>;
    desc?: string;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListShipmentsRequest {
    shipmentId?: Array<string>;
    orderId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<ShipmentStatus>;
    orderType?: Array<ShipmentOrderType>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListSuppliersRequest {
    supplierId?: Array<string>;
    name?: string;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListTrackingsRequest {
    trackingId?: Array<string>;
    trackingNumber?: Array<string>;
    fulfillmentId?: Array<string>;
    shippingCarrier?: Array<string>;
    status?: Array<TrackingStatus>;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListTransfersRequest {
    transferId?: Array<string>;
    extOrderId?: Array<string>;
    status?: Array<TransferStatus>;
    deleted?: boolean;
    since?: Date;
    until?: Date;
    timestamp?: boolean;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListUsersRequest {
    userId?: Array<string>;
    name?: string;
    email?: string;
    status?: Array<UserStatus>;
    organizations?: string;
    since?: Date;
    until?: Date;
    updatedSince?: Date;
    updatedUntil?: Date;
    skip?: number;
    count?: number;
    order?: string;
    keys?: string;
}
export interface P17ApiListWebhooksRequest {
    webhookId?: Array<string>;
    topic?: WebhookTopics;
    related?: boolean;
    order?: string;
    keys?: string;
}
export interface P17ApiReinviteUserRequest {
    userId: string;
    body: object;
}
export interface P17ApiReplayEventRequest {
    eventId: string;
}
export interface P17ApiResetPasswordRequest {
    userId: string;
    body: object;
}
export interface P17ApiUpdateAccountRequest {
    activationKey: string;
    accountUpdateRequest: AccountUpdateRequest;
}
export interface P17ApiUpdateApiKeyRequest {
    apiKey: string;
    apiKeyUpdateRequest: ApiKeyUpdateRequest;
}
export interface P17ApiUpdateArrivalRequest {
    arrivalId: string;
    arrivalUpdateRequest: ArrivalUpdateRequest;
}
export interface P17ApiUpdateConnectorRequest {
    connectorId: string;
    connectorUpdateRequest: ConnectorUpdateRequest;
}
export interface P17ApiUpdateEntitySchemaRequest {
    schemaId: string;
    entitySchemaUpdateRequest: EntitySchemaUpdateRequest;
}
export interface P17ApiUpdateEventRequest {
    eventId: string;
    eventUpdateRequest: EventUpdateRequest;
}
export interface P17ApiUpdateIntegrationRequest {
    integrationId: string;
    integrationUpdateRequest: IntegrationUpdateRequest;
}
export interface P17ApiUpdateInventoryRequest {
    inventoryUpdateRequest: InventoryUpdateRequest;
}
export interface P17ApiUpdateInventoryRuleRequest {
    ruleId: string;
    inventoryRuleUpdateRequest: InventoryRuleUpdateRequest;
}
export interface P17ApiUpdateLocationRequest {
    locationId: string;
    locationUpdateRequest: LocationUpdateRequest;
}
export interface P17ApiUpdateMappingRequest {
    mappingId: string;
    mappingUpdateRequest: MappingUpdateRequest;
}
export interface P17ApiUpdateOrderRequest {
    orderId: string;
    orderUpdateRequest: OrderUpdateRequest;
}
export interface P17ApiUpdateOrganizationRequest {
    organizationId: string;
    organizationUpdateRequest: OrganizationUpdateRequest;
}
export interface P17ApiUpdateProductRequest {
    productId: string;
    productUpdateRequest: ProductUpdateRequest;
}
export interface P17ApiUpdatePurchaseRequest {
    purchaseId: string;
    purchaseUpdateRequest: PurchaseUpdateRequest;
}
export interface P17ApiUpdateRefundRequest {
    refundId: string;
    refundUpdateRequest: RefundUpdateRequest;
}
export interface P17ApiUpdateReturnRequest {
    returnId: string;
    returnUpdateRequest: ReturnUpdateRequest;
}
export interface P17ApiUpdateRoleRequest {
    roleId: string;
    roleUpdateRequest: RoleUpdateRequest;
}
export interface P17ApiUpdateRoutingRequest {
    routingId: string;
    routingUpdateRequest: RoutingUpdateRequest;
}
export interface P17ApiUpdateShipmentRequest {
    shipmentId: string;
    shipmentUpdateRequest: ShipmentUpdateRequest;
}
export interface P17ApiUpdateSupplierRequest {
    supplierId: string;
    supplierUpdateRequest: SupplierUpdateRequest;
}
export interface P17ApiUpdateTrackingRequest {
    trackingId: string;
    trackingUpdateRequest: TrackingUpdateRequest;
}
export interface P17ApiUpdateTransferRequest {
    transferId: string;
    transferUpdateRequest: TransferUpdateRequest;
}
export interface P17ApiUpdateUserRequest {
    userId: string;
    userUpdateRequest: UserUpdateRequest;
}
export interface P17ApiUpdateWebhookRequest {
    webhookId: string;
    webhookUpdateRequest: WebhookUpdateRequest;
}
export interface P17ApiUpgradeIntegrationRequest {
    integrationId: string;
    integrationUpgradeRequest: IntegrationUpgradeRequest;
}
export interface P17ApiUploadLabelContentRequest {
    labelId: string;
    body: Blob;
}
/**
 *
 */
export declare class P17Api extends runtime.BaseAPI {
    /**
     * Convert a source object to target using specified mapping rules
     */
    converterRaw(requestParameters: P17ApiConverterOperationRequest): Promise<runtime.ApiResponse<ConverterResponse>>;
    /**
     * Convert a source object to target using specified mapping rules
     */
    converter(requestParameters: P17ApiConverterOperationRequest): Promise<ConverterResponse>;
    /**
     * Create Account
     */
    createAccountRaw(requestParameters: P17ApiCreateAccountRequest): Promise<runtime.ApiResponse<AccountCreateResponse>>;
    /**
     * Create Account
     */
    createAccount(requestParameters: P17ApiCreateAccountRequest): Promise<AccountCreateResponse>;
    /**
     * Create API Key
     */
    createApiKeyRaw(requestParameters: P17ApiCreateApiKeyRequest): Promise<runtime.ApiResponse<ApiKeyCreateResponse>>;
    /**
     * Create API Key
     */
    createApiKey(requestParameters: P17ApiCreateApiKeyRequest): Promise<ApiKeyCreateResponse>;
    /**
     * Create Arrival Notifications
     */
    createArrivalsRaw(requestParameters: P17ApiCreateArrivalsRequest): Promise<runtime.ApiResponse<ArrivalsCreateResponse>>;
    /**
     * Create Arrival Notifications
     */
    createArrivals(requestParameters: P17ApiCreateArrivalsRequest): Promise<ArrivalsCreateResponse>;
    /**
     * Create Connector
     */
    createConnectorRaw(requestParameters: P17ApiCreateConnectorRequest): Promise<runtime.ApiResponse<ConnectorCreateResponse>>;
    /**
     * Create Connector
     */
    createConnector(requestParameters: P17ApiCreateConnectorRequest): Promise<ConnectorCreateResponse>;
    /**
     * Create event
     */
    createEventRaw(requestParameters: P17ApiCreateEventRequest): Promise<runtime.ApiResponse<EventCreateResponse>>;
    /**
     * Create event
     */
    createEvent(requestParameters: P17ApiCreateEventRequest): Promise<EventCreateResponse>;
    /**
     * Create Fulfillments
     */
    createFulfillmentsRaw(requestParameters: P17ApiCreateFulfillmentsRequest): Promise<runtime.ApiResponse<FulfillmentsCreateResponse>>;
    /**
     * Create Fulfillments
     */
    createFulfillments(requestParameters: P17ApiCreateFulfillmentsRequest): Promise<FulfillmentsCreateResponse>;
    /**
     * Create Integration
     */
    createIntegrationRaw(requestParameters: P17ApiCreateIntegrationRequest): Promise<runtime.ApiResponse<IntegrationCreateResponse>>;
    /**
     * Create Integration
     */
    createIntegration(requestParameters: P17ApiCreateIntegrationRequest): Promise<IntegrationCreateResponse>;
    /**
     * Create Inventory Log Items
     */
    createInventoryRaw(requestParameters: P17ApiCreateInventoryRequest): Promise<runtime.ApiResponse<InventoryCreateResponse>>;
    /**
     * Create Inventory Log Items
     */
    createInventory(requestParameters: P17ApiCreateInventoryRequest): Promise<InventoryCreateResponse>;
    /**
     * Create Inventory Rule
     */
    createInventoryRuleRaw(requestParameters: P17ApiCreateInventoryRuleRequest): Promise<runtime.ApiResponse<InventoryRuleCreateResponse>>;
    /**
     * Create Inventory Rule
     */
    createInventoryRule(requestParameters: P17ApiCreateInventoryRuleRequest): Promise<InventoryRuleCreateResponse>;
    /**
     * Create long-running job
     */
    createJobRaw(requestParameters: P17ApiCreateJobRequest): Promise<runtime.ApiResponse<JobCreateResponse>>;
    /**
     * Create long-running job
     */
    createJob(requestParameters: P17ApiCreateJobRequest): Promise<JobCreateResponse>;
    /**
     * Create Label
     */
    createLabelRaw(requestParameters: P17ApiCreateLabelRequest): Promise<runtime.ApiResponse<LabelCreateResponse>>;
    /**
     * Create Label
     */
    createLabel(requestParameters: P17ApiCreateLabelRequest): Promise<LabelCreateResponse>;
    /**
     * Create Location
     */
    createLocationRaw(requestParameters: P17ApiCreateLocationRequest): Promise<runtime.ApiResponse<LocationCreateResponse>>;
    /**
     * Create Location
     */
    createLocation(requestParameters: P17ApiCreateLocationRequest): Promise<LocationCreateResponse>;
    /**
     * Create Mapping
     */
    createMappingRaw(requestParameters: P17ApiCreateMappingRequest): Promise<runtime.ApiResponse<MappingCreateResponse>>;
    /**
     * Create Mapping
     */
    createMapping(requestParameters: P17ApiCreateMappingRequest): Promise<MappingCreateResponse>;
    /**
     * Create Orders
     */
    createOrdersRaw(requestParameters: P17ApiCreateOrdersRequest): Promise<runtime.ApiResponse<OrdersCreateResponse>>;
    /**
     * Create Orders
     */
    createOrders(requestParameters: P17ApiCreateOrdersRequest): Promise<OrdersCreateResponse>;
    /**
     * Create Organization
     */
    createOrganizationRaw(requestParameters: P17ApiCreateOrganizationRequest): Promise<runtime.ApiResponse<OrganizationCreateResponse>>;
    /**
     * Create Organization
     */
    createOrganization(requestParameters: P17ApiCreateOrganizationRequest): Promise<OrganizationCreateResponse>;
    /**
     * Create Products
     */
    createProductsRaw(requestParameters: P17ApiCreateProductsRequest): Promise<runtime.ApiResponse<ProductsCreateResponse>>;
    /**
     * Create Products
     */
    createProducts(requestParameters: P17ApiCreateProductsRequest): Promise<ProductsCreateResponse>;
    /**
     * Create Purchases
     */
    createPurchasesRaw(requestParameters: P17ApiCreatePurchasesRequest): Promise<runtime.ApiResponse<PurchasesCreateResponse>>;
    /**
     * Create Purchases
     */
    createPurchases(requestParameters: P17ApiCreatePurchasesRequest): Promise<PurchasesCreateResponse>;
    /**
     * Create Arrival Receipts
     */
    createReceiptsRaw(requestParameters: P17ApiCreateReceiptsRequest): Promise<runtime.ApiResponse<ReceiptsCreateResponse>>;
    /**
     * Create Arrival Receipts
     */
    createReceipts(requestParameters: P17ApiCreateReceiptsRequest): Promise<ReceiptsCreateResponse>;
    /**
     * Create Refund
     */
    createRefundRaw(requestParameters: P17ApiCreateRefundRequest): Promise<runtime.ApiResponse<RefundCreateResponse>>;
    /**
     * Create Refund
     */
    createRefund(requestParameters: P17ApiCreateRefundRequest): Promise<RefundCreateResponse>;
    /**
     * Create Return
     */
    createReturnRaw(requestParameters: P17ApiCreateReturnRequest): Promise<runtime.ApiResponse<ReturnCreateResponse>>;
    /**
     * Create Return
     */
    createReturn(requestParameters: P17ApiCreateReturnRequest): Promise<ReturnCreateResponse>;
    /**
     * Create Role
     */
    createRoleRaw(requestParameters: P17ApiCreateRoleRequest): Promise<runtime.ApiResponse<RoleCreateResponse>>;
    /**
     * Create Role
     */
    createRole(requestParameters: P17ApiCreateRoleRequest): Promise<RoleCreateResponse>;
    /**
     * Create Routing
     */
    createRoutingRaw(requestParameters: P17ApiCreateRoutingRequest): Promise<runtime.ApiResponse<RoutingCreateResponse>>;
    /**
     * Create Routing
     */
    createRouting(requestParameters: P17ApiCreateRoutingRequest): Promise<RoutingCreateResponse>;
    /**
     * Create Shipments
     */
    createShipmentsRaw(requestParameters: P17ApiCreateShipmentsRequest): Promise<runtime.ApiResponse<ShipmentsCreateResponse>>;
    /**
     * Create Shipments
     */
    createShipments(requestParameters: P17ApiCreateShipmentsRequest): Promise<ShipmentsCreateResponse>;
    /**
     * Create Supplier
     */
    createSupplierRaw(requestParameters: P17ApiCreateSupplierRequest): Promise<runtime.ApiResponse<SupplierCreateResponse>>;
    /**
     * Create Supplier
     */
    createSupplier(requestParameters: P17ApiCreateSupplierRequest): Promise<SupplierCreateResponse>;
    /**
     * Create Tracking
     */
    createTrackingRaw(requestParameters: P17ApiCreateTrackingRequest): Promise<runtime.ApiResponse<TrackingCreateResponse>>;
    /**
     * Create Tracking
     */
    createTracking(requestParameters: P17ApiCreateTrackingRequest): Promise<TrackingCreateResponse>;
    /**
     * Create Transfers
     */
    createTransfersRaw(requestParameters: P17ApiCreateTransfersRequest): Promise<runtime.ApiResponse<TransfersCreateResponse>>;
    /**
     * Create Transfers
     */
    createTransfers(requestParameters: P17ApiCreateTransfersRequest): Promise<TransfersCreateResponse>;
    /**
     * Create User
     */
    createUserRaw(requestParameters: P17ApiCreateUserRequest): Promise<runtime.ApiResponse<UserCreateResponse>>;
    /**
     * Create User
     */
    createUser(requestParameters: P17ApiCreateUserRequest): Promise<UserCreateResponse>;
    /**
     * Create Webhook
     */
    createWebhookRaw(requestParameters: P17ApiCreateWebhookRequest): Promise<runtime.ApiResponse<WebhookCreateResponse>>;
    /**
     * Create Webhook
     */
    createWebhook(requestParameters: P17ApiCreateWebhookRequest): Promise<WebhookCreateResponse>;
    /**
     * Delete Account
     */
    deleteAccountRaw(requestParameters: P17ApiDeleteAccountRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Account
     */
    deleteAccount(requestParameters: P17ApiDeleteAccountRequest): Promise<Success>;
    /**
     * Delete API Key
     */
    deleteApiKeyRaw(requestParameters: P17ApiDeleteApiKeyRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete API Key
     */
    deleteApiKey(requestParameters: P17ApiDeleteApiKeyRequest): Promise<Success>;
    /**
     * Delete Arrival
     */
    deleteArrivalRaw(requestParameters: P17ApiDeleteArrivalRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Arrival
     */
    deleteArrival(requestParameters: P17ApiDeleteArrivalRequest): Promise<Success>;
    /**
     * Delete Multiple Arrivals
     */
    deleteArrivalsRaw(requestParameters: P17ApiDeleteArrivalsRequest): Promise<runtime.ApiResponse<ArrivalsDeleteResponse>>;
    /**
     * Delete Multiple Arrivals
     */
    deleteArrivals(requestParameters: P17ApiDeleteArrivalsRequest): Promise<ArrivalsDeleteResponse>;
    /**
     * Delete Connector
     */
    deleteConnectorRaw(requestParameters: P17ApiDeleteConnectorRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Connector
     */
    deleteConnector(requestParameters: P17ApiDeleteConnectorRequest): Promise<Success>;
    /**
     * Delete Event
     */
    deleteEventRaw(requestParameters: P17ApiDeleteEventRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Event
     */
    deleteEvent(requestParameters: P17ApiDeleteEventRequest): Promise<Success>;
    /**
     * Delete Integration
     */
    deleteIntegrationRaw(requestParameters: P17ApiDeleteIntegrationRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Integration
     */
    deleteIntegration(requestParameters: P17ApiDeleteIntegrationRequest): Promise<Success>;
    /**
     * Delete Multiple Inventory
     */
    deleteInventoriesRaw(requestParameters: P17ApiDeleteInventoriesRequest): Promise<runtime.ApiResponse<InventoryDeleteResponse>>;
    /**
     * Delete Multiple Inventory
     */
    deleteInventories(requestParameters: P17ApiDeleteInventoriesRequest): Promise<InventoryDeleteResponse>;
    /**
     * Delete Inventory
     */
    deleteInventoryRaw(requestParameters: P17ApiDeleteInventoryRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Inventory
     */
    deleteInventory(requestParameters: P17ApiDeleteInventoryRequest): Promise<Success>;
    /**
     * Delete Inventory Rule
     */
    deleteInventoryRuleRaw(requestParameters: P17ApiDeleteInventoryRuleRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Inventory Rule
     */
    deleteInventoryRule(requestParameters: P17ApiDeleteInventoryRuleRequest): Promise<Success>;
    /**
     * Delete Multiple InventoryRules
     */
    deleteInventoryRulesRaw(requestParameters: P17ApiDeleteInventoryRulesRequest): Promise<runtime.ApiResponse<InventoryRulesDeleteResponse>>;
    /**
     * Delete Multiple InventoryRules
     */
    deleteInventoryRules(requestParameters: P17ApiDeleteInventoryRulesRequest): Promise<InventoryRulesDeleteResponse>;
    /**
     * Delete (cancel) long-running job
     */
    deleteJobRaw(requestParameters: P17ApiDeleteJobRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete (cancel) long-running job
     */
    deleteJob(requestParameters: P17ApiDeleteJobRequest): Promise<Success>;
    /**
     * Delete Multiple Jobs
     */
    deleteJobsRaw(requestParameters: P17ApiDeleteJobsRequest): Promise<runtime.ApiResponse<JobsDeleteResponse>>;
    /**
     * Delete Multiple Jobs
     */
    deleteJobs(requestParameters: P17ApiDeleteJobsRequest): Promise<JobsDeleteResponse>;
    /**
     * Delete Label
     */
    deleteLabelRaw(requestParameters: P17ApiDeleteLabelRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Label
     */
    deleteLabel(requestParameters: P17ApiDeleteLabelRequest): Promise<Success>;
    /**
     * Delete Labels
     */
    deleteLabelsRaw(requestParameters: P17ApiDeleteLabelsRequest): Promise<runtime.ApiResponse<LabelsDeleteResponse>>;
    /**
     * Delete Labels
     */
    deleteLabels(requestParameters: P17ApiDeleteLabelsRequest): Promise<LabelsDeleteResponse>;
    /**
     * Delete Location
     */
    deleteLocationRaw(requestParameters: P17ApiDeleteLocationRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Location
     */
    deleteLocation(requestParameters: P17ApiDeleteLocationRequest): Promise<Success>;
    /**
     * Delete Multiple Locations
     */
    deleteLocationsRaw(requestParameters: P17ApiDeleteLocationsRequest): Promise<runtime.ApiResponse<LocationsDeleteResponse>>;
    /**
     * Delete Multiple Locations
     */
    deleteLocations(requestParameters: P17ApiDeleteLocationsRequest): Promise<LocationsDeleteResponse>;
    /**
     * Delete Mapping
     */
    deleteMappingRaw(requestParameters: P17ApiDeleteMappingRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Mapping
     */
    deleteMapping(requestParameters: P17ApiDeleteMappingRequest): Promise<Success>;
    /**
     * Delete Order
     */
    deleteOrderRaw(requestParameters: P17ApiDeleteOrderRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Order
     */
    deleteOrder(requestParameters: P17ApiDeleteOrderRequest): Promise<Success>;
    /**
     * Delete Multiple Orders
     */
    deleteOrdersRaw(requestParameters: P17ApiDeleteOrdersRequest): Promise<runtime.ApiResponse<OrdersDeleteResponse>>;
    /**
     * Delete Multiple Orders
     */
    deleteOrders(requestParameters: P17ApiDeleteOrdersRequest): Promise<OrdersDeleteResponse>;
    /**
     * Delete Organization
     */
    deleteOrganizationRaw(requestParameters: P17ApiDeleteOrganizationRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Organization
     */
    deleteOrganization(requestParameters: P17ApiDeleteOrganizationRequest): Promise<Success>;
    /**
     * Delete Product
     */
    deleteProductRaw(requestParameters: P17ApiDeleteProductRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Product
     */
    deleteProduct(requestParameters: P17ApiDeleteProductRequest): Promise<Success>;
    /**
     * Delete Multiple Products
     */
    deleteProductsRaw(requestParameters: P17ApiDeleteProductsRequest): Promise<runtime.ApiResponse<ProductsDeleteResponse>>;
    /**
     * Delete Multiple Products
     */
    deleteProducts(requestParameters: P17ApiDeleteProductsRequest): Promise<ProductsDeleteResponse>;
    /**
     * Delete Purchase
     */
    deletePurchaseRaw(requestParameters: P17ApiDeletePurchaseRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Purchase
     */
    deletePurchase(requestParameters: P17ApiDeletePurchaseRequest): Promise<Success>;
    /**
     * Delete Multiple Purchases
     */
    deletePurchasesRaw(requestParameters: P17ApiDeletePurchasesRequest): Promise<runtime.ApiResponse<PurchasesDeleteResponse>>;
    /**
     * Delete Multiple Purchases
     */
    deletePurchases(requestParameters: P17ApiDeletePurchasesRequest): Promise<PurchasesDeleteResponse>;
    /**
     * Delete Refund
     */
    deleteRefundRaw(requestParameters: P17ApiDeleteRefundRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Refund
     */
    deleteRefund(requestParameters: P17ApiDeleteRefundRequest): Promise<Success>;
    /**
     * Delete Multiple Refunds
     */
    deleteRefundsRaw(requestParameters: P17ApiDeleteRefundsRequest): Promise<runtime.ApiResponse<RefundsDeleteResponse>>;
    /**
     * Delete Multiple Refunds
     */
    deleteRefunds(requestParameters: P17ApiDeleteRefundsRequest): Promise<RefundsDeleteResponse>;
    /**
     * Delete Return
     */
    deleteReturnRaw(requestParameters: P17ApiDeleteReturnRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Return
     */
    deleteReturn(requestParameters: P17ApiDeleteReturnRequest): Promise<Success>;
    /**
     * Delete Multiple Returns
     */
    deleteReturnsRaw(requestParameters: P17ApiDeleteReturnsRequest): Promise<runtime.ApiResponse<ReturnsDeleteResponse>>;
    /**
     * Delete Multiple Returns
     */
    deleteReturns(requestParameters: P17ApiDeleteReturnsRequest): Promise<ReturnsDeleteResponse>;
    /**
     * Delete Role
     */
    deleteRoleRaw(requestParameters: P17ApiDeleteRoleRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Role
     */
    deleteRole(requestParameters: P17ApiDeleteRoleRequest): Promise<Success>;
    /**
     * Delete Routing
     */
    deleteRoutingRaw(requestParameters: P17ApiDeleteRoutingRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Routing
     */
    deleteRouting(requestParameters: P17ApiDeleteRoutingRequest): Promise<Success>;
    /**
     * Delete Routings
     */
    deleteRoutingsRaw(requestParameters: P17ApiDeleteRoutingsRequest): Promise<runtime.ApiResponse<RoutingsDeleteResponse>>;
    /**
     * Delete Routings
     */
    deleteRoutings(requestParameters: P17ApiDeleteRoutingsRequest): Promise<RoutingsDeleteResponse>;
    /**
     * Delete Shipment
     */
    deleteShipmentRaw(requestParameters: P17ApiDeleteShipmentRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Shipment
     */
    deleteShipment(requestParameters: P17ApiDeleteShipmentRequest): Promise<Success>;
    /**
     * Delete Multiple Shipments
     */
    deleteShipmentsRaw(requestParameters: P17ApiDeleteShipmentsRequest): Promise<runtime.ApiResponse<ShipmentsDeleteResponse>>;
    /**
     * Delete Multiple Shipments
     */
    deleteShipments(requestParameters: P17ApiDeleteShipmentsRequest): Promise<ShipmentsDeleteResponse>;
    /**
     * Delete Supplier
     */
    deleteSupplierRaw(requestParameters: P17ApiDeleteSupplierRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Supplier
     */
    deleteSupplier(requestParameters: P17ApiDeleteSupplierRequest): Promise<Success>;
    /**
     * Delete Multiple Suppliers
     */
    deleteSuppliersRaw(requestParameters: P17ApiDeleteSuppliersRequest): Promise<runtime.ApiResponse<SuppliersDeleteResponse>>;
    /**
     * Delete Multiple Suppliers
     */
    deleteSuppliers(requestParameters: P17ApiDeleteSuppliersRequest): Promise<SuppliersDeleteResponse>;
    /**
     * Delete Transfer
     */
    deleteTransferRaw(requestParameters: P17ApiDeleteTransferRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Transfer
     */
    deleteTransfer(requestParameters: P17ApiDeleteTransferRequest): Promise<Success>;
    /**
     * Delete Multiple Transfers
     */
    deleteTransfersRaw(requestParameters: P17ApiDeleteTransfersRequest): Promise<runtime.ApiResponse<TransfersDeleteResponse>>;
    /**
     * Delete Multiple Transfers
     */
    deleteTransfers(requestParameters: P17ApiDeleteTransfersRequest): Promise<TransfersDeleteResponse>;
    /**
     * Delete User
     */
    deleteUserRaw(requestParameters: P17ApiDeleteUserRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete User
     */
    deleteUser(requestParameters: P17ApiDeleteUserRequest): Promise<Success>;
    /**
     * Delete Webhook
     */
    deleteWebhookRaw(requestParameters: P17ApiDeleteWebhookRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Delete Webhook
     */
    deleteWebhook(requestParameters: P17ApiDeleteWebhookRequest): Promise<Success>;
    /**
     * Download Label Content
     */
    downloadLabelContentRaw(requestParameters: P17ApiDownloadLabelContentRequest): Promise<runtime.ApiResponse<Blob>>;
    /**
     * Download Label Content
     */
    downloadLabelContent(requestParameters: P17ApiDownloadLabelContentRequest): Promise<Blob>;
    /**
     * Fetch Account
     */
    fetchAccountRaw(requestParameters: P17ApiFetchAccountRequest): Promise<runtime.ApiResponse<AccountFetchResponse>>;
    /**
     * Fetch Account
     */
    fetchAccount(requestParameters: P17ApiFetchAccountRequest): Promise<AccountFetchResponse>;
    /**
     * Fetch API Key Info
     */
    fetchApiKeyRaw(requestParameters: P17ApiFetchApiKeyRequest): Promise<runtime.ApiResponse<ApiKeyFetchResponse>>;
    /**
     * Fetch API Key Info
     */
    fetchApiKey(requestParameters: P17ApiFetchApiKeyRequest): Promise<ApiKeyFetchResponse>;
    /**
     * Fetch API Key Info, including the key value
     */
    fetchApiKeyValueRaw(requestParameters: P17ApiFetchApiKeyValueRequest): Promise<runtime.ApiResponse<ApiKeyValueFetchResponse>>;
    /**
     * Fetch API Key Info, including the key value
     */
    fetchApiKeyValue(requestParameters: P17ApiFetchApiKeyValueRequest): Promise<ApiKeyValueFetchResponse>;
    /**
     * Fetch Arrival Notification
     */
    fetchArrivalRaw(requestParameters: P17ApiFetchArrivalRequest): Promise<runtime.ApiResponse<ArrivalFetchResponse>>;
    /**
     * Fetch Arrival Notification
     */
    fetchArrival(requestParameters: P17ApiFetchArrivalRequest): Promise<ArrivalFetchResponse>;
    /**
     * Fetch Connector
     */
    fetchConnectorRaw(requestParameters: P17ApiFetchConnectorRequest): Promise<runtime.ApiResponse<ConnectorFetchResponse>>;
    /**
     * Fetch Connector
     */
    fetchConnector(requestParameters: P17ApiFetchConnectorRequest): Promise<ConnectorFetchResponse>;
    /**
     * Fetch Entity Schema
     */
    fetchEntitySchemaRaw(requestParameters: P17ApiFetchEntitySchemaRequest): Promise<runtime.ApiResponse<EntitySchemaFetchResponse>>;
    /**
     * Fetch Entity Schema
     */
    fetchEntitySchema(requestParameters: P17ApiFetchEntitySchemaRequest): Promise<EntitySchemaFetchResponse>;
    /**
     * Fetch Event
     */
    fetchEventRaw(requestParameters: P17ApiFetchEventRequest): Promise<runtime.ApiResponse<EventFetchResponse>>;
    /**
     * Fetch Event
     */
    fetchEvent(requestParameters: P17ApiFetchEventRequest): Promise<EventFetchResponse>;
    /**
     * Fetch Fulfillment
     */
    fetchFulfillmentRaw(requestParameters: P17ApiFetchFulfillmentRequest): Promise<runtime.ApiResponse<FulfillmentFetchResponse>>;
    /**
     * Fetch Fulfillment
     */
    fetchFulfillment(requestParameters: P17ApiFetchFulfillmentRequest): Promise<FulfillmentFetchResponse>;
    /**
     * Fetch Integration
     */
    fetchIntegrationRaw(requestParameters: P17ApiFetchIntegrationRequest): Promise<runtime.ApiResponse<IntegrationFetchResponse>>;
    /**
     * Fetch Integration
     */
    fetchIntegration(requestParameters: P17ApiFetchIntegrationRequest): Promise<IntegrationFetchResponse>;
    /**
     * Fetch Inventory Log Item
     */
    fetchInventoryRaw(requestParameters: P17ApiFetchInventoryRequest): Promise<runtime.ApiResponse<InventoryFetchResponse>>;
    /**
     * Fetch Inventory Log Item
     */
    fetchInventory(requestParameters: P17ApiFetchInventoryRequest): Promise<InventoryFetchResponse>;
    /**
     * Fetch Inventory Rule
     */
    fetchInventoryRuleRaw(requestParameters: P17ApiFetchInventoryRuleRequest): Promise<runtime.ApiResponse<InventoryRuleFetchResponse>>;
    /**
     * Fetch Inventory Rule
     */
    fetchInventoryRule(requestParameters: P17ApiFetchInventoryRuleRequest): Promise<InventoryRuleFetchResponse>;
    /**
     * Fetch long-running job
     */
    fetchJobRaw(requestParameters: P17ApiFetchJobRequest): Promise<runtime.ApiResponse<JobFetchResponse>>;
    /**
     * Fetch long-running job
     */
    fetchJob(requestParameters: P17ApiFetchJobRequest): Promise<JobFetchResponse>;
    /**
     * Fetch long-running job results
     */
    fetchJobResultsRaw(requestParameters: P17ApiFetchJobResultsRequest): Promise<runtime.ApiResponse<string>>;
    /**
     * Fetch long-running job results
     */
    fetchJobResults(requestParameters: P17ApiFetchJobResultsRequest): Promise<string>;
    /**
     * Fetch Label
     */
    fetchLabelRaw(requestParameters: P17ApiFetchLabelRequest): Promise<runtime.ApiResponse<LabelFetchResponse>>;
    /**
     * Fetch Label
     */
    fetchLabel(requestParameters: P17ApiFetchLabelRequest): Promise<LabelFetchResponse>;
    /**
     * Fetch Location
     */
    fetchLocationRaw(requestParameters: P17ApiFetchLocationRequest): Promise<runtime.ApiResponse<LocationFetchResponse>>;
    /**
     * Fetch Location
     */
    fetchLocation(requestParameters: P17ApiFetchLocationRequest): Promise<LocationFetchResponse>;
    /**
     * Fetch Mapping
     */
    fetchMappingRaw(requestParameters: P17ApiFetchMappingRequest): Promise<runtime.ApiResponse<MappingFetchResponse>>;
    /**
     * Fetch Mapping
     */
    fetchMapping(requestParameters: P17ApiFetchMappingRequest): Promise<MappingFetchResponse>;
    /**
     * Fetch Order
     */
    fetchOrderRaw(requestParameters: P17ApiFetchOrderRequest): Promise<runtime.ApiResponse<OrderFetchResponse>>;
    /**
     * Fetch Order
     */
    fetchOrder(requestParameters: P17ApiFetchOrderRequest): Promise<OrderFetchResponse>;
    /**
     * Fetch Organization
     */
    fetchOrganizationRaw(requestParameters: P17ApiFetchOrganizationRequest): Promise<runtime.ApiResponse<OrganizationFetchResponse>>;
    /**
     * Fetch Organization
     */
    fetchOrganization(requestParameters: P17ApiFetchOrganizationRequest): Promise<OrganizationFetchResponse>;
    /**
     * Retrieve Product
     */
    fetchProductRaw(requestParameters: P17ApiFetchProductRequest): Promise<runtime.ApiResponse<ProductFetchResponse>>;
    /**
     * Retrieve Product
     */
    fetchProduct(requestParameters: P17ApiFetchProductRequest): Promise<ProductFetchResponse>;
    /**
     * Fetch Purchase
     */
    fetchPurchaseRaw(requestParameters: P17ApiFetchPurchaseRequest): Promise<runtime.ApiResponse<PurchaseFetchResponse>>;
    /**
     * Fetch Purchase
     */
    fetchPurchase(requestParameters: P17ApiFetchPurchaseRequest): Promise<PurchaseFetchResponse>;
    /**
     * Fetch Receipt
     */
    fetchReceiptRaw(requestParameters: P17ApiFetchReceiptRequest): Promise<runtime.ApiResponse<ReceiptFetchResponse>>;
    /**
     * Fetch Receipt
     */
    fetchReceipt(requestParameters: P17ApiFetchReceiptRequest): Promise<ReceiptFetchResponse>;
    /**
     * Fetch Refund
     */
    fetchRefundRaw(requestParameters: P17ApiFetchRefundRequest): Promise<runtime.ApiResponse<RefundFetchResponse>>;
    /**
     * Fetch Refund
     */
    fetchRefund(requestParameters: P17ApiFetchRefundRequest): Promise<RefundFetchResponse>;
    /**
     * Fetch Return
     */
    fetchReturnRaw(requestParameters: P17ApiFetchReturnRequest): Promise<runtime.ApiResponse<ReturnFetchResponse>>;
    /**
     * Fetch Return
     */
    fetchReturn(requestParameters: P17ApiFetchReturnRequest): Promise<ReturnFetchResponse>;
    /**
     * Fetch Role
     */
    fetchRoleRaw(requestParameters: P17ApiFetchRoleRequest): Promise<runtime.ApiResponse<RoleFetchResponse>>;
    /**
     * Fetch Role
     */
    fetchRole(requestParameters: P17ApiFetchRoleRequest): Promise<RoleFetchResponse>;
    /**
     * Fetch Routing
     */
    fetchRoutingRaw(requestParameters: P17ApiFetchRoutingRequest): Promise<runtime.ApiResponse<RoutingFetchResponse>>;
    /**
     * Fetch Routing
     */
    fetchRouting(requestParameters: P17ApiFetchRoutingRequest): Promise<RoutingFetchResponse>;
    /**
     * Fetch Shipment
     */
    fetchShipmentRaw(requestParameters: P17ApiFetchShipmentRequest): Promise<runtime.ApiResponse<ShipmentFetchResponse>>;
    /**
     * Fetch Shipment
     */
    fetchShipment(requestParameters: P17ApiFetchShipmentRequest): Promise<ShipmentFetchResponse>;
    /**
     * Fetch Supplier
     */
    fetchSupplierRaw(requestParameters: P17ApiFetchSupplierRequest): Promise<runtime.ApiResponse<SupplierFetchResponse>>;
    /**
     * Fetch Supplier
     */
    fetchSupplier(requestParameters: P17ApiFetchSupplierRequest): Promise<SupplierFetchResponse>;
    /**
     * Fetch Tracking
     */
    fetchTrackingRaw(requestParameters: P17ApiFetchTrackingRequest): Promise<runtime.ApiResponse<TrackingFetchResponse>>;
    /**
     * Fetch Tracking
     */
    fetchTracking(requestParameters: P17ApiFetchTrackingRequest): Promise<TrackingFetchResponse>;
    /**
     * Fetch Transfer
     */
    fetchTransferRaw(requestParameters: P17ApiFetchTransferRequest): Promise<runtime.ApiResponse<TransferFetchResponse>>;
    /**
     * Fetch Transfer
     */
    fetchTransfer(requestParameters: P17ApiFetchTransferRequest): Promise<TransferFetchResponse>;
    /**
     * Fetch User
     */
    fetchUserRaw(requestParameters: P17ApiFetchUserRequest): Promise<runtime.ApiResponse<UserFetchResponse>>;
    /**
     * Fetch User
     */
    fetchUser(requestParameters: P17ApiFetchUserRequest): Promise<UserFetchResponse>;
    /**
     * Fetch Webhook
     */
    fetchWebhookRaw(requestParameters: P17ApiFetchWebhookRequest): Promise<runtime.ApiResponse<WebhookFetchResponse>>;
    /**
     * Fetch Webhook
     */
    fetchWebhook(requestParameters: P17ApiFetchWebhookRequest): Promise<WebhookFetchResponse>;
    /**
     * List Accounts
     */
    listAccountsRaw(requestParameters: P17ApiListAccountsRequest): Promise<runtime.ApiResponse<AccountsListResponse>>;
    /**
     * List Accounts
     */
    listAccounts(requestParameters: P17ApiListAccountsRequest): Promise<AccountsListResponse>;
    /**
     * List API Keys
     */
    listApiKeysRaw(requestParameters: P17ApiListApiKeysRequest): Promise<runtime.ApiResponse<ApiKeysListResponse>>;
    /**
     * List API Keys
     */
    listApiKeys(requestParameters: P17ApiListApiKeysRequest): Promise<ApiKeysListResponse>;
    /**
     * List Arrival Notifications
     */
    listArrivalsRaw(requestParameters: P17ApiListArrivalsRequest): Promise<runtime.ApiResponse<ArrivalsListResponse>>;
    /**
     * List Arrival Notifications
     */
    listArrivals(requestParameters: P17ApiListArrivalsRequest): Promise<ArrivalsListResponse>;
    /**
     * List Connectors
     */
    listConnectorsRaw(requestParameters: P17ApiListConnectorsRequest): Promise<runtime.ApiResponse<ConnectorsListResponse>>;
    /**
     * List Connectors
     */
    listConnectors(requestParameters: P17ApiListConnectorsRequest): Promise<ConnectorsListResponse>;
    /**
     * List Events
     */
    listEventsRaw(requestParameters: P17ApiListEventsRequest): Promise<runtime.ApiResponse<EventsListResponse>>;
    /**
     * List Events
     */
    listEvents(requestParameters: P17ApiListEventsRequest): Promise<EventsListResponse>;
    /**
     * List Fulfillments
     */
    listFulfillmentsRaw(requestParameters: P17ApiListFulfillmentsRequest): Promise<runtime.ApiResponse<FulfillmentsListResponse>>;
    /**
     * List Fulfillments
     */
    listFulfillments(requestParameters: P17ApiListFulfillmentsRequest): Promise<FulfillmentsListResponse>;
    /**
     * List Integrations
     */
    listIntegrationsRaw(requestParameters: P17ApiListIntegrationsRequest): Promise<runtime.ApiResponse<IntegrationsListResponse>>;
    /**
     * List Integrations
     */
    listIntegrations(requestParameters: P17ApiListIntegrationsRequest): Promise<IntegrationsListResponse>;
    /**
     * List Inventory Log Items
     */
    listInventoryRaw(requestParameters: P17ApiListInventoryRequest): Promise<runtime.ApiResponse<InventoryListResponse>>;
    /**
     * List Inventory Log Items
     */
    listInventory(requestParameters: P17ApiListInventoryRequest): Promise<InventoryListResponse>;
    /**
     * List Inventory Rules
     */
    listInventoryRulesRaw(requestParameters: P17ApiListInventoryRulesRequest): Promise<runtime.ApiResponse<InventoryRulesListResponse>>;
    /**
     * List Inventory Rules
     */
    listInventoryRules(requestParameters: P17ApiListInventoryRulesRequest): Promise<InventoryRulesListResponse>;
    /**
     * List long-running jobs
     */
    listJobsRaw(requestParameters: P17ApiListJobsRequest): Promise<runtime.ApiResponse<JobsListResponse>>;
    /**
     * List long-running jobs
     */
    listJobs(requestParameters: P17ApiListJobsRequest): Promise<JobsListResponse>;
    /**
     * List Labels
     */
    listLabelsRaw(requestParameters: P17ApiListLabelsRequest): Promise<runtime.ApiResponse<LabelsListResponse>>;
    /**
     * List Labels
     */
    listLabels(requestParameters: P17ApiListLabelsRequest): Promise<LabelsListResponse>;
    /**
     * List Locations
     */
    listLocationsRaw(requestParameters: P17ApiListLocationsRequest): Promise<runtime.ApiResponse<LocationsListResponse>>;
    /**
     * List Locations
     */
    listLocations(requestParameters: P17ApiListLocationsRequest): Promise<LocationsListResponse>;
    /**
     * List Mappings
     */
    listMappingsRaw(requestParameters: P17ApiListMappingsRequest): Promise<runtime.ApiResponse<MappingsListResponse>>;
    /**
     * List Mappings
     */
    listMappings(requestParameters: P17ApiListMappingsRequest): Promise<MappingsListResponse>;
    /**
     * List Orders
     */
    listOrdersRaw(requestParameters: P17ApiListOrdersRequest): Promise<runtime.ApiResponse<OrdersListResponse>>;
    /**
     * List Orders
     */
    listOrders(requestParameters: P17ApiListOrdersRequest): Promise<OrdersListResponse>;
    /**
     * List Organizations
     */
    listOrganizationsRaw(requestParameters: P17ApiListOrganizationsRequest): Promise<runtime.ApiResponse<OrganizationsListResponse>>;
    /**
     * List Organizations
     */
    listOrganizations(requestParameters: P17ApiListOrganizationsRequest): Promise<OrganizationsListResponse>;
    /**
     * List Products
     */
    listProductsRaw(requestParameters: P17ApiListProductsRequest): Promise<runtime.ApiResponse<ProducstListResponse>>;
    /**
     * List Products
     */
    listProducts(requestParameters: P17ApiListProductsRequest): Promise<ProducstListResponse>;
    /**
     * List Purchases
     */
    listPurchasesRaw(requestParameters: P17ApiListPurchasesRequest): Promise<runtime.ApiResponse<PurchasesListResponse>>;
    /**
     * List Purchases
     */
    listPurchases(requestParameters: P17ApiListPurchasesRequest): Promise<PurchasesListResponse>;
    /**
     * List Receipts
     */
    listReceiptsRaw(requestParameters: P17ApiListReceiptsRequest): Promise<runtime.ApiResponse<ReceiptsListResponse>>;
    /**
     * List Receipts
     */
    listReceipts(requestParameters: P17ApiListReceiptsRequest): Promise<ReceiptsListResponse>;
    /**
     * List Refunds
     */
    listRefundsRaw(requestParameters: P17ApiListRefundsRequest): Promise<runtime.ApiResponse<RefundsListResponse>>;
    /**
     * List Refunds
     */
    listRefunds(requestParameters: P17ApiListRefundsRequest): Promise<RefundsListResponse>;
    /**
     * List Returns
     */
    listReturnsRaw(requestParameters: P17ApiListReturnsRequest): Promise<runtime.ApiResponse<ReturnsListResponse>>;
    /**
     * List Returns
     */
    listReturns(requestParameters: P17ApiListReturnsRequest): Promise<ReturnsListResponse>;
    /**
     * List Roles
     */
    listRolesRaw(requestParameters: P17ApiListRolesRequest): Promise<runtime.ApiResponse<RolesListResponse>>;
    /**
     * List Roles
     */
    listRoles(requestParameters: P17ApiListRolesRequest): Promise<RolesListResponse>;
    /**
     * List Routings
     */
    listRoutingsRaw(requestParameters: P17ApiListRoutingsRequest): Promise<runtime.ApiResponse<RoutingsListResponse>>;
    /**
     * List Routings
     */
    listRoutings(requestParameters: P17ApiListRoutingsRequest): Promise<RoutingsListResponse>;
    /**
     * List Shipments
     */
    listShipmentsRaw(requestParameters: P17ApiListShipmentsRequest): Promise<runtime.ApiResponse<ShipmentsListResponse>>;
    /**
     * List Shipments
     */
    listShipments(requestParameters: P17ApiListShipmentsRequest): Promise<ShipmentsListResponse>;
    /**
     * List Suppliers
     */
    listSuppliersRaw(requestParameters: P17ApiListSuppliersRequest): Promise<runtime.ApiResponse<SuppliersListResponse>>;
    /**
     * List Suppliers
     */
    listSuppliers(requestParameters: P17ApiListSuppliersRequest): Promise<SuppliersListResponse>;
    /**
     * List Trackings
     */
    listTrackingsRaw(requestParameters: P17ApiListTrackingsRequest): Promise<runtime.ApiResponse<TrackingListResponse>>;
    /**
     * List Trackings
     */
    listTrackings(requestParameters: P17ApiListTrackingsRequest): Promise<TrackingListResponse>;
    /**
     * List Transfers
     */
    listTransfersRaw(requestParameters: P17ApiListTransfersRequest): Promise<runtime.ApiResponse<TransfersListResponse>>;
    /**
     * List Transfers
     */
    listTransfers(requestParameters: P17ApiListTransfersRequest): Promise<TransfersListResponse>;
    /**
     * List Users
     */
    listUsersRaw(requestParameters: P17ApiListUsersRequest): Promise<runtime.ApiResponse<UsersListResponse>>;
    /**
     * List Users
     */
    listUsers(requestParameters: P17ApiListUsersRequest): Promise<UsersListResponse>;
    /**
     * List Webhooks
     */
    listWebhooksRaw(requestParameters: P17ApiListWebhooksRequest): Promise<runtime.ApiResponse<WebhooksListResponse>>;
    /**
     * List Webhooks
     */
    listWebhooks(requestParameters: P17ApiListWebhooksRequest): Promise<WebhooksListResponse>;
    /**
     * Send verification email to user
     */
    reinviteUserRaw(requestParameters: P17ApiReinviteUserRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Send verification email to user
     */
    reinviteUser(requestParameters: P17ApiReinviteUserRequest): Promise<Success>;
    /**
     * Replay event entry
     */
    replayEventRaw(requestParameters: P17ApiReplayEventRequest): Promise<runtime.ApiResponse<EventReplayResponse>>;
    /**
     * Replay event entry
     */
    replayEvent(requestParameters: P17ApiReplayEventRequest): Promise<EventReplayResponse>;
    /**
     * Reset user\'s password
     */
    resetPasswordRaw(requestParameters: P17ApiResetPasswordRequest): Promise<runtime.ApiResponse<UserUpdateResponse>>;
    /**
     * Reset user\'s password
     */
    resetPassword(requestParameters: P17ApiResetPasswordRequest): Promise<UserUpdateResponse>;
    /**
     * Update Account
     */
    updateAccountRaw(requestParameters: P17ApiUpdateAccountRequest): Promise<runtime.ApiResponse<AccountUpdateResponse>>;
    /**
     * Update Account
     */
    updateAccount(requestParameters: P17ApiUpdateAccountRequest): Promise<AccountUpdateResponse>;
    /**
     * Update API Key Info
     */
    updateApiKeyRaw(requestParameters: P17ApiUpdateApiKeyRequest): Promise<runtime.ApiResponse<ApiKeyUpdateResponse>>;
    /**
     * Update API Key Info
     */
    updateApiKey(requestParameters: P17ApiUpdateApiKeyRequest): Promise<ApiKeyUpdateResponse>;
    /**
     * Update Arrival Notification
     */
    updateArrivalRaw(requestParameters: P17ApiUpdateArrivalRequest): Promise<runtime.ApiResponse<ArrivalUpdateResponse>>;
    /**
     * Update Arrival Notification
     */
    updateArrival(requestParameters: P17ApiUpdateArrivalRequest): Promise<ArrivalUpdateResponse>;
    /**
     * Update Connector
     */
    updateConnectorRaw(requestParameters: P17ApiUpdateConnectorRequest): Promise<runtime.ApiResponse<ConnectorUpdateResponse>>;
    /**
     * Update Connector
     */
    updateConnector(requestParameters: P17ApiUpdateConnectorRequest): Promise<ConnectorUpdateResponse>;
    /**
     * Update Entity Schema
     */
    updateEntitySchemaRaw(requestParameters: P17ApiUpdateEntitySchemaRequest): Promise<runtime.ApiResponse<EntitySchemaUpdateResponse>>;
    /**
     * Update Entity Schema
     */
    updateEntitySchema(requestParameters: P17ApiUpdateEntitySchemaRequest): Promise<EntitySchemaUpdateResponse>;
    /**
     * Update event entry
     */
    updateEventRaw(requestParameters: P17ApiUpdateEventRequest): Promise<runtime.ApiResponse<EventUpdateResponse>>;
    /**
     * Update event entry
     */
    updateEvent(requestParameters: P17ApiUpdateEventRequest): Promise<EventUpdateResponse>;
    /**
     * Update Integration
     */
    updateIntegrationRaw(requestParameters: P17ApiUpdateIntegrationRequest): Promise<runtime.ApiResponse<IntegrationUpdateResponse>>;
    /**
     * Update Integration
     */
    updateIntegration(requestParameters: P17ApiUpdateIntegrationRequest): Promise<IntegrationUpdateResponse>;
    /**
     * Update Inventory
     */
    updateInventoryRaw(requestParameters: P17ApiUpdateInventoryRequest): Promise<runtime.ApiResponse<InventoryUpdateResponse>>;
    /**
     * Update Inventory
     */
    updateInventory(requestParameters: P17ApiUpdateInventoryRequest): Promise<InventoryUpdateResponse>;
    /**
     * Update Inventory Rule
     */
    updateInventoryRuleRaw(requestParameters: P17ApiUpdateInventoryRuleRequest): Promise<runtime.ApiResponse<InventoryRuleUpdateResponse>>;
    /**
     * Update Inventory Rule
     */
    updateInventoryRule(requestParameters: P17ApiUpdateInventoryRuleRequest): Promise<InventoryRuleUpdateResponse>;
    /**
     * Update Location
     */
    updateLocationRaw(requestParameters: P17ApiUpdateLocationRequest): Promise<runtime.ApiResponse<LocationUpdateResponse>>;
    /**
     * Update Location
     */
    updateLocation(requestParameters: P17ApiUpdateLocationRequest): Promise<LocationUpdateResponse>;
    /**
     * Update Mapping
     */
    updateMappingRaw(requestParameters: P17ApiUpdateMappingRequest): Promise<runtime.ApiResponse<MappingUpdateResponse>>;
    /**
     * Update Mapping
     */
    updateMapping(requestParameters: P17ApiUpdateMappingRequest): Promise<MappingUpdateResponse>;
    /**
     * Update Order
     */
    updateOrderRaw(requestParameters: P17ApiUpdateOrderRequest): Promise<runtime.ApiResponse<OrderUpdateResponse>>;
    /**
     * Update Order
     */
    updateOrder(requestParameters: P17ApiUpdateOrderRequest): Promise<OrderUpdateResponse>;
    /**
     * Update Organization
     */
    updateOrganizationRaw(requestParameters: P17ApiUpdateOrganizationRequest): Promise<runtime.ApiResponse<OrganizationUpdateResponse>>;
    /**
     * Update Organization
     */
    updateOrganization(requestParameters: P17ApiUpdateOrganizationRequest): Promise<OrganizationUpdateResponse>;
    /**
     * Update Product
     */
    updateProductRaw(requestParameters: P17ApiUpdateProductRequest): Promise<runtime.ApiResponse<ProductUpdateResponse>>;
    /**
     * Update Product
     */
    updateProduct(requestParameters: P17ApiUpdateProductRequest): Promise<ProductUpdateResponse>;
    /**
     * Update Purchase
     */
    updatePurchaseRaw(requestParameters: P17ApiUpdatePurchaseRequest): Promise<runtime.ApiResponse<PurchaseUpdateResponse>>;
    /**
     * Update Purchase
     */
    updatePurchase(requestParameters: P17ApiUpdatePurchaseRequest): Promise<PurchaseUpdateResponse>;
    /**
     * Update Refund
     */
    updateRefundRaw(requestParameters: P17ApiUpdateRefundRequest): Promise<runtime.ApiResponse<RefundUpdateResponse>>;
    /**
     * Update Refund
     */
    updateRefund(requestParameters: P17ApiUpdateRefundRequest): Promise<RefundUpdateResponse>;
    /**
     * Update Return
     */
    updateReturnRaw(requestParameters: P17ApiUpdateReturnRequest): Promise<runtime.ApiResponse<ReturnUpdateResponse>>;
    /**
     * Update Return
     */
    updateReturn(requestParameters: P17ApiUpdateReturnRequest): Promise<ReturnUpdateResponse>;
    /**
     * Update Role
     */
    updateRoleRaw(requestParameters: P17ApiUpdateRoleRequest): Promise<runtime.ApiResponse<RoleUpdateResponse>>;
    /**
     * Update Role
     */
    updateRole(requestParameters: P17ApiUpdateRoleRequest): Promise<RoleUpdateResponse>;
    /**
     * Update Routing
     */
    updateRoutingRaw(requestParameters: P17ApiUpdateRoutingRequest): Promise<runtime.ApiResponse<RoutingUpdateResponse>>;
    /**
     * Update Routing
     */
    updateRouting(requestParameters: P17ApiUpdateRoutingRequest): Promise<RoutingUpdateResponse>;
    /**
     * Update Shipment
     */
    updateShipmentRaw(requestParameters: P17ApiUpdateShipmentRequest): Promise<runtime.ApiResponse<ShipmentUpdateResponse>>;
    /**
     * Update Shipment
     */
    updateShipment(requestParameters: P17ApiUpdateShipmentRequest): Promise<ShipmentUpdateResponse>;
    /**
     * Update Supplier
     */
    updateSupplierRaw(requestParameters: P17ApiUpdateSupplierRequest): Promise<runtime.ApiResponse<SupplierUpdateResponse>>;
    /**
     * Update Supplier
     */
    updateSupplier(requestParameters: P17ApiUpdateSupplierRequest): Promise<SupplierUpdateResponse>;
    /**
     * Update Tracking
     */
    updateTrackingRaw(requestParameters: P17ApiUpdateTrackingRequest): Promise<runtime.ApiResponse<TrackingUpdateResponse>>;
    /**
     * Update Tracking
     */
    updateTracking(requestParameters: P17ApiUpdateTrackingRequest): Promise<TrackingUpdateResponse>;
    /**
     * Update Transfer
     */
    updateTransferRaw(requestParameters: P17ApiUpdateTransferRequest): Promise<runtime.ApiResponse<TransferUpdateResponse>>;
    /**
     * Update Transfer
     */
    updateTransfer(requestParameters: P17ApiUpdateTransferRequest): Promise<TransferUpdateResponse>;
    /**
     * Update User
     */
    updateUserRaw(requestParameters: P17ApiUpdateUserRequest): Promise<runtime.ApiResponse<UserUpdateResponse>>;
    /**
     * Update User
     */
    updateUser(requestParameters: P17ApiUpdateUserRequest): Promise<UserUpdateResponse>;
    /**
     * Update Webhook
     */
    updateWebhookRaw(requestParameters: P17ApiUpdateWebhookRequest): Promise<runtime.ApiResponse<WebhookUpdateResponse>>;
    /**
     * Update Webhook
     */
    updateWebhook(requestParameters: P17ApiUpdateWebhookRequest): Promise<WebhookUpdateResponse>;
    /**
     * Upgrade Integration
     */
    upgradeIntegrationRaw(requestParameters: P17ApiUpgradeIntegrationRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Upgrade Integration
     */
    upgradeIntegration(requestParameters: P17ApiUpgradeIntegrationRequest): Promise<Success>;
    /**
     * Upload Label Content
     */
    uploadLabelContentRaw(requestParameters: P17ApiUploadLabelContentRequest): Promise<runtime.ApiResponse<Success>>;
    /**
     * Upload Label Content
     */
    uploadLabelContent(requestParameters: P17ApiUploadLabelContentRequest): Promise<Success>;
}
