"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionsFilterAllOfToJSON = exports.ExceptionsFilterAllOfFromJSONTyped = exports.ExceptionsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ExceptionsFilterAllOfFromJSON(json) {
    return ExceptionsFilterAllOfFromJSONTyped(json, false);
}
exports.ExceptionsFilterAllOfFromJSON = ExceptionsFilterAllOfFromJSON;
function ExceptionsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'exceptionId': !runtime_1.exists(json, 'exceptionId') ? undefined : json['exceptionId'],
        'exceptionType': !runtime_1.exists(json, 'exceptionType') ? undefined : json['exceptionType'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'entityType': !runtime_1.exists(json, 'entityType') ? undefined : json['entityType'],
    };
}
exports.ExceptionsFilterAllOfFromJSONTyped = ExceptionsFilterAllOfFromJSONTyped;
function ExceptionsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'exceptionId': value.exceptionId,
        'exceptionType': value.exceptionType,
        'entityId': value.entityId,
        'entityType': value.entityType,
    };
}
exports.ExceptionsFilterAllOfToJSON = ExceptionsFilterAllOfToJSON;
