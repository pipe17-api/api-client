"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsListErrorToJSON = exports.RoutingsListErrorFromJSONTyped = exports.RoutingsListErrorFromJSON = exports.RoutingsListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoutingsListErrorSuccessEnum;
(function (RoutingsListErrorSuccessEnum) {
    RoutingsListErrorSuccessEnum["False"] = "false";
})(RoutingsListErrorSuccessEnum = exports.RoutingsListErrorSuccessEnum || (exports.RoutingsListErrorSuccessEnum = {}));
function RoutingsListErrorFromJSON(json) {
    return RoutingsListErrorFromJSONTyped(json, false);
}
exports.RoutingsListErrorFromJSON = RoutingsListErrorFromJSON;
function RoutingsListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RoutingsListFilterFromJSON(json['filters']),
    };
}
exports.RoutingsListErrorFromJSONTyped = RoutingsListErrorFromJSONTyped;
function RoutingsListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.RoutingsListFilterToJSON(value.filters),
    };
}
exports.RoutingsListErrorToJSON = RoutingsListErrorToJSON;
