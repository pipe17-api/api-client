"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuppliersListResponseToJSON = exports.SuppliersListResponseFromJSONTyped = exports.SuppliersListResponseFromJSON = exports.SuppliersListResponseCodeEnum = exports.SuppliersListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var SuppliersListResponseSuccessEnum;
(function (SuppliersListResponseSuccessEnum) {
    SuppliersListResponseSuccessEnum["True"] = "true";
})(SuppliersListResponseSuccessEnum = exports.SuppliersListResponseSuccessEnum || (exports.SuppliersListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var SuppliersListResponseCodeEnum;
(function (SuppliersListResponseCodeEnum) {
    SuppliersListResponseCodeEnum[SuppliersListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    SuppliersListResponseCodeEnum[SuppliersListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    SuppliersListResponseCodeEnum[SuppliersListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(SuppliersListResponseCodeEnum = exports.SuppliersListResponseCodeEnum || (exports.SuppliersListResponseCodeEnum = {}));
function SuppliersListResponseFromJSON(json) {
    return SuppliersListResponseFromJSONTyped(json, false);
}
exports.SuppliersListResponseFromJSON = SuppliersListResponseFromJSON;
function SuppliersListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.SuppliersListFilterFromJSON(json['filters']),
        'suppliers': !runtime_1.exists(json, 'suppliers') ? undefined : (json['suppliers'].map(_1.SupplierFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.SuppliersListResponseFromJSONTyped = SuppliersListResponseFromJSONTyped;
function SuppliersListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.SuppliersListFilterToJSON(value.filters),
        'suppliers': value.suppliers === undefined ? undefined : (value.suppliers.map(_1.SupplierToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.SuppliersListResponseToJSON = SuppliersListResponseToJSON;
