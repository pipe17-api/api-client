/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleStatus } from './';
/**
 *
 * @export
 * @interface RolesListFilter
 */
export interface RolesListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RolesListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RolesListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RolesListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RolesListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RolesListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RolesListFilter
     */
    count?: number;
    /**
     * Roles by list of roleId
     * @type {Array<string>}
     * @memberof RolesListFilter
     */
    roleId?: Array<string>;
    /**
     * Roles whose name contains this string
     * @type {string}
     * @memberof RolesListFilter
     */
    name?: string;
    /**
     * Roles by list of statuses
     * @type {Array<RoleStatus>}
     * @memberof RolesListFilter
     */
    status?: Array<RoleStatus>;
    /**
     * List sort order
     * @type {string}
     * @memberof RolesListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof RolesListFilter
     */
    keys?: string;
}
export declare function RolesListFilterFromJSON(json: any): RolesListFilter;
export declare function RolesListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesListFilter;
export declare function RolesListFilterToJSON(value?: RolesListFilter | null): any;
