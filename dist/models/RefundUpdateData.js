"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundUpdateDataToJSON = exports.RefundUpdateDataFromJSONTyped = exports.RefundUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundUpdateDataFromJSON(json) {
    return RefundUpdateDataFromJSONTyped(json, false);
}
exports.RefundUpdateDataFromJSON = RefundUpdateDataFromJSON;
function RefundUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.RefundStatusFromJSON(json['status']),
        'amount': !runtime_1.exists(json, 'amount') ? undefined : json['amount'],
        'creditIssued': !runtime_1.exists(json, 'creditIssued') ? undefined : json['creditIssued'],
        'creditSpent': !runtime_1.exists(json, 'creditSpent') ? undefined : json['creditSpent'],
        'merchantInvoiceAmount': !runtime_1.exists(json, 'merchantInvoiceAmount') ? undefined : json['merchantInvoiceAmount'],
        'customerInvoiceAmount': !runtime_1.exists(json, 'customerInvoiceAmount') ? undefined : json['customerInvoiceAmount'],
    };
}
exports.RefundUpdateDataFromJSONTyped = RefundUpdateDataFromJSONTyped;
function RefundUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.RefundStatusToJSON(value.status),
        'amount': value.amount,
        'creditIssued': value.creditIssued,
        'creditSpent': value.creditSpent,
        'merchantInvoiceAmount': value.merchantInvoiceAmount,
        'customerInvoiceAmount': value.customerInvoiceAmount,
    };
}
exports.RefundUpdateDataToJSON = RefundUpdateDataToJSON;
