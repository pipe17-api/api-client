/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductUpdateData } from './';
/**
 *
 * @export
 * @interface ProductUpdateError
 */
export interface ProductUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ProductUpdateError
     */
    success: ProductUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ProductUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ProductUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ProductUpdateData}
     * @memberof ProductUpdateError
     */
    product?: ProductUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ProductUpdateErrorFromJSON(json: any): ProductUpdateError;
export declare function ProductUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductUpdateError;
export declare function ProductUpdateErrorToJSON(value?: ProductUpdateError | null): any;
