/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory, InventoryDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryDeleteResponse
 */
export interface InventoryDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryDeleteResponse
     */
    success: InventoryDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryDeleteResponse
     */
    code: InventoryDeleteResponseCodeEnum;
    /**
     *
     * @type {InventoryDeleteFilter}
     * @memberof InventoryDeleteResponse
     */
    filters?: InventoryDeleteFilter;
    /**
     *
     * @type {Array<Inventory>}
     * @memberof InventoryDeleteResponse
     */
    inventory?: Array<Inventory>;
    /**
     * Number of deleted inventory items
     * @type {number}
     * @memberof InventoryDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryDeleteResponseFromJSON(json: any): InventoryDeleteResponse;
export declare function InventoryDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryDeleteResponse;
export declare function InventoryDeleteResponseToJSON(value?: InventoryDeleteResponse | null): any;
