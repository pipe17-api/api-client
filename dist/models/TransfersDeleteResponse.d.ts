/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Transfer, TransfersDeleteFilter } from './';
/**
 *
 * @export
 * @interface TransfersDeleteResponse
 */
export interface TransfersDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TransfersDeleteResponse
     */
    success: TransfersDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransfersDeleteResponse
     */
    code: TransfersDeleteResponseCodeEnum;
    /**
     *
     * @type {TransfersDeleteFilter}
     * @memberof TransfersDeleteResponse
     */
    filters?: TransfersDeleteFilter;
    /**
     *
     * @type {Array<Transfer>}
     * @memberof TransfersDeleteResponse
     */
    transfers?: Array<Transfer>;
    /**
     * Number of deleted transfers
     * @type {number}
     * @memberof TransfersDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof TransfersDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum TransfersDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TransfersDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TransfersDeleteResponseFromJSON(json: any): TransfersDeleteResponse;
export declare function TransfersDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersDeleteResponse;
export declare function TransfersDeleteResponseToJSON(value?: TransfersDeleteResponse | null): any;
