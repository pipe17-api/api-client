/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentsFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsListErrorAllOf
 */
export interface ShipmentsListErrorAllOf {
    /**
     *
     * @type {ShipmentsFilter}
     * @memberof ShipmentsListErrorAllOf
     */
    filters?: ShipmentsFilter;
}
export declare function ShipmentsListErrorAllOfFromJSON(json: any): ShipmentsListErrorAllOf;
export declare function ShipmentsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsListErrorAllOf;
export declare function ShipmentsListErrorAllOfToJSON(value?: ShipmentsListErrorAllOf | null): any;
