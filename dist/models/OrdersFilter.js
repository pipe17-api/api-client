"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersFilterToJSON = exports.OrdersFilterFromJSONTyped = exports.OrdersFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersFilterFromJSON(json) {
    return OrdersFilterFromJSONTyped(json, false);
}
exports.OrdersFilterFromJSON = OrdersFilterFromJSON;
function OrdersFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'orderSource': !runtime_1.exists(json, 'orderSource') ? undefined : json['orderSource'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.OrderStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.OrdersFilterFromJSONTyped = OrdersFilterFromJSONTyped;
function OrdersFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'orderId': value.orderId,
        'extOrderId': value.extOrderId,
        'orderSource': value.orderSource,
        'status': value.status === undefined ? undefined : (value.status.map(_1.OrderStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.OrdersFilterToJSON = OrdersFilterToJSON;
