/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryCreateResult } from './';
/**
 *
 * @export
 * @interface InventoryCreateResponse
 */
export interface InventoryCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryCreateResponse
     */
    success: InventoryCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryCreateResponse
     */
    code: InventoryCreateResponseCodeEnum;
    /**
     *
     * @type {Array<InventoryCreateResult>}
     * @memberof InventoryCreateResponse
     */
    inventory?: Array<InventoryCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryCreateResponseFromJSON(json: any): InventoryCreateResponse;
export declare function InventoryCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryCreateResponse;
export declare function InventoryCreateResponseToJSON(value?: InventoryCreateResponse | null): any;
