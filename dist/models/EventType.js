"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventTypeToJSON = exports.EventTypeFromJSONTyped = exports.EventTypeFromJSON = exports.EventType = void 0;
/**
 * Type of event triggering inventory change
 * @export
 * @enum {string}
 */
var EventType;
(function (EventType) {
    EventType["Ingest"] = "ingest";
    EventType["Adjust"] = "adjust";
    EventType["Ship"] = "ship";
    EventType["Fulfill"] = "fulfill";
    EventType["Xferin"] = "xferin";
    EventType["Xferout"] = "xferout";
    EventType["Return"] = "return";
    EventType["Receive"] = "receive";
    EventType["Xferfulfill"] = "xferfulfill";
    EventType["FutureShip"] = "futureShip";
    EventType["Release"] = "release";
    EventType["ShipCancel"] = "shipCancel";
    EventType["ShipCancelRestock"] = "shipCancelRestock";
    EventType["FutureShipCancel"] = "futureShipCancel";
    EventType["VirtualCommit"] = "virtualCommit";
})(EventType = exports.EventType || (exports.EventType = {}));
function EventTypeFromJSON(json) {
    return EventTypeFromJSONTyped(json, false);
}
exports.EventTypeFromJSON = EventTypeFromJSON;
function EventTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EventTypeFromJSONTyped = EventTypeFromJSONTyped;
function EventTypeToJSON(value) {
    return value;
}
exports.EventTypeToJSON = EventTypeToJSON;
