"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelsListResponseAllOfToJSON = exports.LabelsListResponseAllOfFromJSONTyped = exports.LabelsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LabelsListResponseAllOfFromJSON(json) {
    return LabelsListResponseAllOfFromJSONTyped(json, false);
}
exports.LabelsListResponseAllOfFromJSON = LabelsListResponseAllOfFromJSON;
function LabelsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LabelsListFilterFromJSON(json['filters']),
        'labels': !runtime_1.exists(json, 'labels') ? undefined : (json['labels'].map(_1.LabelFromJSON)),
    };
}
exports.LabelsListResponseAllOfFromJSONTyped = LabelsListResponseAllOfFromJSONTyped;
function LabelsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LabelsListFilterToJSON(value.filters),
        'labels': value.labels === undefined ? undefined : (value.labels.map(_1.LabelToJSON)),
    };
}
exports.LabelsListResponseAllOfToJSON = LabelsListResponseAllOfToJSON;
