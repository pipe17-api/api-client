/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AccountAddress
 */
export interface AccountAddress {
    /**
     * Address Line 1
     * @type {string}
     * @memberof AccountAddress
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof AccountAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof AccountAddress
     */
    city?: string;
    /**
     * State or Province
     * @type {string}
     * @memberof AccountAddress
     */
    stateOrProvince: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof AccountAddress
     */
    zipCodeOrPostalCode: string;
    /**
     * Country
     * @type {string}
     * @memberof AccountAddress
     */
    country: string;
}
export declare function AccountAddressFromJSON(json: any): AccountAddress;
export declare function AccountAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountAddress;
export declare function AccountAddressToJSON(value?: AccountAddress | null): any;
