/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Role } from './';
/**
 *
 * @export
 * @interface RoleCreateResponse
 */
export interface RoleCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoleCreateResponse
     */
    success: RoleCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoleCreateResponse
     */
    code: RoleCreateResponseCodeEnum;
    /**
     *
     * @type {Role}
     * @memberof RoleCreateResponse
     */
    role?: Role;
}
/**
* @export
* @enum {string}
*/
export declare enum RoleCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoleCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoleCreateResponseFromJSON(json: any): RoleCreateResponse;
export declare function RoleCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleCreateResponse;
export declare function RoleCreateResponseToJSON(value?: RoleCreateResponse | null): any;
