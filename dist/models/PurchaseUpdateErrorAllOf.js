"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseUpdateErrorAllOfToJSON = exports.PurchaseUpdateErrorAllOfFromJSONTyped = exports.PurchaseUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchaseUpdateErrorAllOfFromJSON(json) {
    return PurchaseUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.PurchaseUpdateErrorAllOfFromJSON = PurchaseUpdateErrorAllOfFromJSON;
function PurchaseUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'purchase': !runtime_1.exists(json, 'purchase') ? undefined : _1.PurchaseUpdateDataFromJSON(json['purchase']),
    };
}
exports.PurchaseUpdateErrorAllOfFromJSONTyped = PurchaseUpdateErrorAllOfFromJSONTyped;
function PurchaseUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'purchase': _1.PurchaseUpdateDataToJSON(value.purchase),
    };
}
exports.PurchaseUpdateErrorAllOfToJSON = PurchaseUpdateErrorAllOfToJSON;
