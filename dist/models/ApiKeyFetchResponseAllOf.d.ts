/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKey } from './';
/**
 *
 * @export
 * @interface ApiKeyFetchResponseAllOf
 */
export interface ApiKeyFetchResponseAllOf {
    /**
     *
     * @type {ApiKey}
     * @memberof ApiKeyFetchResponseAllOf
     */
    apikey?: ApiKey;
}
export declare function ApiKeyFetchResponseAllOfFromJSON(json: any): ApiKeyFetchResponseAllOf;
export declare function ApiKeyFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyFetchResponseAllOf;
export declare function ApiKeyFetchResponseAllOfToJSON(value?: ApiKeyFetchResponseAllOf | null): any;
