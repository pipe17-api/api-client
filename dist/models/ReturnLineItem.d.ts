/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReturnLineItem
 */
export interface ReturnLineItem {
    /**
     * Item unique Id (within original order)
     * @type {string}
     * @memberof ReturnLineItem
     */
    uniqueId: string;
    /**
     * original order extOrderId
     * @type {string}
     * @memberof ReturnLineItem
     */
    extOrderId: string;
    /**
     * Item SKU
     * @type {string}
     * @memberof ReturnLineItem
     */
    sku: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof ReturnLineItem
     */
    name?: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof ReturnLineItem
     */
    quantity: number;
    /**
     * Return cause
     * @type {string}
     * @memberof ReturnLineItem
     */
    cause: string;
    /**
     * Return item original charge (??)
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemCharge?: number;
    /**
     * Return item original shipping charge (??)
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemShippingCharge?: number;
    /**
     * Return item original discount (??)
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemDiscount?: number;
    /**
     * Return item original tax (??)
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemTax?: number;
    /**
     * Return item refund amount
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemTotal?: number;
    /**
     * Return item estimated refund amount
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemEstimatedTotal?: number;
    /**
     * Return item restocking fee
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemRestockingFee?: number;
    /**
     * Return shipping label cost
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemLabelCost?: number;
    /**
     * Return shipping paid amount
     * @type {number}
     * @memberof ReturnLineItem
     */
    itemReturnShippingPaid?: number;
}
export declare function ReturnLineItemFromJSON(json: any): ReturnLineItem;
export declare function ReturnLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnLineItem;
export declare function ReturnLineItemToJSON(value?: ReturnLineItem | null): any;
