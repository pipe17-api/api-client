/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionsListFilter } from './';
/**
 *
 * @export
 * @interface ExceptionsListErrorAllOf
 */
export interface ExceptionsListErrorAllOf {
    /**
     *
     * @type {ExceptionsListFilter}
     * @memberof ExceptionsListErrorAllOf
     */
    filters?: ExceptionsListFilter;
}
export declare function ExceptionsListErrorAllOfFromJSON(json: any): ExceptionsListErrorAllOf;
export declare function ExceptionsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsListErrorAllOf;
export declare function ExceptionsListErrorAllOfToJSON(value?: ExceptionsListErrorAllOf | null): any;
