/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Transfer } from './';
/**
 *
 * @export
 * @interface TransferCreateResult
 */
export interface TransferCreateResult {
    /**
     *
     * @type {string}
     * @memberof TransferCreateResult
     */
    status?: TransferCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof TransferCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof TransferCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Transfer}
     * @memberof TransferCreateResult
     */
    transfer?: Transfer;
}
/**
* @export
* @enum {string}
*/
export declare enum TransferCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function TransferCreateResultFromJSON(json: any): TransferCreateResult;
export declare function TransferCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferCreateResult;
export declare function TransferCreateResultToJSON(value?: TransferCreateResult | null): any;
