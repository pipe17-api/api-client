/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserAddress } from './';
/**
 *
 * @export
 * @interface UserCreateRequest
 */
export interface UserCreateRequest {
    /**
     * User Name
     * @type {string}
     * @memberof UserCreateRequest
     */
    name: string;
    /**
     * User email
     * @type {string}
     * @memberof UserCreateRequest
     */
    email: string;
    /**
     * User named Role(s)
     * @type {Array<string>}
     * @memberof UserCreateRequest
     */
    roles: Array<string>;
    /**
     * User Phone
     * @type {string}
     * @memberof UserCreateRequest
     */
    phone?: string;
    /**
     * User Time Zone
     * @type {string}
     * @memberof UserCreateRequest
     */
    timeZone?: string;
    /**
     * User Description
     * @type {string}
     * @memberof UserCreateRequest
     */
    description?: string;
    /**
     *
     * @type {UserAddress}
     * @memberof UserCreateRequest
     */
    address?: UserAddress;
    /**
     * User password
     * @type {string}
     * @memberof UserCreateRequest
     */
    password: string;
}
export declare function UserCreateRequestFromJSON(json: any): UserCreateRequest;
export declare function UserCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateRequest;
export declare function UserCreateRequestToJSON(value?: UserCreateRequest | null): any;
