"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalStatusToJSON = exports.ArrivalStatusFromJSONTyped = exports.ArrivalStatusFromJSON = exports.ArrivalStatus = void 0;
/**
 * Arrival notification Status
 * @export
 * @enum {string}
 */
var ArrivalStatus;
(function (ArrivalStatus) {
    ArrivalStatus["Draft"] = "draft";
    ArrivalStatus["Shipped"] = "shipped";
    ArrivalStatus["Expected"] = "expected";
    ArrivalStatus["PartialReceived"] = "partialReceived";
    ArrivalStatus["Received"] = "received";
    ArrivalStatus["Canceled"] = "canceled";
    ArrivalStatus["Failed"] = "failed";
})(ArrivalStatus = exports.ArrivalStatus || (exports.ArrivalStatus = {}));
function ArrivalStatusFromJSON(json) {
    return ArrivalStatusFromJSONTyped(json, false);
}
exports.ArrivalStatusFromJSON = ArrivalStatusFromJSON;
function ArrivalStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ArrivalStatusFromJSONTyped = ArrivalStatusFromJSONTyped;
function ArrivalStatusToJSON(value) {
    return value;
}
exports.ArrivalStatusToJSON = ArrivalStatusToJSON;
