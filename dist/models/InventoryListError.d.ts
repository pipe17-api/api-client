/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryListFilter } from './';
/**
 *
 * @export
 * @interface InventoryListError
 */
export interface InventoryListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryListError
     */
    success: InventoryListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryListFilter}
     * @memberof InventoryListError
     */
    filters?: InventoryListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryListErrorSuccessEnum {
    False = "false"
}
export declare function InventoryListErrorFromJSON(json: any): InventoryListError;
export declare function InventoryListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryListError;
export declare function InventoryListErrorToJSON(value?: InventoryListError | null): any;
