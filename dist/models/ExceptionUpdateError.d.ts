/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionUpdateData } from './';
/**
 *
 * @export
 * @interface ExceptionUpdateError
 */
export interface ExceptionUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionUpdateError
     */
    success: ExceptionUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ExceptionUpdateData}
     * @memberof ExceptionUpdateError
     */
    exception?: ExceptionUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionUpdateErrorFromJSON(json: any): ExceptionUpdateError;
export declare function ExceptionUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionUpdateError;
export declare function ExceptionUpdateErrorToJSON(value?: ExceptionUpdateError | null): any;
