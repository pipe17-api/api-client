"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleCreateResponseToJSON = exports.InventoryRuleCreateResponseFromJSONTyped = exports.InventoryRuleCreateResponseFromJSON = exports.InventoryRuleCreateResponseCodeEnum = exports.InventoryRuleCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryRuleCreateResponseSuccessEnum;
(function (InventoryRuleCreateResponseSuccessEnum) {
    InventoryRuleCreateResponseSuccessEnum["True"] = "true";
})(InventoryRuleCreateResponseSuccessEnum = exports.InventoryRuleCreateResponseSuccessEnum || (exports.InventoryRuleCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryRuleCreateResponseCodeEnum;
(function (InventoryRuleCreateResponseCodeEnum) {
    InventoryRuleCreateResponseCodeEnum[InventoryRuleCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryRuleCreateResponseCodeEnum[InventoryRuleCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryRuleCreateResponseCodeEnum[InventoryRuleCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryRuleCreateResponseCodeEnum = exports.InventoryRuleCreateResponseCodeEnum || (exports.InventoryRuleCreateResponseCodeEnum = {}));
function InventoryRuleCreateResponseFromJSON(json) {
    return InventoryRuleCreateResponseFromJSONTyped(json, false);
}
exports.InventoryRuleCreateResponseFromJSON = InventoryRuleCreateResponseFromJSON;
function InventoryRuleCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'inventoryrule': !runtime_1.exists(json, 'inventoryrule') ? undefined : _1.InventoryRuleFromJSON(json['inventoryrule']),
    };
}
exports.InventoryRuleCreateResponseFromJSONTyped = InventoryRuleCreateResponseFromJSONTyped;
function InventoryRuleCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'inventoryrule': _1.InventoryRuleToJSON(value.inventoryrule),
    };
}
exports.InventoryRuleCreateResponseToJSON = InventoryRuleCreateResponseToJSON;
