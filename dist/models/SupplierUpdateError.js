"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierUpdateErrorToJSON = exports.SupplierUpdateErrorFromJSONTyped = exports.SupplierUpdateErrorFromJSON = exports.SupplierUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var SupplierUpdateErrorSuccessEnum;
(function (SupplierUpdateErrorSuccessEnum) {
    SupplierUpdateErrorSuccessEnum["False"] = "false";
})(SupplierUpdateErrorSuccessEnum = exports.SupplierUpdateErrorSuccessEnum || (exports.SupplierUpdateErrorSuccessEnum = {}));
function SupplierUpdateErrorFromJSON(json) {
    return SupplierUpdateErrorFromJSONTyped(json, false);
}
exports.SupplierUpdateErrorFromJSON = SupplierUpdateErrorFromJSON;
function SupplierUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierUpdateDataFromJSON(json['supplier']),
    };
}
exports.SupplierUpdateErrorFromJSONTyped = SupplierUpdateErrorFromJSONTyped;
function SupplierUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'supplier': _1.SupplierUpdateDataToJSON(value.supplier),
    };
}
exports.SupplierUpdateErrorToJSON = SupplierUpdateErrorToJSON;
