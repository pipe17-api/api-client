"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingCreateDataToJSON = exports.OrderRoutingCreateDataFromJSONTyped = exports.OrderRoutingCreateDataFromJSON = void 0;
function OrderRoutingCreateDataFromJSON(json) {
    return OrderRoutingCreateDataFromJSONTyped(json, false);
}
exports.OrderRoutingCreateDataFromJSON = OrderRoutingCreateDataFromJSON;
function OrderRoutingCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'rules': json['rules'],
    };
}
exports.OrderRoutingCreateDataFromJSONTyped = OrderRoutingCreateDataFromJSONTyped;
function OrderRoutingCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'rules': value.rules,
    };
}
exports.OrderRoutingCreateDataToJSON = OrderRoutingCreateDataToJSON;
