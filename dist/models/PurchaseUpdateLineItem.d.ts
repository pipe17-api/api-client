/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface PurchaseUpdateLineItem
 */
export interface PurchaseUpdateLineItem {
    /**
     * Item unique Id (within purchase)
     * @type {string}
     * @memberof PurchaseUpdateLineItem
     */
    uniqueId?: string;
    /**
     * Manufacturer Part ID
     * @type {string}
     * @memberof PurchaseUpdateLineItem
     */
    partId?: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof PurchaseUpdateLineItem
     */
    quantity?: number;
    /**
     * Purchaser Item SKU
     * @type {string}
     * @memberof PurchaseUpdateLineItem
     */
    sku?: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof PurchaseUpdateLineItem
     */
    name?: string;
    /**
     * Item UPC
     * @type {string}
     * @memberof PurchaseUpdateLineItem
     */
    upc?: string;
    /**
     * Item Price
     * @type {number}
     * @memberof PurchaseUpdateLineItem
     */
    price?: number;
    /**
     * Shipped quantity
     * @type {number}
     * @memberof PurchaseUpdateLineItem
     */
    shippedQuantity?: number;
    /**
     * Received quantity
     * @type {number}
     * @memberof PurchaseUpdateLineItem
     */
    receivedQuantity?: number;
    /**
     *
     * @type {string}
     * @memberof PurchaseUpdateLineItem
     */
    locationId?: string;
}
export declare function PurchaseUpdateLineItemFromJSON(json: any): PurchaseUpdateLineItem;
export declare function PurchaseUpdateLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseUpdateLineItem;
export declare function PurchaseUpdateLineItemToJSON(value?: PurchaseUpdateLineItem | null): any;
