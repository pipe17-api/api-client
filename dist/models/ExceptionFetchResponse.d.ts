/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Exception } from './';
/**
 *
 * @export
 * @interface ExceptionFetchResponse
 */
export interface ExceptionFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionFetchResponse
     */
    success: ExceptionFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFetchResponse
     */
    code: ExceptionFetchResponseCodeEnum;
    /**
     *
     * @type {Exception}
     * @memberof ExceptionFetchResponse
     */
    exception?: Exception;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionFetchResponseFromJSON(json: any): ExceptionFetchResponse;
export declare function ExceptionFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFetchResponse;
export declare function ExceptionFetchResponseToJSON(value?: ExceptionFetchResponse | null): any;
