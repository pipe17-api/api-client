/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestData, EventRequestSourcePublic, EventRequestStatus, EventResponseData } from './';
/**
 *
 * @export
 * @interface EventCreateData
 */
export interface EventCreateData {
    /**
     *
     * @type {EventRequestSourcePublic}
     * @memberof EventCreateData
     */
    source?: EventRequestSourcePublic;
    /**
     *
     * @type {EventRequestStatus}
     * @memberof EventCreateData
     */
    status?: EventRequestStatus;
    /**
     * API request Id
     * @type {string}
     * @memberof EventCreateData
     */
    requestId?: string;
    /**
     *
     * @type {EventRequestData}
     * @memberof EventCreateData
     */
    request?: EventRequestData;
    /**
     *
     * @type {EventResponseData}
     * @memberof EventCreateData
     */
    response?: EventResponseData;
    /**
     * integration id
     * @type {string}
     * @memberof EventCreateData
     */
    integration?: string;
    /**
     * operation
     * @type {string}
     * @memberof EventCreateData
     */
    operation?: EventCreateDataOperationEnum;
    /**
     * entity type
     * @type {string}
     * @memberof EventCreateData
     */
    entityType?: string;
    /**
     * entityId
     * @type {string}
     * @memberof EventCreateData
     */
    entityId?: string;
    /**
     * When the event was triggered
     * @type {Date}
     * @memberof EventCreateData
     */
    timestamp?: Date;
    /**
     * operation duration
     * @type {number}
     * @memberof EventCreateData
     */
    duration?: number;
    /**
     * API user id
     * @type {string}
     * @memberof EventCreateData
     */
    userId?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum EventCreateDataOperationEnum {
    Create = "create",
    Update = "update",
    Delete = "delete"
}
export declare function EventCreateDataFromJSON(json: any): EventCreateData;
export declare function EventCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventCreateData;
export declare function EventCreateDataToJSON(value?: EventCreateData | null): any;
