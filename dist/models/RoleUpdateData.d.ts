/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MethodsUpdateData, RoleStatus } from './';
/**
 *
 * @export
 * @interface RoleUpdateData
 */
export interface RoleUpdateData {
    /**
     * Role Name
     * @type {string}
     * @memberof RoleUpdateData
     */
    name?: string;
    /**
     * Role's description
     * @type {string}
     * @memberof RoleUpdateData
     */
    description?: string;
    /**
     *
     * @type {MethodsUpdateData}
     * @memberof RoleUpdateData
     */
    methods?: MethodsUpdateData;
    /**
     *
     * @type {RoleStatus}
     * @memberof RoleUpdateData
     */
    status?: RoleStatus;
}
export declare function RoleUpdateDataFromJSON(json: any): RoleUpdateData;
export declare function RoleUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleUpdateData;
export declare function RoleUpdateDataToJSON(value?: RoleUpdateData | null): any;
