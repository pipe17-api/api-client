"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobsListResponseToJSON = exports.JobsListResponseFromJSONTyped = exports.JobsListResponseFromJSON = exports.JobsListResponseCodeEnum = exports.JobsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var JobsListResponseSuccessEnum;
(function (JobsListResponseSuccessEnum) {
    JobsListResponseSuccessEnum["True"] = "true";
})(JobsListResponseSuccessEnum = exports.JobsListResponseSuccessEnum || (exports.JobsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var JobsListResponseCodeEnum;
(function (JobsListResponseCodeEnum) {
    JobsListResponseCodeEnum[JobsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    JobsListResponseCodeEnum[JobsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    JobsListResponseCodeEnum[JobsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(JobsListResponseCodeEnum = exports.JobsListResponseCodeEnum || (exports.JobsListResponseCodeEnum = {}));
function JobsListResponseFromJSON(json) {
    return JobsListResponseFromJSONTyped(json, false);
}
exports.JobsListResponseFromJSON = JobsListResponseFromJSON;
function JobsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.JobsListFilterFromJSON(json['filters']),
        'jobs': !runtime_1.exists(json, 'jobs') ? undefined : (json['jobs'].map(_1.JobFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.JobsListResponseFromJSONTyped = JobsListResponseFromJSONTyped;
function JobsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.JobsListFilterToJSON(value.filters),
        'jobs': value.jobs === undefined ? undefined : (value.jobs.map(_1.JobToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.JobsListResponseToJSON = JobsListResponseToJSON;
