"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConverterResponseAllOfToJSON = exports.ConverterResponseAllOfFromJSONTyped = exports.ConverterResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConverterResponseAllOfFromJSON(json) {
    return ConverterResponseAllOfFromJSONTyped(json, false);
}
exports.ConverterResponseAllOfFromJSON = ConverterResponseAllOfFromJSON;
function ConverterResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'result': !runtime_1.exists(json, 'result') ? undefined : _1.ConverterResponseAllOfResultFromJSON(json['result']),
        'info': !runtime_1.exists(json, 'info') ? undefined : json['info'],
    };
}
exports.ConverterResponseAllOfFromJSONTyped = ConverterResponseAllOfFromJSONTyped;
function ConverterResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'message': value.message,
        'result': _1.ConverterResponseAllOfResultToJSON(value.result),
        'info': value.info,
    };
}
exports.ConverterResponseAllOfToJSON = ConverterResponseAllOfToJSON;
