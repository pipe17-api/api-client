"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaCreateResponseToJSON = exports.EntitySchemaCreateResponseFromJSONTyped = exports.EntitySchemaCreateResponseFromJSON = exports.EntitySchemaCreateResponseCodeEnum = exports.EntitySchemaCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntitySchemaCreateResponseSuccessEnum;
(function (EntitySchemaCreateResponseSuccessEnum) {
    EntitySchemaCreateResponseSuccessEnum["True"] = "true";
})(EntitySchemaCreateResponseSuccessEnum = exports.EntitySchemaCreateResponseSuccessEnum || (exports.EntitySchemaCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntitySchemaCreateResponseCodeEnum;
(function (EntitySchemaCreateResponseCodeEnum) {
    EntitySchemaCreateResponseCodeEnum[EntitySchemaCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntitySchemaCreateResponseCodeEnum[EntitySchemaCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntitySchemaCreateResponseCodeEnum[EntitySchemaCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntitySchemaCreateResponseCodeEnum = exports.EntitySchemaCreateResponseCodeEnum || (exports.EntitySchemaCreateResponseCodeEnum = {}));
function EntitySchemaCreateResponseFromJSON(json) {
    return EntitySchemaCreateResponseFromJSONTyped(json, false);
}
exports.EntitySchemaCreateResponseFromJSON = EntitySchemaCreateResponseFromJSON;
function EntitySchemaCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'entitySchema': !runtime_1.exists(json, 'entitySchema') ? undefined : _1.EntitySchemaFromJSON(json['entitySchema']),
    };
}
exports.EntitySchemaCreateResponseFromJSONTyped = EntitySchemaCreateResponseFromJSONTyped;
function EntitySchemaCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'entitySchema': _1.EntitySchemaToJSON(value.entitySchema),
    };
}
exports.EntitySchemaCreateResponseToJSON = EntitySchemaCreateResponseToJSON;
