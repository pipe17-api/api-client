/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Tracking } from './';
/**
 *
 * @export
 * @interface TrackingFetchResponse
 */
export interface TrackingFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TrackingFetchResponse
     */
    success: TrackingFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingFetchResponse
     */
    code: TrackingFetchResponseCodeEnum;
    /**
     *
     * @type {Tracking}
     * @memberof TrackingFetchResponse
     */
    tracking?: Tracking;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TrackingFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TrackingFetchResponseFromJSON(json: any): TrackingFetchResponse;
export declare function TrackingFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingFetchResponse;
export declare function TrackingFetchResponseToJSON(value?: TrackingFetchResponse | null): any;
