/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @enum {string}
 */
export declare enum TrackingStatus {
    Created = "created",
    Shipped = "shipped",
    Delivered = "delivered"
}
export declare function TrackingStatusFromJSON(json: any): TrackingStatus;
export declare function TrackingStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingStatus;
export declare function TrackingStatusToJSON(value?: TrackingStatus | null): any;
