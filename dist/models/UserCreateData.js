"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreateDataToJSON = exports.UserCreateDataFromJSONTyped = exports.UserCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UserCreateDataFromJSON(json) {
    return UserCreateDataFromJSONTyped(json, false);
}
exports.UserCreateDataFromJSON = UserCreateDataFromJSON;
function UserCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'email': json['email'],
        'roles': json['roles'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'timeZone': !runtime_1.exists(json, 'timeZone') ? undefined : json['timeZone'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'address': !runtime_1.exists(json, 'address') ? undefined : _1.UserAddressFromJSON(json['address']),
    };
}
exports.UserCreateDataFromJSONTyped = UserCreateDataFromJSONTyped;
function UserCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'email': value.email,
        'roles': value.roles,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'description': value.description,
        'address': _1.UserAddressToJSON(value.address),
    };
}
exports.UserCreateDataToJSON = UserCreateDataToJSON;
