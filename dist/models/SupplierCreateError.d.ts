/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierCreateData } from './';
/**
 *
 * @export
 * @interface SupplierCreateError
 */
export interface SupplierCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof SupplierCreateError
     */
    success: SupplierCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SupplierCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof SupplierCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof SupplierCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {SupplierCreateData}
     * @memberof SupplierCreateError
     */
    supplier?: SupplierCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum SupplierCreateErrorSuccessEnum {
    False = "false"
}
export declare function SupplierCreateErrorFromJSON(json: any): SupplierCreateError;
export declare function SupplierCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierCreateError;
export declare function SupplierCreateErrorToJSON(value?: SupplierCreateError | null): any;
