"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductUpdateErrorToJSON = exports.ProductUpdateErrorFromJSONTyped = exports.ProductUpdateErrorFromJSON = exports.ProductUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductUpdateErrorSuccessEnum;
(function (ProductUpdateErrorSuccessEnum) {
    ProductUpdateErrorSuccessEnum["False"] = "false";
})(ProductUpdateErrorSuccessEnum = exports.ProductUpdateErrorSuccessEnum || (exports.ProductUpdateErrorSuccessEnum = {}));
function ProductUpdateErrorFromJSON(json) {
    return ProductUpdateErrorFromJSONTyped(json, false);
}
exports.ProductUpdateErrorFromJSON = ProductUpdateErrorFromJSON;
function ProductUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'product': !runtime_1.exists(json, 'product') ? undefined : _1.ProductUpdateDataFromJSON(json['product']),
    };
}
exports.ProductUpdateErrorFromJSONTyped = ProductUpdateErrorFromJSONTyped;
function ProductUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'product': _1.ProductUpdateDataToJSON(value.product),
    };
}
exports.ProductUpdateErrorToJSON = ProductUpdateErrorToJSON;
