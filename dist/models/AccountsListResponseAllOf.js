"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsListResponseAllOfToJSON = exports.AccountsListResponseAllOfFromJSONTyped = exports.AccountsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountsListResponseAllOfFromJSON(json) {
    return AccountsListResponseAllOfFromJSONTyped(json, false);
}
exports.AccountsListResponseAllOfFromJSON = AccountsListResponseAllOfFromJSON;
function AccountsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.AccountsListFilterFromJSON(json['filters']),
        'accounts': !runtime_1.exists(json, 'accounts') ? undefined : (json['accounts'].map(_1.AccountFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.AccountsListResponseAllOfFromJSONTyped = AccountsListResponseAllOfFromJSONTyped;
function AccountsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.AccountsListFilterToJSON(value.filters),
        'accounts': value.accounts === undefined ? undefined : (value.accounts.map(_1.AccountToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.AccountsListResponseAllOfToJSON = AccountsListResponseAllOfToJSON;
