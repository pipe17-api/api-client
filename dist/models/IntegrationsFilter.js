"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationsFilterToJSON = exports.IntegrationsFilterFromJSONTyped = exports.IntegrationsFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationsFilterFromJSON(json) {
    return IntegrationsFilterFromJSONTyped(json, false);
}
exports.IntegrationsFilterFromJSON = IntegrationsFilterFromJSON;
function IntegrationsFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'integrationId': !runtime_1.exists(json, 'integrationId') ? undefined : json['integrationId'],
        'connectorName': !runtime_1.exists(json, 'connectorName') ? undefined : json['connectorName'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.IntegrationStatusFromJSON(json['status']),
    };
}
exports.IntegrationsFilterFromJSONTyped = IntegrationsFilterFromJSONTyped;
function IntegrationsFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'integrationId': value.integrationId,
        'connectorName': value.connectorName,
        'connectorId': value.connectorId,
        'status': _1.IntegrationStatusToJSON(value.status),
    };
}
exports.IntegrationsFilterToJSON = IntegrationsFilterToJSON;
