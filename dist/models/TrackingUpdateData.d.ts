/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TrackingUpdateData
 */
export interface TrackingUpdateData {
    /**
     *
     * @type {Date}
     * @memberof TrackingUpdateData
     */
    expectedArrivalDate?: Date;
    /**
     *
     * @type {string}
     * @memberof TrackingUpdateData
     */
    status: TrackingUpdateDataStatusEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingUpdateDataStatusEnum {
    Shipped = "shipped",
    Delivered = "delivered"
}
export declare function TrackingUpdateDataFromJSON(json: any): TrackingUpdateData;
export declare function TrackingUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingUpdateData;
export declare function TrackingUpdateDataToJSON(value?: TrackingUpdateData | null): any;
