/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Purchase } from './';
/**
 *
 * @export
 * @interface PurchaseFetchResponse
 */
export interface PurchaseFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof PurchaseFetchResponse
     */
    success: PurchaseFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchaseFetchResponse
     */
    code: PurchaseFetchResponseCodeEnum;
    /**
     *
     * @type {Purchase}
     * @memberof PurchaseFetchResponse
     */
    purchase?: Purchase;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchaseFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum PurchaseFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function PurchaseFetchResponseFromJSON(json: any): PurchaseFetchResponse;
export declare function PurchaseFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseFetchResponse;
export declare function PurchaseFetchResponseToJSON(value?: PurchaseFetchResponse | null): any;
