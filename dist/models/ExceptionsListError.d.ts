/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionsListFilter } from './';
/**
 *
 * @export
 * @interface ExceptionsListError
 */
export interface ExceptionsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionsListError
     */
    success: ExceptionsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ExceptionsListFilter}
     * @memberof ExceptionsListError
     */
    filters?: ExceptionsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionsListErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionsListErrorFromJSON(json: any): ExceptionsListError;
export declare function ExceptionsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsListError;
export declare function ExceptionsListErrorToJSON(value?: ExceptionsListError | null): any;
