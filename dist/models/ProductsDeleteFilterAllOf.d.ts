/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Products Filter
 * @export
 * @interface ProductsDeleteFilterAllOf
 */
export interface ProductsDeleteFilterAllOf {
    /**
     * Products of these integrations
     * @type {Array<string>}
     * @memberof ProductsDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function ProductsDeleteFilterAllOfFromJSON(json: any): ProductsDeleteFilterAllOf;
export declare function ProductsDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsDeleteFilterAllOf;
export declare function ProductsDeleteFilterAllOfToJSON(value?: ProductsDeleteFilterAllOf | null): any;
