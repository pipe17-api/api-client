"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionCreateErrorToJSON = exports.ExceptionCreateErrorFromJSONTyped = exports.ExceptionCreateErrorFromJSON = exports.ExceptionCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionCreateErrorSuccessEnum;
(function (ExceptionCreateErrorSuccessEnum) {
    ExceptionCreateErrorSuccessEnum["False"] = "false";
})(ExceptionCreateErrorSuccessEnum = exports.ExceptionCreateErrorSuccessEnum || (exports.ExceptionCreateErrorSuccessEnum = {}));
function ExceptionCreateErrorFromJSON(json) {
    return ExceptionCreateErrorFromJSONTyped(json, false);
}
exports.ExceptionCreateErrorFromJSON = ExceptionCreateErrorFromJSON;
function ExceptionCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionCreateDataFromJSON(json['exception']),
    };
}
exports.ExceptionCreateErrorFromJSONTyped = ExceptionCreateErrorFromJSONTyped;
function ExceptionCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'exception': _1.ExceptionCreateDataToJSON(value.exception),
    };
}
exports.ExceptionCreateErrorToJSON = ExceptionCreateErrorToJSON;
