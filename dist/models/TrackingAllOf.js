"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingAllOfToJSON = exports.TrackingAllOfFromJSONTyped = exports.TrackingAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingAllOfFromJSON(json) {
    return TrackingAllOfFromJSONTyped(json, false);
}
exports.TrackingAllOfFromJSON = TrackingAllOfFromJSON;
function TrackingAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'trackingId': !runtime_1.exists(json, 'trackingId') ? undefined : json['trackingId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.TrackingStatusFromJSON(json['status']),
        'actualShipDate': !runtime_1.exists(json, 'actualShipDate') ? undefined : (new Date(json['actualShipDate'])),
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (new Date(json['actualArrivalDate'])),
    };
}
exports.TrackingAllOfFromJSONTyped = TrackingAllOfFromJSONTyped;
function TrackingAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'trackingId': value.trackingId,
        'status': _1.TrackingStatusToJSON(value.status),
        'actualShipDate': value.actualShipDate === undefined ? undefined : (new Date(value.actualShipDate).toISOString()),
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (new Date(value.actualArrivalDate).toISOString()),
    };
}
exports.TrackingAllOfToJSON = TrackingAllOfToJSON;
