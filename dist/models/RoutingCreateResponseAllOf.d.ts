/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Routing } from './';
/**
 *
 * @export
 * @interface RoutingCreateResponseAllOf
 */
export interface RoutingCreateResponseAllOf {
    /**
     *
     * @type {Routing}
     * @memberof RoutingCreateResponseAllOf
     */
    routing?: Routing;
}
export declare function RoutingCreateResponseAllOfFromJSON(json: any): RoutingCreateResponseAllOf;
export declare function RoutingCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingCreateResponseAllOf;
export declare function RoutingCreateResponseAllOfToJSON(value?: RoutingCreateResponseAllOf | null): any;
