"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsFilterAllOfToJSON = exports.ShipmentsFilterAllOfFromJSONTyped = exports.ShipmentsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentsFilterAllOfFromJSON(json) {
    return ShipmentsFilterAllOfFromJSONTyped(json, false);
}
exports.ShipmentsFilterAllOfFromJSON = ShipmentsFilterAllOfFromJSON;
function ShipmentsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'orderType': !runtime_1.exists(json, 'orderType') ? undefined : json['orderType'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ShipmentStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.ShipmentsFilterAllOfFromJSONTyped = ShipmentsFilterAllOfFromJSONTyped;
function ShipmentsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipmentId': value.shipmentId,
        'orderId': value.orderId,
        'orderType': value.orderType,
        'extOrderId': value.extOrderId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ShipmentStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.ShipmentsFilterAllOfToJSON = ShipmentsFilterAllOfToJSON;
