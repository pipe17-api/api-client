/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MappingsFilter
 */
export interface MappingsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof MappingsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof MappingsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof MappingsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof MappingsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof MappingsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof MappingsFilter
     */
    count?: number;
    /**
     * Mappings by list of mappingId
     * @type {Array<string>}
     * @memberof MappingsFilter
     */
    mappingId?: Array<string>;
    /**
     * Mappings by list of UUIDs
     * @type {Array<string>}
     * @memberof MappingsFilter
     */
    uuid?: Array<string>;
    /**
     * Fetch all mappings matching this description
     * @type {string}
     * @memberof MappingsFilter
     */
    desc?: string;
}
export declare function MappingsFilterFromJSON(json: any): MappingsFilter;
export declare function MappingsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsFilter;
export declare function MappingsFilterToJSON(value?: MappingsFilter | null): any;
