"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionCreateErrorAllOfToJSON = exports.ExceptionCreateErrorAllOfFromJSONTyped = exports.ExceptionCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionCreateErrorAllOfFromJSON(json) {
    return ExceptionCreateErrorAllOfFromJSONTyped(json, false);
}
exports.ExceptionCreateErrorAllOfFromJSON = ExceptionCreateErrorAllOfFromJSON;
function ExceptionCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionCreateDataFromJSON(json['exception']),
    };
}
exports.ExceptionCreateErrorAllOfFromJSONTyped = ExceptionCreateErrorAllOfFromJSONTyped;
function ExceptionCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exception': _1.ExceptionCreateDataToJSON(value.exception),
    };
}
exports.ExceptionCreateErrorAllOfToJSON = ExceptionCreateErrorAllOfToJSON;
