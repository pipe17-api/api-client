/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStatus } from './';
/**
 * Integrations Filter
 * @export
 * @interface IntegrationsFilterAllOf
 */
export interface IntegrationsFilterAllOf {
    /**
     * Retrieve by list of integrationId
     * @type {Array<string>}
     * @memberof IntegrationsFilterAllOf
     */
    integrationId?: Array<string>;
    /**
     * Retrieve integrations by list of connector names
     * @type {Array<string>}
     * @memberof IntegrationsFilterAllOf
     */
    connectorName?: Array<string>;
    /**
     * Retrieve integrations by connector ID
     * @type {string}
     * @memberof IntegrationsFilterAllOf
     */
    connectorId?: string;
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationsFilterAllOf
     */
    status?: IntegrationStatus;
}
export declare function IntegrationsFilterAllOfFromJSON(json: any): IntegrationsFilterAllOf;
export declare function IntegrationsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsFilterAllOf;
export declare function IntegrationsFilterAllOfToJSON(value?: IntegrationsFilterAllOf | null): any;
