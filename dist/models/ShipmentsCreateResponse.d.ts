/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentCreateResult } from './';
/**
 *
 * @export
 * @interface ShipmentsCreateResponse
 */
export interface ShipmentsCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ShipmentsCreateResponse
     */
    success: ShipmentsCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentsCreateResponse
     */
    code: ShipmentsCreateResponseCodeEnum;
    /**
     *
     * @type {Array<ShipmentCreateResult>}
     * @memberof ShipmentsCreateResponse
     */
    shipments?: Array<ShipmentCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentsCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ShipmentsCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ShipmentsCreateResponseFromJSON(json: any): ShipmentsCreateResponse;
export declare function ShipmentsCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsCreateResponse;
export declare function ShipmentsCreateResponseToJSON(value?: ShipmentsCreateResponse | null): any;
