/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentsListFilter
 */
export interface FulfillmentsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof FulfillmentsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof FulfillmentsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof FulfillmentsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof FulfillmentsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof FulfillmentsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof FulfillmentsListFilter
     */
    count?: number;
    /**
     * Fulfillments by list of fulfillmentId
     * @type {Array<string>}
     * @memberof FulfillmentsListFilter
     */
    fulfillmentId?: Array<string>;
    /**
     * Fulfillments by list of shipmentId
     * @type {Array<string>}
     * @memberof FulfillmentsListFilter
     */
    shipmentId?: Array<string>;
    /**
     * Fulfillments by list of extShipmentId
     * @type {Array<string>}
     * @memberof FulfillmentsListFilter
     */
    extShipmentId?: Array<string>;
    /**
     * Fulfillments by list of extOrderId
     * @type {Array<string>}
     * @memberof FulfillmentsListFilter
     */
    extOrderId?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof FulfillmentsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof FulfillmentsListFilter
     */
    keys?: string;
}
export declare function FulfillmentsListFilterFromJSON(json: any): FulfillmentsListFilter;
export declare function FulfillmentsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsListFilter;
export declare function FulfillmentsListFilterToJSON(value?: FulfillmentsListFilter | null): any;
