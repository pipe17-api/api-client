/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule } from './';
/**
 *
 * @export
 * @interface InventoryRuleCreateResponse
 */
export interface InventoryRuleCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryRuleCreateResponse
     */
    success: InventoryRuleCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRuleCreateResponse
     */
    code: InventoryRuleCreateResponseCodeEnum;
    /**
     *
     * @type {InventoryRule}
     * @memberof InventoryRuleCreateResponse
     */
    inventoryrule?: InventoryRule;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRuleCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryRuleCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryRuleCreateResponseFromJSON(json: any): InventoryRuleCreateResponse;
export declare function InventoryRuleCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleCreateResponse;
export declare function InventoryRuleCreateResponseToJSON(value?: InventoryRuleCreateResponse | null): any;
