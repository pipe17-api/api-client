"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsDeleteResponseAllOfToJSON = exports.ProductsDeleteResponseAllOfFromJSONTyped = exports.ProductsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductsDeleteResponseAllOfFromJSON(json) {
    return ProductsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.ProductsDeleteResponseAllOfFromJSON = ProductsDeleteResponseAllOfFromJSON;
function ProductsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ProductsDeleteFilterFromJSON(json['filters']),
        'products': !runtime_1.exists(json, 'products') ? undefined : (json['products'].map(_1.ProductFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ProductsDeleteResponseAllOfFromJSONTyped = ProductsDeleteResponseAllOfFromJSONTyped;
function ProductsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ProductsDeleteFilterToJSON(value.filters),
        'products': value.products === undefined ? undefined : (value.products.map(_1.ProductToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ProductsDeleteResponseAllOfToJSON = ProductsDeleteResponseAllOfToJSON;
