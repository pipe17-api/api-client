"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderFetchErrorToJSON = exports.OrderFetchErrorFromJSONTyped = exports.OrderFetchErrorFromJSON = exports.OrderFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var OrderFetchErrorSuccessEnum;
(function (OrderFetchErrorSuccessEnum) {
    OrderFetchErrorSuccessEnum["False"] = "false";
})(OrderFetchErrorSuccessEnum = exports.OrderFetchErrorSuccessEnum || (exports.OrderFetchErrorSuccessEnum = {}));
function OrderFetchErrorFromJSON(json) {
    return OrderFetchErrorFromJSONTyped(json, false);
}
exports.OrderFetchErrorFromJSON = OrderFetchErrorFromJSON;
function OrderFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.OrderFetchErrorFromJSONTyped = OrderFetchErrorFromJSONTyped;
function OrderFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.OrderFetchErrorToJSON = OrderFetchErrorToJSON;
