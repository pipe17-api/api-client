/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationUpdateData } from './';
/**
 *
 * @export
 * @interface IntegrationUpdateError
 */
export interface IntegrationUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationUpdateError
     */
    success: IntegrationUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {IntegrationUpdateData}
     * @memberof IntegrationUpdateError
     */
    integration?: IntegrationUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationUpdateErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationUpdateErrorFromJSON(json: any): IntegrationUpdateError;
export declare function IntegrationUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationUpdateError;
export declare function IntegrationUpdateErrorToJSON(value?: IntegrationUpdateError | null): any;
