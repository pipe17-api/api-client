"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelFetchResponseToJSON = exports.LabelFetchResponseFromJSONTyped = exports.LabelFetchResponseFromJSON = exports.LabelFetchResponseCodeEnum = exports.LabelFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LabelFetchResponseSuccessEnum;
(function (LabelFetchResponseSuccessEnum) {
    LabelFetchResponseSuccessEnum["True"] = "true";
})(LabelFetchResponseSuccessEnum = exports.LabelFetchResponseSuccessEnum || (exports.LabelFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LabelFetchResponseCodeEnum;
(function (LabelFetchResponseCodeEnum) {
    LabelFetchResponseCodeEnum[LabelFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LabelFetchResponseCodeEnum[LabelFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LabelFetchResponseCodeEnum[LabelFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LabelFetchResponseCodeEnum = exports.LabelFetchResponseCodeEnum || (exports.LabelFetchResponseCodeEnum = {}));
function LabelFetchResponseFromJSON(json) {
    return LabelFetchResponseFromJSONTyped(json, false);
}
exports.LabelFetchResponseFromJSON = LabelFetchResponseFromJSON;
function LabelFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'label': !runtime_1.exists(json, 'label') ? undefined : _1.LabelFromJSON(json['label']),
    };
}
exports.LabelFetchResponseFromJSONTyped = LabelFetchResponseFromJSONTyped;
function LabelFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'label': _1.LabelToJSON(value.label),
    };
}
exports.LabelFetchResponseToJSON = LabelFetchResponseToJSON;
