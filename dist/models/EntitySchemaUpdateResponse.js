"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaUpdateResponseToJSON = exports.EntitySchemaUpdateResponseFromJSONTyped = exports.EntitySchemaUpdateResponseFromJSON = exports.EntitySchemaUpdateResponseCodeEnum = exports.EntitySchemaUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var EntitySchemaUpdateResponseSuccessEnum;
(function (EntitySchemaUpdateResponseSuccessEnum) {
    EntitySchemaUpdateResponseSuccessEnum["True"] = "true";
})(EntitySchemaUpdateResponseSuccessEnum = exports.EntitySchemaUpdateResponseSuccessEnum || (exports.EntitySchemaUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntitySchemaUpdateResponseCodeEnum;
(function (EntitySchemaUpdateResponseCodeEnum) {
    EntitySchemaUpdateResponseCodeEnum[EntitySchemaUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntitySchemaUpdateResponseCodeEnum[EntitySchemaUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntitySchemaUpdateResponseCodeEnum[EntitySchemaUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntitySchemaUpdateResponseCodeEnum = exports.EntitySchemaUpdateResponseCodeEnum || (exports.EntitySchemaUpdateResponseCodeEnum = {}));
function EntitySchemaUpdateResponseFromJSON(json) {
    return EntitySchemaUpdateResponseFromJSONTyped(json, false);
}
exports.EntitySchemaUpdateResponseFromJSON = EntitySchemaUpdateResponseFromJSON;
function EntitySchemaUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.EntitySchemaUpdateResponseFromJSONTyped = EntitySchemaUpdateResponseFromJSONTyped;
function EntitySchemaUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.EntitySchemaUpdateResponseToJSON = EntitySchemaUpdateResponseToJSON;
