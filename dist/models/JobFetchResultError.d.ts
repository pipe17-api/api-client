/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface JobFetchResultError
 */
export interface JobFetchResultError {
    /**
     * Always false
     * @type {boolean}
     * @memberof JobFetchResultError
     */
    success: JobFetchResultErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobFetchResultError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof JobFetchResultError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof JobFetchResultError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum JobFetchResultErrorSuccessEnum {
    False = "false"
}
export declare function JobFetchResultErrorFromJSON(json: any): JobFetchResultError;
export declare function JobFetchResultErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobFetchResultError;
export declare function JobFetchResultErrorToJSON(value?: JobFetchResultError | null): any;
