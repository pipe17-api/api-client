"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentFetchErrorToJSON = exports.ShipmentFetchErrorFromJSONTyped = exports.ShipmentFetchErrorFromJSON = exports.ShipmentFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ShipmentFetchErrorSuccessEnum;
(function (ShipmentFetchErrorSuccessEnum) {
    ShipmentFetchErrorSuccessEnum["False"] = "false";
})(ShipmentFetchErrorSuccessEnum = exports.ShipmentFetchErrorSuccessEnum || (exports.ShipmentFetchErrorSuccessEnum = {}));
function ShipmentFetchErrorFromJSON(json) {
    return ShipmentFetchErrorFromJSONTyped(json, false);
}
exports.ShipmentFetchErrorFromJSON = ShipmentFetchErrorFromJSON;
function ShipmentFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.ShipmentFetchErrorFromJSONTyped = ShipmentFetchErrorFromJSONTyped;
function ShipmentFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.ShipmentFetchErrorToJSON = ShipmentFetchErrorToJSON;
