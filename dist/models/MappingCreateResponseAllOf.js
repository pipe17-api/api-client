"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingCreateResponseAllOfToJSON = exports.MappingCreateResponseAllOfFromJSONTyped = exports.MappingCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingCreateResponseAllOfFromJSON(json) {
    return MappingCreateResponseAllOfFromJSONTyped(json, false);
}
exports.MappingCreateResponseAllOfFromJSON = MappingCreateResponseAllOfFromJSON;
function MappingCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingFromJSON(json['mapping']),
    };
}
exports.MappingCreateResponseAllOfFromJSONTyped = MappingCreateResponseAllOfFromJSONTyped;
function MappingCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mapping': _1.MappingToJSON(value.mapping),
    };
}
exports.MappingCreateResponseAllOfToJSON = MappingCreateResponseAllOfToJSON;
