/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressUpdateData, NameValue, OrderCustomer, OrderStatus, OrderUpdateLineItem, Payment } from './';
/**
 *
 * @export
 * @interface OrderUpdateRequest
 */
export interface OrderUpdateRequest {
    /**
     *
     * @type {OrderStatus}
     * @memberof OrderUpdateRequest
     */
    status?: OrderStatus;
    /**
     * The original order platform, walmart, etsy, etc
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    orderSource?: string | null;
    /**
     * Order Url in External System
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    extOrderUrl?: string;
    /**
     *
     * @type {Array<OrderUpdateLineItem>}
     * @memberof OrderUpdateRequest
     */
    lineItems?: Array<OrderUpdateLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof OrderUpdateRequest
     */
    subTotalPrice?: number | null;
    /**
     * Order Discount
     * @type {number}
     * @memberof OrderUpdateRequest
     */
    orderDiscount?: number | null;
    /**
     * Order Tax
     * @type {number}
     * @memberof OrderUpdateRequest
     */
    orderTax?: number | null;
    /**
     * Total Price
     * @type {number}
     * @memberof OrderUpdateRequest
     */
    totalPrice?: number | null;
    /**
     * Order Notes
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    orderNote?: string | null;
    /**
     * Order Gift note
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    giftNote?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof OrderUpdateRequest
     */
    shippingAddress?: AddressUpdateData;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    shippingCarrier?: string | null;
    /**
     * Required with ShippingCarrier, 2nd Priority
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    shippingClass?: string | null;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof OrderUpdateRequest
     */
    shipByDate?: Date | null;
    /**
     * Ship After Date
     * @type {Date}
     * @memberof OrderUpdateRequest
     */
    shipAfterDate?: Date | null;
    /**
     * First Priority to be respected
     * @type {Date}
     * @memberof OrderUpdateRequest
     */
    expectedDeliveryDate?: Date | null;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    shippingNote?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof OrderUpdateRequest
     */
    billingAddress?: AddressUpdateData;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof OrderUpdateRequest
     */
    timestamp?: Date | null;
    /**
     * status of the payment
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    paymentStatus?: string | null;
    /**
     * status of the fulfillment
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    fulfillmentStatus?: string | null;
    /**
     * External Order ID
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    extOrderId?: string;
    /**
     * External System Order API ID
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    extOrderApiId?: string | null;
    /**
     * When the order was created at source
     * @type {Date}
     * @memberof OrderUpdateRequest
     */
    extOrderCreatedAt?: Date | null;
    /**
     * When the order was updated at source
     * @type {Date}
     * @memberof OrderUpdateRequest
     */
    extOrderUpdatedAt?: Date | null;
    /**
     * Id of location defined in organization. Used as item origin for shipment.
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    locationId?: string | null;
    /**
     *
     * @type {number}
     * @memberof OrderUpdateRequest
     */
    shippingPrice?: number | null;
    /**
     * Indicates if fetching of shipping labels is required
     * @type {boolean}
     * @memberof OrderUpdateRequest
     */
    requireShippingLabels?: boolean | null;
    /**
     *
     * @type {OrderCustomer}
     * @memberof OrderUpdateRequest
     */
    customer?: OrderCustomer;
    /**
     * 3rd Priority
     * @type {string}
     * @memberof OrderUpdateRequest
     */
    shippingCode?: string | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof OrderUpdateRequest
     */
    customFields?: Array<NameValue>;
    /**
     * Order tags
     * @type {Array<string>}
     * @memberof OrderUpdateRequest
     */
    tags?: Array<string>;
    /**
     * Payments
     * @type {Array<Payment>}
     * @memberof OrderUpdateRequest
     */
    payments?: Array<Payment>;
}
export declare function OrderUpdateRequestFromJSON(json: any): OrderUpdateRequest;
export declare function OrderUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderUpdateRequest;
export declare function OrderUpdateRequestToJSON(value?: OrderUpdateRequest | null): any;
