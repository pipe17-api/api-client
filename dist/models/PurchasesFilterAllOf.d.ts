/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseStatus } from './';
/**
 * Purchases Filter
 * @export
 * @interface PurchasesFilterAllOf
 */
export interface PurchasesFilterAllOf {
    /**
     * Purchases by list of purchaseId
     * @type {Array<string>}
     * @memberof PurchasesFilterAllOf
     */
    purchaseId?: Array<string>;
    /**
     * Purchases by list of external purchase IDs
     * @type {Array<string>}
     * @memberof PurchasesFilterAllOf
     */
    extOrderId?: Array<string>;
    /**
     * Purchases by list of statuses
     * @type {Array<PurchaseStatus>}
     * @memberof PurchasesFilterAllOf
     */
    status?: Array<PurchaseStatus>;
    /**
     * Soft deleted purchases
     * @type {boolean}
     * @memberof PurchasesFilterAllOf
     */
    deleted?: boolean;
}
export declare function PurchasesFilterAllOfFromJSON(json: any): PurchasesFilterAllOf;
export declare function PurchasesFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesFilterAllOf;
export declare function PurchasesFilterAllOfToJSON(value?: PurchasesFilterAllOf | null): any;
