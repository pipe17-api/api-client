/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountAddress } from './';
/**
 *
 * @export
 * @interface AccountCreateRequest
 */
export interface AccountCreateRequest {
    /**
     * Account Owner email
     * @type {string}
     * @memberof AccountCreateRequest
     */
    email: string;
    /**
     * Account Owner First Name
     * @type {string}
     * @memberof AccountCreateRequest
     */
    ownerFirstName: string;
    /**
     * Account Owner Last Name
     * @type {string}
     * @memberof AccountCreateRequest
     */
    ownerLastName: string;
    /**
     * Account Name
     * @type {string}
     * @memberof AccountCreateRequest
     */
    accountName?: string;
    /**
     * Company Name
     * @type {string}
     * @memberof AccountCreateRequest
     */
    companyName?: string;
    /**
     * Account Phone
     * @type {string}
     * @memberof AccountCreateRequest
     */
    phone: string;
    /**
     * Account Time Zone
     * @type {string}
     * @memberof AccountCreateRequest
     */
    timeZone: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof AccountCreateRequest
     */
    logoUrl?: string;
    /**
     *
     * @type {AccountAddress}
     * @memberof AccountCreateRequest
     */
    address: AccountAddress;
    /**
     * Account Integrations
     * @type {Array<string>}
     * @memberof AccountCreateRequest
     */
    integrations?: Array<string>;
    /**
     *
     * @type {object}
     * @memberof AccountCreateRequest
     */
    payment?: object;
    /**
     * Account Owner password
     * @type {string}
     * @memberof AccountCreateRequest
     */
    password: string;
}
export declare function AccountCreateRequestFromJSON(json: any): AccountCreateRequest;
export declare function AccountCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountCreateRequest;
export declare function AccountCreateRequestToJSON(value?: AccountCreateRequest | null): any;
