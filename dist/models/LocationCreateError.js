"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationCreateErrorToJSON = exports.LocationCreateErrorFromJSONTyped = exports.LocationCreateErrorFromJSON = exports.LocationCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationCreateErrorSuccessEnum;
(function (LocationCreateErrorSuccessEnum) {
    LocationCreateErrorSuccessEnum["False"] = "false";
})(LocationCreateErrorSuccessEnum = exports.LocationCreateErrorSuccessEnum || (exports.LocationCreateErrorSuccessEnum = {}));
function LocationCreateErrorFromJSON(json) {
    return LocationCreateErrorFromJSONTyped(json, false);
}
exports.LocationCreateErrorFromJSON = LocationCreateErrorFromJSON;
function LocationCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationCreateDataFromJSON(json['location']),
    };
}
exports.LocationCreateErrorFromJSONTyped = LocationCreateErrorFromJSONTyped;
function LocationCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'location': _1.LocationCreateDataToJSON(value.location),
    };
}
exports.LocationCreateErrorToJSON = LocationCreateErrorToJSON;
