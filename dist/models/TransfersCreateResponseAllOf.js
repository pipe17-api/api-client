"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersCreateResponseAllOfToJSON = exports.TransfersCreateResponseAllOfFromJSONTyped = exports.TransfersCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransfersCreateResponseAllOfFromJSON(json) {
    return TransfersCreateResponseAllOfFromJSONTyped(json, false);
}
exports.TransfersCreateResponseAllOfFromJSON = TransfersCreateResponseAllOfFromJSON;
function TransfersCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'transfers': !runtime_1.exists(json, 'transfers') ? undefined : (json['transfers'].map(_1.TransferCreateResultFromJSON)),
    };
}
exports.TransfersCreateResponseAllOfFromJSONTyped = TransfersCreateResponseAllOfFromJSONTyped;
function TransfersCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'transfers': value.transfers === undefined ? undefined : (value.transfers.map(_1.TransferCreateResultToJSON)),
    };
}
exports.TransfersCreateResponseAllOfToJSON = TransfersCreateResponseAllOfToJSON;
