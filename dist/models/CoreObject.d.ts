/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface CoreObject
 */
export interface CoreObject {
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof CoreObject
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof CoreObject
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof CoreObject
     */
    readonly orgKey?: string;
}
export declare function CoreObjectFromJSON(json: any): CoreObject;
export declare function CoreObjectFromJSONTyped(json: any, ignoreDiscriminator: boolean): CoreObject;
export declare function CoreObjectToJSON(value?: CoreObject | null): any;
