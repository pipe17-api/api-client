/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Fulfillments Filter
 * @export
 * @interface FulfillmentsFilterAllOf
 */
export interface FulfillmentsFilterAllOf {
    /**
     * Fulfillments by list of fulfillmentId
     * @type {Array<string>}
     * @memberof FulfillmentsFilterAllOf
     */
    fulfillmentId?: Array<string>;
    /**
     * Fulfillments by list of shipmentId
     * @type {Array<string>}
     * @memberof FulfillmentsFilterAllOf
     */
    shipmentId?: Array<string>;
    /**
     * Fulfillments by list of extShipmentId
     * @type {Array<string>}
     * @memberof FulfillmentsFilterAllOf
     */
    extShipmentId?: Array<string>;
    /**
     * Fulfillments by list of extOrderId
     * @type {Array<string>}
     * @memberof FulfillmentsFilterAllOf
     */
    extOrderId?: Array<string>;
}
export declare function FulfillmentsFilterAllOfFromJSON(json: any): FulfillmentsFilterAllOf;
export declare function FulfillmentsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsFilterAllOf;
export declare function FulfillmentsFilterAllOfToJSON(value?: FulfillmentsFilterAllOf | null): any;
