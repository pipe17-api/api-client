"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseToJSON = exports.PurchaseFromJSONTyped = exports.PurchaseFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchaseFromJSON(json) {
    return PurchaseFromJSONTyped(json, false);
}
exports.PurchaseFromJSON = PurchaseFromJSON;
function PurchaseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'purchaseId': !runtime_1.exists(json, 'purchaseId') ? undefined : json['purchaseId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.PurchaseStatusFromJSON(json['status']),
        'extOrderId': json['extOrderId'],
        'purchaseOrderDate': !runtime_1.exists(json, 'purchaseOrderDate') ? undefined : (new Date(json['purchaseOrderDate'])),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.PurchaseLineItemFromJSON)),
        'subTotalPrice': !runtime_1.exists(json, 'subTotalPrice') ? undefined : json['subTotalPrice'],
        'purchaseTax': !runtime_1.exists(json, 'purchaseTax') ? undefined : json['purchaseTax'],
        'costs': !runtime_1.exists(json, 'costs') ? undefined : (json['costs'] === null ? null : json['costs'].map(_1.PurchaseCostFromJSON)),
        'totalPrice': !runtime_1.exists(json, 'totalPrice') ? undefined : json['totalPrice'],
        'employeeName': !runtime_1.exists(json, 'employeeName') ? undefined : json['employeeName'],
        'purchaseNotes': !runtime_1.exists(json, 'purchaseNotes') ? undefined : json['purchaseNotes'],
        'vendorAddress': !runtime_1.exists(json, 'vendorAddress') ? undefined : _1.AddressFromJSON(json['vendorAddress']),
        'toAddress': !runtime_1.exists(json, 'toAddress') ? undefined : _1.AddressFromJSON(json['toAddress']),
        'toLocationId': !runtime_1.exists(json, 'toLocationId') ? undefined : json['toLocationId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingService': !runtime_1.exists(json, 'shippingService') ? undefined : json['shippingService'],
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (new Date(json['shipByDate'])),
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (new Date(json['actualArrivalDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'shippingNotes': !runtime_1.exists(json, 'shippingNotes') ? undefined : json['shippingNotes'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'billingAddress': !runtime_1.exists(json, 'billingAddress') ? undefined : _1.AddressFromJSON(json['billingAddress']),
        'referenceNumber': !runtime_1.exists(json, 'referenceNumber') ? undefined : json['referenceNumber'],
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (new Date(json['timestamp'])),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.PurchaseFromJSONTyped = PurchaseFromJSONTyped;
function PurchaseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'purchaseId': value.purchaseId,
        'integration': value.integration,
        'status': _1.PurchaseStatusToJSON(value.status),
        'extOrderId': value.extOrderId,
        'purchaseOrderDate': value.purchaseOrderDate === undefined ? undefined : (new Date(value.purchaseOrderDate).toISOString()),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.PurchaseLineItemToJSON)),
        'subTotalPrice': value.subTotalPrice,
        'purchaseTax': value.purchaseTax,
        'costs': value.costs === undefined ? undefined : (value.costs === null ? null : value.costs.map(_1.PurchaseCostToJSON)),
        'totalPrice': value.totalPrice,
        'employeeName': value.employeeName,
        'purchaseNotes': value.purchaseNotes,
        'vendorAddress': _1.AddressToJSON(value.vendorAddress),
        'toAddress': _1.AddressToJSON(value.toAddress),
        'toLocationId': value.toLocationId,
        'shippingCarrier': value.shippingCarrier,
        'shippingService': value.shippingService,
        'shipByDate': value.shipByDate === undefined ? undefined : (new Date(value.shipByDate).toISOString()),
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (new Date(value.actualArrivalDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'shippingNotes': value.shippingNotes,
        'incoterms': value.incoterms,
        'billingAddress': _1.AddressToJSON(value.billingAddress),
        'referenceNumber': value.referenceNumber,
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'timestamp': value.timestamp === undefined ? undefined : (new Date(value.timestamp).toISOString()),
    };
}
exports.PurchaseToJSON = PurchaseToJSON;
