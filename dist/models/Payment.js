"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaymentToJSON = exports.PaymentFromJSONTyped = exports.PaymentFromJSON = exports.PaymentKindEnum = exports.PaymentMethodEnum = exports.PaymentStatusEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var PaymentStatusEnum;
(function (PaymentStatusEnum) {
    PaymentStatusEnum["Succeeded"] = "succeeded";
    PaymentStatusEnum["Failed"] = "failed";
})(PaymentStatusEnum = exports.PaymentStatusEnum || (exports.PaymentStatusEnum = {})); /**
* @export
* @enum {string}
*/
var PaymentMethodEnum;
(function (PaymentMethodEnum) {
    PaymentMethodEnum["Cash"] = "cash";
    PaymentMethodEnum["GiftCard"] = "giftCard";
    PaymentMethodEnum["CreditCard"] = "creditCard";
    PaymentMethodEnum["StoreCredit"] = "storeCredit";
    PaymentMethodEnum["Other"] = "other";
})(PaymentMethodEnum = exports.PaymentMethodEnum || (exports.PaymentMethodEnum = {})); /**
* @export
* @enum {string}
*/
var PaymentKindEnum;
(function (PaymentKindEnum) {
    PaymentKindEnum["Visa"] = "visa";
    PaymentKindEnum["Mastercard"] = "mastercard";
    PaymentKindEnum["Amex"] = "amex";
    PaymentKindEnum["Discover"] = "discover";
})(PaymentKindEnum = exports.PaymentKindEnum || (exports.PaymentKindEnum = {}));
function PaymentFromJSON(json) {
    return PaymentFromJSONTyped(json, false);
}
exports.PaymentFromJSON = PaymentFromJSON;
function PaymentFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'amount': json['amount'],
        'currency': json['currency'],
        'conversion': !runtime_1.exists(json, 'conversion') ? undefined : json['conversion'],
        'status': json['status'],
        'method': json['method'],
        'kind': !runtime_1.exists(json, 'kind') ? undefined : json['kind'],
    };
}
exports.PaymentFromJSONTyped = PaymentFromJSONTyped;
function PaymentToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'amount': value.amount,
        'currency': value.currency,
        'conversion': value.conversion,
        'status': value.status,
        'method': value.method,
        'kind': value.kind,
    };
}
exports.PaymentToJSON = PaymentToJSON;
