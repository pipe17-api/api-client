/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface JobCreateError
 */
export interface JobCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof JobCreateError
     */
    success: JobCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof JobCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof JobCreateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum JobCreateErrorSuccessEnum {
    False = "false"
}
export declare function JobCreateErrorFromJSON(json: any): JobCreateError;
export declare function JobCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobCreateError;
export declare function JobCreateErrorToJSON(value?: JobCreateError | null): any;
