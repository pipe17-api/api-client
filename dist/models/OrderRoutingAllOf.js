"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingAllOfToJSON = exports.OrderRoutingAllOfFromJSONTyped = exports.OrderRoutingAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrderRoutingAllOfFromJSON(json) {
    return OrderRoutingAllOfFromJSONTyped(json, false);
}
exports.OrderRoutingAllOfFromJSON = OrderRoutingAllOfFromJSON;
function OrderRoutingAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routingId': !runtime_1.exists(json, 'routingId') ? undefined : json['routingId'],
    };
}
exports.OrderRoutingAllOfFromJSONTyped = OrderRoutingAllOfFromJSONTyped;
function OrderRoutingAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routingId': value.routingId,
    };
}
exports.OrderRoutingAllOfToJSON = OrderRoutingAllOfToJSON;
