"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentCreateDataAllOfToJSON = exports.FulfillmentCreateDataAllOfFromJSONTyped = exports.FulfillmentCreateDataAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function FulfillmentCreateDataAllOfFromJSON(json) {
    return FulfillmentCreateDataAllOfFromJSONTyped(json, false);
}
exports.FulfillmentCreateDataAllOfFromJSON = FulfillmentCreateDataAllOfFromJSON;
function FulfillmentCreateDataAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.FulfillmentItemFromJSON)),
    };
}
exports.FulfillmentCreateDataAllOfFromJSONTyped = FulfillmentCreateDataAllOfFromJSONTyped;
function FulfillmentCreateDataAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.FulfillmentItemToJSON)),
    };
}
exports.FulfillmentCreateDataAllOfToJSON = FulfillmentCreateDataAllOfToJSON;
