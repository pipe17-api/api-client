"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertRequestToJSON = exports.ConvertRequestFromJSONTyped = exports.ConvertRequestFromJSON = void 0;
const _1 = require("./");
function ConvertRequestFromJSON(json) {
    return ConvertRequestFromJSONTyped(json, false);
}
exports.ConvertRequestFromJSON = ConvertRequestFromJSON;
function ConvertRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'rules': _1.ConvertRequestRulesFromJSON(json['rules']),
        'source': json['source'],
    };
}
exports.ConvertRequestFromJSONTyped = ConvertRequestFromJSONTyped;
function ConvertRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'rules': _1.ConvertRequestRulesToJSON(value.rules),
        'source': value.source,
    };
}
exports.ConvertRequestToJSON = ConvertRequestToJSON;
