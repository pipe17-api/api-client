/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentItemExt
 */
export interface FulfillmentItemExt {
    /**
     * Item SKU
     * @type {string}
     * @memberof FulfillmentItemExt
     */
    sku: string;
    /**
     * Item quantity
     * @type {number}
     * @memberof FulfillmentItemExt
     */
    quantity: number;
    /**
     * Item unique Id (to be matched with shipment line item)
     * @type {string}
     * @memberof FulfillmentItemExt
     */
    uniqueId?: string;
    /**
     * Matching bundle SKU when available
     * @type {string}
     * @memberof FulfillmentItemExt
     */
    bundleSKU?: string;
    /**
     * Matching bundle quantity when available
     * @type {number}
     * @memberof FulfillmentItemExt
     */
    bundleQuantity?: number;
}
export declare function FulfillmentItemExtFromJSON(json: any): FulfillmentItemExt;
export declare function FulfillmentItemExtFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentItemExt;
export declare function FulfillmentItemExtToJSON(value?: FulfillmentItemExt | null): any;
