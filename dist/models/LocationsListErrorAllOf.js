"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsListErrorAllOfToJSON = exports.LocationsListErrorAllOfFromJSONTyped = exports.LocationsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationsListErrorAllOfFromJSON(json) {
    return LocationsListErrorAllOfFromJSONTyped(json, false);
}
exports.LocationsListErrorAllOfFromJSON = LocationsListErrorAllOfFromJSON;
function LocationsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsListFilterFromJSON(json['filters']),
    };
}
exports.LocationsListErrorAllOfFromJSONTyped = LocationsListErrorAllOfFromJSONTyped;
function LocationsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LocationsListFilterToJSON(value.filters),
    };
}
exports.LocationsListErrorAllOfToJSON = LocationsListErrorAllOfToJSON;
