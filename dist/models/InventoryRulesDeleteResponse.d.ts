/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule, InventoryRulesDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryRulesDeleteResponse
 */
export interface InventoryRulesDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryRulesDeleteResponse
     */
    success: InventoryRulesDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRulesDeleteResponse
     */
    code: InventoryRulesDeleteResponseCodeEnum;
    /**
     *
     * @type {InventoryRulesDeleteFilter}
     * @memberof InventoryRulesDeleteResponse
     */
    filters?: InventoryRulesDeleteFilter;
    /**
     *
     * @type {Array<InventoryRule>}
     * @memberof InventoryRulesDeleteResponse
     */
    rules?: Array<InventoryRule>;
    /**
     * Number of deleted rules
     * @type {number}
     * @memberof InventoryRulesDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryRulesDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRulesDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryRulesDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryRulesDeleteResponseFromJSON(json: any): InventoryRulesDeleteResponse;
export declare function InventoryRulesDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesDeleteResponse;
export declare function InventoryRulesDeleteResponseToJSON(value?: InventoryRulesDeleteResponse | null): any;
