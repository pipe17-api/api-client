"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsListFilterAllOfToJSON = exports.ProductsListFilterAllOfFromJSONTyped = exports.ProductsListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ProductsListFilterAllOfFromJSON(json) {
    return ProductsListFilterAllOfFromJSONTyped(json, false);
}
exports.ProductsListFilterAllOfFromJSON = ProductsListFilterAllOfFromJSON;
function ProductsListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'bundleSKU': !runtime_1.exists(json, 'bundleSKU') ? undefined : json['bundleSKU'],
        'inBundle': !runtime_1.exists(json, 'inBundle') ? undefined : json['inBundle'],
    };
}
exports.ProductsListFilterAllOfFromJSONTyped = ProductsListFilterAllOfFromJSONTyped;
function ProductsListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'name': value.name,
        'bundleSKU': value.bundleSKU,
        'inBundle': value.inBundle,
    };
}
exports.ProductsListFilterAllOfToJSON = ProductsListFilterAllOfToJSON;
