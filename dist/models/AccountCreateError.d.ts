/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountCreateData } from './';
/**
 *
 * @export
 * @interface AccountCreateError
 */
export interface AccountCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof AccountCreateError
     */
    success: AccountCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof AccountCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof AccountCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {AccountCreateData}
     * @memberof AccountCreateError
     */
    account?: AccountCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountCreateErrorSuccessEnum {
    False = "false"
}
export declare function AccountCreateErrorFromJSON(json: any): AccountCreateError;
export declare function AccountCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountCreateError;
export declare function AccountCreateErrorToJSON(value?: AccountCreateError | null): any;
