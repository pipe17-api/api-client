/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingUpdateData } from './';
/**
 *
 * @export
 * @interface MappingUpdateError
 */
export interface MappingUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof MappingUpdateError
     */
    success: MappingUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof MappingUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof MappingUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {MappingUpdateData}
     * @memberof MappingUpdateError
     */
    mapping?: MappingUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingUpdateErrorSuccessEnum {
    False = "false"
}
export declare function MappingUpdateErrorFromJSON(json: any): MappingUpdateError;
export declare function MappingUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingUpdateError;
export declare function MappingUpdateErrorToJSON(value?: MappingUpdateError | null): any;
