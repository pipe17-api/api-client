"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersFilterAllOfToJSON = exports.UsersFilterAllOfFromJSONTyped = exports.UsersFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UsersFilterAllOfFromJSON(json) {
    return UsersFilterAllOfFromJSONTyped(json, false);
}
exports.UsersFilterAllOfFromJSON = UsersFilterAllOfFromJSON;
function UsersFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'userId': !runtime_1.exists(json, 'userId') ? undefined : json['userId'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.UserStatusFromJSON)),
        'organizations': !runtime_1.exists(json, 'organizations') ? undefined : json['organizations'],
    };
}
exports.UsersFilterAllOfFromJSONTyped = UsersFilterAllOfFromJSONTyped;
function UsersFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'userId': value.userId,
        'name': value.name,
        'email': value.email,
        'status': value.status === undefined ? undefined : (value.status.map(_1.UserStatusToJSON)),
        'organizations': value.organizations,
    };
}
exports.UsersFilterAllOfToJSON = UsersFilterAllOfToJSON;
