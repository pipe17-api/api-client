/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ArrivalLineItem
 */
export interface ArrivalLineItem {
    /**
     * Item unique Id (within an order)
     * @type {string}
     * @memberof ArrivalLineItem
     */
    uniqueId: string;
    /**
     * Item SKU
     * @type {string}
     * @memberof ArrivalLineItem
     */
    sku: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof ArrivalLineItem
     */
    quantity: number;
    /**
     * Item Name
     * @type {string}
     * @memberof ArrivalLineItem
     */
    name?: string;
    /**
     * Item UPC
     * @type {string}
     * @memberof ArrivalLineItem
     */
    upc?: string;
    /**
     * Item Arrived Quantity
     * @type {number}
     * @memberof ArrivalLineItem
     */
    receivedQuantity?: number;
}
export declare function ArrivalLineItemFromJSON(json: any): ArrivalLineItem;
export declare function ArrivalLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalLineItem;
export declare function ArrivalLineItemToJSON(value?: ArrivalLineItem | null): any;
