/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location, LocationsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface LocationsDeleteResponse
 */
export interface LocationsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LocationsDeleteResponse
     */
    success: LocationsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationsDeleteResponse
     */
    code: LocationsDeleteResponseCodeEnum;
    /**
     *
     * @type {LocationsDeleteFilter}
     * @memberof LocationsDeleteResponse
     */
    filters?: LocationsDeleteFilter;
    /**
     *
     * @type {Array<Location>}
     * @memberof LocationsDeleteResponse
     */
    locations?: Array<Location>;
    /**
     * Number of deleted locations
     * @type {number}
     * @memberof LocationsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof LocationsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LocationsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LocationsDeleteResponseFromJSON(json: any): LocationsDeleteResponse;
export declare function LocationsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsDeleteResponse;
export declare function LocationsDeleteResponseToJSON(value?: LocationsDeleteResponse | null): any;
