"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionTypeToJSON = exports.ExceptionTypeFromJSONTyped = exports.ExceptionTypeFromJSON = exports.ExceptionType = void 0;
/**
 * Exception type
 * @export
 * @enum {string}
 */
var ExceptionType;
(function (ExceptionType) {
    ExceptionType["EGeneric"] = "eGeneric";
    ExceptionType["ESyncFailure"] = "eSyncFailure";
    ExceptionType["EUnacknowledged"] = "eUnacknowledged";
    ExceptionType["EDelayed"] = "eDelayed";
    ExceptionType["ELate"] = "eLate";
    ExceptionType["EEarly"] = "eEarly";
    ExceptionType["ELost"] = "eLost";
    ExceptionType["ESplit"] = "eSplit";
    ExceptionType["EStuck"] = "eStuck";
    ExceptionType["ELarge"] = "eLarge";
    ExceptionType["ELow"] = "eLow";
    ExceptionType["EMissing"] = "eMissing";
    ExceptionType["EMissingSku"] = "eMissingSKU";
    ExceptionType["EMissingLocation"] = "eMissingLocation";
    ExceptionType["EMismatch"] = "eMismatch";
    ExceptionType["EMismatchTax"] = "eMismatchTax";
    ExceptionType["EInvalid"] = "eInvalid";
    ExceptionType["EInvalidAddress"] = "eInvalidAddress";
    ExceptionType["EInvalidDate"] = "eInvalidDate";
    ExceptionType["EInvalidLocation"] = "eInvalidLocation";
    ExceptionType["EInvalidSku"] = "eInvalidSKU";
    ExceptionType["EIncomplete"] = "eIncomplete";
    ExceptionType["EIncompleteAddress"] = "eIncompleteAddress";
    ExceptionType["EDuplicate"] = "eDuplicate";
    ExceptionType["EDuplicateSku"] = "eDuplicateSKU";
    ExceptionType["EUnexpected"] = "eUnexpected";
    ExceptionType["EUnexpectedDate"] = "eUnexpectedDate";
    ExceptionType["ERestock"] = "eRestock";
    ExceptionType["EOutOfStock"] = "eOutOfStock";
    ExceptionType["EFraudSuspected"] = "eFraudSuspected";
    ExceptionType["EApprovalRequired"] = "eApprovalRequired";
    ExceptionType["EPartiallyCanceled"] = "ePartiallyCanceled";
    ExceptionType["ERouteNotFound"] = "eRouteNotFound";
    ExceptionType["ERouteOutOfStock"] = "eRouteOutOfStock";
    ExceptionType["ECreditMemoInsufficient"] = "eCreditMemoInsufficient";
    ExceptionType["EOrderSyncFailure"] = "eOrderSyncFailure";
    ExceptionType["EOrderTaxMismatch"] = "eOrderTaxMismatch";
    ExceptionType["EInventoryOutOfStock"] = "eInventoryOutOfStock";
})(ExceptionType = exports.ExceptionType || (exports.ExceptionType = {}));
function ExceptionTypeFromJSON(json) {
    return ExceptionTypeFromJSONTyped(json, false);
}
exports.ExceptionTypeFromJSON = ExceptionTypeFromJSON;
function ExceptionTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ExceptionTypeFromJSONTyped = ExceptionTypeFromJSONTyped;
function ExceptionTypeToJSON(value) {
    return value;
}
exports.ExceptionTypeToJSON = ExceptionTypeToJSON;
