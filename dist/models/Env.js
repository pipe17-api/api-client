"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnvToJSON = exports.EnvFromJSONTyped = exports.EnvFromJSON = exports.Env = void 0;
/**
 * Environment ID
 * @export
 * @enum {string}
 */
var Env;
(function (Env) {
    Env["Test"] = "test";
    Env["Prod"] = "prod";
})(Env = exports.Env || (exports.Env = {}));
function EnvFromJSON(json) {
    return EnvFromJSONTyped(json, false);
}
exports.EnvFromJSON = EnvFromJSON;
function EnvFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EnvFromJSONTyped = EnvFromJSONTyped;
function EnvToJSON(value) {
    return value;
}
exports.EnvToJSON = EnvToJSON;
