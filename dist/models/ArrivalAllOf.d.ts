/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalAllOf
 */
export interface ArrivalAllOf {
    /**
     * Arrival notification ID
     * @type {string}
     * @memberof ArrivalAllOf
     */
    arrivalId?: string;
    /**
     *
     * @type {ArrivalStatus}
     * @memberof ArrivalAllOf
     */
    status?: ArrivalStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof ArrivalAllOf
     */
    integration?: string;
}
export declare function ArrivalAllOfFromJSON(json: any): ArrivalAllOf;
export declare function ArrivalAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalAllOf;
export declare function ArrivalAllOfToJSON(value?: ArrivalAllOf | null): any;
