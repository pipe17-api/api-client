"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingCreateErrorAllOfToJSON = exports.TrackingCreateErrorAllOfFromJSONTyped = exports.TrackingCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingCreateErrorAllOfFromJSON(json) {
    return TrackingCreateErrorAllOfFromJSONTyped(json, false);
}
exports.TrackingCreateErrorAllOfFromJSON = TrackingCreateErrorAllOfFromJSON;
function TrackingCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'tracking': !runtime_1.exists(json, 'tracking') ? undefined : _1.TrackingCreateDataFromJSON(json['tracking']),
    };
}
exports.TrackingCreateErrorAllOfFromJSONTyped = TrackingCreateErrorAllOfFromJSONTyped;
function TrackingCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'tracking': _1.TrackingCreateDataToJSON(value.tracking),
    };
}
exports.TrackingCreateErrorAllOfToJSON = TrackingCreateErrorAllOfToJSON;
