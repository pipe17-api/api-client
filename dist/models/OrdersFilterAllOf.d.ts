/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderStatus } from './';
/**
 * Orders Filter
 * @export
 * @interface OrdersFilterAllOf
 */
export interface OrdersFilterAllOf {
    /**
     * Orders by list of orderId
     * @type {Array<string>}
     * @memberof OrdersFilterAllOf
     */
    orderId?: Array<string>;
    /**
     * Orders by list of extOrderId
     * @type {Array<string>}
     * @memberof OrdersFilterAllOf
     */
    extOrderId?: Array<string>;
    /**
     * Orders by list of order sources
     * @type {Array<string>}
     * @memberof OrdersFilterAllOf
     */
    orderSource?: Array<string>;
    /**
     * Orders by list of statuses
     * @type {Array<OrderStatus>}
     * @memberof OrdersFilterAllOf
     */
    status?: Array<OrderStatus>;
    /**
     * Soft deleted orders
     * @type {boolean}
     * @memberof OrdersFilterAllOf
     */
    deleted?: boolean;
}
export declare function OrdersFilterAllOfFromJSON(json: any): OrdersFilterAllOf;
export declare function OrdersFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersFilterAllOf;
export declare function OrdersFilterAllOfToJSON(value?: OrdersFilterAllOf | null): any;
