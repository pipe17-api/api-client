"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsDeleteFilterToJSON = exports.RefundsDeleteFilterFromJSONTyped = exports.RefundsDeleteFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundsDeleteFilterFromJSON(json) {
    return RefundsDeleteFilterFromJSONTyped(json, false);
}
exports.RefundsDeleteFilterFromJSON = RefundsDeleteFilterFromJSON;
function RefundsDeleteFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'refundId': !runtime_1.exists(json, 'refundId') ? undefined : json['refundId'],
        'extRefundId': !runtime_1.exists(json, 'extRefundId') ? undefined : json['extRefundId'],
        'extReturnId': !runtime_1.exists(json, 'extReturnId') ? undefined : json['extReturnId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'type': !runtime_1.exists(json, 'type') ? undefined : (json['type'].map(_1.RefundTypeFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.RefundStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.RefundsDeleteFilterFromJSONTyped = RefundsDeleteFilterFromJSONTyped;
function RefundsDeleteFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'refundId': value.refundId,
        'extRefundId': value.extRefundId,
        'extReturnId': value.extReturnId,
        'extOrderId': value.extOrderId,
        'type': value.type === undefined ? undefined : (value.type.map(_1.RefundTypeToJSON)),
        'status': value.status === undefined ? undefined : (value.status.map(_1.RefundStatusToJSON)),
        'deleted': value.deleted,
        'integration': value.integration,
    };
}
exports.RefundsDeleteFilterToJSON = RefundsDeleteFilterToJSON;
