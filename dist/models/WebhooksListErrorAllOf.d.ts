/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhooksListFilter } from './';
/**
 *
 * @export
 * @interface WebhooksListErrorAllOf
 */
export interface WebhooksListErrorAllOf {
    /**
     *
     * @type {WebhooksListFilter}
     * @memberof WebhooksListErrorAllOf
     */
    filters?: WebhooksListFilter;
}
export declare function WebhooksListErrorAllOfFromJSON(json: any): WebhooksListErrorAllOf;
export declare function WebhooksListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhooksListErrorAllOf;
export declare function WebhooksListErrorAllOfToJSON(value?: WebhooksListErrorAllOf | null): any;
