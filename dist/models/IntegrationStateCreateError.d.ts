/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStateCreateData } from './';
/**
 *
 * @export
 * @interface IntegrationStateCreateError
 */
export interface IntegrationStateCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationStateCreateError
     */
    success: IntegrationStateCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationStateCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationStateCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationStateCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {IntegrationStateCreateData}
     * @memberof IntegrationStateCreateError
     */
    integrationState?: IntegrationStateCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationStateCreateErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationStateCreateErrorFromJSON(json: any): IntegrationStateCreateError;
export declare function IntegrationStateCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateCreateError;
export declare function IntegrationStateCreateErrorToJSON(value?: IntegrationStateCreateError | null): any;
