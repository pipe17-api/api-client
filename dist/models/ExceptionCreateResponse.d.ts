/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Exception } from './';
/**
 *
 * @export
 * @interface ExceptionCreateResponse
 */
export interface ExceptionCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionCreateResponse
     */
    success: ExceptionCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionCreateResponse
     */
    code: ExceptionCreateResponseCodeEnum;
    /**
     *
     * @type {Exception}
     * @memberof ExceptionCreateResponse
     */
    exception?: Exception;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionCreateResponseFromJSON(json: any): ExceptionCreateResponse;
export declare function ExceptionCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionCreateResponse;
export declare function ExceptionCreateResponseToJSON(value?: ExceptionCreateResponse | null): any;
