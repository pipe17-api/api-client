"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryUpdateErrorAllOfToJSON = exports.InventoryUpdateErrorAllOfFromJSONTyped = exports.InventoryUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryUpdateErrorAllOfFromJSON(json) {
    return InventoryUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.InventoryUpdateErrorAllOfFromJSON = InventoryUpdateErrorAllOfFromJSON;
function InventoryUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : _1.InventoryUpdateDataFromJSON(json['inventory']),
    };
}
exports.InventoryUpdateErrorAllOfFromJSONTyped = InventoryUpdateErrorAllOfFromJSONTyped;
function InventoryUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventory': _1.InventoryUpdateDataToJSON(value.inventory),
    };
}
exports.InventoryUpdateErrorAllOfToJSON = InventoryUpdateErrorAllOfToJSON;
