"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierUpdateResponseToJSON = exports.SupplierUpdateResponseFromJSONTyped = exports.SupplierUpdateResponseFromJSON = exports.SupplierUpdateResponseCodeEnum = exports.SupplierUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var SupplierUpdateResponseSuccessEnum;
(function (SupplierUpdateResponseSuccessEnum) {
    SupplierUpdateResponseSuccessEnum["True"] = "true";
})(SupplierUpdateResponseSuccessEnum = exports.SupplierUpdateResponseSuccessEnum || (exports.SupplierUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var SupplierUpdateResponseCodeEnum;
(function (SupplierUpdateResponseCodeEnum) {
    SupplierUpdateResponseCodeEnum[SupplierUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    SupplierUpdateResponseCodeEnum[SupplierUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    SupplierUpdateResponseCodeEnum[SupplierUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(SupplierUpdateResponseCodeEnum = exports.SupplierUpdateResponseCodeEnum || (exports.SupplierUpdateResponseCodeEnum = {}));
function SupplierUpdateResponseFromJSON(json) {
    return SupplierUpdateResponseFromJSONTyped(json, false);
}
exports.SupplierUpdateResponseFromJSON = SupplierUpdateResponseFromJSON;
function SupplierUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.SupplierUpdateResponseFromJSONTyped = SupplierUpdateResponseFromJSONTyped;
function SupplierUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.SupplierUpdateResponseToJSON = SupplierUpdateResponseToJSON;
