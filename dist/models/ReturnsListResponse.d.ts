/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Return, ReturnsFilter } from './';
/**
 *
 * @export
 * @interface ReturnsListResponse
 */
export interface ReturnsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReturnsListResponse
     */
    success: ReturnsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnsListResponse
     */
    code: ReturnsListResponseCodeEnum;
    /**
     *
     * @type {ReturnsFilter}
     * @memberof ReturnsListResponse
     */
    filters?: ReturnsFilter;
    /**
     *
     * @type {Array<Return>}
     * @memberof ReturnsListResponse
     */
    returns?: Array<Return>;
    /**
     *
     * @type {Pagination}
     * @memberof ReturnsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReturnsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReturnsListResponseFromJSON(json: any): ReturnsListResponse;
export declare function ReturnsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsListResponse;
export declare function ReturnsListResponseToJSON(value?: ReturnsListResponse | null): any;
