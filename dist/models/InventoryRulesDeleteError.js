"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRulesDeleteErrorToJSON = exports.InventoryRulesDeleteErrorFromJSONTyped = exports.InventoryRulesDeleteErrorFromJSON = exports.InventoryRulesDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryRulesDeleteErrorSuccessEnum;
(function (InventoryRulesDeleteErrorSuccessEnum) {
    InventoryRulesDeleteErrorSuccessEnum["False"] = "false";
})(InventoryRulesDeleteErrorSuccessEnum = exports.InventoryRulesDeleteErrorSuccessEnum || (exports.InventoryRulesDeleteErrorSuccessEnum = {}));
function InventoryRulesDeleteErrorFromJSON(json) {
    return InventoryRulesDeleteErrorFromJSONTyped(json, false);
}
exports.InventoryRulesDeleteErrorFromJSON = InventoryRulesDeleteErrorFromJSON;
function InventoryRulesDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryRulesDeleteFilterFromJSON(json['filters']),
    };
}
exports.InventoryRulesDeleteErrorFromJSONTyped = InventoryRulesDeleteErrorFromJSONTyped;
function InventoryRulesDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.InventoryRulesDeleteFilterToJSON(value.filters),
    };
}
exports.InventoryRulesDeleteErrorToJSON = InventoryRulesDeleteErrorToJSON;
