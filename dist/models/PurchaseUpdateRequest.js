"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseUpdateRequestToJSON = exports.PurchaseUpdateRequestFromJSONTyped = exports.PurchaseUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchaseUpdateRequestFromJSON(json) {
    return PurchaseUpdateRequestFromJSONTyped(json, false);
}
exports.PurchaseUpdateRequestFromJSON = PurchaseUpdateRequestFromJSON;
function PurchaseUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.PurchaseStatusFromJSON(json['status']),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.PurchaseUpdateLineItemFromJSON)),
        'subTotalPrice': !runtime_1.exists(json, 'subTotalPrice') ? undefined : json['subTotalPrice'],
        'purchaseTax': !runtime_1.exists(json, 'purchaseTax') ? undefined : json['purchaseTax'],
        'totalPrice': !runtime_1.exists(json, 'totalPrice') ? undefined : json['totalPrice'],
        'costs': !runtime_1.exists(json, 'costs') ? undefined : (json['costs'] === null ? null : json['costs'].map(_1.PurchaseCostFromJSON)),
        'employeeName': !runtime_1.exists(json, 'employeeName') ? undefined : json['employeeName'],
        'purchaseNotes': !runtime_1.exists(json, 'purchaseNotes') ? undefined : json['purchaseNotes'],
        'vendorAddress': !runtime_1.exists(json, 'vendorAddress') ? undefined : _1.AddressUpdateDataFromJSON(json['vendorAddress']),
        'toAddress': !runtime_1.exists(json, 'toAddress') ? undefined : _1.AddressNullableFromJSON(json['toAddress']),
        'toLocationId': !runtime_1.exists(json, 'toLocationId') ? undefined : json['toLocationId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingService': !runtime_1.exists(json, 'shippingService') ? undefined : json['shippingService'],
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (json['shipByDate'] === null ? null : new Date(json['shipByDate'])),
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (json['actualArrivalDate'] === null ? null : new Date(json['actualArrivalDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (json['expectedArrivalDate'] === null ? null : new Date(json['expectedArrivalDate'])),
        'shippingNotes': !runtime_1.exists(json, 'shippingNotes') ? undefined : json['shippingNotes'],
        'billingAddress': !runtime_1.exists(json, 'billingAddress') ? undefined : _1.AddressUpdateDataFromJSON(json['billingAddress']),
        'referenceNumber': !runtime_1.exists(json, 'referenceNumber') ? undefined : json['referenceNumber'],
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (json['timestamp'] === null ? null : new Date(json['timestamp'])),
    };
}
exports.PurchaseUpdateRequestFromJSONTyped = PurchaseUpdateRequestFromJSONTyped;
function PurchaseUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.PurchaseStatusToJSON(value.status),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.PurchaseUpdateLineItemToJSON)),
        'subTotalPrice': value.subTotalPrice,
        'purchaseTax': value.purchaseTax,
        'totalPrice': value.totalPrice,
        'costs': value.costs === undefined ? undefined : (value.costs === null ? null : value.costs.map(_1.PurchaseCostToJSON)),
        'employeeName': value.employeeName,
        'purchaseNotes': value.purchaseNotes,
        'vendorAddress': _1.AddressUpdateDataToJSON(value.vendorAddress),
        'toAddress': _1.AddressNullableToJSON(value.toAddress),
        'toLocationId': value.toLocationId,
        'shippingCarrier': value.shippingCarrier,
        'shippingService': value.shippingService,
        'shipByDate': value.shipByDate === undefined ? undefined : (value.shipByDate === null ? null : new Date(value.shipByDate).toISOString()),
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (value.actualArrivalDate === null ? null : new Date(value.actualArrivalDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (value.expectedArrivalDate === null ? null : new Date(value.expectedArrivalDate).toISOString()),
        'shippingNotes': value.shippingNotes,
        'billingAddress': _1.AddressUpdateDataToJSON(value.billingAddress),
        'referenceNumber': value.referenceNumber,
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'timestamp': value.timestamp === undefined ? undefined : (value.timestamp === null ? null : new Date(value.timestamp).toISOString()),
    };
}
exports.PurchaseUpdateRequestToJSON = PurchaseUpdateRequestToJSON;
