"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventUpdateDataToJSON = exports.EventUpdateDataFromJSONTyped = exports.EventUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EventUpdateDataFromJSON(json) {
    return EventUpdateDataFromJSONTyped(json, false);
}
exports.EventUpdateDataFromJSON = EventUpdateDataFromJSON;
function EventUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.EventRequestStatusFromJSON(json['status']),
        'response': !runtime_1.exists(json, 'response') ? undefined : _1.EventResponseDataFromJSON(json['response']),
        'duration': !runtime_1.exists(json, 'duration') ? undefined : json['duration'],
    };
}
exports.EventUpdateDataFromJSONTyped = EventUpdateDataFromJSONTyped;
function EventUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.EventRequestStatusToJSON(value.status),
        'response': _1.EventResponseDataToJSON(value.response),
        'duration': value.duration,
    };
}
exports.EventUpdateDataToJSON = EventUpdateDataToJSON;
