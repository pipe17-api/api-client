"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountCreateRequestToJSON = exports.AccountCreateRequestFromJSONTyped = exports.AccountCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountCreateRequestFromJSON(json) {
    return AccountCreateRequestFromJSONTyped(json, false);
}
exports.AccountCreateRequestFromJSON = AccountCreateRequestFromJSON;
function AccountCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'email': json['email'],
        'ownerFirstName': json['ownerFirstName'],
        'ownerLastName': json['ownerLastName'],
        'accountName': !runtime_1.exists(json, 'accountName') ? undefined : json['accountName'],
        'companyName': !runtime_1.exists(json, 'companyName') ? undefined : json['companyName'],
        'phone': json['phone'],
        'timeZone': json['timeZone'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'address': _1.AccountAddressFromJSON(json['address']),
        'integrations': !runtime_1.exists(json, 'integrations') ? undefined : json['integrations'],
        'payment': !runtime_1.exists(json, 'payment') ? undefined : json['payment'],
        'password': json['password'],
    };
}
exports.AccountCreateRequestFromJSONTyped = AccountCreateRequestFromJSONTyped;
function AccountCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'email': value.email,
        'ownerFirstName': value.ownerFirstName,
        'ownerLastName': value.ownerLastName,
        'accountName': value.accountName,
        'companyName': value.companyName,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'logoUrl': value.logoUrl,
        'address': _1.AccountAddressToJSON(value.address),
        'integrations': value.integrations,
        'payment': value.payment,
        'password': value.password,
    };
}
exports.AccountCreateRequestToJSON = AccountCreateRequestToJSON;
