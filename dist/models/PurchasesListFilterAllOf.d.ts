/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Purchases Filter
 * @export
 * @interface PurchasesListFilterAllOf
 */
export interface PurchasesListFilterAllOf {
    /**
     * if set to true use timestamp instead of create time
     * @type {boolean}
     * @memberof PurchasesListFilterAllOf
     */
    timestamp?: boolean;
}
export declare function PurchasesListFilterAllOfFromJSON(json: any): PurchasesListFilterAllOf;
export declare function PurchasesListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesListFilterAllOf;
export declare function PurchasesListFilterAllOfToJSON(value?: PurchasesListFilterAllOf | null): any;
