"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobsFilterAllOfToJSON = exports.JobsFilterAllOfFromJSONTyped = exports.JobsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobsFilterAllOfFromJSON(json) {
    return JobsFilterAllOfFromJSONTyped(json, false);
}
exports.JobsFilterAllOfFromJSON = JobsFilterAllOfFromJSON;
function JobsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'jobId': !runtime_1.exists(json, 'jobId') ? undefined : json['jobId'],
        'type': !runtime_1.exists(json, 'type') ? undefined : (json['type'].map(_1.JobTypeFromJSON)),
        'subType': !runtime_1.exists(json, 'subType') ? undefined : (json['subType'].map(_1.JobSubTypeFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.JobStatusFromJSON)),
    };
}
exports.JobsFilterAllOfFromJSONTyped = JobsFilterAllOfFromJSONTyped;
function JobsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'jobId': value.jobId,
        'type': value.type === undefined ? undefined : (value.type.map(_1.JobTypeToJSON)),
        'subType': value.subType === undefined ? undefined : (value.subType.map(_1.JobSubTypeToJSON)),
        'status': value.status === undefined ? undefined : (value.status.map(_1.JobStatusToJSON)),
    };
}
exports.JobsFilterAllOfToJSON = JobsFilterAllOfToJSON;
