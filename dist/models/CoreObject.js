"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.CoreObjectToJSON = exports.CoreObjectFromJSONTyped = exports.CoreObjectFromJSON = void 0;
const runtime_1 = require("../runtime");
function CoreObjectFromJSON(json) {
    return CoreObjectFromJSONTyped(json, false);
}
exports.CoreObjectFromJSON = CoreObjectFromJSON;
function CoreObjectFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.CoreObjectFromJSONTyped = CoreObjectFromJSONTyped;
function CoreObjectToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {};
}
exports.CoreObjectToJSON = CoreObjectToJSON;
