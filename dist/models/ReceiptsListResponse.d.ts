/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Receipt, ReceiptsListFilter } from './';
/**
 *
 * @export
 * @interface ReceiptsListResponse
 */
export interface ReceiptsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReceiptsListResponse
     */
    success: ReceiptsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReceiptsListResponse
     */
    code: ReceiptsListResponseCodeEnum;
    /**
     *
     * @type {ReceiptsListFilter}
     * @memberof ReceiptsListResponse
     */
    filters?: ReceiptsListFilter;
    /**
     *
     * @type {Array<Receipt>}
     * @memberof ReceiptsListResponse
     */
    receipts: Array<Receipt>;
    /**
     *
     * @type {Pagination}
     * @memberof ReceiptsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReceiptsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReceiptsListResponseFromJSON(json: any): ReceiptsListResponse;
export declare function ReceiptsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsListResponse;
export declare function ReceiptsListResponseToJSON(value?: ReceiptsListResponse | null): any;
