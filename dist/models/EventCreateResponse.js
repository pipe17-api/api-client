"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventCreateResponseToJSON = exports.EventCreateResponseFromJSONTyped = exports.EventCreateResponseFromJSON = exports.EventCreateResponseCodeEnum = exports.EventCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EventCreateResponseSuccessEnum;
(function (EventCreateResponseSuccessEnum) {
    EventCreateResponseSuccessEnum["True"] = "true";
})(EventCreateResponseSuccessEnum = exports.EventCreateResponseSuccessEnum || (exports.EventCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EventCreateResponseCodeEnum;
(function (EventCreateResponseCodeEnum) {
    EventCreateResponseCodeEnum[EventCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EventCreateResponseCodeEnum[EventCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EventCreateResponseCodeEnum[EventCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EventCreateResponseCodeEnum = exports.EventCreateResponseCodeEnum || (exports.EventCreateResponseCodeEnum = {}));
function EventCreateResponseFromJSON(json) {
    return EventCreateResponseFromJSONTyped(json, false);
}
exports.EventCreateResponseFromJSON = EventCreateResponseFromJSON;
function EventCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'event': !runtime_1.exists(json, 'event') ? undefined : _1.EventFromJSON(json['event']),
    };
}
exports.EventCreateResponseFromJSONTyped = EventCreateResponseFromJSONTyped;
function EventCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'event': _1.EventToJSON(value.event),
    };
}
exports.EventCreateResponseToJSON = EventCreateResponseToJSON;
