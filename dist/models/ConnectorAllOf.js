"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorAllOfToJSON = exports.ConnectorAllOfFromJSONTyped = exports.ConnectorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConnectorAllOfFromJSON(json) {
    return ConnectorAllOfFromJSONTyped(json, false);
}
exports.ConnectorAllOfFromJSON = ConnectorAllOfFromJSON;
function ConnectorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ConnectorStatusFromJSON(json['status']),
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'rank': !runtime_1.exists(json, 'rank') ? undefined : json['rank'],
    };
}
exports.ConnectorAllOfFromJSONTyped = ConnectorAllOfFromJSONTyped;
function ConnectorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'connectorId': value.connectorId,
        'status': _1.ConnectorStatusToJSON(value.status),
        'isPublic': value.isPublic,
        'rank': value.rank,
    };
}
exports.ConnectorAllOfToJSON = ConnectorAllOfToJSON;
