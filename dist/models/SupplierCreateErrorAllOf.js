"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierCreateErrorAllOfToJSON = exports.SupplierCreateErrorAllOfFromJSONTyped = exports.SupplierCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SupplierCreateErrorAllOfFromJSON(json) {
    return SupplierCreateErrorAllOfFromJSONTyped(json, false);
}
exports.SupplierCreateErrorAllOfFromJSON = SupplierCreateErrorAllOfFromJSON;
function SupplierCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierCreateDataFromJSON(json['supplier']),
    };
}
exports.SupplierCreateErrorAllOfFromJSONTyped = SupplierCreateErrorAllOfFromJSONTyped;
function SupplierCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'supplier': _1.SupplierCreateDataToJSON(value.supplier),
    };
}
exports.SupplierCreateErrorAllOfToJSON = SupplierCreateErrorAllOfToJSON;
