"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsListResponseAllOfToJSON = exports.LocationsListResponseAllOfFromJSONTyped = exports.LocationsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationsListResponseAllOfFromJSON(json) {
    return LocationsListResponseAllOfFromJSONTyped(json, false);
}
exports.LocationsListResponseAllOfFromJSON = LocationsListResponseAllOfFromJSON;
function LocationsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsListFilterFromJSON(json['filters']),
        'locations': !runtime_1.exists(json, 'locations') ? undefined : (json['locations'].map(_1.LocationFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.LocationsListResponseAllOfFromJSONTyped = LocationsListResponseAllOfFromJSONTyped;
function LocationsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LocationsListFilterToJSON(value.filters),
        'locations': value.locations === undefined ? undefined : (value.locations.map(_1.LocationToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.LocationsListResponseAllOfToJSON = LocationsListResponseAllOfToJSON;
