/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Purchase } from './';
/**
 *
 * @export
 * @interface PurchaseCreateResult
 */
export interface PurchaseCreateResult {
    /**
     *
     * @type {string}
     * @memberof PurchaseCreateResult
     */
    status?: PurchaseCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof PurchaseCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof PurchaseCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Purchase}
     * @memberof PurchaseCreateResult
     */
    purchase?: Purchase;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchaseCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function PurchaseCreateResultFromJSON(json: any): PurchaseCreateResult;
export declare function PurchaseCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseCreateResult;
export declare function PurchaseCreateResultToJSON(value?: PurchaseCreateResult | null): any;
