/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentFetchError
 */
export interface FulfillmentFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof FulfillmentFetchError
     */
    success: FulfillmentFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof FulfillmentFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof FulfillmentFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof FulfillmentFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentFetchErrorSuccessEnum {
    False = "false"
}
export declare function FulfillmentFetchErrorFromJSON(json: any): FulfillmentFetchError;
export declare function FulfillmentFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentFetchError;
export declare function FulfillmentFetchErrorToJSON(value?: FulfillmentFetchError | null): any;
