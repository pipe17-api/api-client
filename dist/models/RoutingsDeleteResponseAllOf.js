"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsDeleteResponseAllOfToJSON = exports.RoutingsDeleteResponseAllOfFromJSONTyped = exports.RoutingsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoutingsDeleteResponseAllOfFromJSON(json) {
    return RoutingsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.RoutingsDeleteResponseAllOfFromJSON = RoutingsDeleteResponseAllOfFromJSON;
function RoutingsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RoutingsDeleteFilterFromJSON(json['filters']),
        'routings': !runtime_1.exists(json, 'routings') ? undefined : (json['routings'].map(_1.RoutingFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RoutingsDeleteResponseAllOfFromJSONTyped = RoutingsDeleteResponseAllOfFromJSONTyped;
function RoutingsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RoutingsDeleteFilterToJSON(value.filters),
        'routings': value.routings === undefined ? undefined : (value.routings.map(_1.RoutingToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RoutingsDeleteResponseAllOfToJSON = RoutingsDeleteResponseAllOfToJSON;
