/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentsListFilter } from './';
/**
 *
 * @export
 * @interface FulfillmentsListError
 */
export interface FulfillmentsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof FulfillmentsListError
     */
    success: FulfillmentsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof FulfillmentsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof FulfillmentsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof FulfillmentsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {FulfillmentsListFilter}
     * @memberof FulfillmentsListError
     */
    filters?: FulfillmentsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentsListErrorSuccessEnum {
    False = "false"
}
export declare function FulfillmentsListErrorFromJSON(json: any): FulfillmentsListError;
export declare function FulfillmentsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsListError;
export declare function FulfillmentsListErrorToJSON(value?: FulfillmentsListError | null): any;
