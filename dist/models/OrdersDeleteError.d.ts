/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrdersDeleteFilter } from './';
/**
 *
 * @export
 * @interface OrdersDeleteError
 */
export interface OrdersDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrdersDeleteError
     */
    success: OrdersDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrdersDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrdersDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrdersDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrdersDeleteFilter}
     * @memberof OrdersDeleteError
     */
    filters?: OrdersDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum OrdersDeleteErrorSuccessEnum {
    False = "false"
}
export declare function OrdersDeleteErrorFromJSON(json: any): OrdersDeleteError;
export declare function OrdersDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersDeleteError;
export declare function OrdersDeleteErrorToJSON(value?: OrdersDeleteError | null): any;
