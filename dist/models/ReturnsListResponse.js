"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsListResponseToJSON = exports.ReturnsListResponseFromJSONTyped = exports.ReturnsListResponseFromJSON = exports.ReturnsListResponseCodeEnum = exports.ReturnsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReturnsListResponseSuccessEnum;
(function (ReturnsListResponseSuccessEnum) {
    ReturnsListResponseSuccessEnum["True"] = "true";
})(ReturnsListResponseSuccessEnum = exports.ReturnsListResponseSuccessEnum || (exports.ReturnsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReturnsListResponseCodeEnum;
(function (ReturnsListResponseCodeEnum) {
    ReturnsListResponseCodeEnum[ReturnsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReturnsListResponseCodeEnum[ReturnsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReturnsListResponseCodeEnum[ReturnsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReturnsListResponseCodeEnum = exports.ReturnsListResponseCodeEnum || (exports.ReturnsListResponseCodeEnum = {}));
function ReturnsListResponseFromJSON(json) {
    return ReturnsListResponseFromJSONTyped(json, false);
}
exports.ReturnsListResponseFromJSON = ReturnsListResponseFromJSON;
function ReturnsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReturnsFilterFromJSON(json['filters']),
        'returns': !runtime_1.exists(json, 'returns') ? undefined : (json['returns'].map(_1.ReturnFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ReturnsListResponseFromJSONTyped = ReturnsListResponseFromJSONTyped;
function ReturnsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ReturnsFilterToJSON(value.filters),
        'returns': value.returns === undefined ? undefined : (value.returns.map(_1.ReturnToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ReturnsListResponseToJSON = ReturnsListResponseToJSON;
