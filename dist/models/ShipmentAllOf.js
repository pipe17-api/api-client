"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentAllOfToJSON = exports.ShipmentAllOfFromJSONTyped = exports.ShipmentAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentAllOfFromJSON(json) {
    return ShipmentAllOfFromJSONTyped(json, false);
}
exports.ShipmentAllOfFromJSON = ShipmentAllOfFromJSON;
function ShipmentAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ShipmentStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'sentToFulfillmentAt': !runtime_1.exists(json, 'sentToFulfillmentAt') ? undefined : (new Date(json['sentToFulfillmentAt'])),
    };
}
exports.ShipmentAllOfFromJSONTyped = ShipmentAllOfFromJSONTyped;
function ShipmentAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipmentId': value.shipmentId,
        'status': _1.ShipmentStatusToJSON(value.status),
        'integration': value.integration,
        'sentToFulfillmentAt': value.sentToFulfillmentAt === undefined ? undefined : (new Date(value.sentToFulfillmentAt).toISOString()),
    };
}
exports.ShipmentAllOfToJSON = ShipmentAllOfToJSON;
