/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderUpdateResponse
 */
export interface OrderUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrderUpdateResponse
     */
    success: OrderUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderUpdateResponse
     */
    code: OrderUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrderUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrderUpdateResponseFromJSON(json: any): OrderUpdateResponse;
export declare function OrderUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderUpdateResponse;
export declare function OrderUpdateResponseToJSON(value?: OrderUpdateResponse | null): any;
