/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorConnection, ConnectorEntity, ConnectorSettings, ConnectorStatus, ConnectorType, ConnectorWebhook } from './';
/**
 *
 * @export
 * @interface Connector
 */
export interface Connector {
    /**
     * Connector ID
     * @type {string}
     * @memberof Connector
     */
    connectorId?: string;
    /**
     *
     * @type {ConnectorStatus}
     * @memberof Connector
     */
    status?: ConnectorStatus;
    /**
     * Public connector indicator
     * @type {boolean}
     * @memberof Connector
     */
    isPublic?: boolean;
    /**
     * Connector rank in category (higher ranks indicate better match)
     * @type {number}
     * @memberof Connector
     */
    rank?: number;
    /**
     * Connector description
     * @type {string}
     * @memberof Connector
     */
    description?: string;
    /**
     * Connector readme markup
     * @type {string}
     * @memberof Connector
     */
    readme?: string;
    /**
     * Connector version
     * @type {string}
     * @memberof Connector
     */
    version?: string;
    /**
     * Connector Name
     * @type {string}
     * @memberof Connector
     */
    connectorName: string;
    /**
     *
     * @type {ConnectorType}
     * @memberof Connector
     */
    connectorType: ConnectorType;
    /**
     * Logo URL
     * @type {string}
     * @memberof Connector
     */
    logoUrl?: string;
    /**
     *
     * @type {Array<ConnectorEntity>}
     * @memberof Connector
     */
    entities?: Array<ConnectorEntity>;
    /**
     *
     * @type {ConnectorSettings}
     * @memberof Connector
     */
    settings?: ConnectorSettings;
    /**
     *
     * @type {ConnectorConnection}
     * @memberof Connector
     */
    connection?: ConnectorConnection;
    /**
     *
     * @type {ConnectorWebhook}
     * @memberof Connector
     */
    webhook?: ConnectorWebhook;
    /**
     * Indicates whether connector/integration should have access to all declared entity types
     * @type {boolean}
     * @memberof Connector
     */
    orgWideAccess?: boolean;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof Connector
     */
    orgUnique?: boolean;
    /**
     * Connector Display Name
     * @type {string}
     * @memberof Connector
     */
    displayName?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Connector
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Connector
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Connector
     */
    readonly orgKey?: string;
}
export declare function ConnectorFromJSON(json: any): Connector;
export declare function ConnectorFromJSONTyped(json: any, ignoreDiscriminator: boolean): Connector;
export declare function ConnectorToJSON(value?: Connector | null): any;
