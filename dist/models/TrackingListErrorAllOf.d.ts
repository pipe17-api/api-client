/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingFilter } from './';
/**
 *
 * @export
 * @interface TrackingListErrorAllOf
 */
export interface TrackingListErrorAllOf {
    /**
     *
     * @type {TrackingFilter}
     * @memberof TrackingListErrorAllOf
     */
    filters?: TrackingFilter;
}
export declare function TrackingListErrorAllOfFromJSON(json: any): TrackingListErrorAllOf;
export declare function TrackingListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingListErrorAllOf;
export declare function TrackingListErrorAllOfToJSON(value?: TrackingListErrorAllOf | null): any;
