/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderUpdateData } from './';
/**
 *
 * @export
 * @interface OrderUpdateError
 */
export interface OrderUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrderUpdateError
     */
    success: OrderUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrderUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrderUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrderUpdateData}
     * @memberof OrderUpdateError
     */
    order?: OrderUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderUpdateErrorSuccessEnum {
    False = "false"
}
export declare function OrderUpdateErrorFromJSON(json: any): OrderUpdateError;
export declare function OrderUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderUpdateError;
export declare function OrderUpdateErrorToJSON(value?: OrderUpdateError | null): any;
