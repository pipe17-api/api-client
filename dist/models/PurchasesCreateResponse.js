"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesCreateResponseToJSON = exports.PurchasesCreateResponseFromJSONTyped = exports.PurchasesCreateResponseFromJSON = exports.PurchasesCreateResponseCodeEnum = exports.PurchasesCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchasesCreateResponseSuccessEnum;
(function (PurchasesCreateResponseSuccessEnum) {
    PurchasesCreateResponseSuccessEnum["True"] = "true";
})(PurchasesCreateResponseSuccessEnum = exports.PurchasesCreateResponseSuccessEnum || (exports.PurchasesCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var PurchasesCreateResponseCodeEnum;
(function (PurchasesCreateResponseCodeEnum) {
    PurchasesCreateResponseCodeEnum[PurchasesCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    PurchasesCreateResponseCodeEnum[PurchasesCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    PurchasesCreateResponseCodeEnum[PurchasesCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(PurchasesCreateResponseCodeEnum = exports.PurchasesCreateResponseCodeEnum || (exports.PurchasesCreateResponseCodeEnum = {}));
function PurchasesCreateResponseFromJSON(json) {
    return PurchasesCreateResponseFromJSONTyped(json, false);
}
exports.PurchasesCreateResponseFromJSON = PurchasesCreateResponseFromJSON;
function PurchasesCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'purchases': !runtime_1.exists(json, 'purchases') ? undefined : (json['purchases'].map(_1.PurchaseCreateResultFromJSON)),
    };
}
exports.PurchasesCreateResponseFromJSONTyped = PurchasesCreateResponseFromJSONTyped;
function PurchasesCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'purchases': value.purchases === undefined ? undefined : (value.purchases.map(_1.PurchaseCreateResultToJSON)),
    };
}
exports.PurchasesCreateResponseToJSON = PurchasesCreateResponseToJSON;
