/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStateCreateData } from './';
/**
 *
 * @export
 * @interface IntegrationStateCreateErrorAllOf
 */
export interface IntegrationStateCreateErrorAllOf {
    /**
     *
     * @type {IntegrationStateCreateData}
     * @memberof IntegrationStateCreateErrorAllOf
     */
    integrationState?: IntegrationStateCreateData;
}
export declare function IntegrationStateCreateErrorAllOfFromJSON(json: any): IntegrationStateCreateErrorAllOf;
export declare function IntegrationStateCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateCreateErrorAllOf;
export declare function IntegrationStateCreateErrorAllOfToJSON(value?: IntegrationStateCreateErrorAllOf | null): any;
