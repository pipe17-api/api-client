"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseFetchErrorToJSON = exports.PurchaseFetchErrorFromJSONTyped = exports.PurchaseFetchErrorFromJSON = exports.PurchaseFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var PurchaseFetchErrorSuccessEnum;
(function (PurchaseFetchErrorSuccessEnum) {
    PurchaseFetchErrorSuccessEnum["False"] = "false";
})(PurchaseFetchErrorSuccessEnum = exports.PurchaseFetchErrorSuccessEnum || (exports.PurchaseFetchErrorSuccessEnum = {}));
function PurchaseFetchErrorFromJSON(json) {
    return PurchaseFetchErrorFromJSONTyped(json, false);
}
exports.PurchaseFetchErrorFromJSON = PurchaseFetchErrorFromJSON;
function PurchaseFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.PurchaseFetchErrorFromJSONTyped = PurchaseFetchErrorFromJSONTyped;
function PurchaseFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.PurchaseFetchErrorToJSON = PurchaseFetchErrorToJSON;
