"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationsListResponseToJSON = exports.IntegrationsListResponseFromJSONTyped = exports.IntegrationsListResponseFromJSON = exports.IntegrationsListResponseCodeEnum = exports.IntegrationsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationsListResponseSuccessEnum;
(function (IntegrationsListResponseSuccessEnum) {
    IntegrationsListResponseSuccessEnum["True"] = "true";
})(IntegrationsListResponseSuccessEnum = exports.IntegrationsListResponseSuccessEnum || (exports.IntegrationsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationsListResponseCodeEnum;
(function (IntegrationsListResponseCodeEnum) {
    IntegrationsListResponseCodeEnum[IntegrationsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationsListResponseCodeEnum[IntegrationsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationsListResponseCodeEnum[IntegrationsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationsListResponseCodeEnum = exports.IntegrationsListResponseCodeEnum || (exports.IntegrationsListResponseCodeEnum = {}));
function IntegrationsListResponseFromJSON(json) {
    return IntegrationsListResponseFromJSONTyped(json, false);
}
exports.IntegrationsListResponseFromJSON = IntegrationsListResponseFromJSON;
function IntegrationsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationsListFilterFromJSON(json['filters']),
        'integrations': !runtime_1.exists(json, 'integrations') ? undefined : (json['integrations'].map(_1.IntegrationBaseFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.IntegrationsListResponseFromJSONTyped = IntegrationsListResponseFromJSONTyped;
function IntegrationsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.IntegrationsListFilterToJSON(value.filters),
        'integrations': value.integrations === undefined ? undefined : (value.integrations.map(_1.IntegrationBaseToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.IntegrationsListResponseToJSON = IntegrationsListResponseToJSON;
