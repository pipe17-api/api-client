"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsListResponseAllOfToJSON = exports.ReturnsListResponseAllOfFromJSONTyped = exports.ReturnsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnsListResponseAllOfFromJSON(json) {
    return ReturnsListResponseAllOfFromJSONTyped(json, false);
}
exports.ReturnsListResponseAllOfFromJSON = ReturnsListResponseAllOfFromJSON;
function ReturnsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReturnsFilterFromJSON(json['filters']),
        'returns': !runtime_1.exists(json, 'returns') ? undefined : (json['returns'].map(_1.ReturnFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ReturnsListResponseAllOfFromJSONTyped = ReturnsListResponseAllOfFromJSONTyped;
function ReturnsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ReturnsFilterToJSON(value.filters),
        'returns': value.returns === undefined ? undefined : (value.returns.map(_1.ReturnToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ReturnsListResponseAllOfToJSON = ReturnsListResponseAllOfToJSON;
