/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationCreateData } from './';
/**
 *
 * @export
 * @interface IntegrationCreateErrorAllOf
 */
export interface IntegrationCreateErrorAllOf {
    /**
     *
     * @type {IntegrationCreateData}
     * @memberof IntegrationCreateErrorAllOf
     */
    integration?: IntegrationCreateData;
}
export declare function IntegrationCreateErrorAllOfFromJSON(json: any): IntegrationCreateErrorAllOf;
export declare function IntegrationCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationCreateErrorAllOf;
export declare function IntegrationCreateErrorAllOfToJSON(value?: IntegrationCreateErrorAllOf | null): any;
