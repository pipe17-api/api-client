"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsListResponseAllOfToJSON = exports.RefundsListResponseAllOfFromJSONTyped = exports.RefundsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundsListResponseAllOfFromJSON(json) {
    return RefundsListResponseAllOfFromJSONTyped(json, false);
}
exports.RefundsListResponseAllOfFromJSON = RefundsListResponseAllOfFromJSON;
function RefundsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RefundsFilterFromJSON(json['filters']),
        'refunds': !runtime_1.exists(json, 'refunds') ? undefined : (json['refunds'].map(_1.RefundFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RefundsListResponseAllOfFromJSONTyped = RefundsListResponseAllOfFromJSONTyped;
function RefundsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RefundsFilterToJSON(value.filters),
        'refunds': value.refunds === undefined ? undefined : (value.refunds.map(_1.RefundToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RefundsListResponseAllOfToJSON = RefundsListResponseAllOfToJSON;
