/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorSettingsOptionsValues } from './';
/**
 *
 * @export
 * @interface ConnectorSettingsOptions
 */
export interface ConnectorSettingsOptions {
    /**
     * Option user readable label
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    label?: string;
    /**
     * Option initial variable value
     * @type {object}
     * @memberof ConnectorSettingsOptions
     */
    defaultValue?: object;
    /**
     * Option random initial value
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    genkey?: ConnectorSettingsOptionsGenkeyEnum;
    /**
     * Option group
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    group?: string;
    /**
     * Nested fields
     * @type {Array<object>}
     * @memberof ConnectorSettingsOptions
     */
    fields?: Array<object> | null;
    /**
     * Option user cannot edit value
     * @type {boolean}
     * @memberof ConnectorSettingsOptions
     */
    readonly?: boolean;
    /**
     * Option not displayed to user
     * @type {boolean}
     * @memberof ConnectorSettingsOptions
     */
    hidden?: boolean;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    startAdornment?: string;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    endAdornment?: string;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    helperText?: string;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    placeholder?: string;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    component?: string;
    /**
     *
     * @type {Array<ConnectorSettingsOptionsValues>}
     * @memberof ConnectorSettingsOptions
     */
    values?: Array<ConnectorSettingsOptionsValues>;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptions
     */
    readme?: string;
    /**
     *
     * @type {object}
     * @memberof ConnectorSettingsOptions
     */
    extra?: object;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorSettingsOptionsGenkeyEnum {
    Apikey = "apikey",
    Nonce = "nonce"
}
export declare function ConnectorSettingsOptionsFromJSON(json: any): ConnectorSettingsOptions;
export declare function ConnectorSettingsOptionsFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSettingsOptions;
export declare function ConnectorSettingsOptionsToJSON(value?: ConnectorSettingsOptions | null): any;
