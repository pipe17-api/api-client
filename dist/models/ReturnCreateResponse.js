"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnCreateResponseToJSON = exports.ReturnCreateResponseFromJSONTyped = exports.ReturnCreateResponseFromJSON = exports.ReturnCreateResponseCodeEnum = exports.ReturnCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReturnCreateResponseSuccessEnum;
(function (ReturnCreateResponseSuccessEnum) {
    ReturnCreateResponseSuccessEnum["True"] = "true";
})(ReturnCreateResponseSuccessEnum = exports.ReturnCreateResponseSuccessEnum || (exports.ReturnCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReturnCreateResponseCodeEnum;
(function (ReturnCreateResponseCodeEnum) {
    ReturnCreateResponseCodeEnum[ReturnCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReturnCreateResponseCodeEnum[ReturnCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReturnCreateResponseCodeEnum[ReturnCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReturnCreateResponseCodeEnum = exports.ReturnCreateResponseCodeEnum || (exports.ReturnCreateResponseCodeEnum = {}));
function ReturnCreateResponseFromJSON(json) {
    return ReturnCreateResponseFromJSONTyped(json, false);
}
exports.ReturnCreateResponseFromJSON = ReturnCreateResponseFromJSON;
function ReturnCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        '_return': !runtime_1.exists(json, 'return') ? undefined : _1.ReturnFromJSON(json['return']),
    };
}
exports.ReturnCreateResponseFromJSONTyped = ReturnCreateResponseFromJSONTyped;
function ReturnCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'return': _1.ReturnToJSON(value._return),
    };
}
exports.ReturnCreateResponseToJSON = ReturnCreateResponseToJSON;
