"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelsListResponseToJSON = exports.LabelsListResponseFromJSONTyped = exports.LabelsListResponseFromJSON = exports.LabelsListResponseCodeEnum = exports.LabelsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LabelsListResponseSuccessEnum;
(function (LabelsListResponseSuccessEnum) {
    LabelsListResponseSuccessEnum["True"] = "true";
})(LabelsListResponseSuccessEnum = exports.LabelsListResponseSuccessEnum || (exports.LabelsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LabelsListResponseCodeEnum;
(function (LabelsListResponseCodeEnum) {
    LabelsListResponseCodeEnum[LabelsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LabelsListResponseCodeEnum[LabelsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LabelsListResponseCodeEnum[LabelsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LabelsListResponseCodeEnum = exports.LabelsListResponseCodeEnum || (exports.LabelsListResponseCodeEnum = {}));
function LabelsListResponseFromJSON(json) {
    return LabelsListResponseFromJSONTyped(json, false);
}
exports.LabelsListResponseFromJSON = LabelsListResponseFromJSON;
function LabelsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LabelsListFilterFromJSON(json['filters']),
        'labels': !runtime_1.exists(json, 'labels') ? undefined : (json['labels'].map(_1.LabelFromJSON)),
    };
}
exports.LabelsListResponseFromJSONTyped = LabelsListResponseFromJSONTyped;
function LabelsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.LabelsListFilterToJSON(value.filters),
        'labels': value.labels === undefined ? undefined : (value.labels.map(_1.LabelToJSON)),
    };
}
exports.LabelsListResponseToJSON = LabelsListResponseToJSON;
