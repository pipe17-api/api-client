/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierAddress, SupplierContact } from './';
/**
 *
 * @export
 * @interface SupplierUpdateRequest
 */
export interface SupplierUpdateRequest {
    /**
     * Supplier Name
     * @type {string}
     * @memberof SupplierUpdateRequest
     */
    name?: string;
    /**
     *
     * @type {SupplierAddress}
     * @memberof SupplierUpdateRequest
     */
    address?: SupplierAddress;
    /**
     * Supplier Contacts
     * @type {Array<SupplierContact>}
     * @memberof SupplierUpdateRequest
     */
    contacts?: Array<SupplierContact>;
}
export declare function SupplierUpdateRequestFromJSON(json: any): SupplierUpdateRequest;
export declare function SupplierUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierUpdateRequest;
export declare function SupplierUpdateRequestToJSON(value?: SupplierUpdateRequest | null): any;
