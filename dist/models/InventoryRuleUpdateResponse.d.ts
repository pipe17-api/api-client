/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRuleUpdateResponse
 */
export interface InventoryRuleUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryRuleUpdateResponse
     */
    success: InventoryRuleUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRuleUpdateResponse
     */
    code: InventoryRuleUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRuleUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryRuleUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryRuleUpdateResponseFromJSON(json: any): InventoryRuleUpdateResponse;
export declare function InventoryRuleUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleUpdateResponse;
export declare function InventoryRuleUpdateResponseToJSON(value?: InventoryRuleUpdateResponse | null): any;
