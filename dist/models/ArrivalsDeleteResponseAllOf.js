"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsDeleteResponseAllOfToJSON = exports.ArrivalsDeleteResponseAllOfFromJSONTyped = exports.ArrivalsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalsDeleteResponseAllOfFromJSON(json) {
    return ArrivalsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.ArrivalsDeleteResponseAllOfFromJSON = ArrivalsDeleteResponseAllOfFromJSON;
function ArrivalsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ArrivalsDeleteFilterFromJSON(json['filters']),
        'arrivals': !runtime_1.exists(json, 'arrivals') ? undefined : (json['arrivals'].map(_1.ArrivalFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ArrivalsDeleteResponseAllOfFromJSONTyped = ArrivalsDeleteResponseAllOfFromJSONTyped;
function ArrivalsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ArrivalsDeleteFilterToJSON(value.filters),
        'arrivals': value.arrivals === undefined ? undefined : (value.arrivals.map(_1.ArrivalToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ArrivalsDeleteResponseAllOfToJSON = ArrivalsDeleteResponseAllOfToJSON;
