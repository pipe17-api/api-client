/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory, InventoryListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryListResponse
 */
export interface InventoryListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryListResponse
     */
    success: InventoryListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryListResponse
     */
    code: InventoryListResponseCodeEnum;
    /**
     *
     * @type {InventoryListFilter}
     * @memberof InventoryListResponse
     */
    filters?: InventoryListFilter;
    /**
     *
     * @type {Array<Inventory>}
     * @memberof InventoryListResponse
     */
    inventory?: Array<Inventory>;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryListResponseFromJSON(json: any): InventoryListResponse;
export declare function InventoryListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryListResponse;
export declare function InventoryListResponseToJSON(value?: InventoryListResponse | null): any;
