/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrganizationAddress
 */
export interface OrganizationAddress {
    /**
     * First Name
     * @type {string}
     * @memberof OrganizationAddress
     */
    firstName: string;
    /**
     * Last Name
     * @type {string}
     * @memberof OrganizationAddress
     */
    lastName: string;
    /**
     * Company
     * @type {string}
     * @memberof OrganizationAddress
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof OrganizationAddress
     */
    address1: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof OrganizationAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof OrganizationAddress
     */
    city: string;
    /**
     * State or Province
     * @type {string}
     * @memberof OrganizationAddress
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof OrganizationAddress
     */
    zipCodeOrPostalCode: string;
    /**
     * Country
     * @type {string}
     * @memberof OrganizationAddress
     */
    country: string;
}
export declare function OrganizationAddressFromJSON(json: any): OrganizationAddress;
export declare function OrganizationAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationAddress;
export declare function OrganizationAddressToJSON(value?: OrganizationAddress | null): any;
