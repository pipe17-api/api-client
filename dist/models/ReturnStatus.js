"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnStatusToJSON = exports.ReturnStatusFromJSONTyped = exports.ReturnStatusFromJSON = exports.ReturnStatus = void 0;
/**
 * Return status
 * @export
 * @enum {string}
 */
var ReturnStatus;
(function (ReturnStatus) {
    ReturnStatus["Authorized"] = "authorized";
    ReturnStatus["InTransit"] = "inTransit";
    ReturnStatus["Delivered"] = "delivered";
    ReturnStatus["DeliveredLate"] = "deliveredLate";
    ReturnStatus["Pending"] = "pending";
    ReturnStatus["Refunded"] = "refunded";
    ReturnStatus["RefundedExternally"] = "refundedExternally";
    ReturnStatus["Canceled"] = "canceled";
    ReturnStatus["Deleted"] = "deleted";
})(ReturnStatus = exports.ReturnStatus || (exports.ReturnStatus = {}));
function ReturnStatusFromJSON(json) {
    return ReturnStatusFromJSONTyped(json, false);
}
exports.ReturnStatusFromJSON = ReturnStatusFromJSON;
function ReturnStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ReturnStatusFromJSONTyped = ReturnStatusFromJSONTyped;
function ReturnStatusToJSON(value) {
    return value;
}
exports.ReturnStatusToJSON = ReturnStatusToJSON;
