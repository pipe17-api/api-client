/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RolesListFilter } from './';
/**
 *
 * @export
 * @interface RolesListError
 */
export interface RolesListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RolesListError
     */
    success: RolesListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RolesListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RolesListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RolesListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RolesListFilter}
     * @memberof RolesListError
     */
    filters?: RolesListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum RolesListErrorSuccessEnum {
    False = "false"
}
export declare function RolesListErrorFromJSON(json: any): RolesListError;
export declare function RolesListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesListError;
export declare function RolesListErrorToJSON(value?: RolesListError | null): any;
