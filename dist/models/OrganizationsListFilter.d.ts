/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationStatus } from './';
/**
 *
 * @export
 * @interface OrganizationsListFilter
 */
export interface OrganizationsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrganizationsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrganizationsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrganizationsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrganizationsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrganizationsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrganizationsListFilter
     */
    count?: number;
    /**
     * Organizations by list of orgKey
     * @type {Array<string>}
     * @memberof OrganizationsListFilter
     */
    orgKey?: Array<string>;
    /**
     * Organizations whose name contains this string
     * @type {string}
     * @memberof OrganizationsListFilter
     */
    name?: string;
    /**
     * Organizations whose phone number contains this string
     * @type {string}
     * @memberof OrganizationsListFilter
     */
    phone?: string;
    /**
     * Organizations whose email contains this string
     * @type {string}
     * @memberof OrganizationsListFilter
     */
    email?: string;
    /**
     * Organizations by list of statuses
     * @type {Array<OrganizationStatus>}
     * @memberof OrganizationsListFilter
     */
    status?: Array<OrganizationStatus>;
    /**
     * List sort order
     * @type {string}
     * @memberof OrganizationsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof OrganizationsListFilter
     */
    keys?: string;
}
export declare function OrganizationsListFilterFromJSON(json: any): OrganizationsListFilter;
export declare function OrganizationsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsListFilter;
export declare function OrganizationsListFilterToJSON(value?: OrganizationsListFilter | null): any;
