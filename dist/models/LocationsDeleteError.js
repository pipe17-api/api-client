"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsDeleteErrorToJSON = exports.LocationsDeleteErrorFromJSONTyped = exports.LocationsDeleteErrorFromJSON = exports.LocationsDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationsDeleteErrorSuccessEnum;
(function (LocationsDeleteErrorSuccessEnum) {
    LocationsDeleteErrorSuccessEnum["False"] = "false";
})(LocationsDeleteErrorSuccessEnum = exports.LocationsDeleteErrorSuccessEnum || (exports.LocationsDeleteErrorSuccessEnum = {}));
function LocationsDeleteErrorFromJSON(json) {
    return LocationsDeleteErrorFromJSONTyped(json, false);
}
exports.LocationsDeleteErrorFromJSON = LocationsDeleteErrorFromJSON;
function LocationsDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsDeleteFilterFromJSON(json['filters']),
    };
}
exports.LocationsDeleteErrorFromJSONTyped = LocationsDeleteErrorFromJSONTyped;
function LocationsDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.LocationsDeleteFilterToJSON(value.filters),
    };
}
exports.LocationsDeleteErrorToJSON = LocationsDeleteErrorToJSON;
