/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobStatus, JobSubType, JobType } from './';
/**
 *
 * @export
 * @interface JobsDeleteFilter
 */
export interface JobsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof JobsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof JobsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof JobsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof JobsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof JobsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof JobsDeleteFilter
     */
    count?: number;
    /**
     * Jobs by list of jobId
     * @type {Array<string>}
     * @memberof JobsDeleteFilter
     */
    jobId?: Array<string>;
    /**
     * Jobs with given type
     * @type {Array<JobType>}
     * @memberof JobsDeleteFilter
     */
    type?: Array<JobType>;
    /**
     * Jobs with given sub-type
     * @type {Array<JobSubType>}
     * @memberof JobsDeleteFilter
     */
    subType?: Array<JobSubType>;
    /**
     * Jobs with given status
     * @type {Array<JobStatus>}
     * @memberof JobsDeleteFilter
     */
    status?: Array<JobStatus>;
}
export declare function JobsDeleteFilterFromJSON(json: any): JobsDeleteFilter;
export declare function JobsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsDeleteFilter;
export declare function JobsDeleteFilterToJSON(value?: JobsDeleteFilter | null): any;
