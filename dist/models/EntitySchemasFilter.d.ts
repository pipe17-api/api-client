/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntitySchemasFilter
 */
export interface EntitySchemasFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof EntitySchemasFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof EntitySchemasFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof EntitySchemasFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof EntitySchemasFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof EntitySchemasFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof EntitySchemasFilter
     */
    count?: number;
    /**
     * Fetch by list of schemaId
     * @type {Array<string>}
     * @memberof EntitySchemasFilter
     */
    schemaId?: Array<string>;
    /**
     * Fetch by list of entity types
     * @type {Array<EntityName>}
     * @memberof EntitySchemasFilter
     */
    entity?: Array<EntityName>;
    /**
     * Fetch by list of schema names
     * @type {Array<string>}
     * @memberof EntitySchemasFilter
     */
    name?: Array<string>;
}
export declare function EntitySchemasFilterFromJSON(json: any): EntitySchemasFilter;
export declare function EntitySchemasFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemasFilter;
export declare function EntitySchemasFilterToJSON(value?: EntitySchemasFilter | null): any;
