/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Refund, RefundsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RefundsDeleteResponseAllOf
 */
export interface RefundsDeleteResponseAllOf {
    /**
     *
     * @type {RefundsDeleteFilter}
     * @memberof RefundsDeleteResponseAllOf
     */
    filters?: RefundsDeleteFilter;
    /**
     *
     * @type {Array<Refund>}
     * @memberof RefundsDeleteResponseAllOf
     */
    refunds?: Array<Refund>;
    /**
     * Number of deleted refunds
     * @type {number}
     * @memberof RefundsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof RefundsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function RefundsDeleteResponseAllOfFromJSON(json: any): RefundsDeleteResponseAllOf;
export declare function RefundsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsDeleteResponseAllOf;
export declare function RefundsDeleteResponseAllOfToJSON(value?: RefundsDeleteResponseAllOf | null): any;
