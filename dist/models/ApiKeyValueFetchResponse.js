"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyValueFetchResponseToJSON = exports.ApiKeyValueFetchResponseFromJSONTyped = exports.ApiKeyValueFetchResponseFromJSON = exports.ApiKeyValueFetchResponseCodeEnum = exports.ApiKeyValueFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ApiKeyValueFetchResponseSuccessEnum;
(function (ApiKeyValueFetchResponseSuccessEnum) {
    ApiKeyValueFetchResponseSuccessEnum["True"] = "true";
})(ApiKeyValueFetchResponseSuccessEnum = exports.ApiKeyValueFetchResponseSuccessEnum || (exports.ApiKeyValueFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ApiKeyValueFetchResponseCodeEnum;
(function (ApiKeyValueFetchResponseCodeEnum) {
    ApiKeyValueFetchResponseCodeEnum[ApiKeyValueFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ApiKeyValueFetchResponseCodeEnum[ApiKeyValueFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ApiKeyValueFetchResponseCodeEnum[ApiKeyValueFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ApiKeyValueFetchResponseCodeEnum = exports.ApiKeyValueFetchResponseCodeEnum || (exports.ApiKeyValueFetchResponseCodeEnum = {}));
function ApiKeyValueFetchResponseFromJSON(json) {
    return ApiKeyValueFetchResponseFromJSONTyped(json, false);
}
exports.ApiKeyValueFetchResponseFromJSON = ApiKeyValueFetchResponseFromJSON;
function ApiKeyValueFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : _1.ApiKeyValueFromJSON(json['apikey']),
    };
}
exports.ApiKeyValueFetchResponseFromJSONTyped = ApiKeyValueFetchResponseFromJSONTyped;
function ApiKeyValueFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'apikey': _1.ApiKeyValueToJSON(value.apikey),
    };
}
exports.ApiKeyValueFetchResponseToJSON = ApiKeyValueFetchResponseToJSON;
