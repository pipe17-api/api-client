"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorsListResponseToJSON = exports.ConnectorsListResponseFromJSONTyped = exports.ConnectorsListResponseFromJSON = exports.ConnectorsListResponseCodeEnum = exports.ConnectorsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorsListResponseSuccessEnum;
(function (ConnectorsListResponseSuccessEnum) {
    ConnectorsListResponseSuccessEnum["True"] = "true";
})(ConnectorsListResponseSuccessEnum = exports.ConnectorsListResponseSuccessEnum || (exports.ConnectorsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ConnectorsListResponseCodeEnum;
(function (ConnectorsListResponseCodeEnum) {
    ConnectorsListResponseCodeEnum[ConnectorsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ConnectorsListResponseCodeEnum[ConnectorsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ConnectorsListResponseCodeEnum[ConnectorsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ConnectorsListResponseCodeEnum = exports.ConnectorsListResponseCodeEnum || (exports.ConnectorsListResponseCodeEnum = {}));
function ConnectorsListResponseFromJSON(json) {
    return ConnectorsListResponseFromJSONTyped(json, false);
}
exports.ConnectorsListResponseFromJSON = ConnectorsListResponseFromJSON;
function ConnectorsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ConnectorsListFilterFromJSON(json['filters']),
        'connectors': !runtime_1.exists(json, 'connectors') ? undefined : (json['connectors'].map(_1.ConnectorFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ConnectorsListResponseFromJSONTyped = ConnectorsListResponseFromJSONTyped;
function ConnectorsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ConnectorsListFilterToJSON(value.filters),
        'connectors': value.connectors === undefined ? undefined : (value.connectors.map(_1.ConnectorToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ConnectorsListResponseToJSON = ConnectorsListResponseToJSON;
