"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuppliersFilterAllOfToJSON = exports.SuppliersFilterAllOfFromJSONTyped = exports.SuppliersFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function SuppliersFilterAllOfFromJSON(json) {
    return SuppliersFilterAllOfFromJSONTyped(json, false);
}
exports.SuppliersFilterAllOfFromJSON = SuppliersFilterAllOfFromJSON;
function SuppliersFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'supplierId': !runtime_1.exists(json, 'supplierId') ? undefined : json['supplierId'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.SuppliersFilterAllOfFromJSONTyped = SuppliersFilterAllOfFromJSONTyped;
function SuppliersFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'supplierId': value.supplierId,
        'deleted': value.deleted,
    };
}
exports.SuppliersFilterAllOfToJSON = SuppliersFilterAllOfToJSON;
