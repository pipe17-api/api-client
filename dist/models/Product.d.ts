/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { NameValue, Price, ProductBundleItem, ProductPublishedItem, ProductStatus, ProductType } from './';
/**
 *
 * @export
 * @interface Product
 */
export interface Product {
    /**
     * Product ID
     * @type {string}
     * @memberof Product
     */
    productId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Product
     */
    integration?: string;
    /**
     * Product SKU
     * @type {string}
     * @memberof Product
     */
    sku: string;
    /**
     * Display Name
     * @type {string}
     * @memberof Product
     */
    name: string;
    /**
     *
     * @type {ProductStatus}
     * @memberof Product
     */
    status?: ProductStatus;
    /**
     * Description
     * @type {string}
     * @memberof Product
     */
    description?: string;
    /**
     * Product types
     * @type {Array<ProductType>}
     * @memberof Product
     */
    types?: Array<ProductType>;
    /**
     * External Product Id
     * @type {string}
     * @memberof Product
     */
    extProductId?: string;
    /**
     * External Product API Id
     * @type {string}
     * @memberof Product
     */
    extProductApiId?: string;
    /**
     * UPC
     * @type {string}
     * @memberof Product
     */
    upc?: string;
    /**
     * Manufacturer part number
     * @type {string}
     * @memberof Product
     */
    partId?: string;
    /**
     * Start off with basePrice
     * @type {Array<Price>}
     * @memberof Product
     */
    prices?: Array<Price>;
    /**
     * Cost
     * @type {number}
     * @memberof Product
     */
    cost?: number;
    /**
     * Currency
     * @type {string}
     * @memberof Product
     */
    costCurrency?: string;
    /**
     * Taxable
     * @type {boolean}
     * @memberof Product
     */
    taxable?: boolean;
    /**
     * Country of Origin
     * @type {string}
     * @memberof Product
     */
    countryOfOrigin?: string;
    /**
     * Harmonized System/Tarrif Code
     * @type {string}
     * @memberof Product
     */
    harmonizedCode?: string;
    /**
     * Weight
     * @type {number}
     * @memberof Product
     */
    weight?: number;
    /**
     * Weight UOM
     * @type {string}
     * @memberof Product
     */
    weightUnit?: ProductWeightUnitEnum;
    /**
     * Height
     * @type {number}
     * @memberof Product
     */
    height?: number;
    /**
     * Length
     * @type {number}
     * @memberof Product
     */
    length?: number;
    /**
     * Width
     * @type {number}
     * @memberof Product
     */
    width?: number;
    /**
     * Dimensions UOM
     * @type {string}
     * @memberof Product
     */
    dimensionsUnit?: ProductDimensionsUnitEnum;
    /**
     * vendor
     * @type {string}
     * @memberof Product
     */
    vendor?: string;
    /**
     * Tags
     * @type {Array<string>}
     * @memberof Product
     */
    tags?: Array<string>;
    /**
     * Image URLs
     * @type {Array<string>}
     * @memberof Product
     */
    imageURLs?: Array<string>;
    /**
     * Variant parent external product Id
     * @type {string}
     * @memberof Product
     */
    parentExtProductId?: string;
    /**
     * this let us know the variant description
     * @type {Array<NameValue>}
     * @memberof Product
     */
    variantOptions?: Array<NameValue>;
    /**
     * Bundle Item need to reference the SKU of the actual product
     * @type {Array<ProductBundleItem>}
     * @memberof Product
     */
    bundleItems?: Array<ProductBundleItem>;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof Product
     */
    customFields?: Array<NameValue>;
    /**
     * Published Channels
     * @type {Array<ProductPublishedItem>}
     * @memberof Product
     */
    published?: Array<ProductPublishedItem>;
    /**
     * Do not track inventory flag
     * @type {boolean}
     * @memberof Product
     */
    inventoryNotTracked?: boolean;
    /**
     * Mark item as hidden in marketplace Storefront
     * @type {boolean}
     * @memberof Product
     */
    hideInStore?: boolean;
    /**
     *
     * @type {Date}
     * @memberof Product
     */
    extProductCreatedAt?: Date;
    /**
     *
     * @type {Date}
     * @memberof Product
     */
    extProductUpdatedAt?: Date;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Product
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Product
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Product
     */
    readonly orgKey?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
} /**
* @export
* @enum {string}
*/
export declare enum ProductDimensionsUnitEnum {
    Cm = "cm",
    In = "in",
    Ft = "ft"
}
export declare function ProductFromJSON(json: any): Product;
export declare function ProductFromJSONTyped(json: any, ignoreDiscriminator: boolean): Product;
export declare function ProductToJSON(value?: Product | null): any;
