"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterToJSON = exports.EntityFilterFromJSONTyped = exports.EntityFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntityFilterFromJSON(json) {
    return EntityFilterFromJSONTyped(json, false);
}
exports.EntityFilterFromJSON = EntityFilterFromJSON;
function EntityFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filterId': !runtime_1.exists(json, 'filterId') ? undefined : json['filterId'],
        'name': json['name'],
        'query': json['query'],
        'entity': _1.EntityNameFromJSON(json['entity']),
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.EntityFilterFromJSONTyped = EntityFilterFromJSONTyped;
function EntityFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filterId': value.filterId,
        'name': value.name,
        'query': value.query,
        'entity': _1.EntityNameToJSON(value.entity),
        'isPublic': value.isPublic,
        'originId': value.originId,
    };
}
exports.EntityFilterToJSON = EntityFilterToJSON;
