"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRulesDeleteResponseToJSON = exports.InventoryRulesDeleteResponseFromJSONTyped = exports.InventoryRulesDeleteResponseFromJSON = exports.InventoryRulesDeleteResponseCodeEnum = exports.InventoryRulesDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryRulesDeleteResponseSuccessEnum;
(function (InventoryRulesDeleteResponseSuccessEnum) {
    InventoryRulesDeleteResponseSuccessEnum["True"] = "true";
})(InventoryRulesDeleteResponseSuccessEnum = exports.InventoryRulesDeleteResponseSuccessEnum || (exports.InventoryRulesDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryRulesDeleteResponseCodeEnum;
(function (InventoryRulesDeleteResponseCodeEnum) {
    InventoryRulesDeleteResponseCodeEnum[InventoryRulesDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryRulesDeleteResponseCodeEnum[InventoryRulesDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryRulesDeleteResponseCodeEnum[InventoryRulesDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryRulesDeleteResponseCodeEnum = exports.InventoryRulesDeleteResponseCodeEnum || (exports.InventoryRulesDeleteResponseCodeEnum = {}));
function InventoryRulesDeleteResponseFromJSON(json) {
    return InventoryRulesDeleteResponseFromJSONTyped(json, false);
}
exports.InventoryRulesDeleteResponseFromJSON = InventoryRulesDeleteResponseFromJSON;
function InventoryRulesDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryRulesDeleteFilterFromJSON(json['filters']),
        'rules': !runtime_1.exists(json, 'rules') ? undefined : (json['rules'].map(_1.InventoryRuleFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryRulesDeleteResponseFromJSONTyped = InventoryRulesDeleteResponseFromJSONTyped;
function InventoryRulesDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.InventoryRulesDeleteFilterToJSON(value.filters),
        'rules': value.rules === undefined ? undefined : (value.rules.map(_1.InventoryRuleToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryRulesDeleteResponseToJSON = InventoryRulesDeleteResponseToJSON;
