"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductCreateResultToJSON = exports.ProductCreateResultFromJSONTyped = exports.ProductCreateResultFromJSON = exports.ProductCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductCreateResultStatusEnum;
(function (ProductCreateResultStatusEnum) {
    ProductCreateResultStatusEnum["Submitted"] = "submitted";
    ProductCreateResultStatusEnum["Failed"] = "failed";
})(ProductCreateResultStatusEnum = exports.ProductCreateResultStatusEnum || (exports.ProductCreateResultStatusEnum = {}));
function ProductCreateResultFromJSON(json) {
    return ProductCreateResultFromJSONTyped(json, false);
}
exports.ProductCreateResultFromJSON = ProductCreateResultFromJSON;
function ProductCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'product': !runtime_1.exists(json, 'product') ? undefined : _1.ProductFromJSON(json['product']),
    };
}
exports.ProductCreateResultFromJSONTyped = ProductCreateResultFromJSONTyped;
function ProductCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'product': _1.ProductToJSON(value.product),
    };
}
exports.ProductCreateResultToJSON = ProductCreateResultToJSON;
