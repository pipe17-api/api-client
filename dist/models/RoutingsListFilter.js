"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsListFilterToJSON = exports.RoutingsListFilterFromJSONTyped = exports.RoutingsListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingsListFilterFromJSON(json) {
    return RoutingsListFilterFromJSONTyped(json, false);
}
exports.RoutingsListFilterFromJSON = RoutingsListFilterFromJSON;
function RoutingsListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'routingId': !runtime_1.exists(json, 'routingId') ? undefined : json['routingId'],
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
        'desc': !runtime_1.exists(json, 'desc') ? undefined : json['desc'],
    };
}
exports.RoutingsListFilterFromJSONTyped = RoutingsListFilterFromJSONTyped;
function RoutingsListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'routingId': value.routingId,
        'uuid': value.uuid,
        'deleted': value.deleted,
        'order': value.order,
        'keys': value.keys,
        'desc': value.desc,
    };
}
exports.RoutingsListFilterToJSON = RoutingsListFilterToJSON;
