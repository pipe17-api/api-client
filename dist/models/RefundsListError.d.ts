/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundsFilter } from './';
/**
 *
 * @export
 * @interface RefundsListError
 */
export interface RefundsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RefundsListError
     */
    success: RefundsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RefundsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RefundsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RefundsFilter}
     * @memberof RefundsListError
     */
    filters?: RefundsFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundsListErrorSuccessEnum {
    False = "false"
}
export declare function RefundsListErrorFromJSON(json: any): RefundsListError;
export declare function RefundsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsListError;
export declare function RefundsListErrorToJSON(value?: RefundsListError | null): any;
