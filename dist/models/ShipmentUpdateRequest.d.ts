/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressUpdateData, ShipmentStatus, ShipmentUpdateDataLineItems } from './';
/**
 *
 * @export
 * @interface ShipmentUpdateRequest
 */
export interface ShipmentUpdateRequest {
    /**
     * Ship By Date (can be updated if shipment is not sentToFulfillment yet)
     * @type {Date}
     * @memberof ShipmentUpdateRequest
     */
    shipByDate?: Date;
    /**
     * Expected ship date (can be updated if shipment is not sentToFulfillment yet)
     * @type {Date}
     * @memberof ShipmentUpdateRequest
     */
    expectedShipDate?: Date;
    /**
     * First Priority to be respected (can be updated if shipment is not sentToFulfillment yet)
     * @type {Date}
     * @memberof ShipmentUpdateRequest
     */
    expectedDeliveryDate?: Date;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof ShipmentUpdateRequest
     */
    shippingAddress?: AddressUpdateData;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    shippingCarrier?: string;
    /**
     * 3rd Priority. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    shippingCode?: string;
    /**
     * Required with ShippingCarrier, 2nd Priority. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    shippingClass?: string;
    /**
     * Shipping Note. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    shippingNote?: string;
    /**
     * Shipping Labels. (can be updated if shipment is not sentToFulfillment yet)
     * @type {Array<string>}
     * @memberof ShipmentUpdateRequest
     */
    shippingLabels?: Array<string>;
    /**
     * Shipping Gift note
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    giftNote?: string;
    /**
     * International Commercial Terms. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    incoterms?: string;
    /**
     * Id of location defined in organization. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    locationId?: string;
    /**
     *
     * @type {ShipmentStatus}
     * @memberof ShipmentUpdateRequest
     */
    status?: ShipmentStatus;
    /**
     * Customer Friendly Internal reference to ShipmentId that they use
     * @type {string}
     * @memberof ShipmentUpdateRequest
     */
    extShipmentId?: string;
    /**
     *
     * @type {Array<ShipmentUpdateDataLineItems>}
     * @memberof ShipmentUpdateRequest
     */
    lineItems?: Array<ShipmentUpdateDataLineItems>;
}
export declare function ShipmentUpdateRequestFromJSON(json: any): ShipmentUpdateRequest;
export declare function ShipmentUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentUpdateRequest;
export declare function ShipmentUpdateRequestToJSON(value?: ShipmentUpdateRequest | null): any;
