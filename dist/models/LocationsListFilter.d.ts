/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationStatus } from './';
/**
 *
 * @export
 * @interface LocationsListFilter
 */
export interface LocationsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof LocationsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof LocationsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof LocationsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof LocationsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof LocationsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof LocationsListFilter
     */
    count?: number;
    /**
     * Locations by list of locationId
     * @type {Array<string>}
     * @memberof LocationsListFilter
     */
    locationId?: Array<string>;
    /**
     *
     * @type {Array<LocationStatus>}
     * @memberof LocationsListFilter
     */
    status?: Array<LocationStatus>;
    /**
     * Soft deleted locations
     * @type {boolean}
     * @memberof LocationsListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof LocationsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof LocationsListFilter
     */
    keys?: string;
    /**
     * Retrieve locations whose names contains this string
     * @type {string}
     * @memberof LocationsListFilter
     */
    name?: string;
    /**
     * Retrieve locations whose email addreses contains this string
     * @type {string}
     * @memberof LocationsListFilter
     */
    email?: string;
    /**
     * Retrieve locations whose phone numbers contains this string
     * @type {string}
     * @memberof LocationsListFilter
     */
    phone?: string;
    /**
     * Retrieve locations by list of external location ids
     * @type {Array<string>}
     * @memberof LocationsListFilter
     */
    extLocationId?: Array<string>;
    /**
     * Retrieve locations by list of external integration ids
     * @type {Array<string>}
     * @memberof LocationsListFilter
     */
    extIntegrationId?: Array<string>;
}
export declare function LocationsListFilterFromJSON(json: any): LocationsListFilter;
export declare function LocationsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsListFilter;
export declare function LocationsListFilterToJSON(value?: LocationsListFilter | null): any;
