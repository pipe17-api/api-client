/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Product published item status
 * @export
 * @enum {string}
 */
export declare enum ProductPublishedItemStatus {
    Tobepublished = "tobepublished",
    Published = "published",
    Tobeunpublished = "tobeunpublished",
    Unpublished = "unpublished"
}
export declare function ProductPublishedItemStatusFromJSON(json: any): ProductPublishedItemStatus;
export declare function ProductPublishedItemStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductPublishedItemStatus;
export declare function ProductPublishedItemStatusToJSON(value?: ProductPublishedItemStatus | null): any;
