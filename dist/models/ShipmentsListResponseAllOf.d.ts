/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Shipment, ShipmentsFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsListResponseAllOf
 */
export interface ShipmentsListResponseAllOf {
    /**
     *
     * @type {ShipmentsFilter}
     * @memberof ShipmentsListResponseAllOf
     */
    filters?: ShipmentsFilter;
    /**
     *
     * @type {Array<Shipment>}
     * @memberof ShipmentsListResponseAllOf
     */
    shipments: Array<Shipment>;
    /**
     *
     * @type {Pagination}
     * @memberof ShipmentsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ShipmentsListResponseAllOfFromJSON(json: any): ShipmentsListResponseAllOf;
export declare function ShipmentsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsListResponseAllOf;
export declare function ShipmentsListResponseAllOfToJSON(value?: ShipmentsListResponseAllOf | null): any;
