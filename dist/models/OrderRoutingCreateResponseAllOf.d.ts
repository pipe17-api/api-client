/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRouting } from './';
/**
 *
 * @export
 * @interface OrderRoutingCreateResponseAllOf
 */
export interface OrderRoutingCreateResponseAllOf {
    /**
     *
     * @type {OrderRouting}
     * @memberof OrderRoutingCreateResponseAllOf
     */
    routing?: OrderRouting;
}
export declare function OrderRoutingCreateResponseAllOfFromJSON(json: any): OrderRoutingCreateResponseAllOf;
export declare function OrderRoutingCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingCreateResponseAllOf;
export declare function OrderRoutingCreateResponseAllOfToJSON(value?: OrderRoutingCreateResponseAllOf | null): any;
