"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsFilterToJSON = exports.ShipmentsFilterFromJSONTyped = exports.ShipmentsFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentsFilterFromJSON(json) {
    return ShipmentsFilterFromJSONTyped(json, false);
}
exports.ShipmentsFilterFromJSON = ShipmentsFilterFromJSON;
function ShipmentsFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'orderType': !runtime_1.exists(json, 'orderType') ? undefined : json['orderType'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ShipmentStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.ShipmentsFilterFromJSONTyped = ShipmentsFilterFromJSONTyped;
function ShipmentsFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'shipmentId': value.shipmentId,
        'orderId': value.orderId,
        'orderType': value.orderType,
        'extOrderId': value.extOrderId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ShipmentStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.ShipmentsFilterToJSON = ShipmentsFilterToJSON;
