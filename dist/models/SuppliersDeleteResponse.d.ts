/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Supplier, SuppliersDeleteFilter } from './';
/**
 *
 * @export
 * @interface SuppliersDeleteResponse
 */
export interface SuppliersDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof SuppliersDeleteResponse
     */
    success: SuppliersDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SuppliersDeleteResponse
     */
    code: SuppliersDeleteResponseCodeEnum;
    /**
     *
     * @type {SuppliersDeleteFilter}
     * @memberof SuppliersDeleteResponse
     */
    filters?: SuppliersDeleteFilter;
    /**
     *
     * @type {Array<Supplier>}
     * @memberof SuppliersDeleteResponse
     */
    suppliers?: Array<Supplier>;
    /**
     * Number of deleted suppliers
     * @type {number}
     * @memberof SuppliersDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof SuppliersDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum SuppliersDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum SuppliersDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function SuppliersDeleteResponseFromJSON(json: any): SuppliersDeleteResponse;
export declare function SuppliersDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersDeleteResponse;
export declare function SuppliersDeleteResponseToJSON(value?: SuppliersDeleteResponse | null): any;
