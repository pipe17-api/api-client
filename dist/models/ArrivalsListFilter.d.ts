/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalsListFilter
 */
export interface ArrivalsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ArrivalsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ArrivalsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ArrivalsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ArrivalsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ArrivalsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ArrivalsListFilter
     */
    count?: number;
    /**
     * Arrivals by list of arrivalId
     * @type {Array<string>}
     * @memberof ArrivalsListFilter
     */
    arrivalId?: Array<string>;
    /**
     * Arrivals by list transfer orders
     * @type {Array<string>}
     * @memberof ArrivalsListFilter
     */
    transferId?: Array<string>;
    /**
     * Arrivals by list purchase orders
     * @type {Array<string>}
     * @memberof ArrivalsListFilter
     */
    purchaseId?: Array<string>;
    /**
     * Arrivals by list external orders (PO or TO)
     * @type {Array<string>}
     * @memberof ArrivalsListFilter
     */
    extOrderId?: Array<string>;
    /**
     * Arrivals by list fulfillmentId
     * @type {Array<string>}
     * @memberof ArrivalsListFilter
     */
    fulfillmentId?: Array<string>;
    /**
     * Arrivals in specific status
     * @type {Array<ArrivalStatus>}
     * @memberof ArrivalsListFilter
     */
    status?: Array<ArrivalStatus>;
    /**
     * Soft deleted arrivals
     * @type {boolean}
     * @memberof ArrivalsListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof ArrivalsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ArrivalsListFilter
     */
    keys?: string;
}
export declare function ArrivalsListFilterFromJSON(json: any): ArrivalsListFilter;
export declare function ArrivalsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsListFilter;
export declare function ArrivalsListFilterToJSON(value?: ArrivalsListFilter | null): any;
