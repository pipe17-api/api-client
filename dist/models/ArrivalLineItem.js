"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalLineItemToJSON = exports.ArrivalLineItemFromJSONTyped = exports.ArrivalLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function ArrivalLineItemFromJSON(json) {
    return ArrivalLineItemFromJSONTyped(json, false);
}
exports.ArrivalLineItemFromJSON = ArrivalLineItemFromJSON;
function ArrivalLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': json['uniqueId'],
        'sku': json['sku'],
        'quantity': json['quantity'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'receivedQuantity': !runtime_1.exists(json, 'receivedQuantity') ? undefined : json['receivedQuantity'],
    };
}
exports.ArrivalLineItemFromJSONTyped = ArrivalLineItemFromJSONTyped;
function ArrivalLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'sku': value.sku,
        'quantity': value.quantity,
        'name': value.name,
        'upc': value.upc,
        'receivedQuantity': value.receivedQuantity,
    };
}
exports.ArrivalLineItemToJSON = ArrivalLineItemToJSON;
