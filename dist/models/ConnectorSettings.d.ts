/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorSettingsField } from './';
/**
 * Connector specific settings
 * @export
 * @interface ConnectorSettings
 */
export interface ConnectorSettings {
    /**
     * correspond to ui fields
     * @type {Array<ConnectorSettingsField>}
     * @memberof ConnectorSettings
     */
    fields?: Array<ConnectorSettingsField> | null;
}
export declare function ConnectorSettingsFromJSON(json: any): ConnectorSettings;
export declare function ConnectorSettingsFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSettings;
export declare function ConnectorSettingsToJSON(value?: ConnectorSettings | null): any;
