/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationUpgradeRequest
 */
export interface IntegrationUpgradeRequest {
    /**
     * Connector version
     * @type {string}
     * @memberof IntegrationUpgradeRequest
     */
    version?: string;
}
export declare function IntegrationUpgradeRequestFromJSON(json: any): IntegrationUpgradeRequest;
export declare function IntegrationUpgradeRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationUpgradeRequest;
export declare function IntegrationUpgradeRequestToJSON(value?: IntegrationUpgradeRequest | null): any;
