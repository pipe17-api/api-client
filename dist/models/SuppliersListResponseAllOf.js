"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuppliersListResponseAllOfToJSON = exports.SuppliersListResponseAllOfFromJSONTyped = exports.SuppliersListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SuppliersListResponseAllOfFromJSON(json) {
    return SuppliersListResponseAllOfFromJSONTyped(json, false);
}
exports.SuppliersListResponseAllOfFromJSON = SuppliersListResponseAllOfFromJSON;
function SuppliersListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.SuppliersListFilterFromJSON(json['filters']),
        'suppliers': !runtime_1.exists(json, 'suppliers') ? undefined : (json['suppliers'].map(_1.SupplierFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.SuppliersListResponseAllOfFromJSONTyped = SuppliersListResponseAllOfFromJSONTyped;
function SuppliersListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.SuppliersListFilterToJSON(value.filters),
        'suppliers': value.suppliers === undefined ? undefined : (value.suppliers.map(_1.SupplierToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.SuppliersListResponseAllOfToJSON = SuppliersListResponseAllOfToJSON;
