/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus, RefundType } from './';
/**
 *
 * @export
 * @interface RefundsFilter
 */
export interface RefundsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RefundsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RefundsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RefundsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RefundsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RefundsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RefundsFilter
     */
    count?: number;
    /**
     * Refunds by list of refundId
     * @type {Array<string>}
     * @memberof RefundsFilter
     */
    refundId?: Array<string>;
    /**
     * Refunds by list of external refund ids
     * @type {Array<string>}
     * @memberof RefundsFilter
     */
    extRefundId?: Array<string>;
    /**
     * Refunds by list of external refund ids
     * @type {Array<string>}
     * @memberof RefundsFilter
     */
    extReturnId?: Array<string>;
    /**
     * Refunds by list of external order ids
     * @type {Array<string>}
     * @memberof RefundsFilter
     */
    extOrderId?: Array<string>;
    /**
     * Refunds by list of types
     * @type {Array<RefundType>}
     * @memberof RefundsFilter
     */
    type?: Array<RefundType>;
    /**
     * Refunds by list of statuses
     * @type {Array<RefundStatus>}
     * @memberof RefundsFilter
     */
    status?: Array<RefundStatus>;
    /**
     * Soft deleted refunds
     * @type {boolean}
     * @memberof RefundsFilter
     */
    deleted?: boolean;
}
export declare function RefundsFilterFromJSON(json: any): RefundsFilter;
export declare function RefundsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsFilter;
export declare function RefundsFilterToJSON(value?: RefundsFilter | null): any;
