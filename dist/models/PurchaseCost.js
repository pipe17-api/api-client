"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseCostToJSON = exports.PurchaseCostFromJSONTyped = exports.PurchaseCostFromJSON = void 0;
const runtime_1 = require("../runtime");
function PurchaseCostFromJSON(json) {
    return PurchaseCostFromJSONTyped(json, false);
}
exports.PurchaseCostFromJSON = PurchaseCostFromJSON;
function PurchaseCostFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'cost': json['cost'],
        'quantity': !runtime_1.exists(json, 'quantity') ? undefined : json['quantity'],
    };
}
exports.PurchaseCostFromJSONTyped = PurchaseCostFromJSONTyped;
function PurchaseCostToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'cost': value.cost,
        'quantity': value.quantity,
    };
}
exports.PurchaseCostToJSON = PurchaseCostToJSON;
