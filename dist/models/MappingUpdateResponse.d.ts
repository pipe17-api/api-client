/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MappingUpdateResponse
 */
export interface MappingUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof MappingUpdateResponse
     */
    success: MappingUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingUpdateResponse
     */
    code: MappingUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum MappingUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function MappingUpdateResponseFromJSON(json: any): MappingUpdateResponse;
export declare function MappingUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingUpdateResponse;
export declare function MappingUpdateResponseToJSON(value?: MappingUpdateResponse | null): any;
