"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookCreateResponseAllOfToJSON = exports.WebhookCreateResponseAllOfFromJSONTyped = exports.WebhookCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function WebhookCreateResponseAllOfFromJSON(json) {
    return WebhookCreateResponseAllOfFromJSONTyped(json, false);
}
exports.WebhookCreateResponseAllOfFromJSON = WebhookCreateResponseAllOfFromJSON;
function WebhookCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.WebhookFromJSON(json['webhook']),
    };
}
exports.WebhookCreateResponseAllOfFromJSONTyped = WebhookCreateResponseAllOfFromJSONTyped;
function WebhookCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'webhook': _1.WebhookToJSON(value.webhook),
    };
}
exports.WebhookCreateResponseAllOfToJSON = WebhookCreateResponseAllOfToJSON;
