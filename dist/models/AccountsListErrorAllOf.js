"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsListErrorAllOfToJSON = exports.AccountsListErrorAllOfFromJSONTyped = exports.AccountsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountsListErrorAllOfFromJSON(json) {
    return AccountsListErrorAllOfFromJSONTyped(json, false);
}
exports.AccountsListErrorAllOfFromJSON = AccountsListErrorAllOfFromJSON;
function AccountsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.AccountsListFilterFromJSON(json['filters']),
    };
}
exports.AccountsListErrorAllOfFromJSONTyped = AccountsListErrorAllOfFromJSONTyped;
function AccountsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.AccountsListFilterToJSON(value.filters),
    };
}
exports.AccountsListErrorAllOfToJSON = AccountsListErrorAllOfToJSON;
