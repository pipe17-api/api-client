"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleFetchResponseToJSON = exports.InventoryRuleFetchResponseFromJSONTyped = exports.InventoryRuleFetchResponseFromJSON = exports.InventoryRuleFetchResponseCodeEnum = exports.InventoryRuleFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryRuleFetchResponseSuccessEnum;
(function (InventoryRuleFetchResponseSuccessEnum) {
    InventoryRuleFetchResponseSuccessEnum["True"] = "true";
})(InventoryRuleFetchResponseSuccessEnum = exports.InventoryRuleFetchResponseSuccessEnum || (exports.InventoryRuleFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryRuleFetchResponseCodeEnum;
(function (InventoryRuleFetchResponseCodeEnum) {
    InventoryRuleFetchResponseCodeEnum[InventoryRuleFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryRuleFetchResponseCodeEnum[InventoryRuleFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryRuleFetchResponseCodeEnum[InventoryRuleFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryRuleFetchResponseCodeEnum = exports.InventoryRuleFetchResponseCodeEnum || (exports.InventoryRuleFetchResponseCodeEnum = {}));
function InventoryRuleFetchResponseFromJSON(json) {
    return InventoryRuleFetchResponseFromJSONTyped(json, false);
}
exports.InventoryRuleFetchResponseFromJSON = InventoryRuleFetchResponseFromJSON;
function InventoryRuleFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'inventoryrule': !runtime_1.exists(json, 'inventoryrule') ? undefined : _1.InventoryRuleFromJSON(json['inventoryrule']),
    };
}
exports.InventoryRuleFetchResponseFromJSONTyped = InventoryRuleFetchResponseFromJSONTyped;
function InventoryRuleFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'inventoryrule': _1.InventoryRuleToJSON(value.inventoryrule),
    };
}
exports.InventoryRuleFetchResponseToJSON = InventoryRuleFetchResponseToJSON;
