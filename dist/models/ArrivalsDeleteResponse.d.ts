/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival, ArrivalsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ArrivalsDeleteResponse
 */
export interface ArrivalsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ArrivalsDeleteResponse
     */
    success: ArrivalsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalsDeleteResponse
     */
    code: ArrivalsDeleteResponseCodeEnum;
    /**
     *
     * @type {ArrivalsDeleteFilter}
     * @memberof ArrivalsDeleteResponse
     */
    filters?: ArrivalsDeleteFilter;
    /**
     *
     * @type {Array<Arrival>}
     * @memberof ArrivalsDeleteResponse
     */
    arrivals?: Array<Arrival>;
    /**
     * Number of deleted arrivals
     * @type {number}
     * @memberof ArrivalsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ArrivalsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ArrivalsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ArrivalsDeleteResponseFromJSON(json: any): ArrivalsDeleteResponse;
export declare function ArrivalsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsDeleteResponse;
export declare function ArrivalsDeleteResponseToJSON(value?: ArrivalsDeleteResponse | null): any;
