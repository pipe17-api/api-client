"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterCreateResponseToJSON = exports.EntityFilterCreateResponseFromJSONTyped = exports.EntityFilterCreateResponseFromJSON = exports.EntityFilterCreateResponseCodeEnum = exports.EntityFilterCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntityFilterCreateResponseSuccessEnum;
(function (EntityFilterCreateResponseSuccessEnum) {
    EntityFilterCreateResponseSuccessEnum["True"] = "true";
})(EntityFilterCreateResponseSuccessEnum = exports.EntityFilterCreateResponseSuccessEnum || (exports.EntityFilterCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntityFilterCreateResponseCodeEnum;
(function (EntityFilterCreateResponseCodeEnum) {
    EntityFilterCreateResponseCodeEnum[EntityFilterCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntityFilterCreateResponseCodeEnum[EntityFilterCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntityFilterCreateResponseCodeEnum[EntityFilterCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntityFilterCreateResponseCodeEnum = exports.EntityFilterCreateResponseCodeEnum || (exports.EntityFilterCreateResponseCodeEnum = {}));
function EntityFilterCreateResponseFromJSON(json) {
    return EntityFilterCreateResponseFromJSONTyped(json, false);
}
exports.EntityFilterCreateResponseFromJSON = EntityFilterCreateResponseFromJSON;
function EntityFilterCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'entityFilter': !runtime_1.exists(json, 'entityFilter') ? undefined : _1.EntityFilterFromJSON(json['entityFilter']),
    };
}
exports.EntityFilterCreateResponseFromJSONTyped = EntityFilterCreateResponseFromJSONTyped;
function EntityFilterCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'entityFilter': _1.EntityFilterToJSON(value.entityFilter),
    };
}
exports.EntityFilterCreateResponseToJSON = EntityFilterCreateResponseToJSON;
