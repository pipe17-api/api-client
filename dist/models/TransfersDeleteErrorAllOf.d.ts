/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransfersDeleteFilter } from './';
/**
 *
 * @export
 * @interface TransfersDeleteErrorAllOf
 */
export interface TransfersDeleteErrorAllOf {
    /**
     *
     * @type {TransfersDeleteFilter}
     * @memberof TransfersDeleteErrorAllOf
     */
    filters?: TransfersDeleteFilter;
}
export declare function TransfersDeleteErrorAllOfFromJSON(json: any): TransfersDeleteErrorAllOf;
export declare function TransfersDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersDeleteErrorAllOf;
export declare function TransfersDeleteErrorAllOfToJSON(value?: TransfersDeleteErrorAllOf | null): any;
