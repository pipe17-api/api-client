"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingUpdateRequestToJSON = exports.TrackingUpdateRequestFromJSONTyped = exports.TrackingUpdateRequestFromJSON = exports.TrackingUpdateRequestStatusEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var TrackingUpdateRequestStatusEnum;
(function (TrackingUpdateRequestStatusEnum) {
    TrackingUpdateRequestStatusEnum["Shipped"] = "shipped";
    TrackingUpdateRequestStatusEnum["Delivered"] = "delivered";
})(TrackingUpdateRequestStatusEnum = exports.TrackingUpdateRequestStatusEnum || (exports.TrackingUpdateRequestStatusEnum = {}));
function TrackingUpdateRequestFromJSON(json) {
    return TrackingUpdateRequestFromJSONTyped(json, false);
}
exports.TrackingUpdateRequestFromJSON = TrackingUpdateRequestFromJSON;
function TrackingUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'status': json['status'],
    };
}
exports.TrackingUpdateRequestFromJSONTyped = TrackingUpdateRequestFromJSONTyped;
function TrackingUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'status': value.status,
    };
}
exports.TrackingUpdateRequestToJSON = TrackingUpdateRequestToJSON;
