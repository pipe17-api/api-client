/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorCreateData } from './';
/**
 *
 * @export
 * @interface ConnectorCreateErrorAllOf
 */
export interface ConnectorCreateErrorAllOf {
    /**
     *
     * @type {ConnectorCreateData}
     * @memberof ConnectorCreateErrorAllOf
     */
    connector?: ConnectorCreateData;
}
export declare function ConnectorCreateErrorAllOfFromJSON(json: any): ConnectorCreateErrorAllOf;
export declare function ConnectorCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorCreateErrorAllOf;
export declare function ConnectorCreateErrorAllOfToJSON(value?: ConnectorCreateErrorAllOf | null): any;
