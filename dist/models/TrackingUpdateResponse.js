"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingUpdateResponseToJSON = exports.TrackingUpdateResponseFromJSONTyped = exports.TrackingUpdateResponseFromJSON = exports.TrackingUpdateResponseCodeEnum = exports.TrackingUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var TrackingUpdateResponseSuccessEnum;
(function (TrackingUpdateResponseSuccessEnum) {
    TrackingUpdateResponseSuccessEnum["True"] = "true";
})(TrackingUpdateResponseSuccessEnum = exports.TrackingUpdateResponseSuccessEnum || (exports.TrackingUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TrackingUpdateResponseCodeEnum;
(function (TrackingUpdateResponseCodeEnum) {
    TrackingUpdateResponseCodeEnum[TrackingUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TrackingUpdateResponseCodeEnum[TrackingUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TrackingUpdateResponseCodeEnum[TrackingUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TrackingUpdateResponseCodeEnum = exports.TrackingUpdateResponseCodeEnum || (exports.TrackingUpdateResponseCodeEnum = {}));
function TrackingUpdateResponseFromJSON(json) {
    return TrackingUpdateResponseFromJSONTyped(json, false);
}
exports.TrackingUpdateResponseFromJSON = TrackingUpdateResponseFromJSON;
function TrackingUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.TrackingUpdateResponseFromJSONTyped = TrackingUpdateResponseFromJSONTyped;
function TrackingUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.TrackingUpdateResponseToJSON = TrackingUpdateResponseToJSON;
