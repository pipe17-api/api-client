"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsListFilterAllOfToJSON = exports.RoutingsListFilterAllOfFromJSONTyped = exports.RoutingsListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingsListFilterAllOfFromJSON(json) {
    return RoutingsListFilterAllOfFromJSONTyped(json, false);
}
exports.RoutingsListFilterAllOfFromJSON = RoutingsListFilterAllOfFromJSON;
function RoutingsListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'desc': !runtime_1.exists(json, 'desc') ? undefined : json['desc'],
    };
}
exports.RoutingsListFilterAllOfFromJSONTyped = RoutingsListFilterAllOfFromJSONTyped;
function RoutingsListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'desc': value.desc,
    };
}
exports.RoutingsListFilterAllOfToJSON = RoutingsListFilterAllOfToJSON;
