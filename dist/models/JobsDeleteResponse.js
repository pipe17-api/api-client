"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobsDeleteResponseToJSON = exports.JobsDeleteResponseFromJSONTyped = exports.JobsDeleteResponseFromJSON = exports.JobsDeleteResponseCodeEnum = exports.JobsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var JobsDeleteResponseSuccessEnum;
(function (JobsDeleteResponseSuccessEnum) {
    JobsDeleteResponseSuccessEnum["True"] = "true";
})(JobsDeleteResponseSuccessEnum = exports.JobsDeleteResponseSuccessEnum || (exports.JobsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var JobsDeleteResponseCodeEnum;
(function (JobsDeleteResponseCodeEnum) {
    JobsDeleteResponseCodeEnum[JobsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    JobsDeleteResponseCodeEnum[JobsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    JobsDeleteResponseCodeEnum[JobsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(JobsDeleteResponseCodeEnum = exports.JobsDeleteResponseCodeEnum || (exports.JobsDeleteResponseCodeEnum = {}));
function JobsDeleteResponseFromJSON(json) {
    return JobsDeleteResponseFromJSONTyped(json, false);
}
exports.JobsDeleteResponseFromJSON = JobsDeleteResponseFromJSON;
function JobsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.JobsDeleteFilterFromJSON(json['filters']),
        'jobs': !runtime_1.exists(json, 'jobs') ? undefined : (json['jobs'].map(_1.JobFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.JobsDeleteResponseFromJSONTyped = JobsDeleteResponseFromJSONTyped;
function JobsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.JobsDeleteFilterToJSON(value.filters),
        'jobs': value.jobs === undefined ? undefined : (value.jobs.map(_1.JobToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.JobsDeleteResponseToJSON = JobsDeleteResponseToJSON;
