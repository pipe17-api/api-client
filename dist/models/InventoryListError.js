"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryListErrorToJSON = exports.InventoryListErrorFromJSONTyped = exports.InventoryListErrorFromJSON = exports.InventoryListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryListErrorSuccessEnum;
(function (InventoryListErrorSuccessEnum) {
    InventoryListErrorSuccessEnum["False"] = "false";
})(InventoryListErrorSuccessEnum = exports.InventoryListErrorSuccessEnum || (exports.InventoryListErrorSuccessEnum = {}));
function InventoryListErrorFromJSON(json) {
    return InventoryListErrorFromJSONTyped(json, false);
}
exports.InventoryListErrorFromJSON = InventoryListErrorFromJSON;
function InventoryListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryListFilterFromJSON(json['filters']),
    };
}
exports.InventoryListErrorFromJSONTyped = InventoryListErrorFromJSONTyped;
function InventoryListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.InventoryListFilterToJSON(value.filters),
    };
}
exports.InventoryListErrorToJSON = InventoryListErrorToJSON;
