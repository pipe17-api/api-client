/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventResponseData
 */
export interface EventResponseData {
    /**
     * Number of retries
     * @type {number}
     * @memberof EventResponseData
     */
    retries?: number;
    /**
     * Response status
     * @type {number}
     * @memberof EventResponseData
     */
    status?: number;
    /**
     * Response message
     * @type {string}
     * @memberof EventResponseData
     */
    message?: string;
    /**
     * Response headers
     * @type {object}
     * @memberof EventResponseData
     */
    headers?: object;
    /**
     * Response body
     * @type {string}
     * @memberof EventResponseData
     */
    body?: string;
}
export declare function EventResponseDataFromJSON(json: any): EventResponseData;
export declare function EventResponseDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventResponseData;
export declare function EventResponseDataToJSON(value?: EventResponseData | null): any;
