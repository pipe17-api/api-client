/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKey, ApiKeysListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ApiKeysListResponse
 */
export interface ApiKeysListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ApiKeysListResponse
     */
    success: ApiKeysListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeysListResponse
     */
    code: ApiKeysListResponseCodeEnum;
    /**
     *
     * @type {ApiKeysListFilter}
     * @memberof ApiKeysListResponse
     */
    filters?: ApiKeysListFilter;
    /**
     *
     * @type {Array<ApiKey>}
     * @memberof ApiKeysListResponse
     */
    apikeys?: Array<ApiKey>;
    /**
     *
     * @type {Pagination}
     * @memberof ApiKeysListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeysListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ApiKeysListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ApiKeysListResponseFromJSON(json: any): ApiKeysListResponse;
export declare function ApiKeysListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysListResponse;
export declare function ApiKeysListResponseToJSON(value?: ApiKeysListResponse | null): any;
