/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * API Keys Filter
 * @export
 * @interface ApiKeysFilterAllOf
 */
export interface ApiKeysFilterAllOf {
    /**
     * List API Keys by matching connector id(s)
     * @type {Array<string>}
     * @memberof ApiKeysFilterAllOf
     */
    connector?: Array<string>;
    /**
     * List API Keys by matching integration id(s)
     * @type {Array<string>}
     * @memberof ApiKeysFilterAllOf
     */
    integration?: Array<string>;
    /**
     * List API Keys by matching name(s)
     * @type {string}
     * @memberof ApiKeysFilterAllOf
     */
    name?: string;
}
export declare function ApiKeysFilterAllOfFromJSON(json: any): ApiKeysFilterAllOf;
export declare function ApiKeysFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysFilterAllOf;
export declare function ApiKeysFilterAllOfToJSON(value?: ApiKeysFilterAllOf | null): any;
