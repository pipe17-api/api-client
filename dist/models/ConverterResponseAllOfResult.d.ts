/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Conversion result
 * @export
 * @interface ConverterResponseAllOfResult
 */
export interface ConverterResponseAllOfResult {
    /**
     *
     * @type {number}
     * @memberof ConverterResponseAllOfResult
     */
    status?: number;
    /**
     *
     * @type {object}
     * @memberof ConverterResponseAllOfResult
     */
    body?: object;
}
export declare function ConverterResponseAllOfResultFromJSON(json: any): ConverterResponseAllOfResult;
export declare function ConverterResponseAllOfResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConverterResponseAllOfResult;
export declare function ConverterResponseAllOfResultToJSON(value?: ConverterResponseAllOfResult | null): any;
