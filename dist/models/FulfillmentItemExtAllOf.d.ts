/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentItemExtAllOf
 */
export interface FulfillmentItemExtAllOf {
    /**
     * Matching bundle SKU when available
     * @type {string}
     * @memberof FulfillmentItemExtAllOf
     */
    bundleSKU?: string;
    /**
     * Matching bundle quantity when available
     * @type {number}
     * @memberof FulfillmentItemExtAllOf
     */
    bundleQuantity?: number;
}
export declare function FulfillmentItemExtAllOfFromJSON(json: any): FulfillmentItemExtAllOf;
export declare function FulfillmentItemExtAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentItemExtAllOf;
export declare function FulfillmentItemExtAllOfToJSON(value?: FulfillmentItemExtAllOf | null): any;
