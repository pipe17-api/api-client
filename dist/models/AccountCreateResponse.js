"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountCreateResponseToJSON = exports.AccountCreateResponseFromJSONTyped = exports.AccountCreateResponseFromJSON = exports.AccountCreateResponseCodeEnum = exports.AccountCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var AccountCreateResponseSuccessEnum;
(function (AccountCreateResponseSuccessEnum) {
    AccountCreateResponseSuccessEnum["True"] = "true";
})(AccountCreateResponseSuccessEnum = exports.AccountCreateResponseSuccessEnum || (exports.AccountCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var AccountCreateResponseCodeEnum;
(function (AccountCreateResponseCodeEnum) {
    AccountCreateResponseCodeEnum[AccountCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    AccountCreateResponseCodeEnum[AccountCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    AccountCreateResponseCodeEnum[AccountCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(AccountCreateResponseCodeEnum = exports.AccountCreateResponseCodeEnum || (exports.AccountCreateResponseCodeEnum = {}));
function AccountCreateResponseFromJSON(json) {
    return AccountCreateResponseFromJSONTyped(json, false);
}
exports.AccountCreateResponseFromJSON = AccountCreateResponseFromJSON;
function AccountCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountFromJSON(json['account']),
        'activationKey': !runtime_1.exists(json, 'activationKey') ? undefined : json['activationKey'],
    };
}
exports.AccountCreateResponseFromJSONTyped = AccountCreateResponseFromJSONTyped;
function AccountCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'account': _1.AccountToJSON(value.account),
        'activationKey': value.activationKey,
    };
}
exports.AccountCreateResponseToJSON = AccountCreateResponseToJSON;
