/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingsListFilter
 */
export interface OrderRoutingsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrderRoutingsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrderRoutingsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrderRoutingsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrderRoutingsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrderRoutingsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrderRoutingsListFilter
     */
    count?: number;
    /**
     * List sort order
     * @type {string}
     * @memberof OrderRoutingsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof OrderRoutingsListFilter
     */
    keys?: string;
}
export declare function OrderRoutingsListFilterFromJSON(json: any): OrderRoutingsListFilter;
export declare function OrderRoutingsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingsListFilter;
export declare function OrderRoutingsListFilterToJSON(value?: OrderRoutingsListFilter | null): any;
