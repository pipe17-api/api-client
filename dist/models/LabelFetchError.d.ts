/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelFetchError
 */
export interface LabelFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LabelFetchError
     */
    success: LabelFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LabelFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LabelFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelFetchErrorSuccessEnum {
    False = "false"
}
export declare function LabelFetchErrorFromJSON(json: any): LabelFetchError;
export declare function LabelFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelFetchError;
export declare function LabelFetchErrorToJSON(value?: LabelFetchError | null): any;
