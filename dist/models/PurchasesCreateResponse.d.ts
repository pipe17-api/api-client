/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseCreateResult } from './';
/**
 *
 * @export
 * @interface PurchasesCreateResponse
 */
export interface PurchasesCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof PurchasesCreateResponse
     */
    success: PurchasesCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchasesCreateResponse
     */
    code: PurchasesCreateResponseCodeEnum;
    /**
     *
     * @type {Array<PurchaseCreateResult>}
     * @memberof PurchasesCreateResponse
     */
    purchases?: Array<PurchaseCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchasesCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum PurchasesCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function PurchasesCreateResponseFromJSON(json: any): PurchasesCreateResponse;
export declare function PurchasesCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesCreateResponse;
export declare function PurchasesCreateResponseToJSON(value?: PurchasesCreateResponse | null): any;
