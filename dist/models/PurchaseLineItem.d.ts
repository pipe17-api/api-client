/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface PurchaseLineItem
 */
export interface PurchaseLineItem {
    /**
     * Item unique Id (within purchase)
     * @type {string}
     * @memberof PurchaseLineItem
     */
    uniqueId: string;
    /**
     * Manufacturer Part ID
     * @type {string}
     * @memberof PurchaseLineItem
     */
    partId?: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof PurchaseLineItem
     */
    quantity: number;
    /**
     * Purchaser Item SKU
     * @type {string}
     * @memberof PurchaseLineItem
     */
    sku?: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof PurchaseLineItem
     */
    name?: string;
    /**
     * Item UPC
     * @type {string}
     * @memberof PurchaseLineItem
     */
    upc?: string;
    /**
     * Item Price
     * @type {number}
     * @memberof PurchaseLineItem
     */
    price?: number;
    /**
     * Shipped quantity
     * @type {number}
     * @memberof PurchaseLineItem
     */
    shippedQuantity?: number;
    /**
     * Received quantity
     * @type {number}
     * @memberof PurchaseLineItem
     */
    receivedQuantity?: number;
    /**
     *
     * @type {string}
     * @memberof PurchaseLineItem
     */
    locationId?: string;
}
export declare function PurchaseLineItemFromJSON(json: any): PurchaseLineItem;
export declare function PurchaseLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseLineItem;
export declare function PurchaseLineItemToJSON(value?: PurchaseLineItem | null): any;
