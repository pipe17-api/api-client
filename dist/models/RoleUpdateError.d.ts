/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleUpdateData } from './';
/**
 *
 * @export
 * @interface RoleUpdateError
 */
export interface RoleUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoleUpdateError
     */
    success: RoleUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoleUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoleUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoleUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RoleUpdateData}
     * @memberof RoleUpdateError
     */
    role?: RoleUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum RoleUpdateErrorSuccessEnum {
    False = "false"
}
export declare function RoleUpdateErrorFromJSON(json: any): RoleUpdateError;
export declare function RoleUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleUpdateError;
export declare function RoleUpdateErrorToJSON(value?: RoleUpdateError | null): any;
