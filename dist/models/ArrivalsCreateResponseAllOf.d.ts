/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalCreateResult } from './';
/**
 *
 * @export
 * @interface ArrivalsCreateResponseAllOf
 */
export interface ArrivalsCreateResponseAllOf {
    /**
     *
     * @type {Array<ArrivalCreateResult>}
     * @memberof ArrivalsCreateResponseAllOf
     */
    arrivals?: Array<ArrivalCreateResult>;
}
export declare function ArrivalsCreateResponseAllOfFromJSON(json: any): ArrivalsCreateResponseAllOf;
export declare function ArrivalsCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsCreateResponseAllOf;
export declare function ArrivalsCreateResponseAllOfToJSON(value?: ArrivalsCreateResponseAllOf | null): any;
