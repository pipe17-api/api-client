"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderUpdateResponseToJSON = exports.OrderUpdateResponseFromJSONTyped = exports.OrderUpdateResponseFromJSON = exports.OrderUpdateResponseCodeEnum = exports.OrderUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var OrderUpdateResponseSuccessEnum;
(function (OrderUpdateResponseSuccessEnum) {
    OrderUpdateResponseSuccessEnum["True"] = "true";
})(OrderUpdateResponseSuccessEnum = exports.OrderUpdateResponseSuccessEnum || (exports.OrderUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrderUpdateResponseCodeEnum;
(function (OrderUpdateResponseCodeEnum) {
    OrderUpdateResponseCodeEnum[OrderUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrderUpdateResponseCodeEnum[OrderUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrderUpdateResponseCodeEnum[OrderUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrderUpdateResponseCodeEnum = exports.OrderUpdateResponseCodeEnum || (exports.OrderUpdateResponseCodeEnum = {}));
function OrderUpdateResponseFromJSON(json) {
    return OrderUpdateResponseFromJSONTyped(json, false);
}
exports.OrderUpdateResponseFromJSON = OrderUpdateResponseFromJSON;
function OrderUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.OrderUpdateResponseFromJSONTyped = OrderUpdateResponseFromJSONTyped;
function OrderUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.OrderUpdateResponseToJSON = OrderUpdateResponseToJSON;
