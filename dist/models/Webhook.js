"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookToJSON = exports.WebhookFromJSONTyped = exports.WebhookFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function WebhookFromJSON(json) {
    return WebhookFromJSONTyped(json, false);
}
exports.WebhookFromJSON = WebhookFromJSON;
function WebhookFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'webhookId': !runtime_1.exists(json, 'webhookId') ? undefined : json['webhookId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'url': json['url'],
        'apikey': json['apikey'],
        'topics': (json['topics'].map(_1.WebhookTopicsFromJSON)),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.WebhookFromJSONTyped = WebhookFromJSONTyped;
function WebhookToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'webhookId': value.webhookId,
        'integration': value.integration,
        'connectorId': value.connectorId,
        'url': value.url,
        'apikey': value.apikey,
        'topics': (value.topics.map(_1.WebhookTopicsToJSON)),
    };
}
exports.WebhookToJSON = WebhookToJSON;
