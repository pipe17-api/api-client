"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConverterRequestToJSON = exports.ConverterRequestFromJSONTyped = exports.ConverterRequestFromJSON = void 0;
const _1 = require("./");
function ConverterRequestFromJSON(json) {
    return ConverterRequestFromJSONTyped(json, false);
}
exports.ConverterRequestFromJSON = ConverterRequestFromJSON;
function ConverterRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'rules': _1.ConverterRequestRulesFromJSON(json['rules']),
        'source': json['source'],
    };
}
exports.ConverterRequestFromJSONTyped = ConverterRequestFromJSONTyped;
function ConverterRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'rules': _1.ConverterRequestRulesToJSON(value.rules),
        'source': value.source,
    };
}
exports.ConverterRequestToJSON = ConverterRequestToJSON;
