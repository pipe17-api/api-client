/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRuleFetchError
 */
export interface InventoryRuleFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryRuleFetchError
     */
    success: InventoryRuleFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRuleFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryRuleFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryRuleFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRuleFetchErrorSuccessEnum {
    False = "false"
}
export declare function InventoryRuleFetchErrorFromJSON(json: any): InventoryRuleFetchError;
export declare function InventoryRuleFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleFetchError;
export declare function InventoryRuleFetchErrorToJSON(value?: InventoryRuleFetchError | null): any;
