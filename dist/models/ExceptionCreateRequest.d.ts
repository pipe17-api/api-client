/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName, ExceptionType } from './';
/**
 *
 * @export
 * @interface ExceptionCreateRequest
 */
export interface ExceptionCreateRequest {
    /**
     *
     * @type {ExceptionType}
     * @memberof ExceptionCreateRequest
     */
    exceptionType: ExceptionType;
    /**
     * Exception specific error message/details
     * @type {string}
     * @memberof ExceptionCreateRequest
     */
    exceptionDetails?: string;
    /**
     * Entity ID
     * @type {string}
     * @memberof ExceptionCreateRequest
     */
    entityId: string;
    /**
     *
     * @type {EntityName}
     * @memberof ExceptionCreateRequest
     */
    entityType: EntityName;
}
export declare function ExceptionCreateRequestFromJSON(json: any): ExceptionCreateRequest;
export declare function ExceptionCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionCreateRequest;
export declare function ExceptionCreateRequestToJSON(value?: ExceptionCreateRequest | null): any;
