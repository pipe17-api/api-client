/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SupplierContact
 */
export interface SupplierContact {
    /**
     * First Name
     * @type {string}
     * @memberof SupplierContact
     */
    firstName?: string;
    /**
     * Last Name
     * @type {string}
     * @memberof SupplierContact
     */
    lastName?: string;
    /**
     * Email
     * @type {string}
     * @memberof SupplierContact
     */
    email?: string;
    /**
     * Phone
     * @type {string}
     * @memberof SupplierContact
     */
    phone?: string;
}
export declare function SupplierContactFromJSON(json: any): SupplierContact;
export declare function SupplierContactFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierContact;
export declare function SupplierContactToJSON(value?: SupplierContact | null): any;
