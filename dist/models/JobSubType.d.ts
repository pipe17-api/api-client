/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Job sub-type
 * @export
 * @enum {string}
 */
export declare enum JobSubType {
    Inventory = "inventory",
    Replenishment = "replenishment",
    Purchases = "purchases",
    Transfers = "transfers",
    Arrivals = "arrivals",
    Products = "products"
}
export declare function JobSubTypeFromJSON(json: any): JobSubType;
export declare function JobSubTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobSubType;
export declare function JobSubTypeToJSON(value?: JobSubType | null): any;
