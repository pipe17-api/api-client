"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookFetchErrorToJSON = exports.WebhookFetchErrorFromJSONTyped = exports.WebhookFetchErrorFromJSON = exports.WebhookFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var WebhookFetchErrorSuccessEnum;
(function (WebhookFetchErrorSuccessEnum) {
    WebhookFetchErrorSuccessEnum["False"] = "false";
})(WebhookFetchErrorSuccessEnum = exports.WebhookFetchErrorSuccessEnum || (exports.WebhookFetchErrorSuccessEnum = {}));
function WebhookFetchErrorFromJSON(json) {
    return WebhookFetchErrorFromJSONTyped(json, false);
}
exports.WebhookFetchErrorFromJSON = WebhookFetchErrorFromJSON;
function WebhookFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.WebhookFetchErrorFromJSONTyped = WebhookFetchErrorFromJSONTyped;
function WebhookFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.WebhookFetchErrorToJSON = WebhookFetchErrorToJSON;
