/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalCreateResult } from './';
/**
 *
 * @export
 * @interface ArrivalsCreateError
 */
export interface ArrivalsCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ArrivalsCreateError
     */
    success: ArrivalsCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalsCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ArrivalsCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ArrivalsCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<ArrivalCreateResult>}
     * @memberof ArrivalsCreateError
     */
    arrivals?: Array<ArrivalCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalsCreateErrorSuccessEnum {
    False = "false"
}
export declare function ArrivalsCreateErrorFromJSON(json: any): ArrivalsCreateError;
export declare function ArrivalsCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsCreateError;
export declare function ArrivalsCreateErrorToJSON(value?: ArrivalsCreateError | null): any;
