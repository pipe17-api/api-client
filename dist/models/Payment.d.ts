/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Payment
 */
export interface Payment {
    /**
     *
     * @type {number}
     * @memberof Payment
     */
    amount: number;
    /**
     *
     * @type {string}
     * @memberof Payment
     */
    currency: string;
    /**
     *
     * @type {number}
     * @memberof Payment
     */
    conversion?: number;
    /**
     *
     * @type {string}
     * @memberof Payment
     */
    status: PaymentStatusEnum;
    /**
     *
     * @type {string}
     * @memberof Payment
     */
    method: PaymentMethodEnum;
    /**
     *
     * @type {string}
     * @memberof Payment
     */
    kind?: PaymentKindEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum PaymentStatusEnum {
    Succeeded = "succeeded",
    Failed = "failed"
} /**
* @export
* @enum {string}
*/
export declare enum PaymentMethodEnum {
    Cash = "cash",
    GiftCard = "giftCard",
    CreditCard = "creditCard",
    StoreCredit = "storeCredit",
    Other = "other"
} /**
* @export
* @enum {string}
*/
export declare enum PaymentKindEnum {
    Visa = "visa",
    Mastercard = "mastercard",
    Amex = "amex",
    Discover = "discover"
}
export declare function PaymentFromJSON(json: any): Payment;
export declare function PaymentFromJSONTyped(json: any, ignoreDiscriminator: boolean): Payment;
export declare function PaymentToJSON(value?: Payment | null): any;
