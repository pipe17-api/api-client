/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentsFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsListError
 */
export interface ShipmentsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ShipmentsListError
     */
    success: ShipmentsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ShipmentsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ShipmentsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ShipmentsFilter}
     * @memberof ShipmentsListError
     */
    filters?: ShipmentsFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentsListErrorSuccessEnum {
    False = "false"
}
export declare function ShipmentsListErrorFromJSON(json: any): ShipmentsListError;
export declare function ShipmentsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsListError;
export declare function ShipmentsListErrorToJSON(value?: ShipmentsListError | null): any;
