/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobStatus, JobSubType, JobType } from './';
/**
 *
 * @export
 * @interface JobsListFilter
 */
export interface JobsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof JobsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof JobsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof JobsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof JobsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof JobsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof JobsListFilter
     */
    count?: number;
    /**
     * Jobs by list of jobId
     * @type {Array<string>}
     * @memberof JobsListFilter
     */
    jobId?: Array<string>;
    /**
     * Jobs with given type
     * @type {Array<JobType>}
     * @memberof JobsListFilter
     */
    type?: Array<JobType>;
    /**
     * Jobs with given sub-type
     * @type {Array<JobSubType>}
     * @memberof JobsListFilter
     */
    subType?: Array<JobSubType>;
    /**
     * Jobs with given status
     * @type {Array<JobStatus>}
     * @memberof JobsListFilter
     */
    status?: Array<JobStatus>;
    /**
     * List sort order
     * @type {string}
     * @memberof JobsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof JobsListFilter
     */
    keys?: string;
}
export declare function JobsListFilterFromJSON(json: any): JobsListFilter;
export declare function JobsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsListFilter;
export declare function JobsListFilterToJSON(value?: JobsListFilter | null): any;
