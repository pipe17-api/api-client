/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Action for list delete operation
 * @export
 * @enum {string}
 */
export declare enum ListDeleteAction {
    List = "list",
    Soft = "soft",
    Hard = "hard"
}
export declare function ListDeleteActionFromJSON(json: any): ListDeleteAction;
export declare function ListDeleteActionFromJSONTyped(json: any, ignoreDiscriminator: boolean): ListDeleteAction;
export declare function ListDeleteActionToJSON(value?: ListDeleteAction | null): any;
