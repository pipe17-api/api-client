/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MappingsListFilter
 */
export interface MappingsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof MappingsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof MappingsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof MappingsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof MappingsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof MappingsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof MappingsListFilter
     */
    count?: number;
    /**
     * Mappings by list of mappingId
     * @type {Array<string>}
     * @memberof MappingsListFilter
     */
    mappingId?: Array<string>;
    /**
     * Mappings by list of UUIDs
     * @type {Array<string>}
     * @memberof MappingsListFilter
     */
    uuid?: Array<string>;
    /**
     * Fetch all mappings matching this description
     * @type {string}
     * @memberof MappingsListFilter
     */
    desc?: string;
    /**
     * List sort order
     * @type {string}
     * @memberof MappingsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof MappingsListFilter
     */
    keys?: string;
}
export declare function MappingsListFilterFromJSON(json: any): MappingsListFilter;
export declare function MappingsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsListFilter;
export declare function MappingsListFilterToJSON(value?: MappingsListFilter | null): any;
