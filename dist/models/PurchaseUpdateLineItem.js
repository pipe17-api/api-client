"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseUpdateLineItemToJSON = exports.PurchaseUpdateLineItemFromJSONTyped = exports.PurchaseUpdateLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function PurchaseUpdateLineItemFromJSON(json) {
    return PurchaseUpdateLineItemFromJSONTyped(json, false);
}
exports.PurchaseUpdateLineItemFromJSON = PurchaseUpdateLineItemFromJSON;
function PurchaseUpdateLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': !runtime_1.exists(json, 'uniqueId') ? undefined : json['uniqueId'],
        'partId': !runtime_1.exists(json, 'partId') ? undefined : json['partId'],
        'quantity': !runtime_1.exists(json, 'quantity') ? undefined : json['quantity'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'price': !runtime_1.exists(json, 'price') ? undefined : json['price'],
        'shippedQuantity': !runtime_1.exists(json, 'shippedQuantity') ? undefined : json['shippedQuantity'],
        'receivedQuantity': !runtime_1.exists(json, 'receivedQuantity') ? undefined : json['receivedQuantity'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
    };
}
exports.PurchaseUpdateLineItemFromJSONTyped = PurchaseUpdateLineItemFromJSONTyped;
function PurchaseUpdateLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'partId': value.partId,
        'quantity': value.quantity,
        'sku': value.sku,
        'name': value.name,
        'upc': value.upc,
        'price': value.price,
        'shippedQuantity': value.shippedQuantity,
        'receivedQuantity': value.receivedQuantity,
        'locationId': value.locationId,
    };
}
exports.PurchaseUpdateLineItemToJSON = PurchaseUpdateLineItemToJSON;
