/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentCreateResult } from './';
/**
 *
 * @export
 * @interface FulfillmentsCreateResponseAllOf
 */
export interface FulfillmentsCreateResponseAllOf {
    /**
     *
     * @type {Array<FulfillmentCreateResult>}
     * @memberof FulfillmentsCreateResponseAllOf
     */
    fulfillments?: Array<FulfillmentCreateResult>;
}
export declare function FulfillmentsCreateResponseAllOfFromJSON(json: any): FulfillmentsCreateResponseAllOf;
export declare function FulfillmentsCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsCreateResponseAllOf;
export declare function FulfillmentsCreateResponseAllOfToJSON(value?: FulfillmentsCreateResponseAllOf | null): any;
