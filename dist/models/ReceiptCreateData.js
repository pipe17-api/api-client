"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptCreateDataToJSON = exports.ReceiptCreateDataFromJSONTyped = exports.ReceiptCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReceiptCreateDataFromJSON(json) {
    return ReceiptCreateDataFromJSONTyped(json, false);
}
exports.ReceiptCreateDataFromJSON = ReceiptCreateDataFromJSON;
function ReceiptCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrivalId': json['arrivalId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'currency': !runtime_1.exists(json, 'currency') ? undefined : json['currency'],
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (new Date(json['actualArrivalDate'])),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ReceiptItemFromJSON)),
        'extReceiptId': !runtime_1.exists(json, 'extReceiptId') ? undefined : json['extReceiptId'],
    };
}
exports.ReceiptCreateDataFromJSONTyped = ReceiptCreateDataFromJSONTyped;
function ReceiptCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrivalId': value.arrivalId,
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'currency': value.currency,
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (new Date(value.actualArrivalDate).toISOString()),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ReceiptItemToJSON)),
        'extReceiptId': value.extReceiptId,
    };
}
exports.ReceiptCreateDataToJSON = ReceiptCreateDataToJSON;
