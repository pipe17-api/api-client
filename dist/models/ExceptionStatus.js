"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionStatusToJSON = exports.ExceptionStatusFromJSONTyped = exports.ExceptionStatusFromJSON = exports.ExceptionStatus = void 0;
/**
 * Exception status
 * @export
 * @enum {string}
 */
var ExceptionStatus;
(function (ExceptionStatus) {
    ExceptionStatus["Active"] = "active";
    ExceptionStatus["Dismissed"] = "dismissed";
    ExceptionStatus["Resolved"] = "resolved";
})(ExceptionStatus = exports.ExceptionStatus || (exports.ExceptionStatus = {}));
function ExceptionStatusFromJSON(json) {
    return ExceptionStatusFromJSONTyped(json, false);
}
exports.ExceptionStatusFromJSON = ExceptionStatusFromJSON;
function ExceptionStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ExceptionStatusFromJSONTyped = ExceptionStatusFromJSONTyped;
function ExceptionStatusToJSON(value) {
    return value;
}
exports.ExceptionStatusToJSON = ExceptionStatusToJSON;
