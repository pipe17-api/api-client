"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryDeleteResponseAllOfToJSON = exports.InventoryDeleteResponseAllOfFromJSONTyped = exports.InventoryDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryDeleteResponseAllOfFromJSON(json) {
    return InventoryDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.InventoryDeleteResponseAllOfFromJSON = InventoryDeleteResponseAllOfFromJSON;
function InventoryDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryDeleteFilterFromJSON(json['filters']),
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryDeleteResponseAllOfFromJSONTyped = InventoryDeleteResponseAllOfFromJSONTyped;
function InventoryDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.InventoryDeleteFilterToJSON(value.filters),
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryDeleteResponseAllOfToJSON = InventoryDeleteResponseAllOfToJSON;
