/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionFetchError
 */
export interface ExceptionFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionFetchError
     */
    success: ExceptionFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFetchErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionFetchErrorFromJSON(json: any): ExceptionFetchError;
export declare function ExceptionFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFetchError;
export declare function ExceptionFetchErrorToJSON(value?: ExceptionFetchError | null): any;
