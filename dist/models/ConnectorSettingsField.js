"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSettingsFieldToJSON = exports.ConnectorSettingsFieldFromJSONTyped = exports.ConnectorSettingsFieldFromJSON = exports.ConnectorSettingsFieldTypeEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorSettingsFieldTypeEnum;
(function (ConnectorSettingsFieldTypeEnum) {
    ConnectorSettingsFieldTypeEnum["String"] = "string";
    ConnectorSettingsFieldTypeEnum["Number"] = "number";
    ConnectorSettingsFieldTypeEnum["Boolean"] = "boolean";
    ConnectorSettingsFieldTypeEnum["Date"] = "date";
    ConnectorSettingsFieldTypeEnum["Array"] = "array";
    ConnectorSettingsFieldTypeEnum["Object"] = "object";
    ConnectorSettingsFieldTypeEnum["Json"] = "json";
})(ConnectorSettingsFieldTypeEnum = exports.ConnectorSettingsFieldTypeEnum || (exports.ConnectorSettingsFieldTypeEnum = {}));
function ConnectorSettingsFieldFromJSON(json) {
    return ConnectorSettingsFieldFromJSONTyped(json, false);
}
exports.ConnectorSettingsFieldFromJSON = ConnectorSettingsFieldFromJSON;
function ConnectorSettingsFieldFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'path': !runtime_1.exists(json, 'path') ? undefined : json['path'],
        'type': !runtime_1.exists(json, 'type') ? undefined : json['type'],
        'secret': !runtime_1.exists(json, 'secret') ? undefined : json['secret'],
        'group': !runtime_1.exists(json, 'group') ? undefined : json['group'],
        'options': !runtime_1.exists(json, 'options') ? undefined : _1.ConnectorSettingsOptionsFromJSON(json['options']),
        'validation': !runtime_1.exists(json, 'validation') ? undefined : json['validation'],
    };
}
exports.ConnectorSettingsFieldFromJSONTyped = ConnectorSettingsFieldFromJSONTyped;
function ConnectorSettingsFieldToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'path': value.path,
        'type': value.type,
        'secret': value.secret,
        'group': value.group,
        'options': _1.ConnectorSettingsOptionsToJSON(value.options),
        'validation': value.validation,
    };
}
exports.ConnectorSettingsFieldToJSON = ConnectorSettingsFieldToJSON;
