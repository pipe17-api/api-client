"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationStatusToJSON = exports.LocationStatusFromJSONTyped = exports.LocationStatusFromJSON = exports.LocationStatus = void 0;
/**
 * Location status
 * @export
 * @enum {string}
 */
var LocationStatus;
(function (LocationStatus) {
    LocationStatus["Active"] = "active";
    LocationStatus["Inactive"] = "inactive";
    LocationStatus["Pending"] = "pending";
    LocationStatus["Deleted"] = "deleted";
})(LocationStatus = exports.LocationStatus || (exports.LocationStatus = {}));
function LocationStatusFromJSON(json) {
    return LocationStatusFromJSONTyped(json, false);
}
exports.LocationStatusFromJSON = LocationStatusFromJSON;
function LocationStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.LocationStatusFromJSONTyped = LocationStatusFromJSONTyped;
function LocationStatusToJSON(value) {
    return value;
}
exports.LocationStatusToJSON = LocationStatusToJSON;
