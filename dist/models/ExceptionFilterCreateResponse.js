"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterCreateResponseToJSON = exports.ExceptionFilterCreateResponseFromJSONTyped = exports.ExceptionFilterCreateResponseFromJSON = exports.ExceptionFilterCreateResponseCodeEnum = exports.ExceptionFilterCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionFilterCreateResponseSuccessEnum;
(function (ExceptionFilterCreateResponseSuccessEnum) {
    ExceptionFilterCreateResponseSuccessEnum["True"] = "true";
})(ExceptionFilterCreateResponseSuccessEnum = exports.ExceptionFilterCreateResponseSuccessEnum || (exports.ExceptionFilterCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionFilterCreateResponseCodeEnum;
(function (ExceptionFilterCreateResponseCodeEnum) {
    ExceptionFilterCreateResponseCodeEnum[ExceptionFilterCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionFilterCreateResponseCodeEnum[ExceptionFilterCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionFilterCreateResponseCodeEnum[ExceptionFilterCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionFilterCreateResponseCodeEnum = exports.ExceptionFilterCreateResponseCodeEnum || (exports.ExceptionFilterCreateResponseCodeEnum = {}));
function ExceptionFilterCreateResponseFromJSON(json) {
    return ExceptionFilterCreateResponseFromJSONTyped(json, false);
}
exports.ExceptionFilterCreateResponseFromJSON = ExceptionFilterCreateResponseFromJSON;
function ExceptionFilterCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'exceptionFilter': !runtime_1.exists(json, 'exceptionFilter') ? undefined : _1.ExceptionFilterFromJSON(json['exceptionFilter']),
    };
}
exports.ExceptionFilterCreateResponseFromJSONTyped = ExceptionFilterCreateResponseFromJSONTyped;
function ExceptionFilterCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'exceptionFilter': _1.ExceptionFilterToJSON(value.exceptionFilter),
    };
}
exports.ExceptionFilterCreateResponseToJSON = ExceptionFilterCreateResponseToJSON;
