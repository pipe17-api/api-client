"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsDeleteResponseAllOfToJSON = exports.ReturnsDeleteResponseAllOfFromJSONTyped = exports.ReturnsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnsDeleteResponseAllOfFromJSON(json) {
    return ReturnsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.ReturnsDeleteResponseAllOfFromJSON = ReturnsDeleteResponseAllOfFromJSON;
function ReturnsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReturnsDeleteFilterFromJSON(json['filters']),
        'suppliers': !runtime_1.exists(json, 'suppliers') ? undefined : (json['suppliers'].map(_1.ReturnFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ReturnsDeleteResponseAllOfFromJSONTyped = ReturnsDeleteResponseAllOfFromJSONTyped;
function ReturnsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ReturnsDeleteFilterToJSON(value.filters),
        'suppliers': value.suppliers === undefined ? undefined : (value.suppliers.map(_1.ReturnToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ReturnsDeleteResponseAllOfToJSON = ReturnsDeleteResponseAllOfToJSON;
