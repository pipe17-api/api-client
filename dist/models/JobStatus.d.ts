/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Job status
 * @export
 * @enum {string}
 */
export declare enum JobStatus {
    Submitted = "submitted",
    Processing = "processing",
    Completed = "completed",
    Canceled = "canceled",
    Failed = "failed"
}
export declare function JobStatusFromJSON(json: any): JobStatus;
export declare function JobStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobStatus;
export declare function JobStatusToJSON(value?: JobStatus | null): any;
