/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingsListFilter } from './';
/**
 *
 * @export
 * @interface RoutingsListError
 */
export interface RoutingsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoutingsListError
     */
    success: RoutingsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoutingsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoutingsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RoutingsListFilter}
     * @memberof RoutingsListError
     */
    filters?: RoutingsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingsListErrorSuccessEnum {
    False = "false"
}
export declare function RoutingsListErrorFromJSON(json: any): RoutingsListError;
export declare function RoutingsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsListError;
export declare function RoutingsListErrorToJSON(value?: RoutingsListError | null): any;
