/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalStatus } from './';
/**
 * Arrivals Filter
 * @export
 * @interface ArrivalsFilterAllOf
 */
export interface ArrivalsFilterAllOf {
    /**
     * Arrivals by list of arrivalId
     * @type {Array<string>}
     * @memberof ArrivalsFilterAllOf
     */
    arrivalId?: Array<string>;
    /**
     * Arrivals by list transfer orders
     * @type {Array<string>}
     * @memberof ArrivalsFilterAllOf
     */
    transferId?: Array<string>;
    /**
     * Arrivals by list purchase orders
     * @type {Array<string>}
     * @memberof ArrivalsFilterAllOf
     */
    purchaseId?: Array<string>;
    /**
     * Arrivals by list external orders (PO or TO)
     * @type {Array<string>}
     * @memberof ArrivalsFilterAllOf
     */
    extOrderId?: Array<string>;
    /**
     * Arrivals by list fulfillmentId
     * @type {Array<string>}
     * @memberof ArrivalsFilterAllOf
     */
    fulfillmentId?: Array<string>;
    /**
     * Arrivals in specific status
     * @type {Array<ArrivalStatus>}
     * @memberof ArrivalsFilterAllOf
     */
    status?: Array<ArrivalStatus>;
    /**
     * Soft deleted arrivals
     * @type {boolean}
     * @memberof ArrivalsFilterAllOf
     */
    deleted?: boolean;
}
export declare function ArrivalsFilterAllOfFromJSON(json: any): ArrivalsFilterAllOf;
export declare function ArrivalsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsFilterAllOf;
export declare function ArrivalsFilterAllOfToJSON(value?: ArrivalsFilterAllOf | null): any;
