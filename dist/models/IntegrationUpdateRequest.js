"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationUpdateRequestToJSON = exports.IntegrationUpdateRequestFromJSONTyped = exports.IntegrationUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationUpdateRequestFromJSON(json) {
    return IntegrationUpdateRequestFromJSONTyped(json, false);
}
exports.IntegrationUpdateRequestFromJSON = IntegrationUpdateRequestFromJSON;
function IntegrationUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.IntegrationStatusFromJSON(json['status']),
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : json['apikey'],
        'integrationName': !runtime_1.exists(json, 'integrationName') ? undefined : json['integrationName'],
        'entities': !runtime_1.exists(json, 'entities') ? undefined : (json['entities'].map(_1.IntegrationEntityFromJSON)),
        'settings': !runtime_1.exists(json, 'settings') ? undefined : _1.IntegrationSettingsFromJSON(json['settings']),
        'connection': !runtime_1.exists(json, 'connection') ? undefined : _1.IntegrationConnectionFromJSON(json['connection']),
        'errorText': !runtime_1.exists(json, 'errorText') ? undefined : json['errorText'],
    };
}
exports.IntegrationUpdateRequestFromJSONTyped = IntegrationUpdateRequestFromJSONTyped;
function IntegrationUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.IntegrationStatusToJSON(value.status),
        'apikey': value.apikey,
        'integrationName': value.integrationName,
        'entities': value.entities === undefined ? undefined : (value.entities.map(_1.IntegrationEntityToJSON)),
        'settings': _1.IntegrationSettingsToJSON(value.settings),
        'connection': _1.IntegrationConnectionToJSON(value.connection),
        'errorText': value.errorText,
    };
}
exports.IntegrationUpdateRequestToJSON = IntegrationUpdateRequestToJSON;
