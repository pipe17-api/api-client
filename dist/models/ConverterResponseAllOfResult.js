"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConverterResponseAllOfResultToJSON = exports.ConverterResponseAllOfResultFromJSONTyped = exports.ConverterResponseAllOfResultFromJSON = void 0;
const runtime_1 = require("../runtime");
function ConverterResponseAllOfResultFromJSON(json) {
    return ConverterResponseAllOfResultFromJSONTyped(json, false);
}
exports.ConverterResponseAllOfResultFromJSON = ConverterResponseAllOfResultFromJSON;
function ConverterResponseAllOfResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'body': !runtime_1.exists(json, 'body') ? undefined : json['body'],
    };
}
exports.ConverterResponseAllOfResultFromJSONTyped = ConverterResponseAllOfResultFromJSONTyped;
function ConverterResponseAllOfResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'body': value.body,
    };
}
exports.ConverterResponseAllOfResultToJSON = ConverterResponseAllOfResultToJSON;
