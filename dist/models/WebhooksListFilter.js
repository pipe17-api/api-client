"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhooksListFilterToJSON = exports.WebhooksListFilterFromJSONTyped = exports.WebhooksListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function WebhooksListFilterFromJSON(json) {
    return WebhooksListFilterFromJSONTyped(json, false);
}
exports.WebhooksListFilterFromJSON = WebhooksListFilterFromJSON;
function WebhooksListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'webhookId': !runtime_1.exists(json, 'webhookId') ? undefined : json['webhookId'],
        'topic': !runtime_1.exists(json, 'topic') ? undefined : _1.WebhookTopicsFromJSON(json['topic']),
        'related': !runtime_1.exists(json, 'related') ? undefined : json['related'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
    };
}
exports.WebhooksListFilterFromJSONTyped = WebhooksListFilterFromJSONTyped;
function WebhooksListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'webhookId': value.webhookId,
        'topic': _1.WebhookTopicsToJSON(value.topic),
        'related': value.related,
        'order': value.order,
        'keys': value.keys,
    };
}
exports.WebhooksListFilterToJSON = WebhooksListFilterToJSON;
