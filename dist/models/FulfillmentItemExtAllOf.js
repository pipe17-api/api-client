"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentItemExtAllOfToJSON = exports.FulfillmentItemExtAllOfFromJSONTyped = exports.FulfillmentItemExtAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function FulfillmentItemExtAllOfFromJSON(json) {
    return FulfillmentItemExtAllOfFromJSONTyped(json, false);
}
exports.FulfillmentItemExtAllOfFromJSON = FulfillmentItemExtAllOfFromJSON;
function FulfillmentItemExtAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'bundleSKU': !runtime_1.exists(json, 'bundleSKU') ? undefined : json['bundleSKU'],
        'bundleQuantity': !runtime_1.exists(json, 'bundleQuantity') ? undefined : json['bundleQuantity'],
    };
}
exports.FulfillmentItemExtAllOfFromJSONTyped = FulfillmentItemExtAllOfFromJSONTyped;
function FulfillmentItemExtAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'bundleSKU': value.bundleSKU,
        'bundleQuantity': value.bundleQuantity,
    };
}
exports.FulfillmentItemExtAllOfToJSON = FulfillmentItemExtAllOfToJSON;
