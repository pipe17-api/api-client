"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateFetchErrorToJSON = exports.IntegrationStateFetchErrorFromJSONTyped = exports.IntegrationStateFetchErrorFromJSON = exports.IntegrationStateFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var IntegrationStateFetchErrorSuccessEnum;
(function (IntegrationStateFetchErrorSuccessEnum) {
    IntegrationStateFetchErrorSuccessEnum["False"] = "false";
})(IntegrationStateFetchErrorSuccessEnum = exports.IntegrationStateFetchErrorSuccessEnum || (exports.IntegrationStateFetchErrorSuccessEnum = {}));
function IntegrationStateFetchErrorFromJSON(json) {
    return IntegrationStateFetchErrorFromJSONTyped(json, false);
}
exports.IntegrationStateFetchErrorFromJSON = IntegrationStateFetchErrorFromJSON;
function IntegrationStateFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.IntegrationStateFetchErrorFromJSONTyped = IntegrationStateFetchErrorFromJSONTyped;
function IntegrationStateFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.IntegrationStateFetchErrorToJSON = IntegrationStateFetchErrorToJSON;
