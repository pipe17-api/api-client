"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelsDeleteErrorAllOfToJSON = exports.LabelsDeleteErrorAllOfFromJSONTyped = exports.LabelsDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LabelsDeleteErrorAllOfFromJSON(json) {
    return LabelsDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.LabelsDeleteErrorAllOfFromJSON = LabelsDeleteErrorAllOfFromJSON;
function LabelsDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LabelsDeleteFilterFromJSON(json['filters']),
    };
}
exports.LabelsDeleteErrorAllOfFromJSONTyped = LabelsDeleteErrorAllOfFromJSONTyped;
function LabelsDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LabelsDeleteFilterToJSON(value.filters),
    };
}
exports.LabelsDeleteErrorAllOfToJSON = LabelsDeleteErrorAllOfToJSON;
