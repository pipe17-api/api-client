"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalUpdateDataToJSON = exports.ArrivalUpdateDataFromJSONTyped = exports.ArrivalUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalUpdateDataFromJSON(json) {
    return ArrivalUpdateDataFromJSONTyped(json, false);
}
exports.ArrivalUpdateDataFromJSON = ArrivalUpdateDataFromJSON;
function ArrivalUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'senderName': !runtime_1.exists(json, 'senderName') ? undefined : json['senderName'],
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'toLocationId': !runtime_1.exists(json, 'toLocationId') ? undefined : json['toLocationId'],
        'fromLocationId': !runtime_1.exists(json, 'fromLocationId') ? undefined : json['fromLocationId'],
        'fromAddress': !runtime_1.exists(json, 'fromAddress') ? undefined : _1.AddressFromJSON(json['fromAddress']),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingMethod': !runtime_1.exists(json, 'shippingMethod') ? undefined : json['shippingMethod'],
        'shippingNote': !runtime_1.exists(json, 'shippingNote') ? undefined : json['shippingNote'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'totalWeight': !runtime_1.exists(json, 'totalWeight') ? undefined : json['totalWeight'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ArrivalLineItemFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ArrivalStatusFromJSON(json['status']),
        'extArrivalId': !runtime_1.exists(json, 'extArrivalId') ? undefined : json['extArrivalId'],
    };
}
exports.ArrivalUpdateDataFromJSONTyped = ArrivalUpdateDataFromJSONTyped;
function ArrivalUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'senderName': value.senderName,
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'toLocationId': value.toLocationId,
        'fromLocationId': value.fromLocationId,
        'fromAddress': _1.AddressToJSON(value.fromAddress),
        'shippingCarrier': value.shippingCarrier,
        'shippingMethod': value.shippingMethod,
        'shippingNote': value.shippingNote,
        'incoterms': value.incoterms,
        'totalWeight': value.totalWeight,
        'weightUnit': value.weightUnit,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ArrivalLineItemToJSON)),
        'status': _1.ArrivalStatusToJSON(value.status),
        'extArrivalId': value.extArrivalId,
    };
}
exports.ArrivalUpdateDataToJSON = ArrivalUpdateDataToJSON;
