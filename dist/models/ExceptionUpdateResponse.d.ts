/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionUpdateResponse
 */
export interface ExceptionUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionUpdateResponse
     */
    success: ExceptionUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionUpdateResponse
     */
    code: ExceptionUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionUpdateResponseFromJSON(json: any): ExceptionUpdateResponse;
export declare function ExceptionUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionUpdateResponse;
export declare function ExceptionUpdateResponseToJSON(value?: ExceptionUpdateResponse | null): any;
