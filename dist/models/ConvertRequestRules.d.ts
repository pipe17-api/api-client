/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingRule } from './';
/**
 *
 * @export
 * @interface ConvertRequestRules
 */
export interface ConvertRequestRules {
    /**
     * Mapping Rules
     * @type {Array<MappingRule>}
     * @memberof ConvertRequestRules
     */
    mappings?: Array<MappingRule>;
    /**
     * Mapping ID (- ignored if mappings specified ???)
     * @type {string}
     * @memberof ConvertRequestRules
     */
    mappingId?: string;
}
export declare function ConvertRequestRulesFromJSON(json: any): ConvertRequestRules;
export declare function ConvertRequestRulesFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConvertRequestRules;
export declare function ConvertRequestRulesToJSON(value?: ConvertRequestRules | null): any;
