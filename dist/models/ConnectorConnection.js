"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorConnectionToJSON = exports.ConnectorConnectionFromJSONTyped = exports.ConnectorConnectionFromJSON = exports.ConnectorConnectionTypeEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorConnectionTypeEnum;
(function (ConnectorConnectionTypeEnum) {
    ConnectorConnectionTypeEnum["Basic"] = "basic";
    ConnectorConnectionTypeEnum["Apikey"] = "apikey";
    ConnectorConnectionTypeEnum["Appid"] = "appid";
    ConnectorConnectionTypeEnum["Oauth2"] = "oauth2";
})(ConnectorConnectionTypeEnum = exports.ConnectorConnectionTypeEnum || (exports.ConnectorConnectionTypeEnum = {}));
function ConnectorConnectionFromJSON(json) {
    return ConnectorConnectionFromJSONTyped(json, false);
}
exports.ConnectorConnectionFromJSON = ConnectorConnectionFromJSON;
function ConnectorConnectionFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'type': !runtime_1.exists(json, 'type') ? undefined : json['type'],
        'url': !runtime_1.exists(json, 'url') ? undefined : json['url'],
        'fields': !runtime_1.exists(json, 'fields') ? undefined : (json['fields'] === null ? null : json['fields'].map(_1.ConnectorSettingsFieldFromJSON)),
    };
}
exports.ConnectorConnectionFromJSONTyped = ConnectorConnectionFromJSONTyped;
function ConnectorConnectionToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'type': value.type,
        'url': value.url,
        'fields': value.fields === undefined ? undefined : (value.fields === null ? null : value.fields.map(_1.ConnectorSettingsFieldToJSON)),
    };
}
exports.ConnectorConnectionToJSON = ConnectorConnectionToJSON;
