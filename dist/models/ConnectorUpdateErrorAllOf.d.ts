/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorUpdateData } from './';
/**
 *
 * @export
 * @interface ConnectorUpdateErrorAllOf
 */
export interface ConnectorUpdateErrorAllOf {
    /**
     *
     * @type {ConnectorUpdateData}
     * @memberof ConnectorUpdateErrorAllOf
     */
    connector?: ConnectorUpdateData;
}
export declare function ConnectorUpdateErrorAllOfFromJSON(json: any): ConnectorUpdateErrorAllOf;
export declare function ConnectorUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorUpdateErrorAllOf;
export declare function ConnectorUpdateErrorAllOfToJSON(value?: ConnectorUpdateErrorAllOf | null): any;
