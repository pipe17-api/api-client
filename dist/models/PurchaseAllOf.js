"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseAllOfToJSON = exports.PurchaseAllOfFromJSONTyped = exports.PurchaseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function PurchaseAllOfFromJSON(json) {
    return PurchaseAllOfFromJSONTyped(json, false);
}
exports.PurchaseAllOfFromJSON = PurchaseAllOfFromJSON;
function PurchaseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'purchaseId': !runtime_1.exists(json, 'purchaseId') ? undefined : json['purchaseId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.PurchaseAllOfFromJSONTyped = PurchaseAllOfFromJSONTyped;
function PurchaseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'purchaseId': value.purchaseId,
        'integration': value.integration,
    };
}
exports.PurchaseAllOfToJSON = PurchaseAllOfToJSON;
