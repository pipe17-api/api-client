"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsDeleteErrorToJSON = exports.ProductsDeleteErrorFromJSONTyped = exports.ProductsDeleteErrorFromJSON = exports.ProductsDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductsDeleteErrorSuccessEnum;
(function (ProductsDeleteErrorSuccessEnum) {
    ProductsDeleteErrorSuccessEnum["False"] = "false";
})(ProductsDeleteErrorSuccessEnum = exports.ProductsDeleteErrorSuccessEnum || (exports.ProductsDeleteErrorSuccessEnum = {}));
function ProductsDeleteErrorFromJSON(json) {
    return ProductsDeleteErrorFromJSONTyped(json, false);
}
exports.ProductsDeleteErrorFromJSON = ProductsDeleteErrorFromJSON;
function ProductsDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ProductsDeleteFilterFromJSON(json['filters']),
    };
}
exports.ProductsDeleteErrorFromJSONTyped = ProductsDeleteErrorFromJSONTyped;
function ProductsDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.ProductsDeleteFilterToJSON(value.filters),
    };
}
exports.ProductsDeleteErrorToJSON = ProductsDeleteErrorToJSON;
