/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Product } from './';
/**
 *
 * @export
 * @interface ProductCreateResult
 */
export interface ProductCreateResult {
    /**
     *
     * @type {string}
     * @memberof ProductCreateResult
     */
    status?: ProductCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof ProductCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof ProductCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Product}
     * @memberof ProductCreateResult
     */
    product?: Product;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function ProductCreateResultFromJSON(json: any): ProductCreateResult;
export declare function ProductCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductCreateResult;
export declare function ProductCreateResultToJSON(value?: ProductCreateResult | null): any;
