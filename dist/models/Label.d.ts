/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Label
 */
export interface Label {
    /**
     * Label ID
     * @type {string}
     * @memberof Label
     */
    labelId?: string;
    /**
     * Label status
     * @type {string}
     * @memberof Label
     */
    status?: LabelStatusEnum;
    /**
     *
     * @type {string}
     * @memberof Label
     */
    fileName: string;
    /**
     *
     * @type {string}
     * @memberof Label
     */
    contentType: string;
    /**
     *
     * @type {string}
     * @memberof Label
     */
    trackingNumber?: string;
    /**
     *
     * @type {string}
     * @memberof Label
     */
    shippingMethod?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Label
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Label
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Label
     */
    readonly orgKey?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelStatusEnum {
    New = "new",
    Ready = "ready"
}
export declare function LabelFromJSON(json: any): Label;
export declare function LabelFromJSONTyped(json: any, ignoreDiscriminator: boolean): Label;
export declare function LabelToJSON(value?: Label | null): any;
