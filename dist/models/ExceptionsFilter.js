"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionsFilterToJSON = exports.ExceptionsFilterFromJSONTyped = exports.ExceptionsFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
function ExceptionsFilterFromJSON(json) {
    return ExceptionsFilterFromJSONTyped(json, false);
}
exports.ExceptionsFilterFromJSON = ExceptionsFilterFromJSON;
function ExceptionsFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'exceptionId': !runtime_1.exists(json, 'exceptionId') ? undefined : json['exceptionId'],
        'exceptionType': !runtime_1.exists(json, 'exceptionType') ? undefined : json['exceptionType'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'entityType': !runtime_1.exists(json, 'entityType') ? undefined : json['entityType'],
    };
}
exports.ExceptionsFilterFromJSONTyped = ExceptionsFilterFromJSONTyped;
function ExceptionsFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'status': value.status,
        'exceptionId': value.exceptionId,
        'exceptionType': value.exceptionType,
        'entityId': value.entityId,
        'entityType': value.entityType,
    };
}
exports.ExceptionsFilterToJSON = ExceptionsFilterToJSON;
