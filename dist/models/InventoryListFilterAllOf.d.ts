/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Inventory Items Filter
 * @export
 * @interface InventoryListFilterAllOf
 */
export interface InventoryListFilterAllOf {
    /**
     * Return inventory ledger information
     * @type {boolean}
     * @memberof InventoryListFilterAllOf
     */
    ledger?: boolean;
    /**
     * Return inventory totals across all locations
     * @type {boolean}
     * @memberof InventoryListFilterAllOf
     */
    totals?: boolean;
}
export declare function InventoryListFilterAllOfFromJSON(json: any): InventoryListFilterAllOf;
export declare function InventoryListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryListFilterAllOf;
export declare function InventoryListFilterAllOfToJSON(value?: InventoryListFilterAllOf | null): any;
