"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationCreateRequestToJSON = exports.IntegrationCreateRequestFromJSONTyped = exports.IntegrationCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationCreateRequestFromJSON(json) {
    return IntegrationCreateRequestFromJSONTyped(json, false);
}
exports.IntegrationCreateRequestFromJSON = IntegrationCreateRequestFromJSON;
function IntegrationCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integrationName': json['integrationName'],
        'connectorName': !runtime_1.exists(json, 'connectorName') ? undefined : json['connectorName'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'environment': !runtime_1.exists(json, 'environment') ? undefined : _1.EnvFromJSON(json['environment']),
        'entities': !runtime_1.exists(json, 'entities') ? undefined : (json['entities'].map(_1.IntegrationEntityFromJSON)),
        'settings': !runtime_1.exists(json, 'settings') ? undefined : _1.IntegrationSettingsFromJSON(json['settings']),
        'connection': !runtime_1.exists(json, 'connection') ? undefined : _1.IntegrationConnectionFromJSON(json['connection']),
    };
}
exports.IntegrationCreateRequestFromJSONTyped = IntegrationCreateRequestFromJSONTyped;
function IntegrationCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integrationName': value.integrationName,
        'connectorName': value.connectorName,
        'connectorId': value.connectorId,
        'environment': _1.EnvToJSON(value.environment),
        'entities': value.entities === undefined ? undefined : (value.entities.map(_1.IntegrationEntityToJSON)),
        'settings': _1.IntegrationSettingsToJSON(value.settings),
        'connection': _1.IntegrationConnectionToJSON(value.connection),
    };
}
exports.IntegrationCreateRequestToJSON = IntegrationCreateRequestToJSON;
