"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsDeleteErrorAllOfToJSON = exports.ProductsDeleteErrorAllOfFromJSONTyped = exports.ProductsDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductsDeleteErrorAllOfFromJSON(json) {
    return ProductsDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.ProductsDeleteErrorAllOfFromJSON = ProductsDeleteErrorAllOfFromJSON;
function ProductsDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ProductsDeleteFilterFromJSON(json['filters']),
    };
}
exports.ProductsDeleteErrorAllOfFromJSONTyped = ProductsDeleteErrorAllOfFromJSONTyped;
function ProductsDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ProductsDeleteFilterToJSON(value.filters),
    };
}
exports.ProductsDeleteErrorAllOfToJSON = ProductsDeleteErrorAllOfToJSON;
