/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Integration } from './';
/**
 *
 * @export
 * @interface IntegrationCreateResponse
 */
export interface IntegrationCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationCreateResponse
     */
    success: IntegrationCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationCreateResponse
     */
    code: IntegrationCreateResponseCodeEnum;
    /**
     *
     * @type {Integration}
     * @memberof IntegrationCreateResponse
     */
    integration?: Integration;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationCreateResponseFromJSON(json: any): IntegrationCreateResponse;
export declare function IntegrationCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationCreateResponse;
export declare function IntegrationCreateResponseToJSON(value?: IntegrationCreateResponse | null): any;
