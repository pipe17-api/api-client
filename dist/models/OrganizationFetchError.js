"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationFetchErrorToJSON = exports.OrganizationFetchErrorFromJSONTyped = exports.OrganizationFetchErrorFromJSON = exports.OrganizationFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var OrganizationFetchErrorSuccessEnum;
(function (OrganizationFetchErrorSuccessEnum) {
    OrganizationFetchErrorSuccessEnum["False"] = "false";
})(OrganizationFetchErrorSuccessEnum = exports.OrganizationFetchErrorSuccessEnum || (exports.OrganizationFetchErrorSuccessEnum = {}));
function OrganizationFetchErrorFromJSON(json) {
    return OrganizationFetchErrorFromJSONTyped(json, false);
}
exports.OrganizationFetchErrorFromJSON = OrganizationFetchErrorFromJSON;
function OrganizationFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.OrganizationFetchErrorFromJSONTyped = OrganizationFetchErrorFromJSONTyped;
function OrganizationFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.OrganizationFetchErrorToJSON = OrganizationFetchErrorToJSON;
