/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Event } from './';
/**
 *
 * @export
 * @interface EventCreateResponseAllOf
 */
export interface EventCreateResponseAllOf {
    /**
     *
     * @type {Event}
     * @memberof EventCreateResponseAllOf
     */
    event?: Event;
}
export declare function EventCreateResponseAllOfFromJSON(json: any): EventCreateResponseAllOf;
export declare function EventCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventCreateResponseAllOf;
export declare function EventCreateResponseAllOfToJSON(value?: EventCreateResponseAllOf | null): any;
