"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersFilterAllOfToJSON = exports.OrdersFilterAllOfFromJSONTyped = exports.OrdersFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersFilterAllOfFromJSON(json) {
    return OrdersFilterAllOfFromJSONTyped(json, false);
}
exports.OrdersFilterAllOfFromJSON = OrdersFilterAllOfFromJSON;
function OrdersFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'orderSource': !runtime_1.exists(json, 'orderSource') ? undefined : json['orderSource'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.OrderStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.OrdersFilterAllOfFromJSONTyped = OrdersFilterAllOfFromJSONTyped;
function OrdersFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'orderId': value.orderId,
        'extOrderId': value.extOrderId,
        'orderSource': value.orderSource,
        'status': value.status === undefined ? undefined : (value.status.map(_1.OrderStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.OrdersFilterAllOfToJSON = OrdersFilterAllOfToJSON;
