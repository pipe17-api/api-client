"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionCreateResponseToJSON = exports.ExceptionCreateResponseFromJSONTyped = exports.ExceptionCreateResponseFromJSON = exports.ExceptionCreateResponseCodeEnum = exports.ExceptionCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionCreateResponseSuccessEnum;
(function (ExceptionCreateResponseSuccessEnum) {
    ExceptionCreateResponseSuccessEnum["True"] = "true";
})(ExceptionCreateResponseSuccessEnum = exports.ExceptionCreateResponseSuccessEnum || (exports.ExceptionCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionCreateResponseCodeEnum;
(function (ExceptionCreateResponseCodeEnum) {
    ExceptionCreateResponseCodeEnum[ExceptionCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionCreateResponseCodeEnum[ExceptionCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionCreateResponseCodeEnum[ExceptionCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionCreateResponseCodeEnum = exports.ExceptionCreateResponseCodeEnum || (exports.ExceptionCreateResponseCodeEnum = {}));
function ExceptionCreateResponseFromJSON(json) {
    return ExceptionCreateResponseFromJSONTyped(json, false);
}
exports.ExceptionCreateResponseFromJSON = ExceptionCreateResponseFromJSON;
function ExceptionCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionFromJSON(json['exception']),
    };
}
exports.ExceptionCreateResponseFromJSONTyped = ExceptionCreateResponseFromJSONTyped;
function ExceptionCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'exception': _1.ExceptionToJSON(value.exception),
    };
}
exports.ExceptionCreateResponseToJSON = ExceptionCreateResponseToJSON;
