"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingCreateResponseAllOfToJSON = exports.RoutingCreateResponseAllOfFromJSONTyped = exports.RoutingCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoutingCreateResponseAllOfFromJSON(json) {
    return RoutingCreateResponseAllOfFromJSONTyped(json, false);
}
exports.RoutingCreateResponseAllOfFromJSON = RoutingCreateResponseAllOfFromJSON;
function RoutingCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.RoutingFromJSON(json['routing']),
    };
}
exports.RoutingCreateResponseAllOfFromJSONTyped = RoutingCreateResponseAllOfFromJSONTyped;
function RoutingCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routing': _1.RoutingToJSON(value.routing),
    };
}
exports.RoutingCreateResponseAllOfToJSON = RoutingCreateResponseAllOfToJSON;
