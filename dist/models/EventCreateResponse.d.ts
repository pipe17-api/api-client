/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Event } from './';
/**
 *
 * @export
 * @interface EventCreateResponse
 */
export interface EventCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EventCreateResponse
     */
    success: EventCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventCreateResponse
     */
    code: EventCreateResponseCodeEnum;
    /**
     *
     * @type {Event}
     * @memberof EventCreateResponse
     */
    event?: Event;
}
/**
* @export
* @enum {string}
*/
export declare enum EventCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EventCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EventCreateResponseFromJSON(json: any): EventCreateResponse;
export declare function EventCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventCreateResponse;
export declare function EventCreateResponseToJSON(value?: EventCreateResponse | null): any;
