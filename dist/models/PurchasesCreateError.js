"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesCreateErrorToJSON = exports.PurchasesCreateErrorFromJSONTyped = exports.PurchasesCreateErrorFromJSON = exports.PurchasesCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchasesCreateErrorSuccessEnum;
(function (PurchasesCreateErrorSuccessEnum) {
    PurchasesCreateErrorSuccessEnum["False"] = "false";
})(PurchasesCreateErrorSuccessEnum = exports.PurchasesCreateErrorSuccessEnum || (exports.PurchasesCreateErrorSuccessEnum = {}));
function PurchasesCreateErrorFromJSON(json) {
    return PurchasesCreateErrorFromJSONTyped(json, false);
}
exports.PurchasesCreateErrorFromJSON = PurchasesCreateErrorFromJSON;
function PurchasesCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'purchases': !runtime_1.exists(json, 'purchases') ? undefined : (json['purchases'].map(_1.PurchaseCreateResultFromJSON)),
    };
}
exports.PurchasesCreateErrorFromJSONTyped = PurchasesCreateErrorFromJSONTyped;
function PurchasesCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'purchases': value.purchases === undefined ? undefined : (value.purchases.map(_1.PurchaseCreateResultToJSON)),
    };
}
exports.PurchasesCreateErrorToJSON = PurchasesCreateErrorToJSON;
