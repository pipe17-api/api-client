"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsCreateErrorToJSON = exports.ShipmentsCreateErrorFromJSONTyped = exports.ShipmentsCreateErrorFromJSON = exports.ShipmentsCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentsCreateErrorSuccessEnum;
(function (ShipmentsCreateErrorSuccessEnum) {
    ShipmentsCreateErrorSuccessEnum["False"] = "false";
})(ShipmentsCreateErrorSuccessEnum = exports.ShipmentsCreateErrorSuccessEnum || (exports.ShipmentsCreateErrorSuccessEnum = {}));
function ShipmentsCreateErrorFromJSON(json) {
    return ShipmentsCreateErrorFromJSONTyped(json, false);
}
exports.ShipmentsCreateErrorFromJSON = ShipmentsCreateErrorFromJSON;
function ShipmentsCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'shipments': !runtime_1.exists(json, 'shipments') ? undefined : (json['shipments'].map(_1.ShipmentCreateResultFromJSON)),
    };
}
exports.ShipmentsCreateErrorFromJSONTyped = ShipmentsCreateErrorFromJSONTyped;
function ShipmentsCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'shipments': value.shipments === undefined ? undefined : (value.shipments.map(_1.ShipmentCreateResultToJSON)),
    };
}
exports.ShipmentsCreateErrorToJSON = ShipmentsCreateErrorToJSON;
