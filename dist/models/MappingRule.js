"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingRuleToJSON = exports.MappingRuleFromJSONTyped = exports.MappingRuleFromJSON = exports.MappingRuleTypeEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var MappingRuleTypeEnum;
(function (MappingRuleTypeEnum) {
    MappingRuleTypeEnum["String"] = "string";
    MappingRuleTypeEnum["Number"] = "number";
    MappingRuleTypeEnum["Boolean"] = "boolean";
    MappingRuleTypeEnum["Date"] = "date";
})(MappingRuleTypeEnum = exports.MappingRuleTypeEnum || (exports.MappingRuleTypeEnum = {}));
function MappingRuleFromJSON(json) {
    return MappingRuleFromJSONTyped(json, false);
}
exports.MappingRuleFromJSON = MappingRuleFromJSON;
function MappingRuleFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'enabled': json['enabled'],
        'source': json['source'],
        'target': json['target'],
        'type': !runtime_1.exists(json, 'type') ? undefined : json['type'],
        'require': !runtime_1.exists(json, 'require') ? undefined : json['require'],
        'missing': !runtime_1.exists(json, 'missing') ? undefined : json['missing'],
        'default': !runtime_1.exists(json, 'default') ? undefined : json['default'],
        'lookup': !runtime_1.exists(json, 'lookup') ? undefined : json['lookup'],
    };
}
exports.MappingRuleFromJSONTyped = MappingRuleFromJSONTyped;
function MappingRuleToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'enabled': value.enabled,
        'source': value.source,
        'target': value.target,
        'type': value.type,
        'require': value.require,
        'missing': value.missing,
        'default': value.default,
        'lookup': value.lookup,
    };
}
exports.MappingRuleToJSON = MappingRuleToJSON;
