/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival, ArrivalsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ArrivalsListResponseAllOf
 */
export interface ArrivalsListResponseAllOf {
    /**
     *
     * @type {ArrivalsListFilter}
     * @memberof ArrivalsListResponseAllOf
     */
    filters?: ArrivalsListFilter;
    /**
     *
     * @type {Array<Arrival>}
     * @memberof ArrivalsListResponseAllOf
     */
    arrivals: Array<Arrival>;
    /**
     *
     * @type {Pagination}
     * @memberof ArrivalsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ArrivalsListResponseAllOfFromJSON(json: any): ArrivalsListResponseAllOf;
export declare function ArrivalsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsListResponseAllOf;
export declare function ArrivalsListResponseAllOfToJSON(value?: ArrivalsListResponseAllOf | null): any;
