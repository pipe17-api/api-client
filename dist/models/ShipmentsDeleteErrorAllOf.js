"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsDeleteErrorAllOfToJSON = exports.ShipmentsDeleteErrorAllOfFromJSONTyped = exports.ShipmentsDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentsDeleteErrorAllOfFromJSON(json) {
    return ShipmentsDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.ShipmentsDeleteErrorAllOfFromJSON = ShipmentsDeleteErrorAllOfFromJSON;
function ShipmentsDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ShipmentsDeleteFilterFromJSON(json['filters']),
    };
}
exports.ShipmentsDeleteErrorAllOfFromJSONTyped = ShipmentsDeleteErrorAllOfFromJSONTyped;
function ShipmentsDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ShipmentsDeleteFilterToJSON(value.filters),
    };
}
exports.ShipmentsDeleteErrorAllOfToJSON = ShipmentsDeleteErrorAllOfToJSON;
