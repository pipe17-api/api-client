"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalCreateResultToJSON = exports.ArrivalCreateResultFromJSONTyped = exports.ArrivalCreateResultFromJSON = exports.ArrivalCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalCreateResultStatusEnum;
(function (ArrivalCreateResultStatusEnum) {
    ArrivalCreateResultStatusEnum["Submitted"] = "submitted";
    ArrivalCreateResultStatusEnum["Failed"] = "failed";
})(ArrivalCreateResultStatusEnum = exports.ArrivalCreateResultStatusEnum || (exports.ArrivalCreateResultStatusEnum = {}));
function ArrivalCreateResultFromJSON(json) {
    return ArrivalCreateResultFromJSONTyped(json, false);
}
exports.ArrivalCreateResultFromJSON = ArrivalCreateResultFromJSON;
function ArrivalCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'arrival': !runtime_1.exists(json, 'arrival') ? undefined : _1.ArrivalFromJSON(json['arrival']),
    };
}
exports.ArrivalCreateResultFromJSONTyped = ArrivalCreateResultFromJSONTyped;
function ArrivalCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'arrival': _1.ArrivalToJSON(value.arrival),
    };
}
exports.ArrivalCreateResultToJSON = ArrivalCreateResultToJSON;
