/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnUpdateData } from './';
/**
 *
 * @export
 * @interface ReturnUpdateErrorAllOf
 */
export interface ReturnUpdateErrorAllOf {
    /**
     *
     * @type {ReturnUpdateData}
     * @memberof ReturnUpdateErrorAllOf
     */
    _return?: ReturnUpdateData;
}
export declare function ReturnUpdateErrorAllOfFromJSON(json: any): ReturnUpdateErrorAllOf;
export declare function ReturnUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnUpdateErrorAllOf;
export declare function ReturnUpdateErrorAllOfToJSON(value?: ReturnUpdateErrorAllOf | null): any;
