/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Transfers Filter
 * @export
 * @interface TransfersDeleteFilterAllOf
 */
export interface TransfersDeleteFilterAllOf {
    /**
     * Transfers of these integrations
     * @type {Array<string>}
     * @memberof TransfersDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function TransfersDeleteFilterAllOfFromJSON(json: any): TransfersDeleteFilterAllOf;
export declare function TransfersDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersDeleteFilterAllOf;
export declare function TransfersDeleteFilterAllOfToJSON(value?: TransfersDeleteFilterAllOf | null): any;
