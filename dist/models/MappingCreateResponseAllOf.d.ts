/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Mapping } from './';
/**
 *
 * @export
 * @interface MappingCreateResponseAllOf
 */
export interface MappingCreateResponseAllOf {
    /**
     *
     * @type {Mapping}
     * @memberof MappingCreateResponseAllOf
     */
    mapping?: Mapping;
}
export declare function MappingCreateResponseAllOfFromJSON(json: any): MappingCreateResponseAllOf;
export declare function MappingCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingCreateResponseAllOf;
export declare function MappingCreateResponseAllOfToJSON(value?: MappingCreateResponseAllOf | null): any;
