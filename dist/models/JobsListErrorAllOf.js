"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobsListErrorAllOfToJSON = exports.JobsListErrorAllOfFromJSONTyped = exports.JobsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobsListErrorAllOfFromJSON(json) {
    return JobsListErrorAllOfFromJSONTyped(json, false);
}
exports.JobsListErrorAllOfFromJSON = JobsListErrorAllOfFromJSON;
function JobsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.JobsListFilterFromJSON(json['filters']),
    };
}
exports.JobsListErrorAllOfFromJSONTyped = JobsListErrorAllOfFromJSONTyped;
function JobsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.JobsListFilterToJSON(value.filters),
    };
}
exports.JobsListErrorAllOfToJSON = JobsListErrorAllOfToJSON;
