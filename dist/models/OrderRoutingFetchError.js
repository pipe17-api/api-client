"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingFetchErrorToJSON = exports.OrderRoutingFetchErrorFromJSONTyped = exports.OrderRoutingFetchErrorFromJSON = exports.OrderRoutingFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var OrderRoutingFetchErrorSuccessEnum;
(function (OrderRoutingFetchErrorSuccessEnum) {
    OrderRoutingFetchErrorSuccessEnum["False"] = "false";
})(OrderRoutingFetchErrorSuccessEnum = exports.OrderRoutingFetchErrorSuccessEnum || (exports.OrderRoutingFetchErrorSuccessEnum = {}));
function OrderRoutingFetchErrorFromJSON(json) {
    return OrderRoutingFetchErrorFromJSONTyped(json, false);
}
exports.OrderRoutingFetchErrorFromJSON = OrderRoutingFetchErrorFromJSON;
function OrderRoutingFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.OrderRoutingFetchErrorFromJSONTyped = OrderRoutingFetchErrorFromJSONTyped;
function OrderRoutingFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.OrderRoutingFetchErrorToJSON = OrderRoutingFetchErrorToJSON;
