"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemasListFilterToJSON = exports.EntitySchemasListFilterFromJSONTyped = exports.EntitySchemasListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntitySchemasListFilterFromJSON(json) {
    return EntitySchemasListFilterFromJSONTyped(json, false);
}
exports.EntitySchemasListFilterFromJSON = EntitySchemasListFilterFromJSON;
function EntitySchemasListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'schemaId': !runtime_1.exists(json, 'schemaId') ? undefined : json['schemaId'],
        'entity': !runtime_1.exists(json, 'entity') ? undefined : (json['entity'].map(_1.EntityNameFromJSON)),
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
    };
}
exports.EntitySchemasListFilterFromJSONTyped = EntitySchemasListFilterFromJSONTyped;
function EntitySchemasListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'schemaId': value.schemaId,
        'entity': value.entity === undefined ? undefined : (value.entity.map(_1.EntityNameToJSON)),
        'name': value.name,
        'order': value.order,
        'keys': value.keys,
    };
}
exports.EntitySchemasListFilterToJSON = EntitySchemasListFilterToJSON;
