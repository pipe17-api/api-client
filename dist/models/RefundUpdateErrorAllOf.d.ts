/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundUpdateData } from './';
/**
 *
 * @export
 * @interface RefundUpdateErrorAllOf
 */
export interface RefundUpdateErrorAllOf {
    /**
     *
     * @type {RefundUpdateData}
     * @memberof RefundUpdateErrorAllOf
     */
    refund?: RefundUpdateData;
}
export declare function RefundUpdateErrorAllOfFromJSON(json: any): RefundUpdateErrorAllOf;
export declare function RefundUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundUpdateErrorAllOf;
export declare function RefundUpdateErrorAllOfToJSON(value?: RefundUpdateErrorAllOf | null): any;
