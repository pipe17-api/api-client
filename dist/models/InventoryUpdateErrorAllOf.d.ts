/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryUpdateData } from './';
/**
 *
 * @export
 * @interface InventoryUpdateErrorAllOf
 */
export interface InventoryUpdateErrorAllOf {
    /**
     *
     * @type {InventoryUpdateData}
     * @memberof InventoryUpdateErrorAllOf
     */
    inventory?: InventoryUpdateData;
}
export declare function InventoryUpdateErrorAllOfFromJSON(json: any): InventoryUpdateErrorAllOf;
export declare function InventoryUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryUpdateErrorAllOf;
export declare function InventoryUpdateErrorAllOfToJSON(value?: InventoryUpdateErrorAllOf | null): any;
