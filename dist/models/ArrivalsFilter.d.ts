/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalsFilter
 */
export interface ArrivalsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ArrivalsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ArrivalsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ArrivalsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ArrivalsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ArrivalsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ArrivalsFilter
     */
    count?: number;
    /**
     * Arrivals by list of arrivalId
     * @type {Array<string>}
     * @memberof ArrivalsFilter
     */
    arrivalId?: Array<string>;
    /**
     * Arrivals by list transfer orders
     * @type {Array<string>}
     * @memberof ArrivalsFilter
     */
    transferId?: Array<string>;
    /**
     * Arrivals by list purchase orders
     * @type {Array<string>}
     * @memberof ArrivalsFilter
     */
    purchaseId?: Array<string>;
    /**
     * Arrivals by list external orders (PO or TO)
     * @type {Array<string>}
     * @memberof ArrivalsFilter
     */
    extOrderId?: Array<string>;
    /**
     * Arrivals by list fulfillmentId
     * @type {Array<string>}
     * @memberof ArrivalsFilter
     */
    fulfillmentId?: Array<string>;
    /**
     * Arrivals in specific status
     * @type {Array<ArrivalStatus>}
     * @memberof ArrivalsFilter
     */
    status?: Array<ArrivalStatus>;
    /**
     * Soft deleted arrivals
     * @type {boolean}
     * @memberof ArrivalsFilter
     */
    deleted?: boolean;
}
export declare function ArrivalsFilterFromJSON(json: any): ArrivalsFilter;
export declare function ArrivalsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsFilter;
export declare function ArrivalsFilterToJSON(value?: ArrivalsFilter | null): any;
