/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ArrivalLineItem, ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalUpdateRequest
 */
export interface ArrivalUpdateRequest {
    /**
     * Sender's Name
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    senderName?: string;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof ArrivalUpdateRequest
     */
    expectedArrivalDate?: Date;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    toLocationId?: string;
    /**
     * Sender Location ID (for TO)
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    fromLocationId?: string;
    /**
     *
     * @type {Address}
     * @memberof ArrivalUpdateRequest
     */
    fromAddress?: Address;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    shippingCarrier?: string;
    /**
     * Shipping Method
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    shippingMethod?: string;
    /**
     * Shipping Note
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    shippingNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    incoterms?: string;
    /**
     * Total Weight
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    totalWeight?: string;
    /**
     * Weight Unit
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    weightUnit?: string;
    /**
     *
     * @type {Array<ArrivalLineItem>}
     * @memberof ArrivalUpdateRequest
     */
    lineItems?: Array<ArrivalLineItem>;
    /**
     *
     * @type {ArrivalStatus}
     * @memberof ArrivalUpdateRequest
     */
    status?: ArrivalStatus;
    /**
     * Reference to arrival on external system (vendor)
     * @type {string}
     * @memberof ArrivalUpdateRequest
     */
    extArrivalId?: string;
}
export declare function ArrivalUpdateRequestFromJSON(json: any): ArrivalUpdateRequest;
export declare function ArrivalUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalUpdateRequest;
export declare function ArrivalUpdateRequestToJSON(value?: ArrivalUpdateRequest | null): any;
