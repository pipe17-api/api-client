/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ReturnLineItem, ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnCreateData
 */
export interface ReturnCreateData {
    /**
     * Return ID on external system
     * @type {string}
     * @memberof ReturnCreateData
     */
    extReturnId?: string;
    /**
     *
     * @type {ReturnStatus}
     * @memberof ReturnCreateData
     */
    status?: ReturnStatus;
    /**
     *
     * @type {Array<ReturnLineItem>}
     * @memberof ReturnCreateData
     */
    lineItems?: Array<ReturnLineItem>;
    /**
     *
     * @type {Address}
     * @memberof ReturnCreateData
     */
    shippingAddress?: Address;
    /**
     * Currency value, e.g. 'USD'
     * @type {string}
     * @memberof ReturnCreateData
     */
    currency?: string;
    /**
     * Customer notes
     * @type {string}
     * @memberof ReturnCreateData
     */
    customerNotes?: string;
    /**
     * Merchant notes
     * @type {string}
     * @memberof ReturnCreateData
     */
    notes?: string;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    tax?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    discount?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    subTotal?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    total?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    shippingPrice?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    shippingRefund?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    shippingQuote?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    shippingLabelFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    restockingFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateData
     */
    estimatedTotal?: number;
    /**
     *
     * @type {boolean}
     * @memberof ReturnCreateData
     */
    isExchange?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnCreateData
     */
    isGift?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnCreateData
     */
    requiresShipping?: boolean;
    /**
     *
     * @type {Date}
     * @memberof ReturnCreateData
     */
    refundedAt?: Date;
}
export declare function ReturnCreateDataFromJSON(json: any): ReturnCreateData;
export declare function ReturnCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnCreateData;
export declare function ReturnCreateDataToJSON(value?: ReturnCreateData | null): any;
