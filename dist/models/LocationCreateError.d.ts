/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationCreateData } from './';
/**
 *
 * @export
 * @interface LocationCreateError
 */
export interface LocationCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LocationCreateError
     */
    success: LocationCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LocationCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LocationCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {LocationCreateData}
     * @memberof LocationCreateError
     */
    location?: LocationCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationCreateErrorSuccessEnum {
    False = "false"
}
export declare function LocationCreateErrorFromJSON(json: any): LocationCreateError;
export declare function LocationCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationCreateError;
export declare function LocationCreateErrorToJSON(value?: LocationCreateError | null): any;
