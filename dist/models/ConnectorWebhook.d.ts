/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface ConnectorWebhook
 */
export interface ConnectorWebhook {
    /**
     * Webhook URL
     * @type {string}
     * @memberof ConnectorWebhook
     */
    url: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof ConnectorWebhook
     */
    apikey: string;
    /**
     * Webhook topics
     * @type {Array<WebhookTopics>}
     * @memberof ConnectorWebhook
     */
    topics: Array<WebhookTopics>;
}
export declare function ConnectorWebhookFromJSON(json: any): ConnectorWebhook;
export declare function ConnectorWebhookFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorWebhook;
export declare function ConnectorWebhookToJSON(value?: ConnectorWebhook | null): any;
