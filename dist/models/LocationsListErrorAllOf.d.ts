/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationsListFilter } from './';
/**
 *
 * @export
 * @interface LocationsListErrorAllOf
 */
export interface LocationsListErrorAllOf {
    /**
     *
     * @type {LocationsListFilter}
     * @memberof LocationsListErrorAllOf
     */
    filters?: LocationsListFilter;
}
export declare function LocationsListErrorAllOfFromJSON(json: any): LocationsListErrorAllOf;
export declare function LocationsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsListErrorAllOf;
export declare function LocationsListErrorAllOfToJSON(value?: LocationsListErrorAllOf | null): any;
