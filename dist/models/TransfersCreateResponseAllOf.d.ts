/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferCreateResult } from './';
/**
 *
 * @export
 * @interface TransfersCreateResponseAllOf
 */
export interface TransfersCreateResponseAllOf {
    /**
     *
     * @type {Array<TransferCreateResult>}
     * @memberof TransfersCreateResponseAllOf
     */
    transfers?: Array<TransferCreateResult>;
}
export declare function TransfersCreateResponseAllOfFromJSON(json: any): TransfersCreateResponseAllOf;
export declare function TransfersCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersCreateResponseAllOf;
export declare function TransfersCreateResponseAllOfToJSON(value?: TransfersCreateResponseAllOf | null): any;
