/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobsDeleteFilter } from './';
/**
 *
 * @export
 * @interface JobsDeleteErrorAllOf
 */
export interface JobsDeleteErrorAllOf {
    /**
     *
     * @type {JobsDeleteFilter}
     * @memberof JobsDeleteErrorAllOf
     */
    filters?: JobsDeleteFilter;
}
export declare function JobsDeleteErrorAllOfFromJSON(json: any): JobsDeleteErrorAllOf;
export declare function JobsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsDeleteErrorAllOf;
export declare function JobsDeleteErrorAllOfToJSON(value?: JobsDeleteErrorAllOf | null): any;
