/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobsListFilter } from './';
/**
 *
 * @export
 * @interface JobsListError
 */
export interface JobsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof JobsListError
     */
    success: JobsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof JobsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof JobsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {JobsListFilter}
     * @memberof JobsListError
     */
    filters?: JobsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum JobsListErrorSuccessEnum {
    False = "false"
}
export declare function JobsListErrorFromJSON(json: any): JobsListError;
export declare function JobsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsListError;
export declare function JobsListErrorToJSON(value?: JobsListError | null): any;
