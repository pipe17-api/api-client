/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserUpdateData } from './';
/**
 *
 * @export
 * @interface UserUpdateError
 */
export interface UserUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof UserUpdateError
     */
    success: UserUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UserUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof UserUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof UserUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {UserUpdateData}
     * @memberof UserUpdateError
     */
    user?: UserUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum UserUpdateErrorSuccessEnum {
    False = "false"
}
export declare function UserUpdateErrorFromJSON(json: any): UserUpdateError;
export declare function UserUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserUpdateError;
export declare function UserUpdateErrorToJSON(value?: UserUpdateError | null): any;
