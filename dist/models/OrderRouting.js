"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingToJSON = exports.OrderRoutingFromJSONTyped = exports.OrderRoutingFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrderRoutingFromJSON(json) {
    return OrderRoutingFromJSONTyped(json, false);
}
exports.OrderRoutingFromJSON = OrderRoutingFromJSON;
function OrderRoutingFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routingId': !runtime_1.exists(json, 'routingId') ? undefined : json['routingId'],
        'rules': json['rules'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.OrderRoutingFromJSONTyped = OrderRoutingFromJSONTyped;
function OrderRoutingToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routingId': value.routingId,
        'rules': value.rules,
    };
}
exports.OrderRoutingToJSON = OrderRoutingToJSON;
