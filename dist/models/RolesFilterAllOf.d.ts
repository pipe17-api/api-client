/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleStatus } from './';
/**
 * Roles Filter
 * @export
 * @interface RolesFilterAllOf
 */
export interface RolesFilterAllOf {
    /**
     * Roles by list of roleId
     * @type {Array<string>}
     * @memberof RolesFilterAllOf
     */
    roleId?: Array<string>;
    /**
     * Roles whose name contains this string
     * @type {string}
     * @memberof RolesFilterAllOf
     */
    name?: string;
    /**
     * Roles by list of statuses
     * @type {Array<RoleStatus>}
     * @memberof RolesFilterAllOf
     */
    status?: Array<RoleStatus>;
}
export declare function RolesFilterAllOfFromJSON(json: any): RolesFilterAllOf;
export declare function RolesFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesFilterAllOf;
export declare function RolesFilterAllOfToJSON(value?: RolesFilterAllOf | null): any;
