"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferUpdateResponseToJSON = exports.TransferUpdateResponseFromJSONTyped = exports.TransferUpdateResponseFromJSON = exports.TransferUpdateResponseCodeEnum = exports.TransferUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var TransferUpdateResponseSuccessEnum;
(function (TransferUpdateResponseSuccessEnum) {
    TransferUpdateResponseSuccessEnum["True"] = "true";
})(TransferUpdateResponseSuccessEnum = exports.TransferUpdateResponseSuccessEnum || (exports.TransferUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TransferUpdateResponseCodeEnum;
(function (TransferUpdateResponseCodeEnum) {
    TransferUpdateResponseCodeEnum[TransferUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TransferUpdateResponseCodeEnum[TransferUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TransferUpdateResponseCodeEnum[TransferUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TransferUpdateResponseCodeEnum = exports.TransferUpdateResponseCodeEnum || (exports.TransferUpdateResponseCodeEnum = {}));
function TransferUpdateResponseFromJSON(json) {
    return TransferUpdateResponseFromJSONTyped(json, false);
}
exports.TransferUpdateResponseFromJSON = TransferUpdateResponseFromJSON;
function TransferUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.TransferUpdateResponseFromJSONTyped = TransferUpdateResponseFromJSONTyped;
function TransferUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.TransferUpdateResponseToJSON = TransferUpdateResponseToJSON;
