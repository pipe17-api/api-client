/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location, LocationsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface LocationsDeleteResponseAllOf
 */
export interface LocationsDeleteResponseAllOf {
    /**
     *
     * @type {LocationsDeleteFilter}
     * @memberof LocationsDeleteResponseAllOf
     */
    filters?: LocationsDeleteFilter;
    /**
     *
     * @type {Array<Location>}
     * @memberof LocationsDeleteResponseAllOf
     */
    locations?: Array<Location>;
    /**
     * Number of deleted locations
     * @type {number}
     * @memberof LocationsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof LocationsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function LocationsDeleteResponseAllOfFromJSON(json: any): LocationsDeleteResponseAllOf;
export declare function LocationsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsDeleteResponseAllOf;
export declare function LocationsDeleteResponseAllOfToJSON(value?: LocationsDeleteResponseAllOf | null): any;
