"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyFetchResponseToJSON = exports.ApiKeyFetchResponseFromJSONTyped = exports.ApiKeyFetchResponseFromJSON = exports.ApiKeyFetchResponseCodeEnum = exports.ApiKeyFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ApiKeyFetchResponseSuccessEnum;
(function (ApiKeyFetchResponseSuccessEnum) {
    ApiKeyFetchResponseSuccessEnum["True"] = "true";
})(ApiKeyFetchResponseSuccessEnum = exports.ApiKeyFetchResponseSuccessEnum || (exports.ApiKeyFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ApiKeyFetchResponseCodeEnum;
(function (ApiKeyFetchResponseCodeEnum) {
    ApiKeyFetchResponseCodeEnum[ApiKeyFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ApiKeyFetchResponseCodeEnum[ApiKeyFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ApiKeyFetchResponseCodeEnum[ApiKeyFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ApiKeyFetchResponseCodeEnum = exports.ApiKeyFetchResponseCodeEnum || (exports.ApiKeyFetchResponseCodeEnum = {}));
function ApiKeyFetchResponseFromJSON(json) {
    return ApiKeyFetchResponseFromJSONTyped(json, false);
}
exports.ApiKeyFetchResponseFromJSON = ApiKeyFetchResponseFromJSON;
function ApiKeyFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : _1.ApiKeyFromJSON(json['apikey']),
    };
}
exports.ApiKeyFetchResponseFromJSONTyped = ApiKeyFetchResponseFromJSONTyped;
function ApiKeyFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'apikey': _1.ApiKeyToJSON(value.apikey),
    };
}
exports.ApiKeyFetchResponseToJSON = ApiKeyFetchResponseToJSON;
