"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentFetchResponseAllOfToJSON = exports.ShipmentFetchResponseAllOfFromJSONTyped = exports.ShipmentFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentFetchResponseAllOfFromJSON(json) {
    return ShipmentFetchResponseAllOfFromJSONTyped(json, false);
}
exports.ShipmentFetchResponseAllOfFromJSON = ShipmentFetchResponseAllOfFromJSON;
function ShipmentFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipment': !runtime_1.exists(json, 'shipment') ? undefined : _1.ShipmentFromJSON(json['shipment']),
    };
}
exports.ShipmentFetchResponseAllOfFromJSONTyped = ShipmentFetchResponseAllOfFromJSONTyped;
function ShipmentFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipment': _1.ShipmentToJSON(value.shipment),
    };
}
exports.ShipmentFetchResponseAllOfToJSON = ShipmentFetchResponseAllOfToJSON;
