"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentUpdateResponseToJSON = exports.ShipmentUpdateResponseFromJSONTyped = exports.ShipmentUpdateResponseFromJSON = exports.ShipmentUpdateResponseCodeEnum = exports.ShipmentUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ShipmentUpdateResponseSuccessEnum;
(function (ShipmentUpdateResponseSuccessEnum) {
    ShipmentUpdateResponseSuccessEnum["True"] = "true";
})(ShipmentUpdateResponseSuccessEnum = exports.ShipmentUpdateResponseSuccessEnum || (exports.ShipmentUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ShipmentUpdateResponseCodeEnum;
(function (ShipmentUpdateResponseCodeEnum) {
    ShipmentUpdateResponseCodeEnum[ShipmentUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ShipmentUpdateResponseCodeEnum[ShipmentUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ShipmentUpdateResponseCodeEnum[ShipmentUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ShipmentUpdateResponseCodeEnum = exports.ShipmentUpdateResponseCodeEnum || (exports.ShipmentUpdateResponseCodeEnum = {}));
function ShipmentUpdateResponseFromJSON(json) {
    return ShipmentUpdateResponseFromJSONTyped(json, false);
}
exports.ShipmentUpdateResponseFromJSON = ShipmentUpdateResponseFromJSON;
function ShipmentUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ShipmentUpdateResponseFromJSONTyped = ShipmentUpdateResponseFromJSONTyped;
function ShipmentUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ShipmentUpdateResponseToJSON = ShipmentUpdateResponseToJSON;
