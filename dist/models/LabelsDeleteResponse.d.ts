/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label, LabelsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface LabelsDeleteResponse
 */
export interface LabelsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LabelsDeleteResponse
     */
    success: LabelsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelsDeleteResponse
     */
    code: LabelsDeleteResponseCodeEnum;
    /**
     *
     * @type {LabelsDeleteFilter}
     * @memberof LabelsDeleteResponse
     */
    filters?: LabelsDeleteFilter;
    /**
     *
     * @type {Array<Label>}
     * @memberof LabelsDeleteResponse
     */
    labels?: Array<Label>;
    /**
     * Number of deleted labels
     * @type {number}
     * @memberof LabelsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof LabelsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LabelsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LabelsDeleteResponseFromJSON(json: any): LabelsDeleteResponse;
export declare function LabelsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsDeleteResponse;
export declare function LabelsDeleteResponseToJSON(value?: LabelsDeleteResponse | null): any;
