/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderCreateResult } from './';
/**
 *
 * @export
 * @interface OrdersCreateResponse
 */
export interface OrdersCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrdersCreateResponse
     */
    success: OrdersCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrdersCreateResponse
     */
    code: OrdersCreateResponseCodeEnum;
    /**
     *
     * @type {Array<OrderCreateResult>}
     * @memberof OrdersCreateResponse
     */
    orders?: Array<OrderCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum OrdersCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrdersCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrdersCreateResponseFromJSON(json: any): OrdersCreateResponse;
export declare function OrdersCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersCreateResponse;
export declare function OrdersCreateResponseToJSON(value?: OrdersCreateResponse | null): any;
