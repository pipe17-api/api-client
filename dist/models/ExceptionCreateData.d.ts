/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName, ExceptionType } from './';
/**
 *
 * @export
 * @interface ExceptionCreateData
 */
export interface ExceptionCreateData {
    /**
     *
     * @type {ExceptionType}
     * @memberof ExceptionCreateData
     */
    exceptionType: ExceptionType;
    /**
     * Exception specific error message/details
     * @type {string}
     * @memberof ExceptionCreateData
     */
    exceptionDetails?: string;
    /**
     * Entity ID
     * @type {string}
     * @memberof ExceptionCreateData
     */
    entityId: string;
    /**
     *
     * @type {EntityName}
     * @memberof ExceptionCreateData
     */
    entityType: EntityName;
}
export declare function ExceptionCreateDataFromJSON(json: any): ExceptionCreateData;
export declare function ExceptionCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionCreateData;
export declare function ExceptionCreateDataToJSON(value?: ExceptionCreateData | null): any;
