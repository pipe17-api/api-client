"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionUpdateResponseToJSON = exports.ExceptionUpdateResponseFromJSONTyped = exports.ExceptionUpdateResponseFromJSON = exports.ExceptionUpdateResponseCodeEnum = exports.ExceptionUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ExceptionUpdateResponseSuccessEnum;
(function (ExceptionUpdateResponseSuccessEnum) {
    ExceptionUpdateResponseSuccessEnum["True"] = "true";
})(ExceptionUpdateResponseSuccessEnum = exports.ExceptionUpdateResponseSuccessEnum || (exports.ExceptionUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionUpdateResponseCodeEnum;
(function (ExceptionUpdateResponseCodeEnum) {
    ExceptionUpdateResponseCodeEnum[ExceptionUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionUpdateResponseCodeEnum[ExceptionUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionUpdateResponseCodeEnum[ExceptionUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionUpdateResponseCodeEnum = exports.ExceptionUpdateResponseCodeEnum || (exports.ExceptionUpdateResponseCodeEnum = {}));
function ExceptionUpdateResponseFromJSON(json) {
    return ExceptionUpdateResponseFromJSONTyped(json, false);
}
exports.ExceptionUpdateResponseFromJSON = ExceptionUpdateResponseFromJSON;
function ExceptionUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ExceptionUpdateResponseFromJSONTyped = ExceptionUpdateResponseFromJSONTyped;
function ExceptionUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ExceptionUpdateResponseToJSON = ExceptionUpdateResponseToJSON;
