/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConvertResponseResult } from './';
/**
 *
 * @export
 * @interface ConvertResponse
 */
export interface ConvertResponse {
    /**
     *
     * @type {string}
     * @memberof ConvertResponse
     */
    message?: string;
    /**
     *
     * @type {ConvertResponseResult}
     * @memberof ConvertResponse
     */
    result?: ConvertResponseResult;
}
export declare function ConvertResponseFromJSON(json: any): ConvertResponse;
export declare function ConvertResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConvertResponse;
export declare function ConvertResponseToJSON(value?: ConvertResponse | null): any;
