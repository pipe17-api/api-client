/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingCreateData } from './';
/**
 *
 * @export
 * @interface MappingCreateErrorAllOf
 */
export interface MappingCreateErrorAllOf {
    /**
     *
     * @type {MappingCreateData}
     * @memberof MappingCreateErrorAllOf
     */
    mapping?: MappingCreateData;
}
export declare function MappingCreateErrorAllOfFromJSON(json: any): MappingCreateErrorAllOf;
export declare function MappingCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingCreateErrorAllOf;
export declare function MappingCreateErrorAllOfToJSON(value?: MappingCreateErrorAllOf | null): any;
