"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingAllOfToJSON = exports.MappingAllOfFromJSONTyped = exports.MappingAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function MappingAllOfFromJSON(json) {
    return MappingAllOfFromJSONTyped(json, false);
}
exports.MappingAllOfFromJSON = MappingAllOfFromJSON;
function MappingAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mappingId': !runtime_1.exists(json, 'mappingId') ? undefined : json['mappingId'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
    };
}
exports.MappingAllOfFromJSONTyped = MappingAllOfFromJSONTyped;
function MappingAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mappingId': value.mappingId,
        'connectorId': value.connectorId,
    };
}
exports.MappingAllOfToJSON = MappingAllOfToJSON;
