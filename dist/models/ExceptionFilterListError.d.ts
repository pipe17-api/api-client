/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFiltersListFilter } from './';
/**
 *
 * @export
 * @interface ExceptionFilterListError
 */
export interface ExceptionFilterListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionFilterListError
     */
    success: ExceptionFilterListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionFilterListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionFilterListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ExceptionFiltersListFilter}
     * @memberof ExceptionFilterListError
     */
    filters?: ExceptionFiltersListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterListErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionFilterListErrorFromJSON(json: any): ExceptionFilterListError;
export declare function ExceptionFilterListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterListError;
export declare function ExceptionFilterListErrorToJSON(value?: ExceptionFilterListError | null): any;
