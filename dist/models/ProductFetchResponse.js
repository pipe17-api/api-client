"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductFetchResponseToJSON = exports.ProductFetchResponseFromJSONTyped = exports.ProductFetchResponseFromJSON = exports.ProductFetchResponseCodeEnum = exports.ProductFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductFetchResponseSuccessEnum;
(function (ProductFetchResponseSuccessEnum) {
    ProductFetchResponseSuccessEnum["True"] = "true";
})(ProductFetchResponseSuccessEnum = exports.ProductFetchResponseSuccessEnum || (exports.ProductFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ProductFetchResponseCodeEnum;
(function (ProductFetchResponseCodeEnum) {
    ProductFetchResponseCodeEnum[ProductFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ProductFetchResponseCodeEnum[ProductFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ProductFetchResponseCodeEnum[ProductFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ProductFetchResponseCodeEnum = exports.ProductFetchResponseCodeEnum || (exports.ProductFetchResponseCodeEnum = {}));
function ProductFetchResponseFromJSON(json) {
    return ProductFetchResponseFromJSONTyped(json, false);
}
exports.ProductFetchResponseFromJSON = ProductFetchResponseFromJSON;
function ProductFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'product': !runtime_1.exists(json, 'product') ? undefined : _1.ProductFromJSON(json['product']),
    };
}
exports.ProductFetchResponseFromJSONTyped = ProductFetchResponseFromJSONTyped;
function ProductFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'product': _1.ProductToJSON(value.product),
    };
}
exports.ProductFetchResponseToJSON = ProductFetchResponseToJSON;
