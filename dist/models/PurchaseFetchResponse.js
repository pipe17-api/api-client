"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseFetchResponseToJSON = exports.PurchaseFetchResponseFromJSONTyped = exports.PurchaseFetchResponseFromJSON = exports.PurchaseFetchResponseCodeEnum = exports.PurchaseFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchaseFetchResponseSuccessEnum;
(function (PurchaseFetchResponseSuccessEnum) {
    PurchaseFetchResponseSuccessEnum["True"] = "true";
})(PurchaseFetchResponseSuccessEnum = exports.PurchaseFetchResponseSuccessEnum || (exports.PurchaseFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var PurchaseFetchResponseCodeEnum;
(function (PurchaseFetchResponseCodeEnum) {
    PurchaseFetchResponseCodeEnum[PurchaseFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    PurchaseFetchResponseCodeEnum[PurchaseFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    PurchaseFetchResponseCodeEnum[PurchaseFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(PurchaseFetchResponseCodeEnum = exports.PurchaseFetchResponseCodeEnum || (exports.PurchaseFetchResponseCodeEnum = {}));
function PurchaseFetchResponseFromJSON(json) {
    return PurchaseFetchResponseFromJSONTyped(json, false);
}
exports.PurchaseFetchResponseFromJSON = PurchaseFetchResponseFromJSON;
function PurchaseFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'purchase': !runtime_1.exists(json, 'purchase') ? undefined : _1.PurchaseFromJSON(json['purchase']),
    };
}
exports.PurchaseFetchResponseFromJSONTyped = PurchaseFetchResponseFromJSONTyped;
function PurchaseFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'purchase': _1.PurchaseToJSON(value.purchase),
    };
}
exports.PurchaseFetchResponseToJSON = PurchaseFetchResponseToJSON;
