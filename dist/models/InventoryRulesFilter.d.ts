/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRulesFilter
 */
export interface InventoryRulesFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof InventoryRulesFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof InventoryRulesFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof InventoryRulesFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof InventoryRulesFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof InventoryRulesFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof InventoryRulesFilter
     */
    count?: number;
    /**
     * Rules by list of ruleId
     * @type {Array<string>}
     * @memberof InventoryRulesFilter
     */
    ruleId?: Array<string>;
    /**
     * Rules by list of integration ids
     * @type {Array<string>}
     * @memberof InventoryRulesFilter
     */
    integration?: Array<string>;
    /**
     * Soft deleted rules
     * @type {boolean}
     * @memberof InventoryRulesFilter
     */
    deleted?: boolean;
}
export declare function InventoryRulesFilterFromJSON(json: any): InventoryRulesFilter;
export declare function InventoryRulesFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesFilter;
export declare function InventoryRulesFilterToJSON(value?: InventoryRulesFilter | null): any;
