"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierContactToJSON = exports.SupplierContactFromJSONTyped = exports.SupplierContactFromJSON = void 0;
const runtime_1 = require("../runtime");
function SupplierContactFromJSON(json) {
    return SupplierContactFromJSONTyped(json, false);
}
exports.SupplierContactFromJSON = SupplierContactFromJSON;
function SupplierContactFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'firstName': !runtime_1.exists(json, 'firstName') ? undefined : json['firstName'],
        'lastName': !runtime_1.exists(json, 'lastName') ? undefined : json['lastName'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
    };
}
exports.SupplierContactFromJSONTyped = SupplierContactFromJSONTyped;
function SupplierContactToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'firstName': value.firstName,
        'lastName': value.lastName,
        'email': value.email,
        'phone': value.phone,
    };
}
exports.SupplierContactToJSON = SupplierContactToJSON;
