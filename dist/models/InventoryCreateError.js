"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryCreateErrorToJSON = exports.InventoryCreateErrorFromJSONTyped = exports.InventoryCreateErrorFromJSON = exports.InventoryCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryCreateErrorSuccessEnum;
(function (InventoryCreateErrorSuccessEnum) {
    InventoryCreateErrorSuccessEnum["False"] = "false";
})(InventoryCreateErrorSuccessEnum = exports.InventoryCreateErrorSuccessEnum || (exports.InventoryCreateErrorSuccessEnum = {}));
function InventoryCreateErrorFromJSON(json) {
    return InventoryCreateErrorFromJSONTyped(json, false);
}
exports.InventoryCreateErrorFromJSON = InventoryCreateErrorFromJSON;
function InventoryCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryCreateResultFromJSON)),
    };
}
exports.InventoryCreateErrorFromJSONTyped = InventoryCreateErrorFromJSONTyped;
function InventoryCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryCreateResultToJSON)),
    };
}
exports.InventoryCreateErrorToJSON = InventoryCreateErrorToJSON;
