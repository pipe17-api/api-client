/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Entity Name
 * @export
 * @enum {string}
 */
export declare enum EntityName {
    Accounts = "accounts",
    Arrivals = "arrivals",
    EntityFilters = "entity_filters",
    Fulfillments = "fulfillments",
    Inventory = "inventory",
    Labels = "labels",
    Locations = "locations",
    Orders = "orders",
    OrderRegions = "order_regions",
    OrderRoutings = "order_routings",
    Products = "products",
    Purchases = "purchases",
    Receipts = "receipts",
    Refunds = "refunds",
    Returns = "returns",
    Shipments = "shipments",
    Suppliers = "suppliers",
    Transfers = "transfers",
    Trackings = "trackings"
}
export declare function EntityNameFromJSON(json: any): EntityName;
export declare function EntityNameFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityName;
export declare function EntityNameToJSON(value?: EntityName | null): any;
