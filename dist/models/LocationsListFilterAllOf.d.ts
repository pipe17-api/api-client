/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Locations Filter
 * @export
 * @interface LocationsListFilterAllOf
 */
export interface LocationsListFilterAllOf {
    /**
     * Retrieve locations whose names contains this string
     * @type {string}
     * @memberof LocationsListFilterAllOf
     */
    name?: string;
    /**
     * Retrieve locations whose email addreses contains this string
     * @type {string}
     * @memberof LocationsListFilterAllOf
     */
    email?: string;
    /**
     * Retrieve locations whose phone numbers contains this string
     * @type {string}
     * @memberof LocationsListFilterAllOf
     */
    phone?: string;
    /**
     * Retrieve locations by list of external location ids
     * @type {Array<string>}
     * @memberof LocationsListFilterAllOf
     */
    extLocationId?: Array<string>;
    /**
     * Retrieve locations by list of external integration ids
     * @type {Array<string>}
     * @memberof LocationsListFilterAllOf
     */
    extIntegrationId?: Array<string>;
}
export declare function LocationsListFilterAllOfFromJSON(json: any): LocationsListFilterAllOf;
export declare function LocationsListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsListFilterAllOf;
export declare function LocationsListFilterAllOfToJSON(value?: LocationsListFilterAllOf | null): any;
