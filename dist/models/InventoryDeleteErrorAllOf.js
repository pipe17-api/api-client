"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryDeleteErrorAllOfToJSON = exports.InventoryDeleteErrorAllOfFromJSONTyped = exports.InventoryDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryDeleteErrorAllOfFromJSON(json) {
    return InventoryDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.InventoryDeleteErrorAllOfFromJSON = InventoryDeleteErrorAllOfFromJSON;
function InventoryDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryDeleteFilterFromJSON(json['filters']),
    };
}
exports.InventoryDeleteErrorAllOfFromJSONTyped = InventoryDeleteErrorAllOfFromJSONTyped;
function InventoryDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.InventoryDeleteFilterToJSON(value.filters),
    };
}
exports.InventoryDeleteErrorAllOfToJSON = InventoryDeleteErrorAllOfToJSON;
