"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreateErrorAllOfToJSON = exports.UserCreateErrorAllOfFromJSONTyped = exports.UserCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UserCreateErrorAllOfFromJSON(json) {
    return UserCreateErrorAllOfFromJSONTyped(json, false);
}
exports.UserCreateErrorAllOfFromJSON = UserCreateErrorAllOfFromJSON;
function UserCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserCreateDataFromJSON(json['user']),
    };
}
exports.UserCreateErrorAllOfFromJSONTyped = UserCreateErrorAllOfFromJSONTyped;
function UserCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'user': _1.UserCreateDataToJSON(value.user),
    };
}
exports.UserCreateErrorAllOfToJSON = UserCreateErrorAllOfToJSON;
