/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventReplayResponse
 */
export interface EventReplayResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EventReplayResponse
     */
    success: EventReplayResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventReplayResponse
     */
    code: EventReplayResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum EventReplayResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EventReplayResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EventReplayResponseFromJSON(json: any): EventReplayResponse;
export declare function EventReplayResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventReplayResponse;
export declare function EventReplayResponseToJSON(value?: EventReplayResponse | null): any;
