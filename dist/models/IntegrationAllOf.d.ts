/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationAllOf
 */
export interface IntegrationAllOf {
    /**
     * Integration api Key
     * @type {string}
     * @memberof IntegrationAllOf
     */
    apikey?: string;
}
export declare function IntegrationAllOfFromJSON(json: any): IntegrationAllOf;
export declare function IntegrationAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationAllOf;
export declare function IntegrationAllOfToJSON(value?: IntegrationAllOf | null): any;
