"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingFetchErrorToJSON = exports.TrackingFetchErrorFromJSONTyped = exports.TrackingFetchErrorFromJSON = exports.TrackingFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var TrackingFetchErrorSuccessEnum;
(function (TrackingFetchErrorSuccessEnum) {
    TrackingFetchErrorSuccessEnum["False"] = "false";
})(TrackingFetchErrorSuccessEnum = exports.TrackingFetchErrorSuccessEnum || (exports.TrackingFetchErrorSuccessEnum = {}));
function TrackingFetchErrorFromJSON(json) {
    return TrackingFetchErrorFromJSONTyped(json, false);
}
exports.TrackingFetchErrorFromJSON = TrackingFetchErrorFromJSON;
function TrackingFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.TrackingFetchErrorFromJSONTyped = TrackingFetchErrorFromJSONTyped;
function TrackingFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.TrackingFetchErrorToJSON = TrackingFetchErrorToJSON;
