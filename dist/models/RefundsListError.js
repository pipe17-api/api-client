"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsListErrorToJSON = exports.RefundsListErrorFromJSONTyped = exports.RefundsListErrorFromJSON = exports.RefundsListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RefundsListErrorSuccessEnum;
(function (RefundsListErrorSuccessEnum) {
    RefundsListErrorSuccessEnum["False"] = "false";
})(RefundsListErrorSuccessEnum = exports.RefundsListErrorSuccessEnum || (exports.RefundsListErrorSuccessEnum = {}));
function RefundsListErrorFromJSON(json) {
    return RefundsListErrorFromJSONTyped(json, false);
}
exports.RefundsListErrorFromJSON = RefundsListErrorFromJSON;
function RefundsListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RefundsFilterFromJSON(json['filters']),
    };
}
exports.RefundsListErrorFromJSONTyped = RefundsListErrorFromJSONTyped;
function RefundsListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.RefundsFilterToJSON(value.filters),
    };
}
exports.RefundsListErrorToJSON = RefundsListErrorToJSON;
