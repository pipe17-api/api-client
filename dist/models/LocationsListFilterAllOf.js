"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsListFilterAllOfToJSON = exports.LocationsListFilterAllOfFromJSONTyped = exports.LocationsListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function LocationsListFilterAllOfFromJSON(json) {
    return LocationsListFilterAllOfFromJSONTyped(json, false);
}
exports.LocationsListFilterAllOfFromJSON = LocationsListFilterAllOfFromJSON;
function LocationsListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'extLocationId': !runtime_1.exists(json, 'extLocationId') ? undefined : json['extLocationId'],
        'extIntegrationId': !runtime_1.exists(json, 'extIntegrationId') ? undefined : json['extIntegrationId'],
    };
}
exports.LocationsListFilterAllOfFromJSONTyped = LocationsListFilterAllOfFromJSONTyped;
function LocationsListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'email': value.email,
        'phone': value.phone,
        'extLocationId': value.extLocationId,
        'extIntegrationId': value.extIntegrationId,
    };
}
exports.LocationsListFilterAllOfToJSON = LocationsListFilterAllOfToJSON;
