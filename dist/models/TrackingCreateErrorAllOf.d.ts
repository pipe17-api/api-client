/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingCreateData } from './';
/**
 *
 * @export
 * @interface TrackingCreateErrorAllOf
 */
export interface TrackingCreateErrorAllOf {
    /**
     *
     * @type {TrackingCreateData}
     * @memberof TrackingCreateErrorAllOf
     */
    tracking?: TrackingCreateData;
}
export declare function TrackingCreateErrorAllOfFromJSON(json: any): TrackingCreateErrorAllOf;
export declare function TrackingCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingCreateErrorAllOf;
export declare function TrackingCreateErrorAllOfToJSON(value?: TrackingCreateErrorAllOf | null): any;
