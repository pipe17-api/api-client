/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SuppliersListFilter } from './';
/**
 *
 * @export
 * @interface SuppliersListError
 */
export interface SuppliersListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof SuppliersListError
     */
    success: SuppliersListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SuppliersListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof SuppliersListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof SuppliersListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {SuppliersListFilter}
     * @memberof SuppliersListError
     */
    filters?: SuppliersListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum SuppliersListErrorSuccessEnum {
    False = "false"
}
export declare function SuppliersListErrorFromJSON(json: any): SuppliersListError;
export declare function SuppliersListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersListError;
export declare function SuppliersListErrorToJSON(value?: SuppliersListError | null): any;
