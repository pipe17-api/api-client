/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule } from './';
/**
 *
 * @export
 * @interface InventoryRuleFetchResponse
 */
export interface InventoryRuleFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryRuleFetchResponse
     */
    success: InventoryRuleFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRuleFetchResponse
     */
    code: InventoryRuleFetchResponseCodeEnum;
    /**
     *
     * @type {InventoryRule}
     * @memberof InventoryRuleFetchResponse
     */
    inventoryrule?: InventoryRule;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRuleFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryRuleFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryRuleFetchResponseFromJSON(json: any): InventoryRuleFetchResponse;
export declare function InventoryRuleFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleFetchResponse;
export declare function InventoryRuleFetchResponseToJSON(value?: InventoryRuleFetchResponse | null): any;
