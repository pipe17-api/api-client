/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Product Type
 * @export
 * @enum {string}
 */
export declare enum ProductType {
    Simple = "simple",
    Bundle = "bundle",
    BundleItem = "bundleItem",
    Child = "child",
    Parent = "parent"
}
export declare function ProductTypeFromJSON(json: any): ProductType;
export declare function ProductTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductType;
export declare function ProductTypeToJSON(value?: ProductType | null): any;
