"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationAllOfToJSON = exports.LocationAllOfFromJSONTyped = exports.LocationAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function LocationAllOfFromJSON(json) {
    return LocationAllOfFromJSONTyped(json, false);
}
exports.LocationAllOfFromJSON = LocationAllOfFromJSON;
function LocationAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.LocationAllOfFromJSONTyped = LocationAllOfFromJSONTyped;
function LocationAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'locationId': value.locationId,
        'integration': value.integration,
    };
}
exports.LocationAllOfToJSON = LocationAllOfToJSON;
