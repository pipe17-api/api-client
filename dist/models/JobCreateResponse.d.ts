/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job } from './';
/**
 *
 * @export
 * @interface JobCreateResponse
 */
export interface JobCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof JobCreateResponse
     */
    success: JobCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobCreateResponse
     */
    code: JobCreateResponseCodeEnum;
    /**
     *
     * @type {Job}
     * @memberof JobCreateResponse
     */
    job?: Job;
}
/**
* @export
* @enum {string}
*/
export declare enum JobCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum JobCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function JobCreateResponseFromJSON(json: any): JobCreateResponse;
export declare function JobCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobCreateResponse;
export declare function JobCreateResponseToJSON(value?: JobCreateResponse | null): any;
