/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFilter, EntityFiltersListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface EntityFilterListResponseAllOf
 */
export interface EntityFilterListResponseAllOf {
    /**
     *
     * @type {EntityFiltersListFilter}
     * @memberof EntityFilterListResponseAllOf
     */
    filters?: EntityFiltersListFilter;
    /**
     *
     * @type {Array<EntityFilter>}
     * @memberof EntityFilterListResponseAllOf
     */
    entityFilters?: Array<EntityFilter>;
    /**
     *
     * @type {Pagination}
     * @memberof EntityFilterListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function EntityFilterListResponseAllOfFromJSON(json: any): EntityFilterListResponseAllOf;
export declare function EntityFilterListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterListResponseAllOf;
export declare function EntityFilterListResponseAllOfToJSON(value?: EntityFilterListResponseAllOf | null): any;
