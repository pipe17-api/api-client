"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundCreateErrorAllOfToJSON = exports.RefundCreateErrorAllOfFromJSONTyped = exports.RefundCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundCreateErrorAllOfFromJSON(json) {
    return RefundCreateErrorAllOfFromJSONTyped(json, false);
}
exports.RefundCreateErrorAllOfFromJSON = RefundCreateErrorAllOfFromJSON;
function RefundCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'refund': !runtime_1.exists(json, 'refund') ? undefined : _1.RefundCreateDataFromJSON(json['refund']),
    };
}
exports.RefundCreateErrorAllOfFromJSONTyped = RefundCreateErrorAllOfFromJSONTyped;
function RefundCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'refund': _1.RefundCreateDataToJSON(value.refund),
    };
}
exports.RefundCreateErrorAllOfToJSON = RefundCreateErrorAllOfToJSON;
