/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityRestrictionsValueAllOf
 */
export interface EntityRestrictionsValueAllOf {
    /**
     *
     * @type {string}
     * @memberof EntityRestrictionsValueAllOf
     */
    type?: EntityRestrictionsValueAllOfTypeEnum;
    /**
     *
     * @type {object}
     * @memberof EntityRestrictionsValueAllOf
     */
    value?: object;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityRestrictionsValueAllOfTypeEnum {
    Boolean = "boolean",
    Date = "date",
    Array = "array"
}
export declare function EntityRestrictionsValueAllOfFromJSON(json: any): EntityRestrictionsValueAllOf;
export declare function EntityRestrictionsValueAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityRestrictionsValueAllOf;
export declare function EntityRestrictionsValueAllOfToJSON(value?: EntityRestrictionsValueAllOf | null): any;
