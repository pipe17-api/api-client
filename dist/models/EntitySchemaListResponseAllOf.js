"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaListResponseAllOfToJSON = exports.EntitySchemaListResponseAllOfFromJSONTyped = exports.EntitySchemaListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntitySchemaListResponseAllOfFromJSON(json) {
    return EntitySchemaListResponseAllOfFromJSONTyped(json, false);
}
exports.EntitySchemaListResponseAllOfFromJSON = EntitySchemaListResponseAllOfFromJSON;
function EntitySchemaListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntitySchemasListFilterFromJSON(json['filters']),
        'entitySchemas': !runtime_1.exists(json, 'entitySchemas') ? undefined : (json['entitySchemas'].map(_1.EntitySchemaFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.EntitySchemaListResponseAllOfFromJSONTyped = EntitySchemaListResponseAllOfFromJSONTyped;
function EntitySchemaListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.EntitySchemasListFilterToJSON(value.filters),
        'entitySchemas': value.entitySchemas === undefined ? undefined : (value.entitySchemas.map(_1.EntitySchemaToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.EntitySchemaListResponseAllOfToJSON = EntitySchemaListResponseAllOfToJSON;
