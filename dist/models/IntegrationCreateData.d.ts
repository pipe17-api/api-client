/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, IntegrationConnection, IntegrationEntity, IntegrationSettings } from './';
/**
 *
 * @export
 * @interface IntegrationCreateData
 */
export interface IntegrationCreateData {
    /**
     * Integration Name
     * @type {string}
     * @memberof IntegrationCreateData
     */
    integrationName: string;
    /**
     * Connector Name (either Name or ID required)
     * @type {string}
     * @memberof IntegrationCreateData
     */
    connectorName?: string;
    /**
     * Connector ID (either Name or ID required)
     * @type {string}
     * @memberof IntegrationCreateData
     */
    connectorId?: string;
    /**
     *
     * @type {Env}
     * @memberof IntegrationCreateData
     */
    environment?: Env;
    /**
     *
     * @type {Array<IntegrationEntity>}
     * @memberof IntegrationCreateData
     */
    entities?: Array<IntegrationEntity>;
    /**
     *
     * @type {IntegrationSettings}
     * @memberof IntegrationCreateData
     */
    settings?: IntegrationSettings;
    /**
     *
     * @type {IntegrationConnection}
     * @memberof IntegrationCreateData
     */
    connection?: IntegrationConnection;
}
export declare function IntegrationCreateDataFromJSON(json: any): IntegrationCreateData;
export declare function IntegrationCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationCreateData;
export declare function IntegrationCreateDataToJSON(value?: IntegrationCreateData | null): any;
