/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStatus } from './';
/**
 *
 * @export
 * @interface IntegrationsFilter
 */
export interface IntegrationsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof IntegrationsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof IntegrationsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof IntegrationsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof IntegrationsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof IntegrationsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof IntegrationsFilter
     */
    count?: number;
    /**
     * Retrieve by list of integrationId
     * @type {Array<string>}
     * @memberof IntegrationsFilter
     */
    integrationId?: Array<string>;
    /**
     * Retrieve integrations by list of connector names
     * @type {Array<string>}
     * @memberof IntegrationsFilter
     */
    connectorName?: Array<string>;
    /**
     * Retrieve integrations by connector ID
     * @type {string}
     * @memberof IntegrationsFilter
     */
    connectorId?: string;
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationsFilter
     */
    status?: IntegrationStatus;
}
export declare function IntegrationsFilterFromJSON(json: any): IntegrationsFilter;
export declare function IntegrationsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsFilter;
export declare function IntegrationsFilterToJSON(value?: IntegrationsFilter | null): any;
