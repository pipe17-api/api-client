/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * External System
 * @export
 * @interface LocationExternalSystem
 */
export interface LocationExternalSystem {
    /**
     * External System Integration Id
     * @type {string}
     * @memberof LocationExternalSystem
     */
    integrationId?: string;
    /**
     * External System Location Value
     * @type {string}
     * @memberof LocationExternalSystem
     */
    locationValue?: string;
}
export declare function LocationExternalSystemFromJSON(json: any): LocationExternalSystem;
export declare function LocationExternalSystemFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationExternalSystem;
export declare function LocationExternalSystemToJSON(value?: LocationExternalSystem | null): any;
