"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersListResponseAllOfToJSON = exports.OrdersListResponseAllOfFromJSONTyped = exports.OrdersListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersListResponseAllOfFromJSON(json) {
    return OrdersListResponseAllOfFromJSONTyped(json, false);
}
exports.OrdersListResponseAllOfFromJSON = OrdersListResponseAllOfFromJSON;
function OrdersListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersListFilterFromJSON(json['filters']),
        'orders': (json['orders'].map(_1.OrderFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrdersListResponseAllOfFromJSONTyped = OrdersListResponseAllOfFromJSONTyped;
function OrdersListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.OrdersListFilterToJSON(value.filters),
        'orders': (value.orders.map(_1.OrderToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrdersListResponseAllOfToJSON = OrdersListResponseAllOfToJSON;
