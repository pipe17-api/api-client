"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleFetchResponseToJSON = exports.RoleFetchResponseFromJSONTyped = exports.RoleFetchResponseFromJSON = exports.RoleFetchResponseCodeEnum = exports.RoleFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoleFetchResponseSuccessEnum;
(function (RoleFetchResponseSuccessEnum) {
    RoleFetchResponseSuccessEnum["True"] = "true";
})(RoleFetchResponseSuccessEnum = exports.RoleFetchResponseSuccessEnum || (exports.RoleFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoleFetchResponseCodeEnum;
(function (RoleFetchResponseCodeEnum) {
    RoleFetchResponseCodeEnum[RoleFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoleFetchResponseCodeEnum[RoleFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoleFetchResponseCodeEnum[RoleFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoleFetchResponseCodeEnum = exports.RoleFetchResponseCodeEnum || (exports.RoleFetchResponseCodeEnum = {}));
function RoleFetchResponseFromJSON(json) {
    return RoleFetchResponseFromJSONTyped(json, false);
}
exports.RoleFetchResponseFromJSON = RoleFetchResponseFromJSON;
function RoleFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleFromJSON(json['role']),
    };
}
exports.RoleFetchResponseFromJSONTyped = RoleFetchResponseFromJSONTyped;
function RoleFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'role': _1.RoleToJSON(value.role),
    };
}
exports.RoleFetchResponseToJSON = RoleFetchResponseToJSON;
