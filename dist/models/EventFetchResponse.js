"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventFetchResponseToJSON = exports.EventFetchResponseFromJSONTyped = exports.EventFetchResponseFromJSON = exports.EventFetchResponseCodeEnum = exports.EventFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EventFetchResponseSuccessEnum;
(function (EventFetchResponseSuccessEnum) {
    EventFetchResponseSuccessEnum["True"] = "true";
})(EventFetchResponseSuccessEnum = exports.EventFetchResponseSuccessEnum || (exports.EventFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EventFetchResponseCodeEnum;
(function (EventFetchResponseCodeEnum) {
    EventFetchResponseCodeEnum[EventFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EventFetchResponseCodeEnum[EventFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EventFetchResponseCodeEnum[EventFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EventFetchResponseCodeEnum = exports.EventFetchResponseCodeEnum || (exports.EventFetchResponseCodeEnum = {}));
function EventFetchResponseFromJSON(json) {
    return EventFetchResponseFromJSONTyped(json, false);
}
exports.EventFetchResponseFromJSON = EventFetchResponseFromJSON;
function EventFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'event': !runtime_1.exists(json, 'event') ? undefined : _1.EventFromJSON(json['event']),
    };
}
exports.EventFetchResponseFromJSONTyped = EventFetchResponseFromJSONTyped;
function EventFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'event': _1.EventToJSON(value.event),
    };
}
exports.EventFetchResponseToJSON = EventFetchResponseToJSON;
