/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationState } from './';
/**
 *
 * @export
 * @interface IntegrationStateCreateResponse
 */
export interface IntegrationStateCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationStateCreateResponse
     */
    success: IntegrationStateCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationStateCreateResponse
     */
    code: IntegrationStateCreateResponseCodeEnum;
    /**
     *
     * @type {IntegrationState}
     * @memberof IntegrationStateCreateResponse
     */
    integrationState?: IntegrationState;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationStateCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationStateCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationStateCreateResponseFromJSON(json: any): IntegrationStateCreateResponse;
export declare function IntegrationStateCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateCreateResponse;
export declare function IntegrationStateCreateResponseToJSON(value?: IntegrationStateCreateResponse | null): any;
