/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface UserFetchError
 */
export interface UserFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof UserFetchError
     */
    success: UserFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UserFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof UserFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof UserFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum UserFetchErrorSuccessEnum {
    False = "false"
}
export declare function UserFetchErrorFromJSON(json: any): UserFetchError;
export declare function UserFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserFetchError;
export declare function UserFetchErrorToJSON(value?: UserFetchError | null): any;
