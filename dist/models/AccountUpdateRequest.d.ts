/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 *
 * @export
 * @interface AccountUpdateRequest
 */
export interface AccountUpdateRequest {
    /**
     *
     * @type {AccountStatus}
     * @memberof AccountUpdateRequest
     */
    status?: AccountStatus;
    /**
     *
     * @type {Date}
     * @memberof AccountUpdateRequest
     */
    expirationDate?: Date;
    /**
     *
     * @type {object}
     * @memberof AccountUpdateRequest
     */
    payment?: object;
}
export declare function AccountUpdateRequestFromJSON(json: any): AccountUpdateRequest;
export declare function AccountUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountUpdateRequest;
export declare function AccountUpdateRequestToJSON(value?: AccountUpdateRequest | null): any;
