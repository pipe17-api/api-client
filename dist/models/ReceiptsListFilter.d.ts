/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReceiptsListFilter
 */
export interface ReceiptsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ReceiptsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ReceiptsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ReceiptsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ReceiptsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ReceiptsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ReceiptsListFilter
     */
    count?: number;
    /**
     * Receipts by list of receiptId
     * @type {Array<string>}
     * @memberof ReceiptsListFilter
     */
    receiptId?: Array<string>;
    /**
     * Receipts by list of arrivalId
     * @type {Array<string>}
     * @memberof ReceiptsListFilter
     */
    arrivalId?: Array<string>;
    /**
     * Receipts by list of extOrderId
     * @type {Array<string>}
     * @memberof ReceiptsListFilter
     */
    extOrderId?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof ReceiptsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ReceiptsListFilter
     */
    keys?: string;
}
export declare function ReceiptsListFilterFromJSON(json: any): ReceiptsListFilter;
export declare function ReceiptsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsListFilter;
export declare function ReceiptsListFilterToJSON(value?: ReceiptsListFilter | null): any;
