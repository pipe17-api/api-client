"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventCreateResponseAllOfToJSON = exports.EventCreateResponseAllOfFromJSONTyped = exports.EventCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EventCreateResponseAllOfFromJSON(json) {
    return EventCreateResponseAllOfFromJSONTyped(json, false);
}
exports.EventCreateResponseAllOfFromJSON = EventCreateResponseAllOfFromJSON;
function EventCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'event': !runtime_1.exists(json, 'event') ? undefined : _1.EventFromJSON(json['event']),
    };
}
exports.EventCreateResponseAllOfFromJSONTyped = EventCreateResponseAllOfFromJSONTyped;
function EventCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'event': _1.EventToJSON(value.event),
    };
}
exports.EventCreateResponseAllOfToJSON = EventCreateResponseAllOfToJSON;
