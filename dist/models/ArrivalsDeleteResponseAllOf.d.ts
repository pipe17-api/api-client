/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival, ArrivalsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ArrivalsDeleteResponseAllOf
 */
export interface ArrivalsDeleteResponseAllOf {
    /**
     *
     * @type {ArrivalsDeleteFilter}
     * @memberof ArrivalsDeleteResponseAllOf
     */
    filters?: ArrivalsDeleteFilter;
    /**
     *
     * @type {Array<Arrival>}
     * @memberof ArrivalsDeleteResponseAllOf
     */
    arrivals?: Array<Arrival>;
    /**
     * Number of deleted arrivals
     * @type {number}
     * @memberof ArrivalsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ArrivalsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ArrivalsDeleteResponseAllOfFromJSON(json: any): ArrivalsDeleteResponseAllOf;
export declare function ArrivalsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsDeleteResponseAllOf;
export declare function ArrivalsDeleteResponseAllOfToJSON(value?: ArrivalsDeleteResponseAllOf | null): any;
