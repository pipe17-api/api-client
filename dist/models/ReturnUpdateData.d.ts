/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ReturnLineItem, ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnUpdateData
 */
export interface ReturnUpdateData {
    /**
     *
     * @type {ReturnStatus}
     * @memberof ReturnUpdateData
     */
    status?: ReturnStatus;
    /**
     *
     * @type {Array<ReturnLineItem>}
     * @memberof ReturnUpdateData
     */
    lineItems?: Array<ReturnLineItem>;
    /**
     *
     * @type {Address}
     * @memberof ReturnUpdateData
     */
    shippingAddress?: Address;
    /**
     * Currency value, e.g. 'USD'
     * @type {string}
     * @memberof ReturnUpdateData
     */
    currency?: string;
    /**
     * Customer notes
     * @type {string}
     * @memberof ReturnUpdateData
     */
    customerNotes?: string;
    /**
     * Merchant notes
     * @type {string}
     * @memberof ReturnUpdateData
     */
    notes?: string;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    tax?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    discount?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    subTotal?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    total?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    shippingPrice?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    shippingRefund?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    shippingQuote?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    shippingLabelFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    restockingFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateData
     */
    estimatedTotal?: number;
    /**
     *
     * @type {boolean}
     * @memberof ReturnUpdateData
     */
    isExchange?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnUpdateData
     */
    isGift?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnUpdateData
     */
    requiresShipping?: boolean;
    /**
     *
     * @type {Date}
     * @memberof ReturnUpdateData
     */
    refundedAt?: Date;
}
export declare function ReturnUpdateDataFromJSON(json: any): ReturnUpdateData;
export declare function ReturnUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnUpdateData;
export declare function ReturnUpdateDataToJSON(value?: ReturnUpdateData | null): any;
