"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhooksListErrorAllOfToJSON = exports.WebhooksListErrorAllOfFromJSONTyped = exports.WebhooksListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function WebhooksListErrorAllOfFromJSON(json) {
    return WebhooksListErrorAllOfFromJSONTyped(json, false);
}
exports.WebhooksListErrorAllOfFromJSON = WebhooksListErrorAllOfFromJSON;
function WebhooksListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.WebhooksListFilterFromJSON(json['filters']),
    };
}
exports.WebhooksListErrorAllOfFromJSONTyped = WebhooksListErrorAllOfFromJSONTyped;
function WebhooksListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.WebhooksListFilterToJSON(value.filters),
    };
}
exports.WebhooksListErrorAllOfToJSON = WebhooksListErrorAllOfToJSON;
