/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchema } from './';
/**
 *
 * @export
 * @interface EntitySchemaCreateResponse
 */
export interface EntitySchemaCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntitySchemaCreateResponse
     */
    success: EntitySchemaCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaCreateResponse
     */
    code: EntitySchemaCreateResponseCodeEnum;
    /**
     *
     * @type {EntitySchema}
     * @memberof EntitySchemaCreateResponse
     */
    entitySchema?: EntitySchema;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntitySchemaCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntitySchemaCreateResponseFromJSON(json: any): EntitySchemaCreateResponse;
export declare function EntitySchemaCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaCreateResponse;
export declare function EntitySchemaCreateResponseToJSON(value?: EntitySchemaCreateResponse | null): any;
