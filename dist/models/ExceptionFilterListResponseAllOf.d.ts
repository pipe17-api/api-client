/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilter, ExceptionFiltersListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ExceptionFilterListResponseAllOf
 */
export interface ExceptionFilterListResponseAllOf {
    /**
     *
     * @type {ExceptionFiltersListFilter}
     * @memberof ExceptionFilterListResponseAllOf
     */
    filters?: ExceptionFiltersListFilter;
    /**
     *
     * @type {Array<ExceptionFilter>}
     * @memberof ExceptionFilterListResponseAllOf
     */
    exceptionFilters?: Array<ExceptionFilter>;
    /**
     *
     * @type {Pagination}
     * @memberof ExceptionFilterListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ExceptionFilterListResponseAllOfFromJSON(json: any): ExceptionFilterListResponseAllOf;
export declare function ExceptionFilterListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterListResponseAllOf;
export declare function ExceptionFilterListResponseAllOfToJSON(value?: ExceptionFilterListResponseAllOf | null): any;
