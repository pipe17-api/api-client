/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventRequestData
 */
export interface EventRequestData {
    /**
     * Request url
     * @type {string}
     * @memberof EventRequestData
     */
    url?: string;
    /**
     * Request method
     * @type {string}
     * @memberof EventRequestData
     */
    method?: string;
    /**
     * Request headers
     * @type {object}
     * @memberof EventRequestData
     */
    headers?: object;
    /**
     * Request authorization info
     * @type {object}
     * @memberof EventRequestData
     */
    authobj?: object;
    /**
     * Request body
     * @type {string}
     * @memberof EventRequestData
     */
    body?: string;
}
export declare function EventRequestDataFromJSON(json: any): EventRequestData;
export declare function EventRequestDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventRequestData;
export declare function EventRequestDataToJSON(value?: EventRequestData | null): any;
