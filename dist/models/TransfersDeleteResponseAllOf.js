"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersDeleteResponseAllOfToJSON = exports.TransfersDeleteResponseAllOfFromJSONTyped = exports.TransfersDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransfersDeleteResponseAllOfFromJSON(json) {
    return TransfersDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.TransfersDeleteResponseAllOfFromJSON = TransfersDeleteResponseAllOfFromJSON;
function TransfersDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TransfersDeleteFilterFromJSON(json['filters']),
        'transfers': !runtime_1.exists(json, 'transfers') ? undefined : (json['transfers'].map(_1.TransferFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.TransfersDeleteResponseAllOfFromJSONTyped = TransfersDeleteResponseAllOfFromJSONTyped;
function TransfersDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.TransfersDeleteFilterToJSON(value.filters),
        'transfers': value.transfers === undefined ? undefined : (value.transfers.map(_1.TransferToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.TransfersDeleteResponseAllOfToJSON = TransfersDeleteResponseAllOfToJSON;
