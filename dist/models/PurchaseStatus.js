"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseStatusToJSON = exports.PurchaseStatusFromJSONTyped = exports.PurchaseStatusFromJSON = exports.PurchaseStatus = void 0;
/**
 * Purchase Status
 * @export
 * @enum {string}
 */
var PurchaseStatus;
(function (PurchaseStatus) {
    PurchaseStatus["Draft"] = "draft";
    PurchaseStatus["New"] = "new";
    PurchaseStatus["OnHold"] = "onHold";
    PurchaseStatus["ToBeValidated"] = "toBeValidated";
    PurchaseStatus["ReviewRequired"] = "reviewRequired";
    PurchaseStatus["ReadyForFulfillment"] = "readyForFulfillment";
    PurchaseStatus["SentToFulfillment"] = "sentToFulfillment";
    PurchaseStatus["PartialFulfillment"] = "partialFulfillment";
    PurchaseStatus["Fulfilled"] = "fulfilled";
    PurchaseStatus["InTransit"] = "inTransit";
    PurchaseStatus["PartialReceived"] = "partialReceived";
    PurchaseStatus["Received"] = "received";
    PurchaseStatus["Canceled"] = "canceled";
    PurchaseStatus["Returned"] = "returned";
    PurchaseStatus["Refunded"] = "refunded";
})(PurchaseStatus = exports.PurchaseStatus || (exports.PurchaseStatus = {}));
function PurchaseStatusFromJSON(json) {
    return PurchaseStatusFromJSONTyped(json, false);
}
exports.PurchaseStatusFromJSON = PurchaseStatusFromJSON;
function PurchaseStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.PurchaseStatusFromJSONTyped = PurchaseStatusFromJSONTyped;
function PurchaseStatusToJSON(value) {
    return value;
}
exports.PurchaseStatusToJSON = PurchaseStatusToJSON;
