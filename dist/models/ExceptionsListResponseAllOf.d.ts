/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Exception, ExceptionsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ExceptionsListResponseAllOf
 */
export interface ExceptionsListResponseAllOf {
    /**
     *
     * @type {ExceptionsListFilter}
     * @memberof ExceptionsListResponseAllOf
     */
    filters?: ExceptionsListFilter;
    /**
     *
     * @type {Array<Exception>}
     * @memberof ExceptionsListResponseAllOf
     */
    exceptions?: Array<Exception>;
    /**
     *
     * @type {Pagination}
     * @memberof ExceptionsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ExceptionsListResponseAllOfFromJSON(json: any): ExceptionsListResponseAllOf;
export declare function ExceptionsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsListResponseAllOf;
export declare function ExceptionsListResponseAllOfToJSON(value?: ExceptionsListResponseAllOf | null): any;
