/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Tier (payment plan)
 * @export
 * @enum {string}
 */
export declare enum Tier {
    Free = "free",
    Basic = "basic",
    Enterprise = "enterprise"
}
export declare function TierFromJSON(json: any): Tier;
export declare function TierFromJSONTyped(json: any, ignoreDiscriminator: boolean): Tier;
export declare function TierToJSON(value?: Tier | null): any;
