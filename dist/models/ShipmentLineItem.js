"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentLineItemToJSON = exports.ShipmentLineItemFromJSONTyped = exports.ShipmentLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function ShipmentLineItemFromJSON(json) {
    return ShipmentLineItemFromJSONTyped(json, false);
}
exports.ShipmentLineItemFromJSON = ShipmentLineItemFromJSON;
function ShipmentLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': json['uniqueId'],
        'sku': json['sku'],
        'quantity': json['quantity'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'bundleSKU': !runtime_1.exists(json, 'bundleSKU') ? undefined : json['bundleSKU'],
        'bundleQuantity': !runtime_1.exists(json, 'bundleQuantity') ? undefined : json['bundleQuantity'],
        'shippedQuantity': !runtime_1.exists(json, 'shippedQuantity') ? undefined : json['shippedQuantity'],
    };
}
exports.ShipmentLineItemFromJSONTyped = ShipmentLineItemFromJSONTyped;
function ShipmentLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'sku': value.sku,
        'quantity': value.quantity,
        'name': value.name,
        'bundleSKU': value.bundleSKU,
        'bundleQuantity': value.bundleQuantity,
        'shippedQuantity': value.shippedQuantity,
    };
}
exports.ShipmentLineItemToJSON = ShipmentLineItemToJSON;
