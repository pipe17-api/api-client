"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingUpdateErrorToJSON = exports.OrderRoutingUpdateErrorFromJSONTyped = exports.OrderRoutingUpdateErrorFromJSON = exports.OrderRoutingUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var OrderRoutingUpdateErrorSuccessEnum;
(function (OrderRoutingUpdateErrorSuccessEnum) {
    OrderRoutingUpdateErrorSuccessEnum["False"] = "false";
})(OrderRoutingUpdateErrorSuccessEnum = exports.OrderRoutingUpdateErrorSuccessEnum || (exports.OrderRoutingUpdateErrorSuccessEnum = {}));
function OrderRoutingUpdateErrorFromJSON(json) {
    return OrderRoutingUpdateErrorFromJSONTyped(json, false);
}
exports.OrderRoutingUpdateErrorFromJSON = OrderRoutingUpdateErrorFromJSON;
function OrderRoutingUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.OrderRoutingUpdateErrorFromJSONTyped = OrderRoutingUpdateErrorFromJSONTyped;
function OrderRoutingUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.OrderRoutingUpdateErrorToJSON = OrderRoutingUpdateErrorToJSON;
