"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesDeleteResponseToJSON = exports.PurchasesDeleteResponseFromJSONTyped = exports.PurchasesDeleteResponseFromJSON = exports.PurchasesDeleteResponseCodeEnum = exports.PurchasesDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchasesDeleteResponseSuccessEnum;
(function (PurchasesDeleteResponseSuccessEnum) {
    PurchasesDeleteResponseSuccessEnum["True"] = "true";
})(PurchasesDeleteResponseSuccessEnum = exports.PurchasesDeleteResponseSuccessEnum || (exports.PurchasesDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var PurchasesDeleteResponseCodeEnum;
(function (PurchasesDeleteResponseCodeEnum) {
    PurchasesDeleteResponseCodeEnum[PurchasesDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    PurchasesDeleteResponseCodeEnum[PurchasesDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    PurchasesDeleteResponseCodeEnum[PurchasesDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(PurchasesDeleteResponseCodeEnum = exports.PurchasesDeleteResponseCodeEnum || (exports.PurchasesDeleteResponseCodeEnum = {}));
function PurchasesDeleteResponseFromJSON(json) {
    return PurchasesDeleteResponseFromJSONTyped(json, false);
}
exports.PurchasesDeleteResponseFromJSON = PurchasesDeleteResponseFromJSON;
function PurchasesDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesDeleteFilterFromJSON(json['filters']),
        'purchases': !runtime_1.exists(json, 'purchases') ? undefined : (json['purchases'].map(_1.PurchaseFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.PurchasesDeleteResponseFromJSONTyped = PurchasesDeleteResponseFromJSONTyped;
function PurchasesDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.PurchasesDeleteFilterToJSON(value.filters),
        'purchases': value.purchases === undefined ? undefined : (value.purchases.map(_1.PurchaseToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.PurchasesDeleteResponseToJSON = PurchasesDeleteResponseToJSON;
