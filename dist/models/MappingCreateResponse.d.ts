/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Mapping } from './';
/**
 *
 * @export
 * @interface MappingCreateResponse
 */
export interface MappingCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof MappingCreateResponse
     */
    success: MappingCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingCreateResponse
     */
    code: MappingCreateResponseCodeEnum;
    /**
     *
     * @type {Mapping}
     * @memberof MappingCreateResponse
     */
    mapping?: Mapping;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum MappingCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function MappingCreateResponseFromJSON(json: any): MappingCreateResponse;
export declare function MappingCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingCreateResponse;
export declare function MappingCreateResponseToJSON(value?: MappingCreateResponse | null): any;
