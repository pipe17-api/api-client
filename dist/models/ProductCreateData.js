"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductCreateDataToJSON = exports.ProductCreateDataFromJSONTyped = exports.ProductCreateDataFromJSON = exports.ProductCreateDataDimensionsUnitEnum = exports.ProductCreateDataWeightUnitEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductCreateDataWeightUnitEnum;
(function (ProductCreateDataWeightUnitEnum) {
    ProductCreateDataWeightUnitEnum["Lb"] = "lb";
    ProductCreateDataWeightUnitEnum["Oz"] = "oz";
    ProductCreateDataWeightUnitEnum["Kg"] = "kg";
    ProductCreateDataWeightUnitEnum["G"] = "g";
})(ProductCreateDataWeightUnitEnum = exports.ProductCreateDataWeightUnitEnum || (exports.ProductCreateDataWeightUnitEnum = {})); /**
* @export
* @enum {string}
*/
var ProductCreateDataDimensionsUnitEnum;
(function (ProductCreateDataDimensionsUnitEnum) {
    ProductCreateDataDimensionsUnitEnum["Cm"] = "cm";
    ProductCreateDataDimensionsUnitEnum["In"] = "in";
    ProductCreateDataDimensionsUnitEnum["Ft"] = "ft";
})(ProductCreateDataDimensionsUnitEnum = exports.ProductCreateDataDimensionsUnitEnum || (exports.ProductCreateDataDimensionsUnitEnum = {}));
function ProductCreateDataFromJSON(json) {
    return ProductCreateDataFromJSONTyped(json, false);
}
exports.ProductCreateDataFromJSON = ProductCreateDataFromJSON;
function ProductCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': json['sku'],
        'name': json['name'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ProductStatusFromJSON(json['status']),
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'types': !runtime_1.exists(json, 'types') ? undefined : (json['types'].map(_1.ProductTypeFromJSON)),
        'extProductId': !runtime_1.exists(json, 'extProductId') ? undefined : json['extProductId'],
        'extProductApiId': !runtime_1.exists(json, 'extProductApiId') ? undefined : json['extProductApiId'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'partId': !runtime_1.exists(json, 'partId') ? undefined : json['partId'],
        'prices': !runtime_1.exists(json, 'prices') ? undefined : (json['prices'].map(_1.PriceFromJSON)),
        'cost': !runtime_1.exists(json, 'cost') ? undefined : json['cost'],
        'costCurrency': !runtime_1.exists(json, 'costCurrency') ? undefined : json['costCurrency'],
        'taxable': !runtime_1.exists(json, 'taxable') ? undefined : json['taxable'],
        'countryOfOrigin': !runtime_1.exists(json, 'countryOfOrigin') ? undefined : json['countryOfOrigin'],
        'harmonizedCode': !runtime_1.exists(json, 'harmonizedCode') ? undefined : json['harmonizedCode'],
        'weight': !runtime_1.exists(json, 'weight') ? undefined : json['weight'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'height': !runtime_1.exists(json, 'height') ? undefined : json['height'],
        'length': !runtime_1.exists(json, 'length') ? undefined : json['length'],
        'width': !runtime_1.exists(json, 'width') ? undefined : json['width'],
        'dimensionsUnit': !runtime_1.exists(json, 'dimensionsUnit') ? undefined : json['dimensionsUnit'],
        'vendor': !runtime_1.exists(json, 'vendor') ? undefined : json['vendor'],
        'tags': !runtime_1.exists(json, 'tags') ? undefined : json['tags'],
        'imageURLs': !runtime_1.exists(json, 'imageURLs') ? undefined : json['imageURLs'],
        'parentExtProductId': !runtime_1.exists(json, 'parentExtProductId') ? undefined : json['parentExtProductId'],
        'variantOptions': !runtime_1.exists(json, 'variantOptions') ? undefined : (json['variantOptions'].map(_1.NameValueFromJSON)),
        'bundleItems': !runtime_1.exists(json, 'bundleItems') ? undefined : (json['bundleItems'].map(_1.ProductBundleItemFromJSON)),
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'published': !runtime_1.exists(json, 'published') ? undefined : (json['published'].map(_1.ProductPublishedItemFromJSON)),
        'inventoryNotTracked': !runtime_1.exists(json, 'inventoryNotTracked') ? undefined : json['inventoryNotTracked'],
        'hideInStore': !runtime_1.exists(json, 'hideInStore') ? undefined : json['hideInStore'],
        'extProductCreatedAt': !runtime_1.exists(json, 'extProductCreatedAt') ? undefined : (new Date(json['extProductCreatedAt'])),
        'extProductUpdatedAt': !runtime_1.exists(json, 'extProductUpdatedAt') ? undefined : (new Date(json['extProductUpdatedAt'])),
    };
}
exports.ProductCreateDataFromJSONTyped = ProductCreateDataFromJSONTyped;
function ProductCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'name': value.name,
        'status': _1.ProductStatusToJSON(value.status),
        'description': value.description,
        'types': value.types === undefined ? undefined : (value.types.map(_1.ProductTypeToJSON)),
        'extProductId': value.extProductId,
        'extProductApiId': value.extProductApiId,
        'upc': value.upc,
        'partId': value.partId,
        'prices': value.prices === undefined ? undefined : (value.prices.map(_1.PriceToJSON)),
        'cost': value.cost,
        'costCurrency': value.costCurrency,
        'taxable': value.taxable,
        'countryOfOrigin': value.countryOfOrigin,
        'harmonizedCode': value.harmonizedCode,
        'weight': value.weight,
        'weightUnit': value.weightUnit,
        'height': value.height,
        'length': value.length,
        'width': value.width,
        'dimensionsUnit': value.dimensionsUnit,
        'vendor': value.vendor,
        'tags': value.tags,
        'imageURLs': value.imageURLs,
        'parentExtProductId': value.parentExtProductId,
        'variantOptions': value.variantOptions === undefined ? undefined : (value.variantOptions.map(_1.NameValueToJSON)),
        'bundleItems': value.bundleItems === undefined ? undefined : (value.bundleItems.map(_1.ProductBundleItemToJSON)),
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'published': value.published === undefined ? undefined : (value.published.map(_1.ProductPublishedItemToJSON)),
        'inventoryNotTracked': value.inventoryNotTracked,
        'hideInStore': value.hideInStore,
        'extProductCreatedAt': value.extProductCreatedAt === undefined ? undefined : (new Date(value.extProductCreatedAt).toISOString()),
        'extProductUpdatedAt': value.extProductUpdatedAt === undefined ? undefined : (new Date(value.extProductUpdatedAt).toISOString()),
    };
}
exports.ProductCreateDataToJSON = ProductCreateDataToJSON;
