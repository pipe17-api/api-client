"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsCreateResponseToJSON = exports.ProductsCreateResponseFromJSONTyped = exports.ProductsCreateResponseFromJSON = exports.ProductsCreateResponseCodeEnum = exports.ProductsCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductsCreateResponseSuccessEnum;
(function (ProductsCreateResponseSuccessEnum) {
    ProductsCreateResponseSuccessEnum["True"] = "true";
})(ProductsCreateResponseSuccessEnum = exports.ProductsCreateResponseSuccessEnum || (exports.ProductsCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ProductsCreateResponseCodeEnum;
(function (ProductsCreateResponseCodeEnum) {
    ProductsCreateResponseCodeEnum[ProductsCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ProductsCreateResponseCodeEnum[ProductsCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ProductsCreateResponseCodeEnum[ProductsCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ProductsCreateResponseCodeEnum = exports.ProductsCreateResponseCodeEnum || (exports.ProductsCreateResponseCodeEnum = {}));
function ProductsCreateResponseFromJSON(json) {
    return ProductsCreateResponseFromJSONTyped(json, false);
}
exports.ProductsCreateResponseFromJSON = ProductsCreateResponseFromJSON;
function ProductsCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'products': !runtime_1.exists(json, 'products') ? undefined : (json['products'].map(_1.ProductCreateResultFromJSON)),
    };
}
exports.ProductsCreateResponseFromJSONTyped = ProductsCreateResponseFromJSONTyped;
function ProductsCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'products': value.products === undefined ? undefined : (value.products.map(_1.ProductCreateResultToJSON)),
    };
}
exports.ProductsCreateResponseToJSON = ProductsCreateResponseToJSON;
