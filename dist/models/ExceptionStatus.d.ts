/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Exception status
 * @export
 * @enum {string}
 */
export declare enum ExceptionStatus {
    Active = "active",
    Dismissed = "dismissed",
    Resolved = "resolved"
}
export declare function ExceptionStatusFromJSON(json: any): ExceptionStatus;
export declare function ExceptionStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionStatus;
export declare function ExceptionStatusToJSON(value?: ExceptionStatus | null): any;
