"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationsListResponseAllOfToJSON = exports.IntegrationsListResponseAllOfFromJSONTyped = exports.IntegrationsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationsListResponseAllOfFromJSON(json) {
    return IntegrationsListResponseAllOfFromJSONTyped(json, false);
}
exports.IntegrationsListResponseAllOfFromJSON = IntegrationsListResponseAllOfFromJSON;
function IntegrationsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationsListFilterFromJSON(json['filters']),
        'integrations': !runtime_1.exists(json, 'integrations') ? undefined : (json['integrations'].map(_1.IntegrationBaseFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.IntegrationsListResponseAllOfFromJSONTyped = IntegrationsListResponseAllOfFromJSONTyped;
function IntegrationsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.IntegrationsListFilterToJSON(value.filters),
        'integrations': value.integrations === undefined ? undefined : (value.integrations.map(_1.IntegrationBaseToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.IntegrationsListResponseAllOfToJSON = IntegrationsListResponseAllOfToJSON;
