"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleUpdateErrorToJSON = exports.RoleUpdateErrorFromJSONTyped = exports.RoleUpdateErrorFromJSON = exports.RoleUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoleUpdateErrorSuccessEnum;
(function (RoleUpdateErrorSuccessEnum) {
    RoleUpdateErrorSuccessEnum["False"] = "false";
})(RoleUpdateErrorSuccessEnum = exports.RoleUpdateErrorSuccessEnum || (exports.RoleUpdateErrorSuccessEnum = {}));
function RoleUpdateErrorFromJSON(json) {
    return RoleUpdateErrorFromJSONTyped(json, false);
}
exports.RoleUpdateErrorFromJSON = RoleUpdateErrorFromJSON;
function RoleUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleUpdateDataFromJSON(json['role']),
    };
}
exports.RoleUpdateErrorFromJSONTyped = RoleUpdateErrorFromJSONTyped;
function RoleUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'role': _1.RoleUpdateDataToJSON(value.role),
    };
}
exports.RoleUpdateErrorToJSON = RoleUpdateErrorToJSON;
