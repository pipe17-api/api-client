/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentUpdateData } from './';
/**
 *
 * @export
 * @interface ShipmentUpdateError
 */
export interface ShipmentUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ShipmentUpdateError
     */
    success: ShipmentUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ShipmentUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ShipmentUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ShipmentUpdateData}
     * @memberof ShipmentUpdateError
     */
    shipment?: ShipmentUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ShipmentUpdateErrorFromJSON(json: any): ShipmentUpdateError;
export declare function ShipmentUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentUpdateError;
export declare function ShipmentUpdateErrorToJSON(value?: ShipmentUpdateError | null): any;
