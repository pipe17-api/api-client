"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseUpdateResponseToJSON = exports.PurchaseUpdateResponseFromJSONTyped = exports.PurchaseUpdateResponseFromJSON = exports.PurchaseUpdateResponseCodeEnum = exports.PurchaseUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var PurchaseUpdateResponseSuccessEnum;
(function (PurchaseUpdateResponseSuccessEnum) {
    PurchaseUpdateResponseSuccessEnum["True"] = "true";
})(PurchaseUpdateResponseSuccessEnum = exports.PurchaseUpdateResponseSuccessEnum || (exports.PurchaseUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var PurchaseUpdateResponseCodeEnum;
(function (PurchaseUpdateResponseCodeEnum) {
    PurchaseUpdateResponseCodeEnum[PurchaseUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    PurchaseUpdateResponseCodeEnum[PurchaseUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    PurchaseUpdateResponseCodeEnum[PurchaseUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(PurchaseUpdateResponseCodeEnum = exports.PurchaseUpdateResponseCodeEnum || (exports.PurchaseUpdateResponseCodeEnum = {}));
function PurchaseUpdateResponseFromJSON(json) {
    return PurchaseUpdateResponseFromJSONTyped(json, false);
}
exports.PurchaseUpdateResponseFromJSON = PurchaseUpdateResponseFromJSON;
function PurchaseUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.PurchaseUpdateResponseFromJSONTyped = PurchaseUpdateResponseFromJSONTyped;
function PurchaseUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.PurchaseUpdateResponseToJSON = PurchaseUpdateResponseToJSON;
