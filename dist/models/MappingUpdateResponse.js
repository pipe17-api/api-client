"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingUpdateResponseToJSON = exports.MappingUpdateResponseFromJSONTyped = exports.MappingUpdateResponseFromJSON = exports.MappingUpdateResponseCodeEnum = exports.MappingUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var MappingUpdateResponseSuccessEnum;
(function (MappingUpdateResponseSuccessEnum) {
    MappingUpdateResponseSuccessEnum["True"] = "true";
})(MappingUpdateResponseSuccessEnum = exports.MappingUpdateResponseSuccessEnum || (exports.MappingUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var MappingUpdateResponseCodeEnum;
(function (MappingUpdateResponseCodeEnum) {
    MappingUpdateResponseCodeEnum[MappingUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    MappingUpdateResponseCodeEnum[MappingUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    MappingUpdateResponseCodeEnum[MappingUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(MappingUpdateResponseCodeEnum = exports.MappingUpdateResponseCodeEnum || (exports.MappingUpdateResponseCodeEnum = {}));
function MappingUpdateResponseFromJSON(json) {
    return MappingUpdateResponseFromJSONTyped(json, false);
}
exports.MappingUpdateResponseFromJSON = MappingUpdateResponseFromJSON;
function MappingUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.MappingUpdateResponseFromJSONTyped = MappingUpdateResponseFromJSONTyped;
function MappingUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.MappingUpdateResponseToJSON = MappingUpdateResponseToJSON;
