/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchema } from './';
/**
 *
 * @export
 * @interface EntitySchemaFetchResponse
 */
export interface EntitySchemaFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntitySchemaFetchResponse
     */
    success: EntitySchemaFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaFetchResponse
     */
    code: EntitySchemaFetchResponseCodeEnum;
    /**
     *
     * @type {EntitySchema}
     * @memberof EntitySchemaFetchResponse
     */
    entitySchema?: EntitySchema;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntitySchemaFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntitySchemaFetchResponseFromJSON(json: any): EntitySchemaFetchResponse;
export declare function EntitySchemaFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaFetchResponse;
export declare function EntitySchemaFetchResponseToJSON(value?: EntitySchemaFetchResponse | null): any;
