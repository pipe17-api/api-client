"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionCreateResponseAllOfToJSON = exports.ExceptionCreateResponseAllOfFromJSONTyped = exports.ExceptionCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionCreateResponseAllOfFromJSON(json) {
    return ExceptionCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ExceptionCreateResponseAllOfFromJSON = ExceptionCreateResponseAllOfFromJSON;
function ExceptionCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionFromJSON(json['exception']),
    };
}
exports.ExceptionCreateResponseAllOfFromJSONTyped = ExceptionCreateResponseAllOfFromJSONTyped;
function ExceptionCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exception': _1.ExceptionToJSON(value.exception),
    };
}
exports.ExceptionCreateResponseAllOfToJSON = ExceptionCreateResponseAllOfToJSON;
