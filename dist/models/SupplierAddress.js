"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierAddressToJSON = exports.SupplierAddressFromJSONTyped = exports.SupplierAddressFromJSON = void 0;
const runtime_1 = require("../runtime");
function SupplierAddressFromJSON(json) {
    return SupplierAddressFromJSONTyped(json, false);
}
exports.SupplierAddressFromJSON = SupplierAddressFromJSON;
function SupplierAddressFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'address1': !runtime_1.exists(json, 'address1') ? undefined : json['address1'],
        'address2': !runtime_1.exists(json, 'address2') ? undefined : json['address2'],
        'city': !runtime_1.exists(json, 'city') ? undefined : json['city'],
        'stateOrProvince': !runtime_1.exists(json, 'stateOrProvince') ? undefined : json['stateOrProvince'],
        'zipCodeOrPostalCode': !runtime_1.exists(json, 'zipCodeOrPostalCode') ? undefined : json['zipCodeOrPostalCode'],
        'country': !runtime_1.exists(json, 'country') ? undefined : json['country'],
    };
}
exports.SupplierAddressFromJSONTyped = SupplierAddressFromJSONTyped;
function SupplierAddressToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'address1': value.address1,
        'address2': value.address2,
        'city': value.city,
        'stateOrProvince': value.stateOrProvince,
        'zipCodeOrPostalCode': value.zipCodeOrPostalCode,
        'country': value.country,
    };
}
exports.SupplierAddressToJSON = SupplierAddressToJSON;
