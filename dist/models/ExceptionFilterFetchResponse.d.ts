/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilter } from './';
/**
 *
 * @export
 * @interface ExceptionFilterFetchResponse
 */
export interface ExceptionFilterFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionFilterFetchResponse
     */
    success: ExceptionFilterFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterFetchResponse
     */
    code: ExceptionFilterFetchResponseCodeEnum;
    /**
     *
     * @type {ExceptionFilter}
     * @memberof ExceptionFilterFetchResponse
     */
    exceptionFilter?: ExceptionFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionFilterFetchResponseFromJSON(json: any): ExceptionFilterFetchResponse;
export declare function ExceptionFilterFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterFetchResponse;
export declare function ExceptionFilterFetchResponseToJSON(value?: ExceptionFilterFetchResponse | null): any;
