"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryCreateDataToJSON = exports.InventoryCreateDataFromJSONTyped = exports.InventoryCreateDataFromJSON = exports.InventoryCreateDataEventEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryCreateDataEventEnum;
(function (InventoryCreateDataEventEnum) {
    InventoryCreateDataEventEnum["Ingest"] = "ingest";
    InventoryCreateDataEventEnum["Adjust"] = "adjust";
})(InventoryCreateDataEventEnum = exports.InventoryCreateDataEventEnum || (exports.InventoryCreateDataEventEnum = {}));
function InventoryCreateDataFromJSON(json) {
    return InventoryCreateDataFromJSONTyped(json, false);
}
exports.InventoryCreateDataFromJSON = InventoryCreateDataFromJSON;
function InventoryCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'event': json['event'],
        'ptype': _1.EventSourceFromJSON(json['ptype']),
        'sku': json['sku'],
        'locationId': json['locationId'],
        'onHand': json['onHand'],
        'committed': json['committed'],
        'committedFuture': !runtime_1.exists(json, 'committedFuture') ? undefined : json['committedFuture'],
        'future': !runtime_1.exists(json, 'future') ? undefined : json['future'],
        'available': !runtime_1.exists(json, 'available') ? undefined : json['available'],
        'incoming': !runtime_1.exists(json, 'incoming') ? undefined : json['incoming'],
        'commitShip': !runtime_1.exists(json, 'commitShip') ? undefined : json['commitShip'],
        'commitXfer': !runtime_1.exists(json, 'commitXfer') ? undefined : json['commitXfer'],
        'unavailable': !runtime_1.exists(json, 'unavailable') ? undefined : json['unavailable'],
    };
}
exports.InventoryCreateDataFromJSONTyped = InventoryCreateDataFromJSONTyped;
function InventoryCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'event': value.event,
        'ptype': _1.EventSourceToJSON(value.ptype),
        'sku': value.sku,
        'locationId': value.locationId,
        'onHand': value.onHand,
        'committed': value.committed,
        'committedFuture': value.committedFuture,
        'future': value.future,
        'available': value.available,
        'incoming': value.incoming,
        'commitShip': value.commitShip,
        'commitXfer': value.commitXfer,
        'unavailable': value.unavailable,
    };
}
exports.InventoryCreateDataToJSON = InventoryCreateDataToJSON;
