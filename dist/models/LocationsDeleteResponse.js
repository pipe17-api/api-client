"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsDeleteResponseToJSON = exports.LocationsDeleteResponseFromJSONTyped = exports.LocationsDeleteResponseFromJSON = exports.LocationsDeleteResponseCodeEnum = exports.LocationsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationsDeleteResponseSuccessEnum;
(function (LocationsDeleteResponseSuccessEnum) {
    LocationsDeleteResponseSuccessEnum["True"] = "true";
})(LocationsDeleteResponseSuccessEnum = exports.LocationsDeleteResponseSuccessEnum || (exports.LocationsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LocationsDeleteResponseCodeEnum;
(function (LocationsDeleteResponseCodeEnum) {
    LocationsDeleteResponseCodeEnum[LocationsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LocationsDeleteResponseCodeEnum[LocationsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LocationsDeleteResponseCodeEnum[LocationsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LocationsDeleteResponseCodeEnum = exports.LocationsDeleteResponseCodeEnum || (exports.LocationsDeleteResponseCodeEnum = {}));
function LocationsDeleteResponseFromJSON(json) {
    return LocationsDeleteResponseFromJSONTyped(json, false);
}
exports.LocationsDeleteResponseFromJSON = LocationsDeleteResponseFromJSON;
function LocationsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsDeleteFilterFromJSON(json['filters']),
        'locations': !runtime_1.exists(json, 'locations') ? undefined : (json['locations'].map(_1.LocationFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.LocationsDeleteResponseFromJSONTyped = LocationsDeleteResponseFromJSONTyped;
function LocationsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.LocationsDeleteFilterToJSON(value.filters),
        'locations': value.locations === undefined ? undefined : (value.locations.map(_1.LocationToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.LocationsDeleteResponseToJSON = LocationsDeleteResponseToJSON;
