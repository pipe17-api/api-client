/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyFetchError
 */
export interface ApiKeyFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ApiKeyFetchError
     */
    success: ApiKeyFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ApiKeyFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ApiKeyFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyFetchErrorSuccessEnum {
    False = "false"
}
export declare function ApiKeyFetchErrorFromJSON(json: any): ApiKeyFetchError;
export declare function ApiKeyFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyFetchError;
export declare function ApiKeyFetchErrorToJSON(value?: ApiKeyFetchError | null): any;
