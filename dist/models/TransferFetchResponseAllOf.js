"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferFetchResponseAllOfToJSON = exports.TransferFetchResponseAllOfFromJSONTyped = exports.TransferFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransferFetchResponseAllOfFromJSON(json) {
    return TransferFetchResponseAllOfFromJSONTyped(json, false);
}
exports.TransferFetchResponseAllOfFromJSON = TransferFetchResponseAllOfFromJSON;
function TransferFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'transfer': !runtime_1.exists(json, 'transfer') ? undefined : _1.TransferFromJSON(json['transfer']),
    };
}
exports.TransferFetchResponseAllOfFromJSONTyped = TransferFetchResponseAllOfFromJSONTyped;
function TransferFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'transfer': _1.TransferToJSON(value.transfer),
    };
}
exports.TransferFetchResponseAllOfToJSON = TransferFetchResponseAllOfToJSON;
