/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Receipt } from './';
/**
 *
 * @export
 * @interface ReceiptFetchResponseAllOf
 */
export interface ReceiptFetchResponseAllOf {
    /**
     *
     * @type {Receipt}
     * @memberof ReceiptFetchResponseAllOf
     */
    receipt?: Receipt;
}
export declare function ReceiptFetchResponseAllOfFromJSON(json: any): ReceiptFetchResponseAllOf;
export declare function ReceiptFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptFetchResponseAllOf;
export declare function ReceiptFetchResponseAllOfToJSON(value?: ReceiptFetchResponseAllOf | null): any;
