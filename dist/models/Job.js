"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobToJSON = exports.JobFromJSONTyped = exports.JobFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobFromJSON(json) {
    return JobFromJSONTyped(json, false);
}
exports.JobFromJSON = JobFromJSON;
function JobFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'jobId': !runtime_1.exists(json, 'jobId') ? undefined : json['jobId'],
        'progress': !runtime_1.exists(json, 'progress') ? undefined : json['progress'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.JobStatusFromJSON(json['status']),
        'type': _1.JobTypeFromJSON(json['type']),
        'subType': _1.JobSubTypeFromJSON(json['subType']),
        'params': !runtime_1.exists(json, 'params') ? undefined : json['params'],
        'contentType': !runtime_1.exists(json, 'contentType') ? undefined : json['contentType'],
        'timeout': !runtime_1.exists(json, 'timeout') ? undefined : json['timeout'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.JobFromJSONTyped = JobFromJSONTyped;
function JobToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'jobId': value.jobId,
        'progress': value.progress,
        'status': _1.JobStatusToJSON(value.status),
        'type': _1.JobTypeToJSON(value.type),
        'subType': _1.JobSubTypeToJSON(value.subType),
        'params': value.params,
        'contentType': value.contentType,
        'timeout': value.timeout,
    };
}
exports.JobToJSON = JobToJSON;
