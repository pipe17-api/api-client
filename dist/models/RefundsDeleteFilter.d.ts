/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus, RefundType } from './';
/**
 *
 * @export
 * @interface RefundsDeleteFilter
 */
export interface RefundsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RefundsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RefundsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RefundsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RefundsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RefundsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RefundsDeleteFilter
     */
    count?: number;
    /**
     * Refunds by list of refundId
     * @type {Array<string>}
     * @memberof RefundsDeleteFilter
     */
    refundId?: Array<string>;
    /**
     * Refunds by list of external refund ids
     * @type {Array<string>}
     * @memberof RefundsDeleteFilter
     */
    extRefundId?: Array<string>;
    /**
     * Refunds by list of external refund ids
     * @type {Array<string>}
     * @memberof RefundsDeleteFilter
     */
    extReturnId?: Array<string>;
    /**
     * Refunds by list of external order ids
     * @type {Array<string>}
     * @memberof RefundsDeleteFilter
     */
    extOrderId?: Array<string>;
    /**
     * Refunds by list of types
     * @type {Array<RefundType>}
     * @memberof RefundsDeleteFilter
     */
    type?: Array<RefundType>;
    /**
     * Refunds by list of statuses
     * @type {Array<RefundStatus>}
     * @memberof RefundsDeleteFilter
     */
    status?: Array<RefundStatus>;
    /**
     * Soft deleted refunds
     * @type {boolean}
     * @memberof RefundsDeleteFilter
     */
    deleted?: boolean;
    /**
     * Refunds of these integrations
     * @type {Array<string>}
     * @memberof RefundsDeleteFilter
     */
    integration?: Array<string>;
}
export declare function RefundsDeleteFilterFromJSON(json: any): RefundsDeleteFilter;
export declare function RefundsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsDeleteFilter;
export declare function RefundsDeleteFilterToJSON(value?: RefundsDeleteFilter | null): any;
