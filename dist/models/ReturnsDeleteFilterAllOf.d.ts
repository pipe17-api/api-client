/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Returns Filter
 * @export
 * @interface ReturnsDeleteFilterAllOf
 */
export interface ReturnsDeleteFilterAllOf {
    /**
     *
     * @type {Array<string>}
     * @memberof ReturnsDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function ReturnsDeleteFilterAllOfFromJSON(json: any): ReturnsDeleteFilterAllOf;
export declare function ReturnsDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsDeleteFilterAllOf;
export declare function ReturnsDeleteFilterAllOfToJSON(value?: ReturnsDeleteFilterAllOf | null): any;
