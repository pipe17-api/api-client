/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelsListFilter
 */
export interface LabelsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof LabelsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof LabelsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof LabelsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof LabelsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof LabelsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof LabelsListFilter
     */
    count?: number;
    /**
     * Labels by list of labelId
     * @type {Array<string>}
     * @memberof LabelsListFilter
     */
    labelId?: Array<string>;
    /**
     * Soft deleted labels
     * @type {boolean}
     * @memberof LabelsListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof LabelsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof LabelsListFilter
     */
    keys?: string;
}
export declare function LabelsListFilterFromJSON(json: any): LabelsListFilter;
export declare function LabelsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsListFilter;
export declare function LabelsListFilterToJSON(value?: LabelsListFilter | null): any;
