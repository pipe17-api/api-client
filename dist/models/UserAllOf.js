"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserAllOfToJSON = exports.UserAllOfFromJSONTyped = exports.UserAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UserAllOfFromJSON(json) {
    return UserAllOfFromJSONTyped(json, false);
}
exports.UserAllOfFromJSON = UserAllOfFromJSON;
function UserAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'userId': !runtime_1.exists(json, 'userId') ? undefined : json['userId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.UserStatusFromJSON(json['status']),
    };
}
exports.UserAllOfFromJSONTyped = UserAllOfFromJSONTyped;
function UserAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'userId': value.userId,
        'status': _1.UserStatusToJSON(value.status),
    };
}
exports.UserAllOfToJSON = UserAllOfToJSON;
