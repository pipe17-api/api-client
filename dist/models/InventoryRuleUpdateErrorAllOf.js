"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleUpdateErrorAllOfToJSON = exports.InventoryRuleUpdateErrorAllOfFromJSONTyped = exports.InventoryRuleUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryRuleUpdateErrorAllOfFromJSON(json) {
    return InventoryRuleUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.InventoryRuleUpdateErrorAllOfFromJSON = InventoryRuleUpdateErrorAllOfFromJSON;
function InventoryRuleUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventoryrule': !runtime_1.exists(json, 'inventoryrule') ? undefined : _1.InventoryRuleUpdateDataFromJSON(json['inventoryrule']),
    };
}
exports.InventoryRuleUpdateErrorAllOfFromJSONTyped = InventoryRuleUpdateErrorAllOfFromJSONTyped;
function InventoryRuleUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventoryrule': _1.InventoryRuleUpdateDataToJSON(value.inventoryrule),
    };
}
exports.InventoryRuleUpdateErrorAllOfToJSON = InventoryRuleUpdateErrorAllOfToJSON;
