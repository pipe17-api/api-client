/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorsListFilter } from './';
/**
 *
 * @export
 * @interface ConnectorsListErrorAllOf
 */
export interface ConnectorsListErrorAllOf {
    /**
     *
     * @type {ConnectorsListFilter}
     * @memberof ConnectorsListErrorAllOf
     */
    filters?: ConnectorsListFilter;
}
export declare function ConnectorsListErrorAllOfFromJSON(json: any): ConnectorsListErrorAllOf;
export declare function ConnectorsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsListErrorAllOf;
export declare function ConnectorsListErrorAllOfToJSON(value?: ConnectorsListErrorAllOf | null): any;
