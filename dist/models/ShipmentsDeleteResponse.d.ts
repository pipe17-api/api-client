/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Shipment, ShipmentsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsDeleteResponse
 */
export interface ShipmentsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ShipmentsDeleteResponse
     */
    success: ShipmentsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentsDeleteResponse
     */
    code: ShipmentsDeleteResponseCodeEnum;
    /**
     *
     * @type {ShipmentsDeleteFilter}
     * @memberof ShipmentsDeleteResponse
     */
    filters?: ShipmentsDeleteFilter;
    /**
     *
     * @type {Array<Shipment>}
     * @memberof ShipmentsDeleteResponse
     */
    shipments?: Array<Shipment>;
    /**
     * Number of deleted shipments
     * @type {number}
     * @memberof ShipmentsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ShipmentsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ShipmentsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ShipmentsDeleteResponseFromJSON(json: any): ShipmentsDeleteResponse;
export declare function ShipmentsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsDeleteResponse;
export declare function ShipmentsDeleteResponseToJSON(value?: ShipmentsDeleteResponse | null): any;
