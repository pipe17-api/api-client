/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Account's Status
 * @export
 * @enum {string}
 */
export declare enum AccountStatus {
    Active = "active",
    Disabled = "disabled"
}
export declare function AccountStatusFromJSON(json: any): AccountStatus;
export declare function AccountStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountStatus;
export declare function AccountStatusToJSON(value?: AccountStatus | null): any;
