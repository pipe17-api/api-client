"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersDeleteResponseAllOfToJSON = exports.OrdersDeleteResponseAllOfFromJSONTyped = exports.OrdersDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersDeleteResponseAllOfFromJSON(json) {
    return OrdersDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.OrdersDeleteResponseAllOfFromJSON = OrdersDeleteResponseAllOfFromJSON;
function OrdersDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersDeleteFilterFromJSON(json['filters']),
        'orders': !runtime_1.exists(json, 'orders') ? undefined : (json['orders'].map(_1.OrderFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrdersDeleteResponseAllOfFromJSONTyped = OrdersDeleteResponseAllOfFromJSONTyped;
function OrdersDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.OrdersDeleteFilterToJSON(value.filters),
        'orders': value.orders === undefined ? undefined : (value.orders.map(_1.OrderToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrdersDeleteResponseAllOfToJSON = OrdersDeleteResponseAllOfToJSON;
