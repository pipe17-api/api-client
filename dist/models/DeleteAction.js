"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.DeleteActionToJSON = exports.DeleteActionFromJSONTyped = exports.DeleteActionFromJSON = exports.DeleteAction = void 0;
/**
 * Action for delete operation
 * @export
 * @enum {string}
 */
var DeleteAction;
(function (DeleteAction) {
    DeleteAction["Soft"] = "soft";
    DeleteAction["Hard"] = "hard";
})(DeleteAction = exports.DeleteAction || (exports.DeleteAction = {}));
function DeleteActionFromJSON(json) {
    return DeleteActionFromJSONTyped(json, false);
}
exports.DeleteActionFromJSON = DeleteActionFromJSON;
function DeleteActionFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.DeleteActionFromJSONTyped = DeleteActionFromJSONTyped;
function DeleteActionToJSON(value) {
    return value;
}
exports.DeleteActionToJSON = DeleteActionToJSON;
