"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsCreateResponseAllOfToJSON = exports.ReceiptsCreateResponseAllOfFromJSONTyped = exports.ReceiptsCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReceiptsCreateResponseAllOfFromJSON(json) {
    return ReceiptsCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ReceiptsCreateResponseAllOfFromJSON = ReceiptsCreateResponseAllOfFromJSON;
function ReceiptsCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'receipts': !runtime_1.exists(json, 'receipts') ? undefined : (json['receipts'].map(_1.ReceiptCreateResultFromJSON)),
    };
}
exports.ReceiptsCreateResponseAllOfFromJSONTyped = ReceiptsCreateResponseAllOfFromJSONTyped;
function ReceiptsCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'receipts': value.receipts === undefined ? undefined : (value.receipts.map(_1.ReceiptCreateResultToJSON)),
    };
}
exports.ReceiptsCreateResponseAllOfToJSON = ReceiptsCreateResponseAllOfToJSON;
