/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalUpdateData } from './';
/**
 *
 * @export
 * @interface ArrivalUpdateErrorAllOf
 */
export interface ArrivalUpdateErrorAllOf {
    /**
     *
     * @type {ArrivalUpdateData}
     * @memberof ArrivalUpdateErrorAllOf
     */
    arrival?: ArrivalUpdateData;
}
export declare function ArrivalUpdateErrorAllOfFromJSON(json: any): ArrivalUpdateErrorAllOf;
export declare function ArrivalUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalUpdateErrorAllOf;
export declare function ArrivalUpdateErrorAllOfToJSON(value?: ArrivalUpdateErrorAllOf | null): any;
