"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalToJSON = exports.ArrivalFromJSONTyped = exports.ArrivalFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalFromJSON(json) {
    return ArrivalFromJSONTyped(json, false);
}
exports.ArrivalFromJSON = ArrivalFromJSON;
function ArrivalFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrivalId': !runtime_1.exists(json, 'arrivalId') ? undefined : json['arrivalId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ArrivalStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'transferId': !runtime_1.exists(json, 'transferId') ? undefined : json['transferId'],
        'purchaseId': !runtime_1.exists(json, 'purchaseId') ? undefined : json['purchaseId'],
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'senderName': json['senderName'],
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'toLocationId': json['toLocationId'],
        'fromLocationId': !runtime_1.exists(json, 'fromLocationId') ? undefined : json['fromLocationId'],
        'fromAddress': !runtime_1.exists(json, 'fromAddress') ? undefined : _1.AddressFromJSON(json['fromAddress']),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingMethod': !runtime_1.exists(json, 'shippingMethod') ? undefined : json['shippingMethod'],
        'shippingNote': !runtime_1.exists(json, 'shippingNote') ? undefined : json['shippingNote'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'totalWeight': !runtime_1.exists(json, 'totalWeight') ? undefined : json['totalWeight'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ArrivalLineItemFromJSON)),
        'extArrivalId': !runtime_1.exists(json, 'extArrivalId') ? undefined : json['extArrivalId'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.ArrivalFromJSONTyped = ArrivalFromJSONTyped;
function ArrivalToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrivalId': value.arrivalId,
        'status': _1.ArrivalStatusToJSON(value.status),
        'integration': value.integration,
        'transferId': value.transferId,
        'purchaseId': value.purchaseId,
        'fulfillmentId': value.fulfillmentId,
        'extOrderId': value.extOrderId,
        'senderName': value.senderName,
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'toLocationId': value.toLocationId,
        'fromLocationId': value.fromLocationId,
        'fromAddress': _1.AddressToJSON(value.fromAddress),
        'shippingCarrier': value.shippingCarrier,
        'shippingMethod': value.shippingMethod,
        'shippingNote': value.shippingNote,
        'incoterms': value.incoterms,
        'totalWeight': value.totalWeight,
        'weightUnit': value.weightUnit,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ArrivalLineItemToJSON)),
        'extArrivalId': value.extArrivalId,
    };
}
exports.ArrivalToJSON = ArrivalToJSON;
