/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionType } from './';
/**
 *
 * @export
 * @interface ExceptionFilter
 */
export interface ExceptionFilter {
    /**
     * Filter ID
     * @type {string}
     * @memberof ExceptionFilter
     */
    filterId?: string;
    /**
     *
     * @type {ExceptionType}
     * @memberof ExceptionFilter
     */
    exceptionType: ExceptionType;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilter
     */
    enabled?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilter
     */
    blocking?: boolean;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof ExceptionFilter
     */
    isPublic?: boolean;
    /**
     *
     * @type {object}
     * @memberof ExceptionFilter
     */
    settings?: object;
    /**
     *
     * @type {string}
     * @memberof ExceptionFilter
     */
    originId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof ExceptionFilter
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof ExceptionFilter
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof ExceptionFilter
     */
    readonly orgKey?: string;
}
export declare function ExceptionFilterFromJSON(json: any): ExceptionFilter;
export declare function ExceptionFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilter;
export declare function ExceptionFilterToJSON(value?: ExceptionFilter | null): any;
