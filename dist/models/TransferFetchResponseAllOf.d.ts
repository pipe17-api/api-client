/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Transfer } from './';
/**
 *
 * @export
 * @interface TransferFetchResponseAllOf
 */
export interface TransferFetchResponseAllOf {
    /**
     *
     * @type {Transfer}
     * @memberof TransferFetchResponseAllOf
     */
    transfer?: Transfer;
}
export declare function TransferFetchResponseAllOfFromJSON(json: any): TransferFetchResponseAllOf;
export declare function TransferFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferFetchResponseAllOf;
export declare function TransferFetchResponseAllOfToJSON(value?: TransferFetchResponseAllOf | null): any;
