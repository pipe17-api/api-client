"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderStatusToJSON = exports.OrderStatusFromJSONTyped = exports.OrderStatusFromJSON = exports.OrderStatus = void 0;
/**
 * Order Status
 * @export
 * @enum {string}
 */
var OrderStatus;
(function (OrderStatus) {
    OrderStatus["Draft"] = "draft";
    OrderStatus["New"] = "new";
    OrderStatus["OnHold"] = "onHold";
    OrderStatus["ToBeValidated"] = "toBeValidated";
    OrderStatus["ReviewRequired"] = "reviewRequired";
    OrderStatus["ReadyForFulfillment"] = "readyForFulfillment";
    OrderStatus["SentToFulfillment"] = "sentToFulfillment";
    OrderStatus["PartialFulfillment"] = "partialFulfillment";
    OrderStatus["Fulfilled"] = "fulfilled";
    OrderStatus["InTransit"] = "inTransit";
    OrderStatus["PartialReceived"] = "partialReceived";
    OrderStatus["Received"] = "received";
    OrderStatus["Canceled"] = "canceled";
    OrderStatus["Returned"] = "returned";
    OrderStatus["Refunded"] = "refunded";
})(OrderStatus = exports.OrderStatus || (exports.OrderStatus = {}));
function OrderStatusFromJSON(json) {
    return OrderStatusFromJSONTyped(json, false);
}
exports.OrderStatusFromJSON = OrderStatusFromJSON;
function OrderStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.OrderStatusFromJSONTyped = OrderStatusFromJSONTyped;
function OrderStatusToJSON(value) {
    return value;
}
exports.OrderStatusToJSON = OrderStatusToJSON;
