"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalUpdateErrorToJSON = exports.ArrivalUpdateErrorFromJSONTyped = exports.ArrivalUpdateErrorFromJSON = exports.ArrivalUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalUpdateErrorSuccessEnum;
(function (ArrivalUpdateErrorSuccessEnum) {
    ArrivalUpdateErrorSuccessEnum["False"] = "false";
})(ArrivalUpdateErrorSuccessEnum = exports.ArrivalUpdateErrorSuccessEnum || (exports.ArrivalUpdateErrorSuccessEnum = {}));
function ArrivalUpdateErrorFromJSON(json) {
    return ArrivalUpdateErrorFromJSONTyped(json, false);
}
exports.ArrivalUpdateErrorFromJSON = ArrivalUpdateErrorFromJSON;
function ArrivalUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'arrival': !runtime_1.exists(json, 'arrival') ? undefined : _1.ArrivalUpdateDataFromJSON(json['arrival']),
    };
}
exports.ArrivalUpdateErrorFromJSONTyped = ArrivalUpdateErrorFromJSONTyped;
function ArrivalUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'arrival': _1.ArrivalUpdateDataToJSON(value.arrival),
    };
}
exports.ArrivalUpdateErrorToJSON = ArrivalUpdateErrorToJSON;
