/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserCreateData } from './';
/**
 *
 * @export
 * @interface UserCreateErrorAllOf
 */
export interface UserCreateErrorAllOf {
    /**
     *
     * @type {UserCreateData}
     * @memberof UserCreateErrorAllOf
     */
    user?: UserCreateData;
}
export declare function UserCreateErrorAllOfFromJSON(json: any): UserCreateErrorAllOf;
export declare function UserCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateErrorAllOf;
export declare function UserCreateErrorAllOfToJSON(value?: UserCreateErrorAllOf | null): any;
