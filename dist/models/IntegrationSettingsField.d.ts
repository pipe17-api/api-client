/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationSettingsField
 */
export interface IntegrationSettingsField {
    /**
     * Setting Option Name
     * @type {string}
     * @memberof IntegrationSettingsField
     */
    path?: string;
    /**
     * Setting Option Value
     * @type {object}
     * @memberof IntegrationSettingsField
     */
    value?: object;
}
export declare function IntegrationSettingsFieldFromJSON(json: any): IntegrationSettingsField;
export declare function IntegrationSettingsFieldFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationSettingsField;
export declare function IntegrationSettingsFieldToJSON(value?: IntegrationSettingsField | null): any;
