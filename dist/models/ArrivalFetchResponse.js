"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalFetchResponseToJSON = exports.ArrivalFetchResponseFromJSONTyped = exports.ArrivalFetchResponseFromJSON = exports.ArrivalFetchResponseCodeEnum = exports.ArrivalFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalFetchResponseSuccessEnum;
(function (ArrivalFetchResponseSuccessEnum) {
    ArrivalFetchResponseSuccessEnum["True"] = "true";
})(ArrivalFetchResponseSuccessEnum = exports.ArrivalFetchResponseSuccessEnum || (exports.ArrivalFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ArrivalFetchResponseCodeEnum;
(function (ArrivalFetchResponseCodeEnum) {
    ArrivalFetchResponseCodeEnum[ArrivalFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ArrivalFetchResponseCodeEnum[ArrivalFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ArrivalFetchResponseCodeEnum[ArrivalFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ArrivalFetchResponseCodeEnum = exports.ArrivalFetchResponseCodeEnum || (exports.ArrivalFetchResponseCodeEnum = {}));
function ArrivalFetchResponseFromJSON(json) {
    return ArrivalFetchResponseFromJSONTyped(json, false);
}
exports.ArrivalFetchResponseFromJSON = ArrivalFetchResponseFromJSON;
function ArrivalFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'arrival': !runtime_1.exists(json, 'arrival') ? undefined : _1.ArrivalFromJSON(json['arrival']),
    };
}
exports.ArrivalFetchResponseFromJSONTyped = ArrivalFetchResponseFromJSONTyped;
function ArrivalFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'arrival': _1.ArrivalToJSON(value.arrival),
    };
}
exports.ArrivalFetchResponseToJSON = ArrivalFetchResponseToJSON;
