"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductFetchErrorToJSON = exports.ProductFetchErrorFromJSONTyped = exports.ProductFetchErrorFromJSON = exports.ProductFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ProductFetchErrorSuccessEnum;
(function (ProductFetchErrorSuccessEnum) {
    ProductFetchErrorSuccessEnum["False"] = "false";
})(ProductFetchErrorSuccessEnum = exports.ProductFetchErrorSuccessEnum || (exports.ProductFetchErrorSuccessEnum = {}));
function ProductFetchErrorFromJSON(json) {
    return ProductFetchErrorFromJSONTyped(json, false);
}
exports.ProductFetchErrorFromJSON = ProductFetchErrorFromJSON;
function ProductFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.ProductFetchErrorFromJSONTyped = ProductFetchErrorFromJSONTyped;
function ProductFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.ProductFetchErrorToJSON = ProductFetchErrorToJSON;
