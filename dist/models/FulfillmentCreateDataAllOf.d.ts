/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentItem } from './';
/**
 *
 * @export
 * @interface FulfillmentCreateDataAllOf
 */
export interface FulfillmentCreateDataAllOf {
    /**
     *
     * @type {Array<FulfillmentItem>}
     * @memberof FulfillmentCreateDataAllOf
     */
    lineItems?: Array<FulfillmentItem>;
}
export declare function FulfillmentCreateDataAllOfFromJSON(json: any): FulfillmentCreateDataAllOf;
export declare function FulfillmentCreateDataAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentCreateDataAllOf;
export declare function FulfillmentCreateDataAllOfToJSON(value?: FulfillmentCreateDataAllOf | null): any;
