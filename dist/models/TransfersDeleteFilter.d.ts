/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferStatus } from './';
/**
 *
 * @export
 * @interface TransfersDeleteFilter
 */
export interface TransfersDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof TransfersDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof TransfersDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof TransfersDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof TransfersDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof TransfersDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof TransfersDeleteFilter
     */
    count?: number;
    /**
     * Transfers by list of transferId
     * @type {Array<string>}
     * @memberof TransfersDeleteFilter
     */
    transferId?: Array<string>;
    /**
     * Transfers by list of external transfer IDs
     * @type {Array<string>}
     * @memberof TransfersDeleteFilter
     */
    extOrderId?: Array<string>;
    /**
     * Transfers by list of statuses
     * @type {Array<TransferStatus>}
     * @memberof TransfersDeleteFilter
     */
    status?: Array<TransferStatus>;
    /**
     * Soft deleted transfers
     * @type {boolean}
     * @memberof TransfersDeleteFilter
     */
    deleted?: boolean;
    /**
     * Transfers of these integrations
     * @type {Array<string>}
     * @memberof TransfersDeleteFilter
     */
    integration?: Array<string>;
}
export declare function TransfersDeleteFilterFromJSON(json: any): TransfersDeleteFilter;
export declare function TransfersDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersDeleteFilter;
export declare function TransfersDeleteFilterToJSON(value?: TransfersDeleteFilter | null): any;
