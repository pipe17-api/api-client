"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationCreateErrorAllOfToJSON = exports.IntegrationCreateErrorAllOfFromJSONTyped = exports.IntegrationCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationCreateErrorAllOfFromJSON(json) {
    return IntegrationCreateErrorAllOfFromJSONTyped(json, false);
}
exports.IntegrationCreateErrorAllOfFromJSON = IntegrationCreateErrorAllOfFromJSON;
function IntegrationCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : _1.IntegrationCreateDataFromJSON(json['integration']),
    };
}
exports.IntegrationCreateErrorAllOfFromJSONTyped = IntegrationCreateErrorAllOfFromJSONTyped;
function IntegrationCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': _1.IntegrationCreateDataToJSON(value.integration),
    };
}
exports.IntegrationCreateErrorAllOfToJSON = IntegrationCreateErrorAllOfToJSON;
