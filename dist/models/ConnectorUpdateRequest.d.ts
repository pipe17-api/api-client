/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorConnection, ConnectorEntity, ConnectorSettings, ConnectorType, ConnectorUpdateDataWebhook } from './';
/**
 *
 * @export
 * @interface ConnectorUpdateRequest
 */
export interface ConnectorUpdateRequest {
    /**
     * Connector description
     * @type {string}
     * @memberof ConnectorUpdateRequest
     */
    description?: string;
    /**
     * Connector readme markup
     * @type {string}
     * @memberof ConnectorUpdateRequest
     */
    readme?: string;
    /**
     * Connector version
     * @type {string}
     * @memberof ConnectorUpdateRequest
     */
    version?: string;
    /**
     *
     * @type {ConnectorType}
     * @memberof ConnectorUpdateRequest
     */
    connectorType?: ConnectorType;
    /**
     * Connector Status
     * @type {string}
     * @memberof ConnectorUpdateRequest
     */
    status?: ConnectorUpdateRequestStatusEnum;
    /**
     * Logo URL
     * @type {string}
     * @memberof ConnectorUpdateRequest
     */
    logoUrl?: string | null;
    /**
     *
     * @type {Array<ConnectorEntity>}
     * @memberof ConnectorUpdateRequest
     */
    entities?: Array<ConnectorEntity>;
    /**
     *
     * @type {ConnectorSettings}
     * @memberof ConnectorUpdateRequest
     */
    settings?: ConnectorSettings;
    /**
     *
     * @type {ConnectorConnection}
     * @memberof ConnectorUpdateRequest
     */
    connection?: ConnectorConnection;
    /**
     *
     * @type {ConnectorUpdateDataWebhook}
     * @memberof ConnectorUpdateRequest
     */
    webhook?: ConnectorUpdateDataWebhook | null;
    /**
     * Indicates whether connector/integration should have access to all declared entity types
     * @type {boolean}
     * @memberof ConnectorUpdateRequest
     */
    orgWideAccess?: boolean;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof ConnectorUpdateRequest
     */
    orgUnique?: boolean;
    /**
     * Connector Display Name
     * @type {string}
     * @memberof ConnectorUpdateRequest
     */
    displayName?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorUpdateRequestStatusEnum {
    Active = "active",
    Inactive = "inactive"
}
export declare function ConnectorUpdateRequestFromJSON(json: any): ConnectorUpdateRequest;
export declare function ConnectorUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorUpdateRequest;
export declare function ConnectorUpdateRequestToJSON(value?: ConnectorUpdateRequest | null): any;
