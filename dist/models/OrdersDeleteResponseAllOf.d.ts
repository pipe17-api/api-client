/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order, OrdersDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrdersDeleteResponseAllOf
 */
export interface OrdersDeleteResponseAllOf {
    /**
     *
     * @type {OrdersDeleteFilter}
     * @memberof OrdersDeleteResponseAllOf
     */
    filters?: OrdersDeleteFilter;
    /**
     *
     * @type {Array<Order>}
     * @memberof OrdersDeleteResponseAllOf
     */
    orders?: Array<Order>;
    /**
     * Number of deleted orders
     * @type {number}
     * @memberof OrdersDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof OrdersDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function OrdersDeleteResponseAllOfFromJSON(json: any): OrdersDeleteResponseAllOf;
export declare function OrdersDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersDeleteResponseAllOf;
export declare function OrdersDeleteResponseAllOfToJSON(value?: OrdersDeleteResponseAllOf | null): any;
