/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnsFilter } from './';
/**
 *
 * @export
 * @interface ReturnsListError
 */
export interface ReturnsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReturnsListError
     */
    success: ReturnsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReturnsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReturnsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ReturnsFilter}
     * @memberof ReturnsListError
     */
    filters?: ReturnsFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnsListErrorSuccessEnum {
    False = "false"
}
export declare function ReturnsListErrorFromJSON(json: any): ReturnsListError;
export declare function ReturnsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsListError;
export declare function ReturnsListErrorToJSON(value?: ReturnsListError | null): any;
