"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFiltersFilterAllOfToJSON = exports.EntityFiltersFilterAllOfFromJSONTyped = exports.EntityFiltersFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntityFiltersFilterAllOfFromJSON(json) {
    return EntityFiltersFilterAllOfFromJSONTyped(json, false);
}
exports.EntityFiltersFilterAllOfFromJSON = EntityFiltersFilterAllOfFromJSON;
function EntityFiltersFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filterId': !runtime_1.exists(json, 'filterId') ? undefined : json['filterId'],
        'entity': !runtime_1.exists(json, 'entity') ? undefined : (json['entity'].map(_1.EntityNameFromJSON)),
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
    };
}
exports.EntityFiltersFilterAllOfFromJSONTyped = EntityFiltersFilterAllOfFromJSONTyped;
function EntityFiltersFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filterId': value.filterId,
        'entity': value.entity === undefined ? undefined : (value.entity.map(_1.EntityNameToJSON)),
        'name': value.name,
    };
}
exports.EntityFiltersFilterAllOfToJSON = EntityFiltersFilterAllOfToJSON;
