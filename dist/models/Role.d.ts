/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Methods, RoleStatus } from './';
/**
 *
 * @export
 * @interface Role
 */
export interface Role {
    /**
     * Role ID
     * @type {string}
     * @memberof Role
     */
    roleId?: string;
    /**
     *
     * @type {RoleStatus}
     * @memberof Role
     */
    status?: RoleStatus;
    /**
     * Global role indicator
     * @type {boolean}
     * @memberof Role
     */
    isPublic?: boolean;
    /**
     * Role Name
     * @type {string}
     * @memberof Role
     */
    name: string;
    /**
     * Role's description
     * @type {string}
     * @memberof Role
     */
    description?: string;
    /**
     *
     * @type {Methods}
     * @memberof Role
     */
    methods?: Methods;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Role
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Role
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Role
     */
    readonly orgKey?: string;
}
export declare function RoleFromJSON(json: any): Role;
export declare function RoleFromJSONTyped(json: any, ignoreDiscriminator: boolean): Role;
export declare function RoleToJSON(value?: Role | null): any;
