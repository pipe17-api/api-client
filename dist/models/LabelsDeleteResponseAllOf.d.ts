/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label, LabelsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface LabelsDeleteResponseAllOf
 */
export interface LabelsDeleteResponseAllOf {
    /**
     *
     * @type {LabelsDeleteFilter}
     * @memberof LabelsDeleteResponseAllOf
     */
    filters?: LabelsDeleteFilter;
    /**
     *
     * @type {Array<Label>}
     * @memberof LabelsDeleteResponseAllOf
     */
    labels?: Array<Label>;
    /**
     * Number of deleted labels
     * @type {number}
     * @memberof LabelsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof LabelsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function LabelsDeleteResponseAllOfFromJSON(json: any): LabelsDeleteResponseAllOf;
export declare function LabelsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsDeleteResponseAllOf;
export declare function LabelsDeleteResponseAllOfToJSON(value?: LabelsDeleteResponseAllOf | null): any;
