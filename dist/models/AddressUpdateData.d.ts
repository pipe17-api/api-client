/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AddressUpdateData
 */
export interface AddressUpdateData {
    /**
     * First Name
     * @type {string}
     * @memberof AddressUpdateData
     */
    firstName?: string;
    /**
     * Last Name
     * @type {string}
     * @memberof AddressUpdateData
     */
    lastName?: string;
    /**
     * Company
     * @type {string}
     * @memberof AddressUpdateData
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof AddressUpdateData
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof AddressUpdateData
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof AddressUpdateData
     */
    city?: string;
    /**
     * If within the US, required 2 letter State Code
     * @type {string}
     * @memberof AddressUpdateData
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof AddressUpdateData
     */
    zipCodeOrPostalCode?: string;
    /**
     * ISO 3 letter code only
     * @type {string}
     * @memberof AddressUpdateData
     */
    country?: string;
    /**
     * email
     * @type {string}
     * @memberof AddressUpdateData
     */
    email?: string;
    /**
     * Phone
     * @type {string}
     * @memberof AddressUpdateData
     */
    phone?: string;
}
export declare function AddressUpdateDataFromJSON(json: any): AddressUpdateData;
export declare function AddressUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): AddressUpdateData;
export declare function AddressUpdateDataToJSON(value?: AddressUpdateData | null): any;
