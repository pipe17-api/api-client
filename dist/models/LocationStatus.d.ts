/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Location status
 * @export
 * @enum {string}
 */
export declare enum LocationStatus {
    Active = "active",
    Inactive = "inactive",
    Pending = "pending",
    Deleted = "deleted"
}
export declare function LocationStatusFromJSON(json: any): LocationStatus;
export declare function LocationStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationStatus;
export declare function LocationStatusToJSON(value?: LocationStatus | null): any;
