/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Connector } from './';
/**
 *
 * @export
 * @interface ConnectorCreateResponseAllOf
 */
export interface ConnectorCreateResponseAllOf {
    /**
     *
     * @type {Connector}
     * @memberof ConnectorCreateResponseAllOf
     */
    connector?: Connector;
}
export declare function ConnectorCreateResponseAllOfFromJSON(json: any): ConnectorCreateResponseAllOf;
export declare function ConnectorCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorCreateResponseAllOf;
export declare function ConnectorCreateResponseAllOfToJSON(value?: ConnectorCreateResponseAllOf | null): any;
