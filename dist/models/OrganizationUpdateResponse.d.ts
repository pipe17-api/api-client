/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrganizationUpdateResponse
 */
export interface OrganizationUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrganizationUpdateResponse
     */
    success: OrganizationUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationUpdateResponse
     */
    code: OrganizationUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrganizationUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrganizationUpdateResponseFromJSON(json: any): OrganizationUpdateResponse;
export declare function OrganizationUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationUpdateResponse;
export declare function OrganizationUpdateResponseToJSON(value?: OrganizationUpdateResponse | null): any;
