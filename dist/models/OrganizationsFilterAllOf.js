"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationsFilterAllOfToJSON = exports.OrganizationsFilterAllOfFromJSONTyped = exports.OrganizationsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationsFilterAllOfFromJSON(json) {
    return OrganizationsFilterAllOfFromJSONTyped(json, false);
}
exports.OrganizationsFilterAllOfFromJSON = OrganizationsFilterAllOfFromJSON;
function OrganizationsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.OrganizationStatusFromJSON)),
    };
}
exports.OrganizationsFilterAllOfFromJSONTyped = OrganizationsFilterAllOfFromJSONTyped;
function OrganizationsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'orgKey': value.orgKey,
        'name': value.name,
        'phone': value.phone,
        'email': value.email,
        'status': value.status === undefined ? undefined : (value.status.map(_1.OrganizationStatusToJSON)),
    };
}
exports.OrganizationsFilterAllOfToJSON = OrganizationsFilterAllOfToJSON;
