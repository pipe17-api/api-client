"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookCreateErrorToJSON = exports.WebhookCreateErrorFromJSONTyped = exports.WebhookCreateErrorFromJSON = exports.WebhookCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var WebhookCreateErrorSuccessEnum;
(function (WebhookCreateErrorSuccessEnum) {
    WebhookCreateErrorSuccessEnum["False"] = "false";
})(WebhookCreateErrorSuccessEnum = exports.WebhookCreateErrorSuccessEnum || (exports.WebhookCreateErrorSuccessEnum = {}));
function WebhookCreateErrorFromJSON(json) {
    return WebhookCreateErrorFromJSONTyped(json, false);
}
exports.WebhookCreateErrorFromJSON = WebhookCreateErrorFromJSON;
function WebhookCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.WebhookCreateDataFromJSON(json['webhook']),
    };
}
exports.WebhookCreateErrorFromJSONTyped = WebhookCreateErrorFromJSONTyped;
function WebhookCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'webhook': _1.WebhookCreateDataToJSON(value.webhook),
    };
}
exports.WebhookCreateErrorToJSON = WebhookCreateErrorToJSON;
