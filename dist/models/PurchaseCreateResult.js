"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseCreateResultToJSON = exports.PurchaseCreateResultFromJSONTyped = exports.PurchaseCreateResultFromJSON = exports.PurchaseCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchaseCreateResultStatusEnum;
(function (PurchaseCreateResultStatusEnum) {
    PurchaseCreateResultStatusEnum["Submitted"] = "submitted";
    PurchaseCreateResultStatusEnum["Failed"] = "failed";
})(PurchaseCreateResultStatusEnum = exports.PurchaseCreateResultStatusEnum || (exports.PurchaseCreateResultStatusEnum = {}));
function PurchaseCreateResultFromJSON(json) {
    return PurchaseCreateResultFromJSONTyped(json, false);
}
exports.PurchaseCreateResultFromJSON = PurchaseCreateResultFromJSON;
function PurchaseCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'purchase': !runtime_1.exists(json, 'purchase') ? undefined : _1.PurchaseFromJSON(json['purchase']),
    };
}
exports.PurchaseCreateResultFromJSONTyped = PurchaseCreateResultFromJSONTyped;
function PurchaseCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'purchase': _1.PurchaseToJSON(value.purchase),
    };
}
exports.PurchaseCreateResultToJSON = PurchaseCreateResultToJSON;
