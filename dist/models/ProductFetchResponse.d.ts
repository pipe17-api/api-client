/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Product } from './';
/**
 *
 * @export
 * @interface ProductFetchResponse
 */
export interface ProductFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ProductFetchResponse
     */
    success: ProductFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductFetchResponse
     */
    code: ProductFetchResponseCodeEnum;
    /**
     *
     * @type {Product}
     * @memberof ProductFetchResponse
     */
    product?: Product;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ProductFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ProductFetchResponseFromJSON(json: any): ProductFetchResponse;
export declare function ProductFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductFetchResponse;
export declare function ProductFetchResponseToJSON(value?: ProductFetchResponse | null): any;
