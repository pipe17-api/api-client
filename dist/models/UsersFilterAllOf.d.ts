/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserStatus } from './';
/**
 * Users Filter
 * @export
 * @interface UsersFilterAllOf
 */
export interface UsersFilterAllOf {
    /**
     * Users by list of userId
     * @type {Array<string>}
     * @memberof UsersFilterAllOf
     */
    userId?: Array<string>;
    /**
     * Users whose name contains this string
     * @type {string}
     * @memberof UsersFilterAllOf
     */
    name?: string;
    /**
     * Users whose email contains this string
     * @type {string}
     * @memberof UsersFilterAllOf
     */
    email?: string;
    /**
     * Users by list of statuses
     * @type {Array<UserStatus>}
     * @memberof UsersFilterAllOf
     */
    status?: Array<UserStatus>;
    /**
     * Return users of all organizations
     * @type {string}
     * @memberof UsersFilterAllOf
     */
    organizations?: string;
}
export declare function UsersFilterAllOfFromJSON(json: any): UsersFilterAllOf;
export declare function UsersFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersFilterAllOf;
export declare function UsersFilterAllOfToJSON(value?: UsersFilterAllOf | null): any;
