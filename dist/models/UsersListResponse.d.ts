/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, User, UsersListFilter } from './';
/**
 *
 * @export
 * @interface UsersListResponse
 */
export interface UsersListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof UsersListResponse
     */
    success: UsersListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UsersListResponse
     */
    code: UsersListResponseCodeEnum;
    /**
     *
     * @type {UsersListFilter}
     * @memberof UsersListResponse
     */
    filters?: UsersListFilter;
    /**
     *
     * @type {Array<User>}
     * @memberof UsersListResponse
     */
    users?: Array<User>;
    /**
     *
     * @type {Pagination}
     * @memberof UsersListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum UsersListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum UsersListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function UsersListResponseFromJSON(json: any): UsersListResponse;
export declare function UsersListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersListResponse;
export declare function UsersListResponseToJSON(value?: UsersListResponse | null): any;
