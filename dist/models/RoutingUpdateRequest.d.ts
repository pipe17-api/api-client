/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingUpdateRequest
 */
export interface RoutingUpdateRequest {
    /**
     * Description
     * @type {string}
     * @memberof RoutingUpdateRequest
     */
    description?: string;
    /**
     * Is Routing Filter Enabled
     * @type {boolean}
     * @memberof RoutingUpdateRequest
     */
    enabled?: boolean;
    /**
     * Routing Filter
     * @type {string}
     * @memberof RoutingUpdateRequest
     */
    filter?: string | null;
}
export declare function RoutingUpdateRequestFromJSON(json: any): RoutingUpdateRequest;
export declare function RoutingUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingUpdateRequest;
export declare function RoutingUpdateRequestToJSON(value?: RoutingUpdateRequest | null): any;
