/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnCreateData } from './';
/**
 *
 * @export
 * @interface ReturnCreateError
 */
export interface ReturnCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReturnCreateError
     */
    success: ReturnCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReturnCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReturnCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ReturnCreateData}
     * @memberof ReturnCreateError
     */
    _return?: ReturnCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnCreateErrorSuccessEnum {
    False = "false"
}
export declare function ReturnCreateErrorFromJSON(json: any): ReturnCreateError;
export declare function ReturnCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnCreateError;
export declare function ReturnCreateErrorToJSON(value?: ReturnCreateError | null): any;
