/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventSource, EventType } from './';
/**
 *
 * @export
 * @interface InventoryRuleCreateRequest
 */
export interface InventoryRuleCreateRequest {
    /**
     *
     * @type {EventType}
     * @memberof InventoryRuleCreateRequest
     */
    event: EventType;
    /**
     *
     * @type {EventSource}
     * @memberof InventoryRuleCreateRequest
     */
    ptype?: EventSource;
    /**
     * Event source partner name
     * @type {string}
     * @memberof InventoryRuleCreateRequest
     */
    partner?: string;
    /**
     * Event source integration id
     * @type {string}
     * @memberof InventoryRuleCreateRequest
     */
    integration?: string;
    /**
     * Description of the rule
     * @type {string}
     * @memberof InventoryRuleCreateRequest
     */
    description?: string;
    /**
     * Rule's definition as javascript function
     * @type {string}
     * @memberof InventoryRuleCreateRequest
     */
    rule: string;
}
export declare function InventoryRuleCreateRequestFromJSON(json: any): InventoryRuleCreateRequest;
export declare function InventoryRuleCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleCreateRequest;
export declare function InventoryRuleCreateRequestToJSON(value?: InventoryRuleCreateRequest | null): any;
