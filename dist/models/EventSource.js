"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventSourceToJSON = exports.EventSourceFromJSONTyped = exports.EventSourceFromJSON = exports.EventSource = void 0;
/**
 * Event source partner type
 * @export
 * @enum {string}
 */
var EventSource;
(function (EventSource) {
    EventSource["Wms"] = "wms";
    EventSource["Ims"] = "ims";
    EventSource["Oms"] = "oms";
    EventSource["Portal"] = "portal";
})(EventSource = exports.EventSource || (exports.EventSource = {}));
function EventSourceFromJSON(json) {
    return EventSourceFromJSONTyped(json, false);
}
exports.EventSourceFromJSON = EventSourceFromJSON;
function EventSourceFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EventSourceFromJSONTyped = EventSourceFromJSONTyped;
function EventSourceToJSON(value) {
    return value;
}
exports.EventSourceToJSON = EventSourceToJSON;
