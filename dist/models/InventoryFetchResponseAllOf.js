"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryFetchResponseAllOfToJSON = exports.InventoryFetchResponseAllOfFromJSONTyped = exports.InventoryFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryFetchResponseAllOfFromJSON(json) {
    return InventoryFetchResponseAllOfFromJSONTyped(json, false);
}
exports.InventoryFetchResponseAllOfFromJSON = InventoryFetchResponseAllOfFromJSON;
function InventoryFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : _1.InventoryFromJSON(json['inventory']),
    };
}
exports.InventoryFetchResponseAllOfFromJSONTyped = InventoryFetchResponseAllOfFromJSONTyped;
function InventoryFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventory': _1.InventoryToJSON(value.inventory),
    };
}
exports.InventoryFetchResponseAllOfToJSON = InventoryFetchResponseAllOfToJSON;
