/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionsListFilter
 */
export interface ExceptionsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ExceptionsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ExceptionsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ExceptionsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ExceptionsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ExceptionsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ExceptionsListFilter
     */
    count?: number;
    /**
     * Exceptions by status-list
     * @type {Array<string>}
     * @memberof ExceptionsListFilter
     */
    status?: Array<string>;
    /**
     * Exceptions by exceptionId-list
     * @type {Array<string>}
     * @memberof ExceptionsListFilter
     */
    exceptionId?: Array<string>;
    /**
     * Exceptions by exceptionType-list
     * @type {Array<string>}
     * @memberof ExceptionsListFilter
     */
    exceptionType?: Array<string>;
    /**
     * Exceptions related to this list of entities
     * @type {Array<string>}
     * @memberof ExceptionsListFilter
     */
    entityId?: Array<string>;
    /**
     * Exceptions related to list of entity types
     * @type {Array<string>}
     * @memberof ExceptionsListFilter
     */
    entityType?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof ExceptionsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ExceptionsListFilter
     */
    keys?: string;
}
export declare function ExceptionsListFilterFromJSON(json: any): ExceptionsListFilter;
export declare function ExceptionsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsListFilter;
export declare function ExceptionsListFilterToJSON(value?: ExceptionsListFilter | null): any;
