/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntitySchemaUpdateRequest
 */
export interface EntitySchemaUpdateRequest {
    /**
     * Schema name
     * @type {string}
     * @memberof EntitySchemaUpdateRequest
     */
    name?: string;
    /**
     * Entity schema field descriptors
     * @type {Array<object>}
     * @memberof EntitySchemaUpdateRequest
     */
    fields?: Array<object>;
}
export declare function EntitySchemaUpdateRequestFromJSON(json: any): EntitySchemaUpdateRequest;
export declare function EntitySchemaUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaUpdateRequest;
export declare function EntitySchemaUpdateRequestToJSON(value?: EntitySchemaUpdateRequest | null): any;
