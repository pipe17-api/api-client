"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentCreateResultToJSON = exports.ShipmentCreateResultFromJSONTyped = exports.ShipmentCreateResultFromJSON = exports.ShipmentCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentCreateResultStatusEnum;
(function (ShipmentCreateResultStatusEnum) {
    ShipmentCreateResultStatusEnum["Submitted"] = "submitted";
    ShipmentCreateResultStatusEnum["Failed"] = "failed";
})(ShipmentCreateResultStatusEnum = exports.ShipmentCreateResultStatusEnum || (exports.ShipmentCreateResultStatusEnum = {}));
function ShipmentCreateResultFromJSON(json) {
    return ShipmentCreateResultFromJSONTyped(json, false);
}
exports.ShipmentCreateResultFromJSON = ShipmentCreateResultFromJSON;
function ShipmentCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'shipment': !runtime_1.exists(json, 'shipment') ? undefined : _1.ShipmentFromJSON(json['shipment']),
    };
}
exports.ShipmentCreateResultFromJSONTyped = ShipmentCreateResultFromJSONTyped;
function ShipmentCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'shipment': _1.ShipmentToJSON(value.shipment),
    };
}
exports.ShipmentCreateResultToJSON = ShipmentCreateResultToJSON;
