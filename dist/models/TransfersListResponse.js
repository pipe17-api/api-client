"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersListResponseToJSON = exports.TransfersListResponseFromJSONTyped = exports.TransfersListResponseFromJSON = exports.TransfersListResponseCodeEnum = exports.TransfersListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransfersListResponseSuccessEnum;
(function (TransfersListResponseSuccessEnum) {
    TransfersListResponseSuccessEnum["True"] = "true";
})(TransfersListResponseSuccessEnum = exports.TransfersListResponseSuccessEnum || (exports.TransfersListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TransfersListResponseCodeEnum;
(function (TransfersListResponseCodeEnum) {
    TransfersListResponseCodeEnum[TransfersListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TransfersListResponseCodeEnum[TransfersListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TransfersListResponseCodeEnum[TransfersListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TransfersListResponseCodeEnum = exports.TransfersListResponseCodeEnum || (exports.TransfersListResponseCodeEnum = {}));
function TransfersListResponseFromJSON(json) {
    return TransfersListResponseFromJSONTyped(json, false);
}
exports.TransfersListResponseFromJSON = TransfersListResponseFromJSON;
function TransfersListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TransfersListFilterFromJSON(json['filters']),
        'transfers': (json['transfers'].map(_1.TransferFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.TransfersListResponseFromJSONTyped = TransfersListResponseFromJSONTyped;
function TransfersListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.TransfersListFilterToJSON(value.filters),
        'transfers': (value.transfers.map(_1.TransferToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.TransfersListResponseToJSON = TransfersListResponseToJSON;
