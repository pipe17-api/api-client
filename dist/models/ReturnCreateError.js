"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnCreateErrorToJSON = exports.ReturnCreateErrorFromJSONTyped = exports.ReturnCreateErrorFromJSON = exports.ReturnCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReturnCreateErrorSuccessEnum;
(function (ReturnCreateErrorSuccessEnum) {
    ReturnCreateErrorSuccessEnum["False"] = "false";
})(ReturnCreateErrorSuccessEnum = exports.ReturnCreateErrorSuccessEnum || (exports.ReturnCreateErrorSuccessEnum = {}));
function ReturnCreateErrorFromJSON(json) {
    return ReturnCreateErrorFromJSONTyped(json, false);
}
exports.ReturnCreateErrorFromJSON = ReturnCreateErrorFromJSON;
function ReturnCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        '_return': !runtime_1.exists(json, 'return') ? undefined : _1.ReturnCreateDataFromJSON(json['return']),
    };
}
exports.ReturnCreateErrorFromJSONTyped = ReturnCreateErrorFromJSONTyped;
function ReturnCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'return': _1.ReturnCreateDataToJSON(value._return),
    };
}
exports.ReturnCreateErrorToJSON = ReturnCreateErrorToJSON;
