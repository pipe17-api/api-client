/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationUpdateData } from './';
/**
 *
 * @export
 * @interface OrganizationUpdateErrorAllOf
 */
export interface OrganizationUpdateErrorAllOf {
    /**
     *
     * @type {OrganizationUpdateData}
     * @memberof OrganizationUpdateErrorAllOf
     */
    organization?: OrganizationUpdateData;
}
export declare function OrganizationUpdateErrorAllOfFromJSON(json: any): OrganizationUpdateErrorAllOf;
export declare function OrganizationUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationUpdateErrorAllOf;
export declare function OrganizationUpdateErrorAllOfToJSON(value?: OrganizationUpdateErrorAllOf | null): any;
