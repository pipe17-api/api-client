"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingStatusToJSON = exports.TrackingStatusFromJSONTyped = exports.TrackingStatusFromJSON = exports.TrackingStatus = void 0;
/**
 *
 * @export
 * @enum {string}
 */
var TrackingStatus;
(function (TrackingStatus) {
    TrackingStatus["Created"] = "created";
    TrackingStatus["Shipped"] = "shipped";
    TrackingStatus["Delivered"] = "delivered";
})(TrackingStatus = exports.TrackingStatus || (exports.TrackingStatus = {}));
function TrackingStatusFromJSON(json) {
    return TrackingStatusFromJSONTyped(json, false);
}
exports.TrackingStatusFromJSON = TrackingStatusFromJSON;
function TrackingStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.TrackingStatusFromJSONTyped = TrackingStatusFromJSONTyped;
function TrackingStatusToJSON(value) {
    return value;
}
exports.TrackingStatusToJSON = TrackingStatusToJSON;
