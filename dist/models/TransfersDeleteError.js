"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersDeleteErrorToJSON = exports.TransfersDeleteErrorFromJSONTyped = exports.TransfersDeleteErrorFromJSON = exports.TransfersDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransfersDeleteErrorSuccessEnum;
(function (TransfersDeleteErrorSuccessEnum) {
    TransfersDeleteErrorSuccessEnum["False"] = "false";
})(TransfersDeleteErrorSuccessEnum = exports.TransfersDeleteErrorSuccessEnum || (exports.TransfersDeleteErrorSuccessEnum = {}));
function TransfersDeleteErrorFromJSON(json) {
    return TransfersDeleteErrorFromJSONTyped(json, false);
}
exports.TransfersDeleteErrorFromJSON = TransfersDeleteErrorFromJSON;
function TransfersDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TransfersDeleteFilterFromJSON(json['filters']),
    };
}
exports.TransfersDeleteErrorFromJSONTyped = TransfersDeleteErrorFromJSONTyped;
function TransfersDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.TransfersDeleteFilterToJSON(value.filters),
    };
}
exports.TransfersDeleteErrorToJSON = TransfersDeleteErrorToJSON;
