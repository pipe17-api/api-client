/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReceiptItem
 */
export interface ReceiptItem {
    /**
     * Item SKU
     * @type {string}
     * @memberof ReceiptItem
     */
    sku: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof ReceiptItem
     */
    quantity: number;
    /**
     * Item unique Id (to be matched with arrival line item)
     * @type {string}
     * @memberof ReceiptItem
     */
    uniqueId?: string;
}
export declare function ReceiptItemFromJSON(json: any): ReceiptItem;
export declare function ReceiptItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptItem;
export declare function ReceiptItemToJSON(value?: ReceiptItem | null): any;
