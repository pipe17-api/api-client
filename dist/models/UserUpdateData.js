"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateDataToJSON = exports.UserUpdateDataFromJSONTyped = exports.UserUpdateDataFromJSON = exports.UserUpdateDataStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UserUpdateDataStatusEnum;
(function (UserUpdateDataStatusEnum) {
    UserUpdateDataStatusEnum["Active"] = "active";
    UserUpdateDataStatusEnum["Disabled"] = "disabled";
})(UserUpdateDataStatusEnum = exports.UserUpdateDataStatusEnum || (exports.UserUpdateDataStatusEnum = {}));
function UserUpdateDataFromJSON(json) {
    return UserUpdateDataFromJSONTyped(json, false);
}
exports.UserUpdateDataFromJSON = UserUpdateDataFromJSON;
function UserUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'roles': !runtime_1.exists(json, 'roles') ? undefined : json['roles'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'timeZone': !runtime_1.exists(json, 'timeZone') ? undefined : json['timeZone'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'address': !runtime_1.exists(json, 'address') ? undefined : _1.UserAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'password': !runtime_1.exists(json, 'password') ? undefined : json['password'],
    };
}
exports.UserUpdateDataFromJSONTyped = UserUpdateDataFromJSONTyped;
function UserUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'roles': value.roles,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'description': value.description,
        'address': _1.UserAddressToJSON(value.address),
        'status': value.status,
        'password': value.password,
    };
}
exports.UserUpdateDataToJSON = UserUpdateDataToJSON;
