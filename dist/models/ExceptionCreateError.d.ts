/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionCreateData } from './';
/**
 *
 * @export
 * @interface ExceptionCreateError
 */
export interface ExceptionCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionCreateError
     */
    success: ExceptionCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ExceptionCreateData}
     * @memberof ExceptionCreateError
     */
    exception?: ExceptionCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionCreateErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionCreateErrorFromJSON(json: any): ExceptionCreateError;
export declare function ExceptionCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionCreateError;
export declare function ExceptionCreateErrorToJSON(value?: ExceptionCreateError | null): any;
