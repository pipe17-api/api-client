/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface UserAddress
 */
export interface UserAddress {
    /**
     * First Name
     * @type {string}
     * @memberof UserAddress
     */
    firstName?: string;
    /**
     * Last Name
     * @type {string}
     * @memberof UserAddress
     */
    lastName?: string;
    /**
     * Company
     * @type {string}
     * @memberof UserAddress
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof UserAddress
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof UserAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof UserAddress
     */
    city?: string;
    /**
     * State or Province
     * @type {string}
     * @memberof UserAddress
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof UserAddress
     */
    zipCodeOrPostalCode?: string;
    /**
     * Country
     * @type {string}
     * @memberof UserAddress
     */
    country?: string;
}
export declare function UserAddressFromJSON(json: any): UserAddress;
export declare function UserAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserAddress;
export declare function UserAddressToJSON(value?: UserAddress | null): any;
