"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventRequestSourcePublicToJSON = exports.EventRequestSourcePublicFromJSONTyped = exports.EventRequestSourcePublicFromJSON = exports.EventRequestSourcePublic = void 0;
/**
 * Event Type
 * @export
 * @enum {string}
 */
var EventRequestSourcePublic;
(function (EventRequestSourcePublic) {
    EventRequestSourcePublic["Custom"] = "custom";
})(EventRequestSourcePublic = exports.EventRequestSourcePublic || (exports.EventRequestSourcePublic = {}));
function EventRequestSourcePublicFromJSON(json) {
    return EventRequestSourcePublicFromJSONTyped(json, false);
}
exports.EventRequestSourcePublicFromJSON = EventRequestSourcePublicFromJSON;
function EventRequestSourcePublicFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EventRequestSourcePublicFromJSONTyped = EventRequestSourcePublicFromJSONTyped;
function EventRequestSourcePublicToJSON(value) {
    return value;
}
exports.EventRequestSourcePublicToJSON = EventRequestSourcePublicToJSON;
