"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterUpdateRequestToJSON = exports.EntityFilterUpdateRequestFromJSONTyped = exports.EntityFilterUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
function EntityFilterUpdateRequestFromJSON(json) {
    return EntityFilterUpdateRequestFromJSONTyped(json, false);
}
exports.EntityFilterUpdateRequestFromJSON = EntityFilterUpdateRequestFromJSON;
function EntityFilterUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'query': !runtime_1.exists(json, 'query') ? undefined : json['query'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
    };
}
exports.EntityFilterUpdateRequestFromJSONTyped = EntityFilterUpdateRequestFromJSONTyped;
function EntityFilterUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'query': value.query,
        'name': value.name,
        'isPublic': value.isPublic,
        'originId': value.originId,
    };
}
exports.EntityFilterUpdateRequestToJSON = EntityFilterUpdateRequestToJSON;
