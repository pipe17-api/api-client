"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptAllOfToJSON = exports.ReceiptAllOfFromJSONTyped = exports.ReceiptAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ReceiptAllOfFromJSON(json) {
    return ReceiptAllOfFromJSONTyped(json, false);
}
exports.ReceiptAllOfFromJSON = ReceiptAllOfFromJSON;
function ReceiptAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'receiptId': !runtime_1.exists(json, 'receiptId') ? undefined : json['receiptId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.ReceiptAllOfFromJSONTyped = ReceiptAllOfFromJSONTyped;
function ReceiptAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'receiptId': value.receiptId,
        'extOrderId': value.extOrderId,
        'integration': value.integration,
    };
}
exports.ReceiptAllOfToJSON = ReceiptAllOfToJSON;
