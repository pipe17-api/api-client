"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingFetchErrorToJSON = exports.RoutingFetchErrorFromJSONTyped = exports.RoutingFetchErrorFromJSON = exports.RoutingFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var RoutingFetchErrorSuccessEnum;
(function (RoutingFetchErrorSuccessEnum) {
    RoutingFetchErrorSuccessEnum["False"] = "false";
})(RoutingFetchErrorSuccessEnum = exports.RoutingFetchErrorSuccessEnum || (exports.RoutingFetchErrorSuccessEnum = {}));
function RoutingFetchErrorFromJSON(json) {
    return RoutingFetchErrorFromJSONTyped(json, false);
}
exports.RoutingFetchErrorFromJSON = RoutingFetchErrorFromJSON;
function RoutingFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.RoutingFetchErrorFromJSONTyped = RoutingFetchErrorFromJSONTyped;
function RoutingFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.RoutingFetchErrorToJSON = RoutingFetchErrorToJSON;
