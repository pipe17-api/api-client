/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Organization } from './';
/**
 *
 * @export
 * @interface OrganizationCreateResponse
 */
export interface OrganizationCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrganizationCreateResponse
     */
    success: OrganizationCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationCreateResponse
     */
    code: OrganizationCreateResponseCodeEnum;
    /**
     *
     * @type {Organization}
     * @memberof OrganizationCreateResponse
     */
    organization?: Organization;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrganizationCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrganizationCreateResponseFromJSON(json: any): OrganizationCreateResponse;
export declare function OrganizationCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationCreateResponse;
export declare function OrganizationCreateResponseToJSON(value?: OrganizationCreateResponse | null): any;
