/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingCreateData
 */
export interface OrderRoutingCreateData {
    /**
     * Routing rules definition
     * @type {Array<object>}
     * @memberof OrderRoutingCreateData
     */
    rules: Array<object>;
}
export declare function OrderRoutingCreateDataFromJSON(json: any): OrderRoutingCreateData;
export declare function OrderRoutingCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingCreateData;
export declare function OrderRoutingCreateDataToJSON(value?: OrderRoutingCreateData | null): any;
