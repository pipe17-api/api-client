"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundCreateResponseAllOfToJSON = exports.RefundCreateResponseAllOfFromJSONTyped = exports.RefundCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundCreateResponseAllOfFromJSON(json) {
    return RefundCreateResponseAllOfFromJSONTyped(json, false);
}
exports.RefundCreateResponseAllOfFromJSON = RefundCreateResponseAllOfFromJSON;
function RefundCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'refund': !runtime_1.exists(json, 'refund') ? undefined : _1.RefundFromJSON(json['refund']),
    };
}
exports.RefundCreateResponseAllOfFromJSONTyped = RefundCreateResponseAllOfFromJSONTyped;
function RefundCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'refund': _1.RefundToJSON(value.refund),
    };
}
exports.RefundCreateResponseAllOfToJSON = RefundCreateResponseAllOfToJSON;
