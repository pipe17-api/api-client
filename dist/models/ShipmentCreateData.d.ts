/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ShipmentCreateStatus, ShipmentLineItem, ShipmentOrderType } from './';
/**
 *
 * @export
 * @interface ShipmentCreateData
 */
export interface ShipmentCreateData {
    /**
     * Destination of this shipment request
     * @type {string}
     * @memberof ShipmentCreateData
     */
    fulfillmentIntegrationId?: string;
    /**
     * Customer friendly ID on the order object
     * @type {string}
     * @memberof ShipmentCreateData
     */
    extOrderId: string;
    /**
     * Customer Friendly Internal reference to ShipmentId that they use
     * @type {string}
     * @memberof ShipmentCreateData
     */
    extShipmentId?: string;
    /**
     * Pipe17 Internal Order's orderId
     * @type {string}
     * @memberof ShipmentCreateData
     */
    orderId?: string;
    /**
     * Actual Order source, Walmart, Etsy, etc
     * @type {string}
     * @memberof ShipmentCreateData
     */
    orderSource?: string;
    /**
     *
     * @type {ShipmentOrderType}
     * @memberof ShipmentCreateData
     */
    orderType?: ShipmentOrderType;
    /**
     * Create time of linked order
     * @type {Date}
     * @memberof ShipmentCreateData
     */
    orderCreateTime?: Date;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof ShipmentCreateData
     */
    shipByDate?: Date;
    /**
     * Expected ship date
     * @type {Date}
     * @memberof ShipmentCreateData
     */
    expectedShipDate?: Date;
    /**
     * First Priority to be respected
     * @type {Date}
     * @memberof ShipmentCreateData
     */
    expectedDeliveryDate?: Date;
    /**
     *
     * @type {Address}
     * @memberof ShipmentCreateData
     */
    shippingAddress: Address;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority
     * @type {string}
     * @memberof ShipmentCreateData
     */
    shippingCarrier?: string;
    /**
     * 3rd Priority
     * @type {string}
     * @memberof ShipmentCreateData
     */
    shippingCode?: string;
    /**
     * Required with ShippingCarrier, 2nd Priority
     * @type {string}
     * @memberof ShipmentCreateData
     */
    shippingClass?: string;
    /**
     * Shipping Note
     * @type {string}
     * @memberof ShipmentCreateData
     */
    shippingNote?: string;
    /**
     * Shipping Labels
     * @type {Array<string>}
     * @memberof ShipmentCreateData
     */
    shippingLabels?: Array<string>;
    /**
     * Shipping Gift note
     * @type {string}
     * @memberof ShipmentCreateData
     */
    giftNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof ShipmentCreateData
     */
    incoterms?: string;
    /**
     * Id of location defined in organization
     * @type {string}
     * @memberof ShipmentCreateData
     */
    locationId?: string;
    /**
     *
     * @type {ShipmentCreateStatus}
     * @memberof ShipmentCreateData
     */
    status?: ShipmentCreateStatus;
    /**
     *
     * @type {Array<ShipmentLineItem>}
     * @memberof ShipmentCreateData
     */
    lineItems?: Array<ShipmentLineItem>;
}
export declare function ShipmentCreateDataFromJSON(json: any): ShipmentCreateData;
export declare function ShipmentCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentCreateData;
export declare function ShipmentCreateDataToJSON(value?: ShipmentCreateData | null): any;
