/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressUpdateData, NameValue, OrderCustomer, OrderStatus, OrderUpdateLineItem, Payment } from './';
/**
 *
 * @export
 * @interface OrderUpdateData
 */
export interface OrderUpdateData {
    /**
     *
     * @type {OrderStatus}
     * @memberof OrderUpdateData
     */
    status?: OrderStatus;
    /**
     * The original order platform, walmart, etsy, etc
     * @type {string}
     * @memberof OrderUpdateData
     */
    orderSource?: string | null;
    /**
     * Order Url in External System
     * @type {string}
     * @memberof OrderUpdateData
     */
    extOrderUrl?: string;
    /**
     *
     * @type {Array<OrderUpdateLineItem>}
     * @memberof OrderUpdateData
     */
    lineItems?: Array<OrderUpdateLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof OrderUpdateData
     */
    subTotalPrice?: number | null;
    /**
     * Order Discount
     * @type {number}
     * @memberof OrderUpdateData
     */
    orderDiscount?: number | null;
    /**
     * Order Tax
     * @type {number}
     * @memberof OrderUpdateData
     */
    orderTax?: number | null;
    /**
     * Total Price
     * @type {number}
     * @memberof OrderUpdateData
     */
    totalPrice?: number | null;
    /**
     * Order Notes
     * @type {string}
     * @memberof OrderUpdateData
     */
    orderNote?: string | null;
    /**
     * Order Gift note
     * @type {string}
     * @memberof OrderUpdateData
     */
    giftNote?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof OrderUpdateData
     */
    shippingAddress?: AddressUpdateData;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority
     * @type {string}
     * @memberof OrderUpdateData
     */
    shippingCarrier?: string | null;
    /**
     * Required with ShippingCarrier, 2nd Priority
     * @type {string}
     * @memberof OrderUpdateData
     */
    shippingClass?: string | null;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof OrderUpdateData
     */
    shipByDate?: Date | null;
    /**
     * Ship After Date
     * @type {Date}
     * @memberof OrderUpdateData
     */
    shipAfterDate?: Date | null;
    /**
     * First Priority to be respected
     * @type {Date}
     * @memberof OrderUpdateData
     */
    expectedDeliveryDate?: Date | null;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof OrderUpdateData
     */
    shippingNote?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof OrderUpdateData
     */
    billingAddress?: AddressUpdateData;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof OrderUpdateData
     */
    timestamp?: Date | null;
    /**
     * status of the payment
     * @type {string}
     * @memberof OrderUpdateData
     */
    paymentStatus?: string | null;
    /**
     * status of the fulfillment
     * @type {string}
     * @memberof OrderUpdateData
     */
    fulfillmentStatus?: string | null;
    /**
     * External Order ID
     * @type {string}
     * @memberof OrderUpdateData
     */
    extOrderId?: string;
    /**
     * External System Order API ID
     * @type {string}
     * @memberof OrderUpdateData
     */
    extOrderApiId?: string | null;
    /**
     * When the order was created at source
     * @type {Date}
     * @memberof OrderUpdateData
     */
    extOrderCreatedAt?: Date | null;
    /**
     * When the order was updated at source
     * @type {Date}
     * @memberof OrderUpdateData
     */
    extOrderUpdatedAt?: Date | null;
    /**
     * Id of location defined in organization. Used as item origin for shipment.
     * @type {string}
     * @memberof OrderUpdateData
     */
    locationId?: string | null;
    /**
     *
     * @type {number}
     * @memberof OrderUpdateData
     */
    shippingPrice?: number | null;
    /**
     * Indicates if fetching of shipping labels is required
     * @type {boolean}
     * @memberof OrderUpdateData
     */
    requireShippingLabels?: boolean | null;
    /**
     *
     * @type {OrderCustomer}
     * @memberof OrderUpdateData
     */
    customer?: OrderCustomer;
    /**
     * 3rd Priority
     * @type {string}
     * @memberof OrderUpdateData
     */
    shippingCode?: string | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof OrderUpdateData
     */
    customFields?: Array<NameValue>;
    /**
     * Order tags
     * @type {Array<string>}
     * @memberof OrderUpdateData
     */
    tags?: Array<string>;
    /**
     * Payments
     * @type {Array<Payment>}
     * @memberof OrderUpdateData
     */
    payments?: Array<Payment>;
}
export declare function OrderUpdateDataFromJSON(json: any): OrderUpdateData;
export declare function OrderUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderUpdateData;
export declare function OrderUpdateDataToJSON(value?: OrderUpdateData | null): any;
