/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingsListFilter } from './';
/**
 *
 * @export
 * @interface RoutingsListErrorAllOf
 */
export interface RoutingsListErrorAllOf {
    /**
     *
     * @type {RoutingsListFilter}
     * @memberof RoutingsListErrorAllOf
     */
    filters?: RoutingsListFilter;
}
export declare function RoutingsListErrorAllOfFromJSON(json: any): RoutingsListErrorAllOf;
export declare function RoutingsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsListErrorAllOf;
export declare function RoutingsListErrorAllOfToJSON(value?: RoutingsListErrorAllOf | null): any;
