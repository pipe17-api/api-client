/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 * Accounts Filter
 * @export
 * @interface AccountsFilterAllOf
 */
export interface AccountsFilterAllOf {
    /**
     * Accounts by list of orgKey
     * @type {Array<string>}
     * @memberof AccountsFilterAllOf
     */
    orgKey?: Array<string>;
    /**
     * Accounts by list of status
     * @type {Array<AccountStatus>}
     * @memberof AccountsFilterAllOf
     */
    status?: Array<AccountStatus>;
}
export declare function AccountsFilterAllOfFromJSON(json: any): AccountsFilterAllOf;
export declare function AccountsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsFilterAllOf;
export declare function AccountsFilterAllOfToJSON(value?: AccountsFilterAllOf | null): any;
