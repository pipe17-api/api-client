"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingCreateErrorAllOfToJSON = exports.RoutingCreateErrorAllOfFromJSONTyped = exports.RoutingCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoutingCreateErrorAllOfFromJSON(json) {
    return RoutingCreateErrorAllOfFromJSONTyped(json, false);
}
exports.RoutingCreateErrorAllOfFromJSON = RoutingCreateErrorAllOfFromJSON;
function RoutingCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.RoutingCreateDataFromJSON(json['routing']),
    };
}
exports.RoutingCreateErrorAllOfFromJSONTyped = RoutingCreateErrorAllOfFromJSONTyped;
function RoutingCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routing': _1.RoutingCreateDataToJSON(value.routing),
    };
}
exports.RoutingCreateErrorAllOfToJSON = RoutingCreateErrorAllOfToJSON;
