"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryDeleteFilterToJSON = exports.InventoryDeleteFilterFromJSONTyped = exports.InventoryDeleteFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryDeleteFilterFromJSON(json) {
    return InventoryDeleteFilterFromJSONTyped(json, false);
}
exports.InventoryDeleteFilterFromJSON = InventoryDeleteFilterFromJSON;
function InventoryDeleteFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'inventoryId': !runtime_1.exists(json, 'inventoryId') ? undefined : json['inventoryId'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.InventoryDeleteFilterFromJSONTyped = InventoryDeleteFilterFromJSONTyped;
function InventoryDeleteFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'inventoryId': value.inventoryId,
        'entityId': value.entityId,
        'sku': value.sku,
        'locationId': value.locationId,
        'deleted': value.deleted,
    };
}
exports.InventoryDeleteFilterToJSON = InventoryDeleteFilterToJSON;
