/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserAddress } from './';
/**
 *
 * @export
 * @interface UserUpdateRequest
 */
export interface UserUpdateRequest {
    /**
     * User Name
     * @type {string}
     * @memberof UserUpdateRequest
     */
    name?: string;
    /**
     * User named Role(s)
     * @type {Array<string>}
     * @memberof UserUpdateRequest
     */
    roles?: Array<string>;
    /**
     * User Phone
     * @type {string}
     * @memberof UserUpdateRequest
     */
    phone?: string;
    /**
     * User Time Zone
     * @type {string}
     * @memberof UserUpdateRequest
     */
    timeZone?: string;
    /**
     * User Description
     * @type {string}
     * @memberof UserUpdateRequest
     */
    description?: string;
    /**
     *
     * @type {UserAddress}
     * @memberof UserUpdateRequest
     */
    address?: UserAddress;
    /**
     *
     * @type {string}
     * @memberof UserUpdateRequest
     */
    status?: UserUpdateRequestStatusEnum;
    /**
     * User password
     * @type {string}
     * @memberof UserUpdateRequest
     */
    password?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum UserUpdateRequestStatusEnum {
    Active = "active",
    Disabled = "disabled"
}
export declare function UserUpdateRequestFromJSON(json: any): UserUpdateRequest;
export declare function UserUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserUpdateRequest;
export declare function UserUpdateRequestToJSON(value?: UserUpdateRequest | null): any;
