"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingCreateRequestToJSON = exports.TrackingCreateRequestFromJSONTyped = exports.TrackingCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingCreateRequestFromJSON(json) {
    return TrackingCreateRequestFromJSONTyped(json, false);
}
exports.TrackingCreateRequestFromJSON = TrackingCreateRequestFromJSON;
function TrackingCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'trackingNumber': json['trackingNumber'],
        'fulfillmentId': json['fulfillmentId'],
        'shippingCarrier': _1.ShippingCarrierFromJSON(json['shippingCarrier']),
        'url': !runtime_1.exists(json, 'url') ? undefined : json['url'],
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
    };
}
exports.TrackingCreateRequestFromJSONTyped = TrackingCreateRequestFromJSONTyped;
function TrackingCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'trackingNumber': value.trackingNumber,
        'fulfillmentId': value.fulfillmentId,
        'shippingCarrier': _1.ShippingCarrierToJSON(value.shippingCarrier),
        'url': value.url,
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
    };
}
exports.TrackingCreateRequestToJSON = TrackingCreateRequestToJSON;
