/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRouting, OrderRoutingsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrderRoutingListResponseAllOf
 */
export interface OrderRoutingListResponseAllOf {
    /**
     *
     * @type {OrderRoutingsListFilter}
     * @memberof OrderRoutingListResponseAllOf
     */
    filters?: OrderRoutingsListFilter;
    /**
     *
     * @type {Array<OrderRouting>}
     * @memberof OrderRoutingListResponseAllOf
     */
    routings?: Array<OrderRouting>;
    /**
     *
     * @type {Pagination}
     * @memberof OrderRoutingListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function OrderRoutingListResponseAllOfFromJSON(json: any): OrderRoutingListResponseAllOf;
export declare function OrderRoutingListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingListResponseAllOf;
export declare function OrderRoutingListResponseAllOfToJSON(value?: OrderRoutingListResponseAllOf | null): any;
