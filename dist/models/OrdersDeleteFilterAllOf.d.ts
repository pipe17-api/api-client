/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Orders Filter
 * @export
 * @interface OrdersDeleteFilterAllOf
 */
export interface OrdersDeleteFilterAllOf {
    /**
     * Orders of these integrations
     * @type {Array<string>}
     * @memberof OrdersDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function OrdersDeleteFilterAllOfFromJSON(json: any): OrdersDeleteFilterAllOf;
export declare function OrdersDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersDeleteFilterAllOf;
export declare function OrdersDeleteFilterAllOfToJSON(value?: OrdersDeleteFilterAllOf | null): any;
