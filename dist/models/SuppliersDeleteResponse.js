"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuppliersDeleteResponseToJSON = exports.SuppliersDeleteResponseFromJSONTyped = exports.SuppliersDeleteResponseFromJSON = exports.SuppliersDeleteResponseCodeEnum = exports.SuppliersDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var SuppliersDeleteResponseSuccessEnum;
(function (SuppliersDeleteResponseSuccessEnum) {
    SuppliersDeleteResponseSuccessEnum["True"] = "true";
})(SuppliersDeleteResponseSuccessEnum = exports.SuppliersDeleteResponseSuccessEnum || (exports.SuppliersDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var SuppliersDeleteResponseCodeEnum;
(function (SuppliersDeleteResponseCodeEnum) {
    SuppliersDeleteResponseCodeEnum[SuppliersDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    SuppliersDeleteResponseCodeEnum[SuppliersDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    SuppliersDeleteResponseCodeEnum[SuppliersDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(SuppliersDeleteResponseCodeEnum = exports.SuppliersDeleteResponseCodeEnum || (exports.SuppliersDeleteResponseCodeEnum = {}));
function SuppliersDeleteResponseFromJSON(json) {
    return SuppliersDeleteResponseFromJSONTyped(json, false);
}
exports.SuppliersDeleteResponseFromJSON = SuppliersDeleteResponseFromJSON;
function SuppliersDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.SuppliersDeleteFilterFromJSON(json['filters']),
        'suppliers': !runtime_1.exists(json, 'suppliers') ? undefined : (json['suppliers'].map(_1.SupplierFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.SuppliersDeleteResponseFromJSONTyped = SuppliersDeleteResponseFromJSONTyped;
function SuppliersDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.SuppliersDeleteFilterToJSON(value.filters),
        'suppliers': value.suppliers === undefined ? undefined : (value.suppliers.map(_1.SupplierToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.SuppliersDeleteResponseToJSON = SuppliersDeleteResponseToJSON;
