"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalUpdateErrorAllOfToJSON = exports.ArrivalUpdateErrorAllOfFromJSONTyped = exports.ArrivalUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalUpdateErrorAllOfFromJSON(json) {
    return ArrivalUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ArrivalUpdateErrorAllOfFromJSON = ArrivalUpdateErrorAllOfFromJSON;
function ArrivalUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrival': !runtime_1.exists(json, 'arrival') ? undefined : _1.ArrivalUpdateDataFromJSON(json['arrival']),
    };
}
exports.ArrivalUpdateErrorAllOfFromJSONTyped = ArrivalUpdateErrorAllOfFromJSONTyped;
function ArrivalUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrival': _1.ArrivalUpdateDataToJSON(value.arrival),
    };
}
exports.ArrivalUpdateErrorAllOfToJSON = ArrivalUpdateErrorAllOfToJSON;
