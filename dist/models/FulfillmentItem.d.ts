/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentItem
 */
export interface FulfillmentItem {
    /**
     * Item SKU
     * @type {string}
     * @memberof FulfillmentItem
     */
    sku: string;
    /**
     * Item quantity
     * @type {number}
     * @memberof FulfillmentItem
     */
    quantity: number;
    /**
     * Item unique Id (to be matched with shipment line item)
     * @type {string}
     * @memberof FulfillmentItem
     */
    uniqueId?: string;
}
export declare function FulfillmentItemFromJSON(json: any): FulfillmentItem;
export declare function FulfillmentItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentItem;
export declare function FulfillmentItemToJSON(value?: FulfillmentItem | null): any;
