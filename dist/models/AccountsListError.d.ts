/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountsListFilter } from './';
/**
 *
 * @export
 * @interface AccountsListError
 */
export interface AccountsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof AccountsListError
     */
    success: AccountsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof AccountsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof AccountsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {AccountsListFilter}
     * @memberof AccountsListError
     */
    filters?: AccountsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountsListErrorSuccessEnum {
    False = "false"
}
export declare function AccountsListErrorFromJSON(json: any): AccountsListError;
export declare function AccountsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsListError;
export declare function AccountsListErrorToJSON(value?: AccountsListError | null): any;
