/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventUpdateError
 */
export interface EventUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EventUpdateError
     */
    success: EventUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EventUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EventUpdateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EventUpdateErrorSuccessEnum {
    False = "false"
}
export declare function EventUpdateErrorFromJSON(json: any): EventUpdateError;
export declare function EventUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventUpdateError;
export declare function EventUpdateErrorToJSON(value?: EventUpdateError | null): any;
