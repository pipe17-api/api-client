"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationUpdateAddressToJSON = exports.LocationUpdateAddressFromJSONTyped = exports.LocationUpdateAddressFromJSON = void 0;
const runtime_1 = require("../runtime");
function LocationUpdateAddressFromJSON(json) {
    return LocationUpdateAddressFromJSONTyped(json, false);
}
exports.LocationUpdateAddressFromJSON = LocationUpdateAddressFromJSON;
function LocationUpdateAddressFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'company': !runtime_1.exists(json, 'company') ? undefined : json['company'],
        'address1': !runtime_1.exists(json, 'address1') ? undefined : json['address1'],
        'address2': !runtime_1.exists(json, 'address2') ? undefined : json['address2'],
        'city': !runtime_1.exists(json, 'city') ? undefined : json['city'],
        'stateOrProvince': !runtime_1.exists(json, 'stateOrProvince') ? undefined : json['stateOrProvince'],
        'zipCodeOrPostalCode': !runtime_1.exists(json, 'zipCodeOrPostalCode') ? undefined : json['zipCodeOrPostalCode'],
        'country': !runtime_1.exists(json, 'country') ? undefined : json['country'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
    };
}
exports.LocationUpdateAddressFromJSONTyped = LocationUpdateAddressFromJSONTyped;
function LocationUpdateAddressToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'company': value.company,
        'address1': value.address1,
        'address2': value.address2,
        'city': value.city,
        'stateOrProvince': value.stateOrProvince,
        'zipCodeOrPostalCode': value.zipCodeOrPostalCode,
        'country': value.country,
        'email': value.email,
        'phone': value.phone,
    };
}
exports.LocationUpdateAddressToJSON = LocationUpdateAddressToJSON;
