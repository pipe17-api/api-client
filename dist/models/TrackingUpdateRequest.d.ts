/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TrackingUpdateRequest
 */
export interface TrackingUpdateRequest {
    /**
     *
     * @type {Date}
     * @memberof TrackingUpdateRequest
     */
    expectedArrivalDate?: Date;
    /**
     *
     * @type {string}
     * @memberof TrackingUpdateRequest
     */
    status: TrackingUpdateRequestStatusEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingUpdateRequestStatusEnum {
    Shipped = "shipped",
    Delivered = "delivered"
}
export declare function TrackingUpdateRequestFromJSON(json: any): TrackingUpdateRequest;
export declare function TrackingUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingUpdateRequest;
export declare function TrackingUpdateRequestToJSON(value?: TrackingUpdateRequest | null): any;
