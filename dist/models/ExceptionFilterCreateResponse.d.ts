/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilter } from './';
/**
 *
 * @export
 * @interface ExceptionFilterCreateResponse
 */
export interface ExceptionFilterCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionFilterCreateResponse
     */
    success: ExceptionFilterCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterCreateResponse
     */
    code: ExceptionFilterCreateResponseCodeEnum;
    /**
     *
     * @type {ExceptionFilter}
     * @memberof ExceptionFilterCreateResponse
     */
    exceptionFilter?: ExceptionFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionFilterCreateResponseFromJSON(json: any): ExceptionFilterCreateResponse;
export declare function ExceptionFilterCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterCreateResponse;
export declare function ExceptionFilterCreateResponseToJSON(value?: ExceptionFilterCreateResponse | null): any;
