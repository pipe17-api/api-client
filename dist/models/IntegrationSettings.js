"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationSettingsToJSON = exports.IntegrationSettingsFromJSONTyped = exports.IntegrationSettingsFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationSettingsFromJSON(json) {
    return IntegrationSettingsFromJSONTyped(json, false);
}
exports.IntegrationSettingsFromJSON = IntegrationSettingsFromJSON;
function IntegrationSettingsFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fields': !runtime_1.exists(json, 'fields') ? undefined : (json['fields'] === null ? null : json['fields'].map(_1.IntegrationSettingsFieldFromJSON)),
    };
}
exports.IntegrationSettingsFromJSONTyped = IntegrationSettingsFromJSONTyped;
function IntegrationSettingsToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fields': value.fields === undefined ? undefined : (value.fields === null ? null : value.fields.map(_1.IntegrationSettingsFieldToJSON)),
    };
}
exports.IntegrationSettingsToJSON = IntegrationSettingsToJSON;
