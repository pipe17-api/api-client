/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * User Status
 * @export
 * @enum {string}
 */
export declare enum UserStatus {
    Pending = "pending",
    Active = "active",
    Disabled = "disabled"
}
export declare function UserStatusFromJSON(json: any): UserStatus;
export declare function UserStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserStatus;
export declare function UserStatusToJSON(value?: UserStatus | null): any;
