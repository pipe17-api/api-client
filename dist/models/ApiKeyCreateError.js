"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyCreateErrorToJSON = exports.ApiKeyCreateErrorFromJSONTyped = exports.ApiKeyCreateErrorFromJSON = exports.ApiKeyCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ApiKeyCreateErrorSuccessEnum;
(function (ApiKeyCreateErrorSuccessEnum) {
    ApiKeyCreateErrorSuccessEnum["False"] = "false";
})(ApiKeyCreateErrorSuccessEnum = exports.ApiKeyCreateErrorSuccessEnum || (exports.ApiKeyCreateErrorSuccessEnum = {}));
function ApiKeyCreateErrorFromJSON(json) {
    return ApiKeyCreateErrorFromJSONTyped(json, false);
}
exports.ApiKeyCreateErrorFromJSON = ApiKeyCreateErrorFromJSON;
function ApiKeyCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.ApiKeyCreateErrorFromJSONTyped = ApiKeyCreateErrorFromJSONTyped;
function ApiKeyCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.ApiKeyCreateErrorToJSON = ApiKeyCreateErrorToJSON;
