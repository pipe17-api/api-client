"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsDeleteFilterAllOfToJSON = exports.ShipmentsDeleteFilterAllOfFromJSONTyped = exports.ShipmentsDeleteFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ShipmentsDeleteFilterAllOfFromJSON(json) {
    return ShipmentsDeleteFilterAllOfFromJSONTyped(json, false);
}
exports.ShipmentsDeleteFilterAllOfFromJSON = ShipmentsDeleteFilterAllOfFromJSON;
function ShipmentsDeleteFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.ShipmentsDeleteFilterAllOfFromJSONTyped = ShipmentsDeleteFilterAllOfFromJSONTyped;
function ShipmentsDeleteFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
    };
}
exports.ShipmentsDeleteFilterAllOfToJSON = ShipmentsDeleteFilterAllOfToJSON;
