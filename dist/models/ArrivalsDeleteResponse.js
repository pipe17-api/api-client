"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsDeleteResponseToJSON = exports.ArrivalsDeleteResponseFromJSONTyped = exports.ArrivalsDeleteResponseFromJSON = exports.ArrivalsDeleteResponseCodeEnum = exports.ArrivalsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalsDeleteResponseSuccessEnum;
(function (ArrivalsDeleteResponseSuccessEnum) {
    ArrivalsDeleteResponseSuccessEnum["True"] = "true";
})(ArrivalsDeleteResponseSuccessEnum = exports.ArrivalsDeleteResponseSuccessEnum || (exports.ArrivalsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ArrivalsDeleteResponseCodeEnum;
(function (ArrivalsDeleteResponseCodeEnum) {
    ArrivalsDeleteResponseCodeEnum[ArrivalsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ArrivalsDeleteResponseCodeEnum[ArrivalsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ArrivalsDeleteResponseCodeEnum[ArrivalsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ArrivalsDeleteResponseCodeEnum = exports.ArrivalsDeleteResponseCodeEnum || (exports.ArrivalsDeleteResponseCodeEnum = {}));
function ArrivalsDeleteResponseFromJSON(json) {
    return ArrivalsDeleteResponseFromJSONTyped(json, false);
}
exports.ArrivalsDeleteResponseFromJSON = ArrivalsDeleteResponseFromJSON;
function ArrivalsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ArrivalsDeleteFilterFromJSON(json['filters']),
        'arrivals': !runtime_1.exists(json, 'arrivals') ? undefined : (json['arrivals'].map(_1.ArrivalFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ArrivalsDeleteResponseFromJSONTyped = ArrivalsDeleteResponseFromJSONTyped;
function ArrivalsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ArrivalsDeleteFilterToJSON(value.filters),
        'arrivals': value.arrivals === undefined ? undefined : (value.arrivals.map(_1.ArrivalToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ArrivalsDeleteResponseToJSON = ArrivalsDeleteResponseToJSON;
