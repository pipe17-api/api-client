"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsDeleteErrorAllOfToJSON = exports.LocationsDeleteErrorAllOfFromJSONTyped = exports.LocationsDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationsDeleteErrorAllOfFromJSON(json) {
    return LocationsDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.LocationsDeleteErrorAllOfFromJSON = LocationsDeleteErrorAllOfFromJSON;
function LocationsDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsDeleteFilterFromJSON(json['filters']),
    };
}
exports.LocationsDeleteErrorAllOfFromJSONTyped = LocationsDeleteErrorAllOfFromJSONTyped;
function LocationsDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LocationsDeleteFilterToJSON(value.filters),
    };
}
exports.LocationsDeleteErrorAllOfToJSON = LocationsDeleteErrorAllOfToJSON;
