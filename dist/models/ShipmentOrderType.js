"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentOrderTypeToJSON = exports.ShipmentOrderTypeFromJSONTyped = exports.ShipmentOrderTypeFromJSON = exports.ShipmentOrderType = void 0;
/**
 * Type of order that initiated this shipment
 * @export
 * @enum {string}
 */
var ShipmentOrderType;
(function (ShipmentOrderType) {
    ShipmentOrderType["Sales"] = "sales";
    ShipmentOrderType["Transfer"] = "transfer";
    ShipmentOrderType["Purchase"] = "purchase";
})(ShipmentOrderType = exports.ShipmentOrderType || (exports.ShipmentOrderType = {}));
function ShipmentOrderTypeFromJSON(json) {
    return ShipmentOrderTypeFromJSONTyped(json, false);
}
exports.ShipmentOrderTypeFromJSON = ShipmentOrderTypeFromJSON;
function ShipmentOrderTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ShipmentOrderTypeFromJSONTyped = ShipmentOrderTypeFromJSONTyped;
function ShipmentOrderTypeToJSON(value) {
    return value;
}
exports.ShipmentOrderTypeToJSON = ShipmentOrderTypeToJSON;
