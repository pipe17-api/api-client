/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Event, EventsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface EventsListResponse
 */
export interface EventsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EventsListResponse
     */
    success: EventsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventsListResponse
     */
    code: EventsListResponseCodeEnum;
    /**
     *
     * @type {EventsListFilter}
     * @memberof EventsListResponse
     */
    filters?: EventsListFilter;
    /**
     *
     * @type {Array<Event>}
     * @memberof EventsListResponse
     */
    events?: Array<Event>;
    /**
     *
     * @type {Pagination}
     * @memberof EventsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum EventsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EventsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EventsListResponseFromJSON(json: any): EventsListResponse;
export declare function EventsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsListResponse;
export declare function EventsListResponseToJSON(value?: EventsListResponse | null): any;
