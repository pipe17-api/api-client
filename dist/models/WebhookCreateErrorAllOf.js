"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookCreateErrorAllOfToJSON = exports.WebhookCreateErrorAllOfFromJSONTyped = exports.WebhookCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function WebhookCreateErrorAllOfFromJSON(json) {
    return WebhookCreateErrorAllOfFromJSONTyped(json, false);
}
exports.WebhookCreateErrorAllOfFromJSON = WebhookCreateErrorAllOfFromJSON;
function WebhookCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.WebhookCreateDataFromJSON(json['webhook']),
    };
}
exports.WebhookCreateErrorAllOfFromJSONTyped = WebhookCreateErrorAllOfFromJSONTyped;
function WebhookCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'webhook': _1.WebhookCreateDataToJSON(value.webhook),
    };
}
exports.WebhookCreateErrorAllOfToJSON = WebhookCreateErrorAllOfToJSON;
