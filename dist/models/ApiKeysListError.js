"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeysListErrorToJSON = exports.ApiKeysListErrorFromJSONTyped = exports.ApiKeysListErrorFromJSON = exports.ApiKeysListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ApiKeysListErrorSuccessEnum;
(function (ApiKeysListErrorSuccessEnum) {
    ApiKeysListErrorSuccessEnum["False"] = "false";
})(ApiKeysListErrorSuccessEnum = exports.ApiKeysListErrorSuccessEnum || (exports.ApiKeysListErrorSuccessEnum = {}));
function ApiKeysListErrorFromJSON(json) {
    return ApiKeysListErrorFromJSONTyped(json, false);
}
exports.ApiKeysListErrorFromJSON = ApiKeysListErrorFromJSON;
function ApiKeysListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ApiKeysListFilterFromJSON(json['filters']),
    };
}
exports.ApiKeysListErrorFromJSONTyped = ApiKeysListErrorFromJSONTyped;
function ApiKeysListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.ApiKeysListFilterToJSON(value.filters),
    };
}
exports.ApiKeysListErrorToJSON = ApiKeysListErrorToJSON;
