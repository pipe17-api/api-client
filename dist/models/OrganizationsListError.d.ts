/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationsListFilter } from './';
/**
 *
 * @export
 * @interface OrganizationsListError
 */
export interface OrganizationsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrganizationsListError
     */
    success: OrganizationsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrganizationsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrganizationsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrganizationsListFilter}
     * @memberof OrganizationsListError
     */
    filters?: OrganizationsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationsListErrorSuccessEnum {
    False = "false"
}
export declare function OrganizationsListErrorFromJSON(json: any): OrganizationsListError;
export declare function OrganizationsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsListError;
export declare function OrganizationsListErrorToJSON(value?: OrganizationsListError | null): any;
