"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentUpdateDataLineItemsToJSON = exports.ShipmentUpdateDataLineItemsFromJSONTyped = exports.ShipmentUpdateDataLineItemsFromJSON = void 0;
const runtime_1 = require("../runtime");
function ShipmentUpdateDataLineItemsFromJSON(json) {
    return ShipmentUpdateDataLineItemsFromJSONTyped(json, false);
}
exports.ShipmentUpdateDataLineItemsFromJSON = ShipmentUpdateDataLineItemsFromJSON;
function ShipmentUpdateDataLineItemsFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shippedQuantity': !runtime_1.exists(json, 'shippedQuantity') ? undefined : json['shippedQuantity'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
    };
}
exports.ShipmentUpdateDataLineItemsFromJSONTyped = ShipmentUpdateDataLineItemsFromJSONTyped;
function ShipmentUpdateDataLineItemsToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shippedQuantity': value.shippedQuantity,
        'sku': value.sku,
    };
}
exports.ShipmentUpdateDataLineItemsToJSON = ShipmentUpdateDataLineItemsToJSON;
