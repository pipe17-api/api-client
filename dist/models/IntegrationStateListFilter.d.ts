/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationStateListFilter
 */
export interface IntegrationStateListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof IntegrationStateListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof IntegrationStateListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof IntegrationStateListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof IntegrationStateListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof IntegrationStateListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof IntegrationStateListFilter
     */
    count?: number;
    /**
     * List sort order
     * @type {string}
     * @memberof IntegrationStateListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof IntegrationStateListFilter
     */
    keys?: string;
}
export declare function IntegrationStateListFilterFromJSON(json: any): IntegrationStateListFilter;
export declare function IntegrationStateListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateListFilter;
export declare function IntegrationStateListFilterToJSON(value?: IntegrationStateListFilter | null): any;
