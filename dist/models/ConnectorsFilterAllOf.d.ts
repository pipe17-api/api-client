/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Connectors Filter
 * @export
 * @interface ConnectorsFilterAllOf
 */
export interface ConnectorsFilterAllOf {
    /**
     * Retrieve connectors by list of connectorId
     * @type {Array<string>}
     * @memberof ConnectorsFilterAllOf
     */
    connectorId?: Array<string>;
    /**
     * Retrieve connector by list of connector names
     * @type {Array<string>}
     * @memberof ConnectorsFilterAllOf
     */
    connectorName?: Array<string>;
    /**
     * Retrieve connector by list of connector types
     * @type {Array<string>}
     * @memberof ConnectorsFilterAllOf
     */
    connectorType?: Array<string>;
}
export declare function ConnectorsFilterAllOfFromJSON(json: any): ConnectorsFilterAllOf;
export declare function ConnectorsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsFilterAllOf;
export declare function ConnectorsFilterAllOfToJSON(value?: ConnectorsFilterAllOf | null): any;
