/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Mapping Rule
 * @export
 * @interface MappingRule
 */
export interface MappingRule {
    /**
     * Mapping Rule Name
     * @type {string}
     * @memberof MappingRule
     */
    name: string;
    /**
     * Is Mapping Rule Enabled
     * @type {boolean}
     * @memberof MappingRule
     */
    enabled: boolean;
    /**
     * Mapping Rule Source
     * @type {string}
     * @memberof MappingRule
     */
    source: string;
    /**
     * Mapping Rule Target
     * @type {string}
     * @memberof MappingRule
     */
    target: string;
    /**
     * Mapping Rule Target Value Type
     * @type {string}
     * @memberof MappingRule
     */
    type?: MappingRuleTypeEnum;
    /**
     * Is Mapping Rule Source Required
     * @type {string}
     * @memberof MappingRule
     */
    require?: string;
    /**
     * Mapping Rule Source Value if Source is Missing
     * @type {string}
     * @memberof MappingRule
     */
    missing?: string;
    /**
     * Mapping Rule Source Default Value if Source is Missing
     * @type {string}
     * @memberof MappingRule
     */
    default?: string;
    /**
     * Mapping rule lookup definition
     * @type {object}
     * @memberof MappingRule
     */
    lookup?: object;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingRuleTypeEnum {
    String = "string",
    Number = "number",
    Boolean = "boolean",
    Date = "date"
}
export declare function MappingRuleFromJSON(json: any): MappingRule;
export declare function MappingRuleFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingRule;
export declare function MappingRuleToJSON(value?: MappingRule | null): any;
