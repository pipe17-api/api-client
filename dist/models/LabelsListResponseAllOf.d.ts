/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label, LabelsListFilter } from './';
/**
 *
 * @export
 * @interface LabelsListResponseAllOf
 */
export interface LabelsListResponseAllOf {
    /**
     *
     * @type {LabelsListFilter}
     * @memberof LabelsListResponseAllOf
     */
    filters?: LabelsListFilter;
    /**
     *
     * @type {Array<Label>}
     * @memberof LabelsListResponseAllOf
     */
    labels?: Array<Label>;
}
export declare function LabelsListResponseAllOfFromJSON(json: any): LabelsListResponseAllOf;
export declare function LabelsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsListResponseAllOf;
export declare function LabelsListResponseAllOfToJSON(value?: LabelsListResponseAllOf | null): any;
