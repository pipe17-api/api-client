/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Environment ID
 * @export
 * @enum {string}
 */
export declare enum Env {
    Test = "test",
    Prod = "prod"
}
export declare function EnvFromJSON(json: any): Env;
export declare function EnvFromJSONTyped(json: any, ignoreDiscriminator: boolean): Env;
export declare function EnvToJSON(value?: Env | null): any;
