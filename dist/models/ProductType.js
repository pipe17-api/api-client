"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductTypeToJSON = exports.ProductTypeFromJSONTyped = exports.ProductTypeFromJSON = exports.ProductType = void 0;
/**
 * Product Type
 * @export
 * @enum {string}
 */
var ProductType;
(function (ProductType) {
    ProductType["Simple"] = "simple";
    ProductType["Bundle"] = "bundle";
    ProductType["BundleItem"] = "bundleItem";
    ProductType["Child"] = "child";
    ProductType["Parent"] = "parent";
})(ProductType = exports.ProductType || (exports.ProductType = {}));
function ProductTypeFromJSON(json) {
    return ProductTypeFromJSONTyped(json, false);
}
exports.ProductTypeFromJSON = ProductTypeFromJSON;
function ProductTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ProductTypeFromJSONTyped = ProductTypeFromJSONTyped;
function ProductTypeToJSON(value) {
    return value;
}
exports.ProductTypeToJSON = ProductTypeToJSON;
