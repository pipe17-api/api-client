"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierCreateResponseToJSON = exports.SupplierCreateResponseFromJSONTyped = exports.SupplierCreateResponseFromJSON = exports.SupplierCreateResponseCodeEnum = exports.SupplierCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var SupplierCreateResponseSuccessEnum;
(function (SupplierCreateResponseSuccessEnum) {
    SupplierCreateResponseSuccessEnum["True"] = "true";
})(SupplierCreateResponseSuccessEnum = exports.SupplierCreateResponseSuccessEnum || (exports.SupplierCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var SupplierCreateResponseCodeEnum;
(function (SupplierCreateResponseCodeEnum) {
    SupplierCreateResponseCodeEnum[SupplierCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    SupplierCreateResponseCodeEnum[SupplierCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    SupplierCreateResponseCodeEnum[SupplierCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(SupplierCreateResponseCodeEnum = exports.SupplierCreateResponseCodeEnum || (exports.SupplierCreateResponseCodeEnum = {}));
function SupplierCreateResponseFromJSON(json) {
    return SupplierCreateResponseFromJSONTyped(json, false);
}
exports.SupplierCreateResponseFromJSON = SupplierCreateResponseFromJSON;
function SupplierCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierFromJSON(json['supplier']),
    };
}
exports.SupplierCreateResponseFromJSONTyped = SupplierCreateResponseFromJSONTyped;
function SupplierCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'supplier': _1.SupplierToJSON(value.supplier),
    };
}
exports.SupplierCreateResponseToJSON = SupplierCreateResponseToJSON;
