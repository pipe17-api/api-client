"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateFetchResponseToJSON = exports.IntegrationStateFetchResponseFromJSONTyped = exports.IntegrationStateFetchResponseFromJSON = exports.IntegrationStateFetchResponseCodeEnum = exports.IntegrationStateFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationStateFetchResponseSuccessEnum;
(function (IntegrationStateFetchResponseSuccessEnum) {
    IntegrationStateFetchResponseSuccessEnum["True"] = "true";
})(IntegrationStateFetchResponseSuccessEnum = exports.IntegrationStateFetchResponseSuccessEnum || (exports.IntegrationStateFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationStateFetchResponseCodeEnum;
(function (IntegrationStateFetchResponseCodeEnum) {
    IntegrationStateFetchResponseCodeEnum[IntegrationStateFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationStateFetchResponseCodeEnum[IntegrationStateFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationStateFetchResponseCodeEnum[IntegrationStateFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationStateFetchResponseCodeEnum = exports.IntegrationStateFetchResponseCodeEnum || (exports.IntegrationStateFetchResponseCodeEnum = {}));
function IntegrationStateFetchResponseFromJSON(json) {
    return IntegrationStateFetchResponseFromJSONTyped(json, false);
}
exports.IntegrationStateFetchResponseFromJSON = IntegrationStateFetchResponseFromJSON;
function IntegrationStateFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'integrationState': !runtime_1.exists(json, 'integrationState') ? undefined : _1.IntegrationStateFromJSON(json['integrationState']),
    };
}
exports.IntegrationStateFetchResponseFromJSONTyped = IntegrationStateFetchResponseFromJSONTyped;
function IntegrationStateFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'integrationState': _1.IntegrationStateToJSON(value.integrationState),
    };
}
exports.IntegrationStateFetchResponseToJSON = IntegrationStateFetchResponseToJSON;
