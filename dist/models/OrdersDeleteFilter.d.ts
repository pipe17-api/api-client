/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderStatus } from './';
/**
 *
 * @export
 * @interface OrdersDeleteFilter
 */
export interface OrdersDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrdersDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrdersDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrdersDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrdersDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrdersDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrdersDeleteFilter
     */
    count?: number;
    /**
     * Orders by list of orderId
     * @type {Array<string>}
     * @memberof OrdersDeleteFilter
     */
    orderId?: Array<string>;
    /**
     * Orders by list of extOrderId
     * @type {Array<string>}
     * @memberof OrdersDeleteFilter
     */
    extOrderId?: Array<string>;
    /**
     * Orders by list of order sources
     * @type {Array<string>}
     * @memberof OrdersDeleteFilter
     */
    orderSource?: Array<string>;
    /**
     * Orders by list of statuses
     * @type {Array<OrderStatus>}
     * @memberof OrdersDeleteFilter
     */
    status?: Array<OrderStatus>;
    /**
     * Soft deleted orders
     * @type {boolean}
     * @memberof OrdersDeleteFilter
     */
    deleted?: boolean;
    /**
     * Orders of these integrations
     * @type {Array<string>}
     * @memberof OrdersDeleteFilter
     */
    integration?: Array<string>;
}
export declare function OrdersDeleteFilterFromJSON(json: any): OrdersDeleteFilter;
export declare function OrdersDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersDeleteFilter;
export declare function OrdersDeleteFilterToJSON(value?: OrdersDeleteFilter | null): any;
