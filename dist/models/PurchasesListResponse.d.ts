/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Purchase, PurchasesListFilter } from './';
/**
 *
 * @export
 * @interface PurchasesListResponse
 */
export interface PurchasesListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof PurchasesListResponse
     */
    success: PurchasesListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchasesListResponse
     */
    code: PurchasesListResponseCodeEnum;
    /**
     *
     * @type {PurchasesListFilter}
     * @memberof PurchasesListResponse
     */
    filters?: PurchasesListFilter;
    /**
     *
     * @type {Array<Purchase>}
     * @memberof PurchasesListResponse
     */
    purchases: Array<Purchase>;
    /**
     *
     * @type {Pagination}
     * @memberof PurchasesListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchasesListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum PurchasesListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function PurchasesListResponseFromJSON(json: any): PurchasesListResponse;
export declare function PurchasesListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesListResponse;
export declare function PurchasesListResponseToJSON(value?: PurchasesListResponse | null): any;
