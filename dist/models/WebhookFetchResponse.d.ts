/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Webhook } from './';
/**
 *
 * @export
 * @interface WebhookFetchResponse
 */
export interface WebhookFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof WebhookFetchResponse
     */
    success: WebhookFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhookFetchResponse
     */
    code: WebhookFetchResponseCodeEnum;
    /**
     *
     * @type {Webhook}
     * @memberof WebhookFetchResponse
     */
    webhook?: Webhook;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhookFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum WebhookFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function WebhookFetchResponseFromJSON(json: any): WebhookFetchResponse;
export declare function WebhookFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookFetchResponse;
export declare function WebhookFetchResponseToJSON(value?: WebhookFetchResponse | null): any;
