"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptCreateResultToJSON = exports.ReceiptCreateResultFromJSONTyped = exports.ReceiptCreateResultFromJSON = exports.ReceiptCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReceiptCreateResultStatusEnum;
(function (ReceiptCreateResultStatusEnum) {
    ReceiptCreateResultStatusEnum["Submitted"] = "submitted";
    ReceiptCreateResultStatusEnum["Failed"] = "failed";
})(ReceiptCreateResultStatusEnum = exports.ReceiptCreateResultStatusEnum || (exports.ReceiptCreateResultStatusEnum = {}));
function ReceiptCreateResultFromJSON(json) {
    return ReceiptCreateResultFromJSONTyped(json, false);
}
exports.ReceiptCreateResultFromJSON = ReceiptCreateResultFromJSON;
function ReceiptCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'receipt': !runtime_1.exists(json, 'receipt') ? undefined : _1.ReceiptFromJSON(json['receipt']),
    };
}
exports.ReceiptCreateResultFromJSONTyped = ReceiptCreateResultFromJSONTyped;
function ReceiptCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'receipt': _1.ReceiptToJSON(value.receipt),
    };
}
exports.ReceiptCreateResultToJSON = ReceiptCreateResultToJSON;
