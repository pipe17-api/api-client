/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnsFilter } from './';
/**
 *
 * @export
 * @interface ReturnsListErrorAllOf
 */
export interface ReturnsListErrorAllOf {
    /**
     *
     * @type {ReturnsFilter}
     * @memberof ReturnsListErrorAllOf
     */
    filters?: ReturnsFilter;
}
export declare function ReturnsListErrorAllOfFromJSON(json: any): ReturnsListErrorAllOf;
export declare function ReturnsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsListErrorAllOf;
export declare function ReturnsListErrorAllOfToJSON(value?: ReturnsListErrorAllOf | null): any;
