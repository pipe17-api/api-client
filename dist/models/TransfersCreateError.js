"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersCreateErrorToJSON = exports.TransfersCreateErrorFromJSONTyped = exports.TransfersCreateErrorFromJSON = exports.TransfersCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransfersCreateErrorSuccessEnum;
(function (TransfersCreateErrorSuccessEnum) {
    TransfersCreateErrorSuccessEnum["False"] = "false";
})(TransfersCreateErrorSuccessEnum = exports.TransfersCreateErrorSuccessEnum || (exports.TransfersCreateErrorSuccessEnum = {}));
function TransfersCreateErrorFromJSON(json) {
    return TransfersCreateErrorFromJSONTyped(json, false);
}
exports.TransfersCreateErrorFromJSON = TransfersCreateErrorFromJSON;
function TransfersCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'transfers': !runtime_1.exists(json, 'transfers') ? undefined : (json['transfers'].map(_1.TransferCreateResultFromJSON)),
    };
}
exports.TransfersCreateErrorFromJSONTyped = TransfersCreateErrorFromJSONTyped;
function TransfersCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'transfers': value.transfers === undefined ? undefined : (value.transfers.map(_1.TransferCreateResultToJSON)),
    };
}
exports.TransfersCreateErrorToJSON = TransfersCreateErrorToJSON;
