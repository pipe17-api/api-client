"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListDeleteActionToJSON = exports.ListDeleteActionFromJSONTyped = exports.ListDeleteActionFromJSON = exports.ListDeleteAction = void 0;
/**
 * Action for list delete operation
 * @export
 * @enum {string}
 */
var ListDeleteAction;
(function (ListDeleteAction) {
    ListDeleteAction["List"] = "list";
    ListDeleteAction["Soft"] = "soft";
    ListDeleteAction["Hard"] = "hard";
})(ListDeleteAction = exports.ListDeleteAction || (exports.ListDeleteAction = {}));
function ListDeleteActionFromJSON(json) {
    return ListDeleteActionFromJSONTyped(json, false);
}
exports.ListDeleteActionFromJSON = ListDeleteActionFromJSON;
function ListDeleteActionFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ListDeleteActionFromJSONTyped = ListDeleteActionFromJSONTyped;
function ListDeleteActionToJSON(value) {
    return value;
}
exports.ListDeleteActionToJSON = ListDeleteActionToJSON;
