/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Return } from './';
/**
 *
 * @export
 * @interface ReturnFetchResponse
 */
export interface ReturnFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReturnFetchResponse
     */
    success: ReturnFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnFetchResponse
     */
    code: ReturnFetchResponseCodeEnum;
    /**
     *
     * @type {Return}
     * @memberof ReturnFetchResponse
     */
    _return?: Return;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReturnFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReturnFetchResponseFromJSON(json: any): ReturnFetchResponse;
export declare function ReturnFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnFetchResponse;
export declare function ReturnFetchResponseToJSON(value?: ReturnFetchResponse | null): any;
