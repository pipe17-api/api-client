"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsListErrorToJSON = exports.EventsListErrorFromJSONTyped = exports.EventsListErrorFromJSON = exports.EventsListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EventsListErrorSuccessEnum;
(function (EventsListErrorSuccessEnum) {
    EventsListErrorSuccessEnum["False"] = "false";
})(EventsListErrorSuccessEnum = exports.EventsListErrorSuccessEnum || (exports.EventsListErrorSuccessEnum = {}));
function EventsListErrorFromJSON(json) {
    return EventsListErrorFromJSONTyped(json, false);
}
exports.EventsListErrorFromJSON = EventsListErrorFromJSON;
function EventsListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EventsListFilterFromJSON(json['filters']),
    };
}
exports.EventsListErrorFromJSONTyped = EventsListErrorFromJSONTyped;
function EventsListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.EventsListFilterToJSON(value.filters),
    };
}
exports.EventsListErrorToJSON = EventsListErrorToJSON;
