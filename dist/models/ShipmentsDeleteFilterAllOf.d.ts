/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Shipments Filter
 * @export
 * @interface ShipmentsDeleteFilterAllOf
 */
export interface ShipmentsDeleteFilterAllOf {
    /**
     * Shipments of these integrations
     * @type {Array<string>}
     * @memberof ShipmentsDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function ShipmentsDeleteFilterAllOfFromJSON(json: any): ShipmentsDeleteFilterAllOf;
export declare function ShipmentsDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsDeleteFilterAllOf;
export declare function ShipmentsDeleteFilterAllOfToJSON(value?: ShipmentsDeleteFilterAllOf | null): any;
