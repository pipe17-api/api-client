"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateCreateResponseToJSON = exports.IntegrationStateCreateResponseFromJSONTyped = exports.IntegrationStateCreateResponseFromJSON = exports.IntegrationStateCreateResponseCodeEnum = exports.IntegrationStateCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationStateCreateResponseSuccessEnum;
(function (IntegrationStateCreateResponseSuccessEnum) {
    IntegrationStateCreateResponseSuccessEnum["True"] = "true";
})(IntegrationStateCreateResponseSuccessEnum = exports.IntegrationStateCreateResponseSuccessEnum || (exports.IntegrationStateCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationStateCreateResponseCodeEnum;
(function (IntegrationStateCreateResponseCodeEnum) {
    IntegrationStateCreateResponseCodeEnum[IntegrationStateCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationStateCreateResponseCodeEnum[IntegrationStateCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationStateCreateResponseCodeEnum[IntegrationStateCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationStateCreateResponseCodeEnum = exports.IntegrationStateCreateResponseCodeEnum || (exports.IntegrationStateCreateResponseCodeEnum = {}));
function IntegrationStateCreateResponseFromJSON(json) {
    return IntegrationStateCreateResponseFromJSONTyped(json, false);
}
exports.IntegrationStateCreateResponseFromJSON = IntegrationStateCreateResponseFromJSON;
function IntegrationStateCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'integrationState': !runtime_1.exists(json, 'integrationState') ? undefined : _1.IntegrationStateFromJSON(json['integrationState']),
    };
}
exports.IntegrationStateCreateResponseFromJSONTyped = IntegrationStateCreateResponseFromJSONTyped;
function IntegrationStateCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'integrationState': _1.IntegrationStateToJSON(value.integrationState),
    };
}
exports.IntegrationStateCreateResponseToJSON = IntegrationStateCreateResponseToJSON;
