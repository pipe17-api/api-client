"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionUpdateErrorToJSON = exports.ExceptionUpdateErrorFromJSONTyped = exports.ExceptionUpdateErrorFromJSON = exports.ExceptionUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionUpdateErrorSuccessEnum;
(function (ExceptionUpdateErrorSuccessEnum) {
    ExceptionUpdateErrorSuccessEnum["False"] = "false";
})(ExceptionUpdateErrorSuccessEnum = exports.ExceptionUpdateErrorSuccessEnum || (exports.ExceptionUpdateErrorSuccessEnum = {}));
function ExceptionUpdateErrorFromJSON(json) {
    return ExceptionUpdateErrorFromJSONTyped(json, false);
}
exports.ExceptionUpdateErrorFromJSON = ExceptionUpdateErrorFromJSON;
function ExceptionUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionUpdateDataFromJSON(json['exception']),
    };
}
exports.ExceptionUpdateErrorFromJSONTyped = ExceptionUpdateErrorFromJSONTyped;
function ExceptionUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'exception': _1.ExceptionUpdateDataToJSON(value.exception),
    };
}
exports.ExceptionUpdateErrorToJSON = ExceptionUpdateErrorToJSON;
