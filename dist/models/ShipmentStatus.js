"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentStatusToJSON = exports.ShipmentStatusFromJSONTyped = exports.ShipmentStatusFromJSON = exports.ShipmentStatus = void 0;
/**
 * Shipment Status
 * @export
 * @enum {string}
 */
var ShipmentStatus;
(function (ShipmentStatus) {
    ShipmentStatus["PendingInventory"] = "pendingInventory";
    ShipmentStatus["PendingShippingLabel"] = "pendingShippingLabel";
    ShipmentStatus["ReadyForFulfillment"] = "readyForFulfillment";
    ShipmentStatus["SentToFulfillment"] = "sentToFulfillment";
    ShipmentStatus["Fulfilled"] = "fulfilled";
    ShipmentStatus["PartialFulfillment"] = "partialFulfillment";
    ShipmentStatus["Canceled"] = "canceled";
    ShipmentStatus["CanceledRestock"] = "canceledRestock";
    ShipmentStatus["Failed"] = "failed";
})(ShipmentStatus = exports.ShipmentStatus || (exports.ShipmentStatus = {}));
function ShipmentStatusFromJSON(json) {
    return ShipmentStatusFromJSONTyped(json, false);
}
exports.ShipmentStatusFromJSON = ShipmentStatusFromJSON;
function ShipmentStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ShipmentStatusFromJSONTyped = ShipmentStatusFromJSONTyped;
function ShipmentStatusToJSON(value) {
    return value;
}
exports.ShipmentStatusToJSON = ShipmentStatusToJSON;
