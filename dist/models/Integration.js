"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationToJSON = exports.IntegrationFromJSONTyped = exports.IntegrationFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationFromJSON(json) {
    return IntegrationFromJSONTyped(json, false);
}
exports.IntegrationFromJSON = IntegrationFromJSON;
function IntegrationFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : json['apikey'],
        'version': !runtime_1.exists(json, 'version') ? undefined : json['version'],
        'integrationId': !runtime_1.exists(json, 'integrationId') ? undefined : json['integrationId'],
        'errorText': !runtime_1.exists(json, 'errorText') ? undefined : json['errorText'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.IntegrationStatusFromJSON(json['status']),
        'integrationName': json['integrationName'],
        'connectorName': !runtime_1.exists(json, 'connectorName') ? undefined : json['connectorName'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'environment': !runtime_1.exists(json, 'environment') ? undefined : _1.EnvFromJSON(json['environment']),
        'entities': !runtime_1.exists(json, 'entities') ? undefined : (json['entities'].map(_1.IntegrationEntityFromJSON)),
        'settings': !runtime_1.exists(json, 'settings') ? undefined : _1.IntegrationSettingsFromJSON(json['settings']),
        'connection': !runtime_1.exists(json, 'connection') ? undefined : _1.IntegrationConnectionFromJSON(json['connection']),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.IntegrationFromJSONTyped = IntegrationFromJSONTyped;
function IntegrationToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikey': value.apikey,
        'version': value.version,
        'integrationId': value.integrationId,
        'errorText': value.errorText,
        'status': _1.IntegrationStatusToJSON(value.status),
        'integrationName': value.integrationName,
        'connectorName': value.connectorName,
        'connectorId': value.connectorId,
        'environment': _1.EnvToJSON(value.environment),
        'entities': value.entities === undefined ? undefined : (value.entities.map(_1.IntegrationEntityToJSON)),
        'settings': _1.IntegrationSettingsToJSON(value.settings),
        'connection': _1.IntegrationConnectionToJSON(value.connection),
    };
}
exports.IntegrationToJSON = IntegrationToJSON;
