/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressNullable, AddressUpdateData, NameValue, PurchaseCost, PurchaseStatus, PurchaseUpdateLineItem } from './';
/**
 *
 * @export
 * @interface PurchaseUpdateRequest
 */
export interface PurchaseUpdateRequest {
    /**
     *
     * @type {PurchaseStatus}
     * @memberof PurchaseUpdateRequest
     */
    status?: PurchaseStatus;
    /**
     *
     * @type {Array<PurchaseUpdateLineItem>}
     * @memberof PurchaseUpdateRequest
     */
    lineItems?: Array<PurchaseUpdateLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof PurchaseUpdateRequest
     */
    subTotalPrice?: number | null;
    /**
     * Purchase Tax
     * @type {number}
     * @memberof PurchaseUpdateRequest
     */
    purchaseTax?: number | null;
    /**
     * Total Price
     * @type {number}
     * @memberof PurchaseUpdateRequest
     */
    totalPrice?: number | null;
    /**
     * Purchase costs
     * @type {Array<PurchaseCost>}
     * @memberof PurchaseUpdateRequest
     */
    costs?: Array<PurchaseCost> | null;
    /**
     * Purchase order created by
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    employeeName?: string | null;
    /**
     * Purchase Notes
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    purchaseNotes?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof PurchaseUpdateRequest
     */
    vendorAddress?: AddressUpdateData;
    /**
     *
     * @type {AddressNullable}
     * @memberof PurchaseUpdateRequest
     */
    toAddress?: AddressNullable | null;
    /**
     * Ship to location ID
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    toLocationId?: string | null;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    shippingCarrier?: string | null;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    shippingService?: string | null;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof PurchaseUpdateRequest
     */
    shipByDate?: Date | null;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof PurchaseUpdateRequest
     */
    actualArrivalDate?: Date | null;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof PurchaseUpdateRequest
     */
    expectedArrivalDate?: Date | null;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    shippingNotes?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof PurchaseUpdateRequest
     */
    billingAddress?: AddressUpdateData;
    /**
     * Purchase order reference number
     * @type {string}
     * @memberof PurchaseUpdateRequest
     */
    referenceNumber?: string | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof PurchaseUpdateRequest
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof PurchaseUpdateRequest
     */
    timestamp?: Date | null;
}
export declare function PurchaseUpdateRequestFromJSON(json: any): PurchaseUpdateRequest;
export declare function PurchaseUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseUpdateRequest;
export declare function PurchaseUpdateRequestToJSON(value?: PurchaseUpdateRequest | null): any;
