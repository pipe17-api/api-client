"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductUpdateErrorAllOfToJSON = exports.ProductUpdateErrorAllOfFromJSONTyped = exports.ProductUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductUpdateErrorAllOfFromJSON(json) {
    return ProductUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ProductUpdateErrorAllOfFromJSON = ProductUpdateErrorAllOfFromJSON;
function ProductUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'product': !runtime_1.exists(json, 'product') ? undefined : _1.ProductUpdateDataFromJSON(json['product']),
    };
}
exports.ProductUpdateErrorAllOfFromJSONTyped = ProductUpdateErrorAllOfFromJSONTyped;
function ProductUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'product': _1.ProductUpdateDataToJSON(value.product),
    };
}
exports.ProductUpdateErrorAllOfToJSON = ProductUpdateErrorAllOfToJSON;
