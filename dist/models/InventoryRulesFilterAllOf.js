"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRulesFilterAllOfToJSON = exports.InventoryRulesFilterAllOfFromJSONTyped = exports.InventoryRulesFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryRulesFilterAllOfFromJSON(json) {
    return InventoryRulesFilterAllOfFromJSONTyped(json, false);
}
exports.InventoryRulesFilterAllOfFromJSON = InventoryRulesFilterAllOfFromJSON;
function InventoryRulesFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'ruleId': !runtime_1.exists(json, 'ruleId') ? undefined : json['ruleId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.InventoryRulesFilterAllOfFromJSONTyped = InventoryRulesFilterAllOfFromJSONTyped;
function InventoryRulesFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'ruleId': value.ruleId,
        'integration': value.integration,
        'deleted': value.deleted,
    };
}
exports.InventoryRulesFilterAllOfToJSON = InventoryRulesFilterAllOfToJSON;
