"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleToJSON = exports.RoleFromJSONTyped = exports.RoleFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleFromJSON(json) {
    return RoleFromJSONTyped(json, false);
}
exports.RoleFromJSON = RoleFromJSON;
function RoleFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'roleId': !runtime_1.exists(json, 'roleId') ? undefined : json['roleId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.RoleStatusFromJSON(json['status']),
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'name': json['name'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'methods': !runtime_1.exists(json, 'methods') ? undefined : _1.MethodsFromJSON(json['methods']),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.RoleFromJSONTyped = RoleFromJSONTyped;
function RoleToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'roleId': value.roleId,
        'status': _1.RoleStatusToJSON(value.status),
        'isPublic': value.isPublic,
        'name': value.name,
        'description': value.description,
        'methods': _1.MethodsToJSON(value.methods),
    };
}
exports.RoleToJSON = RoleToJSON;
