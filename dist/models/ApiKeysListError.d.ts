/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKeysListFilter } from './';
/**
 *
 * @export
 * @interface ApiKeysListError
 */
export interface ApiKeysListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ApiKeysListError
     */
    success: ApiKeysListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeysListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ApiKeysListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ApiKeysListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ApiKeysListFilter}
     * @memberof ApiKeysListError
     */
    filters?: ApiKeysListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeysListErrorSuccessEnum {
    False = "false"
}
export declare function ApiKeysListErrorFromJSON(json: any): ApiKeysListError;
export declare function ApiKeysListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysListError;
export declare function ApiKeysListErrorToJSON(value?: ApiKeysListError | null): any;
