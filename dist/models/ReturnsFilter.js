"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsFilterToJSON = exports.ReturnsFilterFromJSONTyped = exports.ReturnsFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnsFilterFromJSON(json) {
    return ReturnsFilterFromJSONTyped(json, false);
}
exports.ReturnsFilterFromJSON = ReturnsFilterFromJSON;
function ReturnsFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'returnId': !runtime_1.exists(json, 'returnId') ? undefined : json['returnId'],
        'extReturnId': !runtime_1.exists(json, 'extReturnId') ? undefined : json['extReturnId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ReturnStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.ReturnsFilterFromJSONTyped = ReturnsFilterFromJSONTyped;
function ReturnsFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'returnId': value.returnId,
        'extReturnId': value.extReturnId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ReturnStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.ReturnsFilterToJSON = ReturnsFilterToJSON;
