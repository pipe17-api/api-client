"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsFilterAllOfToJSON = exports.RoutingsFilterAllOfFromJSONTyped = exports.RoutingsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingsFilterAllOfFromJSON(json) {
    return RoutingsFilterAllOfFromJSONTyped(json, false);
}
exports.RoutingsFilterAllOfFromJSON = RoutingsFilterAllOfFromJSON;
function RoutingsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routingId': !runtime_1.exists(json, 'routingId') ? undefined : json['routingId'],
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.RoutingsFilterAllOfFromJSONTyped = RoutingsFilterAllOfFromJSONTyped;
function RoutingsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routingId': value.routingId,
        'uuid': value.uuid,
        'deleted': value.deleted,
    };
}
exports.RoutingsFilterAllOfToJSON = RoutingsFilterAllOfToJSON;
