/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, IntegrationConnection, IntegrationEntity, IntegrationSettings, IntegrationStatus } from './';
/**
 *
 * @export
 * @interface Integration
 */
export interface Integration {
    /**
     * Integration api Key
     * @type {string}
     * @memberof Integration
     */
    apikey?: string;
    /**
     * Integration version
     * @type {string}
     * @memberof Integration
     */
    version?: string;
    /**
     * Integration Id
     * @type {string}
     * @memberof Integration
     */
    integrationId?: string;
    /**
     * Error message
     * @type {string}
     * @memberof Integration
     */
    errorText?: string;
    /**
     *
     * @type {IntegrationStatus}
     * @memberof Integration
     */
    status?: IntegrationStatus;
    /**
     * Integration Name
     * @type {string}
     * @memberof Integration
     */
    integrationName: string;
    /**
     * Connector Name (either Name or ID required)
     * @type {string}
     * @memberof Integration
     */
    connectorName?: string;
    /**
     * Connector ID (either Name or ID required)
     * @type {string}
     * @memberof Integration
     */
    connectorId?: string;
    /**
     *
     * @type {Env}
     * @memberof Integration
     */
    environment?: Env;
    /**
     *
     * @type {Array<IntegrationEntity>}
     * @memberof Integration
     */
    entities?: Array<IntegrationEntity>;
    /**
     *
     * @type {IntegrationSettings}
     * @memberof Integration
     */
    settings?: IntegrationSettings;
    /**
     *
     * @type {IntegrationConnection}
     * @memberof Integration
     */
    connection?: IntegrationConnection;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Integration
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Integration
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Integration
     */
    readonly orgKey?: string;
}
export declare function IntegrationFromJSON(json: any): Integration;
export declare function IntegrationFromJSONTyped(json: any, ignoreDiscriminator: boolean): Integration;
export declare function IntegrationToJSON(value?: Integration | null): any;
