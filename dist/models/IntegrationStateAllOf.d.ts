/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationStateAllOf
 */
export interface IntegrationStateAllOf {
    /**
     * Integration ID
     * @type {string}
     * @memberof IntegrationStateAllOf
     */
    integration?: string;
}
export declare function IntegrationStateAllOfFromJSON(json: any): IntegrationStateAllOf;
export declare function IntegrationStateAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateAllOf;
export declare function IntegrationStateAllOfToJSON(value?: IntegrationStateAllOf | null): any;
