"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationsListErrorAllOfToJSON = exports.OrganizationsListErrorAllOfFromJSONTyped = exports.OrganizationsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationsListErrorAllOfFromJSON(json) {
    return OrganizationsListErrorAllOfFromJSONTyped(json, false);
}
exports.OrganizationsListErrorAllOfFromJSON = OrganizationsListErrorAllOfFromJSON;
function OrganizationsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrganizationsListFilterFromJSON(json['filters']),
    };
}
exports.OrganizationsListErrorAllOfFromJSONTyped = OrganizationsListErrorAllOfFromJSONTyped;
function OrganizationsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.OrganizationsListFilterToJSON(value.filters),
    };
}
exports.OrganizationsListErrorAllOfToJSON = OrganizationsListErrorAllOfToJSON;
