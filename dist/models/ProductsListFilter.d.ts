/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductStatus, ProductType } from './';
/**
 *
 * @export
 * @interface ProductsListFilter
 */
export interface ProductsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ProductsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ProductsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ProductsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ProductsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ProductsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ProductsListFilter
     */
    count?: number;
    /**
     * Products by list of productId
     * @type {Array<string>}
     * @memberof ProductsListFilter
     */
    productId?: Array<string>;
    /**
     * Products by list of extProductId
     * @type {Array<string>}
     * @memberof ProductsListFilter
     */
    extProductId?: Array<string>;
    /**
     * All variants of a parent products by list of external ids
     * @type {Array<string>}
     * @memberof ProductsListFilter
     */
    parentExtProductId?: Array<string>;
    /**
     * Products with this status
     * @type {Array<ProductStatus>}
     * @memberof ProductsListFilter
     */
    status?: Array<ProductStatus>;
    /**
     * Products matching specified types
     * @type {Array<ProductType>}
     * @memberof ProductsListFilter
     */
    types?: Array<ProductType>;
    /**
     * Soft deleted products
     * @type {boolean}
     * @memberof ProductsListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof ProductsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ProductsListFilter
     */
    keys?: string;
    /**
     * Products by list sku
     * @type {Array<string>}
     * @memberof ProductsListFilter
     */
    sku?: Array<string>;
    /**
     * Products matching this name
     * @type {string}
     * @memberof ProductsListFilter
     */
    name?: string;
    /**
     * Products of the bundle with this SKU
     * @type {string}
     * @memberof ProductsListFilter
     */
    bundleSKU?: string;
    /**
     * All bundles containing the item with this SKU
     * @type {string}
     * @memberof ProductsListFilter
     */
    inBundle?: string;
}
export declare function ProductsListFilterFromJSON(json: any): ProductsListFilter;
export declare function ProductsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsListFilter;
export declare function ProductsListFilterToJSON(value?: ProductsListFilter | null): any;
