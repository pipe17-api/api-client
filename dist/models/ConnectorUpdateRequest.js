"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorUpdateRequestToJSON = exports.ConnectorUpdateRequestFromJSONTyped = exports.ConnectorUpdateRequestFromJSON = exports.ConnectorUpdateRequestStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorUpdateRequestStatusEnum;
(function (ConnectorUpdateRequestStatusEnum) {
    ConnectorUpdateRequestStatusEnum["Active"] = "active";
    ConnectorUpdateRequestStatusEnum["Inactive"] = "inactive";
})(ConnectorUpdateRequestStatusEnum = exports.ConnectorUpdateRequestStatusEnum || (exports.ConnectorUpdateRequestStatusEnum = {}));
function ConnectorUpdateRequestFromJSON(json) {
    return ConnectorUpdateRequestFromJSONTyped(json, false);
}
exports.ConnectorUpdateRequestFromJSON = ConnectorUpdateRequestFromJSON;
function ConnectorUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'readme': !runtime_1.exists(json, 'readme') ? undefined : json['readme'],
        'version': !runtime_1.exists(json, 'version') ? undefined : json['version'],
        'connectorType': !runtime_1.exists(json, 'connectorType') ? undefined : _1.ConnectorTypeFromJSON(json['connectorType']),
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'entities': !runtime_1.exists(json, 'entities') ? undefined : (json['entities'].map(_1.ConnectorEntityFromJSON)),
        'settings': !runtime_1.exists(json, 'settings') ? undefined : _1.ConnectorSettingsFromJSON(json['settings']),
        'connection': !runtime_1.exists(json, 'connection') ? undefined : _1.ConnectorConnectionFromJSON(json['connection']),
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.ConnectorUpdateDataWebhookFromJSON(json['webhook']),
        'orgWideAccess': !runtime_1.exists(json, 'orgWideAccess') ? undefined : json['orgWideAccess'],
        'orgUnique': !runtime_1.exists(json, 'orgUnique') ? undefined : json['orgUnique'],
        'displayName': !runtime_1.exists(json, 'displayName') ? undefined : json['displayName'],
    };
}
exports.ConnectorUpdateRequestFromJSONTyped = ConnectorUpdateRequestFromJSONTyped;
function ConnectorUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'description': value.description,
        'readme': value.readme,
        'version': value.version,
        'connectorType': _1.ConnectorTypeToJSON(value.connectorType),
        'status': value.status,
        'logoUrl': value.logoUrl,
        'entities': value.entities === undefined ? undefined : (value.entities.map(_1.ConnectorEntityToJSON)),
        'settings': _1.ConnectorSettingsToJSON(value.settings),
        'connection': _1.ConnectorConnectionToJSON(value.connection),
        'webhook': _1.ConnectorUpdateDataWebhookToJSON(value.webhook),
        'orgWideAccess': value.orgWideAccess,
        'orgUnique': value.orgUnique,
        'displayName': value.displayName,
    };
}
exports.ConnectorUpdateRequestToJSON = ConnectorUpdateRequestToJSON;
