/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ProductAllOf
 */
export interface ProductAllOf {
    /**
     * Product ID
     * @type {string}
     * @memberof ProductAllOf
     */
    productId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof ProductAllOf
     */
    integration?: string;
}
export declare function ProductAllOfFromJSON(json: any): ProductAllOf;
export declare function ProductAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductAllOf;
export declare function ProductAllOfToJSON(value?: ProductAllOf | null): any;
