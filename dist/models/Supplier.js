"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierToJSON = exports.SupplierFromJSONTyped = exports.SupplierFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SupplierFromJSON(json) {
    return SupplierFromJSONTyped(json, false);
}
exports.SupplierFromJSON = SupplierFromJSON;
function SupplierFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'supplierId': !runtime_1.exists(json, 'supplierId') ? undefined : json['supplierId'],
        'name': json['name'],
        'address': _1.SupplierAddressFromJSON(json['address']),
        'contacts': !runtime_1.exists(json, 'contacts') ? undefined : (json['contacts'].map(_1.SupplierContactFromJSON)),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.SupplierFromJSONTyped = SupplierFromJSONTyped;
function SupplierToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'supplierId': value.supplierId,
        'name': value.name,
        'address': _1.SupplierAddressToJSON(value.address),
        'contacts': value.contacts === undefined ? undefined : (value.contacts.map(_1.SupplierContactToJSON)),
    };
}
exports.SupplierToJSON = SupplierToJSON;
