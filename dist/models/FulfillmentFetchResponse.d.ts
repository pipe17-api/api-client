/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Fulfillment } from './';
/**
 *
 * @export
 * @interface FulfillmentFetchResponse
 */
export interface FulfillmentFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof FulfillmentFetchResponse
     */
    success: FulfillmentFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof FulfillmentFetchResponse
     */
    code: FulfillmentFetchResponseCodeEnum;
    /**
     *
     * @type {Fulfillment}
     * @memberof FulfillmentFetchResponse
     */
    fulfillment?: Fulfillment;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum FulfillmentFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function FulfillmentFetchResponseFromJSON(json: any): FulfillmentFetchResponse;
export declare function FulfillmentFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentFetchResponse;
export declare function FulfillmentFetchResponseToJSON(value?: FulfillmentFetchResponse | null): any;
