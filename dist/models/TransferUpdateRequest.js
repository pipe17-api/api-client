"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferUpdateRequestToJSON = exports.TransferUpdateRequestFromJSONTyped = exports.TransferUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransferUpdateRequestFromJSON(json) {
    return TransferUpdateRequestFromJSONTyped(json, false);
}
exports.TransferUpdateRequestFromJSON = TransferUpdateRequestFromJSON;
function TransferUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.TransferStatusFromJSON(json['status']),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.TransferUpdateLineItemFromJSON)),
        'fromLocationId': !runtime_1.exists(json, 'fromLocationId') ? undefined : json['fromLocationId'],
        'toAddress': !runtime_1.exists(json, 'toAddress') ? undefined : _1.AddressNullableFromJSON(json['toAddress']),
        'toLocationId': !runtime_1.exists(json, 'toLocationId') ? undefined : json['toLocationId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingService': !runtime_1.exists(json, 'shippingService') ? undefined : json['shippingService'],
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (json['shipByDate'] === null ? null : new Date(json['shipByDate'])),
        'expectedShipDate': !runtime_1.exists(json, 'expectedShipDate') ? undefined : (json['expectedShipDate'] === null ? null : new Date(json['expectedShipDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (json['expectedArrivalDate'] === null ? null : new Date(json['expectedArrivalDate'])),
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (json['actualArrivalDate'] === null ? null : new Date(json['actualArrivalDate'])),
        'shippingNotes': !runtime_1.exists(json, 'shippingNotes') ? undefined : json['shippingNotes'],
        'employeeName': !runtime_1.exists(json, 'employeeName') ? undefined : json['employeeName'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'transferNotes': !runtime_1.exists(json, 'transferNotes') ? undefined : json['transferNotes'],
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (json['timestamp'] === null ? null : new Date(json['timestamp'])),
    };
}
exports.TransferUpdateRequestFromJSONTyped = TransferUpdateRequestFromJSONTyped;
function TransferUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.TransferStatusToJSON(value.status),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.TransferUpdateLineItemToJSON)),
        'fromLocationId': value.fromLocationId,
        'toAddress': _1.AddressNullableToJSON(value.toAddress),
        'toLocationId': value.toLocationId,
        'shippingCarrier': value.shippingCarrier,
        'shippingService': value.shippingService,
        'shipByDate': value.shipByDate === undefined ? undefined : (value.shipByDate === null ? null : new Date(value.shipByDate).toISOString()),
        'expectedShipDate': value.expectedShipDate === undefined ? undefined : (value.expectedShipDate === null ? null : new Date(value.expectedShipDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (value.expectedArrivalDate === null ? null : new Date(value.expectedArrivalDate).toISOString()),
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (value.actualArrivalDate === null ? null : new Date(value.actualArrivalDate).toISOString()),
        'shippingNotes': value.shippingNotes,
        'employeeName': value.employeeName,
        'incoterms': value.incoterms,
        'transferNotes': value.transferNotes,
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'timestamp': value.timestamp === undefined ? undefined : (value.timestamp === null ? null : new Date(value.timestamp).toISOString()),
    };
}
exports.TransferUpdateRequestToJSON = TransferUpdateRequestToJSON;
