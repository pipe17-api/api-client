/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, Methods, Tier } from './';
/**
 *
 * @export
 * @interface ApiKeyCreateData
 */
export interface ApiKeyCreateData {
    /**
     * API Key Name
     * @type {string}
     * @memberof ApiKeyCreateData
     */
    name: string;
    /**
     * API Key Description
     * @type {string}
     * @memberof ApiKeyCreateData
     */
    description?: string;
    /**
     * Connector ID
     * @type {string}
     * @memberof ApiKeyCreateData
     */
    connector?: string;
    /**
     * Integration ID
     * @type {string}
     * @memberof ApiKeyCreateData
     */
    integration?: string;
    /**
     *
     * @type {Tier}
     * @memberof ApiKeyCreateData
     */
    tier?: Tier;
    /**
     *
     * @type {Env}
     * @memberof ApiKeyCreateData
     */
    environment?: Env;
    /**
     *
     * @type {Methods}
     * @memberof ApiKeyCreateData
     */
    methods: Methods;
    /**
     * IP/CIDR Whitelist
     * @type {Array<string>}
     * @memberof ApiKeyCreateData
     */
    allowedIPs: Array<string>;
}
export declare function ApiKeyCreateDataFromJSON(json: any): ApiKeyCreateData;
export declare function ApiKeyCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyCreateData;
export declare function ApiKeyCreateDataToJSON(value?: ApiKeyCreateData | null): any;
