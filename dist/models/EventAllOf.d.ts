/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestSource } from './';
/**
 *
 * @export
 * @interface EventAllOf
 */
export interface EventAllOf {
    /**
     * Event ID
     * @type {string}
     * @memberof EventAllOf
     */
    eventId?: string;
    /**
     *
     * @type {EventRequestSource}
     * @memberof EventAllOf
     */
    source?: EventRequestSource;
}
export declare function EventAllOfFromJSON(json: any): EventAllOf;
export declare function EventAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventAllOf;
export declare function EventAllOfToJSON(value?: EventAllOf | null): any;
