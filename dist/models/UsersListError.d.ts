/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UsersListFilter } from './';
/**
 *
 * @export
 * @interface UsersListError
 */
export interface UsersListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof UsersListError
     */
    success: UsersListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UsersListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof UsersListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof UsersListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {UsersListFilter}
     * @memberof UsersListError
     */
    filters?: UsersListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum UsersListErrorSuccessEnum {
    False = "false"
}
export declare function UsersListErrorFromJSON(json: any): UsersListError;
export declare function UsersListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersListError;
export declare function UsersListErrorToJSON(value?: UsersListError | null): any;
