"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferCreateResultToJSON = exports.TransferCreateResultFromJSONTyped = exports.TransferCreateResultFromJSON = exports.TransferCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransferCreateResultStatusEnum;
(function (TransferCreateResultStatusEnum) {
    TransferCreateResultStatusEnum["Submitted"] = "submitted";
    TransferCreateResultStatusEnum["Failed"] = "failed";
})(TransferCreateResultStatusEnum = exports.TransferCreateResultStatusEnum || (exports.TransferCreateResultStatusEnum = {}));
function TransferCreateResultFromJSON(json) {
    return TransferCreateResultFromJSONTyped(json, false);
}
exports.TransferCreateResultFromJSON = TransferCreateResultFromJSON;
function TransferCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'transfer': !runtime_1.exists(json, 'transfer') ? undefined : _1.TransferFromJSON(json['transfer']),
    };
}
exports.TransferCreateResultFromJSONTyped = TransferCreateResultFromJSONTyped;
function TransferCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'transfer': _1.TransferToJSON(value.transfer),
    };
}
exports.TransferCreateResultToJSON = TransferCreateResultToJSON;
