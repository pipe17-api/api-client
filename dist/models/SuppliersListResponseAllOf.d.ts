/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Supplier, SuppliersListFilter } from './';
/**
 *
 * @export
 * @interface SuppliersListResponseAllOf
 */
export interface SuppliersListResponseAllOf {
    /**
     *
     * @type {SuppliersListFilter}
     * @memberof SuppliersListResponseAllOf
     */
    filters?: SuppliersListFilter;
    /**
     *
     * @type {Array<Supplier>}
     * @memberof SuppliersListResponseAllOf
     */
    suppliers?: Array<Supplier>;
    /**
     *
     * @type {Pagination}
     * @memberof SuppliersListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function SuppliersListResponseAllOfFromJSON(json: any): SuppliersListResponseAllOf;
export declare function SuppliersListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersListResponseAllOf;
export declare function SuppliersListResponseAllOfToJSON(value?: SuppliersListResponseAllOf | null): any;
