"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseUpdateErrorToJSON = exports.PurchaseUpdateErrorFromJSONTyped = exports.PurchaseUpdateErrorFromJSON = exports.PurchaseUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchaseUpdateErrorSuccessEnum;
(function (PurchaseUpdateErrorSuccessEnum) {
    PurchaseUpdateErrorSuccessEnum["False"] = "false";
})(PurchaseUpdateErrorSuccessEnum = exports.PurchaseUpdateErrorSuccessEnum || (exports.PurchaseUpdateErrorSuccessEnum = {}));
function PurchaseUpdateErrorFromJSON(json) {
    return PurchaseUpdateErrorFromJSONTyped(json, false);
}
exports.PurchaseUpdateErrorFromJSON = PurchaseUpdateErrorFromJSON;
function PurchaseUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'purchase': !runtime_1.exists(json, 'purchase') ? undefined : _1.PurchaseUpdateDataFromJSON(json['purchase']),
    };
}
exports.PurchaseUpdateErrorFromJSONTyped = PurchaseUpdateErrorFromJSONTyped;
function PurchaseUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'purchase': _1.PurchaseUpdateDataToJSON(value.purchase),
    };
}
exports.PurchaseUpdateErrorToJSON = PurchaseUpdateErrorToJSON;
