"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelsFilterAllOfToJSON = exports.LabelsFilterAllOfFromJSONTyped = exports.LabelsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function LabelsFilterAllOfFromJSON(json) {
    return LabelsFilterAllOfFromJSONTyped(json, false);
}
exports.LabelsFilterAllOfFromJSON = LabelsFilterAllOfFromJSON;
function LabelsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'labelId': !runtime_1.exists(json, 'labelId') ? undefined : json['labelId'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.LabelsFilterAllOfFromJSONTyped = LabelsFilterAllOfFromJSONTyped;
function LabelsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'labelId': value.labelId,
        'deleted': value.deleted,
    };
}
exports.LabelsFilterAllOfToJSON = LabelsFilterAllOfToJSON;
