/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserStatus } from './';
/**
 *
 * @export
 * @interface UsersListFilter
 */
export interface UsersListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof UsersListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof UsersListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof UsersListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof UsersListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof UsersListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof UsersListFilter
     */
    count?: number;
    /**
     * Users by list of userId
     * @type {Array<string>}
     * @memberof UsersListFilter
     */
    userId?: Array<string>;
    /**
     * Users whose name contains this string
     * @type {string}
     * @memberof UsersListFilter
     */
    name?: string;
    /**
     * Users whose email contains this string
     * @type {string}
     * @memberof UsersListFilter
     */
    email?: string;
    /**
     * Users by list of statuses
     * @type {Array<UserStatus>}
     * @memberof UsersListFilter
     */
    status?: Array<UserStatus>;
    /**
     * Return users of all organizations
     * @type {string}
     * @memberof UsersListFilter
     */
    organizations?: string;
    /**
     * List sort order
     * @type {string}
     * @memberof UsersListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof UsersListFilter
     */
    keys?: string;
}
export declare function UsersListFilterFromJSON(json: any): UsersListFilter;
export declare function UsersListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersListFilter;
export declare function UsersListFilterToJSON(value?: UsersListFilter | null): any;
