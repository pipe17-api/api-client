/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Organization } from './';
/**
 *
 * @export
 * @interface OrganizationCreateResponseAllOf
 */
export interface OrganizationCreateResponseAllOf {
    /**
     *
     * @type {Organization}
     * @memberof OrganizationCreateResponseAllOf
     */
    organization?: Organization;
}
export declare function OrganizationCreateResponseAllOfFromJSON(json: any): OrganizationCreateResponseAllOf;
export declare function OrganizationCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationCreateResponseAllOf;
export declare function OrganizationCreateResponseAllOfToJSON(value?: OrganizationCreateResponseAllOf | null): any;
