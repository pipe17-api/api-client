/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRuleCreateData } from './';
/**
 *
 * @export
 * @interface InventoryRuleCreateErrorAllOf
 */
export interface InventoryRuleCreateErrorAllOf {
    /**
     *
     * @type {InventoryRuleCreateData}
     * @memberof InventoryRuleCreateErrorAllOf
     */
    inventoryrule?: InventoryRuleCreateData;
}
export declare function InventoryRuleCreateErrorAllOfFromJSON(json: any): InventoryRuleCreateErrorAllOf;
export declare function InventoryRuleCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleCreateErrorAllOf;
export declare function InventoryRuleCreateErrorAllOfToJSON(value?: InventoryRuleCreateErrorAllOf | null): any;
