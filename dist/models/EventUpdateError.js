"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventUpdateErrorToJSON = exports.EventUpdateErrorFromJSONTyped = exports.EventUpdateErrorFromJSON = exports.EventUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var EventUpdateErrorSuccessEnum;
(function (EventUpdateErrorSuccessEnum) {
    EventUpdateErrorSuccessEnum["False"] = "false";
})(EventUpdateErrorSuccessEnum = exports.EventUpdateErrorSuccessEnum || (exports.EventUpdateErrorSuccessEnum = {}));
function EventUpdateErrorFromJSON(json) {
    return EventUpdateErrorFromJSONTyped(json, false);
}
exports.EventUpdateErrorFromJSON = EventUpdateErrorFromJSON;
function EventUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.EventUpdateErrorFromJSONTyped = EventUpdateErrorFromJSONTyped;
function EventUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.EventUpdateErrorToJSON = EventUpdateErrorToJSON;
