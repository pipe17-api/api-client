/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleCreateData } from './';
/**
 *
 * @export
 * @interface RoleCreateError
 */
export interface RoleCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoleCreateError
     */
    success: RoleCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoleCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoleCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoleCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RoleCreateData}
     * @memberof RoleCreateError
     */
    role?: RoleCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum RoleCreateErrorSuccessEnum {
    False = "false"
}
export declare function RoleCreateErrorFromJSON(json: any): RoleCreateError;
export declare function RoleCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleCreateError;
export declare function RoleCreateErrorToJSON(value?: RoleCreateError | null): any;
