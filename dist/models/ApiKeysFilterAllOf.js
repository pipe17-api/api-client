"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeysFilterAllOfToJSON = exports.ApiKeysFilterAllOfFromJSONTyped = exports.ApiKeysFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ApiKeysFilterAllOfFromJSON(json) {
    return ApiKeysFilterAllOfFromJSONTyped(json, false);
}
exports.ApiKeysFilterAllOfFromJSON = ApiKeysFilterAllOfFromJSON;
function ApiKeysFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'connector': !runtime_1.exists(json, 'connector') ? undefined : json['connector'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
    };
}
exports.ApiKeysFilterAllOfFromJSONTyped = ApiKeysFilterAllOfFromJSONTyped;
function ApiKeysFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'connector': value.connector,
        'integration': value.integration,
        'name': value.name,
    };
}
exports.ApiKeysFilterAllOfToJSON = ApiKeysFilterAllOfToJSON;
