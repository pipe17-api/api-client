/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Arrival notification Status
 * @export
 * @enum {string}
 */
export declare enum ArrivalStatus {
    Draft = "draft",
    Shipped = "shipped",
    Expected = "expected",
    PartialReceived = "partialReceived",
    Received = "received",
    Canceled = "canceled",
    Failed = "failed"
}
export declare function ArrivalStatusFromJSON(json: any): ArrivalStatus;
export declare function ArrivalStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalStatus;
export declare function ArrivalStatusToJSON(value?: ArrivalStatus | null): any;
