/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorConnection, ConnectorEntity, ConnectorSettings, ConnectorType, ConnectorWebhook } from './';
/**
 *
 * @export
 * @interface ConnectorCreateRequest
 */
export interface ConnectorCreateRequest {
    /**
     * Connector description
     * @type {string}
     * @memberof ConnectorCreateRequest
     */
    description?: string;
    /**
     * Connector readme markup
     * @type {string}
     * @memberof ConnectorCreateRequest
     */
    readme?: string;
    /**
     * Connector version
     * @type {string}
     * @memberof ConnectorCreateRequest
     */
    version?: string;
    /**
     * Connector Name
     * @type {string}
     * @memberof ConnectorCreateRequest
     */
    connectorName: string;
    /**
     *
     * @type {ConnectorType}
     * @memberof ConnectorCreateRequest
     */
    connectorType: ConnectorType;
    /**
     * Logo URL
     * @type {string}
     * @memberof ConnectorCreateRequest
     */
    logoUrl?: string;
    /**
     *
     * @type {Array<ConnectorEntity>}
     * @memberof ConnectorCreateRequest
     */
    entities?: Array<ConnectorEntity>;
    /**
     *
     * @type {ConnectorSettings}
     * @memberof ConnectorCreateRequest
     */
    settings?: ConnectorSettings;
    /**
     *
     * @type {ConnectorConnection}
     * @memberof ConnectorCreateRequest
     */
    connection?: ConnectorConnection;
    /**
     *
     * @type {ConnectorWebhook}
     * @memberof ConnectorCreateRequest
     */
    webhook?: ConnectorWebhook;
    /**
     * Indicates whether connector/integration should have access to all declared entity types
     * @type {boolean}
     * @memberof ConnectorCreateRequest
     */
    orgWideAccess?: boolean;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof ConnectorCreateRequest
     */
    orgUnique?: boolean;
    /**
     * Connector Display Name
     * @type {string}
     * @memberof ConnectorCreateRequest
     */
    displayName?: string;
}
export declare function ConnectorCreateRequestFromJSON(json: any): ConnectorCreateRequest;
export declare function ConnectorCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorCreateRequest;
export declare function ConnectorCreateRequestToJSON(value?: ConnectorCreateRequest | null): any;
