/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface PurchaseCost
 */
export interface PurchaseCost {
    /**
     * Name
     * @type {string}
     * @memberof PurchaseCost
     */
    name: string;
    /**
     * Cost
     * @type {number}
     * @memberof PurchaseCost
     */
    cost: number;
    /**
     * Quantity
     * @type {number}
     * @memberof PurchaseCost
     */
    quantity?: number;
}
export declare function PurchaseCostFromJSON(json: any): PurchaseCost;
export declare function PurchaseCostFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseCost;
export declare function PurchaseCostToJSON(value?: PurchaseCost | null): any;
