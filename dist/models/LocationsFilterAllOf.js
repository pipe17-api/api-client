"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsFilterAllOfToJSON = exports.LocationsFilterAllOfFromJSONTyped = exports.LocationsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationsFilterAllOfFromJSON(json) {
    return LocationsFilterAllOfFromJSONTyped(json, false);
}
exports.LocationsFilterAllOfFromJSON = LocationsFilterAllOfFromJSON;
function LocationsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.LocationStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.LocationsFilterAllOfFromJSONTyped = LocationsFilterAllOfFromJSONTyped;
function LocationsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'locationId': value.locationId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.LocationStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.LocationsFilterAllOfToJSON = LocationsFilterAllOfToJSON;
