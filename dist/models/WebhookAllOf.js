"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookAllOfToJSON = exports.WebhookAllOfFromJSONTyped = exports.WebhookAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function WebhookAllOfFromJSON(json) {
    return WebhookAllOfFromJSONTyped(json, false);
}
exports.WebhookAllOfFromJSON = WebhookAllOfFromJSON;
function WebhookAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'webhookId': !runtime_1.exists(json, 'webhookId') ? undefined : json['webhookId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
    };
}
exports.WebhookAllOfFromJSONTyped = WebhookAllOfFromJSONTyped;
function WebhookAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'webhookId': value.webhookId,
        'integration': value.integration,
        'connectorId': value.connectorId,
    };
}
exports.WebhookAllOfToJSON = WebhookAllOfToJSON;
