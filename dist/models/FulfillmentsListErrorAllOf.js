"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsListErrorAllOfToJSON = exports.FulfillmentsListErrorAllOfFromJSONTyped = exports.FulfillmentsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function FulfillmentsListErrorAllOfFromJSON(json) {
    return FulfillmentsListErrorAllOfFromJSONTyped(json, false);
}
exports.FulfillmentsListErrorAllOfFromJSON = FulfillmentsListErrorAllOfFromJSON;
function FulfillmentsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.FulfillmentsListFilterFromJSON(json['filters']),
    };
}
exports.FulfillmentsListErrorAllOfFromJSONTyped = FulfillmentsListErrorAllOfFromJSONTyped;
function FulfillmentsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.FulfillmentsListFilterToJSON(value.filters),
    };
}
exports.FulfillmentsListErrorAllOfToJSON = FulfillmentsListErrorAllOfToJSON;
