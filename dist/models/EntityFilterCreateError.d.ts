/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFilter } from './';
/**
 *
 * @export
 * @interface EntityFilterCreateError
 */
export interface EntityFilterCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntityFilterCreateError
     */
    success: EntityFilterCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntityFilterCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntityFilterCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {EntityFilter}
     * @memberof EntityFilterCreateError
     */
    entityFilter?: EntityFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterCreateErrorSuccessEnum {
    False = "false"
}
export declare function EntityFilterCreateErrorFromJSON(json: any): EntityFilterCreateError;
export declare function EntityFilterCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterCreateError;
export declare function EntityFilterCreateErrorToJSON(value?: EntityFilterCreateError | null): any;
