"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountCreateErrorAllOfToJSON = exports.AccountCreateErrorAllOfFromJSONTyped = exports.AccountCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountCreateErrorAllOfFromJSON(json) {
    return AccountCreateErrorAllOfFromJSONTyped(json, false);
}
exports.AccountCreateErrorAllOfFromJSON = AccountCreateErrorAllOfFromJSON;
function AccountCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountCreateDataFromJSON(json['account']),
    };
}
exports.AccountCreateErrorAllOfFromJSONTyped = AccountCreateErrorAllOfFromJSONTyped;
function AccountCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'account': _1.AccountCreateDataToJSON(value.account),
    };
}
exports.AccountCreateErrorAllOfToJSON = AccountCreateErrorAllOfToJSON;
