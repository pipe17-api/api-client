"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationCreateErrorToJSON = exports.IntegrationCreateErrorFromJSONTyped = exports.IntegrationCreateErrorFromJSON = exports.IntegrationCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationCreateErrorSuccessEnum;
(function (IntegrationCreateErrorSuccessEnum) {
    IntegrationCreateErrorSuccessEnum["False"] = "false";
})(IntegrationCreateErrorSuccessEnum = exports.IntegrationCreateErrorSuccessEnum || (exports.IntegrationCreateErrorSuccessEnum = {}));
function IntegrationCreateErrorFromJSON(json) {
    return IntegrationCreateErrorFromJSONTyped(json, false);
}
exports.IntegrationCreateErrorFromJSON = IntegrationCreateErrorFromJSON;
function IntegrationCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : _1.IntegrationCreateDataFromJSON(json['integration']),
    };
}
exports.IntegrationCreateErrorFromJSONTyped = IntegrationCreateErrorFromJSONTyped;
function IntegrationCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'integration': _1.IntegrationCreateDataToJSON(value.integration),
    };
}
exports.IntegrationCreateErrorToJSON = IntegrationCreateErrorToJSON;
