/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntitySchemaAllOf
 */
export interface EntitySchemaAllOf {
    /**
     * Schema ID
     * @type {string}
     * @memberof EntitySchemaAllOf
     */
    schemaId?: string;
}
export declare function EntitySchemaAllOfFromJSON(json: any): EntitySchemaAllOf;
export declare function EntitySchemaAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaAllOf;
export declare function EntitySchemaAllOfToJSON(value?: EntitySchemaAllOf | null): any;
