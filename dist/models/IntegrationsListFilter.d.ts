/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStatus } from './';
/**
 *
 * @export
 * @interface IntegrationsListFilter
 */
export interface IntegrationsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof IntegrationsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof IntegrationsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof IntegrationsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof IntegrationsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof IntegrationsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof IntegrationsListFilter
     */
    count?: number;
    /**
     * Retrieve by list of integrationId
     * @type {Array<string>}
     * @memberof IntegrationsListFilter
     */
    integrationId?: Array<string>;
    /**
     * Retrieve integrations by list of connector names
     * @type {Array<string>}
     * @memberof IntegrationsListFilter
     */
    connectorName?: Array<string>;
    /**
     * Retrieve integrations by connector ID
     * @type {string}
     * @memberof IntegrationsListFilter
     */
    connectorId?: string;
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationsListFilter
     */
    status?: IntegrationStatus;
    /**
     * List sort order
     * @type {string}
     * @memberof IntegrationsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof IntegrationsListFilter
     */
    keys?: string;
}
export declare function IntegrationsListFilterFromJSON(json: any): IntegrationsListFilter;
export declare function IntegrationsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsListFilter;
export declare function IntegrationsListFilterToJSON(value?: IntegrationsListFilter | null): any;
