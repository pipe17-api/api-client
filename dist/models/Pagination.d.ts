/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Pagination
 */
export interface Pagination {
    /**
     * Total number of items matching current filter
     * @type {number}
     * @memberof Pagination
     */
    total?: number;
    /**
     * Page size
     * @type {number}
     * @memberof Pagination
     */
    pageSize?: number;
    /**
     * Current page index, 1-based
     * @type {number}
     * @memberof Pagination
     */
    pageIndex?: number;
    /**
     * Number of pages. Only returned when offset matches size
     * @type {number}
     * @memberof Pagination
     */
    pages?: number;
    /**
     * Indicates first page. Only returned when offset matches size
     * @type {boolean}
     * @memberof Pagination
     */
    first?: boolean;
    /**
     * Indicates last page. Only returned when offset matches size
     * @type {boolean}
     * @memberof Pagination
     */
    last?: boolean;
    /**
     * Indicates that current offset is out of list range.
     * @type {boolean}
     * @memberof Pagination
     */
    outOfBounds?: boolean;
}
export declare function PaginationFromJSON(json: any): Pagination;
export declare function PaginationFromJSONTyped(json: any, ignoreDiscriminator: boolean): Pagination;
export declare function PaginationToJSON(value?: Pagination | null): any;
