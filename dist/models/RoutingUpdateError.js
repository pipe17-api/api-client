"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingUpdateErrorToJSON = exports.RoutingUpdateErrorFromJSONTyped = exports.RoutingUpdateErrorFromJSON = exports.RoutingUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoutingUpdateErrorSuccessEnum;
(function (RoutingUpdateErrorSuccessEnum) {
    RoutingUpdateErrorSuccessEnum["False"] = "false";
})(RoutingUpdateErrorSuccessEnum = exports.RoutingUpdateErrorSuccessEnum || (exports.RoutingUpdateErrorSuccessEnum = {}));
function RoutingUpdateErrorFromJSON(json) {
    return RoutingUpdateErrorFromJSONTyped(json, false);
}
exports.RoutingUpdateErrorFromJSON = RoutingUpdateErrorFromJSON;
function RoutingUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.RoutingUpdateDataFromJSON(json['routing']),
    };
}
exports.RoutingUpdateErrorFromJSONTyped = RoutingUpdateErrorFromJSONTyped;
function RoutingUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'routing': _1.RoutingUpdateDataToJSON(value.routing),
    };
}
exports.RoutingUpdateErrorToJSON = RoutingUpdateErrorToJSON;
