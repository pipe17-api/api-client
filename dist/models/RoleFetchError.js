"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleFetchErrorToJSON = exports.RoleFetchErrorFromJSONTyped = exports.RoleFetchErrorFromJSON = exports.RoleFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var RoleFetchErrorSuccessEnum;
(function (RoleFetchErrorSuccessEnum) {
    RoleFetchErrorSuccessEnum["False"] = "false";
})(RoleFetchErrorSuccessEnum = exports.RoleFetchErrorSuccessEnum || (exports.RoleFetchErrorSuccessEnum = {}));
function RoleFetchErrorFromJSON(json) {
    return RoleFetchErrorFromJSONTyped(json, false);
}
exports.RoleFetchErrorFromJSON = RoleFetchErrorFromJSON;
function RoleFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.RoleFetchErrorFromJSONTyped = RoleFetchErrorFromJSONTyped;
function RoleFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.RoleFetchErrorToJSON = RoleFetchErrorToJSON;
