"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersListFilterAllOfToJSON = exports.OrdersListFilterAllOfFromJSONTyped = exports.OrdersListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrdersListFilterAllOfFromJSON(json) {
    return OrdersListFilterAllOfFromJSONTyped(json, false);
}
exports.OrdersListFilterAllOfFromJSON = OrdersListFilterAllOfFromJSON;
function OrdersListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'requireShippingLabels': !runtime_1.exists(json, 'requireShippingLabels') ? undefined : json['requireShippingLabels'],
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : json['timestamp'],
    };
}
exports.OrdersListFilterAllOfFromJSONTyped = OrdersListFilterAllOfFromJSONTyped;
function OrdersListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'email': value.email,
        'requireShippingLabels': value.requireShippingLabels,
        'timestamp': value.timestamp,
    };
}
exports.OrdersListFilterAllOfToJSON = OrdersListFilterAllOfToJSON;
