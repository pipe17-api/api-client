/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Refund status
 * @export
 * @enum {string}
 */
export declare enum RefundStatus {
    Pending = "pending",
    Settled = "settled"
}
export declare function RefundStatusFromJSON(json: any): RefundStatus;
export declare function RefundStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundStatus;
export declare function RefundStatusToJSON(value?: RefundStatus | null): any;
