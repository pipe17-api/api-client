"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleUpdateResponseToJSON = exports.InventoryRuleUpdateResponseFromJSONTyped = exports.InventoryRuleUpdateResponseFromJSON = exports.InventoryRuleUpdateResponseCodeEnum = exports.InventoryRuleUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var InventoryRuleUpdateResponseSuccessEnum;
(function (InventoryRuleUpdateResponseSuccessEnum) {
    InventoryRuleUpdateResponseSuccessEnum["True"] = "true";
})(InventoryRuleUpdateResponseSuccessEnum = exports.InventoryRuleUpdateResponseSuccessEnum || (exports.InventoryRuleUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryRuleUpdateResponseCodeEnum;
(function (InventoryRuleUpdateResponseCodeEnum) {
    InventoryRuleUpdateResponseCodeEnum[InventoryRuleUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryRuleUpdateResponseCodeEnum[InventoryRuleUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryRuleUpdateResponseCodeEnum[InventoryRuleUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryRuleUpdateResponseCodeEnum = exports.InventoryRuleUpdateResponseCodeEnum || (exports.InventoryRuleUpdateResponseCodeEnum = {}));
function InventoryRuleUpdateResponseFromJSON(json) {
    return InventoryRuleUpdateResponseFromJSONTyped(json, false);
}
exports.InventoryRuleUpdateResponseFromJSON = InventoryRuleUpdateResponseFromJSON;
function InventoryRuleUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.InventoryRuleUpdateResponseFromJSONTyped = InventoryRuleUpdateResponseFromJSONTyped;
function InventoryRuleUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.InventoryRuleUpdateResponseToJSON = InventoryRuleUpdateResponseToJSON;
