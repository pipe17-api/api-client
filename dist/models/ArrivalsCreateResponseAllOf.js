"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsCreateResponseAllOfToJSON = exports.ArrivalsCreateResponseAllOfFromJSONTyped = exports.ArrivalsCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalsCreateResponseAllOfFromJSON(json) {
    return ArrivalsCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ArrivalsCreateResponseAllOfFromJSON = ArrivalsCreateResponseAllOfFromJSON;
function ArrivalsCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrivals': !runtime_1.exists(json, 'arrivals') ? undefined : (json['arrivals'].map(_1.ArrivalCreateResultFromJSON)),
    };
}
exports.ArrivalsCreateResponseAllOfFromJSONTyped = ArrivalsCreateResponseAllOfFromJSONTyped;
function ArrivalsCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrivals': value.arrivals === undefined ? undefined : (value.arrivals.map(_1.ArrivalCreateResultToJSON)),
    };
}
exports.ArrivalsCreateResponseAllOfToJSON = ArrivalsCreateResponseAllOfToJSON;
