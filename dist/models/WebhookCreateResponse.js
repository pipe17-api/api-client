"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookCreateResponseToJSON = exports.WebhookCreateResponseFromJSONTyped = exports.WebhookCreateResponseFromJSON = exports.WebhookCreateResponseCodeEnum = exports.WebhookCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var WebhookCreateResponseSuccessEnum;
(function (WebhookCreateResponseSuccessEnum) {
    WebhookCreateResponseSuccessEnum["True"] = "true";
})(WebhookCreateResponseSuccessEnum = exports.WebhookCreateResponseSuccessEnum || (exports.WebhookCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var WebhookCreateResponseCodeEnum;
(function (WebhookCreateResponseCodeEnum) {
    WebhookCreateResponseCodeEnum[WebhookCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    WebhookCreateResponseCodeEnum[WebhookCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    WebhookCreateResponseCodeEnum[WebhookCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(WebhookCreateResponseCodeEnum = exports.WebhookCreateResponseCodeEnum || (exports.WebhookCreateResponseCodeEnum = {}));
function WebhookCreateResponseFromJSON(json) {
    return WebhookCreateResponseFromJSONTyped(json, false);
}
exports.WebhookCreateResponseFromJSON = WebhookCreateResponseFromJSON;
function WebhookCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.WebhookFromJSON(json['webhook']),
    };
}
exports.WebhookCreateResponseFromJSONTyped = WebhookCreateResponseFromJSONTyped;
function WebhookCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'webhook': _1.WebhookToJSON(value.webhook),
    };
}
exports.WebhookCreateResponseToJSON = WebhookCreateResponseToJSON;
