/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseUpdateData } from './';
/**
 *
 * @export
 * @interface PurchaseUpdateErrorAllOf
 */
export interface PurchaseUpdateErrorAllOf {
    /**
     *
     * @type {PurchaseUpdateData}
     * @memberof PurchaseUpdateErrorAllOf
     */
    purchase?: PurchaseUpdateData;
}
export declare function PurchaseUpdateErrorAllOfFromJSON(json: any): PurchaseUpdateErrorAllOf;
export declare function PurchaseUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseUpdateErrorAllOf;
export declare function PurchaseUpdateErrorAllOfToJSON(value?: PurchaseUpdateErrorAllOf | null): any;
