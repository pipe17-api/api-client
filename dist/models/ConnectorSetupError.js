"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSetupErrorToJSON = exports.ConnectorSetupErrorFromJSONTyped = exports.ConnectorSetupErrorFromJSON = exports.ConnectorSetupErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorSetupErrorSuccessEnum;
(function (ConnectorSetupErrorSuccessEnum) {
    ConnectorSetupErrorSuccessEnum["False"] = "false";
})(ConnectorSetupErrorSuccessEnum = exports.ConnectorSetupErrorSuccessEnum || (exports.ConnectorSetupErrorSuccessEnum = {}));
function ConnectorSetupErrorFromJSON(json) {
    return ConnectorSetupErrorFromJSONTyped(json, false);
}
exports.ConnectorSetupErrorFromJSON = ConnectorSetupErrorFromJSON;
function ConnectorSetupErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'options': !runtime_1.exists(json, 'options') ? undefined : _1.ConnectorSetupDataFromJSON(json['options']),
    };
}
exports.ConnectorSetupErrorFromJSONTyped = ConnectorSetupErrorFromJSONTyped;
function ConnectorSetupErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'options': _1.ConnectorSetupDataToJSON(value.options),
    };
}
exports.ConnectorSetupErrorToJSON = ConnectorSetupErrorToJSON;
