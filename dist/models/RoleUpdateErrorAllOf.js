"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleUpdateErrorAllOfToJSON = exports.RoleUpdateErrorAllOfFromJSONTyped = exports.RoleUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleUpdateErrorAllOfFromJSON(json) {
    return RoleUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.RoleUpdateErrorAllOfFromJSON = RoleUpdateErrorAllOfFromJSON;
function RoleUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleUpdateDataFromJSON(json['role']),
    };
}
exports.RoleUpdateErrorAllOfFromJSONTyped = RoleUpdateErrorAllOfFromJSONTyped;
function RoleUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'role': _1.RoleUpdateDataToJSON(value.role),
    };
}
exports.RoleUpdateErrorAllOfToJSON = RoleUpdateErrorAllOfToJSON;
