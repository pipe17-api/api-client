/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location } from './';
/**
 *
 * @export
 * @interface LocationFetchResponse
 */
export interface LocationFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LocationFetchResponse
     */
    success: LocationFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationFetchResponse
     */
    code: LocationFetchResponseCodeEnum;
    /**
     *
     * @type {Location}
     * @memberof LocationFetchResponse
     */
    location?: Location;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LocationFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LocationFetchResponseFromJSON(json: any): LocationFetchResponse;
export declare function LocationFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationFetchResponse;
export declare function LocationFetchResponseToJSON(value?: LocationFetchResponse | null): any;
