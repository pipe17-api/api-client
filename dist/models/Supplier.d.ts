/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierAddress, SupplierContact } from './';
/**
 *
 * @export
 * @interface Supplier
 */
export interface Supplier {
    /**
     * Supplier ID
     * @type {string}
     * @memberof Supplier
     */
    supplierId?: string;
    /**
     * Supplier Name
     * @type {string}
     * @memberof Supplier
     */
    name: string;
    /**
     *
     * @type {SupplierAddress}
     * @memberof Supplier
     */
    address: SupplierAddress;
    /**
     *
     * @type {Array<SupplierContact>}
     * @memberof Supplier
     */
    contacts?: Array<SupplierContact>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Supplier
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Supplier
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Supplier
     */
    readonly orgKey?: string;
}
export declare function SupplierFromJSON(json: any): Supplier;
export declare function SupplierFromJSONTyped(json: any, ignoreDiscriminator: boolean): Supplier;
export declare function SupplierToJSON(value?: Supplier | null): any;
