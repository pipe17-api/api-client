/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorSetupRequest
 */
export interface ConnectorSetupRequest {
    /**
     * Public connector indicator
     * @type {boolean}
     * @memberof ConnectorSetupRequest
     */
    isPublic?: boolean;
    /**
     * Connector rank in category (higher ranks indicate better match)
     * @type {number}
     * @memberof ConnectorSetupRequest
     */
    rank?: number;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof ConnectorSetupRequest
     */
    orgUnique?: boolean;
}
export declare function ConnectorSetupRequestFromJSON(json: any): ConnectorSetupRequest;
export declare function ConnectorSetupRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSetupRequest;
export declare function ConnectorSetupRequestToJSON(value?: ConnectorSetupRequest | null): any;
