/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderLineItem
 */
export interface OrderLineItem {
    /**
     * Item unique Id (within an order)
     * @type {string}
     * @memberof OrderLineItem
     */
    uniqueId: string;
    /**
     * Item SKU
     * @type {string}
     * @memberof OrderLineItem
     */
    sku: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof OrderLineItem
     */
    name?: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof OrderLineItem
     */
    quantity: number;
    /**
     * Item Price
     * @type {number}
     * @memberof OrderLineItem
     */
    itemPrice?: number;
    /**
     * Item Price
     * @type {number}
     * @memberof OrderLineItem
     */
    itemDiscount?: number;
    /**
     *
     * @type {number}
     * @memberof OrderLineItem
     */
    itemTax?: number;
    /**
     *
     * @type {boolean}
     * @memberof OrderLineItem
     */
    requiresShipping: boolean;
    /**
     *
     * @type {boolean}
     * @memberof OrderLineItem
     */
    taxable?: boolean;
    /**
     *
     * @type {string}
     * @memberof OrderLineItem
     */
    locationId?: string;
}
export declare function OrderLineItemFromJSON(json: any): OrderLineItem;
export declare function OrderLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderLineItem;
export declare function OrderLineItemToJSON(value?: OrderLineItem | null): any;
