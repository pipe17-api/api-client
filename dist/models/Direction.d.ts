/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Route/Mapping Direction
 * @export
 * @enum {string}
 */
export declare enum Direction {
    In = "in",
    Out = "out"
}
export declare function DirectionFromJSON(json: any): Direction;
export declare function DirectionFromJSONTyped(json: any, ignoreDiscriminator: boolean): Direction;
export declare function DirectionToJSON(value?: Direction | null): any;
