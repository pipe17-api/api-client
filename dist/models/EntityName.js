"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityNameToJSON = exports.EntityNameFromJSONTyped = exports.EntityNameFromJSON = exports.EntityName = void 0;
/**
 * Entity Name
 * @export
 * @enum {string}
 */
var EntityName;
(function (EntityName) {
    EntityName["Accounts"] = "accounts";
    EntityName["Arrivals"] = "arrivals";
    EntityName["EntityFilters"] = "entity_filters";
    EntityName["Fulfillments"] = "fulfillments";
    EntityName["Inventory"] = "inventory";
    EntityName["Labels"] = "labels";
    EntityName["Locations"] = "locations";
    EntityName["Orders"] = "orders";
    EntityName["OrderRegions"] = "order_regions";
    EntityName["OrderRoutings"] = "order_routings";
    EntityName["Products"] = "products";
    EntityName["Purchases"] = "purchases";
    EntityName["Receipts"] = "receipts";
    EntityName["Refunds"] = "refunds";
    EntityName["Returns"] = "returns";
    EntityName["Shipments"] = "shipments";
    EntityName["Suppliers"] = "suppliers";
    EntityName["Transfers"] = "transfers";
    EntityName["Trackings"] = "trackings";
})(EntityName = exports.EntityName || (exports.EntityName = {}));
function EntityNameFromJSON(json) {
    return EntityNameFromJSONTyped(json, false);
}
exports.EntityNameFromJSON = EntityNameFromJSON;
function EntityNameFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EntityNameFromJSONTyped = EntityNameFromJSONTyped;
function EntityNameToJSON(value) {
    return value;
}
exports.EntityNameToJSON = EntityNameToJSON;
