/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Transfer, TransfersListFilter } from './';
/**
 *
 * @export
 * @interface TransfersListResponse
 */
export interface TransfersListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TransfersListResponse
     */
    success: TransfersListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransfersListResponse
     */
    code: TransfersListResponseCodeEnum;
    /**
     *
     * @type {TransfersListFilter}
     * @memberof TransfersListResponse
     */
    filters?: TransfersListFilter;
    /**
     *
     * @type {Array<Transfer>}
     * @memberof TransfersListResponse
     */
    transfers: Array<Transfer>;
    /**
     *
     * @type {Pagination}
     * @memberof TransfersListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum TransfersListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TransfersListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TransfersListResponseFromJSON(json: any): TransfersListResponse;
export declare function TransfersListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersListResponse;
export declare function TransfersListResponseToJSON(value?: TransfersListResponse | null): any;
