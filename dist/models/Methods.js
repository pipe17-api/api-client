"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MethodsToJSON = exports.MethodsFromJSONTyped = exports.MethodsFromJSON = exports.MethodsConvertEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var MethodsConvertEnum;
(function (MethodsConvertEnum) {
    MethodsConvertEnum["C"] = "c";
})(MethodsConvertEnum = exports.MethodsConvertEnum || (exports.MethodsConvertEnum = {}));
function MethodsFromJSON(json) {
    return MethodsFromJSONTyped(json, false);
}
exports.MethodsFromJSON = MethodsFromJSON;
function MethodsFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'accounts': !runtime_1.exists(json, 'accounts') ? undefined : json['accounts'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : json['apikey'],
        'arrivals': !runtime_1.exists(json, 'arrivals') ? undefined : json['arrivals'],
        'connectors': !runtime_1.exists(json, 'connectors') ? undefined : json['connectors'],
        'fulfillments': !runtime_1.exists(json, 'fulfillments') ? undefined : json['fulfillments'],
        'integrations': !runtime_1.exists(json, 'integrations') ? undefined : json['integrations'],
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : json['inventory'],
        'inventoryrules': !runtime_1.exists(json, 'inventoryrules') ? undefined : json['inventoryrules'],
        'labels': !runtime_1.exists(json, 'labels') ? undefined : json['labels'],
        'locations': !runtime_1.exists(json, 'locations') ? undefined : json['locations'],
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : json['mappings'],
        'orders': !runtime_1.exists(json, 'orders') ? undefined : json['orders'],
        'organizations': !runtime_1.exists(json, 'organizations') ? undefined : json['organizations'],
        'products': !runtime_1.exists(json, 'products') ? undefined : json['products'],
        'purchases': !runtime_1.exists(json, 'purchases') ? undefined : json['purchases'],
        'receipts': !runtime_1.exists(json, 'receipts') ? undefined : json['receipts'],
        'roles': !runtime_1.exists(json, 'roles') ? undefined : json['roles'],
        'routings': !runtime_1.exists(json, 'routings') ? undefined : json['routings'],
        'shipments': !runtime_1.exists(json, 'shipments') ? undefined : json['shipments'],
        'suppliers': !runtime_1.exists(json, 'suppliers') ? undefined : json['suppliers'],
        'transfers': !runtime_1.exists(json, 'transfers') ? undefined : json['transfers'],
        'trackings': !runtime_1.exists(json, 'trackings') ? undefined : json['trackings'],
        'users': !runtime_1.exists(json, 'users') ? undefined : json['users'],
        'webhooks': !runtime_1.exists(json, 'webhooks') ? undefined : json['webhooks'],
        'convert': !runtime_1.exists(json, 'convert') ? undefined : json['convert'],
    };
}
exports.MethodsFromJSONTyped = MethodsFromJSONTyped;
function MethodsToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'accounts': value.accounts,
        'apikey': value.apikey,
        'arrivals': value.arrivals,
        'connectors': value.connectors,
        'fulfillments': value.fulfillments,
        'integrations': value.integrations,
        'inventory': value.inventory,
        'inventoryrules': value.inventoryrules,
        'labels': value.labels,
        'locations': value.locations,
        'mappings': value.mappings,
        'orders': value.orders,
        'organizations': value.organizations,
        'products': value.products,
        'purchases': value.purchases,
        'receipts': value.receipts,
        'roles': value.roles,
        'routings': value.routings,
        'shipments': value.shipments,
        'suppliers': value.suppliers,
        'transfers': value.transfers,
        'trackings': value.trackings,
        'users': value.users,
        'webhooks': value.webhooks,
        'convert': value.convert,
    };
}
exports.MethodsToJSON = MethodsToJSON;
