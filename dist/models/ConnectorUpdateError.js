"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorUpdateErrorToJSON = exports.ConnectorUpdateErrorFromJSONTyped = exports.ConnectorUpdateErrorFromJSON = exports.ConnectorUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorUpdateErrorSuccessEnum;
(function (ConnectorUpdateErrorSuccessEnum) {
    ConnectorUpdateErrorSuccessEnum["False"] = "false";
})(ConnectorUpdateErrorSuccessEnum = exports.ConnectorUpdateErrorSuccessEnum || (exports.ConnectorUpdateErrorSuccessEnum = {}));
function ConnectorUpdateErrorFromJSON(json) {
    return ConnectorUpdateErrorFromJSONTyped(json, false);
}
exports.ConnectorUpdateErrorFromJSON = ConnectorUpdateErrorFromJSON;
function ConnectorUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'connector': !runtime_1.exists(json, 'connector') ? undefined : _1.ConnectorUpdateDataFromJSON(json['connector']),
    };
}
exports.ConnectorUpdateErrorFromJSONTyped = ConnectorUpdateErrorFromJSONTyped;
function ConnectorUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'connector': _1.ConnectorUpdateDataToJSON(value.connector),
    };
}
exports.ConnectorUpdateErrorToJSON = ConnectorUpdateErrorToJSON;
