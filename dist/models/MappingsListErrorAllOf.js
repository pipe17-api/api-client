"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingsListErrorAllOfToJSON = exports.MappingsListErrorAllOfFromJSONTyped = exports.MappingsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingsListErrorAllOfFromJSON(json) {
    return MappingsListErrorAllOfFromJSONTyped(json, false);
}
exports.MappingsListErrorAllOfFromJSON = MappingsListErrorAllOfFromJSON;
function MappingsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.MappingsListFilterFromJSON(json['filters']),
    };
}
exports.MappingsListErrorAllOfFromJSONTyped = MappingsListErrorAllOfFromJSONTyped;
function MappingsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.MappingsListFilterToJSON(value.filters),
    };
}
exports.MappingsListErrorAllOfToJSON = MappingsListErrorAllOfToJSON;
