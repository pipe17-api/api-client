/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory, InventoryListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryListResponseAllOf
 */
export interface InventoryListResponseAllOf {
    /**
     *
     * @type {InventoryListFilter}
     * @memberof InventoryListResponseAllOf
     */
    filters?: InventoryListFilter;
    /**
     *
     * @type {Array<Inventory>}
     * @memberof InventoryListResponseAllOf
     */
    inventory?: Array<Inventory>;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function InventoryListResponseAllOfFromJSON(json: any): InventoryListResponseAllOf;
export declare function InventoryListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryListResponseAllOf;
export declare function InventoryListResponseAllOfToJSON(value?: InventoryListResponseAllOf | null): any;
