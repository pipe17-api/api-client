"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountCreateRequestAllOfToJSON = exports.AccountCreateRequestAllOfFromJSONTyped = exports.AccountCreateRequestAllOfFromJSON = void 0;
function AccountCreateRequestAllOfFromJSON(json) {
    return AccountCreateRequestAllOfFromJSONTyped(json, false);
}
exports.AccountCreateRequestAllOfFromJSON = AccountCreateRequestAllOfFromJSON;
function AccountCreateRequestAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'password': json['password'],
    };
}
exports.AccountCreateRequestAllOfFromJSONTyped = AccountCreateRequestAllOfFromJSONTyped;
function AccountCreateRequestAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'password': value.password,
    };
}
exports.AccountCreateRequestAllOfToJSON = AccountCreateRequestAllOfToJSON;
