"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobsDeleteFilterToJSON = exports.JobsDeleteFilterFromJSONTyped = exports.JobsDeleteFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobsDeleteFilterFromJSON(json) {
    return JobsDeleteFilterFromJSONTyped(json, false);
}
exports.JobsDeleteFilterFromJSON = JobsDeleteFilterFromJSON;
function JobsDeleteFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'jobId': !runtime_1.exists(json, 'jobId') ? undefined : json['jobId'],
        'type': !runtime_1.exists(json, 'type') ? undefined : (json['type'].map(_1.JobTypeFromJSON)),
        'subType': !runtime_1.exists(json, 'subType') ? undefined : (json['subType'].map(_1.JobSubTypeFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.JobStatusFromJSON)),
    };
}
exports.JobsDeleteFilterFromJSONTyped = JobsDeleteFilterFromJSONTyped;
function JobsDeleteFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'jobId': value.jobId,
        'type': value.type === undefined ? undefined : (value.type.map(_1.JobTypeToJSON)),
        'subType': value.subType === undefined ? undefined : (value.subType.map(_1.JobSubTypeToJSON)),
        'status': value.status === undefined ? undefined : (value.status.map(_1.JobStatusToJSON)),
    };
}
exports.JobsDeleteFilterToJSON = JobsDeleteFilterToJSON;
