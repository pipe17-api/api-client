/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryListFilter
 */
export interface InventoryListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof InventoryListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof InventoryListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof InventoryListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof InventoryListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof InventoryListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof InventoryListFilter
     */
    count?: number;
    /**
     * Inventory items by list of inventoryId
     * @type {Array<string>}
     * @memberof InventoryListFilter
     */
    inventoryId?: Array<string>;
    /**
     * Return inventory items related to certain event record by record id
     * @type {Array<string>}
     * @memberof InventoryListFilter
     */
    entityId?: Array<string>;
    /**
     * Fetch inventory by its SKU
     * @type {Array<string>}
     * @memberof InventoryListFilter
     */
    sku?: Array<string>;
    /**
     * Fetch inventory by its location Id
     * @type {Array<string>}
     * @memberof InventoryListFilter
     */
    locationId?: Array<string>;
    /**
     * Fetch soft deleted inventory items
     * @type {boolean}
     * @memberof InventoryListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof InventoryListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof InventoryListFilter
     */
    keys?: string;
    /**
     * Return inventory ledger information
     * @type {boolean}
     * @memberof InventoryListFilter
     */
    ledger?: boolean;
    /**
     * Return inventory totals across all locations
     * @type {boolean}
     * @memberof InventoryListFilter
     */
    totals?: boolean;
}
export declare function InventoryListFilterFromJSON(json: any): InventoryListFilter;
export declare function InventoryListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryListFilter;
export declare function InventoryListFilterToJSON(value?: InventoryListFilter | null): any;
