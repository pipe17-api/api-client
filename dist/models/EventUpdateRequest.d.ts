/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestStatus, EventResponseData } from './';
/**
 *
 * @export
 * @interface EventUpdateRequest
 */
export interface EventUpdateRequest {
    /**
     *
     * @type {EventRequestStatus}
     * @memberof EventUpdateRequest
     */
    status?: EventRequestStatus;
    /**
     *
     * @type {EventResponseData}
     * @memberof EventUpdateRequest
     */
    response?: EventResponseData;
    /**
     * operation duration
     * @type {number}
     * @memberof EventUpdateRequest
     */
    duration?: number;
}
export declare function EventUpdateRequestFromJSON(json: any): EventUpdateRequest;
export declare function EventUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventUpdateRequest;
export declare function EventUpdateRequestToJSON(value?: EventUpdateRequest | null): any;
