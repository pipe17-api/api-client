"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsFilterAllOfToJSON = exports.RefundsFilterAllOfFromJSONTyped = exports.RefundsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundsFilterAllOfFromJSON(json) {
    return RefundsFilterAllOfFromJSONTyped(json, false);
}
exports.RefundsFilterAllOfFromJSON = RefundsFilterAllOfFromJSON;
function RefundsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'refundId': !runtime_1.exists(json, 'refundId') ? undefined : json['refundId'],
        'extRefundId': !runtime_1.exists(json, 'extRefundId') ? undefined : json['extRefundId'],
        'extReturnId': !runtime_1.exists(json, 'extReturnId') ? undefined : json['extReturnId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'type': !runtime_1.exists(json, 'type') ? undefined : (json['type'].map(_1.RefundTypeFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.RefundStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.RefundsFilterAllOfFromJSONTyped = RefundsFilterAllOfFromJSONTyped;
function RefundsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'refundId': value.refundId,
        'extRefundId': value.extRefundId,
        'extReturnId': value.extReturnId,
        'extOrderId': value.extOrderId,
        'type': value.type === undefined ? undefined : (value.type.map(_1.RefundTypeToJSON)),
        'status': value.status === undefined ? undefined : (value.status.map(_1.RefundStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.RefundsFilterAllOfToJSON = RefundsFilterAllOfToJSON;
