"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterListResponseToJSON = exports.EntityFilterListResponseFromJSONTyped = exports.EntityFilterListResponseFromJSON = exports.EntityFilterListResponseCodeEnum = exports.EntityFilterListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntityFilterListResponseSuccessEnum;
(function (EntityFilterListResponseSuccessEnum) {
    EntityFilterListResponseSuccessEnum["True"] = "true";
})(EntityFilterListResponseSuccessEnum = exports.EntityFilterListResponseSuccessEnum || (exports.EntityFilterListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntityFilterListResponseCodeEnum;
(function (EntityFilterListResponseCodeEnum) {
    EntityFilterListResponseCodeEnum[EntityFilterListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntityFilterListResponseCodeEnum[EntityFilterListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntityFilterListResponseCodeEnum[EntityFilterListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntityFilterListResponseCodeEnum = exports.EntityFilterListResponseCodeEnum || (exports.EntityFilterListResponseCodeEnum = {}));
function EntityFilterListResponseFromJSON(json) {
    return EntityFilterListResponseFromJSONTyped(json, false);
}
exports.EntityFilterListResponseFromJSON = EntityFilterListResponseFromJSON;
function EntityFilterListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntityFiltersListFilterFromJSON(json['filters']),
        'entityFilters': !runtime_1.exists(json, 'entityFilters') ? undefined : (json['entityFilters'].map(_1.EntityFilterFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.EntityFilterListResponseFromJSONTyped = EntityFilterListResponseFromJSONTyped;
function EntityFilterListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.EntityFiltersListFilterToJSON(value.filters),
        'entityFilters': value.entityFilters === undefined ? undefined : (value.entityFilters.map(_1.EntityFilterToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.EntityFilterListResponseToJSON = EntityFilterListResponseToJSON;
