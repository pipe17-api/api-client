/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationConnection, IntegrationEntity, IntegrationSettings, IntegrationStatus } from './';
/**
 *
 * @export
 * @interface IntegrationUpdateData
 */
export interface IntegrationUpdateData {
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationUpdateData
     */
    status?: IntegrationStatus;
    /**
     * Update integration api key
     * @type {boolean}
     * @memberof IntegrationUpdateData
     */
    apikey?: boolean;
    /**
     * Integration Name
     * @type {string}
     * @memberof IntegrationUpdateData
     */
    integrationName?: string;
    /**
     *
     * @type {Array<IntegrationEntity>}
     * @memberof IntegrationUpdateData
     */
    entities?: Array<IntegrationEntity>;
    /**
     *
     * @type {IntegrationSettings}
     * @memberof IntegrationUpdateData
     */
    settings?: IntegrationSettings;
    /**
     *
     * @type {IntegrationConnection}
     * @memberof IntegrationUpdateData
     */
    connection?: IntegrationConnection;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationUpdateData
     */
    errorText?: string | null;
}
export declare function IntegrationUpdateDataFromJSON(json: any): IntegrationUpdateData;
export declare function IntegrationUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationUpdateData;
export declare function IntegrationUpdateDataToJSON(value?: IntegrationUpdateData | null): any;
