/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrdersDeleteFilter } from './';
/**
 *
 * @export
 * @interface OrdersDeleteErrorAllOf
 */
export interface OrdersDeleteErrorAllOf {
    /**
     *
     * @type {OrdersDeleteFilter}
     * @memberof OrdersDeleteErrorAllOf
     */
    filters?: OrdersDeleteFilter;
}
export declare function OrdersDeleteErrorAllOfFromJSON(json: any): OrdersDeleteErrorAllOf;
export declare function OrdersDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersDeleteErrorAllOf;
export declare function OrdersDeleteErrorAllOfToJSON(value?: OrdersDeleteErrorAllOf | null): any;
