"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderCreateDataToJSON = exports.OrderCreateDataFromJSONTyped = exports.OrderCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrderCreateDataFromJSON(json) {
    return OrderCreateDataFromJSONTyped(json, false);
}
exports.OrderCreateDataFromJSON = OrderCreateDataFromJSON;
function OrderCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.OrderStatusFromJSON(json['status']),
        'extOrderId': json['extOrderId'],
        'extOrderUrl': !runtime_1.exists(json, 'extOrderUrl') ? undefined : json['extOrderUrl'],
        'orderSource': !runtime_1.exists(json, 'orderSource') ? undefined : json['orderSource'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.OrderLineItemFromJSON)),
        'subTotalPrice': !runtime_1.exists(json, 'subTotalPrice') ? undefined : json['subTotalPrice'],
        'orderDiscount': !runtime_1.exists(json, 'orderDiscount') ? undefined : json['orderDiscount'],
        'orderTax': !runtime_1.exists(json, 'orderTax') ? undefined : json['orderTax'],
        'totalPrice': !runtime_1.exists(json, 'totalPrice') ? undefined : json['totalPrice'],
        'orderNote': !runtime_1.exists(json, 'orderNote') ? undefined : json['orderNote'],
        'giftNote': !runtime_1.exists(json, 'giftNote') ? undefined : json['giftNote'],
        'shippingAddress': !runtime_1.exists(json, 'shippingAddress') ? undefined : _1.AddressFromJSON(json['shippingAddress']),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (new Date(json['shipByDate'])),
        'shipAfterDate': !runtime_1.exists(json, 'shipAfterDate') ? undefined : (new Date(json['shipAfterDate'])),
        'expectedDeliveryDate': !runtime_1.exists(json, 'expectedDeliveryDate') ? undefined : (new Date(json['expectedDeliveryDate'])),
        'shippingNote': !runtime_1.exists(json, 'shippingNote') ? undefined : json['shippingNote'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'billingAddress': !runtime_1.exists(json, 'billingAddress') ? undefined : _1.AddressFromJSON(json['billingAddress']),
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (new Date(json['timestamp'])),
        'shippingCode': !runtime_1.exists(json, 'shippingCode') ? undefined : json['shippingCode'],
        'customer': !runtime_1.exists(json, 'customer') ? undefined : _1.OrderCustomerFromJSON(json['customer']),
        'paymentStatus': !runtime_1.exists(json, 'paymentStatus') ? undefined : json['paymentStatus'],
        'fulfillmentStatus': !runtime_1.exists(json, 'fulfillmentStatus') ? undefined : json['fulfillmentStatus'],
        'extOrderApiId': !runtime_1.exists(json, 'extOrderApiId') ? undefined : json['extOrderApiId'],
        'extOrderCreatedAt': !runtime_1.exists(json, 'extOrderCreatedAt') ? undefined : (new Date(json['extOrderCreatedAt'])),
        'extOrderUpdatedAt': !runtime_1.exists(json, 'extOrderUpdatedAt') ? undefined : (new Date(json['extOrderUpdatedAt'])),
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'shippingPrice': !runtime_1.exists(json, 'shippingPrice') ? undefined : json['shippingPrice'],
        'requireShippingLabels': !runtime_1.exists(json, 'requireShippingLabels') ? undefined : json['requireShippingLabels'],
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'tags': !runtime_1.exists(json, 'tags') ? undefined : json['tags'],
        'payments': !runtime_1.exists(json, 'payments') ? undefined : (json['payments'].map(_1.PaymentFromJSON)),
    };
}
exports.OrderCreateDataFromJSONTyped = OrderCreateDataFromJSONTyped;
function OrderCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.OrderStatusToJSON(value.status),
        'extOrderId': value.extOrderId,
        'extOrderUrl': value.extOrderUrl,
        'orderSource': value.orderSource,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.OrderLineItemToJSON)),
        'subTotalPrice': value.subTotalPrice,
        'orderDiscount': value.orderDiscount,
        'orderTax': value.orderTax,
        'totalPrice': value.totalPrice,
        'orderNote': value.orderNote,
        'giftNote': value.giftNote,
        'shippingAddress': _1.AddressToJSON(value.shippingAddress),
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'shipByDate': value.shipByDate === undefined ? undefined : (new Date(value.shipByDate).toISOString()),
        'shipAfterDate': value.shipAfterDate === undefined ? undefined : (new Date(value.shipAfterDate).toISOString()),
        'expectedDeliveryDate': value.expectedDeliveryDate === undefined ? undefined : (new Date(value.expectedDeliveryDate).toISOString()),
        'shippingNote': value.shippingNote,
        'incoterms': value.incoterms,
        'billingAddress': _1.AddressToJSON(value.billingAddress),
        'timestamp': value.timestamp === undefined ? undefined : (new Date(value.timestamp).toISOString()),
        'shippingCode': value.shippingCode,
        'customer': _1.OrderCustomerToJSON(value.customer),
        'paymentStatus': value.paymentStatus,
        'fulfillmentStatus': value.fulfillmentStatus,
        'extOrderApiId': value.extOrderApiId,
        'extOrderCreatedAt': value.extOrderCreatedAt === undefined ? undefined : (new Date(value.extOrderCreatedAt).toISOString()),
        'extOrderUpdatedAt': value.extOrderUpdatedAt === undefined ? undefined : (new Date(value.extOrderUpdatedAt).toISOString()),
        'locationId': value.locationId,
        'shippingPrice': value.shippingPrice,
        'requireShippingLabels': value.requireShippingLabels,
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'tags': value.tags,
        'payments': value.payments === undefined ? undefined : (value.payments.map(_1.PaymentToJSON)),
    };
}
exports.OrderCreateDataToJSON = OrderCreateDataToJSON;
