"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsDeleteResponseAllOfToJSON = exports.LocationsDeleteResponseAllOfFromJSONTyped = exports.LocationsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationsDeleteResponseAllOfFromJSON(json) {
    return LocationsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.LocationsDeleteResponseAllOfFromJSON = LocationsDeleteResponseAllOfFromJSON;
function LocationsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsDeleteFilterFromJSON(json['filters']),
        'locations': !runtime_1.exists(json, 'locations') ? undefined : (json['locations'].map(_1.LocationFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.LocationsDeleteResponseAllOfFromJSONTyped = LocationsDeleteResponseAllOfFromJSONTyped;
function LocationsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LocationsDeleteFilterToJSON(value.filters),
        'locations': value.locations === undefined ? undefined : (value.locations.map(_1.LocationToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.LocationsDeleteResponseAllOfToJSON = LocationsDeleteResponseAllOfToJSON;
