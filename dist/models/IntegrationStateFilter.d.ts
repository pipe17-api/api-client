/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationStateFilter
 */
export interface IntegrationStateFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof IntegrationStateFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof IntegrationStateFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof IntegrationStateFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof IntegrationStateFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof IntegrationStateFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof IntegrationStateFilter
     */
    count?: number;
}
export declare function IntegrationStateFilterFromJSON(json: any): IntegrationStateFilter;
export declare function IntegrationStateFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateFilter;
export declare function IntegrationStateFilterToJSON(value?: IntegrationStateFilter | null): any;
