/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntityFilterCreateData
 */
export interface EntityFilterCreateData {
    /**
     * Filter name
     * @type {string}
     * @memberof EntityFilterCreateData
     */
    name: string;
    /**
     * Filter query object
     * @type {object}
     * @memberof EntityFilterCreateData
     */
    query: object;
    /**
     *
     * @type {EntityName}
     * @memberof EntityFilterCreateData
     */
    entity: EntityName;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof EntityFilterCreateData
     */
    isPublic?: boolean;
    /**
     *
     * @type {string}
     * @memberof EntityFilterCreateData
     */
    originId?: string;
}
export declare function EntityFilterCreateDataFromJSON(json: any): EntityFilterCreateData;
export declare function EntityFilterCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterCreateData;
export declare function EntityFilterCreateDataToJSON(value?: EntityFilterCreateData | null): any;
