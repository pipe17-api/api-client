"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryCreateResponseToJSON = exports.InventoryCreateResponseFromJSONTyped = exports.InventoryCreateResponseFromJSON = exports.InventoryCreateResponseCodeEnum = exports.InventoryCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryCreateResponseSuccessEnum;
(function (InventoryCreateResponseSuccessEnum) {
    InventoryCreateResponseSuccessEnum["True"] = "true";
})(InventoryCreateResponseSuccessEnum = exports.InventoryCreateResponseSuccessEnum || (exports.InventoryCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryCreateResponseCodeEnum;
(function (InventoryCreateResponseCodeEnum) {
    InventoryCreateResponseCodeEnum[InventoryCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryCreateResponseCodeEnum[InventoryCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryCreateResponseCodeEnum[InventoryCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryCreateResponseCodeEnum = exports.InventoryCreateResponseCodeEnum || (exports.InventoryCreateResponseCodeEnum = {}));
function InventoryCreateResponseFromJSON(json) {
    return InventoryCreateResponseFromJSONTyped(json, false);
}
exports.InventoryCreateResponseFromJSON = InventoryCreateResponseFromJSON;
function InventoryCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryCreateResultFromJSON)),
    };
}
exports.InventoryCreateResponseFromJSONTyped = InventoryCreateResponseFromJSONTyped;
function InventoryCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryCreateResultToJSON)),
    };
}
exports.InventoryCreateResponseToJSON = InventoryCreateResponseToJSON;
