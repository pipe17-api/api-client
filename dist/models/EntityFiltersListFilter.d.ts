/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntityFiltersListFilter
 */
export interface EntityFiltersListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof EntityFiltersListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof EntityFiltersListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof EntityFiltersListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof EntityFiltersListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof EntityFiltersListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof EntityFiltersListFilter
     */
    count?: number;
    /**
     * Fetch by list of filterId
     * @type {Array<string>}
     * @memberof EntityFiltersListFilter
     */
    filterId?: Array<string>;
    /**
     * Fetch by list of entity types
     * @type {Array<EntityName>}
     * @memberof EntityFiltersListFilter
     */
    entity?: Array<EntityName>;
    /**
     * Fetch by list of filter names
     * @type {Array<string>}
     * @memberof EntityFiltersListFilter
     */
    name?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof EntityFiltersListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof EntityFiltersListFilter
     */
    keys?: string;
}
export declare function EntityFiltersListFilterFromJSON(json: any): EntityFiltersListFilter;
export declare function EntityFiltersListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFiltersListFilter;
export declare function EntityFiltersListFilterToJSON(value?: EntityFiltersListFilter | null): any;
