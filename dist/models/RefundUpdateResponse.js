"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundUpdateResponseToJSON = exports.RefundUpdateResponseFromJSONTyped = exports.RefundUpdateResponseFromJSON = exports.RefundUpdateResponseCodeEnum = exports.RefundUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var RefundUpdateResponseSuccessEnum;
(function (RefundUpdateResponseSuccessEnum) {
    RefundUpdateResponseSuccessEnum["True"] = "true";
})(RefundUpdateResponseSuccessEnum = exports.RefundUpdateResponseSuccessEnum || (exports.RefundUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RefundUpdateResponseCodeEnum;
(function (RefundUpdateResponseCodeEnum) {
    RefundUpdateResponseCodeEnum[RefundUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RefundUpdateResponseCodeEnum[RefundUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RefundUpdateResponseCodeEnum[RefundUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RefundUpdateResponseCodeEnum = exports.RefundUpdateResponseCodeEnum || (exports.RefundUpdateResponseCodeEnum = {}));
function RefundUpdateResponseFromJSON(json) {
    return RefundUpdateResponseFromJSONTyped(json, false);
}
exports.RefundUpdateResponseFromJSON = RefundUpdateResponseFromJSON;
function RefundUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.RefundUpdateResponseFromJSONTyped = RefundUpdateResponseFromJSONTyped;
function RefundUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.RefundUpdateResponseToJSON = RefundUpdateResponseToJSON;
