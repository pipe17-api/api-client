/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrganizationUpdateAddress
 */
export interface OrganizationUpdateAddress {
    /**
     * First Name
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    firstName?: string;
    /**
     * Last Name
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    lastName?: string;
    /**
     * Company
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    city?: string;
    /**
     * State or Province
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    zipCodeOrPostalCode?: string;
    /**
     * Country
     * @type {string}
     * @memberof OrganizationUpdateAddress
     */
    country?: string;
}
export declare function OrganizationUpdateAddressFromJSON(json: any): OrganizationUpdateAddress;
export declare function OrganizationUpdateAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationUpdateAddress;
export declare function OrganizationUpdateAddressToJSON(value?: OrganizationUpdateAddress | null): any;
