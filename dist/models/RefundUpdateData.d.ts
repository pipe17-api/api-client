/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus } from './';
/**
 *
 * @export
 * @interface RefundUpdateData
 */
export interface RefundUpdateData {
    /**
     *
     * @type {RefundStatus}
     * @memberof RefundUpdateData
     */
    status?: RefundStatus;
    /**
     * Refund amount
     * @type {number}
     * @memberof RefundUpdateData
     */
    amount?: number;
    /**
     * Credit issued to customer
     * @type {number}
     * @memberof RefundUpdateData
     */
    creditIssued?: number;
    /**
     * Credit spent by customer
     * @type {number}
     * @memberof RefundUpdateData
     */
    creditSpent?: number;
    /**
     * Merchant invoice amount
     * @type {number}
     * @memberof RefundUpdateData
     */
    merchantInvoiceAmount?: number;
    /**
     * Customer invoice amount
     * @type {number}
     * @memberof RefundUpdateData
     */
    customerInvoiceAmount?: number;
}
export declare function RefundUpdateDataFromJSON(json: any): RefundUpdateData;
export declare function RefundUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundUpdateData;
export declare function RefundUpdateDataToJSON(value?: RefundUpdateData | null): any;
