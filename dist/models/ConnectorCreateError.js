"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorCreateErrorToJSON = exports.ConnectorCreateErrorFromJSONTyped = exports.ConnectorCreateErrorFromJSON = exports.ConnectorCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorCreateErrorSuccessEnum;
(function (ConnectorCreateErrorSuccessEnum) {
    ConnectorCreateErrorSuccessEnum["False"] = "false";
})(ConnectorCreateErrorSuccessEnum = exports.ConnectorCreateErrorSuccessEnum || (exports.ConnectorCreateErrorSuccessEnum = {}));
function ConnectorCreateErrorFromJSON(json) {
    return ConnectorCreateErrorFromJSONTyped(json, false);
}
exports.ConnectorCreateErrorFromJSON = ConnectorCreateErrorFromJSON;
function ConnectorCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'connector': !runtime_1.exists(json, 'connector') ? undefined : _1.ConnectorCreateDataFromJSON(json['connector']),
    };
}
exports.ConnectorCreateErrorFromJSONTyped = ConnectorCreateErrorFromJSONTyped;
function ConnectorCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'connector': _1.ConnectorCreateDataToJSON(value.connector),
    };
}
exports.ConnectorCreateErrorToJSON = ConnectorCreateErrorToJSON;
