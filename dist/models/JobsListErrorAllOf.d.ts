/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobsListFilter } from './';
/**
 *
 * @export
 * @interface JobsListErrorAllOf
 */
export interface JobsListErrorAllOf {
    /**
     *
     * @type {JobsListFilter}
     * @memberof JobsListErrorAllOf
     */
    filters?: JobsListFilter;
}
export declare function JobsListErrorAllOfFromJSON(json: any): JobsListErrorAllOf;
export declare function JobsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsListErrorAllOf;
export declare function JobsListErrorAllOfToJSON(value?: JobsListErrorAllOf | null): any;
