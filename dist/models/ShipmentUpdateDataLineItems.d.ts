/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ShipmentUpdateDataLineItems
 */
export interface ShipmentUpdateDataLineItems {
    /**
     * Item Shipped Quantity
     * @type {number}
     * @memberof ShipmentUpdateDataLineItems
     */
    shippedQuantity?: number;
    /**
     * Item SKU
     * @type {string}
     * @memberof ShipmentUpdateDataLineItems
     */
    sku?: string;
}
export declare function ShipmentUpdateDataLineItemsFromJSON(json: any): ShipmentUpdateDataLineItems;
export declare function ShipmentUpdateDataLineItemsFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentUpdateDataLineItems;
export declare function ShipmentUpdateDataLineItemsToJSON(value?: ShipmentUpdateDataLineItems | null): any;
