/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorsListFilter
 */
export interface ConnectorsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ConnectorsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ConnectorsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ConnectorsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ConnectorsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ConnectorsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ConnectorsListFilter
     */
    count?: number;
    /**
     * Retrieve connectors by list of connectorId
     * @type {Array<string>}
     * @memberof ConnectorsListFilter
     */
    connectorId?: Array<string>;
    /**
     * Retrieve connector by list of connector names
     * @type {Array<string>}
     * @memberof ConnectorsListFilter
     */
    connectorName?: Array<string>;
    /**
     * Retrieve connector by list of connector types
     * @type {Array<string>}
     * @memberof ConnectorsListFilter
     */
    connectorType?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof ConnectorsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ConnectorsListFilter
     */
    keys?: string;
}
export declare function ConnectorsListFilterFromJSON(json: any): ConnectorsListFilter;
export declare function ConnectorsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsListFilter;
export declare function ConnectorsListFilterToJSON(value?: ConnectorsListFilter | null): any;
