/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Event } from './';
/**
 *
 * @export
 * @interface EventCreateError
 */
export interface EventCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EventCreateError
     */
    success: EventCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EventCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EventCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Event}
     * @memberof EventCreateError
     */
    event?: Event;
}
/**
* @export
* @enum {string}
*/
export declare enum EventCreateErrorSuccessEnum {
    False = "false"
}
export declare function EventCreateErrorFromJSON(json: any): EventCreateError;
export declare function EventCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventCreateError;
export declare function EventCreateErrorToJSON(value?: EventCreateError | null): any;
