"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterCreateErrorAllOfToJSON = exports.ExceptionFilterCreateErrorAllOfFromJSONTyped = exports.ExceptionFilterCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFilterCreateErrorAllOfFromJSON(json) {
    return ExceptionFilterCreateErrorAllOfFromJSONTyped(json, false);
}
exports.ExceptionFilterCreateErrorAllOfFromJSON = ExceptionFilterCreateErrorAllOfFromJSON;
function ExceptionFilterCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionFilter': !runtime_1.exists(json, 'exceptionFilter') ? undefined : _1.ExceptionFilterCreateDataFromJSON(json['exceptionFilter']),
    };
}
exports.ExceptionFilterCreateErrorAllOfFromJSONTyped = ExceptionFilterCreateErrorAllOfFromJSONTyped;
function ExceptionFilterCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionFilter': _1.ExceptionFilterCreateDataToJSON(value.exceptionFilter),
    };
}
exports.ExceptionFilterCreateErrorAllOfToJSON = ExceptionFilterCreateErrorAllOfToJSON;
