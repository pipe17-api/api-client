/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Transfer } from './';
/**
 *
 * @export
 * @interface TransferFetchResponse
 */
export interface TransferFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TransferFetchResponse
     */
    success: TransferFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransferFetchResponse
     */
    code: TransferFetchResponseCodeEnum;
    /**
     *
     * @type {Transfer}
     * @memberof TransferFetchResponse
     */
    transfer?: Transfer;
}
/**
* @export
* @enum {string}
*/
export declare enum TransferFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TransferFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TransferFetchResponseFromJSON(json: any): TransferFetchResponse;
export declare function TransferFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferFetchResponse;
export declare function TransferFetchResponseToJSON(value?: TransferFetchResponse | null): any;
