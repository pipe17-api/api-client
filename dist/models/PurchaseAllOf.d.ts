/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface PurchaseAllOf
 */
export interface PurchaseAllOf {
    /**
     * Purchase ID
     * @type {string}
     * @memberof PurchaseAllOf
     */
    purchaseId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof PurchaseAllOf
     */
    integration?: string;
}
export declare function PurchaseAllOfFromJSON(json: any): PurchaseAllOf;
export declare function PurchaseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseAllOf;
export declare function PurchaseAllOfToJSON(value?: PurchaseAllOf | null): any;
