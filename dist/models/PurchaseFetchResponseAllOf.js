"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseFetchResponseAllOfToJSON = exports.PurchaseFetchResponseAllOfFromJSONTyped = exports.PurchaseFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchaseFetchResponseAllOfFromJSON(json) {
    return PurchaseFetchResponseAllOfFromJSONTyped(json, false);
}
exports.PurchaseFetchResponseAllOfFromJSON = PurchaseFetchResponseAllOfFromJSON;
function PurchaseFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'purchase': !runtime_1.exists(json, 'purchase') ? undefined : _1.PurchaseFromJSON(json['purchase']),
    };
}
exports.PurchaseFetchResponseAllOfFromJSONTyped = PurchaseFetchResponseAllOfFromJSONTyped;
function PurchaseFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'purchase': _1.PurchaseToJSON(value.purchase),
    };
}
exports.PurchaseFetchResponseAllOfToJSON = PurchaseFetchResponseAllOfToJSON;
