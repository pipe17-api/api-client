/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundsFilter } from './';
/**
 *
 * @export
 * @interface RefundsListErrorAllOf
 */
export interface RefundsListErrorAllOf {
    /**
     *
     * @type {RefundsFilter}
     * @memberof RefundsListErrorAllOf
     */
    filters?: RefundsFilter;
}
export declare function RefundsListErrorAllOfFromJSON(json: any): RefundsListErrorAllOf;
export declare function RefundsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsListErrorAllOf;
export declare function RefundsListErrorAllOfToJSON(value?: RefundsListErrorAllOf | null): any;
