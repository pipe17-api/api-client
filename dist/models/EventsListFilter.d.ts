/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestSource, EventRequestStatus } from './';
/**
 *
 * @export
 * @interface EventsListFilter
 */
export interface EventsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof EventsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof EventsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof EventsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof EventsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof EventsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof EventsListFilter
     */
    count?: number;
    /**
     * Fetch by list of eventId
     * @type {Array<string>}
     * @memberof EventsListFilter
     */
    eventId?: Array<string>;
    /**
     * Fetch by list of event sources
     * @type {Array<EventRequestSource>}
     * @memberof EventsListFilter
     */
    source?: Array<EventRequestSource>;
    /**
     * Fetch by list of event statuses
     * @type {Array<EventRequestStatus>}
     * @memberof EventsListFilter
     */
    status?: Array<EventRequestStatus>;
    /**
     * Fetch by list of requests
     * @type {Array<string>}
     * @memberof EventsListFilter
     */
    requestId?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof EventsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof EventsListFilter
     */
    keys?: string;
}
export declare function EventsListFilterFromJSON(json: any): EventsListFilter;
export declare function EventsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsListFilter;
export declare function EventsListFilterToJSON(value?: EventsListFilter | null): any;
