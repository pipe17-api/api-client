/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Receipt } from './';
/**
 *
 * @export
 * @interface ReceiptCreateResult
 */
export interface ReceiptCreateResult {
    /**
     *
     * @type {string}
     * @memberof ReceiptCreateResult
     */
    status?: ReceiptCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof ReceiptCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof ReceiptCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Receipt}
     * @memberof ReceiptCreateResult
     */
    receipt?: Receipt;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function ReceiptCreateResultFromJSON(json: any): ReceiptCreateResult;
export declare function ReceiptCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptCreateResult;
export declare function ReceiptCreateResultToJSON(value?: ReceiptCreateResult | null): any;
