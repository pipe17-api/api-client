"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierAllOfToJSON = exports.SupplierAllOfFromJSONTyped = exports.SupplierAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function SupplierAllOfFromJSON(json) {
    return SupplierAllOfFromJSONTyped(json, false);
}
exports.SupplierAllOfFromJSON = SupplierAllOfFromJSON;
function SupplierAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'supplierId': !runtime_1.exists(json, 'supplierId') ? undefined : json['supplierId'],
    };
}
exports.SupplierAllOfFromJSONTyped = SupplierAllOfFromJSONTyped;
function SupplierAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'supplierId': value.supplierId,
    };
}
exports.SupplierAllOfToJSON = SupplierAllOfToJSON;
