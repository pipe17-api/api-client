/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierAddress, SupplierContact } from './';
/**
 *
 * @export
 * @interface SupplierCreateRequest
 */
export interface SupplierCreateRequest {
    /**
     * Supplier Name
     * @type {string}
     * @memberof SupplierCreateRequest
     */
    name: string;
    /**
     *
     * @type {SupplierAddress}
     * @memberof SupplierCreateRequest
     */
    address: SupplierAddress;
    /**
     *
     * @type {Array<SupplierContact>}
     * @memberof SupplierCreateRequest
     */
    contacts?: Array<SupplierContact>;
}
export declare function SupplierCreateRequestFromJSON(json: any): SupplierCreateRequest;
export declare function SupplierCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierCreateRequest;
export declare function SupplierCreateRequestToJSON(value?: SupplierCreateRequest | null): any;
