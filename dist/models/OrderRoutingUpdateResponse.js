"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingUpdateResponseToJSON = exports.OrderRoutingUpdateResponseFromJSONTyped = exports.OrderRoutingUpdateResponseFromJSON = exports.OrderRoutingUpdateResponseCodeEnum = exports.OrderRoutingUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var OrderRoutingUpdateResponseSuccessEnum;
(function (OrderRoutingUpdateResponseSuccessEnum) {
    OrderRoutingUpdateResponseSuccessEnum["True"] = "true";
})(OrderRoutingUpdateResponseSuccessEnum = exports.OrderRoutingUpdateResponseSuccessEnum || (exports.OrderRoutingUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrderRoutingUpdateResponseCodeEnum;
(function (OrderRoutingUpdateResponseCodeEnum) {
    OrderRoutingUpdateResponseCodeEnum[OrderRoutingUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrderRoutingUpdateResponseCodeEnum[OrderRoutingUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrderRoutingUpdateResponseCodeEnum[OrderRoutingUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrderRoutingUpdateResponseCodeEnum = exports.OrderRoutingUpdateResponseCodeEnum || (exports.OrderRoutingUpdateResponseCodeEnum = {}));
function OrderRoutingUpdateResponseFromJSON(json) {
    return OrderRoutingUpdateResponseFromJSONTyped(json, false);
}
exports.OrderRoutingUpdateResponseFromJSON = OrderRoutingUpdateResponseFromJSON;
function OrderRoutingUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.OrderRoutingUpdateResponseFromJSONTyped = OrderRoutingUpdateResponseFromJSONTyped;
function OrderRoutingUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.OrderRoutingUpdateResponseToJSON = OrderRoutingUpdateResponseToJSON;
