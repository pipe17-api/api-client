"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorFetchResponseToJSON = exports.ConnectorFetchResponseFromJSONTyped = exports.ConnectorFetchResponseFromJSON = exports.ConnectorFetchResponseCodeEnum = exports.ConnectorFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorFetchResponseSuccessEnum;
(function (ConnectorFetchResponseSuccessEnum) {
    ConnectorFetchResponseSuccessEnum["True"] = "true";
})(ConnectorFetchResponseSuccessEnum = exports.ConnectorFetchResponseSuccessEnum || (exports.ConnectorFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ConnectorFetchResponseCodeEnum;
(function (ConnectorFetchResponseCodeEnum) {
    ConnectorFetchResponseCodeEnum[ConnectorFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ConnectorFetchResponseCodeEnum[ConnectorFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ConnectorFetchResponseCodeEnum[ConnectorFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ConnectorFetchResponseCodeEnum = exports.ConnectorFetchResponseCodeEnum || (exports.ConnectorFetchResponseCodeEnum = {}));
function ConnectorFetchResponseFromJSON(json) {
    return ConnectorFetchResponseFromJSONTyped(json, false);
}
exports.ConnectorFetchResponseFromJSON = ConnectorFetchResponseFromJSON;
function ConnectorFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'connector': !runtime_1.exists(json, 'connector') ? undefined : _1.ConnectorFromJSON(json['connector']),
    };
}
exports.ConnectorFetchResponseFromJSONTyped = ConnectorFetchResponseFromJSONTyped;
function ConnectorFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'connector': _1.ConnectorToJSON(value.connector),
    };
}
exports.ConnectorFetchResponseToJSON = ConnectorFetchResponseToJSON;
