"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnCreateErrorAllOfToJSON = exports.ReturnCreateErrorAllOfFromJSONTyped = exports.ReturnCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnCreateErrorAllOfFromJSON(json) {
    return ReturnCreateErrorAllOfFromJSONTyped(json, false);
}
exports.ReturnCreateErrorAllOfFromJSON = ReturnCreateErrorAllOfFromJSON;
function ReturnCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        '_return': !runtime_1.exists(json, 'return') ? undefined : _1.ReturnCreateDataFromJSON(json['return']),
    };
}
exports.ReturnCreateErrorAllOfFromJSONTyped = ReturnCreateErrorAllOfFromJSONTyped;
function ReturnCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'return': _1.ReturnCreateDataToJSON(value._return),
    };
}
exports.ReturnCreateErrorAllOfToJSON = ReturnCreateErrorAllOfToJSON;
