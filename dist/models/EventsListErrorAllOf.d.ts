/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventsListFilter } from './';
/**
 *
 * @export
 * @interface EventsListErrorAllOf
 */
export interface EventsListErrorAllOf {
    /**
     *
     * @type {EventsListFilter}
     * @memberof EventsListErrorAllOf
     */
    filters?: EventsListFilter;
}
export declare function EventsListErrorAllOfFromJSON(json: any): EventsListErrorAllOf;
export declare function EventsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsListErrorAllOf;
export declare function EventsListErrorAllOfToJSON(value?: EventsListErrorAllOf | null): any;
