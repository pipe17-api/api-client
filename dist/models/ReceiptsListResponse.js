"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsListResponseToJSON = exports.ReceiptsListResponseFromJSONTyped = exports.ReceiptsListResponseFromJSON = exports.ReceiptsListResponseCodeEnum = exports.ReceiptsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReceiptsListResponseSuccessEnum;
(function (ReceiptsListResponseSuccessEnum) {
    ReceiptsListResponseSuccessEnum["True"] = "true";
})(ReceiptsListResponseSuccessEnum = exports.ReceiptsListResponseSuccessEnum || (exports.ReceiptsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReceiptsListResponseCodeEnum;
(function (ReceiptsListResponseCodeEnum) {
    ReceiptsListResponseCodeEnum[ReceiptsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReceiptsListResponseCodeEnum[ReceiptsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReceiptsListResponseCodeEnum[ReceiptsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReceiptsListResponseCodeEnum = exports.ReceiptsListResponseCodeEnum || (exports.ReceiptsListResponseCodeEnum = {}));
function ReceiptsListResponseFromJSON(json) {
    return ReceiptsListResponseFromJSONTyped(json, false);
}
exports.ReceiptsListResponseFromJSON = ReceiptsListResponseFromJSON;
function ReceiptsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReceiptsListFilterFromJSON(json['filters']),
        'receipts': (json['receipts'].map(_1.ReceiptFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ReceiptsListResponseFromJSONTyped = ReceiptsListResponseFromJSONTyped;
function ReceiptsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ReceiptsListFilterToJSON(value.filters),
        'receipts': (value.receipts.map(_1.ReceiptToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ReceiptsListResponseToJSON = ReceiptsListResponseToJSON;
