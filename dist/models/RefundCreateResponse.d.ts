/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Refund } from './';
/**
 *
 * @export
 * @interface RefundCreateResponse
 */
export interface RefundCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RefundCreateResponse
     */
    success: RefundCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundCreateResponse
     */
    code: RefundCreateResponseCodeEnum;
    /**
     *
     * @type {Refund}
     * @memberof RefundCreateResponse
     */
    refund?: Refund;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RefundCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RefundCreateResponseFromJSON(json: any): RefundCreateResponse;
export declare function RefundCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundCreateResponse;
export declare function RefundCreateResponseToJSON(value?: RefundCreateResponse | null): any;
