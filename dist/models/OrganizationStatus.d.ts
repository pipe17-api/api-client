/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Organization Account Status
 * @export
 * @enum {string}
 */
export declare enum OrganizationStatus {
    Pending = "pending",
    Active = "active",
    Disabled = "disabled"
}
export declare function OrganizationStatusFromJSON(json: any): OrganizationStatus;
export declare function OrganizationStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationStatus;
export declare function OrganizationStatusToJSON(value?: OrganizationStatus | null): any;
