"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationUpdateErrorToJSON = exports.LocationUpdateErrorFromJSONTyped = exports.LocationUpdateErrorFromJSON = exports.LocationUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationUpdateErrorSuccessEnum;
(function (LocationUpdateErrorSuccessEnum) {
    LocationUpdateErrorSuccessEnum["False"] = "false";
})(LocationUpdateErrorSuccessEnum = exports.LocationUpdateErrorSuccessEnum || (exports.LocationUpdateErrorSuccessEnum = {}));
function LocationUpdateErrorFromJSON(json) {
    return LocationUpdateErrorFromJSONTyped(json, false);
}
exports.LocationUpdateErrorFromJSON = LocationUpdateErrorFromJSON;
function LocationUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationUpdateDataFromJSON(json['location']),
    };
}
exports.LocationUpdateErrorFromJSONTyped = LocationUpdateErrorFromJSONTyped;
function LocationUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'location': _1.LocationUpdateDataToJSON(value.location),
    };
}
exports.LocationUpdateErrorToJSON = LocationUpdateErrorToJSON;
