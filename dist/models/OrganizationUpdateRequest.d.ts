/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationServiceTier, OrganizationStatus, OrganizationUpdateAddress } from './';
/**
 *
 * @export
 * @interface OrganizationUpdateRequest
 */
export interface OrganizationUpdateRequest {
    /**
     * Organization Name
     * @type {string}
     * @memberof OrganizationUpdateRequest
     */
    name?: string;
    /**
     *
     * @type {OrganizationServiceTier}
     * @memberof OrganizationUpdateRequest
     */
    tier?: OrganizationServiceTier;
    /**
     * Organization Contact email
     * @type {string}
     * @memberof OrganizationUpdateRequest
     */
    email?: string;
    /**
     * Organization Contact Phone
     * @type {string}
     * @memberof OrganizationUpdateRequest
     */
    phone?: string;
    /**
     * Organization Time Zone
     * @type {string}
     * @memberof OrganizationUpdateRequest
     */
    timeZone?: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof OrganizationUpdateRequest
     */
    logoUrl?: string;
    /**
     *
     * @type {OrganizationUpdateAddress}
     * @memberof OrganizationUpdateRequest
     */
    address?: OrganizationUpdateAddress;
    /**
     *
     * @type {OrganizationStatus}
     * @memberof OrganizationUpdateRequest
     */
    status?: OrganizationStatus;
}
export declare function OrganizationUpdateRequestFromJSON(json: any): OrganizationUpdateRequest;
export declare function OrganizationUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationUpdateRequest;
export declare function OrganizationUpdateRequestToJSON(value?: OrganizationUpdateRequest | null): any;
