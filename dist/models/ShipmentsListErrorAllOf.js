"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsListErrorAllOfToJSON = exports.ShipmentsListErrorAllOfFromJSONTyped = exports.ShipmentsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentsListErrorAllOfFromJSON(json) {
    return ShipmentsListErrorAllOfFromJSONTyped(json, false);
}
exports.ShipmentsListErrorAllOfFromJSON = ShipmentsListErrorAllOfFromJSON;
function ShipmentsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ShipmentsFilterFromJSON(json['filters']),
    };
}
exports.ShipmentsListErrorAllOfFromJSONTyped = ShipmentsListErrorAllOfFromJSONTyped;
function ShipmentsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ShipmentsFilterToJSON(value.filters),
    };
}
exports.ShipmentsListErrorAllOfToJSON = ShipmentsListErrorAllOfToJSON;
