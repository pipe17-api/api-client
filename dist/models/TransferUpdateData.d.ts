/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressNullable, NameValue, TransferStatus, TransferUpdateLineItem } from './';
/**
 *
 * @export
 * @interface TransferUpdateData
 */
export interface TransferUpdateData {
    /**
     *
     * @type {TransferStatus}
     * @memberof TransferUpdateData
     */
    status?: TransferStatus;
    /**
     *
     * @type {Array<TransferUpdateLineItem>}
     * @memberof TransferUpdateData
     */
    lineItems?: Array<TransferUpdateLineItem>;
    /**
     * Shipping Location ID
     * @type {string}
     * @memberof TransferUpdateData
     */
    fromLocationId?: string | null;
    /**
     *
     * @type {AddressNullable}
     * @memberof TransferUpdateData
     */
    toAddress?: AddressNullable | null;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof TransferUpdateData
     */
    toLocationId?: string | null;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof TransferUpdateData
     */
    shippingCarrier?: string | null;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof TransferUpdateData
     */
    shippingService?: string | null;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof TransferUpdateData
     */
    shipByDate?: Date | null;
    /**
     * Expected Ship Date
     * @type {Date}
     * @memberof TransferUpdateData
     */
    expectedShipDate?: Date | null;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof TransferUpdateData
     */
    expectedArrivalDate?: Date | null;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof TransferUpdateData
     */
    actualArrivalDate?: Date | null;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof TransferUpdateData
     */
    shippingNotes?: string | null;
    /**
     * Transfer order created by
     * @type {string}
     * @memberof TransferUpdateData
     */
    employeeName?: string | null;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof TransferUpdateData
     */
    incoterms?: string | null;
    /**
     * Transfer Notes
     * @type {string}
     * @memberof TransferUpdateData
     */
    transferNotes?: string | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof TransferUpdateData
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof TransferUpdateData
     */
    timestamp?: Date | null;
}
export declare function TransferUpdateDataFromJSON(json: any): TransferUpdateData;
export declare function TransferUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferUpdateData;
export declare function TransferUpdateDataToJSON(value?: TransferUpdateData | null): any;
