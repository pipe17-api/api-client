"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptFetchResponseAllOfToJSON = exports.ReceiptFetchResponseAllOfFromJSONTyped = exports.ReceiptFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReceiptFetchResponseAllOfFromJSON(json) {
    return ReceiptFetchResponseAllOfFromJSONTyped(json, false);
}
exports.ReceiptFetchResponseAllOfFromJSON = ReceiptFetchResponseAllOfFromJSON;
function ReceiptFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'receipt': !runtime_1.exists(json, 'receipt') ? undefined : _1.ReceiptFromJSON(json['receipt']),
    };
}
exports.ReceiptFetchResponseAllOfFromJSONTyped = ReceiptFetchResponseAllOfFromJSONTyped;
function ReceiptFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'receipt': _1.ReceiptToJSON(value.receipt),
    };
}
exports.ReceiptFetchResponseAllOfToJSON = ReceiptFetchResponseAllOfToJSON;
