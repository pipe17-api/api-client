/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface WebhookAllOf
 */
export interface WebhookAllOf {
    /**
     * Webhook ID
     * @type {string}
     * @memberof WebhookAllOf
     */
    webhookId?: string;
    /**
     * Integration ID
     * @type {string}
     * @memberof WebhookAllOf
     */
    integration?: string;
    /**
     * Connector ID
     * @type {string}
     * @memberof WebhookAllOf
     */
    connectorId?: string;
}
export declare function WebhookAllOfFromJSON(json: any): WebhookAllOf;
export declare function WebhookAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookAllOf;
export declare function WebhookAllOfToJSON(value?: WebhookAllOf | null): any;
