/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleStatus } from './';
/**
 *
 * @export
 * @interface RoleAllOf
 */
export interface RoleAllOf {
    /**
     * Role ID
     * @type {string}
     * @memberof RoleAllOf
     */
    roleId?: string;
    /**
     *
     * @type {RoleStatus}
     * @memberof RoleAllOf
     */
    status?: RoleStatus;
}
export declare function RoleAllOfFromJSON(json: any): RoleAllOf;
export declare function RoleAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleAllOf;
export declare function RoleAllOfToJSON(value?: RoleAllOf | null): any;
