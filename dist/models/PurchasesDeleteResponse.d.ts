/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Purchase, PurchasesDeleteFilter } from './';
/**
 *
 * @export
 * @interface PurchasesDeleteResponse
 */
export interface PurchasesDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof PurchasesDeleteResponse
     */
    success: PurchasesDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchasesDeleteResponse
     */
    code: PurchasesDeleteResponseCodeEnum;
    /**
     *
     * @type {PurchasesDeleteFilter}
     * @memberof PurchasesDeleteResponse
     */
    filters?: PurchasesDeleteFilter;
    /**
     *
     * @type {Array<Purchase>}
     * @memberof PurchasesDeleteResponse
     */
    purchases?: Array<Purchase>;
    /**
     * Number of deleted purchases
     * @type {number}
     * @memberof PurchasesDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof PurchasesDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchasesDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum PurchasesDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function PurchasesDeleteResponseFromJSON(json: any): PurchasesDeleteResponse;
export declare function PurchasesDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesDeleteResponse;
export declare function PurchasesDeleteResponseToJSON(value?: PurchasesDeleteResponse | null): any;
