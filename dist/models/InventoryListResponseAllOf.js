"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryListResponseAllOfToJSON = exports.InventoryListResponseAllOfFromJSONTyped = exports.InventoryListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryListResponseAllOfFromJSON(json) {
    return InventoryListResponseAllOfFromJSONTyped(json, false);
}
exports.InventoryListResponseAllOfFromJSON = InventoryListResponseAllOfFromJSON;
function InventoryListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryListFilterFromJSON(json['filters']),
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryListResponseAllOfFromJSONTyped = InventoryListResponseAllOfFromJSONTyped;
function InventoryListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.InventoryListFilterToJSON(value.filters),
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryListResponseAllOfToJSON = InventoryListResponseAllOfToJSON;
