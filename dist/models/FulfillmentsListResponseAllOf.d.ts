/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Fulfillment, FulfillmentsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface FulfillmentsListResponseAllOf
 */
export interface FulfillmentsListResponseAllOf {
    /**
     *
     * @type {FulfillmentsListFilter}
     * @memberof FulfillmentsListResponseAllOf
     */
    filters?: FulfillmentsListFilter;
    /**
     *
     * @type {Array<Fulfillment>}
     * @memberof FulfillmentsListResponseAllOf
     */
    fulfillments: Array<Fulfillment>;
    /**
     *
     * @type {Pagination}
     * @memberof FulfillmentsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function FulfillmentsListResponseAllOfFromJSON(json: any): FulfillmentsListResponseAllOf;
export declare function FulfillmentsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsListResponseAllOf;
export declare function FulfillmentsListResponseAllOfToJSON(value?: FulfillmentsListResponseAllOf | null): any;
