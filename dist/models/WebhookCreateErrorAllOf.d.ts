/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookCreateData } from './';
/**
 *
 * @export
 * @interface WebhookCreateErrorAllOf
 */
export interface WebhookCreateErrorAllOf {
    /**
     *
     * @type {WebhookCreateData}
     * @memberof WebhookCreateErrorAllOf
     */
    webhook?: WebhookCreateData;
}
export declare function WebhookCreateErrorAllOfFromJSON(json: any): WebhookCreateErrorAllOf;
export declare function WebhookCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookCreateErrorAllOf;
export declare function WebhookCreateErrorAllOfToJSON(value?: WebhookCreateErrorAllOf | null): any;
