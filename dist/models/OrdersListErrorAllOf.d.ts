/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrdersListFilter } from './';
/**
 *
 * @export
 * @interface OrdersListErrorAllOf
 */
export interface OrdersListErrorAllOf {
    /**
     *
     * @type {OrdersListFilter}
     * @memberof OrdersListErrorAllOf
     */
    filters?: OrdersListFilter;
}
export declare function OrdersListErrorAllOfFromJSON(json: any): OrdersListErrorAllOf;
export declare function OrdersListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersListErrorAllOf;
export declare function OrdersListErrorAllOfToJSON(value?: OrdersListErrorAllOf | null): any;
