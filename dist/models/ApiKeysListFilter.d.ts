/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeysListFilter
 */
export interface ApiKeysListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ApiKeysListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ApiKeysListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ApiKeysListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ApiKeysListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ApiKeysListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ApiKeysListFilter
     */
    count?: number;
    /**
     * List API Keys by matching connector id(s)
     * @type {Array<string>}
     * @memberof ApiKeysListFilter
     */
    connector?: Array<string>;
    /**
     * List API Keys by matching integration id(s)
     * @type {Array<string>}
     * @memberof ApiKeysListFilter
     */
    integration?: Array<string>;
    /**
     * List API Keys by matching name(s)
     * @type {string}
     * @memberof ApiKeysListFilter
     */
    name?: string;
    /**
     * List sort order
     * @type {string}
     * @memberof ApiKeysListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ApiKeysListFilter
     */
    keys?: string;
}
export declare function ApiKeysListFilterFromJSON(json: any): ApiKeysListFilter;
export declare function ApiKeysListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysListFilter;
export declare function ApiKeysListFilterToJSON(value?: ApiKeysListFilter | null): any;
