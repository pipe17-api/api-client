"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsListResponseToJSON = exports.FulfillmentsListResponseFromJSONTyped = exports.FulfillmentsListResponseFromJSON = exports.FulfillmentsListResponseCodeEnum = exports.FulfillmentsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentsListResponseSuccessEnum;
(function (FulfillmentsListResponseSuccessEnum) {
    FulfillmentsListResponseSuccessEnum["True"] = "true";
})(FulfillmentsListResponseSuccessEnum = exports.FulfillmentsListResponseSuccessEnum || (exports.FulfillmentsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var FulfillmentsListResponseCodeEnum;
(function (FulfillmentsListResponseCodeEnum) {
    FulfillmentsListResponseCodeEnum[FulfillmentsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    FulfillmentsListResponseCodeEnum[FulfillmentsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    FulfillmentsListResponseCodeEnum[FulfillmentsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(FulfillmentsListResponseCodeEnum = exports.FulfillmentsListResponseCodeEnum || (exports.FulfillmentsListResponseCodeEnum = {}));
function FulfillmentsListResponseFromJSON(json) {
    return FulfillmentsListResponseFromJSONTyped(json, false);
}
exports.FulfillmentsListResponseFromJSON = FulfillmentsListResponseFromJSON;
function FulfillmentsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.FulfillmentsListFilterFromJSON(json['filters']),
        'fulfillments': (json['fulfillments'].map(_1.FulfillmentFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.FulfillmentsListResponseFromJSONTyped = FulfillmentsListResponseFromJSONTyped;
function FulfillmentsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.FulfillmentsListFilterToJSON(value.filters),
        'fulfillments': (value.fulfillments.map(_1.FulfillmentToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.FulfillmentsListResponseToJSON = FulfillmentsListResponseToJSON;
