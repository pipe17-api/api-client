/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ProductFetchError
 */
export interface ProductFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ProductFetchError
     */
    success: ProductFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ProductFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ProductFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductFetchErrorSuccessEnum {
    False = "false"
}
export declare function ProductFetchErrorFromJSON(json: any): ProductFetchError;
export declare function ProductFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductFetchError;
export declare function ProductFetchErrorToJSON(value?: ProductFetchError | null): any;
