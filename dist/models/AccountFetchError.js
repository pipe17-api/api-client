"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountFetchErrorToJSON = exports.AccountFetchErrorFromJSONTyped = exports.AccountFetchErrorFromJSON = exports.AccountFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var AccountFetchErrorSuccessEnum;
(function (AccountFetchErrorSuccessEnum) {
    AccountFetchErrorSuccessEnum["False"] = "false";
})(AccountFetchErrorSuccessEnum = exports.AccountFetchErrorSuccessEnum || (exports.AccountFetchErrorSuccessEnum = {}));
function AccountFetchErrorFromJSON(json) {
    return AccountFetchErrorFromJSONTyped(json, false);
}
exports.AccountFetchErrorFromJSON = AccountFetchErrorFromJSON;
function AccountFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.AccountFetchErrorFromJSONTyped = AccountFetchErrorFromJSONTyped;
function AccountFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.AccountFetchErrorToJSON = AccountFetchErrorToJSON;
