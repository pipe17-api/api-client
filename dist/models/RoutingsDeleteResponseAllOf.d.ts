/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Routing, RoutingsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RoutingsDeleteResponseAllOf
 */
export interface RoutingsDeleteResponseAllOf {
    /**
     *
     * @type {RoutingsDeleteFilter}
     * @memberof RoutingsDeleteResponseAllOf
     */
    filters?: RoutingsDeleteFilter;
    /**
     *
     * @type {Array<Routing>}
     * @memberof RoutingsDeleteResponseAllOf
     */
    routings?: Array<Routing>;
    /**
     * Number of deleted routings
     * @type {number}
     * @memberof RoutingsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof RoutingsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function RoutingsDeleteResponseAllOfFromJSON(json: any): RoutingsDeleteResponseAllOf;
export declare function RoutingsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsDeleteResponseAllOf;
export declare function RoutingsDeleteResponseAllOfToJSON(value?: RoutingsDeleteResponseAllOf | null): any;
