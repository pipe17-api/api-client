"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingCreateErrorAllOfToJSON = exports.MappingCreateErrorAllOfFromJSONTyped = exports.MappingCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingCreateErrorAllOfFromJSON(json) {
    return MappingCreateErrorAllOfFromJSONTyped(json, false);
}
exports.MappingCreateErrorAllOfFromJSON = MappingCreateErrorAllOfFromJSON;
function MappingCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingCreateDataFromJSON(json['mapping']),
    };
}
exports.MappingCreateErrorAllOfFromJSONTyped = MappingCreateErrorAllOfFromJSONTyped;
function MappingCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mapping': _1.MappingCreateDataToJSON(value.mapping),
    };
}
exports.MappingCreateErrorAllOfToJSON = MappingCreateErrorAllOfToJSON;
