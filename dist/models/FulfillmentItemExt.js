"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentItemExtToJSON = exports.FulfillmentItemExtFromJSONTyped = exports.FulfillmentItemExtFromJSON = void 0;
const runtime_1 = require("../runtime");
function FulfillmentItemExtFromJSON(json) {
    return FulfillmentItemExtFromJSONTyped(json, false);
}
exports.FulfillmentItemExtFromJSON = FulfillmentItemExtFromJSON;
function FulfillmentItemExtFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': json['sku'],
        'quantity': json['quantity'],
        'uniqueId': !runtime_1.exists(json, 'uniqueId') ? undefined : json['uniqueId'],
        'bundleSKU': !runtime_1.exists(json, 'bundleSKU') ? undefined : json['bundleSKU'],
        'bundleQuantity': !runtime_1.exists(json, 'bundleQuantity') ? undefined : json['bundleQuantity'],
    };
}
exports.FulfillmentItemExtFromJSONTyped = FulfillmentItemExtFromJSONTyped;
function FulfillmentItemExtToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'quantity': value.quantity,
        'uniqueId': value.uniqueId,
        'bundleSKU': value.bundleSKU,
        'bundleQuantity': value.bundleQuantity,
    };
}
exports.FulfillmentItemExtToJSON = FulfillmentItemExtToJSON;
