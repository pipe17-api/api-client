"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsCreateErrorToJSON = exports.ProductsCreateErrorFromJSONTyped = exports.ProductsCreateErrorFromJSON = exports.ProductsCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductsCreateErrorSuccessEnum;
(function (ProductsCreateErrorSuccessEnum) {
    ProductsCreateErrorSuccessEnum["False"] = "false";
})(ProductsCreateErrorSuccessEnum = exports.ProductsCreateErrorSuccessEnum || (exports.ProductsCreateErrorSuccessEnum = {}));
function ProductsCreateErrorFromJSON(json) {
    return ProductsCreateErrorFromJSONTyped(json, false);
}
exports.ProductsCreateErrorFromJSON = ProductsCreateErrorFromJSON;
function ProductsCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'products': !runtime_1.exists(json, 'products') ? undefined : (json['products'].map(_1.ProductCreateResultFromJSON)),
    };
}
exports.ProductsCreateErrorFromJSONTyped = ProductsCreateErrorFromJSONTyped;
function ProductsCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'products': value.products === undefined ? undefined : (value.products.map(_1.ProductCreateResultToJSON)),
    };
}
exports.ProductsCreateErrorToJSON = ProductsCreateErrorToJSON;
