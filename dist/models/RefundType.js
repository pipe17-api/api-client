"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundTypeToJSON = exports.RefundTypeFromJSONTyped = exports.RefundTypeFromJSON = exports.RefundType = void 0;
/**
 * Refund type
 * @export
 * @enum {string}
 */
var RefundType;
(function (RefundType) {
    RefundType["Refund"] = "refund";
    RefundType["Voucher"] = "voucher";
})(RefundType = exports.RefundType || (exports.RefundType = {}));
function RefundTypeFromJSON(json) {
    return RefundTypeFromJSONTyped(json, false);
}
exports.RefundTypeFromJSON = RefundTypeFromJSON;
function RefundTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.RefundTypeFromJSONTyped = RefundTypeFromJSONTyped;
function RefundTypeToJSON(value) {
    return value;
}
exports.RefundTypeToJSON = RefundTypeToJSON;
