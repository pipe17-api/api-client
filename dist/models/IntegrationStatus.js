"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStatusToJSON = exports.IntegrationStatusFromJSONTyped = exports.IntegrationStatusFromJSON = exports.IntegrationStatus = void 0;
/**
 * Integration Status
 * @export
 * @enum {string}
 */
var IntegrationStatus;
(function (IntegrationStatus) {
    IntegrationStatus["Created"] = "created";
    IntegrationStatus["Configured"] = "configured";
    IntegrationStatus["Initializing"] = "initializing";
    IntegrationStatus["Connected"] = "connected";
    IntegrationStatus["Error"] = "error";
    IntegrationStatus["Disabled"] = "disabled";
})(IntegrationStatus = exports.IntegrationStatus || (exports.IntegrationStatus = {}));
function IntegrationStatusFromJSON(json) {
    return IntegrationStatusFromJSONTyped(json, false);
}
exports.IntegrationStatusFromJSON = IntegrationStatusFromJSON;
function IntegrationStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.IntegrationStatusFromJSONTyped = IntegrationStatusFromJSONTyped;
function IntegrationStatusToJSON(value) {
    return value;
}
exports.IntegrationStatusToJSON = IntegrationStatusToJSON;
