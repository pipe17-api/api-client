/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RefundsDeleteError
 */
export interface RefundsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RefundsDeleteError
     */
    success: RefundsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RefundsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RefundsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RefundsDeleteFilter}
     * @memberof RefundsDeleteError
     */
    filters?: RefundsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function RefundsDeleteErrorFromJSON(json: any): RefundsDeleteError;
export declare function RefundsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsDeleteError;
export declare function RefundsDeleteErrorToJSON(value?: RefundsDeleteError | null): any;
