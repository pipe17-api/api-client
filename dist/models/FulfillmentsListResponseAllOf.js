"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsListResponseAllOfToJSON = exports.FulfillmentsListResponseAllOfFromJSONTyped = exports.FulfillmentsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function FulfillmentsListResponseAllOfFromJSON(json) {
    return FulfillmentsListResponseAllOfFromJSONTyped(json, false);
}
exports.FulfillmentsListResponseAllOfFromJSON = FulfillmentsListResponseAllOfFromJSON;
function FulfillmentsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.FulfillmentsListFilterFromJSON(json['filters']),
        'fulfillments': (json['fulfillments'].map(_1.FulfillmentFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.FulfillmentsListResponseAllOfFromJSONTyped = FulfillmentsListResponseAllOfFromJSONTyped;
function FulfillmentsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.FulfillmentsListFilterToJSON(value.filters),
        'fulfillments': (value.fulfillments.map(_1.FulfillmentToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.FulfillmentsListResponseAllOfToJSON = FulfillmentsListResponseAllOfToJSON;
