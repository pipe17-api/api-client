/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LocationFetchError
 */
export interface LocationFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LocationFetchError
     */
    success: LocationFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LocationFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LocationFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationFetchErrorSuccessEnum {
    False = "false"
}
export declare function LocationFetchErrorFromJSON(json: any): LocationFetchError;
export declare function LocationFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationFetchError;
export declare function LocationFetchErrorToJSON(value?: LocationFetchError | null): any;
