"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentCreateStatusToJSON = exports.ShipmentCreateStatusFromJSONTyped = exports.ShipmentCreateStatusFromJSON = exports.ShipmentCreateStatus = void 0;
/**
 * Shipment Status
 * @export
 * @enum {string}
 */
var ShipmentCreateStatus;
(function (ShipmentCreateStatus) {
    ShipmentCreateStatus["PendingInventory"] = "pendingInventory";
    ShipmentCreateStatus["PendingShippingLabel"] = "pendingShippingLabel";
    ShipmentCreateStatus["ReadyForFulfillment"] = "readyForFulfillment";
})(ShipmentCreateStatus = exports.ShipmentCreateStatus || (exports.ShipmentCreateStatus = {}));
function ShipmentCreateStatusFromJSON(json) {
    return ShipmentCreateStatusFromJSONTyped(json, false);
}
exports.ShipmentCreateStatusFromJSON = ShipmentCreateStatusFromJSON;
function ShipmentCreateStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ShipmentCreateStatusFromJSONTyped = ShipmentCreateStatusFromJSONTyped;
function ShipmentCreateStatusToJSON(value) {
    return value;
}
exports.ShipmentCreateStatusToJSON = ShipmentCreateStatusToJSON;
