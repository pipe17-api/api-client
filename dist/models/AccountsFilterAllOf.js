"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsFilterAllOfToJSON = exports.AccountsFilterAllOfFromJSONTyped = exports.AccountsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountsFilterAllOfFromJSON(json) {
    return AccountsFilterAllOfFromJSONTyped(json, false);
}
exports.AccountsFilterAllOfFromJSON = AccountsFilterAllOfFromJSON;
function AccountsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.AccountStatusFromJSON)),
    };
}
exports.AccountsFilterAllOfFromJSONTyped = AccountsFilterAllOfFromJSONTyped;
function AccountsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'orgKey': value.orgKey,
        'status': value.status === undefined ? undefined : (value.status.map(_1.AccountStatusToJSON)),
    };
}
exports.AccountsFilterAllOfToJSON = AccountsFilterAllOfToJSON;
