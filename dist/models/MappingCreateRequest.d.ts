/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingRule } from './';
/**
 *
 * @export
 * @interface MappingCreateRequest
 */
export interface MappingCreateRequest {
    /**
     * Public mapping indicator
     * @type {boolean}
     * @memberof MappingCreateRequest
     */
    isPublic: boolean;
    /**
     * Description
     * @type {string}
     * @memberof MappingCreateRequest
     */
    description: string;
    /**
     *
     * @type {Array<MappingRule>}
     * @memberof MappingCreateRequest
     */
    mappings?: Array<MappingRule>;
    /**
     * UUID (generated if not provided)
     * @type {string}
     * @memberof MappingCreateRequest
     */
    uuid?: string;
    /**
     *
     * @type {string}
     * @memberof MappingCreateRequest
     */
    originId?: string;
}
export declare function MappingCreateRequestFromJSON(json: any): MappingCreateRequest;
export declare function MappingCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingCreateRequest;
export declare function MappingCreateRequestToJSON(value?: MappingCreateRequest | null): any;
