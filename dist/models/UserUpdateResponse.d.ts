/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface UserUpdateResponse
 */
export interface UserUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof UserUpdateResponse
     */
    success: UserUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UserUpdateResponse
     */
    code: UserUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum UserUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum UserUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function UserUpdateResponseFromJSON(json: any): UserUpdateResponse;
export declare function UserUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserUpdateResponse;
export declare function UserUpdateResponseToJSON(value?: UserUpdateResponse | null): any;
