/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorSettingsOptionsValues
 */
export interface ConnectorSettingsOptionsValues {
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptionsValues
     */
    label?: string;
    /**
     *
     * @type {string}
     * @memberof ConnectorSettingsOptionsValues
     */
    value?: string;
}
export declare function ConnectorSettingsOptionsValuesFromJSON(json: any): ConnectorSettingsOptionsValues;
export declare function ConnectorSettingsOptionsValuesFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSettingsOptionsValues;
export declare function ConnectorSettingsOptionsValuesToJSON(value?: ConnectorSettingsOptionsValues | null): any;
