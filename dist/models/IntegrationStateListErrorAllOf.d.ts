/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStateListFilter } from './';
/**
 *
 * @export
 * @interface IntegrationStateListErrorAllOf
 */
export interface IntegrationStateListErrorAllOf {
    /**
     *
     * @type {IntegrationStateListFilter}
     * @memberof IntegrationStateListErrorAllOf
     */
    filters?: IntegrationStateListFilter;
}
export declare function IntegrationStateListErrorAllOfFromJSON(json: any): IntegrationStateListErrorAllOf;
export declare function IntegrationStateListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateListErrorAllOf;
export declare function IntegrationStateListErrorAllOfToJSON(value?: IntegrationStateListErrorAllOf | null): any;
