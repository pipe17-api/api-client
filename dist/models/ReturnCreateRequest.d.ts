/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ReturnLineItem, ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnCreateRequest
 */
export interface ReturnCreateRequest {
    /**
     * Return ID on external system
     * @type {string}
     * @memberof ReturnCreateRequest
     */
    extReturnId?: string;
    /**
     *
     * @type {ReturnStatus}
     * @memberof ReturnCreateRequest
     */
    status?: ReturnStatus;
    /**
     *
     * @type {Array<ReturnLineItem>}
     * @memberof ReturnCreateRequest
     */
    lineItems?: Array<ReturnLineItem>;
    /**
     *
     * @type {Address}
     * @memberof ReturnCreateRequest
     */
    shippingAddress?: Address;
    /**
     * Currency value, e.g. 'USD'
     * @type {string}
     * @memberof ReturnCreateRequest
     */
    currency?: string;
    /**
     * Customer notes
     * @type {string}
     * @memberof ReturnCreateRequest
     */
    customerNotes?: string;
    /**
     * Merchant notes
     * @type {string}
     * @memberof ReturnCreateRequest
     */
    notes?: string;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    tax?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    discount?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    subTotal?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    total?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    shippingPrice?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    shippingRefund?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    shippingQuote?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    shippingLabelFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    restockingFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnCreateRequest
     */
    estimatedTotal?: number;
    /**
     *
     * @type {boolean}
     * @memberof ReturnCreateRequest
     */
    isExchange?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnCreateRequest
     */
    isGift?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnCreateRequest
     */
    requiresShipping?: boolean;
    /**
     *
     * @type {Date}
     * @memberof ReturnCreateRequest
     */
    refundedAt?: Date;
}
export declare function ReturnCreateRequestFromJSON(json: any): ReturnCreateRequest;
export declare function ReturnCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnCreateRequest;
export declare function ReturnCreateRequestToJSON(value?: ReturnCreateRequest | null): any;
