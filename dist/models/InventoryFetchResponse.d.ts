/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory } from './';
/**
 *
 * @export
 * @interface InventoryFetchResponse
 */
export interface InventoryFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryFetchResponse
     */
    success: InventoryFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryFetchResponse
     */
    code: InventoryFetchResponseCodeEnum;
    /**
     *
     * @type {Inventory}
     * @memberof InventoryFetchResponse
     */
    inventory?: Inventory;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryFetchResponseFromJSON(json: any): InventoryFetchResponse;
export declare function InventoryFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryFetchResponse;
export declare function InventoryFetchResponseToJSON(value?: InventoryFetchResponse | null): any;
