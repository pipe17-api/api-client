/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorConnection, ConnectorEntity, ConnectorSettings, ConnectorType, ConnectorWebhook } from './';
/**
 *
 * @export
 * @interface ConnectorCreateData
 */
export interface ConnectorCreateData {
    /**
     * Connector description
     * @type {string}
     * @memberof ConnectorCreateData
     */
    description?: string;
    /**
     * Connector readme markup
     * @type {string}
     * @memberof ConnectorCreateData
     */
    readme?: string;
    /**
     * Connector version
     * @type {string}
     * @memberof ConnectorCreateData
     */
    version?: string;
    /**
     * Connector Name
     * @type {string}
     * @memberof ConnectorCreateData
     */
    connectorName: string;
    /**
     *
     * @type {ConnectorType}
     * @memberof ConnectorCreateData
     */
    connectorType: ConnectorType;
    /**
     * Logo URL
     * @type {string}
     * @memberof ConnectorCreateData
     */
    logoUrl?: string;
    /**
     *
     * @type {Array<ConnectorEntity>}
     * @memberof ConnectorCreateData
     */
    entities?: Array<ConnectorEntity>;
    /**
     *
     * @type {ConnectorSettings}
     * @memberof ConnectorCreateData
     */
    settings?: ConnectorSettings;
    /**
     *
     * @type {ConnectorConnection}
     * @memberof ConnectorCreateData
     */
    connection?: ConnectorConnection;
    /**
     *
     * @type {ConnectorWebhook}
     * @memberof ConnectorCreateData
     */
    webhook?: ConnectorWebhook;
    /**
     * Indicates whether connector/integration should have access to all declared entity types
     * @type {boolean}
     * @memberof ConnectorCreateData
     */
    orgWideAccess?: boolean;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof ConnectorCreateData
     */
    orgUnique?: boolean;
    /**
     * Connector Display Name
     * @type {string}
     * @memberof ConnectorCreateData
     */
    displayName?: string;
}
export declare function ConnectorCreateDataFromJSON(json: any): ConnectorCreateData;
export declare function ConnectorCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorCreateData;
export declare function ConnectorCreateDataToJSON(value?: ConnectorCreateData | null): any;
