"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConverterResponseToJSON = exports.ConverterResponseFromJSONTyped = exports.ConverterResponseFromJSON = exports.ConverterResponseCodeEnum = exports.ConverterResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConverterResponseSuccessEnum;
(function (ConverterResponseSuccessEnum) {
    ConverterResponseSuccessEnum["True"] = "true";
})(ConverterResponseSuccessEnum = exports.ConverterResponseSuccessEnum || (exports.ConverterResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ConverterResponseCodeEnum;
(function (ConverterResponseCodeEnum) {
    ConverterResponseCodeEnum[ConverterResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ConverterResponseCodeEnum[ConverterResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ConverterResponseCodeEnum[ConverterResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ConverterResponseCodeEnum = exports.ConverterResponseCodeEnum || (exports.ConverterResponseCodeEnum = {}));
function ConverterResponseFromJSON(json) {
    return ConverterResponseFromJSONTyped(json, false);
}
exports.ConverterResponseFromJSON = ConverterResponseFromJSON;
function ConverterResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'result': !runtime_1.exists(json, 'result') ? undefined : _1.ConverterResponseAllOfResultFromJSON(json['result']),
        'info': !runtime_1.exists(json, 'info') ? undefined : json['info'],
    };
}
exports.ConverterResponseFromJSONTyped = ConverterResponseFromJSONTyped;
function ConverterResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'result': _1.ConverterResponseAllOfResultToJSON(value.result),
        'info': value.info,
    };
}
exports.ConverterResponseToJSON = ConverterResponseToJSON;
