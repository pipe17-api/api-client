"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreateRequestAllOfToJSON = exports.UserCreateRequestAllOfFromJSONTyped = exports.UserCreateRequestAllOfFromJSON = void 0;
function UserCreateRequestAllOfFromJSON(json) {
    return UserCreateRequestAllOfFromJSONTyped(json, false);
}
exports.UserCreateRequestAllOfFromJSON = UserCreateRequestAllOfFromJSON;
function UserCreateRequestAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'password': json['password'],
    };
}
exports.UserCreateRequestAllOfFromJSONTyped = UserCreateRequestAllOfFromJSONTyped;
function UserCreateRequestAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'password': value.password,
    };
}
exports.UserCreateRequestAllOfToJSON = UserCreateRequestAllOfToJSON;
