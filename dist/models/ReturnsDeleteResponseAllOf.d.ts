/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Return, ReturnsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ReturnsDeleteResponseAllOf
 */
export interface ReturnsDeleteResponseAllOf {
    /**
     *
     * @type {ReturnsDeleteFilter}
     * @memberof ReturnsDeleteResponseAllOf
     */
    filters?: ReturnsDeleteFilter;
    /**
     *
     * @type {Array<Return>}
     * @memberof ReturnsDeleteResponseAllOf
     */
    suppliers?: Array<Return>;
    /**
     * Number of deleted suppliers
     * @type {number}
     * @memberof ReturnsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ReturnsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ReturnsDeleteResponseAllOfFromJSON(json: any): ReturnsDeleteResponseAllOf;
export declare function ReturnsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsDeleteResponseAllOf;
export declare function ReturnsDeleteResponseAllOfToJSON(value?: ReturnsDeleteResponseAllOf | null): any;
