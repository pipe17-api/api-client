"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingFetchResponseToJSON = exports.OrderRoutingFetchResponseFromJSONTyped = exports.OrderRoutingFetchResponseFromJSON = exports.OrderRoutingFetchResponseCodeEnum = exports.OrderRoutingFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderRoutingFetchResponseSuccessEnum;
(function (OrderRoutingFetchResponseSuccessEnum) {
    OrderRoutingFetchResponseSuccessEnum["True"] = "true";
})(OrderRoutingFetchResponseSuccessEnum = exports.OrderRoutingFetchResponseSuccessEnum || (exports.OrderRoutingFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrderRoutingFetchResponseCodeEnum;
(function (OrderRoutingFetchResponseCodeEnum) {
    OrderRoutingFetchResponseCodeEnum[OrderRoutingFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrderRoutingFetchResponseCodeEnum[OrderRoutingFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrderRoutingFetchResponseCodeEnum[OrderRoutingFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrderRoutingFetchResponseCodeEnum = exports.OrderRoutingFetchResponseCodeEnum || (exports.OrderRoutingFetchResponseCodeEnum = {}));
function OrderRoutingFetchResponseFromJSON(json) {
    return OrderRoutingFetchResponseFromJSONTyped(json, false);
}
exports.OrderRoutingFetchResponseFromJSON = OrderRoutingFetchResponseFromJSON;
function OrderRoutingFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.OrderRoutingFromJSON(json['routing']),
    };
}
exports.OrderRoutingFetchResponseFromJSONTyped = OrderRoutingFetchResponseFromJSONTyped;
function OrderRoutingFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'routing': _1.OrderRoutingToJSON(value.routing),
    };
}
exports.OrderRoutingFetchResponseToJSON = OrderRoutingFetchResponseToJSON;
