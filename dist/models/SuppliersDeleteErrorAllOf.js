"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuppliersDeleteErrorAllOfToJSON = exports.SuppliersDeleteErrorAllOfFromJSONTyped = exports.SuppliersDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SuppliersDeleteErrorAllOfFromJSON(json) {
    return SuppliersDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.SuppliersDeleteErrorAllOfFromJSON = SuppliersDeleteErrorAllOfFromJSON;
function SuppliersDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.SuppliersDeleteFilterFromJSON(json['filters']),
    };
}
exports.SuppliersDeleteErrorAllOfFromJSONTyped = SuppliersDeleteErrorAllOfFromJSONTyped;
function SuppliersDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.SuppliersDeleteFilterToJSON(value.filters),
    };
}
exports.SuppliersDeleteErrorAllOfToJSON = SuppliersDeleteErrorAllOfToJSON;
