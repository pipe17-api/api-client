"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TierToJSON = exports.TierFromJSONTyped = exports.TierFromJSON = exports.Tier = void 0;
/**
 * Tier (payment plan)
 * @export
 * @enum {string}
 */
var Tier;
(function (Tier) {
    Tier["Free"] = "free";
    Tier["Basic"] = "basic";
    Tier["Enterprise"] = "enterprise";
})(Tier = exports.Tier || (exports.Tier = {}));
function TierFromJSON(json) {
    return TierFromJSONTyped(json, false);
}
exports.TierFromJSON = TierFromJSON;
function TierFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.TierFromJSONTyped = TierFromJSONTyped;
function TierToJSON(value) {
    return value;
}
exports.TierToJSON = TierToJSON;
