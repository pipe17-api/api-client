"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderUpdateDataToJSON = exports.OrderUpdateDataFromJSONTyped = exports.OrderUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrderUpdateDataFromJSON(json) {
    return OrderUpdateDataFromJSONTyped(json, false);
}
exports.OrderUpdateDataFromJSON = OrderUpdateDataFromJSON;
function OrderUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.OrderStatusFromJSON(json['status']),
        'orderSource': !runtime_1.exists(json, 'orderSource') ? undefined : json['orderSource'],
        'extOrderUrl': !runtime_1.exists(json, 'extOrderUrl') ? undefined : json['extOrderUrl'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.OrderUpdateLineItemFromJSON)),
        'subTotalPrice': !runtime_1.exists(json, 'subTotalPrice') ? undefined : json['subTotalPrice'],
        'orderDiscount': !runtime_1.exists(json, 'orderDiscount') ? undefined : json['orderDiscount'],
        'orderTax': !runtime_1.exists(json, 'orderTax') ? undefined : json['orderTax'],
        'totalPrice': !runtime_1.exists(json, 'totalPrice') ? undefined : json['totalPrice'],
        'orderNote': !runtime_1.exists(json, 'orderNote') ? undefined : json['orderNote'],
        'giftNote': !runtime_1.exists(json, 'giftNote') ? undefined : json['giftNote'],
        'shippingAddress': !runtime_1.exists(json, 'shippingAddress') ? undefined : _1.AddressUpdateDataFromJSON(json['shippingAddress']),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (json['shipByDate'] === null ? null : new Date(json['shipByDate'])),
        'shipAfterDate': !runtime_1.exists(json, 'shipAfterDate') ? undefined : (json['shipAfterDate'] === null ? null : new Date(json['shipAfterDate'])),
        'expectedDeliveryDate': !runtime_1.exists(json, 'expectedDeliveryDate') ? undefined : (json['expectedDeliveryDate'] === null ? null : new Date(json['expectedDeliveryDate'])),
        'shippingNote': !runtime_1.exists(json, 'shippingNote') ? undefined : json['shippingNote'],
        'billingAddress': !runtime_1.exists(json, 'billingAddress') ? undefined : _1.AddressUpdateDataFromJSON(json['billingAddress']),
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (json['timestamp'] === null ? null : new Date(json['timestamp'])),
        'paymentStatus': !runtime_1.exists(json, 'paymentStatus') ? undefined : json['paymentStatus'],
        'fulfillmentStatus': !runtime_1.exists(json, 'fulfillmentStatus') ? undefined : json['fulfillmentStatus'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'extOrderApiId': !runtime_1.exists(json, 'extOrderApiId') ? undefined : json['extOrderApiId'],
        'extOrderCreatedAt': !runtime_1.exists(json, 'extOrderCreatedAt') ? undefined : (json['extOrderCreatedAt'] === null ? null : new Date(json['extOrderCreatedAt'])),
        'extOrderUpdatedAt': !runtime_1.exists(json, 'extOrderUpdatedAt') ? undefined : (json['extOrderUpdatedAt'] === null ? null : new Date(json['extOrderUpdatedAt'])),
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'shippingPrice': !runtime_1.exists(json, 'shippingPrice') ? undefined : json['shippingPrice'],
        'requireShippingLabels': !runtime_1.exists(json, 'requireShippingLabels') ? undefined : json['requireShippingLabels'],
        'customer': !runtime_1.exists(json, 'customer') ? undefined : _1.OrderCustomerFromJSON(json['customer']),
        'shippingCode': !runtime_1.exists(json, 'shippingCode') ? undefined : json['shippingCode'],
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'tags': !runtime_1.exists(json, 'tags') ? undefined : json['tags'],
        'payments': !runtime_1.exists(json, 'payments') ? undefined : (json['payments'].map(_1.PaymentFromJSON)),
    };
}
exports.OrderUpdateDataFromJSONTyped = OrderUpdateDataFromJSONTyped;
function OrderUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.OrderStatusToJSON(value.status),
        'orderSource': value.orderSource,
        'extOrderUrl': value.extOrderUrl,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.OrderUpdateLineItemToJSON)),
        'subTotalPrice': value.subTotalPrice,
        'orderDiscount': value.orderDiscount,
        'orderTax': value.orderTax,
        'totalPrice': value.totalPrice,
        'orderNote': value.orderNote,
        'giftNote': value.giftNote,
        'shippingAddress': _1.AddressUpdateDataToJSON(value.shippingAddress),
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'shipByDate': value.shipByDate === undefined ? undefined : (value.shipByDate === null ? null : new Date(value.shipByDate).toISOString()),
        'shipAfterDate': value.shipAfterDate === undefined ? undefined : (value.shipAfterDate === null ? null : new Date(value.shipAfterDate).toISOString()),
        'expectedDeliveryDate': value.expectedDeliveryDate === undefined ? undefined : (value.expectedDeliveryDate === null ? null : new Date(value.expectedDeliveryDate).toISOString()),
        'shippingNote': value.shippingNote,
        'billingAddress': _1.AddressUpdateDataToJSON(value.billingAddress),
        'timestamp': value.timestamp === undefined ? undefined : (value.timestamp === null ? null : new Date(value.timestamp).toISOString()),
        'paymentStatus': value.paymentStatus,
        'fulfillmentStatus': value.fulfillmentStatus,
        'extOrderId': value.extOrderId,
        'extOrderApiId': value.extOrderApiId,
        'extOrderCreatedAt': value.extOrderCreatedAt === undefined ? undefined : (value.extOrderCreatedAt === null ? null : new Date(value.extOrderCreatedAt).toISOString()),
        'extOrderUpdatedAt': value.extOrderUpdatedAt === undefined ? undefined : (value.extOrderUpdatedAt === null ? null : new Date(value.extOrderUpdatedAt).toISOString()),
        'locationId': value.locationId,
        'shippingPrice': value.shippingPrice,
        'requireShippingLabels': value.requireShippingLabels,
        'customer': _1.OrderCustomerToJSON(value.customer),
        'shippingCode': value.shippingCode,
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'tags': value.tags,
        'payments': value.payments === undefined ? undefined : (value.payments.map(_1.PaymentToJSON)),
    };
}
exports.OrderUpdateDataToJSON = OrderUpdateDataToJSON;
