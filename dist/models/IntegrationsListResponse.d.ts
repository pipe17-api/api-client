/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationBase, IntegrationsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface IntegrationsListResponse
 */
export interface IntegrationsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationsListResponse
     */
    success: IntegrationsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationsListResponse
     */
    code: IntegrationsListResponseCodeEnum;
    /**
     *
     * @type {IntegrationsListFilter}
     * @memberof IntegrationsListResponse
     */
    filters?: IntegrationsListFilter;
    /**
     *
     * @type {Array<IntegrationBase>}
     * @memberof IntegrationsListResponse
     */
    integrations?: Array<IntegrationBase>;
    /**
     *
     * @type {Pagination}
     * @memberof IntegrationsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationsListResponseFromJSON(json: any): IntegrationsListResponse;
export declare function IntegrationsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsListResponse;
export declare function IntegrationsListResponseToJSON(value?: IntegrationsListResponse | null): any;
