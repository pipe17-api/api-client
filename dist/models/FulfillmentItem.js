"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentItemToJSON = exports.FulfillmentItemFromJSONTyped = exports.FulfillmentItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function FulfillmentItemFromJSON(json) {
    return FulfillmentItemFromJSONTyped(json, false);
}
exports.FulfillmentItemFromJSON = FulfillmentItemFromJSON;
function FulfillmentItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': json['sku'],
        'quantity': json['quantity'],
        'uniqueId': !runtime_1.exists(json, 'uniqueId') ? undefined : json['uniqueId'],
    };
}
exports.FulfillmentItemFromJSONTyped = FulfillmentItemFromJSONTyped;
function FulfillmentItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'quantity': value.quantity,
        'uniqueId': value.uniqueId,
    };
}
exports.FulfillmentItemToJSON = FulfillmentItemToJSON;
