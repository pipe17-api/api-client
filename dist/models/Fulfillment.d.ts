/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentItemExt } from './';
/**
 *
 * @export
 * @interface Fulfillment
 */
export interface Fulfillment {
    /**
     * Fulfillment ID
     * @type {string}
     * @memberof Fulfillment
     */
    fulfillmentId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Fulfillment
     */
    integration?: string;
    /**
     * locationId of the shipment
     * @type {string}
     * @memberof Fulfillment
     */
    locationId?: string;
    /**
     *
     * @type {Array<FulfillmentItemExt>}
     * @memberof Fulfillment
     */
    lineItems?: Array<FulfillmentItemExt>;
    /**
     * Internal Pipe17 ID of the Shipment
     * @type {string}
     * @memberof Fulfillment
     */
    shipmentId?: string;
    /**
     * Tracking number(s)
     * @type {Array<string>}
     * @memberof Fulfillment
     */
    trackingNumber?: Array<string>;
    /**
     * Actual ship date
     * @type {Date}
     * @memberof Fulfillment
     */
    actualShipDate?: Date;
    /**
     * Expected arrival date
     * @type {Date}
     * @memberof Fulfillment
     */
    expectedArrivalDate?: Date;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof Fulfillment
     */
    shippingCarrier?: string;
    /**
     * Shipping Class
     * @type {string}
     * @memberof Fulfillment
     */
    shippingClass?: string;
    /**
     * Shipping Charge
     * @type {number}
     * @memberof Fulfillment
     */
    shippingCharge?: number;
    /**
     * Weight unit
     * @type {string}
     * @memberof Fulfillment
     */
    weightUnit?: FulfillmentWeightUnitEnum;
    /**
     * External customer friendly ID
     * @type {string}
     * @memberof Fulfillment
     */
    extOrderId?: string;
    /**
     * Weight
     * @type {number}
     * @memberof Fulfillment
     */
    weight?: number;
    /**
     * Reference to fulfillment on external system (vendor)
     * @type {string}
     * @memberof Fulfillment
     */
    extFulfillmentId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Fulfillment
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Fulfillment
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Fulfillment
     */
    readonly orgKey?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
}
export declare function FulfillmentFromJSON(json: any): Fulfillment;
export declare function FulfillmentFromJSONTyped(json: any, ignoreDiscriminator: boolean): Fulfillment;
export declare function FulfillmentToJSON(value?: Fulfillment | null): any;
