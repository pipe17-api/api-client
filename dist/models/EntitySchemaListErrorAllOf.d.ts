/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchemasListFilter } from './';
/**
 *
 * @export
 * @interface EntitySchemaListErrorAllOf
 */
export interface EntitySchemaListErrorAllOf {
    /**
     *
     * @type {EntitySchemasListFilter}
     * @memberof EntitySchemaListErrorAllOf
     */
    filters?: EntitySchemasListFilter;
}
export declare function EntitySchemaListErrorAllOfFromJSON(json: any): EntitySchemaListErrorAllOf;
export declare function EntitySchemaListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaListErrorAllOf;
export declare function EntitySchemaListErrorAllOfToJSON(value?: EntitySchemaListErrorAllOf | null): any;
