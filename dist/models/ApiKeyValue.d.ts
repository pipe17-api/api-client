/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, Methods, Tier } from './';
/**
 *
 * @export
 * @interface ApiKeyValue
 */
export interface ApiKeyValue {
    /**
     * API key Id
     * @type {string}
     * @memberof ApiKeyValue
     */
    apikeyId?: string;
    /**
     * API key value
     * @type {string}
     * @memberof ApiKeyValue
     */
    apikey?: string;
    /**
     * API Key Name
     * @type {string}
     * @memberof ApiKeyValue
     */
    name: string;
    /**
     * API Key Description
     * @type {string}
     * @memberof ApiKeyValue
     */
    description?: string;
    /**
     * Connector ID
     * @type {string}
     * @memberof ApiKeyValue
     */
    connector?: string;
    /**
     * Integration ID
     * @type {string}
     * @memberof ApiKeyValue
     */
    integration?: string;
    /**
     *
     * @type {Tier}
     * @memberof ApiKeyValue
     */
    tier?: Tier;
    /**
     *
     * @type {Env}
     * @memberof ApiKeyValue
     */
    environment?: Env;
    /**
     *
     * @type {Methods}
     * @memberof ApiKeyValue
     */
    methods: Methods;
    /**
     * IP/CIDR Whitelist
     * @type {Array<string>}
     * @memberof ApiKeyValue
     */
    allowedIPs: Array<string>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof ApiKeyValue
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof ApiKeyValue
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof ApiKeyValue
     */
    readonly orgKey?: string;
}
export declare function ApiKeyValueFromJSON(json: any): ApiKeyValue;
export declare function ApiKeyValueFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyValue;
export declare function ApiKeyValueToJSON(value?: ApiKeyValue | null): any;
