"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationCreateResponseToJSON = exports.IntegrationCreateResponseFromJSONTyped = exports.IntegrationCreateResponseFromJSON = exports.IntegrationCreateResponseCodeEnum = exports.IntegrationCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationCreateResponseSuccessEnum;
(function (IntegrationCreateResponseSuccessEnum) {
    IntegrationCreateResponseSuccessEnum["True"] = "true";
})(IntegrationCreateResponseSuccessEnum = exports.IntegrationCreateResponseSuccessEnum || (exports.IntegrationCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationCreateResponseCodeEnum;
(function (IntegrationCreateResponseCodeEnum) {
    IntegrationCreateResponseCodeEnum[IntegrationCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationCreateResponseCodeEnum[IntegrationCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationCreateResponseCodeEnum[IntegrationCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationCreateResponseCodeEnum = exports.IntegrationCreateResponseCodeEnum || (exports.IntegrationCreateResponseCodeEnum = {}));
function IntegrationCreateResponseFromJSON(json) {
    return IntegrationCreateResponseFromJSONTyped(json, false);
}
exports.IntegrationCreateResponseFromJSON = IntegrationCreateResponseFromJSON;
function IntegrationCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : _1.IntegrationFromJSON(json['integration']),
    };
}
exports.IntegrationCreateResponseFromJSONTyped = IntegrationCreateResponseFromJSONTyped;
function IntegrationCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'integration': _1.IntegrationToJSON(value.integration),
    };
}
exports.IntegrationCreateResponseToJSON = IntegrationCreateResponseToJSON;
