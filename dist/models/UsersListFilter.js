"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersListFilterToJSON = exports.UsersListFilterFromJSONTyped = exports.UsersListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UsersListFilterFromJSON(json) {
    return UsersListFilterFromJSONTyped(json, false);
}
exports.UsersListFilterFromJSON = UsersListFilterFromJSON;
function UsersListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'userId': !runtime_1.exists(json, 'userId') ? undefined : json['userId'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.UserStatusFromJSON)),
        'organizations': !runtime_1.exists(json, 'organizations') ? undefined : json['organizations'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
    };
}
exports.UsersListFilterFromJSONTyped = UsersListFilterFromJSONTyped;
function UsersListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'userId': value.userId,
        'name': value.name,
        'email': value.email,
        'status': value.status === undefined ? undefined : (value.status.map(_1.UserStatusToJSON)),
        'organizations': value.organizations,
        'order': value.order,
        'keys': value.keys,
    };
}
exports.UsersListFilterToJSON = UsersListFilterToJSON;
