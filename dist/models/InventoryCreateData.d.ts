/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventSource } from './';
/**
 *
 * @export
 * @interface InventoryCreateData
 */
export interface InventoryCreateData {
    /**
     * Event creating this item
     * @type {string}
     * @memberof InventoryCreateData
     */
    event: InventoryCreateDataEventEnum;
    /**
     *
     * @type {EventSource}
     * @memberof InventoryCreateData
     */
    ptype: EventSource;
    /**
     * Item SKU
     * @type {string}
     * @memberof InventoryCreateData
     */
    sku: string;
    /**
     * Location ID of item
     * @type {string}
     * @memberof InventoryCreateData
     */
    locationId: string;
    /**
     * Number of items on hand
     * @type {number}
     * @memberof InventoryCreateData
     */
    onHand: number;
    /**
     * Number of items committed
     * @type {number}
     * @memberof InventoryCreateData
     */
    committed: number;
    /**
     * Number of items committed for pre-order
     * @type {number}
     * @memberof InventoryCreateData
     */
    committedFuture?: number;
    /**
     * Number of items that can be sold for pre-order
     * @type {number}
     * @memberof InventoryCreateData
     */
    future?: number;
    /**
     * Number of items available
     * @type {number}
     * @memberof InventoryCreateData
     */
    available?: number;
    /**
     * Number of incoming items in transit (maybe included in available)
     * @type {number}
     * @memberof InventoryCreateData
     */
    incoming?: number;
    /**
     * Number of items being shipped (included in committed)
     * @type {number}
     * @memberof InventoryCreateData
     */
    commitShip?: number;
    /**
     * Number of items being transferred out (included in committed)
     * @type {number}
     * @memberof InventoryCreateData
     */
    commitXfer?: number;
    /**
     * Number of unavailable items
     * @type {number}
     * @memberof InventoryCreateData
     */
    unavailable?: number;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryCreateDataEventEnum {
    Ingest = "ingest",
    Adjust = "adjust"
}
export declare function InventoryCreateDataFromJSON(json: any): InventoryCreateData;
export declare function InventoryCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryCreateData;
export declare function InventoryCreateDataToJSON(value?: InventoryCreateData | null): any;
