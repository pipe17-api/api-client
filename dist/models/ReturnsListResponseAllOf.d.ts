/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Return, ReturnsFilter } from './';
/**
 *
 * @export
 * @interface ReturnsListResponseAllOf
 */
export interface ReturnsListResponseAllOf {
    /**
     *
     * @type {ReturnsFilter}
     * @memberof ReturnsListResponseAllOf
     */
    filters?: ReturnsFilter;
    /**
     *
     * @type {Array<Return>}
     * @memberof ReturnsListResponseAllOf
     */
    returns?: Array<Return>;
    /**
     *
     * @type {Pagination}
     * @memberof ReturnsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ReturnsListResponseAllOfFromJSON(json: any): ReturnsListResponseAllOf;
export declare function ReturnsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsListResponseAllOf;
export declare function ReturnsListResponseAllOfToJSON(value?: ReturnsListResponseAllOf | null): any;
