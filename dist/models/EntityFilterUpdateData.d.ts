/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityFilterUpdateData
 */
export interface EntityFilterUpdateData {
    /**
     * Filter query object
     * @type {object}
     * @memberof EntityFilterUpdateData
     */
    query?: object;
    /**
     * Filter name
     * @type {string}
     * @memberof EntityFilterUpdateData
     */
    name?: string;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof EntityFilterUpdateData
     */
    isPublic?: boolean;
    /**
     *
     * @type {string}
     * @memberof EntityFilterUpdateData
     */
    originId?: string;
}
export declare function EntityFilterUpdateDataFromJSON(json: any): EntityFilterUpdateData;
export declare function EntityFilterUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterUpdateData;
export declare function EntityFilterUpdateDataToJSON(value?: EntityFilterUpdateData | null): any;
