/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, NameValue, PurchaseCost, PurchaseLineItem, PurchaseStatus } from './';
/**
 *
 * @export
 * @interface PurchaseCreateData
 */
export interface PurchaseCreateData {
    /**
     *
     * @type {PurchaseStatus}
     * @memberof PurchaseCreateData
     */
    status?: PurchaseStatus;
    /**
     * External Purchase Order ID
     * @type {string}
     * @memberof PurchaseCreateData
     */
    extOrderId: string;
    /**
     * Purchase Order Date
     * @type {Date}
     * @memberof PurchaseCreateData
     */
    purchaseOrderDate?: Date;
    /**
     *
     * @type {Array<PurchaseLineItem>}
     * @memberof PurchaseCreateData
     */
    lineItems?: Array<PurchaseLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof PurchaseCreateData
     */
    subTotalPrice?: number;
    /**
     * Purchase Tax
     * @type {number}
     * @memberof PurchaseCreateData
     */
    purchaseTax?: number;
    /**
     * Purchase costs
     * @type {Array<PurchaseCost>}
     * @memberof PurchaseCreateData
     */
    costs?: Array<PurchaseCost> | null;
    /**
     * Total Price
     * @type {number}
     * @memberof PurchaseCreateData
     */
    totalPrice?: number;
    /**
     * Purchase order created by
     * @type {string}
     * @memberof PurchaseCreateData
     */
    employeeName?: string;
    /**
     * Purchase Notes
     * @type {string}
     * @memberof PurchaseCreateData
     */
    purchaseNotes?: string;
    /**
     *
     * @type {Address}
     * @memberof PurchaseCreateData
     */
    vendorAddress?: Address;
    /**
     *
     * @type {Address}
     * @memberof PurchaseCreateData
     */
    toAddress?: Address;
    /**
     * Ship to location ID
     * @type {string}
     * @memberof PurchaseCreateData
     */
    toLocationId?: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof PurchaseCreateData
     */
    shippingCarrier?: string;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof PurchaseCreateData
     */
    shippingService?: string;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof PurchaseCreateData
     */
    shipByDate?: Date;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof PurchaseCreateData
     */
    actualArrivalDate?: Date;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof PurchaseCreateData
     */
    expectedArrivalDate?: Date;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof PurchaseCreateData
     */
    shippingNotes?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof PurchaseCreateData
     */
    incoterms?: string;
    /**
     *
     * @type {Address}
     * @memberof PurchaseCreateData
     */
    billingAddress?: Address;
    /**
     * Purchase order reference number
     * @type {string}
     * @memberof PurchaseCreateData
     */
    referenceNumber?: string;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof PurchaseCreateData
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof PurchaseCreateData
     */
    timestamp?: Date;
}
export declare function PurchaseCreateDataFromJSON(json: any): PurchaseCreateData;
export declare function PurchaseCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseCreateData;
export declare function PurchaseCreateDataToJSON(value?: PurchaseCreateData | null): any;
