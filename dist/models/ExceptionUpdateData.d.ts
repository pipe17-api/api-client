/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionStatus } from './';
/**
 *
 * @export
 * @interface ExceptionUpdateData
 */
export interface ExceptionUpdateData {
    /**
     *
     * @type {ExceptionStatus}
     * @memberof ExceptionUpdateData
     */
    status: ExceptionStatus;
}
export declare function ExceptionUpdateDataFromJSON(json: any): ExceptionUpdateData;
export declare function ExceptionUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionUpdateData;
export declare function ExceptionUpdateDataToJSON(value?: ExceptionUpdateData | null): any;
