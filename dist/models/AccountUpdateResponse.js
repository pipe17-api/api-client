"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountUpdateResponseToJSON = exports.AccountUpdateResponseFromJSONTyped = exports.AccountUpdateResponseFromJSON = exports.AccountUpdateResponseCodeEnum = exports.AccountUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var AccountUpdateResponseSuccessEnum;
(function (AccountUpdateResponseSuccessEnum) {
    AccountUpdateResponseSuccessEnum["True"] = "true";
})(AccountUpdateResponseSuccessEnum = exports.AccountUpdateResponseSuccessEnum || (exports.AccountUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var AccountUpdateResponseCodeEnum;
(function (AccountUpdateResponseCodeEnum) {
    AccountUpdateResponseCodeEnum[AccountUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    AccountUpdateResponseCodeEnum[AccountUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    AccountUpdateResponseCodeEnum[AccountUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(AccountUpdateResponseCodeEnum = exports.AccountUpdateResponseCodeEnum || (exports.AccountUpdateResponseCodeEnum = {}));
function AccountUpdateResponseFromJSON(json) {
    return AccountUpdateResponseFromJSONTyped(json, false);
}
exports.AccountUpdateResponseFromJSON = AccountUpdateResponseFromJSON;
function AccountUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.AccountUpdateResponseFromJSONTyped = AccountUpdateResponseFromJSONTyped;
function AccountUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.AccountUpdateResponseToJSON = AccountUpdateResponseToJSON;
