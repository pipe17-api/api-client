/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilter } from './';
/**
 *
 * @export
 * @interface ExceptionFilterCreateResponseAllOf
 */
export interface ExceptionFilterCreateResponseAllOf {
    /**
     *
     * @type {ExceptionFilter}
     * @memberof ExceptionFilterCreateResponseAllOf
     */
    exceptionFilter?: ExceptionFilter;
}
export declare function ExceptionFilterCreateResponseAllOfFromJSON(json: any): ExceptionFilterCreateResponseAllOf;
export declare function ExceptionFilterCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterCreateResponseAllOf;
export declare function ExceptionFilterCreateResponseAllOfToJSON(value?: ExceptionFilterCreateResponseAllOf | null): any;
