"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryListErrorAllOfToJSON = exports.InventoryListErrorAllOfFromJSONTyped = exports.InventoryListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryListErrorAllOfFromJSON(json) {
    return InventoryListErrorAllOfFromJSONTyped(json, false);
}
exports.InventoryListErrorAllOfFromJSON = InventoryListErrorAllOfFromJSON;
function InventoryListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryListFilterFromJSON(json['filters']),
    };
}
exports.InventoryListErrorAllOfFromJSONTyped = InventoryListErrorAllOfFromJSONTyped;
function InventoryListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.InventoryListFilterToJSON(value.filters),
    };
}
exports.InventoryListErrorAllOfToJSON = InventoryListErrorAllOfToJSON;
