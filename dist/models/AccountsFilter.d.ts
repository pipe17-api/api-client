/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 *
 * @export
 * @interface AccountsFilter
 */
export interface AccountsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof AccountsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof AccountsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof AccountsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof AccountsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof AccountsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof AccountsFilter
     */
    count?: number;
    /**
     * Accounts by list of orgKey
     * @type {Array<string>}
     * @memberof AccountsFilter
     */
    orgKey?: Array<string>;
    /**
     * Accounts by list of status
     * @type {Array<AccountStatus>}
     * @memberof AccountsFilter
     */
    status?: Array<AccountStatus>;
}
export declare function AccountsFilterFromJSON(json: any): AccountsFilter;
export declare function AccountsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsFilter;
export declare function AccountsFilterToJSON(value?: AccountsFilter | null): any;
