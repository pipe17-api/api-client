"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreateErrorToJSON = exports.UserCreateErrorFromJSONTyped = exports.UserCreateErrorFromJSON = exports.UserCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UserCreateErrorSuccessEnum;
(function (UserCreateErrorSuccessEnum) {
    UserCreateErrorSuccessEnum["False"] = "false";
})(UserCreateErrorSuccessEnum = exports.UserCreateErrorSuccessEnum || (exports.UserCreateErrorSuccessEnum = {}));
function UserCreateErrorFromJSON(json) {
    return UserCreateErrorFromJSONTyped(json, false);
}
exports.UserCreateErrorFromJSON = UserCreateErrorFromJSON;
function UserCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserCreateDataFromJSON(json['user']),
    };
}
exports.UserCreateErrorFromJSONTyped = UserCreateErrorFromJSONTyped;
function UserCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'user': _1.UserCreateDataToJSON(value.user),
    };
}
exports.UserCreateErrorToJSON = UserCreateErrorToJSON;
