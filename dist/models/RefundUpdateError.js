"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundUpdateErrorToJSON = exports.RefundUpdateErrorFromJSONTyped = exports.RefundUpdateErrorFromJSON = exports.RefundUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RefundUpdateErrorSuccessEnum;
(function (RefundUpdateErrorSuccessEnum) {
    RefundUpdateErrorSuccessEnum["False"] = "false";
})(RefundUpdateErrorSuccessEnum = exports.RefundUpdateErrorSuccessEnum || (exports.RefundUpdateErrorSuccessEnum = {}));
function RefundUpdateErrorFromJSON(json) {
    return RefundUpdateErrorFromJSONTyped(json, false);
}
exports.RefundUpdateErrorFromJSON = RefundUpdateErrorFromJSON;
function RefundUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'refund': !runtime_1.exists(json, 'refund') ? undefined : _1.RefundUpdateDataFromJSON(json['refund']),
    };
}
exports.RefundUpdateErrorFromJSONTyped = RefundUpdateErrorFromJSONTyped;
function RefundUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'refund': _1.RefundUpdateDataToJSON(value.refund),
    };
}
exports.RefundUpdateErrorToJSON = RefundUpdateErrorToJSON;
