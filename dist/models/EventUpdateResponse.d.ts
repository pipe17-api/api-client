/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventUpdateResponse
 */
export interface EventUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EventUpdateResponse
     */
    success: EventUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventUpdateResponse
     */
    code: EventUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum EventUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EventUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EventUpdateResponseFromJSON(json: any): EventUpdateResponse;
export declare function EventUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventUpdateResponse;
export declare function EventUpdateResponseToJSON(value?: EventUpdateResponse | null): any;
