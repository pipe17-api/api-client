/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsDeleteErrorAllOf
 */
export interface ShipmentsDeleteErrorAllOf {
    /**
     *
     * @type {ShipmentsDeleteFilter}
     * @memberof ShipmentsDeleteErrorAllOf
     */
    filters?: ShipmentsDeleteFilter;
}
export declare function ShipmentsDeleteErrorAllOfFromJSON(json: any): ShipmentsDeleteErrorAllOf;
export declare function ShipmentsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsDeleteErrorAllOf;
export declare function ShipmentsDeleteErrorAllOfToJSON(value?: ShipmentsDeleteErrorAllOf | null): any;
