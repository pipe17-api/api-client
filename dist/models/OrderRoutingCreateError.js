"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingCreateErrorToJSON = exports.OrderRoutingCreateErrorFromJSONTyped = exports.OrderRoutingCreateErrorFromJSON = exports.OrderRoutingCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderRoutingCreateErrorSuccessEnum;
(function (OrderRoutingCreateErrorSuccessEnum) {
    OrderRoutingCreateErrorSuccessEnum["False"] = "false";
})(OrderRoutingCreateErrorSuccessEnum = exports.OrderRoutingCreateErrorSuccessEnum || (exports.OrderRoutingCreateErrorSuccessEnum = {}));
function OrderRoutingCreateErrorFromJSON(json) {
    return OrderRoutingCreateErrorFromJSONTyped(json, false);
}
exports.OrderRoutingCreateErrorFromJSON = OrderRoutingCreateErrorFromJSON;
function OrderRoutingCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.OrderRoutingFromJSON(json['routing']),
    };
}
exports.OrderRoutingCreateErrorFromJSONTyped = OrderRoutingCreateErrorFromJSONTyped;
function OrderRoutingCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'routing': _1.OrderRoutingToJSON(value.routing),
    };
}
exports.OrderRoutingCreateErrorToJSON = OrderRoutingCreateErrorToJSON;
