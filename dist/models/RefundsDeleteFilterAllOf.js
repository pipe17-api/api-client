"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsDeleteFilterAllOfToJSON = exports.RefundsDeleteFilterAllOfFromJSONTyped = exports.RefundsDeleteFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function RefundsDeleteFilterAllOfFromJSON(json) {
    return RefundsDeleteFilterAllOfFromJSONTyped(json, false);
}
exports.RefundsDeleteFilterAllOfFromJSON = RefundsDeleteFilterAllOfFromJSON;
function RefundsDeleteFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.RefundsDeleteFilterAllOfFromJSONTyped = RefundsDeleteFilterAllOfFromJSONTyped;
function RefundsDeleteFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
    };
}
exports.RefundsDeleteFilterAllOfToJSON = RefundsDeleteFilterAllOfToJSON;
