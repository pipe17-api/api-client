/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobSubType, JobType } from './';
/**
 *
 * @export
 * @interface JobCreateData
 */
export interface JobCreateData {
    /**
     *
     * @type {JobType}
     * @memberof JobCreateData
     */
    type: JobType;
    /**
     *
     * @type {JobSubType}
     * @memberof JobCreateData
     */
    subType: JobSubType;
    /**
     * Job type specific parameters
     * @type {object}
     * @memberof JobCreateData
     */
    params?: object;
    /**
     * Job result content type
     * @type {string}
     * @memberof JobCreateData
     */
    contentType?: string;
    /**
     * Job timeout (in milliseconds)
     * @type {number}
     * @memberof JobCreateData
     */
    timeout?: number;
}
export declare function JobCreateDataFromJSON(json: any): JobCreateData;
export declare function JobCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobCreateData;
export declare function JobCreateDataToJSON(value?: JobCreateData | null): any;
