"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesListErrorToJSON = exports.PurchasesListErrorFromJSONTyped = exports.PurchasesListErrorFromJSON = exports.PurchasesListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchasesListErrorSuccessEnum;
(function (PurchasesListErrorSuccessEnum) {
    PurchasesListErrorSuccessEnum["False"] = "false";
})(PurchasesListErrorSuccessEnum = exports.PurchasesListErrorSuccessEnum || (exports.PurchasesListErrorSuccessEnum = {}));
function PurchasesListErrorFromJSON(json) {
    return PurchasesListErrorFromJSONTyped(json, false);
}
exports.PurchasesListErrorFromJSON = PurchasesListErrorFromJSON;
function PurchasesListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesListFilterFromJSON(json['filters']),
    };
}
exports.PurchasesListErrorFromJSONTyped = PurchasesListErrorFromJSONTyped;
function PurchasesListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.PurchasesListFilterToJSON(value.filters),
    };
}
exports.PurchasesListErrorToJSON = PurchasesListErrorToJSON;
