/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Product } from './';
/**
 *
 * @export
 * @interface ProductFetchResponseAllOf
 */
export interface ProductFetchResponseAllOf {
    /**
     *
     * @type {Product}
     * @memberof ProductFetchResponseAllOf
     */
    product?: Product;
}
export declare function ProductFetchResponseAllOfFromJSON(json: any): ProductFetchResponseAllOf;
export declare function ProductFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductFetchResponseAllOf;
export declare function ProductFetchResponseAllOfToJSON(value?: ProductFetchResponseAllOf | null): any;
