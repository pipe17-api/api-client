/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Connector Type
 * @export
 * @enum {string}
 */
export declare enum ConnectorType {
    Ecommerce = "ecommerce",
    Finance = "finance",
    Logistics = "logistics",
    Plugin = "plugin",
    Other = "other"
}
export declare function ConnectorTypeFromJSON(json: any): ConnectorType;
export declare function ConnectorTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorType;
export declare function ConnectorTypeToJSON(value?: ConnectorType | null): any;
