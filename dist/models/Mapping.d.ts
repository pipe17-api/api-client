/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingRule } from './';
/**
 *
 * @export
 * @interface Mapping
 */
export interface Mapping {
    /**
     * Mapping ID
     * @type {string}
     * @memberof Mapping
     */
    mappingId?: string;
    /**
     * Connector that uses this mapping
     * @type {string}
     * @memberof Mapping
     */
    connectorId?: string;
    /**
     * Public mapping indicator
     * @type {boolean}
     * @memberof Mapping
     */
    isPublic: boolean;
    /**
     * Description
     * @type {string}
     * @memberof Mapping
     */
    description: string;
    /**
     *
     * @type {Array<MappingRule>}
     * @memberof Mapping
     */
    mappings?: Array<MappingRule>;
    /**
     * UUID (generated if not provided)
     * @type {string}
     * @memberof Mapping
     */
    uuid?: string;
    /**
     *
     * @type {string}
     * @memberof Mapping
     */
    originId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Mapping
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Mapping
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Mapping
     */
    readonly orgKey?: string;
}
export declare function MappingFromJSON(json: any): Mapping;
export declare function MappingFromJSONTyped(json: any, ignoreDiscriminator: boolean): Mapping;
export declare function MappingToJSON(value?: Mapping | null): any;
