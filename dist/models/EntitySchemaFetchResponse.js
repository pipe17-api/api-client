"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaFetchResponseToJSON = exports.EntitySchemaFetchResponseFromJSONTyped = exports.EntitySchemaFetchResponseFromJSON = exports.EntitySchemaFetchResponseCodeEnum = exports.EntitySchemaFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntitySchemaFetchResponseSuccessEnum;
(function (EntitySchemaFetchResponseSuccessEnum) {
    EntitySchemaFetchResponseSuccessEnum["True"] = "true";
})(EntitySchemaFetchResponseSuccessEnum = exports.EntitySchemaFetchResponseSuccessEnum || (exports.EntitySchemaFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntitySchemaFetchResponseCodeEnum;
(function (EntitySchemaFetchResponseCodeEnum) {
    EntitySchemaFetchResponseCodeEnum[EntitySchemaFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntitySchemaFetchResponseCodeEnum[EntitySchemaFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntitySchemaFetchResponseCodeEnum[EntitySchemaFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntitySchemaFetchResponseCodeEnum = exports.EntitySchemaFetchResponseCodeEnum || (exports.EntitySchemaFetchResponseCodeEnum = {}));
function EntitySchemaFetchResponseFromJSON(json) {
    return EntitySchemaFetchResponseFromJSONTyped(json, false);
}
exports.EntitySchemaFetchResponseFromJSON = EntitySchemaFetchResponseFromJSON;
function EntitySchemaFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'entitySchema': !runtime_1.exists(json, 'entitySchema') ? undefined : _1.EntitySchemaFromJSON(json['entitySchema']),
    };
}
exports.EntitySchemaFetchResponseFromJSONTyped = EntitySchemaFetchResponseFromJSONTyped;
function EntitySchemaFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'entitySchema': _1.EntitySchemaToJSON(value.entitySchema),
    };
}
exports.EntitySchemaFetchResponseToJSON = EntitySchemaFetchResponseToJSON;
