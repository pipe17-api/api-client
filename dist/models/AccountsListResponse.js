"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsListResponseToJSON = exports.AccountsListResponseFromJSONTyped = exports.AccountsListResponseFromJSON = exports.AccountsListResponseCodeEnum = exports.AccountsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var AccountsListResponseSuccessEnum;
(function (AccountsListResponseSuccessEnum) {
    AccountsListResponseSuccessEnum["True"] = "true";
})(AccountsListResponseSuccessEnum = exports.AccountsListResponseSuccessEnum || (exports.AccountsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var AccountsListResponseCodeEnum;
(function (AccountsListResponseCodeEnum) {
    AccountsListResponseCodeEnum[AccountsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    AccountsListResponseCodeEnum[AccountsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    AccountsListResponseCodeEnum[AccountsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(AccountsListResponseCodeEnum = exports.AccountsListResponseCodeEnum || (exports.AccountsListResponseCodeEnum = {}));
function AccountsListResponseFromJSON(json) {
    return AccountsListResponseFromJSONTyped(json, false);
}
exports.AccountsListResponseFromJSON = AccountsListResponseFromJSON;
function AccountsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.AccountsListFilterFromJSON(json['filters']),
        'accounts': !runtime_1.exists(json, 'accounts') ? undefined : (json['accounts'].map(_1.AccountFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.AccountsListResponseFromJSONTyped = AccountsListResponseFromJSONTyped;
function AccountsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.AccountsListFilterToJSON(value.filters),
        'accounts': value.accounts === undefined ? undefined : (value.accounts.map(_1.AccountToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.AccountsListResponseToJSON = AccountsListResponseToJSON;
