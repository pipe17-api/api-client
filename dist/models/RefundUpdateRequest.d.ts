/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus } from './';
/**
 *
 * @export
 * @interface RefundUpdateRequest
 */
export interface RefundUpdateRequest {
    /**
     *
     * @type {RefundStatus}
     * @memberof RefundUpdateRequest
     */
    status?: RefundStatus;
    /**
     * Refund amount
     * @type {number}
     * @memberof RefundUpdateRequest
     */
    amount?: number;
    /**
     * Credit issued to customer
     * @type {number}
     * @memberof RefundUpdateRequest
     */
    creditIssued?: number;
    /**
     * Credit spent by customer
     * @type {number}
     * @memberof RefundUpdateRequest
     */
    creditSpent?: number;
    /**
     * Merchant invoice amount
     * @type {number}
     * @memberof RefundUpdateRequest
     */
    merchantInvoiceAmount?: number;
    /**
     * Customer invoice amount
     * @type {number}
     * @memberof RefundUpdateRequest
     */
    customerInvoiceAmount?: number;
}
export declare function RefundUpdateRequestFromJSON(json: any): RefundUpdateRequest;
export declare function RefundUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundUpdateRequest;
export declare function RefundUpdateRequestToJSON(value?: RefundUpdateRequest | null): any;
