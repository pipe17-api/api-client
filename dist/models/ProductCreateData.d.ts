/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { NameValue, Price, ProductBundleItem, ProductPublishedItem, ProductStatus, ProductType } from './';
/**
 * We don't allow duplicated SKU within the system
 * @export
 * @interface ProductCreateData
 */
export interface ProductCreateData {
    /**
     * Product SKU
     * @type {string}
     * @memberof ProductCreateData
     */
    sku: string;
    /**
     * Display Name
     * @type {string}
     * @memberof ProductCreateData
     */
    name: string;
    /**
     *
     * @type {ProductStatus}
     * @memberof ProductCreateData
     */
    status?: ProductStatus;
    /**
     * Description
     * @type {string}
     * @memberof ProductCreateData
     */
    description?: string;
    /**
     * Product types
     * @type {Array<ProductType>}
     * @memberof ProductCreateData
     */
    types?: Array<ProductType>;
    /**
     * External Product Id
     * @type {string}
     * @memberof ProductCreateData
     */
    extProductId?: string;
    /**
     * External Product API Id
     * @type {string}
     * @memberof ProductCreateData
     */
    extProductApiId?: string;
    /**
     * UPC
     * @type {string}
     * @memberof ProductCreateData
     */
    upc?: string;
    /**
     * Manufacturer part number
     * @type {string}
     * @memberof ProductCreateData
     */
    partId?: string;
    /**
     * Start off with basePrice
     * @type {Array<Price>}
     * @memberof ProductCreateData
     */
    prices?: Array<Price>;
    /**
     * Cost
     * @type {number}
     * @memberof ProductCreateData
     */
    cost?: number;
    /**
     * Currency
     * @type {string}
     * @memberof ProductCreateData
     */
    costCurrency?: string;
    /**
     * Taxable
     * @type {boolean}
     * @memberof ProductCreateData
     */
    taxable?: boolean;
    /**
     * Country of Origin
     * @type {string}
     * @memberof ProductCreateData
     */
    countryOfOrigin?: string;
    /**
     * Harmonized System/Tarrif Code
     * @type {string}
     * @memberof ProductCreateData
     */
    harmonizedCode?: string;
    /**
     * Weight
     * @type {number}
     * @memberof ProductCreateData
     */
    weight?: number;
    /**
     * Weight UOM
     * @type {string}
     * @memberof ProductCreateData
     */
    weightUnit?: ProductCreateDataWeightUnitEnum;
    /**
     * Height
     * @type {number}
     * @memberof ProductCreateData
     */
    height?: number;
    /**
     * Length
     * @type {number}
     * @memberof ProductCreateData
     */
    length?: number;
    /**
     * Width
     * @type {number}
     * @memberof ProductCreateData
     */
    width?: number;
    /**
     * Dimensions UOM
     * @type {string}
     * @memberof ProductCreateData
     */
    dimensionsUnit?: ProductCreateDataDimensionsUnitEnum;
    /**
     * vendor
     * @type {string}
     * @memberof ProductCreateData
     */
    vendor?: string;
    /**
     * Tags
     * @type {Array<string>}
     * @memberof ProductCreateData
     */
    tags?: Array<string>;
    /**
     * Image URLs
     * @type {Array<string>}
     * @memberof ProductCreateData
     */
    imageURLs?: Array<string>;
    /**
     * Variant parent external product Id
     * @type {string}
     * @memberof ProductCreateData
     */
    parentExtProductId?: string;
    /**
     * this let us know the variant description
     * @type {Array<NameValue>}
     * @memberof ProductCreateData
     */
    variantOptions?: Array<NameValue>;
    /**
     * Bundle Item need to reference the SKU of the actual product
     * @type {Array<ProductBundleItem>}
     * @memberof ProductCreateData
     */
    bundleItems?: Array<ProductBundleItem>;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof ProductCreateData
     */
    customFields?: Array<NameValue>;
    /**
     * Published Channels
     * @type {Array<ProductPublishedItem>}
     * @memberof ProductCreateData
     */
    published?: Array<ProductPublishedItem>;
    /**
     * Do not track inventory flag
     * @type {boolean}
     * @memberof ProductCreateData
     */
    inventoryNotTracked?: boolean;
    /**
     * Mark item as hidden in marketplace Storefront
     * @type {boolean}
     * @memberof ProductCreateData
     */
    hideInStore?: boolean;
    /**
     *
     * @type {Date}
     * @memberof ProductCreateData
     */
    extProductCreatedAt?: Date;
    /**
     *
     * @type {Date}
     * @memberof ProductCreateData
     */
    extProductUpdatedAt?: Date;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductCreateDataWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
} /**
* @export
* @enum {string}
*/
export declare enum ProductCreateDataDimensionsUnitEnum {
    Cm = "cm",
    In = "in",
    Ft = "ft"
}
export declare function ProductCreateDataFromJSON(json: any): ProductCreateData;
export declare function ProductCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductCreateData;
export declare function ProductCreateDataToJSON(value?: ProductCreateData | null): any;
