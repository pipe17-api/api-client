/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Routing, RoutingsListFilter } from './';
/**
 *
 * @export
 * @interface RoutingsListResponse
 */
export interface RoutingsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoutingsListResponse
     */
    success: RoutingsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingsListResponse
     */
    code: RoutingsListResponseCodeEnum;
    /**
     *
     * @type {RoutingsListFilter}
     * @memberof RoutingsListResponse
     */
    filters?: RoutingsListFilter;
    /**
     *
     * @type {Array<Routing>}
     * @memberof RoutingsListResponse
     */
    routings?: Array<Routing>;
    /**
     *
     * @type {Pagination}
     * @memberof RoutingsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoutingsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoutingsListResponseFromJSON(json: any): RoutingsListResponse;
export declare function RoutingsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsListResponse;
export declare function RoutingsListResponseToJSON(value?: RoutingsListResponse | null): any;
