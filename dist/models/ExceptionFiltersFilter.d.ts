/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionFiltersFilter
 */
export interface ExceptionFiltersFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ExceptionFiltersFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ExceptionFiltersFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ExceptionFiltersFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ExceptionFiltersFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ExceptionFiltersFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ExceptionFiltersFilter
     */
    count?: number;
    /**
     * Filters by filterId-list
     * @type {Array<string>}
     * @memberof ExceptionFiltersFilter
     */
    filterId?: Array<string>;
    /**
     * Filters by exceptionType-list
     * @type {Array<string>}
     * @memberof ExceptionFiltersFilter
     */
    exceptionType?: Array<string>;
}
export declare function ExceptionFiltersFilterFromJSON(json: any): ExceptionFiltersFilter;
export declare function ExceptionFiltersFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFiltersFilter;
export declare function ExceptionFiltersFilterToJSON(value?: ExceptionFiltersFilter | null): any;
