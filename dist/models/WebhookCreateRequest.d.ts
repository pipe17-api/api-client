/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface WebhookCreateRequest
 */
export interface WebhookCreateRequest {
    /**
     * Webhook URL
     * @type {string}
     * @memberof WebhookCreateRequest
     */
    url: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof WebhookCreateRequest
     */
    apikey: string;
    /**
     * Webhook topics
     * @type {Array<WebhookTopics>}
     * @memberof WebhookCreateRequest
     */
    topics: Array<WebhookTopics>;
}
export declare function WebhookCreateRequestFromJSON(json: any): WebhookCreateRequest;
export declare function WebhookCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookCreateRequest;
export declare function WebhookCreateRequestToJSON(value?: WebhookCreateRequest | null): any;
