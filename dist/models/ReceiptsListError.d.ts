/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptsListFilter } from './';
/**
 *
 * @export
 * @interface ReceiptsListError
 */
export interface ReceiptsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReceiptsListError
     */
    success: ReceiptsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReceiptsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReceiptsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReceiptsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ReceiptsListFilter}
     * @memberof ReceiptsListError
     */
    filters?: ReceiptsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptsListErrorSuccessEnum {
    False = "false"
}
export declare function ReceiptsListErrorFromJSON(json: any): ReceiptsListError;
export declare function ReceiptsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsListError;
export declare function ReceiptsListErrorToJSON(value?: ReceiptsListError | null): any;
