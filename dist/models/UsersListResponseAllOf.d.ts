/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, User, UsersListFilter } from './';
/**
 *
 * @export
 * @interface UsersListResponseAllOf
 */
export interface UsersListResponseAllOf {
    /**
     *
     * @type {UsersListFilter}
     * @memberof UsersListResponseAllOf
     */
    filters?: UsersListFilter;
    /**
     *
     * @type {Array<User>}
     * @memberof UsersListResponseAllOf
     */
    users?: Array<User>;
    /**
     *
     * @type {Pagination}
     * @memberof UsersListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function UsersListResponseAllOfFromJSON(json: any): UsersListResponseAllOf;
export declare function UsersListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersListResponseAllOf;
export declare function UsersListResponseAllOfToJSON(value?: UsersListResponseAllOf | null): any;
