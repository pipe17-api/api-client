/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Shipment } from './';
/**
 *
 * @export
 * @interface ShipmentCreateResult
 */
export interface ShipmentCreateResult {
    /**
     *
     * @type {string}
     * @memberof ShipmentCreateResult
     */
    status?: ShipmentCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof ShipmentCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof ShipmentCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Shipment}
     * @memberof ShipmentCreateResult
     */
    shipment?: Shipment;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function ShipmentCreateResultFromJSON(json: any): ShipmentCreateResult;
export declare function ShipmentCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentCreateResult;
export declare function ShipmentCreateResultToJSON(value?: ShipmentCreateResult | null): any;
