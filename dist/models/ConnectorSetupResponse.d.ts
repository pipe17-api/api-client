/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorSetupResponse
 */
export interface ConnectorSetupResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ConnectorSetupResponse
     */
    success: ConnectorSetupResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorSetupResponse
     */
    code: ConnectorSetupResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorSetupResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ConnectorSetupResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ConnectorSetupResponseFromJSON(json: any): ConnectorSetupResponse;
export declare function ConnectorSetupResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSetupResponse;
export declare function ConnectorSetupResponseToJSON(value?: ConnectorSetupResponse | null): any;
