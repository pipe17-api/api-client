"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationUpdateResponseToJSON = exports.IntegrationUpdateResponseFromJSONTyped = exports.IntegrationUpdateResponseFromJSON = exports.IntegrationUpdateResponseCodeEnum = exports.IntegrationUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var IntegrationUpdateResponseSuccessEnum;
(function (IntegrationUpdateResponseSuccessEnum) {
    IntegrationUpdateResponseSuccessEnum["True"] = "true";
})(IntegrationUpdateResponseSuccessEnum = exports.IntegrationUpdateResponseSuccessEnum || (exports.IntegrationUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationUpdateResponseCodeEnum;
(function (IntegrationUpdateResponseCodeEnum) {
    IntegrationUpdateResponseCodeEnum[IntegrationUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationUpdateResponseCodeEnum[IntegrationUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationUpdateResponseCodeEnum[IntegrationUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationUpdateResponseCodeEnum = exports.IntegrationUpdateResponseCodeEnum || (exports.IntegrationUpdateResponseCodeEnum = {}));
function IntegrationUpdateResponseFromJSON(json) {
    return IntegrationUpdateResponseFromJSONTyped(json, false);
}
exports.IntegrationUpdateResponseFromJSON = IntegrationUpdateResponseFromJSON;
function IntegrationUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.IntegrationUpdateResponseFromJSONTyped = IntegrationUpdateResponseFromJSONTyped;
function IntegrationUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.IntegrationUpdateResponseToJSON = IntegrationUpdateResponseToJSON;
