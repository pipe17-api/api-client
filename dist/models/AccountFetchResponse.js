"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountFetchResponseToJSON = exports.AccountFetchResponseFromJSONTyped = exports.AccountFetchResponseFromJSON = exports.AccountFetchResponseCodeEnum = exports.AccountFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var AccountFetchResponseSuccessEnum;
(function (AccountFetchResponseSuccessEnum) {
    AccountFetchResponseSuccessEnum["True"] = "true";
})(AccountFetchResponseSuccessEnum = exports.AccountFetchResponseSuccessEnum || (exports.AccountFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var AccountFetchResponseCodeEnum;
(function (AccountFetchResponseCodeEnum) {
    AccountFetchResponseCodeEnum[AccountFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    AccountFetchResponseCodeEnum[AccountFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    AccountFetchResponseCodeEnum[AccountFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(AccountFetchResponseCodeEnum = exports.AccountFetchResponseCodeEnum || (exports.AccountFetchResponseCodeEnum = {}));
function AccountFetchResponseFromJSON(json) {
    return AccountFetchResponseFromJSONTyped(json, false);
}
exports.AccountFetchResponseFromJSON = AccountFetchResponseFromJSON;
function AccountFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountFromJSON(json['account']),
    };
}
exports.AccountFetchResponseFromJSONTyped = AccountFetchResponseFromJSONTyped;
function AccountFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'account': _1.AccountToJSON(value.account),
    };
}
exports.AccountFetchResponseToJSON = AccountFetchResponseToJSON;
