/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundCreateData } from './';
/**
 *
 * @export
 * @interface RefundCreateError
 */
export interface RefundCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RefundCreateError
     */
    success: RefundCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RefundCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RefundCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RefundCreateData}
     * @memberof RefundCreateError
     */
    refund?: RefundCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundCreateErrorSuccessEnum {
    False = "false"
}
export declare function RefundCreateErrorFromJSON(json: any): RefundCreateError;
export declare function RefundCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundCreateError;
export declare function RefundCreateErrorToJSON(value?: RefundCreateError | null): any;
