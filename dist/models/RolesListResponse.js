"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesListResponseToJSON = exports.RolesListResponseFromJSONTyped = exports.RolesListResponseFromJSON = exports.RolesListResponseCodeEnum = exports.RolesListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RolesListResponseSuccessEnum;
(function (RolesListResponseSuccessEnum) {
    RolesListResponseSuccessEnum["True"] = "true";
})(RolesListResponseSuccessEnum = exports.RolesListResponseSuccessEnum || (exports.RolesListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RolesListResponseCodeEnum;
(function (RolesListResponseCodeEnum) {
    RolesListResponseCodeEnum[RolesListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RolesListResponseCodeEnum[RolesListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RolesListResponseCodeEnum[RolesListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RolesListResponseCodeEnum = exports.RolesListResponseCodeEnum || (exports.RolesListResponseCodeEnum = {}));
function RolesListResponseFromJSON(json) {
    return RolesListResponseFromJSONTyped(json, false);
}
exports.RolesListResponseFromJSON = RolesListResponseFromJSON;
function RolesListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RolesListFilterFromJSON(json['filters']),
        'roles': !runtime_1.exists(json, 'roles') ? undefined : (json['roles'].map(_1.RoleFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RolesListResponseFromJSONTyped = RolesListResponseFromJSONTyped;
function RolesListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.RolesListFilterToJSON(value.filters),
        'roles': value.roles === undefined ? undefined : (value.roles.map(_1.RoleToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RolesListResponseToJSON = RolesListResponseToJSON;
