/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentUpdateData } from './';
/**
 *
 * @export
 * @interface ShipmentUpdateErrorAllOf
 */
export interface ShipmentUpdateErrorAllOf {
    /**
     *
     * @type {ShipmentUpdateData}
     * @memberof ShipmentUpdateErrorAllOf
     */
    shipment?: ShipmentUpdateData;
}
export declare function ShipmentUpdateErrorAllOfFromJSON(json: any): ShipmentUpdateErrorAllOf;
export declare function ShipmentUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentUpdateErrorAllOf;
export declare function ShipmentUpdateErrorAllOfToJSON(value?: ShipmentUpdateErrorAllOf | null): any;
