/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionType } from './';
/**
 *
 * @export
 * @interface ExceptionFilterUpdateRequest
 */
export interface ExceptionFilterUpdateRequest {
    /**
     *
     * @type {ExceptionType}
     * @memberof ExceptionFilterUpdateRequest
     */
    exceptionType?: ExceptionType;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilterUpdateRequest
     */
    enabled?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilterUpdateRequest
     */
    blocking?: boolean;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof ExceptionFilterUpdateRequest
     */
    isPublic?: boolean;
    /**
     *
     * @type {object}
     * @memberof ExceptionFilterUpdateRequest
     */
    settings?: object;
    /**
     *
     * @type {string}
     * @memberof ExceptionFilterUpdateRequest
     */
    originId?: string;
}
export declare function ExceptionFilterUpdateRequestFromJSON(json: any): ExceptionFilterUpdateRequest;
export declare function ExceptionFilterUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterUpdateRequest;
export declare function ExceptionFilterUpdateRequestToJSON(value?: ExceptionFilterUpdateRequest | null): any;
