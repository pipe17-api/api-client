"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsFilterAllOfToJSON = exports.ReceiptsFilterAllOfFromJSONTyped = exports.ReceiptsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ReceiptsFilterAllOfFromJSON(json) {
    return ReceiptsFilterAllOfFromJSONTyped(json, false);
}
exports.ReceiptsFilterAllOfFromJSON = ReceiptsFilterAllOfFromJSON;
function ReceiptsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'receiptId': !runtime_1.exists(json, 'receiptId') ? undefined : json['receiptId'],
        'arrivalId': !runtime_1.exists(json, 'arrivalId') ? undefined : json['arrivalId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
    };
}
exports.ReceiptsFilterAllOfFromJSONTyped = ReceiptsFilterAllOfFromJSONTyped;
function ReceiptsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'receiptId': value.receiptId,
        'arrivalId': value.arrivalId,
        'extOrderId': value.extOrderId,
    };
}
exports.ReceiptsFilterAllOfToJSON = ReceiptsFilterAllOfToJSON;
