"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobStatusToJSON = exports.JobStatusFromJSONTyped = exports.JobStatusFromJSON = exports.JobStatus = void 0;
/**
 * Job status
 * @export
 * @enum {string}
 */
var JobStatus;
(function (JobStatus) {
    JobStatus["Submitted"] = "submitted";
    JobStatus["Processing"] = "processing";
    JobStatus["Completed"] = "completed";
    JobStatus["Canceled"] = "canceled";
    JobStatus["Failed"] = "failed";
})(JobStatus = exports.JobStatus || (exports.JobStatus = {}));
function JobStatusFromJSON(json) {
    return JobStatusFromJSONTyped(json, false);
}
exports.JobStatusFromJSON = JobStatusFromJSON;
function JobStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.JobStatusFromJSONTyped = JobStatusFromJSONTyped;
function JobStatusToJSON(value) {
    return value;
}
exports.JobStatusToJSON = JobStatusToJSON;
