/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferUpdateData } from './';
/**
 *
 * @export
 * @interface TransferUpdateError
 */
export interface TransferUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TransferUpdateError
     */
    success: TransferUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransferUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TransferUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TransferUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {TransferUpdateData}
     * @memberof TransferUpdateError
     */
    transfer?: TransferUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum TransferUpdateErrorSuccessEnum {
    False = "false"
}
export declare function TransferUpdateErrorFromJSON(json: any): TransferUpdateError;
export declare function TransferUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferUpdateError;
export declare function TransferUpdateErrorToJSON(value?: TransferUpdateError | null): any;
