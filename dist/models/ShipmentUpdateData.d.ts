/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressUpdateData, ShipmentStatus, ShipmentUpdateDataLineItems } from './';
/**
 *
 * @export
 * @interface ShipmentUpdateData
 */
export interface ShipmentUpdateData {
    /**
     * Ship By Date (can be updated if shipment is not sentToFulfillment yet)
     * @type {Date}
     * @memberof ShipmentUpdateData
     */
    shipByDate?: Date;
    /**
     * Expected ship date (can be updated if shipment is not sentToFulfillment yet)
     * @type {Date}
     * @memberof ShipmentUpdateData
     */
    expectedShipDate?: Date;
    /**
     * First Priority to be respected (can be updated if shipment is not sentToFulfillment yet)
     * @type {Date}
     * @memberof ShipmentUpdateData
     */
    expectedDeliveryDate?: Date;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof ShipmentUpdateData
     */
    shippingAddress?: AddressUpdateData;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    shippingCarrier?: string;
    /**
     * 3rd Priority. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    shippingCode?: string;
    /**
     * Required with ShippingCarrier, 2nd Priority. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    shippingClass?: string;
    /**
     * Shipping Note. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    shippingNote?: string;
    /**
     * Shipping Labels. (can be updated if shipment is not sentToFulfillment yet)
     * @type {Array<string>}
     * @memberof ShipmentUpdateData
     */
    shippingLabels?: Array<string>;
    /**
     * Shipping Gift note
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    giftNote?: string;
    /**
     * International Commercial Terms. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    incoterms?: string;
    /**
     * Id of location defined in organization. (can be updated if shipment is not sentToFulfillment yet)
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    locationId?: string;
    /**
     *
     * @type {ShipmentStatus}
     * @memberof ShipmentUpdateData
     */
    status?: ShipmentStatus;
    /**
     * Customer Friendly Internal reference to ShipmentId that they use
     * @type {string}
     * @memberof ShipmentUpdateData
     */
    extShipmentId?: string;
    /**
     *
     * @type {Array<ShipmentUpdateDataLineItems>}
     * @memberof ShipmentUpdateData
     */
    lineItems?: Array<ShipmentUpdateDataLineItems>;
}
export declare function ShipmentUpdateDataFromJSON(json: any): ShipmentUpdateData;
export declare function ShipmentUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentUpdateData;
export declare function ShipmentUpdateDataToJSON(value?: ShipmentUpdateData | null): any;
