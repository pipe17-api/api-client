"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierCreateRequestToJSON = exports.SupplierCreateRequestFromJSONTyped = exports.SupplierCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SupplierCreateRequestFromJSON(json) {
    return SupplierCreateRequestFromJSONTyped(json, false);
}
exports.SupplierCreateRequestFromJSON = SupplierCreateRequestFromJSON;
function SupplierCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'address': _1.SupplierAddressFromJSON(json['address']),
        'contacts': !runtime_1.exists(json, 'contacts') ? undefined : (json['contacts'].map(_1.SupplierContactFromJSON)),
    };
}
exports.SupplierCreateRequestFromJSONTyped = SupplierCreateRequestFromJSONTyped;
function SupplierCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'address': _1.SupplierAddressToJSON(value.address),
        'contacts': value.contacts === undefined ? undefined : (value.contacts.map(_1.SupplierContactToJSON)),
    };
}
exports.SupplierCreateRequestToJSON = SupplierCreateRequestToJSON;
