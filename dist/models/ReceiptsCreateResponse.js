"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsCreateResponseToJSON = exports.ReceiptsCreateResponseFromJSONTyped = exports.ReceiptsCreateResponseFromJSON = exports.ReceiptsCreateResponseCodeEnum = exports.ReceiptsCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReceiptsCreateResponseSuccessEnum;
(function (ReceiptsCreateResponseSuccessEnum) {
    ReceiptsCreateResponseSuccessEnum["True"] = "true";
})(ReceiptsCreateResponseSuccessEnum = exports.ReceiptsCreateResponseSuccessEnum || (exports.ReceiptsCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReceiptsCreateResponseCodeEnum;
(function (ReceiptsCreateResponseCodeEnum) {
    ReceiptsCreateResponseCodeEnum[ReceiptsCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReceiptsCreateResponseCodeEnum[ReceiptsCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReceiptsCreateResponseCodeEnum[ReceiptsCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReceiptsCreateResponseCodeEnum = exports.ReceiptsCreateResponseCodeEnum || (exports.ReceiptsCreateResponseCodeEnum = {}));
function ReceiptsCreateResponseFromJSON(json) {
    return ReceiptsCreateResponseFromJSONTyped(json, false);
}
exports.ReceiptsCreateResponseFromJSON = ReceiptsCreateResponseFromJSON;
function ReceiptsCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'receipts': !runtime_1.exists(json, 'receipts') ? undefined : (json['receipts'].map(_1.ReceiptCreateResultFromJSON)),
    };
}
exports.ReceiptsCreateResponseFromJSONTyped = ReceiptsCreateResponseFromJSONTyped;
function ReceiptsCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'receipts': value.receipts === undefined ? undefined : (value.receipts.map(_1.ReceiptCreateResultToJSON)),
    };
}
exports.ReceiptsCreateResponseToJSON = ReceiptsCreateResponseToJSON;
