"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnUpdateErrorToJSON = exports.ReturnUpdateErrorFromJSONTyped = exports.ReturnUpdateErrorFromJSON = exports.ReturnUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReturnUpdateErrorSuccessEnum;
(function (ReturnUpdateErrorSuccessEnum) {
    ReturnUpdateErrorSuccessEnum["False"] = "false";
})(ReturnUpdateErrorSuccessEnum = exports.ReturnUpdateErrorSuccessEnum || (exports.ReturnUpdateErrorSuccessEnum = {}));
function ReturnUpdateErrorFromJSON(json) {
    return ReturnUpdateErrorFromJSONTyped(json, false);
}
exports.ReturnUpdateErrorFromJSON = ReturnUpdateErrorFromJSON;
function ReturnUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        '_return': !runtime_1.exists(json, 'return') ? undefined : _1.ReturnUpdateDataFromJSON(json['return']),
    };
}
exports.ReturnUpdateErrorFromJSONTyped = ReturnUpdateErrorFromJSONTyped;
function ReturnUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'return': _1.ReturnUpdateDataToJSON(value._return),
    };
}
exports.ReturnUpdateErrorToJSON = ReturnUpdateErrorToJSON;
