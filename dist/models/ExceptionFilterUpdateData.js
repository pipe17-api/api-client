"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterUpdateDataToJSON = exports.ExceptionFilterUpdateDataFromJSONTyped = exports.ExceptionFilterUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFilterUpdateDataFromJSON(json) {
    return ExceptionFilterUpdateDataFromJSONTyped(json, false);
}
exports.ExceptionFilterUpdateDataFromJSON = ExceptionFilterUpdateDataFromJSON;
function ExceptionFilterUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionType': !runtime_1.exists(json, 'exceptionType') ? undefined : _1.ExceptionTypeFromJSON(json['exceptionType']),
        'enabled': !runtime_1.exists(json, 'enabled') ? undefined : json['enabled'],
        'blocking': !runtime_1.exists(json, 'blocking') ? undefined : json['blocking'],
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'settings': !runtime_1.exists(json, 'settings') ? undefined : json['settings'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
    };
}
exports.ExceptionFilterUpdateDataFromJSONTyped = ExceptionFilterUpdateDataFromJSONTyped;
function ExceptionFilterUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionType': _1.ExceptionTypeToJSON(value.exceptionType),
        'enabled': value.enabled,
        'blocking': value.blocking,
        'isPublic': value.isPublic,
        'settings': value.settings,
        'originId': value.originId,
    };
}
exports.ExceptionFilterUpdateDataToJSON = ExceptionFilterUpdateDataToJSON;
