/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFilter } from './';
/**
 *
 * @export
 * @interface EntityFilterFetchResponse
 */
export interface EntityFilterFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntityFilterFetchResponse
     */
    success: EntityFilterFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterFetchResponse
     */
    code: EntityFilterFetchResponseCodeEnum;
    /**
     *
     * @type {EntityFilter}
     * @memberof EntityFilterFetchResponse
     */
    entityFilter?: EntityFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntityFilterFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntityFilterFetchResponseFromJSON(json: any): EntityFilterFetchResponse;
export declare function EntityFilterFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterFetchResponse;
export declare function EntityFilterFetchResponseToJSON(value?: EntityFilterFetchResponse | null): any;
