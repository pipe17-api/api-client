"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsDeleteErrorToJSON = exports.ShipmentsDeleteErrorFromJSONTyped = exports.ShipmentsDeleteErrorFromJSON = exports.ShipmentsDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentsDeleteErrorSuccessEnum;
(function (ShipmentsDeleteErrorSuccessEnum) {
    ShipmentsDeleteErrorSuccessEnum["False"] = "false";
})(ShipmentsDeleteErrorSuccessEnum = exports.ShipmentsDeleteErrorSuccessEnum || (exports.ShipmentsDeleteErrorSuccessEnum = {}));
function ShipmentsDeleteErrorFromJSON(json) {
    return ShipmentsDeleteErrorFromJSONTyped(json, false);
}
exports.ShipmentsDeleteErrorFromJSON = ShipmentsDeleteErrorFromJSON;
function ShipmentsDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ShipmentsDeleteFilterFromJSON(json['filters']),
    };
}
exports.ShipmentsDeleteErrorFromJSONTyped = ShipmentsDeleteErrorFromJSONTyped;
function ShipmentsDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.ShipmentsDeleteFilterToJSON(value.filters),
    };
}
exports.ShipmentsDeleteErrorToJSON = ShipmentsDeleteErrorToJSON;
