/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConvertRequestRules } from './';
/**
 *
 * @export
 * @interface ConvertRequest
 */
export interface ConvertRequest {
    /**
     *
     * @type {ConvertRequestRules}
     * @memberof ConvertRequest
     */
    rules: ConvertRequestRules;
    /**
     * Source object to convert
     * @type {object}
     * @memberof ConvertRequest
     */
    source: object;
}
export declare function ConvertRequestFromJSON(json: any): ConvertRequest;
export declare function ConvertRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConvertRequest;
export declare function ConvertRequestToJSON(value?: ConvertRequest | null): any;
