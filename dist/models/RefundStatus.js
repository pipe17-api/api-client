"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundStatusToJSON = exports.RefundStatusFromJSONTyped = exports.RefundStatusFromJSON = exports.RefundStatus = void 0;
/**
 * Refund status
 * @export
 * @enum {string}
 */
var RefundStatus;
(function (RefundStatus) {
    RefundStatus["Pending"] = "pending";
    RefundStatus["Settled"] = "settled";
})(RefundStatus = exports.RefundStatus || (exports.RefundStatus = {}));
function RefundStatusFromJSON(json) {
    return RefundStatusFromJSONTyped(json, false);
}
exports.RefundStatusFromJSON = RefundStatusFromJSON;
function RefundStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.RefundStatusFromJSONTyped = RefundStatusFromJSONTyped;
function RefundStatusToJSON(value) {
    return value;
}
exports.RefundStatusToJSON = RefundStatusToJSON;
