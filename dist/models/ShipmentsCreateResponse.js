"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsCreateResponseToJSON = exports.ShipmentsCreateResponseFromJSONTyped = exports.ShipmentsCreateResponseFromJSON = exports.ShipmentsCreateResponseCodeEnum = exports.ShipmentsCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentsCreateResponseSuccessEnum;
(function (ShipmentsCreateResponseSuccessEnum) {
    ShipmentsCreateResponseSuccessEnum["True"] = "true";
})(ShipmentsCreateResponseSuccessEnum = exports.ShipmentsCreateResponseSuccessEnum || (exports.ShipmentsCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ShipmentsCreateResponseCodeEnum;
(function (ShipmentsCreateResponseCodeEnum) {
    ShipmentsCreateResponseCodeEnum[ShipmentsCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ShipmentsCreateResponseCodeEnum[ShipmentsCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ShipmentsCreateResponseCodeEnum[ShipmentsCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ShipmentsCreateResponseCodeEnum = exports.ShipmentsCreateResponseCodeEnum || (exports.ShipmentsCreateResponseCodeEnum = {}));
function ShipmentsCreateResponseFromJSON(json) {
    return ShipmentsCreateResponseFromJSONTyped(json, false);
}
exports.ShipmentsCreateResponseFromJSON = ShipmentsCreateResponseFromJSON;
function ShipmentsCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'shipments': !runtime_1.exists(json, 'shipments') ? undefined : (json['shipments'].map(_1.ShipmentCreateResultFromJSON)),
    };
}
exports.ShipmentsCreateResponseFromJSONTyped = ShipmentsCreateResponseFromJSONTyped;
function ShipmentsCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'shipments': value.shipments === undefined ? undefined : (value.shipments.map(_1.ShipmentCreateResultToJSON)),
    };
}
exports.ShipmentsCreateResponseToJSON = ShipmentsCreateResponseToJSON;
