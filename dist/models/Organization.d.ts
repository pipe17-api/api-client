/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationAddress, OrganizationServiceTier, OrganizationStatus } from './';
/**
 *
 * @export
 * @interface Organization
 */
export interface Organization {
    /**
     *
     * @type {OrganizationStatus}
     * @memberof Organization
     */
    status?: OrganizationStatus;
    /**
     * Organization Name
     * @type {string}
     * @memberof Organization
     */
    name: string;
    /**
     *
     * @type {OrganizationServiceTier}
     * @memberof Organization
     */
    tier: OrganizationServiceTier;
    /**
     * Organization Contact email
     * @type {string}
     * @memberof Organization
     */
    email: string;
    /**
     * Organization Contact Phone
     * @type {string}
     * @memberof Organization
     */
    phone: string;
    /**
     * Organization Time Zone
     * @type {string}
     * @memberof Organization
     */
    timeZone: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof Organization
     */
    logoUrl?: string;
    /**
     *
     * @type {OrganizationAddress}
     * @memberof Organization
     */
    address: OrganizationAddress;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Organization
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Organization
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Organization
     */
    readonly orgKey?: string;
}
export declare function OrganizationFromJSON(json: any): Organization;
export declare function OrganizationFromJSONTyped(json: any, ignoreDiscriminator: boolean): Organization;
export declare function OrganizationToJSON(value?: Organization | null): any;
