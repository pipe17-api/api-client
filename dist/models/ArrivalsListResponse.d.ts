/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival, ArrivalsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ArrivalsListResponse
 */
export interface ArrivalsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ArrivalsListResponse
     */
    success: ArrivalsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalsListResponse
     */
    code: ArrivalsListResponseCodeEnum;
    /**
     *
     * @type {ArrivalsListFilter}
     * @memberof ArrivalsListResponse
     */
    filters?: ArrivalsListFilter;
    /**
     *
     * @type {Array<Arrival>}
     * @memberof ArrivalsListResponse
     */
    arrivals: Array<Arrival>;
    /**
     *
     * @type {Pagination}
     * @memberof ArrivalsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ArrivalsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ArrivalsListResponseFromJSON(json: any): ArrivalsListResponse;
export declare function ArrivalsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsListResponse;
export declare function ArrivalsListResponseToJSON(value?: ArrivalsListResponse | null): any;
