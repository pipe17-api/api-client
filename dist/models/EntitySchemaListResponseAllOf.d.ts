/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchema, EntitySchemasListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface EntitySchemaListResponseAllOf
 */
export interface EntitySchemaListResponseAllOf {
    /**
     *
     * @type {EntitySchemasListFilter}
     * @memberof EntitySchemaListResponseAllOf
     */
    filters?: EntitySchemasListFilter;
    /**
     *
     * @type {Array<EntitySchema>}
     * @memberof EntitySchemaListResponseAllOf
     */
    entitySchemas?: Array<EntitySchema>;
    /**
     *
     * @type {Pagination}
     * @memberof EntitySchemaListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function EntitySchemaListResponseAllOfFromJSON(json: any): EntitySchemaListResponseAllOf;
export declare function EntitySchemaListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaListResponseAllOf;
export declare function EntitySchemaListResponseAllOfToJSON(value?: EntitySchemaListResponseAllOf | null): any;
