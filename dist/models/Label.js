"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelToJSON = exports.LabelFromJSONTyped = exports.LabelFromJSON = exports.LabelStatusEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var LabelStatusEnum;
(function (LabelStatusEnum) {
    LabelStatusEnum["New"] = "new";
    LabelStatusEnum["Ready"] = "ready";
})(LabelStatusEnum = exports.LabelStatusEnum || (exports.LabelStatusEnum = {}));
function LabelFromJSON(json) {
    return LabelFromJSONTyped(json, false);
}
exports.LabelFromJSON = LabelFromJSON;
function LabelFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'labelId': !runtime_1.exists(json, 'labelId') ? undefined : json['labelId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'fileName': json['fileName'],
        'contentType': json['contentType'],
        'trackingNumber': !runtime_1.exists(json, 'trackingNumber') ? undefined : json['trackingNumber'],
        'shippingMethod': !runtime_1.exists(json, 'shippingMethod') ? undefined : json['shippingMethod'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.LabelFromJSONTyped = LabelFromJSONTyped;
function LabelToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'labelId': value.labelId,
        'status': value.status,
        'fileName': value.fileName,
        'contentType': value.contentType,
        'trackingNumber': value.trackingNumber,
        'shippingMethod': value.shippingMethod,
    };
}
exports.LabelToJSON = LabelToJSON;
