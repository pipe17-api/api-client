/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingsDeleteFilter
 */
export interface RoutingsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RoutingsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RoutingsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RoutingsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RoutingsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RoutingsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RoutingsDeleteFilter
     */
    count?: number;
    /**
     * Routings by list of routingId
     * @type {Array<string>}
     * @memberof RoutingsDeleteFilter
     */
    routingId?: Array<string>;
    /**
     * Routings by list of UUIDs
     * @type {Array<string>}
     * @memberof RoutingsDeleteFilter
     */
    uuid?: Array<string>;
    /**
     * Soft deleted routings
     * @type {boolean}
     * @memberof RoutingsDeleteFilter
     */
    deleted?: boolean;
}
export declare function RoutingsDeleteFilterFromJSON(json: any): RoutingsDeleteFilter;
export declare function RoutingsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsDeleteFilter;
export declare function RoutingsDeleteFilterToJSON(value?: RoutingsDeleteFilter | null): any;
