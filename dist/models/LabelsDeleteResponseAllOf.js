"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelsDeleteResponseAllOfToJSON = exports.LabelsDeleteResponseAllOfFromJSONTyped = exports.LabelsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LabelsDeleteResponseAllOfFromJSON(json) {
    return LabelsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.LabelsDeleteResponseAllOfFromJSON = LabelsDeleteResponseAllOfFromJSON;
function LabelsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LabelsDeleteFilterFromJSON(json['filters']),
        'labels': !runtime_1.exists(json, 'labels') ? undefined : (json['labels'].map(_1.LabelFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.LabelsDeleteResponseAllOfFromJSONTyped = LabelsDeleteResponseAllOfFromJSONTyped;
function LabelsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.LabelsDeleteFilterToJSON(value.filters),
        'labels': value.labels === undefined ? undefined : (value.labels.map(_1.LabelToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.LabelsDeleteResponseAllOfToJSON = LabelsDeleteResponseAllOfToJSON;
