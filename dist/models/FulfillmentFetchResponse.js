"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentFetchResponseToJSON = exports.FulfillmentFetchResponseFromJSONTyped = exports.FulfillmentFetchResponseFromJSON = exports.FulfillmentFetchResponseCodeEnum = exports.FulfillmentFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentFetchResponseSuccessEnum;
(function (FulfillmentFetchResponseSuccessEnum) {
    FulfillmentFetchResponseSuccessEnum["True"] = "true";
})(FulfillmentFetchResponseSuccessEnum = exports.FulfillmentFetchResponseSuccessEnum || (exports.FulfillmentFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var FulfillmentFetchResponseCodeEnum;
(function (FulfillmentFetchResponseCodeEnum) {
    FulfillmentFetchResponseCodeEnum[FulfillmentFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    FulfillmentFetchResponseCodeEnum[FulfillmentFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    FulfillmentFetchResponseCodeEnum[FulfillmentFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(FulfillmentFetchResponseCodeEnum = exports.FulfillmentFetchResponseCodeEnum || (exports.FulfillmentFetchResponseCodeEnum = {}));
function FulfillmentFetchResponseFromJSON(json) {
    return FulfillmentFetchResponseFromJSONTyped(json, false);
}
exports.FulfillmentFetchResponseFromJSON = FulfillmentFetchResponseFromJSON;
function FulfillmentFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'fulfillment': !runtime_1.exists(json, 'fulfillment') ? undefined : _1.FulfillmentFromJSON(json['fulfillment']),
    };
}
exports.FulfillmentFetchResponseFromJSONTyped = FulfillmentFetchResponseFromJSONTyped;
function FulfillmentFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'fulfillment': _1.FulfillmentToJSON(value.fulfillment),
    };
}
exports.FulfillmentFetchResponseToJSON = FulfillmentFetchResponseToJSON;
