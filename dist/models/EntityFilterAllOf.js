"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterAllOfToJSON = exports.EntityFilterAllOfFromJSONTyped = exports.EntityFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function EntityFilterAllOfFromJSON(json) {
    return EntityFilterAllOfFromJSONTyped(json, false);
}
exports.EntityFilterAllOfFromJSON = EntityFilterAllOfFromJSON;
function EntityFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filterId': !runtime_1.exists(json, 'filterId') ? undefined : json['filterId'],
    };
}
exports.EntityFilterAllOfFromJSONTyped = EntityFilterAllOfFromJSONTyped;
function EntityFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filterId': value.filterId,
    };
}
exports.EntityFilterAllOfToJSON = EntityFilterAllOfToJSON;
