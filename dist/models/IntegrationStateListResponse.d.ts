/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationState, IntegrationStateListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface IntegrationStateListResponse
 */
export interface IntegrationStateListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationStateListResponse
     */
    success: IntegrationStateListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationStateListResponse
     */
    code: IntegrationStateListResponseCodeEnum;
    /**
     *
     * @type {IntegrationStateListFilter}
     * @memberof IntegrationStateListResponse
     */
    filters?: IntegrationStateListFilter;
    /**
     *
     * @type {Array<IntegrationState>}
     * @memberof IntegrationStateListResponse
     */
    integrationStates?: Array<IntegrationState>;
    /**
     *
     * @type {Pagination}
     * @memberof IntegrationStateListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationStateListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationStateListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationStateListResponseFromJSON(json: any): IntegrationStateListResponse;
export declare function IntegrationStateListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateListResponse;
export declare function IntegrationStateListResponseToJSON(value?: IntegrationStateListResponse | null): any;
