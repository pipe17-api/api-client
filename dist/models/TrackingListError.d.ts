/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingFilter } from './';
/**
 *
 * @export
 * @interface TrackingListError
 */
export interface TrackingListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TrackingListError
     */
    success: TrackingListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TrackingListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TrackingListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {TrackingFilter}
     * @memberof TrackingListError
     */
    filters?: TrackingFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingListErrorSuccessEnum {
    False = "false"
}
export declare function TrackingListErrorFromJSON(json: any): TrackingListError;
export declare function TrackingListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingListError;
export declare function TrackingListErrorToJSON(value?: TrackingListError | null): any;
