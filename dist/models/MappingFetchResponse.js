"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingFetchResponseToJSON = exports.MappingFetchResponseFromJSONTyped = exports.MappingFetchResponseFromJSON = exports.MappingFetchResponseCodeEnum = exports.MappingFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var MappingFetchResponseSuccessEnum;
(function (MappingFetchResponseSuccessEnum) {
    MappingFetchResponseSuccessEnum["True"] = "true";
})(MappingFetchResponseSuccessEnum = exports.MappingFetchResponseSuccessEnum || (exports.MappingFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var MappingFetchResponseCodeEnum;
(function (MappingFetchResponseCodeEnum) {
    MappingFetchResponseCodeEnum[MappingFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    MappingFetchResponseCodeEnum[MappingFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    MappingFetchResponseCodeEnum[MappingFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(MappingFetchResponseCodeEnum = exports.MappingFetchResponseCodeEnum || (exports.MappingFetchResponseCodeEnum = {}));
function MappingFetchResponseFromJSON(json) {
    return MappingFetchResponseFromJSONTyped(json, false);
}
exports.MappingFetchResponseFromJSON = MappingFetchResponseFromJSON;
function MappingFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingFromJSON(json['mapping']),
    };
}
exports.MappingFetchResponseFromJSONTyped = MappingFetchResponseFromJSONTyped;
function MappingFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'mapping': _1.MappingToJSON(value.mapping),
    };
}
exports.MappingFetchResponseToJSON = MappingFetchResponseToJSON;
