"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuccessToJSON = exports.SuccessFromJSONTyped = exports.SuccessFromJSON = exports.SuccessCodeEnum = exports.SuccessSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var SuccessSuccessEnum;
(function (SuccessSuccessEnum) {
    SuccessSuccessEnum["True"] = "true";
})(SuccessSuccessEnum = exports.SuccessSuccessEnum || (exports.SuccessSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var SuccessCodeEnum;
(function (SuccessCodeEnum) {
    SuccessCodeEnum[SuccessCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    SuccessCodeEnum[SuccessCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    SuccessCodeEnum[SuccessCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(SuccessCodeEnum = exports.SuccessCodeEnum || (exports.SuccessCodeEnum = {}));
function SuccessFromJSON(json) {
    return SuccessFromJSONTyped(json, false);
}
exports.SuccessFromJSON = SuccessFromJSON;
function SuccessFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.SuccessFromJSONTyped = SuccessFromJSONTyped;
function SuccessToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.SuccessToJSON = SuccessToJSON;
