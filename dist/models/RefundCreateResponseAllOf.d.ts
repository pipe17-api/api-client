/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Refund } from './';
/**
 *
 * @export
 * @interface RefundCreateResponseAllOf
 */
export interface RefundCreateResponseAllOf {
    /**
     *
     * @type {Refund}
     * @memberof RefundCreateResponseAllOf
     */
    refund?: Refund;
}
export declare function RefundCreateResponseAllOfFromJSON(json: any): RefundCreateResponseAllOf;
export declare function RefundCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundCreateResponseAllOf;
export declare function RefundCreateResponseAllOfToJSON(value?: RefundCreateResponseAllOf | null): any;
