/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingCreateRequest
 */
export interface RoutingCreateRequest {
    /**
     * Public Routing
     * @type {boolean}
     * @memberof RoutingCreateRequest
     */
    isPublic: boolean;
    /**
     * Description
     * @type {string}
     * @memberof RoutingCreateRequest
     */
    description: string;
    /**
     * UUID
     * @type {string}
     * @memberof RoutingCreateRequest
     */
    uuid?: string;
    /**
     * Is Routing Filter Enabled
     * @type {boolean}
     * @memberof RoutingCreateRequest
     */
    enabled: boolean;
    /**
     * Routing Filter
     * @type {string}
     * @memberof RoutingCreateRequest
     */
    filter: string;
}
export declare function RoutingCreateRequestFromJSON(json: any): RoutingCreateRequest;
export declare function RoutingCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingCreateRequest;
export declare function RoutingCreateRequestToJSON(value?: RoutingCreateRequest | null): any;
