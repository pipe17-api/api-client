"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaginationToJSON = exports.PaginationFromJSONTyped = exports.PaginationFromJSON = void 0;
const runtime_1 = require("../runtime");
function PaginationFromJSON(json) {
    return PaginationFromJSONTyped(json, false);
}
exports.PaginationFromJSON = PaginationFromJSON;
function PaginationFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'total': !runtime_1.exists(json, 'total') ? undefined : json['total'],
        'pageSize': !runtime_1.exists(json, 'pageSize') ? undefined : json['pageSize'],
        'pageIndex': !runtime_1.exists(json, 'pageIndex') ? undefined : json['pageIndex'],
        'pages': !runtime_1.exists(json, 'pages') ? undefined : json['pages'],
        'first': !runtime_1.exists(json, 'first') ? undefined : json['first'],
        'last': !runtime_1.exists(json, 'last') ? undefined : json['last'],
        'outOfBounds': !runtime_1.exists(json, 'outOfBounds') ? undefined : json['outOfBounds'],
    };
}
exports.PaginationFromJSONTyped = PaginationFromJSONTyped;
function PaginationToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'total': value.total,
        'pageSize': value.pageSize,
        'pageIndex': value.pageIndex,
        'pages': value.pages,
        'first': value.first,
        'last': value.last,
        'outOfBounds': value.outOfBounds,
    };
}
exports.PaginationToJSON = PaginationToJSON;
