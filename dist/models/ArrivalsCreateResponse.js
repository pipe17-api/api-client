"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsCreateResponseToJSON = exports.ArrivalsCreateResponseFromJSONTyped = exports.ArrivalsCreateResponseFromJSON = exports.ArrivalsCreateResponseCodeEnum = exports.ArrivalsCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalsCreateResponseSuccessEnum;
(function (ArrivalsCreateResponseSuccessEnum) {
    ArrivalsCreateResponseSuccessEnum["True"] = "true";
})(ArrivalsCreateResponseSuccessEnum = exports.ArrivalsCreateResponseSuccessEnum || (exports.ArrivalsCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ArrivalsCreateResponseCodeEnum;
(function (ArrivalsCreateResponseCodeEnum) {
    ArrivalsCreateResponseCodeEnum[ArrivalsCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ArrivalsCreateResponseCodeEnum[ArrivalsCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ArrivalsCreateResponseCodeEnum[ArrivalsCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ArrivalsCreateResponseCodeEnum = exports.ArrivalsCreateResponseCodeEnum || (exports.ArrivalsCreateResponseCodeEnum = {}));
function ArrivalsCreateResponseFromJSON(json) {
    return ArrivalsCreateResponseFromJSONTyped(json, false);
}
exports.ArrivalsCreateResponseFromJSON = ArrivalsCreateResponseFromJSON;
function ArrivalsCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'arrivals': !runtime_1.exists(json, 'arrivals') ? undefined : (json['arrivals'].map(_1.ArrivalCreateResultFromJSON)),
    };
}
exports.ArrivalsCreateResponseFromJSONTyped = ArrivalsCreateResponseFromJSONTyped;
function ArrivalsCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'arrivals': value.arrivals === undefined ? undefined : (value.arrivals.map(_1.ArrivalCreateResultToJSON)),
    };
}
exports.ArrivalsCreateResponseToJSON = ArrivalsCreateResponseToJSON;
