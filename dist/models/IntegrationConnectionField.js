"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationConnectionFieldToJSON = exports.IntegrationConnectionFieldFromJSONTyped = exports.IntegrationConnectionFieldFromJSON = void 0;
const runtime_1 = require("../runtime");
function IntegrationConnectionFieldFromJSON(json) {
    return IntegrationConnectionFieldFromJSONTyped(json, false);
}
exports.IntegrationConnectionFieldFromJSON = IntegrationConnectionFieldFromJSON;
function IntegrationConnectionFieldFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'path': !runtime_1.exists(json, 'path') ? undefined : json['path'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
    };
}
exports.IntegrationConnectionFieldFromJSONTyped = IntegrationConnectionFieldFromJSONTyped;
function IntegrationConnectionFieldToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'path': value.path,
        'value': value.value,
    };
}
exports.IntegrationConnectionFieldToJSON = IntegrationConnectionFieldToJSON;
