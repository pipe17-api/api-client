/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RefundFetchError
 */
export interface RefundFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RefundFetchError
     */
    success: RefundFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RefundFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RefundFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundFetchErrorSuccessEnum {
    False = "false"
}
export declare function RefundFetchErrorFromJSON(json: any): RefundFetchError;
export declare function RefundFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundFetchError;
export declare function RefundFetchErrorToJSON(value?: RefundFetchError | null): any;
