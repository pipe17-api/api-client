/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Supplier } from './';
/**
 *
 * @export
 * @interface SupplierCreateResponseAllOf
 */
export interface SupplierCreateResponseAllOf {
    /**
     *
     * @type {Supplier}
     * @memberof SupplierCreateResponseAllOf
     */
    supplier?: Supplier;
}
export declare function SupplierCreateResponseAllOfFromJSON(json: any): SupplierCreateResponseAllOf;
export declare function SupplierCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierCreateResponseAllOf;
export declare function SupplierCreateResponseAllOfToJSON(value?: SupplierCreateResponseAllOf | null): any;
