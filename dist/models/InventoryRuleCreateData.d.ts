/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventSource, EventType } from './';
/**
 *
 * @export
 * @interface InventoryRuleCreateData
 */
export interface InventoryRuleCreateData {
    /**
     *
     * @type {EventType}
     * @memberof InventoryRuleCreateData
     */
    event: EventType;
    /**
     *
     * @type {EventSource}
     * @memberof InventoryRuleCreateData
     */
    ptype?: EventSource;
    /**
     * Event source partner name
     * @type {string}
     * @memberof InventoryRuleCreateData
     */
    partner?: string;
    /**
     * Event source integration id
     * @type {string}
     * @memberof InventoryRuleCreateData
     */
    integration?: string;
    /**
     * Description of the rule
     * @type {string}
     * @memberof InventoryRuleCreateData
     */
    description?: string;
    /**
     * Rule's definition as javascript function
     * @type {string}
     * @memberof InventoryRuleCreateData
     */
    rule: string;
}
export declare function InventoryRuleCreateDataFromJSON(json: any): InventoryRuleCreateData;
export declare function InventoryRuleCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleCreateData;
export declare function InventoryRuleCreateDataToJSON(value?: InventoryRuleCreateData | null): any;
