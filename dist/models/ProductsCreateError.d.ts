/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductCreateResult } from './';
/**
 *
 * @export
 * @interface ProductsCreateError
 */
export interface ProductsCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ProductsCreateError
     */
    success: ProductsCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductsCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ProductsCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ProductsCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<ProductCreateResult>}
     * @memberof ProductsCreateError
     */
    products?: Array<ProductCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductsCreateErrorSuccessEnum {
    False = "false"
}
export declare function ProductsCreateErrorFromJSON(json: any): ProductsCreateError;
export declare function ProductsCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsCreateError;
export declare function ProductsCreateErrorToJSON(value?: ProductsCreateError | null): any;
