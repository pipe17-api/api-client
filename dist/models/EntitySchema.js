"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaToJSON = exports.EntitySchemaFromJSONTyped = exports.EntitySchemaFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntitySchemaFromJSON(json) {
    return EntitySchemaFromJSONTyped(json, false);
}
exports.EntitySchemaFromJSON = EntitySchemaFromJSON;
function EntitySchemaFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'schemaId': !runtime_1.exists(json, 'schemaId') ? undefined : json['schemaId'],
        'name': json['name'],
        'fields': json['fields'],
        'entity': _1.EntityNameFromJSON(json['entity']),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.EntitySchemaFromJSONTyped = EntitySchemaFromJSONTyped;
function EntitySchemaToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'schemaId': value.schemaId,
        'name': value.name,
        'fields': value.fields,
        'entity': _1.EntityNameToJSON(value.entity),
    };
}
exports.EntitySchemaToJSON = EntitySchemaToJSON;
