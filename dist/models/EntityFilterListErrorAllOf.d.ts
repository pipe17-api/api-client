/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFiltersListFilter } from './';
/**
 *
 * @export
 * @interface EntityFilterListErrorAllOf
 */
export interface EntityFilterListErrorAllOf {
    /**
     *
     * @type {EntityFiltersListFilter}
     * @memberof EntityFilterListErrorAllOf
     */
    filters?: EntityFiltersListFilter;
}
export declare function EntityFilterListErrorAllOfFromJSON(json: any): EntityFilterListErrorAllOf;
export declare function EntityFilterListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterListErrorAllOf;
export declare function EntityFilterListErrorAllOfToJSON(value?: EntityFilterListErrorAllOf | null): any;
