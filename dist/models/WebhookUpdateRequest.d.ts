/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface WebhookUpdateRequest
 */
export interface WebhookUpdateRequest {
    /**
     * Webhook URL
     * @type {string}
     * @memberof WebhookUpdateRequest
     */
    url?: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof WebhookUpdateRequest
     */
    apikey?: string;
    /**
     * Webhook Topics
     * @type {Array<WebhookTopics>}
     * @memberof WebhookUpdateRequest
     */
    topics?: Array<WebhookTopics>;
}
export declare function WebhookUpdateRequestFromJSON(json: any): WebhookUpdateRequest;
export declare function WebhookUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookUpdateRequest;
export declare function WebhookUpdateRequestToJSON(value?: WebhookUpdateRequest | null): any;
