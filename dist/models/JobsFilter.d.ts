/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobStatus, JobSubType, JobType } from './';
/**
 *
 * @export
 * @interface JobsFilter
 */
export interface JobsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof JobsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof JobsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof JobsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof JobsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof JobsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof JobsFilter
     */
    count?: number;
    /**
     * Jobs by list of jobId
     * @type {Array<string>}
     * @memberof JobsFilter
     */
    jobId?: Array<string>;
    /**
     * Jobs with given type
     * @type {Array<JobType>}
     * @memberof JobsFilter
     */
    type?: Array<JobType>;
    /**
     * Jobs with given sub-type
     * @type {Array<JobSubType>}
     * @memberof JobsFilter
     */
    subType?: Array<JobSubType>;
    /**
     * Jobs with given status
     * @type {Array<JobStatus>}
     * @memberof JobsFilter
     */
    status?: Array<JobStatus>;
}
export declare function JobsFilterFromJSON(json: any): JobsFilter;
export declare function JobsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsFilter;
export declare function JobsFilterToJSON(value?: JobsFilter | null): any;
