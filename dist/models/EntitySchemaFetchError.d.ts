/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntitySchemaFetchError
 */
export interface EntitySchemaFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntitySchemaFetchError
     */
    success: EntitySchemaFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntitySchemaFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntitySchemaFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaFetchErrorSuccessEnum {
    False = "false"
}
export declare function EntitySchemaFetchErrorFromJSON(json: any): EntitySchemaFetchError;
export declare function EntitySchemaFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaFetchError;
export declare function EntitySchemaFetchErrorToJSON(value?: EntitySchemaFetchError | null): any;
