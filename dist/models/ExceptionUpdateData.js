"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionUpdateDataToJSON = exports.ExceptionUpdateDataFromJSONTyped = exports.ExceptionUpdateDataFromJSON = void 0;
const _1 = require("./");
function ExceptionUpdateDataFromJSON(json) {
    return ExceptionUpdateDataFromJSONTyped(json, false);
}
exports.ExceptionUpdateDataFromJSON = ExceptionUpdateDataFromJSON;
function ExceptionUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': _1.ExceptionStatusFromJSON(json['status']),
    };
}
exports.ExceptionUpdateDataFromJSONTyped = ExceptionUpdateDataFromJSONTyped;
function ExceptionUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.ExceptionStatusToJSON(value.status),
    };
}
exports.ExceptionUpdateDataToJSON = ExceptionUpdateDataToJSON;
