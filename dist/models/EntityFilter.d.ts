/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntityFilter
 */
export interface EntityFilter {
    /**
     * Filter ID
     * @type {string}
     * @memberof EntityFilter
     */
    filterId?: string;
    /**
     * Filter name
     * @type {string}
     * @memberof EntityFilter
     */
    name: string;
    /**
     * Filter query object
     * @type {object}
     * @memberof EntityFilter
     */
    query: object;
    /**
     *
     * @type {EntityName}
     * @memberof EntityFilter
     */
    entity: EntityName;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof EntityFilter
     */
    isPublic?: boolean;
    /**
     *
     * @type {string}
     * @memberof EntityFilter
     */
    originId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof EntityFilter
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof EntityFilter
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof EntityFilter
     */
    readonly orgKey?: string;
}
export declare function EntityFilterFromJSON(json: any): EntityFilter;
export declare function EntityFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilter;
export declare function EntityFilterToJSON(value?: EntityFilter | null): any;
