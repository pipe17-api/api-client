/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductStatus, ProductType } from './';
/**
 *
 * @export
 * @interface ProductsDeleteFilter
 */
export interface ProductsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ProductsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ProductsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ProductsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ProductsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ProductsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ProductsDeleteFilter
     */
    count?: number;
    /**
     * Products by list of productId
     * @type {Array<string>}
     * @memberof ProductsDeleteFilter
     */
    productId?: Array<string>;
    /**
     * Products by list of extProductId
     * @type {Array<string>}
     * @memberof ProductsDeleteFilter
     */
    extProductId?: Array<string>;
    /**
     * All variants of a parent products by list of external ids
     * @type {Array<string>}
     * @memberof ProductsDeleteFilter
     */
    parentExtProductId?: Array<string>;
    /**
     * Products with this status
     * @type {Array<ProductStatus>}
     * @memberof ProductsDeleteFilter
     */
    status?: Array<ProductStatus>;
    /**
     * Products matching specified types
     * @type {Array<ProductType>}
     * @memberof ProductsDeleteFilter
     */
    types?: Array<ProductType>;
    /**
     * Soft deleted products
     * @type {boolean}
     * @memberof ProductsDeleteFilter
     */
    deleted?: boolean;
    /**
     * Products of these integrations
     * @type {Array<string>}
     * @memberof ProductsDeleteFilter
     */
    integration?: Array<string>;
}
export declare function ProductsDeleteFilterFromJSON(json: any): ProductsDeleteFilter;
export declare function ProductsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsDeleteFilter;
export declare function ProductsDeleteFilterToJSON(value?: ProductsDeleteFilter | null): any;
