"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleStatusToJSON = exports.RoleStatusFromJSONTyped = exports.RoleStatusFromJSON = exports.RoleStatus = void 0;
/**
 * Role status
 * @export
 * @enum {string}
 */
var RoleStatus;
(function (RoleStatus) {
    RoleStatus["Active"] = "active";
    RoleStatus["Disabled"] = "disabled";
})(RoleStatus = exports.RoleStatus || (exports.RoleStatus = {}));
function RoleStatusFromJSON(json) {
    return RoleStatusFromJSONTyped(json, false);
}
exports.RoleStatusFromJSON = RoleStatusFromJSON;
function RoleStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.RoleStatusFromJSONTyped = RoleStatusFromJSONTyped;
function RoleStatusToJSON(value) {
    return value;
}
exports.RoleStatusToJSON = RoleStatusToJSON;
