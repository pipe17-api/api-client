"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaCreateDataToJSON = exports.EntitySchemaCreateDataFromJSONTyped = exports.EntitySchemaCreateDataFromJSON = void 0;
const _1 = require("./");
function EntitySchemaCreateDataFromJSON(json) {
    return EntitySchemaCreateDataFromJSONTyped(json, false);
}
exports.EntitySchemaCreateDataFromJSON = EntitySchemaCreateDataFromJSON;
function EntitySchemaCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'fields': json['fields'],
        'entity': _1.EntityNameFromJSON(json['entity']),
    };
}
exports.EntitySchemaCreateDataFromJSONTyped = EntitySchemaCreateDataFromJSONTyped;
function EntitySchemaCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'fields': value.fields,
        'entity': _1.EntityNameToJSON(value.entity),
    };
}
exports.EntitySchemaCreateDataToJSON = EntitySchemaCreateDataToJSON;
