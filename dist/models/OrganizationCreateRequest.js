"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationCreateRequestToJSON = exports.OrganizationCreateRequestFromJSONTyped = exports.OrganizationCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationCreateRequestFromJSON(json) {
    return OrganizationCreateRequestFromJSONTyped(json, false);
}
exports.OrganizationCreateRequestFromJSON = OrganizationCreateRequestFromJSON;
function OrganizationCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'tier': _1.OrganizationServiceTierFromJSON(json['tier']),
        'email': json['email'],
        'phone': json['phone'],
        'timeZone': json['timeZone'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'address': _1.OrganizationAddressFromJSON(json['address']),
    };
}
exports.OrganizationCreateRequestFromJSONTyped = OrganizationCreateRequestFromJSONTyped;
function OrganizationCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'tier': _1.OrganizationServiceTierToJSON(value.tier),
        'email': value.email,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'logoUrl': value.logoUrl,
        'address': _1.OrganizationAddressToJSON(value.address),
    };
}
exports.OrganizationCreateRequestToJSON = OrganizationCreateRequestToJSON;
