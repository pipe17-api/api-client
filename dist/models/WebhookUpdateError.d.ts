/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookUpdateData } from './';
/**
 *
 * @export
 * @interface WebhookUpdateError
 */
export interface WebhookUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof WebhookUpdateError
     */
    success: WebhookUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhookUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof WebhookUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof WebhookUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {WebhookUpdateData}
     * @memberof WebhookUpdateError
     */
    webhook?: WebhookUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhookUpdateErrorSuccessEnum {
    False = "false"
}
export declare function WebhookUpdateErrorFromJSON(json: any): WebhookUpdateError;
export declare function WebhookUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookUpdateError;
export declare function WebhookUpdateErrorToJSON(value?: WebhookUpdateError | null): any;
