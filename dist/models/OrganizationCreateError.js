"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationCreateErrorToJSON = exports.OrganizationCreateErrorFromJSONTyped = exports.OrganizationCreateErrorFromJSON = exports.OrganizationCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrganizationCreateErrorSuccessEnum;
(function (OrganizationCreateErrorSuccessEnum) {
    OrganizationCreateErrorSuccessEnum["False"] = "false";
})(OrganizationCreateErrorSuccessEnum = exports.OrganizationCreateErrorSuccessEnum || (exports.OrganizationCreateErrorSuccessEnum = {}));
function OrganizationCreateErrorFromJSON(json) {
    return OrganizationCreateErrorFromJSONTyped(json, false);
}
exports.OrganizationCreateErrorFromJSON = OrganizationCreateErrorFromJSON;
function OrganizationCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'organization': !runtime_1.exists(json, 'organization') ? undefined : _1.OrganizationCreateDataFromJSON(json['organization']),
    };
}
exports.OrganizationCreateErrorFromJSONTyped = OrganizationCreateErrorFromJSONTyped;
function OrganizationCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'organization': _1.OrganizationCreateDataToJSON(value.organization),
    };
}
exports.OrganizationCreateErrorToJSON = OrganizationCreateErrorToJSON;
