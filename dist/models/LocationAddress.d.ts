/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LocationAddress
 */
export interface LocationAddress {
    /**
     * Company
     * @type {string}
     * @memberof LocationAddress
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof LocationAddress
     */
    address1: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof LocationAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof LocationAddress
     */
    city?: string;
    /**
     * State or Province
     * @type {string}
     * @memberof LocationAddress
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof LocationAddress
     */
    zipCodeOrPostalCode: string;
    /**
     * Country
     * @type {string}
     * @memberof LocationAddress
     */
    country: string;
    /**
     * email
     * @type {string}
     * @memberof LocationAddress
     */
    email?: string;
    /**
     * Phone
     * @type {string}
     * @memberof LocationAddress
     */
    phone?: string;
}
export declare function LocationAddressFromJSON(json: any): LocationAddress;
export declare function LocationAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationAddress;
export declare function LocationAddressToJSON(value?: LocationAddress | null): any;
