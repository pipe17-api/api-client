/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface WebhookCreateData
 */
export interface WebhookCreateData {
    /**
     * Webhook URL
     * @type {string}
     * @memberof WebhookCreateData
     */
    url: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof WebhookCreateData
     */
    apikey: string;
    /**
     * Webhook topics
     * @type {Array<WebhookTopics>}
     * @memberof WebhookCreateData
     */
    topics: Array<WebhookTopics>;
}
export declare function WebhookCreateDataFromJSON(json: any): WebhookCreateData;
export declare function WebhookCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookCreateData;
export declare function WebhookCreateDataToJSON(value?: WebhookCreateData | null): any;
