"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsFilterAllOfToJSON = exports.ReturnsFilterAllOfFromJSONTyped = exports.ReturnsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnsFilterAllOfFromJSON(json) {
    return ReturnsFilterAllOfFromJSONTyped(json, false);
}
exports.ReturnsFilterAllOfFromJSON = ReturnsFilterAllOfFromJSON;
function ReturnsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'returnId': !runtime_1.exists(json, 'returnId') ? undefined : json['returnId'],
        'extReturnId': !runtime_1.exists(json, 'extReturnId') ? undefined : json['extReturnId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ReturnStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.ReturnsFilterAllOfFromJSONTyped = ReturnsFilterAllOfFromJSONTyped;
function ReturnsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'returnId': value.returnId,
        'extReturnId': value.extReturnId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ReturnStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.ReturnsFilterAllOfToJSON = ReturnsFilterAllOfToJSON;
