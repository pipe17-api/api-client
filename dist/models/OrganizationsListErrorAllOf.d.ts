/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationsListFilter } from './';
/**
 *
 * @export
 * @interface OrganizationsListErrorAllOf
 */
export interface OrganizationsListErrorAllOf {
    /**
     *
     * @type {OrganizationsListFilter}
     * @memberof OrganizationsListErrorAllOf
     */
    filters?: OrganizationsListFilter;
}
export declare function OrganizationsListErrorAllOfFromJSON(json: any): OrganizationsListErrorAllOf;
export declare function OrganizationsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsListErrorAllOf;
export declare function OrganizationsListErrorAllOfToJSON(value?: OrganizationsListErrorAllOf | null): any;
