"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryUpdateRequestToJSON = exports.InventoryUpdateRequestFromJSONTyped = exports.InventoryUpdateRequestFromJSON = exports.InventoryUpdateRequestOrderTypeEnum = exports.InventoryUpdateRequestEntityTypeEnum = exports.InventoryUpdateRequestEventEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryUpdateRequestEventEnum;
(function (InventoryUpdateRequestEventEnum) {
    InventoryUpdateRequestEventEnum["Ship"] = "ship";
    InventoryUpdateRequestEventEnum["Fulfill"] = "fulfill";
    InventoryUpdateRequestEventEnum["Xferout"] = "xferout";
    InventoryUpdateRequestEventEnum["Xferfulfill"] = "xferfulfill";
    InventoryUpdateRequestEventEnum["Xferin"] = "xferin";
    InventoryUpdateRequestEventEnum["Receive"] = "receive";
    InventoryUpdateRequestEventEnum["FutureShip"] = "futureShip";
    InventoryUpdateRequestEventEnum["Release"] = "release";
    InventoryUpdateRequestEventEnum["ShipCancel"] = "shipCancel";
    InventoryUpdateRequestEventEnum["ShipCancelRestock"] = "shipCancelRestock";
    InventoryUpdateRequestEventEnum["FutureShipCancel"] = "futureShipCancel";
    InventoryUpdateRequestEventEnum["VirtualCommit"] = "virtualCommit";
})(InventoryUpdateRequestEventEnum = exports.InventoryUpdateRequestEventEnum || (exports.InventoryUpdateRequestEventEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryUpdateRequestEntityTypeEnum;
(function (InventoryUpdateRequestEntityTypeEnum) {
    InventoryUpdateRequestEntityTypeEnum["Arrivals"] = "arrivals";
    InventoryUpdateRequestEntityTypeEnum["Fulfillments"] = "fulfillments";
    InventoryUpdateRequestEntityTypeEnum["Inventory"] = "inventory";
    InventoryUpdateRequestEntityTypeEnum["Receipts"] = "receipts";
    InventoryUpdateRequestEntityTypeEnum["Shipments"] = "shipments";
    InventoryUpdateRequestEntityTypeEnum["Orders"] = "orders";
})(InventoryUpdateRequestEntityTypeEnum = exports.InventoryUpdateRequestEntityTypeEnum || (exports.InventoryUpdateRequestEntityTypeEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryUpdateRequestOrderTypeEnum;
(function (InventoryUpdateRequestOrderTypeEnum) {
    InventoryUpdateRequestOrderTypeEnum["Sales"] = "sales";
    InventoryUpdateRequestOrderTypeEnum["Transfer"] = "transfer";
    InventoryUpdateRequestOrderTypeEnum["Purchase"] = "purchase";
})(InventoryUpdateRequestOrderTypeEnum = exports.InventoryUpdateRequestOrderTypeEnum || (exports.InventoryUpdateRequestOrderTypeEnum = {}));
function InventoryUpdateRequestFromJSON(json) {
    return InventoryUpdateRequestFromJSONTyped(json, false);
}
exports.InventoryUpdateRequestFromJSON = InventoryUpdateRequestFromJSON;
function InventoryUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'event': json['event'],
        'ptype': _1.EventSourceFromJSON(json['ptype']),
        'sku': json['sku'],
        'locationId': json['locationId'],
        'infinite': !runtime_1.exists(json, 'infinite') ? undefined : json['infinite'],
        'quantity': json['quantity'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'entityType': !runtime_1.exists(json, 'entityType') ? undefined : json['entityType'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'orderType': !runtime_1.exists(json, 'orderType') ? undefined : json['orderType'],
    };
}
exports.InventoryUpdateRequestFromJSONTyped = InventoryUpdateRequestFromJSONTyped;
function InventoryUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'event': value.event,
        'ptype': _1.EventSourceToJSON(value.ptype),
        'sku': value.sku,
        'locationId': value.locationId,
        'infinite': value.infinite,
        'quantity': value.quantity,
        'entityId': value.entityId,
        'entityType': value.entityType,
        'orderId': value.orderId,
        'orderType': value.orderType,
    };
}
exports.InventoryUpdateRequestToJSON = InventoryUpdateRequestToJSON;
