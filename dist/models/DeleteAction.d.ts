/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Action for delete operation
 * @export
 * @enum {string}
 */
export declare enum DeleteAction {
    Soft = "soft",
    Hard = "hard"
}
export declare function DeleteActionFromJSON(json: any): DeleteAction;
export declare function DeleteActionFromJSONTyped(json: any, ignoreDiscriminator: boolean): DeleteAction;
export declare function DeleteActionToJSON(value?: DeleteAction | null): any;
