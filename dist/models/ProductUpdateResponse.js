"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductUpdateResponseToJSON = exports.ProductUpdateResponseFromJSONTyped = exports.ProductUpdateResponseFromJSON = exports.ProductUpdateResponseCodeEnum = exports.ProductUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ProductUpdateResponseSuccessEnum;
(function (ProductUpdateResponseSuccessEnum) {
    ProductUpdateResponseSuccessEnum["True"] = "true";
})(ProductUpdateResponseSuccessEnum = exports.ProductUpdateResponseSuccessEnum || (exports.ProductUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ProductUpdateResponseCodeEnum;
(function (ProductUpdateResponseCodeEnum) {
    ProductUpdateResponseCodeEnum[ProductUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ProductUpdateResponseCodeEnum[ProductUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ProductUpdateResponseCodeEnum[ProductUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ProductUpdateResponseCodeEnum = exports.ProductUpdateResponseCodeEnum || (exports.ProductUpdateResponseCodeEnum = {}));
function ProductUpdateResponseFromJSON(json) {
    return ProductUpdateResponseFromJSONTyped(json, false);
}
exports.ProductUpdateResponseFromJSON = ProductUpdateResponseFromJSON;
function ProductUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ProductUpdateResponseFromJSONTyped = ProductUpdateResponseFromJSONTyped;
function ProductUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ProductUpdateResponseToJSON = ProductUpdateResponseToJSON;
