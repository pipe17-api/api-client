/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TransferAllOf
 */
export interface TransferAllOf {
    /**
     * Transfer ID
     * @type {string}
     * @memberof TransferAllOf
     */
    transferId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof TransferAllOf
     */
    integration?: string;
}
export declare function TransferAllOfFromJSON(json: any): TransferAllOf;
export declare function TransferAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferAllOf;
export declare function TransferAllOfToJSON(value?: TransferAllOf | null): any;
