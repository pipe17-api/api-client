/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule, InventoryRulesDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryRulesDeleteResponseAllOf
 */
export interface InventoryRulesDeleteResponseAllOf {
    /**
     *
     * @type {InventoryRulesDeleteFilter}
     * @memberof InventoryRulesDeleteResponseAllOf
     */
    filters?: InventoryRulesDeleteFilter;
    /**
     *
     * @type {Array<InventoryRule>}
     * @memberof InventoryRulesDeleteResponseAllOf
     */
    rules?: Array<InventoryRule>;
    /**
     * Number of deleted rules
     * @type {number}
     * @memberof InventoryRulesDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryRulesDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function InventoryRulesDeleteResponseAllOfFromJSON(json: any): InventoryRulesDeleteResponseAllOf;
export declare function InventoryRulesDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesDeleteResponseAllOf;
export declare function InventoryRulesDeleteResponseAllOfToJSON(value?: InventoryRulesDeleteResponseAllOf | null): any;
