"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersDeleteFilterAllOfToJSON = exports.OrdersDeleteFilterAllOfFromJSONTyped = exports.OrdersDeleteFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrdersDeleteFilterAllOfFromJSON(json) {
    return OrdersDeleteFilterAllOfFromJSONTyped(json, false);
}
exports.OrdersDeleteFilterAllOfFromJSON = OrdersDeleteFilterAllOfFromJSON;
function OrdersDeleteFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.OrdersDeleteFilterAllOfFromJSONTyped = OrdersDeleteFilterAllOfFromJSONTyped;
function OrdersDeleteFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
    };
}
exports.OrdersDeleteFilterAllOfToJSON = OrdersDeleteFilterAllOfToJSON;
