/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingUpdateData } from './';
/**
 *
 * @export
 * @interface TrackingUpdateError
 */
export interface TrackingUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TrackingUpdateError
     */
    success: TrackingUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TrackingUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TrackingUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {TrackingUpdateData}
     * @memberof TrackingUpdateError
     */
    tracking?: TrackingUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingUpdateErrorSuccessEnum {
    False = "false"
}
export declare function TrackingUpdateErrorFromJSON(json: any): TrackingUpdateError;
export declare function TrackingUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingUpdateError;
export declare function TrackingUpdateErrorToJSON(value?: TrackingUpdateError | null): any;
