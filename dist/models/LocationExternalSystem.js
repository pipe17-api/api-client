"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationExternalSystemToJSON = exports.LocationExternalSystemFromJSONTyped = exports.LocationExternalSystemFromJSON = void 0;
const runtime_1 = require("../runtime");
function LocationExternalSystemFromJSON(json) {
    return LocationExternalSystemFromJSONTyped(json, false);
}
exports.LocationExternalSystemFromJSON = LocationExternalSystemFromJSON;
function LocationExternalSystemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integrationId': !runtime_1.exists(json, 'integrationId') ? undefined : json['integrationId'],
        'locationValue': !runtime_1.exists(json, 'locationValue') ? undefined : json['locationValue'],
    };
}
exports.LocationExternalSystemFromJSONTyped = LocationExternalSystemFromJSONTyped;
function LocationExternalSystemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integrationId': value.integrationId,
        'locationValue': value.locationValue,
    };
}
exports.LocationExternalSystemToJSON = LocationExternalSystemToJSON;
