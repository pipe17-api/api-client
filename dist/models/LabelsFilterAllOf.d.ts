/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Labels Filter
 * @export
 * @interface LabelsFilterAllOf
 */
export interface LabelsFilterAllOf {
    /**
     * Labels by list of labelId
     * @type {Array<string>}
     * @memberof LabelsFilterAllOf
     */
    labelId?: Array<string>;
    /**
     * Soft deleted labels
     * @type {boolean}
     * @memberof LabelsFilterAllOf
     */
    deleted?: boolean;
}
export declare function LabelsFilterAllOfFromJSON(json: any): LabelsFilterAllOf;
export declare function LabelsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsFilterAllOf;
export declare function LabelsFilterAllOfToJSON(value?: LabelsFilterAllOf | null): any;
