"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesDeleteErrorToJSON = exports.PurchasesDeleteErrorFromJSONTyped = exports.PurchasesDeleteErrorFromJSON = exports.PurchasesDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchasesDeleteErrorSuccessEnum;
(function (PurchasesDeleteErrorSuccessEnum) {
    PurchasesDeleteErrorSuccessEnum["False"] = "false";
})(PurchasesDeleteErrorSuccessEnum = exports.PurchasesDeleteErrorSuccessEnum || (exports.PurchasesDeleteErrorSuccessEnum = {}));
function PurchasesDeleteErrorFromJSON(json) {
    return PurchasesDeleteErrorFromJSONTyped(json, false);
}
exports.PurchasesDeleteErrorFromJSON = PurchasesDeleteErrorFromJSON;
function PurchasesDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesDeleteFilterFromJSON(json['filters']),
    };
}
exports.PurchasesDeleteErrorFromJSONTyped = PurchasesDeleteErrorFromJSONTyped;
function PurchasesDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.PurchasesDeleteFilterToJSON(value.filters),
    };
}
exports.PurchasesDeleteErrorToJSON = PurchasesDeleteErrorToJSON;
