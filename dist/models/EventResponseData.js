"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventResponseDataToJSON = exports.EventResponseDataFromJSONTyped = exports.EventResponseDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function EventResponseDataFromJSON(json) {
    return EventResponseDataFromJSONTyped(json, false);
}
exports.EventResponseDataFromJSON = EventResponseDataFromJSON;
function EventResponseDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'retries': !runtime_1.exists(json, 'retries') ? undefined : json['retries'],
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'headers': !runtime_1.exists(json, 'headers') ? undefined : json['headers'],
        'body': !runtime_1.exists(json, 'body') ? undefined : json['body'],
    };
}
exports.EventResponseDataFromJSONTyped = EventResponseDataFromJSONTyped;
function EventResponseDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'retries': value.retries,
        'status': value.status,
        'message': value.message,
        'headers': value.headers,
        'body': value.body,
    };
}
exports.EventResponseDataToJSON = EventResponseDataToJSON;
