/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationState } from './';
/**
 *
 * @export
 * @interface IntegrationStateFetchResponse
 */
export interface IntegrationStateFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationStateFetchResponse
     */
    success: IntegrationStateFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationStateFetchResponse
     */
    code: IntegrationStateFetchResponseCodeEnum;
    /**
     *
     * @type {IntegrationState}
     * @memberof IntegrationStateFetchResponse
     */
    integrationState?: IntegrationState;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationStateFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationStateFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationStateFetchResponseFromJSON(json: any): IntegrationStateFetchResponse;
export declare function IntegrationStateFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateFetchResponse;
export declare function IntegrationStateFetchResponseToJSON(value?: IntegrationStateFetchResponse | null): any;
