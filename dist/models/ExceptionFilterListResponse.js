"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterListResponseToJSON = exports.ExceptionFilterListResponseFromJSONTyped = exports.ExceptionFilterListResponseFromJSON = exports.ExceptionFilterListResponseCodeEnum = exports.ExceptionFilterListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionFilterListResponseSuccessEnum;
(function (ExceptionFilterListResponseSuccessEnum) {
    ExceptionFilterListResponseSuccessEnum["True"] = "true";
})(ExceptionFilterListResponseSuccessEnum = exports.ExceptionFilterListResponseSuccessEnum || (exports.ExceptionFilterListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionFilterListResponseCodeEnum;
(function (ExceptionFilterListResponseCodeEnum) {
    ExceptionFilterListResponseCodeEnum[ExceptionFilterListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionFilterListResponseCodeEnum[ExceptionFilterListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionFilterListResponseCodeEnum[ExceptionFilterListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionFilterListResponseCodeEnum = exports.ExceptionFilterListResponseCodeEnum || (exports.ExceptionFilterListResponseCodeEnum = {}));
function ExceptionFilterListResponseFromJSON(json) {
    return ExceptionFilterListResponseFromJSONTyped(json, false);
}
exports.ExceptionFilterListResponseFromJSON = ExceptionFilterListResponseFromJSON;
function ExceptionFilterListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ExceptionFiltersListFilterFromJSON(json['filters']),
        'exceptionFilters': !runtime_1.exists(json, 'exceptionFilters') ? undefined : (json['exceptionFilters'].map(_1.ExceptionFilterFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ExceptionFilterListResponseFromJSONTyped = ExceptionFilterListResponseFromJSONTyped;
function ExceptionFilterListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ExceptionFiltersListFilterToJSON(value.filters),
        'exceptionFilters': value.exceptionFilters === undefined ? undefined : (value.exceptionFilters.map(_1.ExceptionFilterToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ExceptionFilterListResponseToJSON = ExceptionFilterListResponseToJSON;
