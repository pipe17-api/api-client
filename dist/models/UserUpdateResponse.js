"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateResponseToJSON = exports.UserUpdateResponseFromJSONTyped = exports.UserUpdateResponseFromJSON = exports.UserUpdateResponseCodeEnum = exports.UserUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var UserUpdateResponseSuccessEnum;
(function (UserUpdateResponseSuccessEnum) {
    UserUpdateResponseSuccessEnum["True"] = "true";
})(UserUpdateResponseSuccessEnum = exports.UserUpdateResponseSuccessEnum || (exports.UserUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var UserUpdateResponseCodeEnum;
(function (UserUpdateResponseCodeEnum) {
    UserUpdateResponseCodeEnum[UserUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    UserUpdateResponseCodeEnum[UserUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    UserUpdateResponseCodeEnum[UserUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(UserUpdateResponseCodeEnum = exports.UserUpdateResponseCodeEnum || (exports.UserUpdateResponseCodeEnum = {}));
function UserUpdateResponseFromJSON(json) {
    return UserUpdateResponseFromJSONTyped(json, false);
}
exports.UserUpdateResponseFromJSON = UserUpdateResponseFromJSON;
function UserUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.UserUpdateResponseFromJSONTyped = UserUpdateResponseFromJSONTyped;
function UserUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.UserUpdateResponseToJSON = UserUpdateResponseToJSON;
