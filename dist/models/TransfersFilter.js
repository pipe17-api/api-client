"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersFilterToJSON = exports.TransfersFilterFromJSONTyped = exports.TransfersFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransfersFilterFromJSON(json) {
    return TransfersFilterFromJSONTyped(json, false);
}
exports.TransfersFilterFromJSON = TransfersFilterFromJSON;
function TransfersFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'transferId': !runtime_1.exists(json, 'transferId') ? undefined : json['transferId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.TransferStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.TransfersFilterFromJSONTyped = TransfersFilterFromJSONTyped;
function TransfersFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'transferId': value.transferId,
        'extOrderId': value.extOrderId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.TransferStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.TransfersFilterToJSON = TransfersFilterToJSON;
