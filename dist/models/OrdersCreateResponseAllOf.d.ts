/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderCreateResult } from './';
/**
 *
 * @export
 * @interface OrdersCreateResponseAllOf
 */
export interface OrdersCreateResponseAllOf {
    /**
     *
     * @type {Array<OrderCreateResult>}
     * @memberof OrdersCreateResponseAllOf
     */
    orders?: Array<OrderCreateResult>;
}
export declare function OrdersCreateResponseAllOfFromJSON(json: any): OrdersCreateResponseAllOf;
export declare function OrdersCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersCreateResponseAllOf;
export declare function OrdersCreateResponseAllOfToJSON(value?: OrdersCreateResponseAllOf | null): any;
