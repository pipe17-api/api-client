/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ProductBundleItem
 */
export interface ProductBundleItem {
    /**
     * Bundle Item SKU
     * @type {string}
     * @memberof ProductBundleItem
     */
    sku?: string;
    /**
     * Bundle Item Quantity
     * @type {number}
     * @memberof ProductBundleItem
     */
    quantity?: number;
}
export declare function ProductBundleItemFromJSON(json: any): ProductBundleItem;
export declare function ProductBundleItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductBundleItem;
export declare function ProductBundleItemToJSON(value?: ProductBundleItem | null): any;
