"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundCreateRequestToJSON = exports.RefundCreateRequestFromJSONTyped = exports.RefundCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundCreateRequestFromJSON(json) {
    return RefundCreateRequestFromJSONTyped(json, false);
}
exports.RefundCreateRequestFromJSON = RefundCreateRequestFromJSON;
function RefundCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'returnId': json['returnId'],
        'extRefundId': !runtime_1.exists(json, 'extRefundId') ? undefined : json['extRefundId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'type': _1.RefundTypeFromJSON(json['type']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.RefundStatusFromJSON(json['status']),
        'currency': !runtime_1.exists(json, 'currency') ? undefined : json['currency'],
        'amount': !runtime_1.exists(json, 'amount') ? undefined : json['amount'],
        'creditIssued': !runtime_1.exists(json, 'creditIssued') ? undefined : json['creditIssued'],
        'creditSpent': !runtime_1.exists(json, 'creditSpent') ? undefined : json['creditSpent'],
        'merchantInvoiceAmount': !runtime_1.exists(json, 'merchantInvoiceAmount') ? undefined : json['merchantInvoiceAmount'],
        'customerInvoiceAmount': !runtime_1.exists(json, 'customerInvoiceAmount') ? undefined : json['customerInvoiceAmount'],
    };
}
exports.RefundCreateRequestFromJSONTyped = RefundCreateRequestFromJSONTyped;
function RefundCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'returnId': value.returnId,
        'extRefundId': value.extRefundId,
        'extOrderId': value.extOrderId,
        'type': _1.RefundTypeToJSON(value.type),
        'status': _1.RefundStatusToJSON(value.status),
        'currency': value.currency,
        'amount': value.amount,
        'creditIssued': value.creditIssued,
        'creditSpent': value.creditSpent,
        'merchantInvoiceAmount': value.merchantInvoiceAmount,
        'customerInvoiceAmount': value.customerInvoiceAmount,
    };
}
exports.RefundCreateRequestToJSON = RefundCreateRequestToJSON;
