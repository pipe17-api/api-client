"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobsDeleteResponseAllOfToJSON = exports.JobsDeleteResponseAllOfFromJSONTyped = exports.JobsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobsDeleteResponseAllOfFromJSON(json) {
    return JobsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.JobsDeleteResponseAllOfFromJSON = JobsDeleteResponseAllOfFromJSON;
function JobsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.JobsDeleteFilterFromJSON(json['filters']),
        'jobs': !runtime_1.exists(json, 'jobs') ? undefined : (json['jobs'].map(_1.JobFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.JobsDeleteResponseAllOfFromJSONTyped = JobsDeleteResponseAllOfFromJSONTyped;
function JobsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.JobsDeleteFilterToJSON(value.filters),
        'jobs': value.jobs === undefined ? undefined : (value.jobs.map(_1.JobToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.JobsDeleteResponseAllOfToJSON = JobsDeleteResponseAllOfToJSON;
