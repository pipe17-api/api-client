/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Refund } from './';
/**
 *
 * @export
 * @interface RefundFetchResponse
 */
export interface RefundFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RefundFetchResponse
     */
    success: RefundFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundFetchResponse
     */
    code: RefundFetchResponseCodeEnum;
    /**
     *
     * @type {Refund}
     * @memberof RefundFetchResponse
     */
    refund?: Refund;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RefundFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RefundFetchResponseFromJSON(json: any): RefundFetchResponse;
export declare function RefundFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundFetchResponse;
export declare function RefundFetchResponseToJSON(value?: RefundFetchResponse | null): any;
