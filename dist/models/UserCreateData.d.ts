/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserAddress } from './';
/**
 *
 * @export
 * @interface UserCreateData
 */
export interface UserCreateData {
    /**
     * User Name
     * @type {string}
     * @memberof UserCreateData
     */
    name: string;
    /**
     * User email
     * @type {string}
     * @memberof UserCreateData
     */
    email: string;
    /**
     * User named Role(s)
     * @type {Array<string>}
     * @memberof UserCreateData
     */
    roles: Array<string>;
    /**
     * User Phone
     * @type {string}
     * @memberof UserCreateData
     */
    phone?: string;
    /**
     * User Time Zone
     * @type {string}
     * @memberof UserCreateData
     */
    timeZone?: string;
    /**
     * User Description
     * @type {string}
     * @memberof UserCreateData
     */
    description?: string;
    /**
     *
     * @type {UserAddress}
     * @memberof UserCreateData
     */
    address?: UserAddress;
}
export declare function UserCreateDataFromJSON(json: any): UserCreateData;
export declare function UserCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateData;
export declare function UserCreateDataToJSON(value?: UserCreateData | null): any;
