/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntitySchema
 */
export interface EntitySchema {
    /**
     * Schema ID
     * @type {string}
     * @memberof EntitySchema
     */
    schemaId?: string;
    /**
     * Schema name
     * @type {string}
     * @memberof EntitySchema
     */
    name: string;
    /**
     * Entity schema field descriptors
     * @type {Array<object>}
     * @memberof EntitySchema
     */
    fields: Array<object>;
    /**
     *
     * @type {EntityName}
     * @memberof EntitySchema
     */
    entity: EntityName;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof EntitySchema
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof EntitySchema
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof EntitySchema
     */
    readonly orgKey?: string;
}
export declare function EntitySchemaFromJSON(json: any): EntitySchema;
export declare function EntitySchemaFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchema;
export declare function EntitySchemaToJSON(value?: EntitySchema | null): any;
