/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingUpdateRule } from './';
/**
 *
 * @export
 * @interface MappingUpdateData
 */
export interface MappingUpdateData {
    /**
     * Public mapping indicator
     * @type {boolean}
     * @memberof MappingUpdateData
     */
    isPublic?: boolean;
    /**
     * Description
     * @type {string}
     * @memberof MappingUpdateData
     */
    description?: string;
    /**
     *
     * @type {Array<MappingUpdateRule>}
     * @memberof MappingUpdateData
     */
    mappings?: Array<MappingUpdateRule>;
    /**
     *
     * @type {string}
     * @memberof MappingUpdateData
     */
    originId?: string;
}
export declare function MappingUpdateDataFromJSON(json: any): MappingUpdateData;
export declare function MappingUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingUpdateData;
export declare function MappingUpdateDataToJSON(value?: MappingUpdateData | null): any;
