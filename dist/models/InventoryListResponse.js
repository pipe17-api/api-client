"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryListResponseToJSON = exports.InventoryListResponseFromJSONTyped = exports.InventoryListResponseFromJSON = exports.InventoryListResponseCodeEnum = exports.InventoryListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryListResponseSuccessEnum;
(function (InventoryListResponseSuccessEnum) {
    InventoryListResponseSuccessEnum["True"] = "true";
})(InventoryListResponseSuccessEnum = exports.InventoryListResponseSuccessEnum || (exports.InventoryListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryListResponseCodeEnum;
(function (InventoryListResponseCodeEnum) {
    InventoryListResponseCodeEnum[InventoryListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryListResponseCodeEnum[InventoryListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryListResponseCodeEnum[InventoryListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryListResponseCodeEnum = exports.InventoryListResponseCodeEnum || (exports.InventoryListResponseCodeEnum = {}));
function InventoryListResponseFromJSON(json) {
    return InventoryListResponseFromJSONTyped(json, false);
}
exports.InventoryListResponseFromJSON = InventoryListResponseFromJSON;
function InventoryListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryListFilterFromJSON(json['filters']),
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryListResponseFromJSONTyped = InventoryListResponseFromJSONTyped;
function InventoryListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.InventoryListFilterToJSON(value.filters),
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryListResponseToJSON = InventoryListResponseToJSON;
