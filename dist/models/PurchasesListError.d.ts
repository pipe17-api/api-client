/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchasesListFilter } from './';
/**
 *
 * @export
 * @interface PurchasesListError
 */
export interface PurchasesListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof PurchasesListError
     */
    success: PurchasesListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchasesListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof PurchasesListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof PurchasesListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {PurchasesListFilter}
     * @memberof PurchasesListError
     */
    filters?: PurchasesListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchasesListErrorSuccessEnum {
    False = "false"
}
export declare function PurchasesListErrorFromJSON(json: any): PurchasesListError;
export declare function PurchasesListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesListError;
export declare function PurchasesListErrorToJSON(value?: PurchasesListError | null): any;
