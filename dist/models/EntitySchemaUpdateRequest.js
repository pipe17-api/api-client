"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaUpdateRequestToJSON = exports.EntitySchemaUpdateRequestFromJSONTyped = exports.EntitySchemaUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
function EntitySchemaUpdateRequestFromJSON(json) {
    return EntitySchemaUpdateRequestFromJSONTyped(json, false);
}
exports.EntitySchemaUpdateRequestFromJSON = EntitySchemaUpdateRequestFromJSON;
function EntitySchemaUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'fields': !runtime_1.exists(json, 'fields') ? undefined : json['fields'],
    };
}
exports.EntitySchemaUpdateRequestFromJSONTyped = EntitySchemaUpdateRequestFromJSONTyped;
function EntitySchemaUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'fields': value.fields,
    };
}
exports.EntitySchemaUpdateRequestToJSON = EntitySchemaUpdateRequestToJSON;
