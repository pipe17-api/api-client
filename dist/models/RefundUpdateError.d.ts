/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundUpdateData } from './';
/**
 *
 * @export
 * @interface RefundUpdateError
 */
export interface RefundUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RefundUpdateError
     */
    success: RefundUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RefundUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RefundUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RefundUpdateData}
     * @memberof RefundUpdateError
     */
    refund?: RefundUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundUpdateErrorSuccessEnum {
    False = "false"
}
export declare function RefundUpdateErrorFromJSON(json: any): RefundUpdateError;
export declare function RefundUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundUpdateError;
export declare function RefundUpdateErrorToJSON(value?: RefundUpdateError | null): any;
