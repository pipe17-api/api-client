/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReceiptFetchError
 */
export interface ReceiptFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReceiptFetchError
     */
    success: ReceiptFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReceiptFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReceiptFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReceiptFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptFetchErrorSuccessEnum {
    False = "false"
}
export declare function ReceiptFetchErrorFromJSON(json: any): ReceiptFetchError;
export declare function ReceiptFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptFetchError;
export declare function ReceiptFetchErrorToJSON(value?: ReceiptFetchError | null): any;
