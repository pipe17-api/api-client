/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Failure
 */
export interface Failure {
    /**
     * Always false
     * @type {boolean}
     * @memberof Failure
     */
    success: FailureSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof Failure
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof Failure
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof Failure
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum FailureSuccessEnum {
    False = "false"
}
export declare function FailureFromJSON(json: any): Failure;
export declare function FailureFromJSONTyped(json: any, ignoreDiscriminator: boolean): Failure;
export declare function FailureToJSON(value?: Failure | null): any;
