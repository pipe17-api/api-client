"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountCreateResponseAllOfToJSON = exports.AccountCreateResponseAllOfFromJSONTyped = exports.AccountCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountCreateResponseAllOfFromJSON(json) {
    return AccountCreateResponseAllOfFromJSONTyped(json, false);
}
exports.AccountCreateResponseAllOfFromJSON = AccountCreateResponseAllOfFromJSON;
function AccountCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountFromJSON(json['account']),
        'activationKey': !runtime_1.exists(json, 'activationKey') ? undefined : json['activationKey'],
    };
}
exports.AccountCreateResponseAllOfFromJSONTyped = AccountCreateResponseAllOfFromJSONTyped;
function AccountCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'account': _1.AccountToJSON(value.account),
        'activationKey': value.activationKey,
    };
}
exports.AccountCreateResponseAllOfToJSON = AccountCreateResponseAllOfToJSON;
