"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsListFilterToJSON = exports.LocationsListFilterFromJSONTyped = exports.LocationsListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationsListFilterFromJSON(json) {
    return LocationsListFilterFromJSONTyped(json, false);
}
exports.LocationsListFilterFromJSON = LocationsListFilterFromJSON;
function LocationsListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.LocationStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'extLocationId': !runtime_1.exists(json, 'extLocationId') ? undefined : json['extLocationId'],
        'extIntegrationId': !runtime_1.exists(json, 'extIntegrationId') ? undefined : json['extIntegrationId'],
    };
}
exports.LocationsListFilterFromJSONTyped = LocationsListFilterFromJSONTyped;
function LocationsListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'locationId': value.locationId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.LocationStatusToJSON)),
        'deleted': value.deleted,
        'order': value.order,
        'keys': value.keys,
        'name': value.name,
        'email': value.email,
        'phone': value.phone,
        'extLocationId': value.extLocationId,
        'extIntegrationId': value.extIntegrationId,
    };
}
exports.LocationsListFilterToJSON = LocationsListFilterToJSON;
