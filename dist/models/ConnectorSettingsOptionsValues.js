"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSettingsOptionsValuesToJSON = exports.ConnectorSettingsOptionsValuesFromJSONTyped = exports.ConnectorSettingsOptionsValuesFromJSON = void 0;
const runtime_1 = require("../runtime");
function ConnectorSettingsOptionsValuesFromJSON(json) {
    return ConnectorSettingsOptionsValuesFromJSONTyped(json, false);
}
exports.ConnectorSettingsOptionsValuesFromJSON = ConnectorSettingsOptionsValuesFromJSON;
function ConnectorSettingsOptionsValuesFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'label': !runtime_1.exists(json, 'label') ? undefined : json['label'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
    };
}
exports.ConnectorSettingsOptionsValuesFromJSONTyped = ConnectorSettingsOptionsValuesFromJSONTyped;
function ConnectorSettingsOptionsValuesToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'label': value.label,
        'value': value.value,
    };
}
exports.ConnectorSettingsOptionsValuesToJSON = ConnectorSettingsOptionsValuesToJSON;
