/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MethodsUpdateData } from './';
/**
 *
 * @export
 * @interface ApiKeyUpdateData
 */
export interface ApiKeyUpdateData {
    /**
     * API Key Name
     * @type {string}
     * @memberof ApiKeyUpdateData
     */
    name?: string;
    /**
     * API Key Description
     * @type {string}
     * @memberof ApiKeyUpdateData
     */
    description?: string;
    /**
     *
     * @type {MethodsUpdateData}
     * @memberof ApiKeyUpdateData
     */
    methods?: MethodsUpdateData;
    /**
     * IP/CIDR Whitelist
     * @type {Array<string>}
     * @memberof ApiKeyUpdateData
     */
    allowedIPs?: Array<string>;
}
export declare function ApiKeyUpdateDataFromJSON(json: any): ApiKeyUpdateData;
export declare function ApiKeyUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyUpdateData;
export declare function ApiKeyUpdateDataToJSON(value?: ApiKeyUpdateData | null): any;
