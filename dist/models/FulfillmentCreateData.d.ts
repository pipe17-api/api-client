/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentItem } from './';
/**
 *
 * @export
 * @interface FulfillmentCreateData
 */
export interface FulfillmentCreateData {
    /**
     *
     * @type {Array<FulfillmentItem>}
     * @memberof FulfillmentCreateData
     */
    lineItems?: Array<FulfillmentItem>;
    /**
     * Internal Pipe17 ID of the Shipment
     * @type {string}
     * @memberof FulfillmentCreateData
     */
    shipmentId: string;
    /**
     * Tracking number(s)
     * @type {Array<string>}
     * @memberof FulfillmentCreateData
     */
    trackingNumber: Array<string>;
    /**
     * Actual ship date
     * @type {Date}
     * @memberof FulfillmentCreateData
     */
    actualShipDate?: Date;
    /**
     * Expected arrival date
     * @type {Date}
     * @memberof FulfillmentCreateData
     */
    expectedArrivalDate?: Date;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof FulfillmentCreateData
     */
    shippingCarrier?: string;
    /**
     * Shipping Class
     * @type {string}
     * @memberof FulfillmentCreateData
     */
    shippingClass?: string;
    /**
     * Shipping Charge
     * @type {number}
     * @memberof FulfillmentCreateData
     */
    shippingCharge?: number;
    /**
     * Weight unit
     * @type {string}
     * @memberof FulfillmentCreateData
     */
    weightUnit?: FulfillmentCreateDataWeightUnitEnum;
    /**
     * External customer friendly ID
     * @type {string}
     * @memberof FulfillmentCreateData
     */
    extOrderId?: string;
    /**
     * Weight
     * @type {number}
     * @memberof FulfillmentCreateData
     */
    weight?: number;
    /**
     * Reference to fulfillment on external system (vendor)
     * @type {string}
     * @memberof FulfillmentCreateData
     */
    extFulfillmentId?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentCreateDataWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
}
export declare function FulfillmentCreateDataFromJSON(json: any): FulfillmentCreateData;
export declare function FulfillmentCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentCreateData;
export declare function FulfillmentCreateDataToJSON(value?: FulfillmentCreateData | null): any;
