"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentAllOfToJSON = exports.FulfillmentAllOfFromJSONTyped = exports.FulfillmentAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function FulfillmentAllOfFromJSON(json) {
    return FulfillmentAllOfFromJSONTyped(json, false);
}
exports.FulfillmentAllOfFromJSON = FulfillmentAllOfFromJSON;
function FulfillmentAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.FulfillmentItemExtFromJSON)),
    };
}
exports.FulfillmentAllOfFromJSONTyped = FulfillmentAllOfFromJSONTyped;
function FulfillmentAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fulfillmentId': value.fulfillmentId,
        'integration': value.integration,
        'locationId': value.locationId,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.FulfillmentItemExtToJSON)),
    };
}
exports.FulfillmentAllOfToJSON = FulfillmentAllOfToJSON;
