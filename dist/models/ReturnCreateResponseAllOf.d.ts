/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Return } from './';
/**
 *
 * @export
 * @interface ReturnCreateResponseAllOf
 */
export interface ReturnCreateResponseAllOf {
    /**
     *
     * @type {Return}
     * @memberof ReturnCreateResponseAllOf
     */
    _return?: Return;
}
export declare function ReturnCreateResponseAllOfFromJSON(json: any): ReturnCreateResponseAllOf;
export declare function ReturnCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnCreateResponseAllOf;
export declare function ReturnCreateResponseAllOfToJSON(value?: ReturnCreateResponseAllOf | null): any;
