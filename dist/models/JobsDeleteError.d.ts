/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobsDeleteFilter } from './';
/**
 *
 * @export
 * @interface JobsDeleteError
 */
export interface JobsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof JobsDeleteError
     */
    success: JobsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof JobsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof JobsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {JobsDeleteFilter}
     * @memberof JobsDeleteError
     */
    filters?: JobsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum JobsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function JobsDeleteErrorFromJSON(json: any): JobsDeleteError;
export declare function JobsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsDeleteError;
export declare function JobsDeleteErrorToJSON(value?: JobsDeleteError | null): any;
