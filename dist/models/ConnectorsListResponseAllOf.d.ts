/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Connector, ConnectorsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ConnectorsListResponseAllOf
 */
export interface ConnectorsListResponseAllOf {
    /**
     *
     * @type {ConnectorsListFilter}
     * @memberof ConnectorsListResponseAllOf
     */
    filters?: ConnectorsListFilter;
    /**
     *
     * @type {Array<Connector>}
     * @memberof ConnectorsListResponseAllOf
     */
    connectors?: Array<Connector>;
    /**
     *
     * @type {Pagination}
     * @memberof ConnectorsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ConnectorsListResponseAllOfFromJSON(json: any): ConnectorsListResponseAllOf;
export declare function ConnectorsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsListResponseAllOf;
export declare function ConnectorsListResponseAllOfToJSON(value?: ConnectorsListResponseAllOf | null): any;
