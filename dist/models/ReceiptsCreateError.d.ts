/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptCreateResult } from './';
/**
 *
 * @export
 * @interface ReceiptsCreateError
 */
export interface ReceiptsCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReceiptsCreateError
     */
    success: ReceiptsCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReceiptsCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReceiptsCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReceiptsCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<ReceiptCreateResult>}
     * @memberof ReceiptsCreateError
     */
    receipts?: Array<ReceiptCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptsCreateErrorSuccessEnum {
    False = "false"
}
export declare function ReceiptsCreateErrorFromJSON(json: any): ReceiptsCreateError;
export declare function ReceiptsCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsCreateError;
export declare function ReceiptsCreateErrorToJSON(value?: ReceiptsCreateError | null): any;
