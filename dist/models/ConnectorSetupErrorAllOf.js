"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSetupErrorAllOfToJSON = exports.ConnectorSetupErrorAllOfFromJSONTyped = exports.ConnectorSetupErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConnectorSetupErrorAllOfFromJSON(json) {
    return ConnectorSetupErrorAllOfFromJSONTyped(json, false);
}
exports.ConnectorSetupErrorAllOfFromJSON = ConnectorSetupErrorAllOfFromJSON;
function ConnectorSetupErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'options': !runtime_1.exists(json, 'options') ? undefined : _1.ConnectorSetupDataFromJSON(json['options']),
    };
}
exports.ConnectorSetupErrorAllOfFromJSONTyped = ConnectorSetupErrorAllOfFromJSONTyped;
function ConnectorSetupErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'options': _1.ConnectorSetupDataToJSON(value.options),
    };
}
exports.ConnectorSetupErrorAllOfToJSON = ConnectorSetupErrorAllOfToJSON;
