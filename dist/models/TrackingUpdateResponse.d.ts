/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TrackingUpdateResponse
 */
export interface TrackingUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TrackingUpdateResponse
     */
    success: TrackingUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingUpdateResponse
     */
    code: TrackingUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TrackingUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TrackingUpdateResponseFromJSON(json: any): TrackingUpdateResponse;
export declare function TrackingUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingUpdateResponse;
export declare function TrackingUpdateResponseToJSON(value?: TrackingUpdateResponse | null): any;
