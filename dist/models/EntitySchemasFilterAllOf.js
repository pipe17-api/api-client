"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemasFilterAllOfToJSON = exports.EntitySchemasFilterAllOfFromJSONTyped = exports.EntitySchemasFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntitySchemasFilterAllOfFromJSON(json) {
    return EntitySchemasFilterAllOfFromJSONTyped(json, false);
}
exports.EntitySchemasFilterAllOfFromJSON = EntitySchemasFilterAllOfFromJSON;
function EntitySchemasFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'schemaId': !runtime_1.exists(json, 'schemaId') ? undefined : json['schemaId'],
        'entity': !runtime_1.exists(json, 'entity') ? undefined : (json['entity'].map(_1.EntityNameFromJSON)),
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
    };
}
exports.EntitySchemasFilterAllOfFromJSONTyped = EntitySchemasFilterAllOfFromJSONTyped;
function EntitySchemasFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'schemaId': value.schemaId,
        'entity': value.entity === undefined ? undefined : (value.entity.map(_1.EntityNameToJSON)),
        'name': value.name,
    };
}
exports.EntitySchemasFilterAllOfToJSON = EntitySchemasFilterAllOfToJSON;
