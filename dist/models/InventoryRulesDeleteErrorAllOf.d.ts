/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRulesDeleteFilter } from './';
/**
 *
 * @export
 * @interface InventoryRulesDeleteErrorAllOf
 */
export interface InventoryRulesDeleteErrorAllOf {
    /**
     *
     * @type {InventoryRulesDeleteFilter}
     * @memberof InventoryRulesDeleteErrorAllOf
     */
    filters?: InventoryRulesDeleteFilter;
}
export declare function InventoryRulesDeleteErrorAllOfFromJSON(json: any): InventoryRulesDeleteErrorAllOf;
export declare function InventoryRulesDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesDeleteErrorAllOf;
export declare function InventoryRulesDeleteErrorAllOfToJSON(value?: InventoryRulesDeleteErrorAllOf | null): any;
