"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaUpdateDataToJSON = exports.EntitySchemaUpdateDataFromJSONTyped = exports.EntitySchemaUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function EntitySchemaUpdateDataFromJSON(json) {
    return EntitySchemaUpdateDataFromJSONTyped(json, false);
}
exports.EntitySchemaUpdateDataFromJSON = EntitySchemaUpdateDataFromJSON;
function EntitySchemaUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'fields': !runtime_1.exists(json, 'fields') ? undefined : json['fields'],
    };
}
exports.EntitySchemaUpdateDataFromJSONTyped = EntitySchemaUpdateDataFromJSONTyped;
function EntitySchemaUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'fields': value.fields,
    };
}
exports.EntitySchemaUpdateDataToJSON = EntitySchemaUpdateDataToJSON;
