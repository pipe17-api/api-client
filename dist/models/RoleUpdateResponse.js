"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleUpdateResponseToJSON = exports.RoleUpdateResponseFromJSONTyped = exports.RoleUpdateResponseFromJSON = exports.RoleUpdateResponseCodeEnum = exports.RoleUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var RoleUpdateResponseSuccessEnum;
(function (RoleUpdateResponseSuccessEnum) {
    RoleUpdateResponseSuccessEnum["True"] = "true";
})(RoleUpdateResponseSuccessEnum = exports.RoleUpdateResponseSuccessEnum || (exports.RoleUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoleUpdateResponseCodeEnum;
(function (RoleUpdateResponseCodeEnum) {
    RoleUpdateResponseCodeEnum[RoleUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoleUpdateResponseCodeEnum[RoleUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoleUpdateResponseCodeEnum[RoleUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoleUpdateResponseCodeEnum = exports.RoleUpdateResponseCodeEnum || (exports.RoleUpdateResponseCodeEnum = {}));
function RoleUpdateResponseFromJSON(json) {
    return RoleUpdateResponseFromJSONTyped(json, false);
}
exports.RoleUpdateResponseFromJSON = RoleUpdateResponseFromJSON;
function RoleUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.RoleUpdateResponseFromJSONTyped = RoleUpdateResponseFromJSONTyped;
function RoleUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.RoleUpdateResponseToJSON = RoleUpdateResponseToJSON;
