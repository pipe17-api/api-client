/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 * Entity Filters Filter
 * @export
 * @interface EntityFiltersFilterAllOf
 */
export interface EntityFiltersFilterAllOf {
    /**
     * Fetch by list of filterId
     * @type {Array<string>}
     * @memberof EntityFiltersFilterAllOf
     */
    filterId?: Array<string>;
    /**
     * Fetch by list of entity types
     * @type {Array<EntityName>}
     * @memberof EntityFiltersFilterAllOf
     */
    entity?: Array<EntityName>;
    /**
     * Fetch by list of filter names
     * @type {Array<string>}
     * @memberof EntityFiltersFilterAllOf
     */
    name?: Array<string>;
}
export declare function EntityFiltersFilterAllOfFromJSON(json: any): EntityFiltersFilterAllOf;
export declare function EntityFiltersFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFiltersFilterAllOf;
export declare function EntityFiltersFilterAllOfToJSON(value?: EntityFiltersFilterAllOf | null): any;
