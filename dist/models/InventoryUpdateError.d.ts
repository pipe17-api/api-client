/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryUpdateData } from './';
/**
 *
 * @export
 * @interface InventoryUpdateError
 */
export interface InventoryUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryUpdateError
     */
    success: InventoryUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryUpdateData}
     * @memberof InventoryUpdateError
     */
    inventory?: InventoryUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateErrorSuccessEnum {
    False = "false"
}
export declare function InventoryUpdateErrorFromJSON(json: any): InventoryUpdateError;
export declare function InventoryUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryUpdateError;
export declare function InventoryUpdateErrorToJSON(value?: InventoryUpdateError | null): any;
