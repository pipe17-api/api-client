/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Shipment Status
 * @export
 * @enum {string}
 */
export declare enum ShipmentCreateStatus {
    PendingInventory = "pendingInventory",
    PendingShippingLabel = "pendingShippingLabel",
    ReadyForFulfillment = "readyForFulfillment"
}
export declare function ShipmentCreateStatusFromJSON(json: any): ShipmentCreateStatus;
export declare function ShipmentCreateStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentCreateStatus;
export declare function ShipmentCreateStatusToJSON(value?: ShipmentCreateStatus | null): any;
