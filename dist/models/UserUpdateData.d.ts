/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserAddress } from './';
/**
 *
 * @export
 * @interface UserUpdateData
 */
export interface UserUpdateData {
    /**
     * User Name
     * @type {string}
     * @memberof UserUpdateData
     */
    name?: string;
    /**
     * User named Role(s)
     * @type {Array<string>}
     * @memberof UserUpdateData
     */
    roles?: Array<string>;
    /**
     * User Phone
     * @type {string}
     * @memberof UserUpdateData
     */
    phone?: string;
    /**
     * User Time Zone
     * @type {string}
     * @memberof UserUpdateData
     */
    timeZone?: string;
    /**
     * User Description
     * @type {string}
     * @memberof UserUpdateData
     */
    description?: string;
    /**
     *
     * @type {UserAddress}
     * @memberof UserUpdateData
     */
    address?: UserAddress;
    /**
     *
     * @type {string}
     * @memberof UserUpdateData
     */
    status?: UserUpdateDataStatusEnum;
    /**
     * User password
     * @type {string}
     * @memberof UserUpdateData
     */
    password?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum UserUpdateDataStatusEnum {
    Active = "active",
    Disabled = "disabled"
}
export declare function UserUpdateDataFromJSON(json: any): UserUpdateData;
export declare function UserUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserUpdateData;
export declare function UserUpdateDataToJSON(value?: UserUpdateData | null): any;
