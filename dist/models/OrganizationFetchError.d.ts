/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrganizationFetchError
 */
export interface OrganizationFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrganizationFetchError
     */
    success: OrganizationFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrganizationFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrganizationFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationFetchErrorSuccessEnum {
    False = "false"
}
export declare function OrganizationFetchErrorFromJSON(json: any): OrganizationFetchError;
export declare function OrganizationFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationFetchError;
export declare function OrganizationFetchErrorToJSON(value?: OrganizationFetchError | null): any;
