/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LabelsDeleteFilter } from './';
/**
 *
 * @export
 * @interface LabelsDeleteError
 */
export interface LabelsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LabelsDeleteError
     */
    success: LabelsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LabelsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LabelsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {LabelsDeleteFilter}
     * @memberof LabelsDeleteError
     */
    filters?: LabelsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function LabelsDeleteErrorFromJSON(json: any): LabelsDeleteError;
export declare function LabelsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsDeleteError;
export declare function LabelsDeleteErrorToJSON(value?: LabelsDeleteError | null): any;
