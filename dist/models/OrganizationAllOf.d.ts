/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationStatus } from './';
/**
 *
 * @export
 * @interface OrganizationAllOf
 */
export interface OrganizationAllOf {
    /**
     *
     * @type {OrganizationStatus}
     * @memberof OrganizationAllOf
     */
    status?: OrganizationStatus;
}
export declare function OrganizationAllOfFromJSON(json: any): OrganizationAllOf;
export declare function OrganizationAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationAllOf;
export declare function OrganizationAllOfToJSON(value?: OrganizationAllOf | null): any;
