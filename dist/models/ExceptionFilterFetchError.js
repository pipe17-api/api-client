"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterFetchErrorToJSON = exports.ExceptionFilterFetchErrorFromJSONTyped = exports.ExceptionFilterFetchErrorFromJSON = exports.ExceptionFilterFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ExceptionFilterFetchErrorSuccessEnum;
(function (ExceptionFilterFetchErrorSuccessEnum) {
    ExceptionFilterFetchErrorSuccessEnum["False"] = "false";
})(ExceptionFilterFetchErrorSuccessEnum = exports.ExceptionFilterFetchErrorSuccessEnum || (exports.ExceptionFilterFetchErrorSuccessEnum = {}));
function ExceptionFilterFetchErrorFromJSON(json) {
    return ExceptionFilterFetchErrorFromJSONTyped(json, false);
}
exports.ExceptionFilterFetchErrorFromJSON = ExceptionFilterFetchErrorFromJSON;
function ExceptionFilterFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.ExceptionFilterFetchErrorFromJSONTyped = ExceptionFilterFetchErrorFromJSONTyped;
function ExceptionFilterFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.ExceptionFilterFetchErrorToJSON = ExceptionFilterFetchErrorToJSON;
