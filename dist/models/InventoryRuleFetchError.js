"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleFetchErrorToJSON = exports.InventoryRuleFetchErrorFromJSONTyped = exports.InventoryRuleFetchErrorFromJSON = exports.InventoryRuleFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var InventoryRuleFetchErrorSuccessEnum;
(function (InventoryRuleFetchErrorSuccessEnum) {
    InventoryRuleFetchErrorSuccessEnum["False"] = "false";
})(InventoryRuleFetchErrorSuccessEnum = exports.InventoryRuleFetchErrorSuccessEnum || (exports.InventoryRuleFetchErrorSuccessEnum = {}));
function InventoryRuleFetchErrorFromJSON(json) {
    return InventoryRuleFetchErrorFromJSONTyped(json, false);
}
exports.InventoryRuleFetchErrorFromJSON = InventoryRuleFetchErrorFromJSON;
function InventoryRuleFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.InventoryRuleFetchErrorFromJSONTyped = InventoryRuleFetchErrorFromJSONTyped;
function InventoryRuleFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.InventoryRuleFetchErrorToJSON = InventoryRuleFetchErrorToJSON;
