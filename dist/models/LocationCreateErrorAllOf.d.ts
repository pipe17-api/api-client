/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationCreateData } from './';
/**
 *
 * @export
 * @interface LocationCreateErrorAllOf
 */
export interface LocationCreateErrorAllOf {
    /**
     *
     * @type {LocationCreateData}
     * @memberof LocationCreateErrorAllOf
     */
    location?: LocationCreateData;
}
export declare function LocationCreateErrorAllOfFromJSON(json: any): LocationCreateErrorAllOf;
export declare function LocationCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationCreateErrorAllOf;
export declare function LocationCreateErrorAllOfToJSON(value?: LocationCreateErrorAllOf | null): any;
