"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationFetchResponseToJSON = exports.IntegrationFetchResponseFromJSONTyped = exports.IntegrationFetchResponseFromJSON = exports.IntegrationFetchResponseCodeEnum = exports.IntegrationFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationFetchResponseSuccessEnum;
(function (IntegrationFetchResponseSuccessEnum) {
    IntegrationFetchResponseSuccessEnum["True"] = "true";
})(IntegrationFetchResponseSuccessEnum = exports.IntegrationFetchResponseSuccessEnum || (exports.IntegrationFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationFetchResponseCodeEnum;
(function (IntegrationFetchResponseCodeEnum) {
    IntegrationFetchResponseCodeEnum[IntegrationFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationFetchResponseCodeEnum[IntegrationFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationFetchResponseCodeEnum[IntegrationFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationFetchResponseCodeEnum = exports.IntegrationFetchResponseCodeEnum || (exports.IntegrationFetchResponseCodeEnum = {}));
function IntegrationFetchResponseFromJSON(json) {
    return IntegrationFetchResponseFromJSONTyped(json, false);
}
exports.IntegrationFetchResponseFromJSON = IntegrationFetchResponseFromJSON;
function IntegrationFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : _1.IntegrationFromJSON(json['integration']),
    };
}
exports.IntegrationFetchResponseFromJSONTyped = IntegrationFetchResponseFromJSONTyped;
function IntegrationFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'integration': _1.IntegrationToJSON(value.integration),
    };
}
exports.IntegrationFetchResponseToJSON = IntegrationFetchResponseToJSON;
