"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityRestrictionsValueAllOfToJSON = exports.EntityRestrictionsValueAllOfFromJSONTyped = exports.EntityRestrictionsValueAllOfFromJSON = exports.EntityRestrictionsValueAllOfTypeEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var EntityRestrictionsValueAllOfTypeEnum;
(function (EntityRestrictionsValueAllOfTypeEnum) {
    EntityRestrictionsValueAllOfTypeEnum["Boolean"] = "boolean";
    EntityRestrictionsValueAllOfTypeEnum["Date"] = "date";
    EntityRestrictionsValueAllOfTypeEnum["Array"] = "array";
})(EntityRestrictionsValueAllOfTypeEnum = exports.EntityRestrictionsValueAllOfTypeEnum || (exports.EntityRestrictionsValueAllOfTypeEnum = {}));
function EntityRestrictionsValueAllOfFromJSON(json) {
    return EntityRestrictionsValueAllOfFromJSONTyped(json, false);
}
exports.EntityRestrictionsValueAllOfFromJSON = EntityRestrictionsValueAllOfFromJSON;
function EntityRestrictionsValueAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'type': !runtime_1.exists(json, 'type') ? undefined : json['type'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
    };
}
exports.EntityRestrictionsValueAllOfFromJSONTyped = EntityRestrictionsValueAllOfFromJSONTyped;
function EntityRestrictionsValueAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'type': value.type,
        'value': value.value,
    };
}
exports.EntityRestrictionsValueAllOfToJSON = EntityRestrictionsValueAllOfToJSON;
