/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventReplayError
 */
export interface EventReplayError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EventReplayError
     */
    success: EventReplayErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventReplayError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EventReplayError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EventReplayError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EventReplayErrorSuccessEnum {
    False = "false"
}
export declare function EventReplayErrorFromJSON(json: any): EventReplayError;
export declare function EventReplayErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventReplayError;
export declare function EventReplayErrorToJSON(value?: EventReplayError | null): any;
