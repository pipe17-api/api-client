/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Mapping, MappingsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface MappingsListResponse
 */
export interface MappingsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof MappingsListResponse
     */
    success: MappingsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingsListResponse
     */
    code: MappingsListResponseCodeEnum;
    /**
     *
     * @type {MappingsListFilter}
     * @memberof MappingsListResponse
     */
    filters?: MappingsListFilter;
    /**
     *
     * @type {Array<Mapping>}
     * @memberof MappingsListResponse
     */
    mappings?: Array<Mapping>;
    /**
     *
     * @type {Pagination}
     * @memberof MappingsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum MappingsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function MappingsListResponseFromJSON(json: any): MappingsListResponse;
export declare function MappingsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsListResponse;
export declare function MappingsListResponseToJSON(value?: MappingsListResponse | null): any;
