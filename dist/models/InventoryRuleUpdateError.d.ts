/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRuleUpdateData } from './';
/**
 *
 * @export
 * @interface InventoryRuleUpdateError
 */
export interface InventoryRuleUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryRuleUpdateError
     */
    success: InventoryRuleUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRuleUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryRuleUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryRuleUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryRuleUpdateData}
     * @memberof InventoryRuleUpdateError
     */
    inventoryrule?: InventoryRuleUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRuleUpdateErrorSuccessEnum {
    False = "false"
}
export declare function InventoryRuleUpdateErrorFromJSON(json: any): InventoryRuleUpdateError;
export declare function InventoryRuleUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleUpdateError;
export declare function InventoryRuleUpdateErrorToJSON(value?: InventoryRuleUpdateError | null): any;
