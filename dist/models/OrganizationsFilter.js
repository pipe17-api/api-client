"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationsFilterToJSON = exports.OrganizationsFilterFromJSONTyped = exports.OrganizationsFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationsFilterFromJSON(json) {
    return OrganizationsFilterFromJSONTyped(json, false);
}
exports.OrganizationsFilterFromJSON = OrganizationsFilterFromJSON;
function OrganizationsFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.OrganizationStatusFromJSON)),
    };
}
exports.OrganizationsFilterFromJSONTyped = OrganizationsFilterFromJSONTyped;
function OrganizationsFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'orgKey': value.orgKey,
        'name': value.name,
        'phone': value.phone,
        'email': value.email,
        'status': value.status === undefined ? undefined : (value.status.map(_1.OrganizationStatusToJSON)),
    };
}
exports.OrganizationsFilterToJSON = OrganizationsFilterToJSON;
