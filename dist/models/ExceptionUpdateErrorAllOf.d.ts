/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionUpdateData } from './';
/**
 *
 * @export
 * @interface ExceptionUpdateErrorAllOf
 */
export interface ExceptionUpdateErrorAllOf {
    /**
     *
     * @type {ExceptionUpdateData}
     * @memberof ExceptionUpdateErrorAllOf
     */
    exception?: ExceptionUpdateData;
}
export declare function ExceptionUpdateErrorAllOfFromJSON(json: any): ExceptionUpdateErrorAllOf;
export declare function ExceptionUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionUpdateErrorAllOf;
export declare function ExceptionUpdateErrorAllOfToJSON(value?: ExceptionUpdateErrorAllOf | null): any;
