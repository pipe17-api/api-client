"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentFetchResponseToJSON = exports.ShipmentFetchResponseFromJSONTyped = exports.ShipmentFetchResponseFromJSON = exports.ShipmentFetchResponseCodeEnum = exports.ShipmentFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentFetchResponseSuccessEnum;
(function (ShipmentFetchResponseSuccessEnum) {
    ShipmentFetchResponseSuccessEnum["True"] = "true";
})(ShipmentFetchResponseSuccessEnum = exports.ShipmentFetchResponseSuccessEnum || (exports.ShipmentFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ShipmentFetchResponseCodeEnum;
(function (ShipmentFetchResponseCodeEnum) {
    ShipmentFetchResponseCodeEnum[ShipmentFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ShipmentFetchResponseCodeEnum[ShipmentFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ShipmentFetchResponseCodeEnum[ShipmentFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ShipmentFetchResponseCodeEnum = exports.ShipmentFetchResponseCodeEnum || (exports.ShipmentFetchResponseCodeEnum = {}));
function ShipmentFetchResponseFromJSON(json) {
    return ShipmentFetchResponseFromJSONTyped(json, false);
}
exports.ShipmentFetchResponseFromJSON = ShipmentFetchResponseFromJSON;
function ShipmentFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'shipment': !runtime_1.exists(json, 'shipment') ? undefined : _1.ShipmentFromJSON(json['shipment']),
    };
}
exports.ShipmentFetchResponseFromJSONTyped = ShipmentFetchResponseFromJSONTyped;
function ShipmentFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'shipment': _1.ShipmentToJSON(value.shipment),
    };
}
exports.ShipmentFetchResponseToJSON = ShipmentFetchResponseToJSON;
