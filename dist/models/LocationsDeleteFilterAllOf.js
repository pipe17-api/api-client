"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsDeleteFilterAllOfToJSON = exports.LocationsDeleteFilterAllOfFromJSONTyped = exports.LocationsDeleteFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function LocationsDeleteFilterAllOfFromJSON(json) {
    return LocationsDeleteFilterAllOfFromJSONTyped(json, false);
}
exports.LocationsDeleteFilterAllOfFromJSON = LocationsDeleteFilterAllOfFromJSON;
function LocationsDeleteFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.LocationsDeleteFilterAllOfFromJSONTyped = LocationsDeleteFilterAllOfFromJSONTyped;
function LocationsDeleteFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
    };
}
exports.LocationsDeleteFilterAllOfToJSON = LocationsDeleteFilterAllOfToJSON;
