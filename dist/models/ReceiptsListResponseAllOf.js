"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsListResponseAllOfToJSON = exports.ReceiptsListResponseAllOfFromJSONTyped = exports.ReceiptsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReceiptsListResponseAllOfFromJSON(json) {
    return ReceiptsListResponseAllOfFromJSONTyped(json, false);
}
exports.ReceiptsListResponseAllOfFromJSON = ReceiptsListResponseAllOfFromJSON;
function ReceiptsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReceiptsListFilterFromJSON(json['filters']),
        'receipts': (json['receipts'].map(_1.ReceiptFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ReceiptsListResponseAllOfFromJSONTyped = ReceiptsListResponseAllOfFromJSONTyped;
function ReceiptsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ReceiptsListFilterToJSON(value.filters),
        'receipts': (value.receipts.map(_1.ReceiptToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ReceiptsListResponseAllOfToJSON = ReceiptsListResponseAllOfToJSON;
