"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleAllOfToJSON = exports.InventoryRuleAllOfFromJSONTyped = exports.InventoryRuleAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryRuleAllOfFromJSON(json) {
    return InventoryRuleAllOfFromJSONTyped(json, false);
}
exports.InventoryRuleAllOfFromJSON = InventoryRuleAllOfFromJSON;
function InventoryRuleAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'ruleId': !runtime_1.exists(json, 'ruleId') ? undefined : json['ruleId'],
    };
}
exports.InventoryRuleAllOfFromJSONTyped = InventoryRuleAllOfFromJSONTyped;
function InventoryRuleAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'ruleId': value.ruleId,
    };
}
exports.InventoryRuleAllOfToJSON = InventoryRuleAllOfToJSON;
