"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionsListResponseToJSON = exports.ExceptionsListResponseFromJSONTyped = exports.ExceptionsListResponseFromJSON = exports.ExceptionsListResponseCodeEnum = exports.ExceptionsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionsListResponseSuccessEnum;
(function (ExceptionsListResponseSuccessEnum) {
    ExceptionsListResponseSuccessEnum["True"] = "true";
})(ExceptionsListResponseSuccessEnum = exports.ExceptionsListResponseSuccessEnum || (exports.ExceptionsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionsListResponseCodeEnum;
(function (ExceptionsListResponseCodeEnum) {
    ExceptionsListResponseCodeEnum[ExceptionsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionsListResponseCodeEnum[ExceptionsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionsListResponseCodeEnum[ExceptionsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionsListResponseCodeEnum = exports.ExceptionsListResponseCodeEnum || (exports.ExceptionsListResponseCodeEnum = {}));
function ExceptionsListResponseFromJSON(json) {
    return ExceptionsListResponseFromJSONTyped(json, false);
}
exports.ExceptionsListResponseFromJSON = ExceptionsListResponseFromJSON;
function ExceptionsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ExceptionsListFilterFromJSON(json['filters']),
        'exceptions': !runtime_1.exists(json, 'exceptions') ? undefined : (json['exceptions'].map(_1.ExceptionFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ExceptionsListResponseFromJSONTyped = ExceptionsListResponseFromJSONTyped;
function ExceptionsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ExceptionsListFilterToJSON(value.filters),
        'exceptions': value.exceptions === undefined ? undefined : (value.exceptions.map(_1.ExceptionToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ExceptionsListResponseToJSON = ExceptionsListResponseToJSON;
