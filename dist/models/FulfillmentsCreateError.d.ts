/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentCreateResult } from './';
/**
 *
 * @export
 * @interface FulfillmentsCreateError
 */
export interface FulfillmentsCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof FulfillmentsCreateError
     */
    success: FulfillmentsCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof FulfillmentsCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof FulfillmentsCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof FulfillmentsCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<FulfillmentCreateResult>}
     * @memberof FulfillmentsCreateError
     */
    fulfillments?: Array<FulfillmentCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentsCreateErrorSuccessEnum {
    False = "false"
}
export declare function FulfillmentsCreateErrorFromJSON(json: any): FulfillmentsCreateError;
export declare function FulfillmentsCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsCreateError;
export declare function FulfillmentsCreateErrorToJSON(value?: FulfillmentsCreateError | null): any;
