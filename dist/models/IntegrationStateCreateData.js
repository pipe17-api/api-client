"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateCreateDataToJSON = exports.IntegrationStateCreateDataFromJSONTyped = exports.IntegrationStateCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function IntegrationStateCreateDataFromJSON(json) {
    return IntegrationStateCreateDataFromJSONTyped(json, false);
}
exports.IntegrationStateCreateDataFromJSON = IntegrationStateCreateDataFromJSON;
function IntegrationStateCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
    };
}
exports.IntegrationStateCreateDataFromJSONTyped = IntegrationStateCreateDataFromJSONTyped;
function IntegrationStateCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'value': value.value,
    };
}
exports.IntegrationStateCreateDataToJSON = IntegrationStateCreateDataToJSON;
