/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStatus } from './';
/**
 *
 * @export
 * @interface IntegrationBaseAllOf
 */
export interface IntegrationBaseAllOf {
    /**
     * Integration version
     * @type {string}
     * @memberof IntegrationBaseAllOf
     */
    version?: string;
    /**
     * Integration Id
     * @type {string}
     * @memberof IntegrationBaseAllOf
     */
    integrationId?: string;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationBaseAllOf
     */
    errorText?: string;
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationBaseAllOf
     */
    status?: IntegrationStatus;
}
export declare function IntegrationBaseAllOfFromJSON(json: any): IntegrationBaseAllOf;
export declare function IntegrationBaseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationBaseAllOf;
export declare function IntegrationBaseAllOfToJSON(value?: IntegrationBaseAllOf | null): any;
