"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryDeleteResponseToJSON = exports.InventoryDeleteResponseFromJSONTyped = exports.InventoryDeleteResponseFromJSON = exports.InventoryDeleteResponseCodeEnum = exports.InventoryDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryDeleteResponseSuccessEnum;
(function (InventoryDeleteResponseSuccessEnum) {
    InventoryDeleteResponseSuccessEnum["True"] = "true";
})(InventoryDeleteResponseSuccessEnum = exports.InventoryDeleteResponseSuccessEnum || (exports.InventoryDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryDeleteResponseCodeEnum;
(function (InventoryDeleteResponseCodeEnum) {
    InventoryDeleteResponseCodeEnum[InventoryDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryDeleteResponseCodeEnum[InventoryDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryDeleteResponseCodeEnum[InventoryDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryDeleteResponseCodeEnum = exports.InventoryDeleteResponseCodeEnum || (exports.InventoryDeleteResponseCodeEnum = {}));
function InventoryDeleteResponseFromJSON(json) {
    return InventoryDeleteResponseFromJSONTyped(json, false);
}
exports.InventoryDeleteResponseFromJSON = InventoryDeleteResponseFromJSON;
function InventoryDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryDeleteFilterFromJSON(json['filters']),
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryDeleteResponseFromJSONTyped = InventoryDeleteResponseFromJSONTyped;
function InventoryDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.InventoryDeleteFilterToJSON(value.filters),
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryDeleteResponseToJSON = InventoryDeleteResponseToJSON;
