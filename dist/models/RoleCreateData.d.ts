/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Methods } from './';
/**
 *
 * @export
 * @interface RoleCreateData
 */
export interface RoleCreateData {
    /**
     * Global role indicator
     * @type {boolean}
     * @memberof RoleCreateData
     */
    isPublic?: boolean;
    /**
     * Role Name
     * @type {string}
     * @memberof RoleCreateData
     */
    name: string;
    /**
     * Role's description
     * @type {string}
     * @memberof RoleCreateData
     */
    description?: string;
    /**
     *
     * @type {Methods}
     * @memberof RoleCreateData
     */
    methods?: Methods;
}
export declare function RoleCreateDataFromJSON(json: any): RoleCreateData;
export declare function RoleCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleCreateData;
export declare function RoleCreateDataToJSON(value?: RoleCreateData | null): any;
