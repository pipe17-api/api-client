/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface JobFetchError
 */
export interface JobFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof JobFetchError
     */
    success: JobFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof JobFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof JobFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum JobFetchErrorSuccessEnum {
    False = "false"
}
export declare function JobFetchErrorFromJSON(json: any): JobFetchError;
export declare function JobFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobFetchError;
export declare function JobFetchErrorToJSON(value?: JobFetchError | null): any;
