/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingRule } from './';
/**
 *
 * @export
 * @interface ConverterRequestRules
 */
export interface ConverterRequestRules {
    /**
     * Mapping Rules (- required if mappingUuid is not specified, otherwise must be missing)
     * @type {Array<MappingRule>}
     * @memberof ConverterRequestRules
     */
    mappings?: Array<MappingRule>;
    /**
     * Mapping UUID (- required if mapping rules are not specified, otherwise must be missing)
     * @type {string}
     * @memberof ConverterRequestRules
     */
    mappingUuid?: string;
}
export declare function ConverterRequestRulesFromJSON(json: any): ConverterRequestRules;
export declare function ConverterRequestRulesFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConverterRequestRules;
export declare function ConverterRequestRulesToJSON(value?: ConverterRequestRules | null): any;
