"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateAllOfToJSON = exports.IntegrationStateAllOfFromJSONTyped = exports.IntegrationStateAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function IntegrationStateAllOfFromJSON(json) {
    return IntegrationStateAllOfFromJSONTyped(json, false);
}
exports.IntegrationStateAllOfFromJSON = IntegrationStateAllOfFromJSON;
function IntegrationStateAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.IntegrationStateAllOfFromJSONTyped = IntegrationStateAllOfFromJSONTyped;
function IntegrationStateAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
    };
}
exports.IntegrationStateAllOfToJSON = IntegrationStateAllOfToJSON;
