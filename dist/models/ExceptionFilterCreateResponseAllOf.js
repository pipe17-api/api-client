"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterCreateResponseAllOfToJSON = exports.ExceptionFilterCreateResponseAllOfFromJSONTyped = exports.ExceptionFilterCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFilterCreateResponseAllOfFromJSON(json) {
    return ExceptionFilterCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ExceptionFilterCreateResponseAllOfFromJSON = ExceptionFilterCreateResponseAllOfFromJSON;
function ExceptionFilterCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionFilter': !runtime_1.exists(json, 'exceptionFilter') ? undefined : _1.ExceptionFilterFromJSON(json['exceptionFilter']),
    };
}
exports.ExceptionFilterCreateResponseAllOfFromJSONTyped = ExceptionFilterCreateResponseAllOfFromJSONTyped;
function ExceptionFilterCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionFilter': _1.ExceptionFilterToJSON(value.exceptionFilter),
    };
}
exports.ExceptionFilterCreateResponseAllOfToJSON = ExceptionFilterCreateResponseAllOfToJSON;
