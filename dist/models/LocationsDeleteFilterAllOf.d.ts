/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Locations Filter
 * @export
 * @interface LocationsDeleteFilterAllOf
 */
export interface LocationsDeleteFilterAllOf {
    /**
     * Locations of these integrations
     * @type {Array<string>}
     * @memberof LocationsDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function LocationsDeleteFilterAllOfFromJSON(json: any): LocationsDeleteFilterAllOf;
export declare function LocationsDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsDeleteFilterAllOf;
export declare function LocationsDeleteFilterAllOfToJSON(value?: LocationsDeleteFilterAllOf | null): any;
