/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ProductsDeleteErrorAllOf
 */
export interface ProductsDeleteErrorAllOf {
    /**
     *
     * @type {ProductsDeleteFilter}
     * @memberof ProductsDeleteErrorAllOf
     */
    filters?: ProductsDeleteFilter;
}
export declare function ProductsDeleteErrorAllOfFromJSON(json: any): ProductsDeleteErrorAllOf;
export declare function ProductsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsDeleteErrorAllOf;
export declare function ProductsDeleteErrorAllOfToJSON(value?: ProductsDeleteErrorAllOf | null): any;
