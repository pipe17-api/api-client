/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ArrivalsDeleteErrorAllOf
 */
export interface ArrivalsDeleteErrorAllOf {
    /**
     *
     * @type {ArrivalsDeleteFilter}
     * @memberof ArrivalsDeleteErrorAllOf
     */
    filters?: ArrivalsDeleteFilter;
}
export declare function ArrivalsDeleteErrorAllOfFromJSON(json: any): ArrivalsDeleteErrorAllOf;
export declare function ArrivalsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsDeleteErrorAllOf;
export declare function ArrivalsDeleteErrorAllOfToJSON(value?: ArrivalsDeleteErrorAllOf | null): any;
