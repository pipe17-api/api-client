"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationServiceTierToJSON = exports.OrganizationServiceTierFromJSONTyped = exports.OrganizationServiceTierFromJSON = exports.OrganizationServiceTier = void 0;
/**
 * Organization Service Tier
 * @export
 * @enum {string}
 */
var OrganizationServiceTier;
(function (OrganizationServiceTier) {
    OrganizationServiceTier["Free"] = "free";
    OrganizationServiceTier["Basic"] = "basic";
    OrganizationServiceTier["Enterprise"] = "enterprise";
})(OrganizationServiceTier = exports.OrganizationServiceTier || (exports.OrganizationServiceTier = {}));
function OrganizationServiceTierFromJSON(json) {
    return OrganizationServiceTierFromJSONTyped(json, false);
}
exports.OrganizationServiceTierFromJSON = OrganizationServiceTierFromJSON;
function OrganizationServiceTierFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.OrganizationServiceTierFromJSONTyped = OrganizationServiceTierFromJSONTyped;
function OrganizationServiceTierToJSON(value) {
    return value;
}
exports.OrganizationServiceTierToJSON = OrganizationServiceTierToJSON;
