/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserStatus } from './';
/**
 *
 * @export
 * @interface UserAllOf
 */
export interface UserAllOf {
    /**
     * User ID
     * @type {string}
     * @memberof UserAllOf
     */
    userId?: string;
    /**
     *
     * @type {UserStatus}
     * @memberof UserAllOf
     */
    status?: UserStatus;
}
export declare function UserAllOfFromJSON(json: any): UserAllOf;
export declare function UserAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserAllOf;
export declare function UserAllOfToJSON(value?: UserAllOf | null): any;
