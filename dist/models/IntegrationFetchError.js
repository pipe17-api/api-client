"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationFetchErrorToJSON = exports.IntegrationFetchErrorFromJSONTyped = exports.IntegrationFetchErrorFromJSON = exports.IntegrationFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var IntegrationFetchErrorSuccessEnum;
(function (IntegrationFetchErrorSuccessEnum) {
    IntegrationFetchErrorSuccessEnum["False"] = "false";
})(IntegrationFetchErrorSuccessEnum = exports.IntegrationFetchErrorSuccessEnum || (exports.IntegrationFetchErrorSuccessEnum = {}));
function IntegrationFetchErrorFromJSON(json) {
    return IntegrationFetchErrorFromJSONTyped(json, false);
}
exports.IntegrationFetchErrorFromJSON = IntegrationFetchErrorFromJSON;
function IntegrationFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.IntegrationFetchErrorFromJSONTyped = IntegrationFetchErrorFromJSONTyped;
function IntegrationFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.IntegrationFetchErrorToJSON = IntegrationFetchErrorToJSON;
