"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyUpdateRequestToJSON = exports.ApiKeyUpdateRequestFromJSONTyped = exports.ApiKeyUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ApiKeyUpdateRequestFromJSON(json) {
    return ApiKeyUpdateRequestFromJSONTyped(json, false);
}
exports.ApiKeyUpdateRequestFromJSON = ApiKeyUpdateRequestFromJSON;
function ApiKeyUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'methods': !runtime_1.exists(json, 'methods') ? undefined : _1.MethodsUpdateDataFromJSON(json['methods']),
        'allowedIPs': !runtime_1.exists(json, 'allowedIPs') ? undefined : json['allowedIPs'],
    };
}
exports.ApiKeyUpdateRequestFromJSONTyped = ApiKeyUpdateRequestFromJSONTyped;
function ApiKeyUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'description': value.description,
        'methods': _1.MethodsUpdateDataToJSON(value.methods),
        'allowedIPs': value.allowedIPs,
    };
}
exports.ApiKeyUpdateRequestToJSON = ApiKeyUpdateRequestToJSON;
