"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventReplayResponseToJSON = exports.EventReplayResponseFromJSONTyped = exports.EventReplayResponseFromJSON = exports.EventReplayResponseCodeEnum = exports.EventReplayResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var EventReplayResponseSuccessEnum;
(function (EventReplayResponseSuccessEnum) {
    EventReplayResponseSuccessEnum["True"] = "true";
})(EventReplayResponseSuccessEnum = exports.EventReplayResponseSuccessEnum || (exports.EventReplayResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EventReplayResponseCodeEnum;
(function (EventReplayResponseCodeEnum) {
    EventReplayResponseCodeEnum[EventReplayResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EventReplayResponseCodeEnum[EventReplayResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EventReplayResponseCodeEnum[EventReplayResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EventReplayResponseCodeEnum = exports.EventReplayResponseCodeEnum || (exports.EventReplayResponseCodeEnum = {}));
function EventReplayResponseFromJSON(json) {
    return EventReplayResponseFromJSONTyped(json, false);
}
exports.EventReplayResponseFromJSON = EventReplayResponseFromJSON;
function EventReplayResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.EventReplayResponseFromJSONTyped = EventReplayResponseFromJSONTyped;
function EventReplayResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.EventReplayResponseToJSON = EventReplayResponseToJSON;
