/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationConnectionField } from './';
/**
 * 3rd party's credentials
 * @export
 * @interface IntegrationConnection
 */
export interface IntegrationConnection {
    /**
     * correspond to ui fields
     * @type {Array<IntegrationConnectionField>}
     * @memberof IntegrationConnection
     */
    fields?: Array<IntegrationConnectionField> | null;
}
export declare function IntegrationConnectionFromJSON(json: any): IntegrationConnection;
export declare function IntegrationConnectionFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationConnection;
export declare function IntegrationConnectionToJSON(value?: IntegrationConnection | null): any;
