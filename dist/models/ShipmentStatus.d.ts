/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Shipment Status
 * @export
 * @enum {string}
 */
export declare enum ShipmentStatus {
    PendingInventory = "pendingInventory",
    PendingShippingLabel = "pendingShippingLabel",
    ReadyForFulfillment = "readyForFulfillment",
    SentToFulfillment = "sentToFulfillment",
    Fulfilled = "fulfilled",
    PartialFulfillment = "partialFulfillment",
    Canceled = "canceled",
    CanceledRestock = "canceledRestock",
    Failed = "failed"
}
export declare function ShipmentStatusFromJSON(json: any): ShipmentStatus;
export declare function ShipmentStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentStatus;
export declare function ShipmentStatusToJSON(value?: ShipmentStatus | null): any;
