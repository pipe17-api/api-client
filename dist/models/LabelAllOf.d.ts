/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelAllOf
 */
export interface LabelAllOf {
    /**
     * Label ID
     * @type {string}
     * @memberof LabelAllOf
     */
    labelId?: string;
    /**
     * Label status
     * @type {string}
     * @memberof LabelAllOf
     */
    status?: LabelAllOfStatusEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelAllOfStatusEnum {
    New = "new",
    Ready = "ready"
}
export declare function LabelAllOfFromJSON(json: any): LabelAllOf;
export declare function LabelAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelAllOf;
export declare function LabelAllOfToJSON(value?: LabelAllOf | null): any;
