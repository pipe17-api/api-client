"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionsListResponseAllOfToJSON = exports.ExceptionsListResponseAllOfFromJSONTyped = exports.ExceptionsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionsListResponseAllOfFromJSON(json) {
    return ExceptionsListResponseAllOfFromJSONTyped(json, false);
}
exports.ExceptionsListResponseAllOfFromJSON = ExceptionsListResponseAllOfFromJSON;
function ExceptionsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ExceptionsListFilterFromJSON(json['filters']),
        'exceptions': !runtime_1.exists(json, 'exceptions') ? undefined : (json['exceptions'].map(_1.ExceptionFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ExceptionsListResponseAllOfFromJSONTyped = ExceptionsListResponseAllOfFromJSONTyped;
function ExceptionsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ExceptionsListFilterToJSON(value.filters),
        'exceptions': value.exceptions === undefined ? undefined : (value.exceptions.map(_1.ExceptionToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ExceptionsListResponseAllOfToJSON = ExceptionsListResponseAllOfToJSON;
