/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label } from './';
/**
 *
 * @export
 * @interface LabelCreateResponse
 */
export interface LabelCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LabelCreateResponse
     */
    success: LabelCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelCreateResponse
     */
    code: LabelCreateResponseCodeEnum;
    /**
     *
     * @type {Label}
     * @memberof LabelCreateResponse
     */
    label?: Label;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LabelCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LabelCreateResponseFromJSON(json: any): LabelCreateResponse;
export declare function LabelCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelCreateResponse;
export declare function LabelCreateResponseToJSON(value?: LabelCreateResponse | null): any;
