"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsListErrorToJSON = exports.AccountsListErrorFromJSONTyped = exports.AccountsListErrorFromJSON = exports.AccountsListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var AccountsListErrorSuccessEnum;
(function (AccountsListErrorSuccessEnum) {
    AccountsListErrorSuccessEnum["False"] = "false";
})(AccountsListErrorSuccessEnum = exports.AccountsListErrorSuccessEnum || (exports.AccountsListErrorSuccessEnum = {}));
function AccountsListErrorFromJSON(json) {
    return AccountsListErrorFromJSONTyped(json, false);
}
exports.AccountsListErrorFromJSON = AccountsListErrorFromJSON;
function AccountsListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.AccountsListFilterFromJSON(json['filters']),
    };
}
exports.AccountsListErrorFromJSONTyped = AccountsListErrorFromJSONTyped;
function AccountsListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.AccountsListFilterToJSON(value.filters),
    };
}
exports.AccountsListErrorToJSON = AccountsListErrorToJSON;
