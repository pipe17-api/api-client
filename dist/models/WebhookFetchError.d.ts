/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface WebhookFetchError
 */
export interface WebhookFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof WebhookFetchError
     */
    success: WebhookFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhookFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof WebhookFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof WebhookFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhookFetchErrorSuccessEnum {
    False = "false"
}
export declare function WebhookFetchErrorFromJSON(json: any): WebhookFetchError;
export declare function WebhookFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookFetchError;
export declare function WebhookFetchErrorToJSON(value?: WebhookFetchError | null): any;
