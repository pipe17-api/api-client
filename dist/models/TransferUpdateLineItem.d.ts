/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TransferUpdateLineItem
 */
export interface TransferUpdateLineItem {
    /**
     * Item unique Id (within transfer)
     * @type {string}
     * @memberof TransferUpdateLineItem
     */
    uniqueId?: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof TransferUpdateLineItem
     */
    quantity?: number;
    /**
     * Item SKU
     * @type {string}
     * @memberof TransferUpdateLineItem
     */
    sku?: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof TransferUpdateLineItem
     */
    name?: string;
    /**
     * Item UPC
     * @type {string}
     * @memberof TransferUpdateLineItem
     */
    upc?: string;
    /**
     * Shipped quantity
     * @type {number}
     * @memberof TransferUpdateLineItem
     */
    shippedQuantity?: number;
    /**
     * Received quantity
     * @type {number}
     * @memberof TransferUpdateLineItem
     */
    receivedQuantity?: number;
    /**
     *
     * @type {string}
     * @memberof TransferUpdateLineItem
     */
    locationId?: string;
}
export declare function TransferUpdateLineItemFromJSON(json: any): TransferUpdateLineItem;
export declare function TransferUpdateLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferUpdateLineItem;
export declare function TransferUpdateLineItemToJSON(value?: TransferUpdateLineItem | null): any;
