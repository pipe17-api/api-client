/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Organization Service Tier
 * @export
 * @enum {string}
 */
export declare enum OrganizationServiceTier {
    Free = "free",
    Basic = "basic",
    Enterprise = "enterprise"
}
export declare function OrganizationServiceTierFromJSON(json: any): OrganizationServiceTier;
export declare function OrganizationServiceTierFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationServiceTier;
export declare function OrganizationServiceTierToJSON(value?: OrganizationServiceTier | null): any;
