"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingUpdateErrorToJSON = exports.TrackingUpdateErrorFromJSONTyped = exports.TrackingUpdateErrorFromJSON = exports.TrackingUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TrackingUpdateErrorSuccessEnum;
(function (TrackingUpdateErrorSuccessEnum) {
    TrackingUpdateErrorSuccessEnum["False"] = "false";
})(TrackingUpdateErrorSuccessEnum = exports.TrackingUpdateErrorSuccessEnum || (exports.TrackingUpdateErrorSuccessEnum = {}));
function TrackingUpdateErrorFromJSON(json) {
    return TrackingUpdateErrorFromJSONTyped(json, false);
}
exports.TrackingUpdateErrorFromJSON = TrackingUpdateErrorFromJSON;
function TrackingUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'tracking': !runtime_1.exists(json, 'tracking') ? undefined : _1.TrackingUpdateDataFromJSON(json['tracking']),
    };
}
exports.TrackingUpdateErrorFromJSONTyped = TrackingUpdateErrorFromJSONTyped;
function TrackingUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'tracking': _1.TrackingUpdateDataToJSON(value.tracking),
    };
}
exports.TrackingUpdateErrorToJSON = TrackingUpdateErrorToJSON;
