/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Refund, RefundsFilter } from './';
/**
 *
 * @export
 * @interface RefundsListResponseAllOf
 */
export interface RefundsListResponseAllOf {
    /**
     *
     * @type {RefundsFilter}
     * @memberof RefundsListResponseAllOf
     */
    filters?: RefundsFilter;
    /**
     *
     * @type {Array<Refund>}
     * @memberof RefundsListResponseAllOf
     */
    refunds?: Array<Refund>;
    /**
     *
     * @type {Pagination}
     * @memberof RefundsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function RefundsListResponseAllOfFromJSON(json: any): RefundsListResponseAllOf;
export declare function RefundsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsListResponseAllOf;
export declare function RefundsListResponseAllOfToJSON(value?: RefundsListResponseAllOf | null): any;
