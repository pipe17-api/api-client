"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderLineItemToJSON = exports.OrderLineItemFromJSONTyped = exports.OrderLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrderLineItemFromJSON(json) {
    return OrderLineItemFromJSONTyped(json, false);
}
exports.OrderLineItemFromJSON = OrderLineItemFromJSON;
function OrderLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': json['uniqueId'],
        'sku': json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'quantity': json['quantity'],
        'itemPrice': !runtime_1.exists(json, 'itemPrice') ? undefined : json['itemPrice'],
        'itemDiscount': !runtime_1.exists(json, 'itemDiscount') ? undefined : json['itemDiscount'],
        'itemTax': !runtime_1.exists(json, 'itemTax') ? undefined : json['itemTax'],
        'requiresShipping': json['requiresShipping'],
        'taxable': !runtime_1.exists(json, 'taxable') ? undefined : json['taxable'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
    };
}
exports.OrderLineItemFromJSONTyped = OrderLineItemFromJSONTyped;
function OrderLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'sku': value.sku,
        'name': value.name,
        'quantity': value.quantity,
        'itemPrice': value.itemPrice,
        'itemDiscount': value.itemDiscount,
        'itemTax': value.itemTax,
        'requiresShipping': value.requiresShipping,
        'taxable': value.taxable,
        'locationId': value.locationId,
    };
}
exports.OrderLineItemToJSON = OrderLineItemToJSON;
