"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsCreateErrorToJSON = exports.ReceiptsCreateErrorFromJSONTyped = exports.ReceiptsCreateErrorFromJSON = exports.ReceiptsCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReceiptsCreateErrorSuccessEnum;
(function (ReceiptsCreateErrorSuccessEnum) {
    ReceiptsCreateErrorSuccessEnum["False"] = "false";
})(ReceiptsCreateErrorSuccessEnum = exports.ReceiptsCreateErrorSuccessEnum || (exports.ReceiptsCreateErrorSuccessEnum = {}));
function ReceiptsCreateErrorFromJSON(json) {
    return ReceiptsCreateErrorFromJSONTyped(json, false);
}
exports.ReceiptsCreateErrorFromJSON = ReceiptsCreateErrorFromJSON;
function ReceiptsCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'receipts': !runtime_1.exists(json, 'receipts') ? undefined : (json['receipts'].map(_1.ReceiptCreateResultFromJSON)),
    };
}
exports.ReceiptsCreateErrorFromJSONTyped = ReceiptsCreateErrorFromJSONTyped;
function ReceiptsCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'receipts': value.receipts === undefined ? undefined : (value.receipts.map(_1.ReceiptCreateResultToJSON)),
    };
}
exports.ReceiptsCreateErrorToJSON = ReceiptsCreateErrorToJSON;
