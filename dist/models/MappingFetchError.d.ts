/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MappingFetchError
 */
export interface MappingFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof MappingFetchError
     */
    success: MappingFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof MappingFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof MappingFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingFetchErrorSuccessEnum {
    False = "false"
}
export declare function MappingFetchErrorFromJSON(json: any): MappingFetchError;
export declare function MappingFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingFetchError;
export declare function MappingFetchErrorToJSON(value?: MappingFetchError | null): any;
