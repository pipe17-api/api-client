/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus, RefundType } from './';
/**
 *
 * @export
 * @interface RefundCreateRequest
 */
export interface RefundCreateRequest {
    /**
     * Return ID
     * @type {string}
     * @memberof RefundCreateRequest
     */
    returnId: string;
    /**
     * Refund ID on external system
     * @type {string}
     * @memberof RefundCreateRequest
     */
    extRefundId?: string;
    /**
     * Order ID on external system
     * @type {string}
     * @memberof RefundCreateRequest
     */
    extOrderId?: string;
    /**
     *
     * @type {RefundType}
     * @memberof RefundCreateRequest
     */
    type: RefundType;
    /**
     *
     * @type {RefundStatus}
     * @memberof RefundCreateRequest
     */
    status?: RefundStatus;
    /**
     * Refund currency
     * @type {string}
     * @memberof RefundCreateRequest
     */
    currency?: string;
    /**
     * Refund amount
     * @type {number}
     * @memberof RefundCreateRequest
     */
    amount?: number;
    /**
     * Credit issued to customer
     * @type {number}
     * @memberof RefundCreateRequest
     */
    creditIssued?: number;
    /**
     * Credit spent by customer
     * @type {number}
     * @memberof RefundCreateRequest
     */
    creditSpent?: number;
    /**
     * Merchant invoice amount
     * @type {number}
     * @memberof RefundCreateRequest
     */
    merchantInvoiceAmount?: number;
    /**
     * Customer invoice amount
     * @type {number}
     * @memberof RefundCreateRequest
     */
    customerInvoiceAmount?: number;
}
export declare function RefundCreateRequestFromJSON(json: any): RefundCreateRequest;
export declare function RefundCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundCreateRequest;
export declare function RefundCreateRequestToJSON(value?: RefundCreateRequest | null): any;
