"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptItemToJSON = exports.ReceiptItemFromJSONTyped = exports.ReceiptItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function ReceiptItemFromJSON(json) {
    return ReceiptItemFromJSONTyped(json, false);
}
exports.ReceiptItemFromJSON = ReceiptItemFromJSON;
function ReceiptItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': json['sku'],
        'quantity': json['quantity'],
        'uniqueId': !runtime_1.exists(json, 'uniqueId') ? undefined : json['uniqueId'],
    };
}
exports.ReceiptItemFromJSONTyped = ReceiptItemFromJSONTyped;
function ReceiptItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'quantity': value.quantity,
        'uniqueId': value.uniqueId,
    };
}
exports.ReceiptItemToJSON = ReceiptItemToJSON;
