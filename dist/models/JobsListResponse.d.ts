/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job, JobsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface JobsListResponse
 */
export interface JobsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof JobsListResponse
     */
    success: JobsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobsListResponse
     */
    code: JobsListResponseCodeEnum;
    /**
     *
     * @type {JobsListFilter}
     * @memberof JobsListResponse
     */
    filters?: JobsListFilter;
    /**
     *
     * @type {Array<Job>}
     * @memberof JobsListResponse
     */
    jobs?: Array<Job>;
    /**
     *
     * @type {Pagination}
     * @memberof JobsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum JobsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum JobsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function JobsListResponseFromJSON(json: any): JobsListResponse;
export declare function JobsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsListResponse;
export declare function JobsListResponseToJSON(value?: JobsListResponse | null): any;
