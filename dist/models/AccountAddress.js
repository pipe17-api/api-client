"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountAddressToJSON = exports.AccountAddressFromJSONTyped = exports.AccountAddressFromJSON = void 0;
const runtime_1 = require("../runtime");
function AccountAddressFromJSON(json) {
    return AccountAddressFromJSONTyped(json, false);
}
exports.AccountAddressFromJSON = AccountAddressFromJSON;
function AccountAddressFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'address1': !runtime_1.exists(json, 'address1') ? undefined : json['address1'],
        'address2': !runtime_1.exists(json, 'address2') ? undefined : json['address2'],
        'city': !runtime_1.exists(json, 'city') ? undefined : json['city'],
        'stateOrProvince': json['stateOrProvince'],
        'zipCodeOrPostalCode': json['zipCodeOrPostalCode'],
        'country': json['country'],
    };
}
exports.AccountAddressFromJSONTyped = AccountAddressFromJSONTyped;
function AccountAddressToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'address1': value.address1,
        'address2': value.address2,
        'city': value.city,
        'stateOrProvince': value.stateOrProvince,
        'zipCodeOrPostalCode': value.zipCodeOrPostalCode,
        'country': value.country,
    };
}
exports.AccountAddressToJSON = AccountAddressToJSON;
