/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountUpdateData } from './';
/**
 *
 * @export
 * @interface AccountUpdateErrorAllOf
 */
export interface AccountUpdateErrorAllOf {
    /**
     *
     * @type {AccountUpdateData}
     * @memberof AccountUpdateErrorAllOf
     */
    account?: AccountUpdateData;
}
export declare function AccountUpdateErrorAllOfFromJSON(json: any): AccountUpdateErrorAllOf;
export declare function AccountUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountUpdateErrorAllOf;
export declare function AccountUpdateErrorAllOfToJSON(value?: AccountUpdateErrorAllOf | null): any;
