/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ReturnsDeleteError
 */
export interface ReturnsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReturnsDeleteError
     */
    success: ReturnsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReturnsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReturnsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ReturnsDeleteFilter}
     * @memberof ReturnsDeleteError
     */
    filters?: ReturnsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function ReturnsDeleteErrorFromJSON(json: any): ReturnsDeleteError;
export declare function ReturnsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsDeleteError;
export declare function ReturnsDeleteErrorToJSON(value?: ReturnsDeleteError | null): any;
