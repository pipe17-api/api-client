/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductCreateResult } from './';
/**
 *
 * @export
 * @interface ProductsCreateResponse
 */
export interface ProductsCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ProductsCreateResponse
     */
    success: ProductsCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductsCreateResponse
     */
    code: ProductsCreateResponseCodeEnum;
    /**
     *
     * @type {Array<ProductCreateResult>}
     * @memberof ProductsCreateResponse
     */
    products?: Array<ProductCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductsCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ProductsCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ProductsCreateResponseFromJSON(json: any): ProductsCreateResponse;
export declare function ProductsCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsCreateResponse;
export declare function ProductsCreateResponseToJSON(value?: ProductsCreateResponse | null): any;
