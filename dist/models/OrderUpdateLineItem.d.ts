/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderUpdateLineItem
 */
export interface OrderUpdateLineItem {
    /**
     * Item unique Id (within an order)
     * @type {string}
     * @memberof OrderUpdateLineItem
     */
    uniqueId?: string;
    /**
     * Item SKU
     * @type {string}
     * @memberof OrderUpdateLineItem
     */
    sku?: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof OrderUpdateLineItem
     */
    name?: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof OrderUpdateLineItem
     */
    quantity?: number;
    /**
     * Item Price
     * @type {number}
     * @memberof OrderUpdateLineItem
     */
    itemPrice?: number;
    /**
     * Item Price
     * @type {number}
     * @memberof OrderUpdateLineItem
     */
    itemDiscount?: number;
    /**
     *
     * @type {number}
     * @memberof OrderUpdateLineItem
     */
    itemTax?: number;
    /**
     *
     * @type {boolean}
     * @memberof OrderUpdateLineItem
     */
    requiresShipping?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof OrderUpdateLineItem
     */
    taxable?: boolean;
    /**
     *
     * @type {string}
     * @memberof OrderUpdateLineItem
     */
    locationId?: string;
}
export declare function OrderUpdateLineItemFromJSON(json: any): OrderUpdateLineItem;
export declare function OrderUpdateLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderUpdateLineItem;
export declare function OrderUpdateLineItemToJSON(value?: OrderUpdateLineItem | null): any;
