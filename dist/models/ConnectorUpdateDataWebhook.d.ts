/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 * Connector Webhook
 * @export
 * @interface ConnectorUpdateDataWebhook
 */
export interface ConnectorUpdateDataWebhook {
    /**
     * Webhook URL
     * @type {string}
     * @memberof ConnectorUpdateDataWebhook
     */
    url?: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof ConnectorUpdateDataWebhook
     */
    apikey?: string;
    /**
     * Webhook topics
     * @type {Array<WebhookTopics>}
     * @memberof ConnectorUpdateDataWebhook
     */
    topics?: Array<WebhookTopics>;
}
export declare function ConnectorUpdateDataWebhookFromJSON(json: any): ConnectorUpdateDataWebhook;
export declare function ConnectorUpdateDataWebhookFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorUpdateDataWebhook;
export declare function ConnectorUpdateDataWebhookToJSON(value?: ConnectorUpdateDataWebhook | null): any;
