"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationCreateDataToJSON = exports.OrganizationCreateDataFromJSONTyped = exports.OrganizationCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationCreateDataFromJSON(json) {
    return OrganizationCreateDataFromJSONTyped(json, false);
}
exports.OrganizationCreateDataFromJSON = OrganizationCreateDataFromJSON;
function OrganizationCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'tier': _1.OrganizationServiceTierFromJSON(json['tier']),
        'email': json['email'],
        'phone': json['phone'],
        'timeZone': json['timeZone'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'address': _1.OrganizationAddressFromJSON(json['address']),
    };
}
exports.OrganizationCreateDataFromJSONTyped = OrganizationCreateDataFromJSONTyped;
function OrganizationCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'tier': _1.OrganizationServiceTierToJSON(value.tier),
        'email': value.email,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'logoUrl': value.logoUrl,
        'address': _1.OrganizationAddressToJSON(value.address),
    };
}
exports.OrganizationCreateDataToJSON = OrganizationCreateDataToJSON;
