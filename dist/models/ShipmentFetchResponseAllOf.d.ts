/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Shipment } from './';
/**
 *
 * @export
 * @interface ShipmentFetchResponseAllOf
 */
export interface ShipmentFetchResponseAllOf {
    /**
     *
     * @type {Shipment}
     * @memberof ShipmentFetchResponseAllOf
     */
    shipment?: Shipment;
}
export declare function ShipmentFetchResponseAllOfFromJSON(json: any): ShipmentFetchResponseAllOf;
export declare function ShipmentFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentFetchResponseAllOf;
export declare function ShipmentFetchResponseAllOfToJSON(value?: ShipmentFetchResponseAllOf | null): any;
