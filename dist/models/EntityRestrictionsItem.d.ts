/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityRestrictionsItem
 */
export interface EntityRestrictionsItem {
    /**
     *
     * @type {string}
     * @memberof EntityRestrictionsItem
     */
    path: EntityRestrictionsItemPathEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityRestrictionsItemPathEnum {
    Restricted = "restricted",
    Since = "since",
    Until = "until",
    Content = "content"
}
export declare function EntityRestrictionsItemFromJSON(json: any): EntityRestrictionsItem;
export declare function EntityRestrictionsItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityRestrictionsItem;
export declare function EntityRestrictionsItemToJSON(value?: EntityRestrictionsItem | null): any;
