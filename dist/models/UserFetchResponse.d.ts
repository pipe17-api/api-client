/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { User } from './';
/**
 *
 * @export
 * @interface UserFetchResponse
 */
export interface UserFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof UserFetchResponse
     */
    success: UserFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UserFetchResponse
     */
    code: UserFetchResponseCodeEnum;
    /**
     *
     * @type {User}
     * @memberof UserFetchResponse
     */
    user?: User;
}
/**
* @export
* @enum {string}
*/
export declare enum UserFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum UserFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function UserFetchResponseFromJSON(json: any): UserFetchResponse;
export declare function UserFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserFetchResponse;
export declare function UserFetchResponseToJSON(value?: UserFetchResponse | null): any;
