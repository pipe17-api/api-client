/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingFetchError
 */
export interface OrderRoutingFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrderRoutingFetchError
     */
    success: OrderRoutingFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrderRoutingFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrderRoutingFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingFetchErrorSuccessEnum {
    False = "false"
}
export declare function OrderRoutingFetchErrorFromJSON(json: any): OrderRoutingFetchError;
export declare function OrderRoutingFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingFetchError;
export declare function OrderRoutingFetchErrorToJSON(value?: OrderRoutingFetchError | null): any;
