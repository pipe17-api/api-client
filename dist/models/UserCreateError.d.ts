/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserCreateData } from './';
/**
 *
 * @export
 * @interface UserCreateError
 */
export interface UserCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof UserCreateError
     */
    success: UserCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UserCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof UserCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof UserCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {UserCreateData}
     * @memberof UserCreateError
     */
    user?: UserCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum UserCreateErrorSuccessEnum {
    False = "false"
}
export declare function UserCreateErrorFromJSON(json: any): UserCreateError;
export declare function UserCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateError;
export declare function UserCreateErrorToJSON(value?: UserCreateError | null): any;
