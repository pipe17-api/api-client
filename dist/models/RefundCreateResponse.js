"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundCreateResponseToJSON = exports.RefundCreateResponseFromJSONTyped = exports.RefundCreateResponseFromJSON = exports.RefundCreateResponseCodeEnum = exports.RefundCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RefundCreateResponseSuccessEnum;
(function (RefundCreateResponseSuccessEnum) {
    RefundCreateResponseSuccessEnum["True"] = "true";
})(RefundCreateResponseSuccessEnum = exports.RefundCreateResponseSuccessEnum || (exports.RefundCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RefundCreateResponseCodeEnum;
(function (RefundCreateResponseCodeEnum) {
    RefundCreateResponseCodeEnum[RefundCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RefundCreateResponseCodeEnum[RefundCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RefundCreateResponseCodeEnum[RefundCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RefundCreateResponseCodeEnum = exports.RefundCreateResponseCodeEnum || (exports.RefundCreateResponseCodeEnum = {}));
function RefundCreateResponseFromJSON(json) {
    return RefundCreateResponseFromJSONTyped(json, false);
}
exports.RefundCreateResponseFromJSON = RefundCreateResponseFromJSON;
function RefundCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'refund': !runtime_1.exists(json, 'refund') ? undefined : _1.RefundFromJSON(json['refund']),
    };
}
exports.RefundCreateResponseFromJSONTyped = RefundCreateResponseFromJSONTyped;
function RefundCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'refund': _1.RefundToJSON(value.refund),
    };
}
exports.RefundCreateResponseToJSON = RefundCreateResponseToJSON;
