/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryFetchError
 */
export interface InventoryFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryFetchError
     */
    success: InventoryFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryFetchErrorSuccessEnum {
    False = "false"
}
export declare function InventoryFetchErrorFromJSON(json: any): InventoryFetchError;
export declare function InventoryFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryFetchError;
export declare function InventoryFetchErrorToJSON(value?: InventoryFetchError | null): any;
