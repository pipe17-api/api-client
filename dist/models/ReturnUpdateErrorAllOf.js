"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnUpdateErrorAllOfToJSON = exports.ReturnUpdateErrorAllOfFromJSONTyped = exports.ReturnUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnUpdateErrorAllOfFromJSON(json) {
    return ReturnUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ReturnUpdateErrorAllOfFromJSON = ReturnUpdateErrorAllOfFromJSON;
function ReturnUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        '_return': !runtime_1.exists(json, 'return') ? undefined : _1.ReturnUpdateDataFromJSON(json['return']),
    };
}
exports.ReturnUpdateErrorAllOfFromJSONTyped = ReturnUpdateErrorAllOfFromJSONTyped;
function ReturnUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'return': _1.ReturnUpdateDataToJSON(value._return),
    };
}
exports.ReturnUpdateErrorAllOfToJSON = ReturnUpdateErrorAllOfToJSON;
