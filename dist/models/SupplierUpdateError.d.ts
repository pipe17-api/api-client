/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierUpdateData } from './';
/**
 *
 * @export
 * @interface SupplierUpdateError
 */
export interface SupplierUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof SupplierUpdateError
     */
    success: SupplierUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SupplierUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof SupplierUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof SupplierUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {SupplierUpdateData}
     * @memberof SupplierUpdateError
     */
    supplier?: SupplierUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum SupplierUpdateErrorSuccessEnum {
    False = "false"
}
export declare function SupplierUpdateErrorFromJSON(json: any): SupplierUpdateError;
export declare function SupplierUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierUpdateError;
export declare function SupplierUpdateErrorToJSON(value?: SupplierUpdateError | null): any;
