"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsFilterAllOfToJSON = exports.ArrivalsFilterAllOfFromJSONTyped = exports.ArrivalsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalsFilterAllOfFromJSON(json) {
    return ArrivalsFilterAllOfFromJSONTyped(json, false);
}
exports.ArrivalsFilterAllOfFromJSON = ArrivalsFilterAllOfFromJSON;
function ArrivalsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrivalId': !runtime_1.exists(json, 'arrivalId') ? undefined : json['arrivalId'],
        'transferId': !runtime_1.exists(json, 'transferId') ? undefined : json['transferId'],
        'purchaseId': !runtime_1.exists(json, 'purchaseId') ? undefined : json['purchaseId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ArrivalStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.ArrivalsFilterAllOfFromJSONTyped = ArrivalsFilterAllOfFromJSONTyped;
function ArrivalsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrivalId': value.arrivalId,
        'transferId': value.transferId,
        'purchaseId': value.purchaseId,
        'extOrderId': value.extOrderId,
        'fulfillmentId': value.fulfillmentId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ArrivalStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.ArrivalsFilterAllOfToJSON = ArrivalsFilterAllOfToJSON;
