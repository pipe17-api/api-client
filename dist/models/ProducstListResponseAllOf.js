"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProducstListResponseAllOfToJSON = exports.ProducstListResponseAllOfFromJSONTyped = exports.ProducstListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProducstListResponseAllOfFromJSON(json) {
    return ProducstListResponseAllOfFromJSONTyped(json, false);
}
exports.ProducstListResponseAllOfFromJSON = ProducstListResponseAllOfFromJSON;
function ProducstListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ProductsListFilterFromJSON(json['filters']),
        'products': (json['products'].map(_1.ProductFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ProducstListResponseAllOfFromJSONTyped = ProducstListResponseAllOfFromJSONTyped;
function ProducstListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ProductsListFilterToJSON(value.filters),
        'products': (value.products.map(_1.ProductToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ProducstListResponseAllOfToJSON = ProducstListResponseAllOfToJSON;
