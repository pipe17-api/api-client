"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersListErrorAllOfToJSON = exports.UsersListErrorAllOfFromJSONTyped = exports.UsersListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UsersListErrorAllOfFromJSON(json) {
    return UsersListErrorAllOfFromJSONTyped(json, false);
}
exports.UsersListErrorAllOfFromJSON = UsersListErrorAllOfFromJSON;
function UsersListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.UsersListFilterFromJSON(json['filters']),
    };
}
exports.UsersListErrorAllOfFromJSONTyped = UsersListErrorAllOfFromJSONTyped;
function UsersListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.UsersListFilterToJSON(value.filters),
    };
}
exports.UsersListErrorAllOfToJSON = UsersListErrorAllOfToJSON;
