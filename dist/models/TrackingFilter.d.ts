/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingStatus } from './';
/**
 * Tracking Filter
 * @export
 * @interface TrackingFilter
 */
export interface TrackingFilter {
    /**
     *
     * @type {Array<string>}
     * @memberof TrackingFilter
     */
    trackingId?: Array<string>;
    /**
     *
     * @type {Array<string>}
     * @memberof TrackingFilter
     */
    trackingNumber?: Array<string>;
    /**
     *
     * @type {Array<string>}
     * @memberof TrackingFilter
     */
    fulfillmentId?: Array<string>;
    /**
     *
     * @type {Array<string>}
     * @memberof TrackingFilter
     */
    shippingCarrier?: Array<string>;
    /**
     *
     * @type {Array<TrackingStatus>}
     * @memberof TrackingFilter
     */
    status?: Array<TrackingStatus>;
    /**
     * Tracking created after this date-time
     * @type {Date}
     * @memberof TrackingFilter
     */
    since?: Date;
    /**
     * Tracking created before this date-time
     * @type {Date}
     * @memberof TrackingFilter
     */
    until?: Date;
    /**
     * Tracking updated after this date-time
     * @type {Date}
     * @memberof TrackingFilter
     */
    updatedSince?: Date;
    /**
     * Tracking updated before this date-time
     * @type {Date}
     * @memberof TrackingFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many tracking records
     * @type {number}
     * @memberof TrackingFilter
     */
    skip?: number;
    /**
     * Return at most this many tracking records
     * @type {number}
     * @memberof TrackingFilter
     */
    count?: number;
    /**
     * List sort order
     * @type {string}
     * @memberof TrackingFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof TrackingFilter
     */
    keys?: string;
}
export declare function TrackingFilterFromJSON(json: any): TrackingFilter;
export declare function TrackingFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingFilter;
export declare function TrackingFilterToJSON(value?: TrackingFilter | null): any;
