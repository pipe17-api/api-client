/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKeyValue } from './';
/**
 *
 * @export
 * @interface ApiKeyValueFetchResponse
 */
export interface ApiKeyValueFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ApiKeyValueFetchResponse
     */
    success: ApiKeyValueFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyValueFetchResponse
     */
    code: ApiKeyValueFetchResponseCodeEnum;
    /**
     *
     * @type {ApiKeyValue}
     * @memberof ApiKeyValueFetchResponse
     */
    apikey?: ApiKeyValue;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyValueFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ApiKeyValueFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ApiKeyValueFetchResponseFromJSON(json: any): ApiKeyValueFetchResponse;
export declare function ApiKeyValueFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyValueFetchResponse;
export declare function ApiKeyValueFetchResponseToJSON(value?: ApiKeyValueFetchResponse | null): any;
