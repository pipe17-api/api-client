"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersCreateResponseToJSON = exports.TransfersCreateResponseFromJSONTyped = exports.TransfersCreateResponseFromJSON = exports.TransfersCreateResponseCodeEnum = exports.TransfersCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransfersCreateResponseSuccessEnum;
(function (TransfersCreateResponseSuccessEnum) {
    TransfersCreateResponseSuccessEnum["True"] = "true";
})(TransfersCreateResponseSuccessEnum = exports.TransfersCreateResponseSuccessEnum || (exports.TransfersCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TransfersCreateResponseCodeEnum;
(function (TransfersCreateResponseCodeEnum) {
    TransfersCreateResponseCodeEnum[TransfersCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TransfersCreateResponseCodeEnum[TransfersCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TransfersCreateResponseCodeEnum[TransfersCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TransfersCreateResponseCodeEnum = exports.TransfersCreateResponseCodeEnum || (exports.TransfersCreateResponseCodeEnum = {}));
function TransfersCreateResponseFromJSON(json) {
    return TransfersCreateResponseFromJSONTyped(json, false);
}
exports.TransfersCreateResponseFromJSON = TransfersCreateResponseFromJSON;
function TransfersCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'transfers': !runtime_1.exists(json, 'transfers') ? undefined : (json['transfers'].map(_1.TransferCreateResultFromJSON)),
    };
}
exports.TransfersCreateResponseFromJSONTyped = TransfersCreateResponseFromJSONTyped;
function TransfersCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'transfers': value.transfers === undefined ? undefined : (value.transfers.map(_1.TransferCreateResultToJSON)),
    };
}
exports.TransfersCreateResponseToJSON = TransfersCreateResponseToJSON;
