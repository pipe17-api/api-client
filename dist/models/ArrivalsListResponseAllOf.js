"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsListResponseAllOfToJSON = exports.ArrivalsListResponseAllOfFromJSONTyped = exports.ArrivalsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalsListResponseAllOfFromJSON(json) {
    return ArrivalsListResponseAllOfFromJSONTyped(json, false);
}
exports.ArrivalsListResponseAllOfFromJSON = ArrivalsListResponseAllOfFromJSON;
function ArrivalsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ArrivalsListFilterFromJSON(json['filters']),
        'arrivals': (json['arrivals'].map(_1.ArrivalFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ArrivalsListResponseAllOfFromJSONTyped = ArrivalsListResponseAllOfFromJSONTyped;
function ArrivalsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ArrivalsListFilterToJSON(value.filters),
        'arrivals': (value.arrivals.map(_1.ArrivalToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ArrivalsListResponseAllOfToJSON = ArrivalsListResponseAllOfToJSON;
