"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingToJSON = exports.MappingFromJSONTyped = exports.MappingFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingFromJSON(json) {
    return MappingFromJSONTyped(json, false);
}
exports.MappingFromJSON = MappingFromJSON;
function MappingFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mappingId': !runtime_1.exists(json, 'mappingId') ? undefined : json['mappingId'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'isPublic': json['isPublic'],
        'description': json['description'],
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingRuleFromJSON)),
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.MappingFromJSONTyped = MappingFromJSONTyped;
function MappingToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mappingId': value.mappingId,
        'connectorId': value.connectorId,
        'isPublic': value.isPublic,
        'description': value.description,
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingRuleToJSON)),
        'uuid': value.uuid,
        'originId': value.originId,
    };
}
exports.MappingToJSON = MappingToJSON;
