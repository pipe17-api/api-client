/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Product, ProductsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ProductsDeleteResponseAllOf
 */
export interface ProductsDeleteResponseAllOf {
    /**
     *
     * @type {ProductsDeleteFilter}
     * @memberof ProductsDeleteResponseAllOf
     */
    filters?: ProductsDeleteFilter;
    /**
     *
     * @type {Array<Product>}
     * @memberof ProductsDeleteResponseAllOf
     */
    products?: Array<Product>;
    /**
     * Number of deleted products
     * @type {number}
     * @memberof ProductsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ProductsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ProductsDeleteResponseAllOfFromJSON(json: any): ProductsDeleteResponseAllOf;
export declare function ProductsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsDeleteResponseAllOf;
export declare function ProductsDeleteResponseAllOfToJSON(value?: ProductsDeleteResponseAllOf | null): any;
