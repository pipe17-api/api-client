/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Exceptions Filter
 * @export
 * @interface ExceptionsFilterAllOf
 */
export interface ExceptionsFilterAllOf {
    /**
     * Exceptions by status-list
     * @type {Array<string>}
     * @memberof ExceptionsFilterAllOf
     */
    status?: Array<string>;
    /**
     * Exceptions by exceptionId-list
     * @type {Array<string>}
     * @memberof ExceptionsFilterAllOf
     */
    exceptionId?: Array<string>;
    /**
     * Exceptions by exceptionType-list
     * @type {Array<string>}
     * @memberof ExceptionsFilterAllOf
     */
    exceptionType?: Array<string>;
    /**
     * Exceptions related to this list of entities
     * @type {Array<string>}
     * @memberof ExceptionsFilterAllOf
     */
    entityId?: Array<string>;
    /**
     * Exceptions related to list of entity types
     * @type {Array<string>}
     * @memberof ExceptionsFilterAllOf
     */
    entityType?: Array<string>;
}
export declare function ExceptionsFilterAllOfFromJSON(json: any): ExceptionsFilterAllOf;
export declare function ExceptionsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsFilterAllOf;
export declare function ExceptionsFilterAllOfToJSON(value?: ExceptionsFilterAllOf | null): any;
