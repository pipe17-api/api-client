"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookUpdateErrorToJSON = exports.WebhookUpdateErrorFromJSONTyped = exports.WebhookUpdateErrorFromJSON = exports.WebhookUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var WebhookUpdateErrorSuccessEnum;
(function (WebhookUpdateErrorSuccessEnum) {
    WebhookUpdateErrorSuccessEnum["False"] = "false";
})(WebhookUpdateErrorSuccessEnum = exports.WebhookUpdateErrorSuccessEnum || (exports.WebhookUpdateErrorSuccessEnum = {}));
function WebhookUpdateErrorFromJSON(json) {
    return WebhookUpdateErrorFromJSONTyped(json, false);
}
exports.WebhookUpdateErrorFromJSON = WebhookUpdateErrorFromJSON;
function WebhookUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.WebhookUpdateDataFromJSON(json['webhook']),
    };
}
exports.WebhookUpdateErrorFromJSONTyped = WebhookUpdateErrorFromJSONTyped;
function WebhookUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'webhook': _1.WebhookUpdateDataToJSON(value.webhook),
    };
}
exports.WebhookUpdateErrorToJSON = WebhookUpdateErrorToJSON;
