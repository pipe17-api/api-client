"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertResponseToJSON = exports.ConvertResponseFromJSONTyped = exports.ConvertResponseFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConvertResponseFromJSON(json) {
    return ConvertResponseFromJSONTyped(json, false);
}
exports.ConvertResponseFromJSON = ConvertResponseFromJSON;
function ConvertResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'result': !runtime_1.exists(json, 'result') ? undefined : _1.ConvertResponseResultFromJSON(json['result']),
    };
}
exports.ConvertResponseFromJSONTyped = ConvertResponseFromJSONTyped;
function ConvertResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'message': value.message,
        'result': _1.ConvertResponseResultToJSON(value.result),
    };
}
exports.ConvertResponseToJSON = ConvertResponseToJSON;
