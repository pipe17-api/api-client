/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierAddress, SupplierContact } from './';
/**
 *
 * @export
 * @interface SupplierCreateData
 */
export interface SupplierCreateData {
    /**
     * Supplier Name
     * @type {string}
     * @memberof SupplierCreateData
     */
    name: string;
    /**
     *
     * @type {SupplierAddress}
     * @memberof SupplierCreateData
     */
    address: SupplierAddress;
    /**
     *
     * @type {Array<SupplierContact>}
     * @memberof SupplierCreateData
     */
    contacts?: Array<SupplierContact>;
}
export declare function SupplierCreateDataFromJSON(json: any): SupplierCreateData;
export declare function SupplierCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierCreateData;
export declare function SupplierCreateDataToJSON(value?: SupplierCreateData | null): any;
