/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Product, ProductsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ProductsDeleteResponse
 */
export interface ProductsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ProductsDeleteResponse
     */
    success: ProductsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductsDeleteResponse
     */
    code: ProductsDeleteResponseCodeEnum;
    /**
     *
     * @type {ProductsDeleteFilter}
     * @memberof ProductsDeleteResponse
     */
    filters?: ProductsDeleteFilter;
    /**
     *
     * @type {Array<Product>}
     * @memberof ProductsDeleteResponse
     */
    products?: Array<Product>;
    /**
     * Number of deleted products
     * @type {number}
     * @memberof ProductsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ProductsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ProductsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ProductsDeleteResponseFromJSON(json: any): ProductsDeleteResponse;
export declare function ProductsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsDeleteResponse;
export declare function ProductsDeleteResponseToJSON(value?: ProductsDeleteResponse | null): any;
