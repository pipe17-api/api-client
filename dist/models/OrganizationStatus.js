"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationStatusToJSON = exports.OrganizationStatusFromJSONTyped = exports.OrganizationStatusFromJSON = exports.OrganizationStatus = void 0;
/**
 * Organization Account Status
 * @export
 * @enum {string}
 */
var OrganizationStatus;
(function (OrganizationStatus) {
    OrganizationStatus["Pending"] = "pending";
    OrganizationStatus["Active"] = "active";
    OrganizationStatus["Disabled"] = "disabled";
})(OrganizationStatus = exports.OrganizationStatus || (exports.OrganizationStatus = {}));
function OrganizationStatusFromJSON(json) {
    return OrganizationStatusFromJSONTyped(json, false);
}
exports.OrganizationStatusFromJSON = OrganizationStatusFromJSON;
function OrganizationStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.OrganizationStatusFromJSONTyped = OrganizationStatusFromJSONTyped;
function OrganizationStatusToJSON(value) {
    return value;
}
exports.OrganizationStatusToJSON = OrganizationStatusToJSON;
