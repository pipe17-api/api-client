"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationUpdateDataToJSON = exports.OrganizationUpdateDataFromJSONTyped = exports.OrganizationUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationUpdateDataFromJSON(json) {
    return OrganizationUpdateDataFromJSONTyped(json, false);
}
exports.OrganizationUpdateDataFromJSON = OrganizationUpdateDataFromJSON;
function OrganizationUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'tier': !runtime_1.exists(json, 'tier') ? undefined : _1.OrganizationServiceTierFromJSON(json['tier']),
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'timeZone': !runtime_1.exists(json, 'timeZone') ? undefined : json['timeZone'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'address': !runtime_1.exists(json, 'address') ? undefined : _1.OrganizationUpdateAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.OrganizationStatusFromJSON(json['status']),
    };
}
exports.OrganizationUpdateDataFromJSONTyped = OrganizationUpdateDataFromJSONTyped;
function OrganizationUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'tier': _1.OrganizationServiceTierToJSON(value.tier),
        'email': value.email,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'logoUrl': value.logoUrl,
        'address': _1.OrganizationUpdateAddressToJSON(value.address),
        'status': _1.OrganizationStatusToJSON(value.status),
    };
}
exports.OrganizationUpdateDataToJSON = OrganizationUpdateDataToJSON;
