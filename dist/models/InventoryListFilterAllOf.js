"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryListFilterAllOfToJSON = exports.InventoryListFilterAllOfFromJSONTyped = exports.InventoryListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryListFilterAllOfFromJSON(json) {
    return InventoryListFilterAllOfFromJSONTyped(json, false);
}
exports.InventoryListFilterAllOfFromJSON = InventoryListFilterAllOfFromJSON;
function InventoryListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'ledger': !runtime_1.exists(json, 'ledger') ? undefined : json['ledger'],
        'totals': !runtime_1.exists(json, 'totals') ? undefined : json['totals'],
    };
}
exports.InventoryListFilterAllOfFromJSONTyped = InventoryListFilterAllOfFromJSONTyped;
function InventoryListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'ledger': value.ledger,
        'totals': value.totals,
    };
}
exports.InventoryListFilterAllOfToJSON = InventoryListFilterAllOfToJSON;
