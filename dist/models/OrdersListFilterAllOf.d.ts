/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Orders Filter
 * @export
 * @interface OrdersListFilterAllOf
 */
export interface OrdersListFilterAllOf {
    /**
     * Orders matching this recipient email
     * @type {string}
     * @memberof OrdersListFilterAllOf
     */
    email?: string;
    /**
     * Orders by shipping labels requirement
     * @type {boolean}
     * @memberof OrdersListFilterAllOf
     */
    requireShippingLabels?: boolean;
    /**
     * if set to true use timestamp instead of create time
     * @type {boolean}
     * @memberof OrdersListFilterAllOf
     */
    timestamp?: boolean;
}
export declare function OrdersListFilterAllOfFromJSON(json: any): OrdersListFilterAllOf;
export declare function OrdersListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersListFilterAllOf;
export declare function OrdersListFilterAllOfToJSON(value?: OrdersListFilterAllOf | null): any;
