"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhooksListErrorToJSON = exports.WebhooksListErrorFromJSONTyped = exports.WebhooksListErrorFromJSON = exports.WebhooksListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var WebhooksListErrorSuccessEnum;
(function (WebhooksListErrorSuccessEnum) {
    WebhooksListErrorSuccessEnum["False"] = "false";
})(WebhooksListErrorSuccessEnum = exports.WebhooksListErrorSuccessEnum || (exports.WebhooksListErrorSuccessEnum = {}));
function WebhooksListErrorFromJSON(json) {
    return WebhooksListErrorFromJSONTyped(json, false);
}
exports.WebhooksListErrorFromJSON = WebhooksListErrorFromJSON;
function WebhooksListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.WebhooksListFilterFromJSON(json['filters']),
    };
}
exports.WebhooksListErrorFromJSONTyped = WebhooksListErrorFromJSONTyped;
function WebhooksListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.WebhooksListFilterToJSON(value.filters),
    };
}
exports.WebhooksListErrorToJSON = WebhooksListErrorToJSON;
