"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserToJSON = exports.UserFromJSONTyped = exports.UserFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UserFromJSON(json) {
    return UserFromJSONTyped(json, false);
}
exports.UserFromJSON = UserFromJSON;
function UserFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'userId': !runtime_1.exists(json, 'userId') ? undefined : json['userId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.UserStatusFromJSON(json['status']),
        'name': json['name'],
        'email': json['email'],
        'roles': json['roles'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'timeZone': !runtime_1.exists(json, 'timeZone') ? undefined : json['timeZone'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'address': !runtime_1.exists(json, 'address') ? undefined : _1.UserAddressFromJSON(json['address']),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.UserFromJSONTyped = UserFromJSONTyped;
function UserToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'userId': value.userId,
        'status': _1.UserStatusToJSON(value.status),
        'name': value.name,
        'email': value.email,
        'roles': value.roles,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'description': value.description,
        'address': _1.UserAddressToJSON(value.address),
    };
}
exports.UserToJSON = UserToJSON;
