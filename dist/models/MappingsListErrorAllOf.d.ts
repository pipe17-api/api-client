/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingsListFilter } from './';
/**
 *
 * @export
 * @interface MappingsListErrorAllOf
 */
export interface MappingsListErrorAllOf {
    /**
     *
     * @type {MappingsListFilter}
     * @memberof MappingsListErrorAllOf
     */
    filters?: MappingsListFilter;
}
export declare function MappingsListErrorAllOfFromJSON(json: any): MappingsListErrorAllOf;
export declare function MappingsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsListErrorAllOf;
export declare function MappingsListErrorAllOfToJSON(value?: MappingsListErrorAllOf | null): any;
