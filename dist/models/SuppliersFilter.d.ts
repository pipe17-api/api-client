/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SuppliersFilter
 */
export interface SuppliersFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof SuppliersFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof SuppliersFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof SuppliersFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof SuppliersFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof SuppliersFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof SuppliersFilter
     */
    count?: number;
    /**
     * Suppliers by list of supplierId
     * @type {Array<string>}
     * @memberof SuppliersFilter
     */
    supplierId?: Array<string>;
    /**
     * Soft deleted suppliers
     * @type {boolean}
     * @memberof SuppliersFilter
     */
    deleted?: boolean;
}
export declare function SuppliersFilterFromJSON(json: any): SuppliersFilter;
export declare function SuppliersFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersFilter;
export declare function SuppliersFilterToJSON(value?: SuppliersFilter | null): any;
