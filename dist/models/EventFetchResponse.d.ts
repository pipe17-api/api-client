/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Event } from './';
/**
 *
 * @export
 * @interface EventFetchResponse
 */
export interface EventFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EventFetchResponse
     */
    success: EventFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventFetchResponse
     */
    code: EventFetchResponseCodeEnum;
    /**
     *
     * @type {Event}
     * @memberof EventFetchResponse
     */
    event?: Event;
}
/**
* @export
* @enum {string}
*/
export declare enum EventFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EventFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EventFetchResponseFromJSON(json: any): EventFetchResponse;
export declare function EventFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventFetchResponse;
export declare function EventFetchResponseToJSON(value?: EventFetchResponse | null): any;
