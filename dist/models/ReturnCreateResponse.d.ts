/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Return } from './';
/**
 *
 * @export
 * @interface ReturnCreateResponse
 */
export interface ReturnCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReturnCreateResponse
     */
    success: ReturnCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnCreateResponse
     */
    code: ReturnCreateResponseCodeEnum;
    /**
     *
     * @type {Return}
     * @memberof ReturnCreateResponse
     */
    _return?: Return;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReturnCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReturnCreateResponseFromJSON(json: any): ReturnCreateResponse;
export declare function ReturnCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnCreateResponse;
export declare function ReturnCreateResponseToJSON(value?: ReturnCreateResponse | null): any;
