/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 *
 * @export
 * @interface AccountUpdateData
 */
export interface AccountUpdateData {
    /**
     *
     * @type {AccountStatus}
     * @memberof AccountUpdateData
     */
    status?: AccountStatus;
    /**
     *
     * @type {Date}
     * @memberof AccountUpdateData
     */
    expirationDate?: Date;
    /**
     *
     * @type {object}
     * @memberof AccountUpdateData
     */
    payment?: object;
}
export declare function AccountUpdateDataFromJSON(json: any): AccountUpdateData;
export declare function AccountUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountUpdateData;
export declare function AccountUpdateDataToJSON(value?: AccountUpdateData | null): any;
