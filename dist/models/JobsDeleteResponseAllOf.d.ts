/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job, JobsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface JobsDeleteResponseAllOf
 */
export interface JobsDeleteResponseAllOf {
    /**
     *
     * @type {JobsDeleteFilter}
     * @memberof JobsDeleteResponseAllOf
     */
    filters?: JobsDeleteFilter;
    /**
     *
     * @type {Array<Job>}
     * @memberof JobsDeleteResponseAllOf
     */
    jobs?: Array<Job>;
    /**
     * Number of deleted jobs
     * @type {number}
     * @memberof JobsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof JobsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function JobsDeleteResponseAllOfFromJSON(json: any): JobsDeleteResponseAllOf;
export declare function JobsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsDeleteResponseAllOf;
export declare function JobsDeleteResponseAllOfToJSON(value?: JobsDeleteResponseAllOf | null): any;
