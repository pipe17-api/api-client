/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalsListFilter } from './';
/**
 *
 * @export
 * @interface ArrivalsListErrorAllOf
 */
export interface ArrivalsListErrorAllOf {
    /**
     *
     * @type {ArrivalsListFilter}
     * @memberof ArrivalsListErrorAllOf
     */
    filters?: ArrivalsListFilter;
}
export declare function ArrivalsListErrorAllOfFromJSON(json: any): ArrivalsListErrorAllOf;
export declare function ArrivalsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsListErrorAllOf;
export declare function ArrivalsListErrorAllOfToJSON(value?: ArrivalsListErrorAllOf | null): any;
