/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ArrivalUpdateResponse
 */
export interface ArrivalUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ArrivalUpdateResponse
     */
    success: ArrivalUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalUpdateResponse
     */
    code: ArrivalUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ArrivalUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ArrivalUpdateResponseFromJSON(json: any): ArrivalUpdateResponse;
export declare function ArrivalUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalUpdateResponse;
export declare function ArrivalUpdateResponseToJSON(value?: ArrivalUpdateResponse | null): any;
