"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateToJSON = exports.IntegrationStateFromJSONTyped = exports.IntegrationStateFromJSON = void 0;
const runtime_1 = require("../runtime");
function IntegrationStateFromJSON(json) {
    return IntegrationStateFromJSONTyped(json, false);
}
exports.IntegrationStateFromJSON = IntegrationStateFromJSON;
function IntegrationStateFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'name': json['name'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.IntegrationStateFromJSONTyped = IntegrationStateFromJSONTyped;
function IntegrationStateToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
        'name': value.name,
        'value': value.value,
    };
}
exports.IntegrationStateToJSON = IntegrationStateToJSON;
