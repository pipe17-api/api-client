/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Exception } from './';
/**
 *
 * @export
 * @interface ExceptionCreateResponseAllOf
 */
export interface ExceptionCreateResponseAllOf {
    /**
     *
     * @type {Exception}
     * @memberof ExceptionCreateResponseAllOf
     */
    exception?: Exception;
}
export declare function ExceptionCreateResponseAllOfFromJSON(json: any): ExceptionCreateResponseAllOf;
export declare function ExceptionCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionCreateResponseAllOf;
export declare function ExceptionCreateResponseAllOfToJSON(value?: ExceptionCreateResponseAllOf | null): any;
