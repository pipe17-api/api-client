"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreateResponseAllOfToJSON = exports.UserCreateResponseAllOfFromJSONTyped = exports.UserCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UserCreateResponseAllOfFromJSON(json) {
    return UserCreateResponseAllOfFromJSONTyped(json, false);
}
exports.UserCreateResponseAllOfFromJSON = UserCreateResponseAllOfFromJSON;
function UserCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserFromJSON(json['user']),
    };
}
exports.UserCreateResponseAllOfFromJSONTyped = UserCreateResponseAllOfFromJSONTyped;
function UserCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'user': _1.UserToJSON(value.user),
    };
}
exports.UserCreateResponseAllOfToJSON = UserCreateResponseAllOfToJSON;
