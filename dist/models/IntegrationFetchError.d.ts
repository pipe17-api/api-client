/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationFetchError
 */
export interface IntegrationFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationFetchError
     */
    success: IntegrationFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationFetchErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationFetchErrorFromJSON(json: any): IntegrationFetchError;
export declare function IntegrationFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationFetchError;
export declare function IntegrationFetchErrorToJSON(value?: IntegrationFetchError | null): any;
