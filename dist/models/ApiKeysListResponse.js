"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeysListResponseToJSON = exports.ApiKeysListResponseFromJSONTyped = exports.ApiKeysListResponseFromJSON = exports.ApiKeysListResponseCodeEnum = exports.ApiKeysListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ApiKeysListResponseSuccessEnum;
(function (ApiKeysListResponseSuccessEnum) {
    ApiKeysListResponseSuccessEnum["True"] = "true";
})(ApiKeysListResponseSuccessEnum = exports.ApiKeysListResponseSuccessEnum || (exports.ApiKeysListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ApiKeysListResponseCodeEnum;
(function (ApiKeysListResponseCodeEnum) {
    ApiKeysListResponseCodeEnum[ApiKeysListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ApiKeysListResponseCodeEnum[ApiKeysListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ApiKeysListResponseCodeEnum[ApiKeysListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ApiKeysListResponseCodeEnum = exports.ApiKeysListResponseCodeEnum || (exports.ApiKeysListResponseCodeEnum = {}));
function ApiKeysListResponseFromJSON(json) {
    return ApiKeysListResponseFromJSONTyped(json, false);
}
exports.ApiKeysListResponseFromJSON = ApiKeysListResponseFromJSON;
function ApiKeysListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ApiKeysListFilterFromJSON(json['filters']),
        'apikeys': !runtime_1.exists(json, 'apikeys') ? undefined : (json['apikeys'].map(_1.ApiKeyFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ApiKeysListResponseFromJSONTyped = ApiKeysListResponseFromJSONTyped;
function ApiKeysListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ApiKeysListFilterToJSON(value.filters),
        'apikeys': value.apikeys === undefined ? undefined : (value.apikeys.map(_1.ApiKeyToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ApiKeysListResponseToJSON = ApiKeysListResponseToJSON;
