/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventsListFilter } from './';
/**
 *
 * @export
 * @interface EventsListError
 */
export interface EventsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EventsListError
     */
    success: EventsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EventsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EventsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {EventsListFilter}
     * @memberof EventsListError
     */
    filters?: EventsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum EventsListErrorSuccessEnum {
    False = "false"
}
export declare function EventsListErrorFromJSON(json: any): EventsListError;
export declare function EventsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsListError;
export declare function EventsListErrorToJSON(value?: EventsListError | null): any;
