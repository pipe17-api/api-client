/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Tracking } from './';
/**
 *
 * @export
 * @interface TrackingCreateResponseAllOf
 */
export interface TrackingCreateResponseAllOf {
    /**
     *
     * @type {Tracking}
     * @memberof TrackingCreateResponseAllOf
     */
    tracking?: Tracking;
}
export declare function TrackingCreateResponseAllOfFromJSON(json: any): TrackingCreateResponseAllOf;
export declare function TrackingCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingCreateResponseAllOf;
export declare function TrackingCreateResponseAllOfToJSON(value?: TrackingCreateResponseAllOf | null): any;
