"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFiltersFilterAllOfToJSON = exports.ExceptionFiltersFilterAllOfFromJSONTyped = exports.ExceptionFiltersFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ExceptionFiltersFilterAllOfFromJSON(json) {
    return ExceptionFiltersFilterAllOfFromJSONTyped(json, false);
}
exports.ExceptionFiltersFilterAllOfFromJSON = ExceptionFiltersFilterAllOfFromJSON;
function ExceptionFiltersFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filterId': !runtime_1.exists(json, 'filterId') ? undefined : json['filterId'],
        'exceptionType': !runtime_1.exists(json, 'exceptionType') ? undefined : json['exceptionType'],
    };
}
exports.ExceptionFiltersFilterAllOfFromJSONTyped = ExceptionFiltersFilterAllOfFromJSONTyped;
function ExceptionFiltersFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filterId': value.filterId,
        'exceptionType': value.exceptionType,
    };
}
exports.ExceptionFiltersFilterAllOfToJSON = ExceptionFiltersFilterAllOfToJSON;
