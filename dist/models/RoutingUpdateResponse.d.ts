/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingUpdateResponse
 */
export interface RoutingUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoutingUpdateResponse
     */
    success: RoutingUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingUpdateResponse
     */
    code: RoutingUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoutingUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoutingUpdateResponseFromJSON(json: any): RoutingUpdateResponse;
export declare function RoutingUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingUpdateResponse;
export declare function RoutingUpdateResponseToJSON(value?: RoutingUpdateResponse | null): any;
