"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersFilterAllOfToJSON = exports.TransfersFilterAllOfFromJSONTyped = exports.TransfersFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransfersFilterAllOfFromJSON(json) {
    return TransfersFilterAllOfFromJSONTyped(json, false);
}
exports.TransfersFilterAllOfFromJSON = TransfersFilterAllOfFromJSON;
function TransfersFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'transferId': !runtime_1.exists(json, 'transferId') ? undefined : json['transferId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.TransferStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.TransfersFilterAllOfFromJSONTyped = TransfersFilterAllOfFromJSONTyped;
function TransfersFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'transferId': value.transferId,
        'extOrderId': value.extOrderId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.TransferStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.TransfersFilterAllOfToJSON = TransfersFilterAllOfToJSON;
