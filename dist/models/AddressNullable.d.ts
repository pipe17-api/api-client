/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AddressNullable
 */
export interface AddressNullable {
    /**
     * First Name
     * @type {string}
     * @memberof AddressNullable
     */
    firstName?: string;
    /**
     * Last Name
     * @type {string}
     * @memberof AddressNullable
     */
    lastName?: string;
    /**
     * Company
     * @type {string}
     * @memberof AddressNullable
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof AddressNullable
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof AddressNullable
     */
    address2?: string | null;
    /**
     * City
     * @type {string}
     * @memberof AddressNullable
     */
    city?: string;
    /**
     * If within the US, required 2 letter State Code
     * @type {string}
     * @memberof AddressNullable
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof AddressNullable
     */
    zipCodeOrPostalCode?: string;
    /**
     * ISO 3 letter code only
     * @type {string}
     * @memberof AddressNullable
     */
    country?: string;
    /**
     * email
     * @type {string}
     * @memberof AddressNullable
     */
    email?: string;
    /**
     * Phone
     * @type {string}
     * @memberof AddressNullable
     */
    phone?: string;
}
export declare function AddressNullableFromJSON(json: any): AddressNullable;
export declare function AddressNullableFromJSONTyped(json: any, ignoreDiscriminator: boolean): AddressNullable;
export declare function AddressNullableToJSON(value?: AddressNullable | null): any;
