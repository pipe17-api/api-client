/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierCreateData } from './';
/**
 *
 * @export
 * @interface SupplierCreateErrorAllOf
 */
export interface SupplierCreateErrorAllOf {
    /**
     *
     * @type {SupplierCreateData}
     * @memberof SupplierCreateErrorAllOf
     */
    supplier?: SupplierCreateData;
}
export declare function SupplierCreateErrorAllOfFromJSON(json: any): SupplierCreateErrorAllOf;
export declare function SupplierCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierCreateErrorAllOf;
export declare function SupplierCreateErrorAllOfToJSON(value?: SupplierCreateErrorAllOf | null): any;
