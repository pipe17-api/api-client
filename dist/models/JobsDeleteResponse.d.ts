/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job, JobsDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface JobsDeleteResponse
 */
export interface JobsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof JobsDeleteResponse
     */
    success: JobsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobsDeleteResponse
     */
    code: JobsDeleteResponseCodeEnum;
    /**
     *
     * @type {JobsDeleteFilter}
     * @memberof JobsDeleteResponse
     */
    filters?: JobsDeleteFilter;
    /**
     *
     * @type {Array<Job>}
     * @memberof JobsDeleteResponse
     */
    jobs?: Array<Job>;
    /**
     * Number of deleted jobs
     * @type {number}
     * @memberof JobsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof JobsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum JobsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum JobsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function JobsDeleteResponseFromJSON(json: any): JobsDeleteResponse;
export declare function JobsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsDeleteResponse;
export declare function JobsDeleteResponseToJSON(value?: JobsDeleteResponse | null): any;
