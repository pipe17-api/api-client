"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyUpdateErrorAllOfToJSON = exports.ApiKeyUpdateErrorAllOfFromJSONTyped = exports.ApiKeyUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ApiKeyUpdateErrorAllOfFromJSON(json) {
    return ApiKeyUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ApiKeyUpdateErrorAllOfFromJSON = ApiKeyUpdateErrorAllOfFromJSON;
function ApiKeyUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : _1.ApiKeyUpdateDataFromJSON(json['apikey']),
    };
}
exports.ApiKeyUpdateErrorAllOfFromJSONTyped = ApiKeyUpdateErrorAllOfFromJSONTyped;
function ApiKeyUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikey': _1.ApiKeyUpdateDataToJSON(value.apikey),
    };
}
exports.ApiKeyUpdateErrorAllOfToJSON = ApiKeyUpdateErrorAllOfToJSON;
