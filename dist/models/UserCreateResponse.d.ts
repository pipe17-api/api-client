/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { User } from './';
/**
 *
 * @export
 * @interface UserCreateResponse
 */
export interface UserCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof UserCreateResponse
     */
    success: UserCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof UserCreateResponse
     */
    code: UserCreateResponseCodeEnum;
    /**
     *
     * @type {User}
     * @memberof UserCreateResponse
     */
    user?: User;
}
/**
* @export
* @enum {string}
*/
export declare enum UserCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum UserCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function UserCreateResponseFromJSON(json: any): UserCreateResponse;
export declare function UserCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateResponse;
export declare function UserCreateResponseToJSON(value?: UserCreateResponse | null): any;
