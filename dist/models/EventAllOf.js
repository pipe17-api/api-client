"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventAllOfToJSON = exports.EventAllOfFromJSONTyped = exports.EventAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EventAllOfFromJSON(json) {
    return EventAllOfFromJSONTyped(json, false);
}
exports.EventAllOfFromJSON = EventAllOfFromJSON;
function EventAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'eventId': !runtime_1.exists(json, 'eventId') ? undefined : json['eventId'],
        'source': !runtime_1.exists(json, 'source') ? undefined : _1.EventRequestSourceFromJSON(json['source']),
    };
}
exports.EventAllOfFromJSONTyped = EventAllOfFromJSONTyped;
function EventAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'eventId': value.eventId,
        'source': _1.EventRequestSourceToJSON(value.source),
    };
}
exports.EventAllOfToJSON = EventAllOfToJSON;
