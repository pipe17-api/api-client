"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountUpdateErrorToJSON = exports.AccountUpdateErrorFromJSONTyped = exports.AccountUpdateErrorFromJSON = exports.AccountUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var AccountUpdateErrorSuccessEnum;
(function (AccountUpdateErrorSuccessEnum) {
    AccountUpdateErrorSuccessEnum["False"] = "false";
})(AccountUpdateErrorSuccessEnum = exports.AccountUpdateErrorSuccessEnum || (exports.AccountUpdateErrorSuccessEnum = {}));
function AccountUpdateErrorFromJSON(json) {
    return AccountUpdateErrorFromJSONTyped(json, false);
}
exports.AccountUpdateErrorFromJSON = AccountUpdateErrorFromJSON;
function AccountUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountUpdateDataFromJSON(json['account']),
    };
}
exports.AccountUpdateErrorFromJSONTyped = AccountUpdateErrorFromJSONTyped;
function AccountUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'account': _1.AccountUpdateDataToJSON(value.account),
    };
}
exports.AccountUpdateErrorToJSON = AccountUpdateErrorToJSON;
