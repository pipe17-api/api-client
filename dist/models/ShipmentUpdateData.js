"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentUpdateDataToJSON = exports.ShipmentUpdateDataFromJSONTyped = exports.ShipmentUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentUpdateDataFromJSON(json) {
    return ShipmentUpdateDataFromJSONTyped(json, false);
}
exports.ShipmentUpdateDataFromJSON = ShipmentUpdateDataFromJSON;
function ShipmentUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (new Date(json['shipByDate'])),
        'expectedShipDate': !runtime_1.exists(json, 'expectedShipDate') ? undefined : (new Date(json['expectedShipDate'])),
        'expectedDeliveryDate': !runtime_1.exists(json, 'expectedDeliveryDate') ? undefined : (new Date(json['expectedDeliveryDate'])),
        'shippingAddress': !runtime_1.exists(json, 'shippingAddress') ? undefined : _1.AddressUpdateDataFromJSON(json['shippingAddress']),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingCode': !runtime_1.exists(json, 'shippingCode') ? undefined : json['shippingCode'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shippingNote': !runtime_1.exists(json, 'shippingNote') ? undefined : json['shippingNote'],
        'shippingLabels': !runtime_1.exists(json, 'shippingLabels') ? undefined : json['shippingLabels'],
        'giftNote': !runtime_1.exists(json, 'giftNote') ? undefined : json['giftNote'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ShipmentStatusFromJSON(json['status']),
        'extShipmentId': !runtime_1.exists(json, 'extShipmentId') ? undefined : json['extShipmentId'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ShipmentUpdateDataLineItemsFromJSON)),
    };
}
exports.ShipmentUpdateDataFromJSONTyped = ShipmentUpdateDataFromJSONTyped;
function ShipmentUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipByDate': value.shipByDate === undefined ? undefined : (new Date(value.shipByDate).toISOString()),
        'expectedShipDate': value.expectedShipDate === undefined ? undefined : (new Date(value.expectedShipDate).toISOString()),
        'expectedDeliveryDate': value.expectedDeliveryDate === undefined ? undefined : (new Date(value.expectedDeliveryDate).toISOString()),
        'shippingAddress': _1.AddressUpdateDataToJSON(value.shippingAddress),
        'shippingCarrier': value.shippingCarrier,
        'shippingCode': value.shippingCode,
        'shippingClass': value.shippingClass,
        'shippingNote': value.shippingNote,
        'shippingLabels': value.shippingLabels,
        'giftNote': value.giftNote,
        'incoterms': value.incoterms,
        'locationId': value.locationId,
        'status': _1.ShipmentStatusToJSON(value.status),
        'extShipmentId': value.extShipmentId,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ShipmentUpdateDataLineItemsToJSON)),
    };
}
exports.ShipmentUpdateDataToJSON = ShipmentUpdateDataToJSON;
