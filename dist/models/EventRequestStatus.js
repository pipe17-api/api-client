"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventRequestStatusToJSON = exports.EventRequestStatusFromJSONTyped = exports.EventRequestStatusFromJSON = exports.EventRequestStatus = void 0;
/**
 * Event Status
 * @export
 * @enum {string}
 */
var EventRequestStatus;
(function (EventRequestStatus) {
    EventRequestStatus["Pending"] = "pending";
    EventRequestStatus["Acknowledged"] = "acknowledged";
    EventRequestStatus["Completed"] = "completed";
    EventRequestStatus["Failed"] = "failed";
})(EventRequestStatus = exports.EventRequestStatus || (exports.EventRequestStatus = {}));
function EventRequestStatusFromJSON(json) {
    return EventRequestStatusFromJSONTyped(json, false);
}
exports.EventRequestStatusFromJSON = EventRequestStatusFromJSON;
function EventRequestStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EventRequestStatusFromJSONTyped = EventRequestStatusFromJSONTyped;
function EventRequestStatusToJSON(value) {
    return value;
}
exports.EventRequestStatusToJSON = EventRequestStatusToJSON;
