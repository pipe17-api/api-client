/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Methods } from './';
/**
 *
 * @export
 * @interface RoleCreateRequest
 */
export interface RoleCreateRequest {
    /**
     * Global role indicator
     * @type {boolean}
     * @memberof RoleCreateRequest
     */
    isPublic?: boolean;
    /**
     * Role Name
     * @type {string}
     * @memberof RoleCreateRequest
     */
    name: string;
    /**
     * Role's description
     * @type {string}
     * @memberof RoleCreateRequest
     */
    description?: string;
    /**
     *
     * @type {Methods}
     * @memberof RoleCreateRequest
     */
    methods?: Methods;
}
export declare function RoleCreateRequestFromJSON(json: any): RoleCreateRequest;
export declare function RoleCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleCreateRequest;
export declare function RoleCreateRequestToJSON(value?: RoleCreateRequest | null): any;
