/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingUpdateData } from './';
/**
 *
 * @export
 * @interface MappingUpdateErrorAllOf
 */
export interface MappingUpdateErrorAllOf {
    /**
     *
     * @type {MappingUpdateData}
     * @memberof MappingUpdateErrorAllOf
     */
    mapping?: MappingUpdateData;
}
export declare function MappingUpdateErrorAllOfFromJSON(json: any): MappingUpdateErrorAllOf;
export declare function MappingUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingUpdateErrorAllOf;
export declare function MappingUpdateErrorAllOfToJSON(value?: MappingUpdateErrorAllOf | null): any;
