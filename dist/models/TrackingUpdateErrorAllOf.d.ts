/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingUpdateData } from './';
/**
 *
 * @export
 * @interface TrackingUpdateErrorAllOf
 */
export interface TrackingUpdateErrorAllOf {
    /**
     *
     * @type {TrackingUpdateData}
     * @memberof TrackingUpdateErrorAllOf
     */
    tracking?: TrackingUpdateData;
}
export declare function TrackingUpdateErrorAllOfFromJSON(json: any): TrackingUpdateErrorAllOf;
export declare function TrackingUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingUpdateErrorAllOf;
export declare function TrackingUpdateErrorAllOfToJSON(value?: TrackingUpdateErrorAllOf | null): any;
