"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsDeleteResponseToJSON = exports.RoutingsDeleteResponseFromJSONTyped = exports.RoutingsDeleteResponseFromJSON = exports.RoutingsDeleteResponseCodeEnum = exports.RoutingsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoutingsDeleteResponseSuccessEnum;
(function (RoutingsDeleteResponseSuccessEnum) {
    RoutingsDeleteResponseSuccessEnum["True"] = "true";
})(RoutingsDeleteResponseSuccessEnum = exports.RoutingsDeleteResponseSuccessEnum || (exports.RoutingsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoutingsDeleteResponseCodeEnum;
(function (RoutingsDeleteResponseCodeEnum) {
    RoutingsDeleteResponseCodeEnum[RoutingsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoutingsDeleteResponseCodeEnum[RoutingsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoutingsDeleteResponseCodeEnum[RoutingsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoutingsDeleteResponseCodeEnum = exports.RoutingsDeleteResponseCodeEnum || (exports.RoutingsDeleteResponseCodeEnum = {}));
function RoutingsDeleteResponseFromJSON(json) {
    return RoutingsDeleteResponseFromJSONTyped(json, false);
}
exports.RoutingsDeleteResponseFromJSON = RoutingsDeleteResponseFromJSON;
function RoutingsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RoutingsDeleteFilterFromJSON(json['filters']),
        'routings': !runtime_1.exists(json, 'routings') ? undefined : (json['routings'].map(_1.RoutingFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RoutingsDeleteResponseFromJSONTyped = RoutingsDeleteResponseFromJSONTyped;
function RoutingsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.RoutingsDeleteFilterToJSON(value.filters),
        'routings': value.routings === undefined ? undefined : (value.routings.map(_1.RoutingToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RoutingsDeleteResponseToJSON = RoutingsDeleteResponseToJSON;
