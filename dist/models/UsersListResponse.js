"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersListResponseToJSON = exports.UsersListResponseFromJSONTyped = exports.UsersListResponseFromJSON = exports.UsersListResponseCodeEnum = exports.UsersListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UsersListResponseSuccessEnum;
(function (UsersListResponseSuccessEnum) {
    UsersListResponseSuccessEnum["True"] = "true";
})(UsersListResponseSuccessEnum = exports.UsersListResponseSuccessEnum || (exports.UsersListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var UsersListResponseCodeEnum;
(function (UsersListResponseCodeEnum) {
    UsersListResponseCodeEnum[UsersListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    UsersListResponseCodeEnum[UsersListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    UsersListResponseCodeEnum[UsersListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(UsersListResponseCodeEnum = exports.UsersListResponseCodeEnum || (exports.UsersListResponseCodeEnum = {}));
function UsersListResponseFromJSON(json) {
    return UsersListResponseFromJSONTyped(json, false);
}
exports.UsersListResponseFromJSON = UsersListResponseFromJSON;
function UsersListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.UsersListFilterFromJSON(json['filters']),
        'users': !runtime_1.exists(json, 'users') ? undefined : (json['users'].map(_1.UserFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.UsersListResponseFromJSONTyped = UsersListResponseFromJSONTyped;
function UsersListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.UsersListFilterToJSON(value.filters),
        'users': value.users === undefined ? undefined : (value.users.map(_1.UserToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.UsersListResponseToJSON = UsersListResponseToJSON;
