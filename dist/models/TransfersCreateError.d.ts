/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferCreateResult } from './';
/**
 *
 * @export
 * @interface TransfersCreateError
 */
export interface TransfersCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TransfersCreateError
     */
    success: TransfersCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransfersCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TransfersCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TransfersCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<TransferCreateResult>}
     * @memberof TransfersCreateError
     */
    transfers?: Array<TransferCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum TransfersCreateErrorSuccessEnum {
    False = "false"
}
export declare function TransfersCreateErrorFromJSON(json: any): TransfersCreateError;
export declare function TransfersCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersCreateError;
export declare function TransfersCreateErrorToJSON(value?: TransfersCreateError | null): any;
