"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsListErrorToJSON = exports.FulfillmentsListErrorFromJSONTyped = exports.FulfillmentsListErrorFromJSON = exports.FulfillmentsListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentsListErrorSuccessEnum;
(function (FulfillmentsListErrorSuccessEnum) {
    FulfillmentsListErrorSuccessEnum["False"] = "false";
})(FulfillmentsListErrorSuccessEnum = exports.FulfillmentsListErrorSuccessEnum || (exports.FulfillmentsListErrorSuccessEnum = {}));
function FulfillmentsListErrorFromJSON(json) {
    return FulfillmentsListErrorFromJSONTyped(json, false);
}
exports.FulfillmentsListErrorFromJSON = FulfillmentsListErrorFromJSON;
function FulfillmentsListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.FulfillmentsListFilterFromJSON(json['filters']),
    };
}
exports.FulfillmentsListErrorFromJSONTyped = FulfillmentsListErrorFromJSONTyped;
function FulfillmentsListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.FulfillmentsListFilterToJSON(value.filters),
    };
}
exports.FulfillmentsListErrorToJSON = FulfillmentsListErrorToJSON;
