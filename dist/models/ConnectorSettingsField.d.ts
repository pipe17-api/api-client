/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorSettingsOptions } from './';
/**
 *
 * @export
 * @interface ConnectorSettingsField
 */
export interface ConnectorSettingsField {
    /**
     * Option Name
     * @type {string}
     * @memberof ConnectorSettingsField
     */
    path?: string;
    /**
     * Option Type
     * @type {string}
     * @memberof ConnectorSettingsField
     */
    type?: ConnectorSettingsFieldTypeEnum;
    /**
     * Setting Option is secret
     * @type {boolean}
     * @memberof ConnectorSettingsField
     */
    secret?: boolean;
    /**
     * Setting defines a group
     * @type {string}
     * @memberof ConnectorSettingsField
     */
    group?: string;
    /**
     *
     * @type {ConnectorSettingsOptions}
     * @memberof ConnectorSettingsField
     */
    options?: ConnectorSettingsOptions;
    /**
     *
     * @type {Array<object>}
     * @memberof ConnectorSettingsField
     */
    validation?: Array<object>;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorSettingsFieldTypeEnum {
    String = "string",
    Number = "number",
    Boolean = "boolean",
    Date = "date",
    Array = "array",
    Object = "object",
    Json = "json"
}
export declare function ConnectorSettingsFieldFromJSON(json: any): ConnectorSettingsField;
export declare function ConnectorSettingsFieldFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSettingsField;
export declare function ConnectorSettingsFieldToJSON(value?: ConnectorSettingsField | null): any;
