/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptsListFilter } from './';
/**
 *
 * @export
 * @interface ReceiptsListErrorAllOf
 */
export interface ReceiptsListErrorAllOf {
    /**
     *
     * @type {ReceiptsListFilter}
     * @memberof ReceiptsListErrorAllOf
     */
    filters?: ReceiptsListFilter;
}
export declare function ReceiptsListErrorAllOfFromJSON(json: any): ReceiptsListErrorAllOf;
export declare function ReceiptsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsListErrorAllOf;
export declare function ReceiptsListErrorAllOfToJSON(value?: ReceiptsListErrorAllOf | null): any;
