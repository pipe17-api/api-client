/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TransferUpdateResponse
 */
export interface TransferUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TransferUpdateResponse
     */
    success: TransferUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransferUpdateResponse
     */
    code: TransferUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum TransferUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TransferUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TransferUpdateResponseFromJSON(json: any): TransferUpdateResponse;
export declare function TransferUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferUpdateResponse;
export declare function TransferUpdateResponseToJSON(value?: TransferUpdateResponse | null): any;
