/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelCreateData
 */
export interface LabelCreateData {
    /**
     *
     * @type {string}
     * @memberof LabelCreateData
     */
    fileName: string;
    /**
     *
     * @type {string}
     * @memberof LabelCreateData
     */
    contentType: string;
    /**
     *
     * @type {string}
     * @memberof LabelCreateData
     */
    trackingNumber?: string;
    /**
     *
     * @type {string}
     * @memberof LabelCreateData
     */
    shippingMethod?: string;
}
export declare function LabelCreateDataFromJSON(json: any): LabelCreateData;
export declare function LabelCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelCreateData;
export declare function LabelCreateDataToJSON(value?: LabelCreateData | null): any;
