/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKeyUpdateData } from './';
/**
 *
 * @export
 * @interface ApiKeyUpdateError
 */
export interface ApiKeyUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ApiKeyUpdateError
     */
    success: ApiKeyUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ApiKeyUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ApiKeyUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ApiKeyUpdateData}
     * @memberof ApiKeyUpdateError
     */
    apikey?: ApiKeyUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ApiKeyUpdateErrorFromJSON(json: any): ApiKeyUpdateError;
export declare function ApiKeyUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyUpdateError;
export declare function ApiKeyUpdateErrorToJSON(value?: ApiKeyUpdateError | null): any;
