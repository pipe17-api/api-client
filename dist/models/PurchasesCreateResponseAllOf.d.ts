/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseCreateResult } from './';
/**
 *
 * @export
 * @interface PurchasesCreateResponseAllOf
 */
export interface PurchasesCreateResponseAllOf {
    /**
     *
     * @type {Array<PurchaseCreateResult>}
     * @memberof PurchasesCreateResponseAllOf
     */
    purchases?: Array<PurchaseCreateResult>;
}
export declare function PurchasesCreateResponseAllOfFromJSON(json: any): PurchasesCreateResponseAllOf;
export declare function PurchasesCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesCreateResponseAllOf;
export declare function PurchasesCreateResponseAllOfToJSON(value?: PurchasesCreateResponseAllOf | null): any;
