"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnUpdateResponseToJSON = exports.ReturnUpdateResponseFromJSONTyped = exports.ReturnUpdateResponseFromJSON = exports.ReturnUpdateResponseCodeEnum = exports.ReturnUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ReturnUpdateResponseSuccessEnum;
(function (ReturnUpdateResponseSuccessEnum) {
    ReturnUpdateResponseSuccessEnum["True"] = "true";
})(ReturnUpdateResponseSuccessEnum = exports.ReturnUpdateResponseSuccessEnum || (exports.ReturnUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReturnUpdateResponseCodeEnum;
(function (ReturnUpdateResponseCodeEnum) {
    ReturnUpdateResponseCodeEnum[ReturnUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReturnUpdateResponseCodeEnum[ReturnUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReturnUpdateResponseCodeEnum[ReturnUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReturnUpdateResponseCodeEnum = exports.ReturnUpdateResponseCodeEnum || (exports.ReturnUpdateResponseCodeEnum = {}));
function ReturnUpdateResponseFromJSON(json) {
    return ReturnUpdateResponseFromJSONTyped(json, false);
}
exports.ReturnUpdateResponseFromJSON = ReturnUpdateResponseFromJSON;
function ReturnUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ReturnUpdateResponseFromJSONTyped = ReturnUpdateResponseFromJSONTyped;
function ReturnUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ReturnUpdateResponseToJSON = ReturnUpdateResponseToJSON;
