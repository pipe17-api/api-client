/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory } from './';
/**
 *
 * @export
 * @interface InventoryCreateResult
 */
export interface InventoryCreateResult {
    /**
     *
     * @type {string}
     * @memberof InventoryCreateResult
     */
    status?: InventoryCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof InventoryCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof InventoryCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Inventory}
     * @memberof InventoryCreateResult
     */
    inventory?: Inventory;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function InventoryCreateResultFromJSON(json: any): InventoryCreateResult;
export declare function InventoryCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryCreateResult;
export declare function InventoryCreateResultToJSON(value?: InventoryCreateResult | null): any;
