"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationUpdateRequestToJSON = exports.OrganizationUpdateRequestFromJSONTyped = exports.OrganizationUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationUpdateRequestFromJSON(json) {
    return OrganizationUpdateRequestFromJSONTyped(json, false);
}
exports.OrganizationUpdateRequestFromJSON = OrganizationUpdateRequestFromJSON;
function OrganizationUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'tier': !runtime_1.exists(json, 'tier') ? undefined : _1.OrganizationServiceTierFromJSON(json['tier']),
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'timeZone': !runtime_1.exists(json, 'timeZone') ? undefined : json['timeZone'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'address': !runtime_1.exists(json, 'address') ? undefined : _1.OrganizationUpdateAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.OrganizationStatusFromJSON(json['status']),
    };
}
exports.OrganizationUpdateRequestFromJSONTyped = OrganizationUpdateRequestFromJSONTyped;
function OrganizationUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'tier': _1.OrganizationServiceTierToJSON(value.tier),
        'email': value.email,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'logoUrl': value.logoUrl,
        'address': _1.OrganizationUpdateAddressToJSON(value.address),
        'status': _1.OrganizationStatusToJSON(value.status),
    };
}
exports.OrganizationUpdateRequestToJSON = OrganizationUpdateRequestToJSON;
