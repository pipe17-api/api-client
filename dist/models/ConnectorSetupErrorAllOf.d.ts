/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorSetupData } from './';
/**
 *
 * @export
 * @interface ConnectorSetupErrorAllOf
 */
export interface ConnectorSetupErrorAllOf {
    /**
     *
     * @type {ConnectorSetupData}
     * @memberof ConnectorSetupErrorAllOf
     */
    options?: ConnectorSetupData;
}
export declare function ConnectorSetupErrorAllOfFromJSON(json: any): ConnectorSetupErrorAllOf;
export declare function ConnectorSetupErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSetupErrorAllOf;
export declare function ConnectorSetupErrorAllOfToJSON(value?: ConnectorSetupErrorAllOf | null): any;
