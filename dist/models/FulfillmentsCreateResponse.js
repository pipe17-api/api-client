"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsCreateResponseToJSON = exports.FulfillmentsCreateResponseFromJSONTyped = exports.FulfillmentsCreateResponseFromJSON = exports.FulfillmentsCreateResponseCodeEnum = exports.FulfillmentsCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentsCreateResponseSuccessEnum;
(function (FulfillmentsCreateResponseSuccessEnum) {
    FulfillmentsCreateResponseSuccessEnum["True"] = "true";
})(FulfillmentsCreateResponseSuccessEnum = exports.FulfillmentsCreateResponseSuccessEnum || (exports.FulfillmentsCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var FulfillmentsCreateResponseCodeEnum;
(function (FulfillmentsCreateResponseCodeEnum) {
    FulfillmentsCreateResponseCodeEnum[FulfillmentsCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    FulfillmentsCreateResponseCodeEnum[FulfillmentsCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    FulfillmentsCreateResponseCodeEnum[FulfillmentsCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(FulfillmentsCreateResponseCodeEnum = exports.FulfillmentsCreateResponseCodeEnum || (exports.FulfillmentsCreateResponseCodeEnum = {}));
function FulfillmentsCreateResponseFromJSON(json) {
    return FulfillmentsCreateResponseFromJSONTyped(json, false);
}
exports.FulfillmentsCreateResponseFromJSON = FulfillmentsCreateResponseFromJSON;
function FulfillmentsCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'fulfillments': !runtime_1.exists(json, 'fulfillments') ? undefined : (json['fulfillments'].map(_1.FulfillmentCreateResultFromJSON)),
    };
}
exports.FulfillmentsCreateResponseFromJSONTyped = FulfillmentsCreateResponseFromJSONTyped;
function FulfillmentsCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'fulfillments': value.fulfillments === undefined ? undefined : (value.fulfillments.map(_1.FulfillmentCreateResultToJSON)),
    };
}
exports.FulfillmentsCreateResponseToJSON = FulfillmentsCreateResponseToJSON;
