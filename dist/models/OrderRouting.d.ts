/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRouting
 */
export interface OrderRouting {
    /**
     * Routing ID
     * @type {string}
     * @memberof OrderRouting
     */
    routingId?: string;
    /**
     * Routing rules definition
     * @type {Array<object>}
     * @memberof OrderRouting
     */
    rules: Array<object>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof OrderRouting
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof OrderRouting
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof OrderRouting
     */
    readonly orgKey?: string;
}
export declare function OrderRoutingFromJSON(json: any): OrderRouting;
export declare function OrderRoutingFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRouting;
export declare function OrderRoutingToJSON(value?: OrderRouting | null): any;
