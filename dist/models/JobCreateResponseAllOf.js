"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobCreateResponseAllOfToJSON = exports.JobCreateResponseAllOfFromJSONTyped = exports.JobCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobCreateResponseAllOfFromJSON(json) {
    return JobCreateResponseAllOfFromJSONTyped(json, false);
}
exports.JobCreateResponseAllOfFromJSON = JobCreateResponseAllOfFromJSON;
function JobCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'job': !runtime_1.exists(json, 'job') ? undefined : _1.JobFromJSON(json['job']),
    };
}
exports.JobCreateResponseAllOfFromJSONTyped = JobCreateResponseAllOfFromJSONTyped;
function JobCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'job': _1.JobToJSON(value.job),
    };
}
exports.JobCreateResponseAllOfToJSON = JobCreateResponseAllOfToJSON;
