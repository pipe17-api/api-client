"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.DirectionToJSON = exports.DirectionFromJSONTyped = exports.DirectionFromJSON = exports.Direction = void 0;
/**
 * Route/Mapping Direction
 * @export
 * @enum {string}
 */
var Direction;
(function (Direction) {
    Direction["In"] = "in";
    Direction["Out"] = "out";
})(Direction = exports.Direction || (exports.Direction = {}));
function DirectionFromJSON(json) {
    return DirectionFromJSONTyped(json, false);
}
exports.DirectionFromJSON = DirectionFromJSON;
function DirectionFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.DirectionFromJSONTyped = DirectionFromJSONTyped;
function DirectionToJSON(value) {
    return value;
}
exports.DirectionToJSON = DirectionToJSON;
