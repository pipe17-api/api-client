"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsListResponseToJSON = exports.EventsListResponseFromJSONTyped = exports.EventsListResponseFromJSON = exports.EventsListResponseCodeEnum = exports.EventsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EventsListResponseSuccessEnum;
(function (EventsListResponseSuccessEnum) {
    EventsListResponseSuccessEnum["True"] = "true";
})(EventsListResponseSuccessEnum = exports.EventsListResponseSuccessEnum || (exports.EventsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EventsListResponseCodeEnum;
(function (EventsListResponseCodeEnum) {
    EventsListResponseCodeEnum[EventsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EventsListResponseCodeEnum[EventsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EventsListResponseCodeEnum[EventsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EventsListResponseCodeEnum = exports.EventsListResponseCodeEnum || (exports.EventsListResponseCodeEnum = {}));
function EventsListResponseFromJSON(json) {
    return EventsListResponseFromJSONTyped(json, false);
}
exports.EventsListResponseFromJSON = EventsListResponseFromJSON;
function EventsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EventsListFilterFromJSON(json['filters']),
        'events': !runtime_1.exists(json, 'events') ? undefined : (json['events'].map(_1.EventFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.EventsListResponseFromJSONTyped = EventsListResponseFromJSONTyped;
function EventsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.EventsListFilterToJSON(value.filters),
        'events': value.events === undefined ? undefined : (value.events.map(_1.EventToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.EventsListResponseToJSON = EventsListResponseToJSON;
