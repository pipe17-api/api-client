/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { User } from './';
/**
 *
 * @export
 * @interface UserCreateResponseAllOf
 */
export interface UserCreateResponseAllOf {
    /**
     *
     * @type {User}
     * @memberof UserCreateResponseAllOf
     */
    user?: User;
}
export declare function UserCreateResponseAllOfFromJSON(json: any): UserCreateResponseAllOf;
export declare function UserCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateResponseAllOf;
export declare function UserCreateResponseAllOfToJSON(value?: UserCreateResponseAllOf | null): any;
