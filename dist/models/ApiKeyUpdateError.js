"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyUpdateErrorToJSON = exports.ApiKeyUpdateErrorFromJSONTyped = exports.ApiKeyUpdateErrorFromJSON = exports.ApiKeyUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ApiKeyUpdateErrorSuccessEnum;
(function (ApiKeyUpdateErrorSuccessEnum) {
    ApiKeyUpdateErrorSuccessEnum["False"] = "false";
})(ApiKeyUpdateErrorSuccessEnum = exports.ApiKeyUpdateErrorSuccessEnum || (exports.ApiKeyUpdateErrorSuccessEnum = {}));
function ApiKeyUpdateErrorFromJSON(json) {
    return ApiKeyUpdateErrorFromJSONTyped(json, false);
}
exports.ApiKeyUpdateErrorFromJSON = ApiKeyUpdateErrorFromJSON;
function ApiKeyUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : _1.ApiKeyUpdateDataFromJSON(json['apikey']),
    };
}
exports.ApiKeyUpdateErrorFromJSONTyped = ApiKeyUpdateErrorFromJSONTyped;
function ApiKeyUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'apikey': _1.ApiKeyUpdateDataToJSON(value.apikey),
    };
}
exports.ApiKeyUpdateErrorToJSON = ApiKeyUpdateErrorToJSON;
