"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelCreateResponseToJSON = exports.LabelCreateResponseFromJSONTyped = exports.LabelCreateResponseFromJSON = exports.LabelCreateResponseCodeEnum = exports.LabelCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LabelCreateResponseSuccessEnum;
(function (LabelCreateResponseSuccessEnum) {
    LabelCreateResponseSuccessEnum["True"] = "true";
})(LabelCreateResponseSuccessEnum = exports.LabelCreateResponseSuccessEnum || (exports.LabelCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LabelCreateResponseCodeEnum;
(function (LabelCreateResponseCodeEnum) {
    LabelCreateResponseCodeEnum[LabelCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LabelCreateResponseCodeEnum[LabelCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LabelCreateResponseCodeEnum[LabelCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LabelCreateResponseCodeEnum = exports.LabelCreateResponseCodeEnum || (exports.LabelCreateResponseCodeEnum = {}));
function LabelCreateResponseFromJSON(json) {
    return LabelCreateResponseFromJSONTyped(json, false);
}
exports.LabelCreateResponseFromJSON = LabelCreateResponseFromJSON;
function LabelCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'label': !runtime_1.exists(json, 'label') ? undefined : _1.LabelFromJSON(json['label']),
    };
}
exports.LabelCreateResponseFromJSONTyped = LabelCreateResponseFromJSONTyped;
function LabelCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'label': _1.LabelToJSON(value.label),
    };
}
exports.LabelCreateResponseToJSON = LabelCreateResponseToJSON;
