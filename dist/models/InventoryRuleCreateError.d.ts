/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRuleCreateData } from './';
/**
 *
 * @export
 * @interface InventoryRuleCreateError
 */
export interface InventoryRuleCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryRuleCreateError
     */
    success: InventoryRuleCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRuleCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryRuleCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryRuleCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryRuleCreateData}
     * @memberof InventoryRuleCreateError
     */
    inventoryrule?: InventoryRuleCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRuleCreateErrorSuccessEnum {
    False = "false"
}
export declare function InventoryRuleCreateErrorFromJSON(json: any): InventoryRuleCreateError;
export declare function InventoryRuleCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleCreateError;
export declare function InventoryRuleCreateErrorToJSON(value?: InventoryRuleCreateError | null): any;
