/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationServiceTier, OrganizationStatus, OrganizationUpdateAddress } from './';
/**
 *
 * @export
 * @interface OrganizationUpdateData
 */
export interface OrganizationUpdateData {
    /**
     * Organization Name
     * @type {string}
     * @memberof OrganizationUpdateData
     */
    name?: string;
    /**
     *
     * @type {OrganizationServiceTier}
     * @memberof OrganizationUpdateData
     */
    tier?: OrganizationServiceTier;
    /**
     * Organization Contact email
     * @type {string}
     * @memberof OrganizationUpdateData
     */
    email?: string;
    /**
     * Organization Contact Phone
     * @type {string}
     * @memberof OrganizationUpdateData
     */
    phone?: string;
    /**
     * Organization Time Zone
     * @type {string}
     * @memberof OrganizationUpdateData
     */
    timeZone?: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof OrganizationUpdateData
     */
    logoUrl?: string;
    /**
     *
     * @type {OrganizationUpdateAddress}
     * @memberof OrganizationUpdateData
     */
    address?: OrganizationUpdateAddress;
    /**
     *
     * @type {OrganizationStatus}
     * @memberof OrganizationUpdateData
     */
    status?: OrganizationStatus;
}
export declare function OrganizationUpdateDataFromJSON(json: any): OrganizationUpdateData;
export declare function OrganizationUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationUpdateData;
export declare function OrganizationUpdateDataToJSON(value?: OrganizationUpdateData | null): any;
