"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleCreateErrorToJSON = exports.RoleCreateErrorFromJSONTyped = exports.RoleCreateErrorFromJSON = exports.RoleCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoleCreateErrorSuccessEnum;
(function (RoleCreateErrorSuccessEnum) {
    RoleCreateErrorSuccessEnum["False"] = "false";
})(RoleCreateErrorSuccessEnum = exports.RoleCreateErrorSuccessEnum || (exports.RoleCreateErrorSuccessEnum = {}));
function RoleCreateErrorFromJSON(json) {
    return RoleCreateErrorFromJSONTyped(json, false);
}
exports.RoleCreateErrorFromJSON = RoleCreateErrorFromJSON;
function RoleCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleCreateDataFromJSON(json['role']),
    };
}
exports.RoleCreateErrorFromJSONTyped = RoleCreateErrorFromJSONTyped;
function RoleCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'role': _1.RoleCreateDataToJSON(value.role),
    };
}
exports.RoleCreateErrorToJSON = RoleCreateErrorToJSON;
