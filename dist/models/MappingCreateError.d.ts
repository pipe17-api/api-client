/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingCreateData } from './';
/**
 *
 * @export
 * @interface MappingCreateError
 */
export interface MappingCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof MappingCreateError
     */
    success: MappingCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof MappingCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof MappingCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {MappingCreateData}
     * @memberof MappingCreateError
     */
    mapping?: MappingCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingCreateErrorSuccessEnum {
    False = "false"
}
export declare function MappingCreateErrorFromJSON(json: any): MappingCreateError;
export declare function MappingCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingCreateError;
export declare function MappingCreateErrorToJSON(value?: MappingCreateError | null): any;
