"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsListResponseToJSON = exports.ShipmentsListResponseFromJSONTyped = exports.ShipmentsListResponseFromJSON = exports.ShipmentsListResponseCodeEnum = exports.ShipmentsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentsListResponseSuccessEnum;
(function (ShipmentsListResponseSuccessEnum) {
    ShipmentsListResponseSuccessEnum["True"] = "true";
})(ShipmentsListResponseSuccessEnum = exports.ShipmentsListResponseSuccessEnum || (exports.ShipmentsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ShipmentsListResponseCodeEnum;
(function (ShipmentsListResponseCodeEnum) {
    ShipmentsListResponseCodeEnum[ShipmentsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ShipmentsListResponseCodeEnum[ShipmentsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ShipmentsListResponseCodeEnum[ShipmentsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ShipmentsListResponseCodeEnum = exports.ShipmentsListResponseCodeEnum || (exports.ShipmentsListResponseCodeEnum = {}));
function ShipmentsListResponseFromJSON(json) {
    return ShipmentsListResponseFromJSONTyped(json, false);
}
exports.ShipmentsListResponseFromJSON = ShipmentsListResponseFromJSON;
function ShipmentsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ShipmentsFilterFromJSON(json['filters']),
        'shipments': (json['shipments'].map(_1.ShipmentFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ShipmentsListResponseFromJSONTyped = ShipmentsListResponseFromJSONTyped;
function ShipmentsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ShipmentsFilterToJSON(value.filters),
        'shipments': (value.shipments.map(_1.ShipmentToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ShipmentsListResponseToJSON = ShipmentsListResponseToJSON;
