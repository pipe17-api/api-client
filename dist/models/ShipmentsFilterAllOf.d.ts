/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentStatus } from './';
/**
 * Shipments Filter
 * @export
 * @interface ShipmentsFilterAllOf
 */
export interface ShipmentsFilterAllOf {
    /**
     * Shipments by list of shipmentId
     * @type {Array<string>}
     * @memberof ShipmentsFilterAllOf
     */
    shipmentId?: Array<string>;
    /**
     * Shipments by list of orderId
     * @type {Array<string>}
     * @memberof ShipmentsFilterAllOf
     */
    orderId?: Array<string>;
    /**
     * Shipments by list of order types
     * @type {Array<string>}
     * @memberof ShipmentsFilterAllOf
     */
    orderType?: Array<string>;
    /**
     * Shipments by list of extOrderId
     * @type {Array<string>}
     * @memberof ShipmentsFilterAllOf
     */
    extOrderId?: Array<string>;
    /**
     * Shipments by list of statuses
     * @type {Array<ShipmentStatus>}
     * @memberof ShipmentsFilterAllOf
     */
    status?: Array<ShipmentStatus>;
    /**
     * Soft deleted shipments
     * @type {boolean}
     * @memberof ShipmentsFilterAllOf
     */
    deleted?: boolean;
}
export declare function ShipmentsFilterAllOfFromJSON(json: any): ShipmentsFilterAllOf;
export declare function ShipmentsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsFilterAllOf;
export declare function ShipmentsFilterAllOfToJSON(value?: ShipmentsFilterAllOf | null): any;
