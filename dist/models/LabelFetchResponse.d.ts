/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label } from './';
/**
 *
 * @export
 * @interface LabelFetchResponse
 */
export interface LabelFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LabelFetchResponse
     */
    success: LabelFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelFetchResponse
     */
    code: LabelFetchResponseCodeEnum;
    /**
     *
     * @type {Label}
     * @memberof LabelFetchResponse
     */
    label?: Label;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LabelFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LabelFetchResponseFromJSON(json: any): LabelFetchResponse;
export declare function LabelFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelFetchResponse;
export declare function LabelFetchResponseToJSON(value?: LabelFetchResponse | null): any;
