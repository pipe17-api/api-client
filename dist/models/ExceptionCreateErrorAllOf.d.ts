/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionCreateData } from './';
/**
 *
 * @export
 * @interface ExceptionCreateErrorAllOf
 */
export interface ExceptionCreateErrorAllOf {
    /**
     *
     * @type {ExceptionCreateData}
     * @memberof ExceptionCreateErrorAllOf
     */
    exception?: ExceptionCreateData;
}
export declare function ExceptionCreateErrorAllOfFromJSON(json: any): ExceptionCreateErrorAllOf;
export declare function ExceptionCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionCreateErrorAllOf;
export declare function ExceptionCreateErrorAllOfToJSON(value?: ExceptionCreateErrorAllOf | null): any;
