/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoleFetchError
 */
export interface RoleFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoleFetchError
     */
    success: RoleFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoleFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoleFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoleFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum RoleFetchErrorSuccessEnum {
    False = "false"
}
export declare function RoleFetchErrorFromJSON(json: any): RoleFetchError;
export declare function RoleFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleFetchError;
export declare function RoleFetchErrorToJSON(value?: RoleFetchError | null): any;
