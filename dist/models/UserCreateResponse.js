"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserCreateResponseToJSON = exports.UserCreateResponseFromJSONTyped = exports.UserCreateResponseFromJSON = exports.UserCreateResponseCodeEnum = exports.UserCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UserCreateResponseSuccessEnum;
(function (UserCreateResponseSuccessEnum) {
    UserCreateResponseSuccessEnum["True"] = "true";
})(UserCreateResponseSuccessEnum = exports.UserCreateResponseSuccessEnum || (exports.UserCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var UserCreateResponseCodeEnum;
(function (UserCreateResponseCodeEnum) {
    UserCreateResponseCodeEnum[UserCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    UserCreateResponseCodeEnum[UserCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    UserCreateResponseCodeEnum[UserCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(UserCreateResponseCodeEnum = exports.UserCreateResponseCodeEnum || (exports.UserCreateResponseCodeEnum = {}));
function UserCreateResponseFromJSON(json) {
    return UserCreateResponseFromJSONTyped(json, false);
}
exports.UserCreateResponseFromJSON = UserCreateResponseFromJSON;
function UserCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserFromJSON(json['user']),
    };
}
exports.UserCreateResponseFromJSONTyped = UserCreateResponseFromJSONTyped;
function UserCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'user': _1.UserToJSON(value.user),
    };
}
exports.UserCreateResponseToJSON = UserCreateResponseToJSON;
