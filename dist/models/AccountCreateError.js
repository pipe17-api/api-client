"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountCreateErrorToJSON = exports.AccountCreateErrorFromJSONTyped = exports.AccountCreateErrorFromJSON = exports.AccountCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var AccountCreateErrorSuccessEnum;
(function (AccountCreateErrorSuccessEnum) {
    AccountCreateErrorSuccessEnum["False"] = "false";
})(AccountCreateErrorSuccessEnum = exports.AccountCreateErrorSuccessEnum || (exports.AccountCreateErrorSuccessEnum = {}));
function AccountCreateErrorFromJSON(json) {
    return AccountCreateErrorFromJSONTyped(json, false);
}
exports.AccountCreateErrorFromJSON = AccountCreateErrorFromJSON;
function AccountCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountCreateDataFromJSON(json['account']),
    };
}
exports.AccountCreateErrorFromJSONTyped = AccountCreateErrorFromJSONTyped;
function AccountCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'account': _1.AccountCreateDataToJSON(value.account),
    };
}
exports.AccountCreateErrorToJSON = AccountCreateErrorToJSON;
