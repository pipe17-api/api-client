"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationUpdateResponseToJSON = exports.OrganizationUpdateResponseFromJSONTyped = exports.OrganizationUpdateResponseFromJSON = exports.OrganizationUpdateResponseCodeEnum = exports.OrganizationUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var OrganizationUpdateResponseSuccessEnum;
(function (OrganizationUpdateResponseSuccessEnum) {
    OrganizationUpdateResponseSuccessEnum["True"] = "true";
})(OrganizationUpdateResponseSuccessEnum = exports.OrganizationUpdateResponseSuccessEnum || (exports.OrganizationUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrganizationUpdateResponseCodeEnum;
(function (OrganizationUpdateResponseCodeEnum) {
    OrganizationUpdateResponseCodeEnum[OrganizationUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrganizationUpdateResponseCodeEnum[OrganizationUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrganizationUpdateResponseCodeEnum[OrganizationUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrganizationUpdateResponseCodeEnum = exports.OrganizationUpdateResponseCodeEnum || (exports.OrganizationUpdateResponseCodeEnum = {}));
function OrganizationUpdateResponseFromJSON(json) {
    return OrganizationUpdateResponseFromJSONTyped(json, false);
}
exports.OrganizationUpdateResponseFromJSON = OrganizationUpdateResponseFromJSON;
function OrganizationUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.OrganizationUpdateResponseFromJSONTyped = OrganizationUpdateResponseFromJSONTyped;
function OrganizationUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.OrganizationUpdateResponseToJSON = OrganizationUpdateResponseToJSON;
