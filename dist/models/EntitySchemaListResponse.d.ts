/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchema, EntitySchemasListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface EntitySchemaListResponse
 */
export interface EntitySchemaListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntitySchemaListResponse
     */
    success: EntitySchemaListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaListResponse
     */
    code: EntitySchemaListResponseCodeEnum;
    /**
     *
     * @type {EntitySchemasListFilter}
     * @memberof EntitySchemaListResponse
     */
    filters?: EntitySchemasListFilter;
    /**
     *
     * @type {Array<EntitySchema>}
     * @memberof EntitySchemaListResponse
     */
    entitySchemas?: Array<EntitySchema>;
    /**
     *
     * @type {Pagination}
     * @memberof EntitySchemaListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntitySchemaListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntitySchemaListResponseFromJSON(json: any): EntitySchemaListResponse;
export declare function EntitySchemaListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaListResponse;
export declare function EntitySchemaListResponseToJSON(value?: EntitySchemaListResponse | null): any;
