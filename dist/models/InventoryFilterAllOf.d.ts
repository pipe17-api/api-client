/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Inventory Items Filter
 * @export
 * @interface InventoryFilterAllOf
 */
export interface InventoryFilterAllOf {
    /**
     * Inventory items by list of inventoryId
     * @type {Array<string>}
     * @memberof InventoryFilterAllOf
     */
    inventoryId?: Array<string>;
    /**
     * Return inventory items related to certain event record by record id
     * @type {Array<string>}
     * @memberof InventoryFilterAllOf
     */
    entityId?: Array<string>;
    /**
     * Fetch inventory by its SKU
     * @type {Array<string>}
     * @memberof InventoryFilterAllOf
     */
    sku?: Array<string>;
    /**
     * Fetch inventory by its location Id
     * @type {Array<string>}
     * @memberof InventoryFilterAllOf
     */
    locationId?: Array<string>;
    /**
     * Fetch soft deleted inventory items
     * @type {boolean}
     * @memberof InventoryFilterAllOf
     */
    deleted?: boolean;
}
export declare function InventoryFilterAllOfFromJSON(json: any): InventoryFilterAllOf;
export declare function InventoryFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryFilterAllOf;
export declare function InventoryFilterAllOfToJSON(value?: InventoryFilterAllOf | null): any;
