"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesFilterAllOfToJSON = exports.RolesFilterAllOfFromJSONTyped = exports.RolesFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RolesFilterAllOfFromJSON(json) {
    return RolesFilterAllOfFromJSONTyped(json, false);
}
exports.RolesFilterAllOfFromJSON = RolesFilterAllOfFromJSON;
function RolesFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'roleId': !runtime_1.exists(json, 'roleId') ? undefined : json['roleId'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.RoleStatusFromJSON)),
    };
}
exports.RolesFilterAllOfFromJSONTyped = RolesFilterAllOfFromJSONTyped;
function RolesFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'roleId': value.roleId,
        'name': value.name,
        'status': value.status === undefined ? undefined : (value.status.map(_1.RoleStatusToJSON)),
    };
}
exports.RolesFilterAllOfToJSON = RolesFilterAllOfToJSON;
