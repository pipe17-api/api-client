/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job, JobsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface JobsListResponseAllOf
 */
export interface JobsListResponseAllOf {
    /**
     *
     * @type {JobsListFilter}
     * @memberof JobsListResponseAllOf
     */
    filters?: JobsListFilter;
    /**
     *
     * @type {Array<Job>}
     * @memberof JobsListResponseAllOf
     */
    jobs?: Array<Job>;
    /**
     *
     * @type {Pagination}
     * @memberof JobsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function JobsListResponseAllOfFromJSON(json: any): JobsListResponseAllOf;
export declare function JobsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsListResponseAllOf;
export declare function JobsListResponseAllOfToJSON(value?: JobsListResponseAllOf | null): any;
