"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobTypeToJSON = exports.JobTypeFromJSONTyped = exports.JobTypeFromJSON = exports.JobType = void 0;
/**
 * Job type
 * @export
 * @enum {string}
 */
var JobType;
(function (JobType) {
    JobType["Report"] = "report";
    JobType["Import"] = "import";
})(JobType = exports.JobType || (exports.JobType = {}));
function JobTypeFromJSON(json) {
    return JobTypeFromJSONTyped(json, false);
}
exports.JobTypeFromJSON = JobTypeFromJSON;
function JobTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.JobTypeFromJSONTyped = JobTypeFromJSONTyped;
function JobTypeToJSON(value) {
    return value;
}
exports.JobTypeToJSON = JobTypeToJSON;
