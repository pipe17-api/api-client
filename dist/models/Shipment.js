"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentToJSON = exports.ShipmentFromJSONTyped = exports.ShipmentFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentFromJSON(json) {
    return ShipmentFromJSONTyped(json, false);
}
exports.ShipmentFromJSON = ShipmentFromJSON;
function ShipmentFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ShipmentCreateStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'sentToFulfillmentAt': !runtime_1.exists(json, 'sentToFulfillmentAt') ? undefined : (new Date(json['sentToFulfillmentAt'])),
        'fulfillmentIntegrationId': !runtime_1.exists(json, 'fulfillmentIntegrationId') ? undefined : json['fulfillmentIntegrationId'],
        'extOrderId': json['extOrderId'],
        'extShipmentId': !runtime_1.exists(json, 'extShipmentId') ? undefined : json['extShipmentId'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'orderSource': !runtime_1.exists(json, 'orderSource') ? undefined : json['orderSource'],
        'orderType': !runtime_1.exists(json, 'orderType') ? undefined : _1.ShipmentOrderTypeFromJSON(json['orderType']),
        'orderCreateTime': !runtime_1.exists(json, 'orderCreateTime') ? undefined : (new Date(json['orderCreateTime'])),
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (new Date(json['shipByDate'])),
        'expectedShipDate': !runtime_1.exists(json, 'expectedShipDate') ? undefined : (new Date(json['expectedShipDate'])),
        'expectedDeliveryDate': !runtime_1.exists(json, 'expectedDeliveryDate') ? undefined : (new Date(json['expectedDeliveryDate'])),
        'shippingAddress': _1.AddressFromJSON(json['shippingAddress']),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingCode': !runtime_1.exists(json, 'shippingCode') ? undefined : json['shippingCode'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shippingNote': !runtime_1.exists(json, 'shippingNote') ? undefined : json['shippingNote'],
        'shippingLabels': !runtime_1.exists(json, 'shippingLabels') ? undefined : json['shippingLabels'],
        'giftNote': !runtime_1.exists(json, 'giftNote') ? undefined : json['giftNote'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ShipmentLineItemFromJSON)),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.ShipmentFromJSONTyped = ShipmentFromJSONTyped;
function ShipmentToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipmentId': value.shipmentId,
        'status': _1.ShipmentCreateStatusToJSON(value.status),
        'integration': value.integration,
        'sentToFulfillmentAt': value.sentToFulfillmentAt === undefined ? undefined : (new Date(value.sentToFulfillmentAt).toISOString()),
        'fulfillmentIntegrationId': value.fulfillmentIntegrationId,
        'extOrderId': value.extOrderId,
        'extShipmentId': value.extShipmentId,
        'orderId': value.orderId,
        'orderSource': value.orderSource,
        'orderType': _1.ShipmentOrderTypeToJSON(value.orderType),
        'orderCreateTime': value.orderCreateTime === undefined ? undefined : (new Date(value.orderCreateTime).toISOString()),
        'shipByDate': value.shipByDate === undefined ? undefined : (new Date(value.shipByDate).toISOString()),
        'expectedShipDate': value.expectedShipDate === undefined ? undefined : (new Date(value.expectedShipDate).toISOString()),
        'expectedDeliveryDate': value.expectedDeliveryDate === undefined ? undefined : (new Date(value.expectedDeliveryDate).toISOString()),
        'shippingAddress': _1.AddressToJSON(value.shippingAddress),
        'shippingCarrier': value.shippingCarrier,
        'shippingCode': value.shippingCode,
        'shippingClass': value.shippingClass,
        'shippingNote': value.shippingNote,
        'shippingLabels': value.shippingLabels,
        'giftNote': value.giftNote,
        'incoterms': value.incoterms,
        'locationId': value.locationId,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ShipmentLineItemToJSON)),
    };
}
exports.ShipmentToJSON = ShipmentToJSON;
