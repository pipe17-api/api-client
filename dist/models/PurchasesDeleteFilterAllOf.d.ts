/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Purchases Filter
 * @export
 * @interface PurchasesDeleteFilterAllOf
 */
export interface PurchasesDeleteFilterAllOf {
    /**
     * Purchases of these integrations
     * @type {Array<string>}
     * @memberof PurchasesDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function PurchasesDeleteFilterAllOfFromJSON(json: any): PurchasesDeleteFilterAllOf;
export declare function PurchasesDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesDeleteFilterAllOf;
export declare function PurchasesDeleteFilterAllOfToJSON(value?: PurchasesDeleteFilterAllOf | null): any;
