/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Role } from './';
/**
 *
 * @export
 * @interface RoleCreateResponseAllOf
 */
export interface RoleCreateResponseAllOf {
    /**
     *
     * @type {Role}
     * @memberof RoleCreateResponseAllOf
     */
    role?: Role;
}
export declare function RoleCreateResponseAllOfFromJSON(json: any): RoleCreateResponseAllOf;
export declare function RoleCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleCreateResponseAllOf;
export declare function RoleCreateResponseAllOfToJSON(value?: RoleCreateResponseAllOf | null): any;
