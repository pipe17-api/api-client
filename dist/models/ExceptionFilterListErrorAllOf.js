"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterListErrorAllOfToJSON = exports.ExceptionFilterListErrorAllOfFromJSONTyped = exports.ExceptionFilterListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFilterListErrorAllOfFromJSON(json) {
    return ExceptionFilterListErrorAllOfFromJSONTyped(json, false);
}
exports.ExceptionFilterListErrorAllOfFromJSON = ExceptionFilterListErrorAllOfFromJSON;
function ExceptionFilterListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ExceptionFiltersListFilterFromJSON(json['filters']),
    };
}
exports.ExceptionFilterListErrorAllOfFromJSONTyped = ExceptionFilterListErrorAllOfFromJSONTyped;
function ExceptionFilterListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ExceptionFiltersListFilterToJSON(value.filters),
    };
}
exports.ExceptionFilterListErrorAllOfToJSON = ExceptionFilterListErrorAllOfToJSON;
