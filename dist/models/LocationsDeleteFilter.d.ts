/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationStatus } from './';
/**
 *
 * @export
 * @interface LocationsDeleteFilter
 */
export interface LocationsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof LocationsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof LocationsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof LocationsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof LocationsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof LocationsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof LocationsDeleteFilter
     */
    count?: number;
    /**
     * Locations by list of locationId
     * @type {Array<string>}
     * @memberof LocationsDeleteFilter
     */
    locationId?: Array<string>;
    /**
     *
     * @type {Array<LocationStatus>}
     * @memberof LocationsDeleteFilter
     */
    status?: Array<LocationStatus>;
    /**
     * Soft deleted locations
     * @type {boolean}
     * @memberof LocationsDeleteFilter
     */
    deleted?: boolean;
    /**
     * Locations of these integrations
     * @type {Array<string>}
     * @memberof LocationsDeleteFilter
     */
    integration?: Array<string>;
}
export declare function LocationsDeleteFilterFromJSON(json: any): LocationsDeleteFilter;
export declare function LocationsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsDeleteFilter;
export declare function LocationsDeleteFilterToJSON(value?: LocationsDeleteFilter | null): any;
