"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyCreateResponseToJSON = exports.ApiKeyCreateResponseFromJSONTyped = exports.ApiKeyCreateResponseFromJSON = exports.ApiKeyCreateResponseCodeEnum = exports.ApiKeyCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ApiKeyCreateResponseSuccessEnum;
(function (ApiKeyCreateResponseSuccessEnum) {
    ApiKeyCreateResponseSuccessEnum["True"] = "true";
})(ApiKeyCreateResponseSuccessEnum = exports.ApiKeyCreateResponseSuccessEnum || (exports.ApiKeyCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ApiKeyCreateResponseCodeEnum;
(function (ApiKeyCreateResponseCodeEnum) {
    ApiKeyCreateResponseCodeEnum[ApiKeyCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ApiKeyCreateResponseCodeEnum[ApiKeyCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ApiKeyCreateResponseCodeEnum[ApiKeyCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ApiKeyCreateResponseCodeEnum = exports.ApiKeyCreateResponseCodeEnum || (exports.ApiKeyCreateResponseCodeEnum = {}));
function ApiKeyCreateResponseFromJSON(json) {
    return ApiKeyCreateResponseFromJSONTyped(json, false);
}
exports.ApiKeyCreateResponseFromJSON = ApiKeyCreateResponseFromJSON;
function ApiKeyCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'apikey': json['apikey'],
        'apikeyId': !runtime_1.exists(json, 'apikeyId') ? undefined : json['apikeyId'],
    };
}
exports.ApiKeyCreateResponseFromJSONTyped = ApiKeyCreateResponseFromJSONTyped;
function ApiKeyCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'apikey': value.apikey,
        'apikeyId': value.apikeyId,
    };
}
exports.ApiKeyCreateResponseToJSON = ApiKeyCreateResponseToJSON;
