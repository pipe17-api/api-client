"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationFetchResponseToJSON = exports.LocationFetchResponseFromJSONTyped = exports.LocationFetchResponseFromJSON = exports.LocationFetchResponseCodeEnum = exports.LocationFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationFetchResponseSuccessEnum;
(function (LocationFetchResponseSuccessEnum) {
    LocationFetchResponseSuccessEnum["True"] = "true";
})(LocationFetchResponseSuccessEnum = exports.LocationFetchResponseSuccessEnum || (exports.LocationFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LocationFetchResponseCodeEnum;
(function (LocationFetchResponseCodeEnum) {
    LocationFetchResponseCodeEnum[LocationFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LocationFetchResponseCodeEnum[LocationFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LocationFetchResponseCodeEnum[LocationFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LocationFetchResponseCodeEnum = exports.LocationFetchResponseCodeEnum || (exports.LocationFetchResponseCodeEnum = {}));
function LocationFetchResponseFromJSON(json) {
    return LocationFetchResponseFromJSONTyped(json, false);
}
exports.LocationFetchResponseFromJSON = LocationFetchResponseFromJSON;
function LocationFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationFromJSON(json['location']),
    };
}
exports.LocationFetchResponseFromJSONTyped = LocationFetchResponseFromJSONTyped;
function LocationFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'location': _1.LocationToJSON(value.location),
    };
}
exports.LocationFetchResponseToJSON = LocationFetchResponseToJSON;
