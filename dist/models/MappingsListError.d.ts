/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingsListFilter } from './';
/**
 *
 * @export
 * @interface MappingsListError
 */
export interface MappingsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof MappingsListError
     */
    success: MappingsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof MappingsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof MappingsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {MappingsListFilter}
     * @memberof MappingsListError
     */
    filters?: MappingsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingsListErrorSuccessEnum {
    False = "false"
}
export declare function MappingsListErrorFromJSON(json: any): MappingsListError;
export declare function MappingsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsListError;
export declare function MappingsListErrorToJSON(value?: MappingsListError | null): any;
