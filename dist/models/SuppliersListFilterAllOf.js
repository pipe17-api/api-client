"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SuppliersListFilterAllOfToJSON = exports.SuppliersListFilterAllOfFromJSONTyped = exports.SuppliersListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function SuppliersListFilterAllOfFromJSON(json) {
    return SuppliersListFilterAllOfFromJSONTyped(json, false);
}
exports.SuppliersListFilterAllOfFromJSON = SuppliersListFilterAllOfFromJSON;
function SuppliersListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
    };
}
exports.SuppliersListFilterAllOfFromJSONTyped = SuppliersListFilterAllOfFromJSONTyped;
function SuppliersListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
    };
}
exports.SuppliersListFilterAllOfToJSON = SuppliersListFilterAllOfToJSON;
