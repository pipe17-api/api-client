"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobSubTypeToJSON = exports.JobSubTypeFromJSONTyped = exports.JobSubTypeFromJSON = exports.JobSubType = void 0;
/**
 * Job sub-type
 * @export
 * @enum {string}
 */
var JobSubType;
(function (JobSubType) {
    JobSubType["Inventory"] = "inventory";
    JobSubType["Replenishment"] = "replenishment";
    JobSubType["Purchases"] = "purchases";
    JobSubType["Transfers"] = "transfers";
    JobSubType["Arrivals"] = "arrivals";
    JobSubType["Products"] = "products";
})(JobSubType = exports.JobSubType || (exports.JobSubType = {}));
function JobSubTypeFromJSON(json) {
    return JobSubTypeFromJSONTyped(json, false);
}
exports.JobSubTypeFromJSON = JobSubTypeFromJSON;
function JobSubTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.JobSubTypeFromJSONTyped = JobSubTypeFromJSONTyped;
function JobSubTypeToJSON(value) {
    return value;
}
exports.JobSubTypeToJSON = JobSubTypeToJSON;
