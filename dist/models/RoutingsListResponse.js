"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsListResponseToJSON = exports.RoutingsListResponseFromJSONTyped = exports.RoutingsListResponseFromJSON = exports.RoutingsListResponseCodeEnum = exports.RoutingsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoutingsListResponseSuccessEnum;
(function (RoutingsListResponseSuccessEnum) {
    RoutingsListResponseSuccessEnum["True"] = "true";
})(RoutingsListResponseSuccessEnum = exports.RoutingsListResponseSuccessEnum || (exports.RoutingsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoutingsListResponseCodeEnum;
(function (RoutingsListResponseCodeEnum) {
    RoutingsListResponseCodeEnum[RoutingsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoutingsListResponseCodeEnum[RoutingsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoutingsListResponseCodeEnum[RoutingsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoutingsListResponseCodeEnum = exports.RoutingsListResponseCodeEnum || (exports.RoutingsListResponseCodeEnum = {}));
function RoutingsListResponseFromJSON(json) {
    return RoutingsListResponseFromJSONTyped(json, false);
}
exports.RoutingsListResponseFromJSON = RoutingsListResponseFromJSON;
function RoutingsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RoutingsListFilterFromJSON(json['filters']),
        'routings': !runtime_1.exists(json, 'routings') ? undefined : (json['routings'].map(_1.RoutingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RoutingsListResponseFromJSONTyped = RoutingsListResponseFromJSONTyped;
function RoutingsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.RoutingsListFilterToJSON(value.filters),
        'routings': value.routings === undefined ? undefined : (value.routings.map(_1.RoutingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RoutingsListResponseToJSON = RoutingsListResponseToJSON;
