"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFetchResponseToJSON = exports.ExceptionFetchResponseFromJSONTyped = exports.ExceptionFetchResponseFromJSON = exports.ExceptionFetchResponseCodeEnum = exports.ExceptionFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionFetchResponseSuccessEnum;
(function (ExceptionFetchResponseSuccessEnum) {
    ExceptionFetchResponseSuccessEnum["True"] = "true";
})(ExceptionFetchResponseSuccessEnum = exports.ExceptionFetchResponseSuccessEnum || (exports.ExceptionFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionFetchResponseCodeEnum;
(function (ExceptionFetchResponseCodeEnum) {
    ExceptionFetchResponseCodeEnum[ExceptionFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionFetchResponseCodeEnum[ExceptionFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionFetchResponseCodeEnum[ExceptionFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionFetchResponseCodeEnum = exports.ExceptionFetchResponseCodeEnum || (exports.ExceptionFetchResponseCodeEnum = {}));
function ExceptionFetchResponseFromJSON(json) {
    return ExceptionFetchResponseFromJSONTyped(json, false);
}
exports.ExceptionFetchResponseFromJSON = ExceptionFetchResponseFromJSON;
function ExceptionFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionFromJSON(json['exception']),
    };
}
exports.ExceptionFetchResponseFromJSONTyped = ExceptionFetchResponseFromJSONTyped;
function ExceptionFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'exception': _1.ExceptionToJSON(value.exception),
    };
}
exports.ExceptionFetchResponseToJSON = ExceptionFetchResponseToJSON;
