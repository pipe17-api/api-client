/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingUpdateRule } from './';
/**
 *
 * @export
 * @interface MappingUpdateRequest
 */
export interface MappingUpdateRequest {
    /**
     * Public mapping indicator
     * @type {boolean}
     * @memberof MappingUpdateRequest
     */
    isPublic?: boolean;
    /**
     * Description
     * @type {string}
     * @memberof MappingUpdateRequest
     */
    description?: string;
    /**
     *
     * @type {Array<MappingUpdateRule>}
     * @memberof MappingUpdateRequest
     */
    mappings?: Array<MappingUpdateRule>;
    /**
     *
     * @type {string}
     * @memberof MappingUpdateRequest
     */
    originId?: string;
}
export declare function MappingUpdateRequestFromJSON(json: any): MappingUpdateRequest;
export declare function MappingUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingUpdateRequest;
export declare function MappingUpdateRequestToJSON(value?: MappingUpdateRequest | null): any;
