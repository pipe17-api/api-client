/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Account } from './';
/**
 *
 * @export
 * @interface AccountCreateResponse
 */
export interface AccountCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof AccountCreateResponse
     */
    success: AccountCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountCreateResponse
     */
    code: AccountCreateResponseCodeEnum;
    /**
     *
     * @type {Account}
     * @memberof AccountCreateResponse
     */
    account?: Account;
    /**
     * Account Activation key
     * @type {string}
     * @memberof AccountCreateResponse
     */
    activationKey?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum AccountCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function AccountCreateResponseFromJSON(json: any): AccountCreateResponse;
export declare function AccountCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountCreateResponse;
export declare function AccountCreateResponseToJSON(value?: AccountCreateResponse | null): any;
