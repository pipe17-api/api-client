/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Product, ProductsListFilter } from './';
/**
 *
 * @export
 * @interface ProducstListResponse
 */
export interface ProducstListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ProducstListResponse
     */
    success: ProducstListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProducstListResponse
     */
    code: ProducstListResponseCodeEnum;
    /**
     *
     * @type {ProductsListFilter}
     * @memberof ProducstListResponse
     */
    filters?: ProductsListFilter;
    /**
     *
     * @type {Array<Product>}
     * @memberof ProducstListResponse
     */
    products: Array<Product>;
    /**
     *
     * @type {Pagination}
     * @memberof ProducstListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ProducstListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ProducstListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ProducstListResponseFromJSON(json: any): ProducstListResponse;
export declare function ProducstListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProducstListResponse;
export declare function ProducstListResponseToJSON(value?: ProducstListResponse | null): any;
