"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingUpdateErrorAllOfToJSON = exports.MappingUpdateErrorAllOfFromJSONTyped = exports.MappingUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingUpdateErrorAllOfFromJSON(json) {
    return MappingUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.MappingUpdateErrorAllOfFromJSON = MappingUpdateErrorAllOfFromJSON;
function MappingUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingUpdateDataFromJSON(json['mapping']),
    };
}
exports.MappingUpdateErrorAllOfFromJSONTyped = MappingUpdateErrorAllOfFromJSONTyped;
function MappingUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mapping': _1.MappingUpdateDataToJSON(value.mapping),
    };
}
exports.MappingUpdateErrorAllOfToJSON = MappingUpdateErrorAllOfToJSON;
