/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Organization, OrganizationsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrganizationsListResponseAllOf
 */
export interface OrganizationsListResponseAllOf {
    /**
     *
     * @type {OrganizationsListFilter}
     * @memberof OrganizationsListResponseAllOf
     */
    filters?: OrganizationsListFilter;
    /**
     *
     * @type {Array<Organization>}
     * @memberof OrganizationsListResponseAllOf
     */
    organizations?: Array<Organization>;
    /**
     *
     * @type {Pagination}
     * @memberof OrganizationsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function OrganizationsListResponseAllOfFromJSON(json: any): OrganizationsListResponseAllOf;
export declare function OrganizationsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsListResponseAllOf;
export declare function OrganizationsListResponseAllOfToJSON(value?: OrganizationsListResponseAllOf | null): any;
