/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Exception type
 * @export
 * @enum {string}
 */
export declare enum ExceptionType {
    EGeneric = "eGeneric",
    ESyncFailure = "eSyncFailure",
    EUnacknowledged = "eUnacknowledged",
    EDelayed = "eDelayed",
    ELate = "eLate",
    EEarly = "eEarly",
    ELost = "eLost",
    ESplit = "eSplit",
    EStuck = "eStuck",
    ELarge = "eLarge",
    ELow = "eLow",
    EMissing = "eMissing",
    EMissingSku = "eMissingSKU",
    EMissingLocation = "eMissingLocation",
    EMismatch = "eMismatch",
    EMismatchTax = "eMismatchTax",
    EInvalid = "eInvalid",
    EInvalidAddress = "eInvalidAddress",
    EInvalidDate = "eInvalidDate",
    EInvalidLocation = "eInvalidLocation",
    EInvalidSku = "eInvalidSKU",
    EIncomplete = "eIncomplete",
    EIncompleteAddress = "eIncompleteAddress",
    EDuplicate = "eDuplicate",
    EDuplicateSku = "eDuplicateSKU",
    EUnexpected = "eUnexpected",
    EUnexpectedDate = "eUnexpectedDate",
    ERestock = "eRestock",
    EOutOfStock = "eOutOfStock",
    EFraudSuspected = "eFraudSuspected",
    EApprovalRequired = "eApprovalRequired",
    EPartiallyCanceled = "ePartiallyCanceled",
    ERouteNotFound = "eRouteNotFound",
    ERouteOutOfStock = "eRouteOutOfStock",
    ECreditMemoInsufficient = "eCreditMemoInsufficient",
    EOrderSyncFailure = "eOrderSyncFailure",
    EOrderTaxMismatch = "eOrderTaxMismatch",
    EInventoryOutOfStock = "eInventoryOutOfStock"
}
export declare function ExceptionTypeFromJSON(json: any): ExceptionType;
export declare function ExceptionTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionType;
export declare function ExceptionTypeToJSON(value?: ExceptionType | null): any;
