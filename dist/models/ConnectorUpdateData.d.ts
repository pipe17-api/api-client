/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorConnection, ConnectorEntity, ConnectorSettings, ConnectorType, ConnectorUpdateDataWebhook } from './';
/**
 *
 * @export
 * @interface ConnectorUpdateData
 */
export interface ConnectorUpdateData {
    /**
     * Connector description
     * @type {string}
     * @memberof ConnectorUpdateData
     */
    description?: string;
    /**
     * Connector readme markup
     * @type {string}
     * @memberof ConnectorUpdateData
     */
    readme?: string;
    /**
     * Connector version
     * @type {string}
     * @memberof ConnectorUpdateData
     */
    version?: string;
    /**
     *
     * @type {ConnectorType}
     * @memberof ConnectorUpdateData
     */
    connectorType?: ConnectorType;
    /**
     * Connector Status
     * @type {string}
     * @memberof ConnectorUpdateData
     */
    status?: ConnectorUpdateDataStatusEnum;
    /**
     * Logo URL
     * @type {string}
     * @memberof ConnectorUpdateData
     */
    logoUrl?: string | null;
    /**
     *
     * @type {Array<ConnectorEntity>}
     * @memberof ConnectorUpdateData
     */
    entities?: Array<ConnectorEntity>;
    /**
     *
     * @type {ConnectorSettings}
     * @memberof ConnectorUpdateData
     */
    settings?: ConnectorSettings;
    /**
     *
     * @type {ConnectorConnection}
     * @memberof ConnectorUpdateData
     */
    connection?: ConnectorConnection;
    /**
     *
     * @type {ConnectorUpdateDataWebhook}
     * @memberof ConnectorUpdateData
     */
    webhook?: ConnectorUpdateDataWebhook | null;
    /**
     * Indicates whether connector/integration should have access to all declared entity types
     * @type {boolean}
     * @memberof ConnectorUpdateData
     */
    orgWideAccess?: boolean;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof ConnectorUpdateData
     */
    orgUnique?: boolean;
    /**
     * Connector Display Name
     * @type {string}
     * @memberof ConnectorUpdateData
     */
    displayName?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorUpdateDataStatusEnum {
    Active = "active",
    Inactive = "inactive"
}
export declare function ConnectorUpdateDataFromJSON(json: any): ConnectorUpdateData;
export declare function ConnectorUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorUpdateData;
export declare function ConnectorUpdateDataToJSON(value?: ConnectorUpdateData | null): any;
