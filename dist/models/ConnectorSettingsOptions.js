"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSettingsOptionsToJSON = exports.ConnectorSettingsOptionsFromJSONTyped = exports.ConnectorSettingsOptionsFromJSON = exports.ConnectorSettingsOptionsGenkeyEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorSettingsOptionsGenkeyEnum;
(function (ConnectorSettingsOptionsGenkeyEnum) {
    ConnectorSettingsOptionsGenkeyEnum["Apikey"] = "apikey";
    ConnectorSettingsOptionsGenkeyEnum["Nonce"] = "nonce";
})(ConnectorSettingsOptionsGenkeyEnum = exports.ConnectorSettingsOptionsGenkeyEnum || (exports.ConnectorSettingsOptionsGenkeyEnum = {}));
function ConnectorSettingsOptionsFromJSON(json) {
    return ConnectorSettingsOptionsFromJSONTyped(json, false);
}
exports.ConnectorSettingsOptionsFromJSON = ConnectorSettingsOptionsFromJSON;
function ConnectorSettingsOptionsFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'label': !runtime_1.exists(json, 'label') ? undefined : json['label'],
        'defaultValue': !runtime_1.exists(json, 'defaultValue') ? undefined : json['defaultValue'],
        'genkey': !runtime_1.exists(json, 'genkey') ? undefined : json['genkey'],
        'group': !runtime_1.exists(json, 'group') ? undefined : json['group'],
        'fields': !runtime_1.exists(json, 'fields') ? undefined : json['fields'],
        'readonly': !runtime_1.exists(json, 'readonly') ? undefined : json['readonly'],
        'hidden': !runtime_1.exists(json, 'hidden') ? undefined : json['hidden'],
        'startAdornment': !runtime_1.exists(json, 'startAdornment') ? undefined : json['startAdornment'],
        'endAdornment': !runtime_1.exists(json, 'endAdornment') ? undefined : json['endAdornment'],
        'helperText': !runtime_1.exists(json, 'helperText') ? undefined : json['helperText'],
        'placeholder': !runtime_1.exists(json, 'placeholder') ? undefined : json['placeholder'],
        'component': !runtime_1.exists(json, 'component') ? undefined : json['component'],
        'values': !runtime_1.exists(json, 'values') ? undefined : (json['values'].map(_1.ConnectorSettingsOptionsValuesFromJSON)),
        'readme': !runtime_1.exists(json, 'readme') ? undefined : json['readme'],
        'extra': !runtime_1.exists(json, 'extra') ? undefined : json['extra'],
    };
}
exports.ConnectorSettingsOptionsFromJSONTyped = ConnectorSettingsOptionsFromJSONTyped;
function ConnectorSettingsOptionsToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'label': value.label,
        'defaultValue': value.defaultValue,
        'genkey': value.genkey,
        'group': value.group,
        'fields': value.fields,
        'readonly': value.readonly,
        'hidden': value.hidden,
        'startAdornment': value.startAdornment,
        'endAdornment': value.endAdornment,
        'helperText': value.helperText,
        'placeholder': value.placeholder,
        'component': value.component,
        'values': value.values === undefined ? undefined : (value.values.map(_1.ConnectorSettingsOptionsValuesToJSON)),
        'readme': value.readme,
        'extra': value.extra,
    };
}
exports.ConnectorSettingsOptionsToJSON = ConnectorSettingsOptionsToJSON;
