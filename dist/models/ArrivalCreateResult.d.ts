/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival } from './';
/**
 *
 * @export
 * @interface ArrivalCreateResult
 */
export interface ArrivalCreateResult {
    /**
     *
     * @type {string}
     * @memberof ArrivalCreateResult
     */
    status?: ArrivalCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof ArrivalCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof ArrivalCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Arrival}
     * @memberof ArrivalCreateResult
     */
    arrival?: Arrival;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function ArrivalCreateResultFromJSON(json: any): ArrivalCreateResult;
export declare function ArrivalCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalCreateResult;
export declare function ArrivalCreateResultToJSON(value?: ArrivalCreateResult | null): any;
