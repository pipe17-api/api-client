/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationExternalSystem, LocationStatus, LocationUpdateAddress } from './';
/**
 *
 * @export
 * @interface LocationUpdateRequest
 */
export interface LocationUpdateRequest {
    /**
     * Location Name
     * @type {string}
     * @memberof LocationUpdateRequest
     */
    name?: string;
    /**
     *
     * @type {LocationUpdateAddress}
     * @memberof LocationUpdateRequest
     */
    address?: LocationUpdateAddress;
    /**
     *
     * @type {LocationStatus}
     * @memberof LocationUpdateRequest
     */
    status?: LocationStatus;
    /**
     * Set if this location has \"infinite\" availability
     * @type {boolean}
     * @memberof LocationUpdateRequest
     */
    infinite?: boolean;
    /**
     * Set if shipments to this location should have lineItems referring to bundles kept intact
     * @type {boolean}
     * @memberof LocationUpdateRequest
     */
    preserveBundles?: boolean;
    /**
     * Set if this is a fulfillment location
     * @type {string}
     * @memberof LocationUpdateRequest
     */
    fulfillmentIntegrationId?: string | null;
    /**
     *
     * @type {Array<LocationExternalSystem>}
     * @memberof LocationUpdateRequest
     */
    externalSystem?: Array<LocationExternalSystem>;
}
export declare function LocationUpdateRequestFromJSON(json: any): LocationUpdateRequest;
export declare function LocationUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationUpdateRequest;
export declare function LocationUpdateRequestToJSON(value?: LocationUpdateRequest | null): any;
