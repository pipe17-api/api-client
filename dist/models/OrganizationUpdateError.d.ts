/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationUpdateData } from './';
/**
 *
 * @export
 * @interface OrganizationUpdateError
 */
export interface OrganizationUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrganizationUpdateError
     */
    success: OrganizationUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrganizationUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrganizationUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrganizationUpdateData}
     * @memberof OrganizationUpdateError
     */
    organization?: OrganizationUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationUpdateErrorSuccessEnum {
    False = "false"
}
export declare function OrganizationUpdateErrorFromJSON(json: any): OrganizationUpdateError;
export declare function OrganizationUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationUpdateError;
export declare function OrganizationUpdateErrorToJSON(value?: OrganizationUpdateError | null): any;
