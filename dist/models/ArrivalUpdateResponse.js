"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalUpdateResponseToJSON = exports.ArrivalUpdateResponseFromJSONTyped = exports.ArrivalUpdateResponseFromJSON = exports.ArrivalUpdateResponseCodeEnum = exports.ArrivalUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ArrivalUpdateResponseSuccessEnum;
(function (ArrivalUpdateResponseSuccessEnum) {
    ArrivalUpdateResponseSuccessEnum["True"] = "true";
})(ArrivalUpdateResponseSuccessEnum = exports.ArrivalUpdateResponseSuccessEnum || (exports.ArrivalUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ArrivalUpdateResponseCodeEnum;
(function (ArrivalUpdateResponseCodeEnum) {
    ArrivalUpdateResponseCodeEnum[ArrivalUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ArrivalUpdateResponseCodeEnum[ArrivalUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ArrivalUpdateResponseCodeEnum[ArrivalUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ArrivalUpdateResponseCodeEnum = exports.ArrivalUpdateResponseCodeEnum || (exports.ArrivalUpdateResponseCodeEnum = {}));
function ArrivalUpdateResponseFromJSON(json) {
    return ArrivalUpdateResponseFromJSONTyped(json, false);
}
exports.ArrivalUpdateResponseFromJSON = ArrivalUpdateResponseFromJSON;
function ArrivalUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ArrivalUpdateResponseFromJSONTyped = ArrivalUpdateResponseFromJSONTyped;
function ArrivalUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ArrivalUpdateResponseToJSON = ArrivalUpdateResponseToJSON;
