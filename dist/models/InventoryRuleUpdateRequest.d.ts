/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRuleUpdateRequest
 */
export interface InventoryRuleUpdateRequest {
    /**
     * Description of the rule
     * @type {string}
     * @memberof InventoryRuleUpdateRequest
     */
    description?: string;
    /**
     * Rule's definition as javascript function
     * @type {string}
     * @memberof InventoryRuleUpdateRequest
     */
    rule: string;
}
export declare function InventoryRuleUpdateRequestFromJSON(json: any): InventoryRuleUpdateRequest;
export declare function InventoryRuleUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleUpdateRequest;
export declare function InventoryRuleUpdateRequestToJSON(value?: InventoryRuleUpdateRequest | null): any;
