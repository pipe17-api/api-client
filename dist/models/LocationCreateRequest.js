"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationCreateRequestToJSON = exports.LocationCreateRequestFromJSONTyped = exports.LocationCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationCreateRequestFromJSON(json) {
    return LocationCreateRequestFromJSONTyped(json, false);
}
exports.LocationCreateRequestFromJSON = LocationCreateRequestFromJSON;
function LocationCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'address': _1.LocationAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.LocationStatusFromJSON(json['status']),
        'infinite': !runtime_1.exists(json, 'infinite') ? undefined : json['infinite'],
        'preserveBundles': !runtime_1.exists(json, 'preserveBundles') ? undefined : json['preserveBundles'],
        'fulfillmentIntegrationId': !runtime_1.exists(json, 'fulfillmentIntegrationId') ? undefined : json['fulfillmentIntegrationId'],
        'externalSystem': !runtime_1.exists(json, 'externalSystem') ? undefined : (json['externalSystem'].map(_1.LocationExternalSystemFromJSON)),
    };
}
exports.LocationCreateRequestFromJSONTyped = LocationCreateRequestFromJSONTyped;
function LocationCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'address': _1.LocationAddressToJSON(value.address),
        'status': _1.LocationStatusToJSON(value.status),
        'infinite': value.infinite,
        'preserveBundles': value.preserveBundles,
        'fulfillmentIntegrationId': value.fulfillmentIntegrationId,
        'externalSystem': value.externalSystem === undefined ? undefined : (value.externalSystem.map(_1.LocationExternalSystemToJSON)),
    };
}
exports.LocationCreateRequestToJSON = LocationCreateRequestToJSON;
