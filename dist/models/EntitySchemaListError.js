"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaListErrorToJSON = exports.EntitySchemaListErrorFromJSONTyped = exports.EntitySchemaListErrorFromJSON = exports.EntitySchemaListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntitySchemaListErrorSuccessEnum;
(function (EntitySchemaListErrorSuccessEnum) {
    EntitySchemaListErrorSuccessEnum["False"] = "false";
})(EntitySchemaListErrorSuccessEnum = exports.EntitySchemaListErrorSuccessEnum || (exports.EntitySchemaListErrorSuccessEnum = {}));
function EntitySchemaListErrorFromJSON(json) {
    return EntitySchemaListErrorFromJSONTyped(json, false);
}
exports.EntitySchemaListErrorFromJSON = EntitySchemaListErrorFromJSON;
function EntitySchemaListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntitySchemasListFilterFromJSON(json['filters']),
    };
}
exports.EntitySchemaListErrorFromJSONTyped = EntitySchemaListErrorFromJSONTyped;
function EntitySchemaListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.EntitySchemasListFilterToJSON(value.filters),
    };
}
exports.EntitySchemaListErrorToJSON = EntitySchemaListErrorToJSON;
