/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Integration } from './';
/**
 *
 * @export
 * @interface IntegrationCreateResponseAllOf
 */
export interface IntegrationCreateResponseAllOf {
    /**
     *
     * @type {Integration}
     * @memberof IntegrationCreateResponseAllOf
     */
    integration?: Integration;
}
export declare function IntegrationCreateResponseAllOfFromJSON(json: any): IntegrationCreateResponseAllOf;
export declare function IntegrationCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationCreateResponseAllOf;
export declare function IntegrationCreateResponseAllOfToJSON(value?: IntegrationCreateResponseAllOf | null): any;
