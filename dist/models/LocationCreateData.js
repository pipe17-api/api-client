"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationCreateDataToJSON = exports.LocationCreateDataFromJSONTyped = exports.LocationCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationCreateDataFromJSON(json) {
    return LocationCreateDataFromJSONTyped(json, false);
}
exports.LocationCreateDataFromJSON = LocationCreateDataFromJSON;
function LocationCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'address': _1.LocationAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.LocationStatusFromJSON(json['status']),
        'infinite': !runtime_1.exists(json, 'infinite') ? undefined : json['infinite'],
        'preserveBundles': !runtime_1.exists(json, 'preserveBundles') ? undefined : json['preserveBundles'],
        'fulfillmentIntegrationId': !runtime_1.exists(json, 'fulfillmentIntegrationId') ? undefined : json['fulfillmentIntegrationId'],
        'externalSystem': !runtime_1.exists(json, 'externalSystem') ? undefined : (json['externalSystem'].map(_1.LocationExternalSystemFromJSON)),
    };
}
exports.LocationCreateDataFromJSONTyped = LocationCreateDataFromJSONTyped;
function LocationCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'address': _1.LocationAddressToJSON(value.address),
        'status': _1.LocationStatusToJSON(value.status),
        'infinite': value.infinite,
        'preserveBundles': value.preserveBundles,
        'fulfillmentIntegrationId': value.fulfillmentIntegrationId,
        'externalSystem': value.externalSystem === undefined ? undefined : (value.externalSystem.map(_1.LocationExternalSystemToJSON)),
    };
}
exports.LocationCreateDataToJSON = LocationCreateDataToJSON;
