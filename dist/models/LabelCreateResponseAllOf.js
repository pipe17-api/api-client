"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelCreateResponseAllOfToJSON = exports.LabelCreateResponseAllOfFromJSONTyped = exports.LabelCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LabelCreateResponseAllOfFromJSON(json) {
    return LabelCreateResponseAllOfFromJSONTyped(json, false);
}
exports.LabelCreateResponseAllOfFromJSON = LabelCreateResponseAllOfFromJSON;
function LabelCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'label': !runtime_1.exists(json, 'label') ? undefined : _1.LabelFromJSON(json['label']),
    };
}
exports.LabelCreateResponseAllOfFromJSONTyped = LabelCreateResponseAllOfFromJSONTyped;
function LabelCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'label': _1.LabelToJSON(value.label),
    };
}
exports.LabelCreateResponseAllOfToJSON = LabelCreateResponseAllOfToJSON;
