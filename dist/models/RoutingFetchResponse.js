"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingFetchResponseToJSON = exports.RoutingFetchResponseFromJSONTyped = exports.RoutingFetchResponseFromJSON = exports.RoutingFetchResponseCodeEnum = exports.RoutingFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoutingFetchResponseSuccessEnum;
(function (RoutingFetchResponseSuccessEnum) {
    RoutingFetchResponseSuccessEnum["True"] = "true";
})(RoutingFetchResponseSuccessEnum = exports.RoutingFetchResponseSuccessEnum || (exports.RoutingFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoutingFetchResponseCodeEnum;
(function (RoutingFetchResponseCodeEnum) {
    RoutingFetchResponseCodeEnum[RoutingFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoutingFetchResponseCodeEnum[RoutingFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoutingFetchResponseCodeEnum[RoutingFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoutingFetchResponseCodeEnum = exports.RoutingFetchResponseCodeEnum || (exports.RoutingFetchResponseCodeEnum = {}));
function RoutingFetchResponseFromJSON(json) {
    return RoutingFetchResponseFromJSONTyped(json, false);
}
exports.RoutingFetchResponseFromJSON = RoutingFetchResponseFromJSON;
function RoutingFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.RoutingFromJSON(json['routing']),
    };
}
exports.RoutingFetchResponseFromJSONTyped = RoutingFetchResponseFromJSONTyped;
function RoutingFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'routing': _1.RoutingToJSON(value.routing),
    };
}
exports.RoutingFetchResponseToJSON = RoutingFetchResponseToJSON;
