/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityFilterAllOf
 */
export interface EntityFilterAllOf {
    /**
     * Filter ID
     * @type {string}
     * @memberof EntityFilterAllOf
     */
    filterId?: string;
}
export declare function EntityFilterAllOfFromJSON(json: any): EntityFilterAllOf;
export declare function EntityFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterAllOf;
export declare function EntityFilterAllOfToJSON(value?: EntityFilterAllOf | null): any;
