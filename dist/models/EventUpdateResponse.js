"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventUpdateResponseToJSON = exports.EventUpdateResponseFromJSONTyped = exports.EventUpdateResponseFromJSON = exports.EventUpdateResponseCodeEnum = exports.EventUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var EventUpdateResponseSuccessEnum;
(function (EventUpdateResponseSuccessEnum) {
    EventUpdateResponseSuccessEnum["True"] = "true";
})(EventUpdateResponseSuccessEnum = exports.EventUpdateResponseSuccessEnum || (exports.EventUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EventUpdateResponseCodeEnum;
(function (EventUpdateResponseCodeEnum) {
    EventUpdateResponseCodeEnum[EventUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EventUpdateResponseCodeEnum[EventUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EventUpdateResponseCodeEnum[EventUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EventUpdateResponseCodeEnum = exports.EventUpdateResponseCodeEnum || (exports.EventUpdateResponseCodeEnum = {}));
function EventUpdateResponseFromJSON(json) {
    return EventUpdateResponseFromJSONTyped(json, false);
}
exports.EventUpdateResponseFromJSON = EventUpdateResponseFromJSON;
function EventUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.EventUpdateResponseFromJSONTyped = EventUpdateResponseFromJSONTyped;
function EventUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.EventUpdateResponseToJSON = EventUpdateResponseToJSON;
