/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Webhook } from './';
/**
 *
 * @export
 * @interface WebhookCreateResponse
 */
export interface WebhookCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof WebhookCreateResponse
     */
    success: WebhookCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhookCreateResponse
     */
    code: WebhookCreateResponseCodeEnum;
    /**
     *
     * @type {Webhook}
     * @memberof WebhookCreateResponse
     */
    webhook?: Webhook;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhookCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum WebhookCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function WebhookCreateResponseFromJSON(json: any): WebhookCreateResponse;
export declare function WebhookCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookCreateResponse;
export declare function WebhookCreateResponseToJSON(value?: WebhookCreateResponse | null): any;
