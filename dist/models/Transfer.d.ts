/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, NameValue, TransferLineItem, TransferStatus } from './';
/**
 *
 * @export
 * @interface Transfer
 */
export interface Transfer {
    /**
     * Transfer ID
     * @type {string}
     * @memberof Transfer
     */
    transferId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Transfer
     */
    integration?: string;
    /**
     *
     * @type {TransferStatus}
     * @memberof Transfer
     */
    status?: TransferStatus;
    /**
     * External Transfer Order ID
     * @type {string}
     * @memberof Transfer
     */
    extOrderId: string;
    /**
     * Transfer Order Date
     * @type {Date}
     * @memberof Transfer
     */
    transferOrderDate?: Date;
    /**
     *
     * @type {Array<TransferLineItem>}
     * @memberof Transfer
     */
    lineItems?: Array<TransferLineItem>;
    /**
     * Shipping Location ID
     * @type {string}
     * @memberof Transfer
     */
    fromLocationId?: string;
    /**
     *
     * @type {Address}
     * @memberof Transfer
     */
    toAddress?: Address;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof Transfer
     */
    toLocationId?: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof Transfer
     */
    shippingCarrier?: string;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof Transfer
     */
    shippingService?: string;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof Transfer
     */
    shipByDate?: Date;
    /**
     * Expected Ship Date
     * @type {Date}
     * @memberof Transfer
     */
    expectedShipDate?: Date;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof Transfer
     */
    expectedArrivalDate?: Date;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof Transfer
     */
    actualArrivalDate?: Date;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof Transfer
     */
    shippingNotes?: string;
    /**
     * Transfer order created by
     * @type {string}
     * @memberof Transfer
     */
    employeeName?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof Transfer
     */
    incoterms?: string;
    /**
     * Transfer Notes
     * @type {string}
     * @memberof Transfer
     */
    transferNotes?: string;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof Transfer
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof Transfer
     */
    timestamp?: Date;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Transfer
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Transfer
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Transfer
     */
    readonly orgKey?: string;
}
export declare function TransferFromJSON(json: any): Transfer;
export declare function TransferFromJSONTyped(json: any, ignoreDiscriminator: boolean): Transfer;
export declare function TransferToJSON(value?: Transfer | null): any;
