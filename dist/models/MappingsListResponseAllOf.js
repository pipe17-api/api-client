"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingsListResponseAllOfToJSON = exports.MappingsListResponseAllOfFromJSONTyped = exports.MappingsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingsListResponseAllOfFromJSON(json) {
    return MappingsListResponseAllOfFromJSONTyped(json, false);
}
exports.MappingsListResponseAllOfFromJSON = MappingsListResponseAllOfFromJSON;
function MappingsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.MappingsListFilterFromJSON(json['filters']),
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.MappingsListResponseAllOfFromJSONTyped = MappingsListResponseAllOfFromJSONTyped;
function MappingsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.MappingsListFilterToJSON(value.filters),
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.MappingsListResponseAllOfToJSON = MappingsListResponseAllOfToJSON;
