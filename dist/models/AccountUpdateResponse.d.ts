/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AccountUpdateResponse
 */
export interface AccountUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof AccountUpdateResponse
     */
    success: AccountUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountUpdateResponse
     */
    code: AccountUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum AccountUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function AccountUpdateResponseFromJSON(json: any): AccountUpdateResponse;
export declare function AccountUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountUpdateResponse;
export declare function AccountUpdateResponseToJSON(value?: AccountUpdateResponse | null): any;
