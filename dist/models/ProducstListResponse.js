"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProducstListResponseToJSON = exports.ProducstListResponseFromJSONTyped = exports.ProducstListResponseFromJSON = exports.ProducstListResponseCodeEnum = exports.ProducstListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProducstListResponseSuccessEnum;
(function (ProducstListResponseSuccessEnum) {
    ProducstListResponseSuccessEnum["True"] = "true";
})(ProducstListResponseSuccessEnum = exports.ProducstListResponseSuccessEnum || (exports.ProducstListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ProducstListResponseCodeEnum;
(function (ProducstListResponseCodeEnum) {
    ProducstListResponseCodeEnum[ProducstListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ProducstListResponseCodeEnum[ProducstListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ProducstListResponseCodeEnum[ProducstListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ProducstListResponseCodeEnum = exports.ProducstListResponseCodeEnum || (exports.ProducstListResponseCodeEnum = {}));
function ProducstListResponseFromJSON(json) {
    return ProducstListResponseFromJSONTyped(json, false);
}
exports.ProducstListResponseFromJSON = ProducstListResponseFromJSON;
function ProducstListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ProductsListFilterFromJSON(json['filters']),
        'products': (json['products'].map(_1.ProductFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ProducstListResponseFromJSONTyped = ProducstListResponseFromJSONTyped;
function ProducstListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ProductsListFilterToJSON(value.filters),
        'products': (value.products.map(_1.ProductToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ProducstListResponseToJSON = ProducstListResponseToJSON;
