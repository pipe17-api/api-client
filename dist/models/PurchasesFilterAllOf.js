"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesFilterAllOfToJSON = exports.PurchasesFilterAllOfFromJSONTyped = exports.PurchasesFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchasesFilterAllOfFromJSON(json) {
    return PurchasesFilterAllOfFromJSONTyped(json, false);
}
exports.PurchasesFilterAllOfFromJSON = PurchasesFilterAllOfFromJSON;
function PurchasesFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'purchaseId': !runtime_1.exists(json, 'purchaseId') ? undefined : json['purchaseId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.PurchaseStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.PurchasesFilterAllOfFromJSONTyped = PurchasesFilterAllOfFromJSONTyped;
function PurchasesFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'purchaseId': value.purchaseId,
        'extOrderId': value.extOrderId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.PurchaseStatusToJSON)),
        'deleted': value.deleted,
    };
}
exports.PurchasesFilterAllOfToJSON = PurchasesFilterAllOfToJSON;
