"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesListErrorAllOfToJSON = exports.PurchasesListErrorAllOfFromJSONTyped = exports.PurchasesListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchasesListErrorAllOfFromJSON(json) {
    return PurchasesListErrorAllOfFromJSONTyped(json, false);
}
exports.PurchasesListErrorAllOfFromJSON = PurchasesListErrorAllOfFromJSON;
function PurchasesListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesListFilterFromJSON(json['filters']),
    };
}
exports.PurchasesListErrorAllOfFromJSONTyped = PurchasesListErrorAllOfFromJSONTyped;
function PurchasesListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.PurchasesListFilterToJSON(value.filters),
    };
}
exports.PurchasesListErrorAllOfToJSON = PurchasesListErrorAllOfToJSON;
