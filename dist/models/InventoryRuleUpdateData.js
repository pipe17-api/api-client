"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleUpdateDataToJSON = exports.InventoryRuleUpdateDataFromJSONTyped = exports.InventoryRuleUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryRuleUpdateDataFromJSON(json) {
    return InventoryRuleUpdateDataFromJSONTyped(json, false);
}
exports.InventoryRuleUpdateDataFromJSON = InventoryRuleUpdateDataFromJSON;
function InventoryRuleUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'rule': json['rule'],
    };
}
exports.InventoryRuleUpdateDataFromJSONTyped = InventoryRuleUpdateDataFromJSONTyped;
function InventoryRuleUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'description': value.description,
        'rule': value.rule,
    };
}
exports.InventoryRuleUpdateDataToJSON = InventoryRuleUpdateDataToJSON;
