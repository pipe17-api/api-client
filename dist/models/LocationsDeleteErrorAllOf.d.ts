/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationsDeleteFilter } from './';
/**
 *
 * @export
 * @interface LocationsDeleteErrorAllOf
 */
export interface LocationsDeleteErrorAllOf {
    /**
     *
     * @type {LocationsDeleteFilter}
     * @memberof LocationsDeleteErrorAllOf
     */
    filters?: LocationsDeleteFilter;
}
export declare function LocationsDeleteErrorAllOfFromJSON(json: any): LocationsDeleteErrorAllOf;
export declare function LocationsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsDeleteErrorAllOf;
export declare function LocationsDeleteErrorAllOfToJSON(value?: LocationsDeleteErrorAllOf | null): any;
