/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SupplierAddress
 */
export interface SupplierAddress {
    /**
     * Address Line 1
     * @type {string}
     * @memberof SupplierAddress
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof SupplierAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof SupplierAddress
     */
    city?: string;
    /**
     * State or Province
     * @type {string}
     * @memberof SupplierAddress
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof SupplierAddress
     */
    zipCodeOrPostalCode?: string;
    /**
     * Country
     * @type {string}
     * @memberof SupplierAddress
     */
    country?: string;
}
export declare function SupplierAddressFromJSON(json: any): SupplierAddress;
export declare function SupplierAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierAddress;
export declare function SupplierAddressToJSON(value?: SupplierAddress | null): any;
