/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationStatus } from './';
/**
 * Organizations Filter
 * @export
 * @interface OrganizationsFilterAllOf
 */
export interface OrganizationsFilterAllOf {
    /**
     * Organizations by list of orgKey
     * @type {Array<string>}
     * @memberof OrganizationsFilterAllOf
     */
    orgKey?: Array<string>;
    /**
     * Organizations whose name contains this string
     * @type {string}
     * @memberof OrganizationsFilterAllOf
     */
    name?: string;
    /**
     * Organizations whose phone number contains this string
     * @type {string}
     * @memberof OrganizationsFilterAllOf
     */
    phone?: string;
    /**
     * Organizations whose email contains this string
     * @type {string}
     * @memberof OrganizationsFilterAllOf
     */
    email?: string;
    /**
     * Organizations by list of statuses
     * @type {Array<OrganizationStatus>}
     * @memberof OrganizationsFilterAllOf
     */
    status?: Array<OrganizationStatus>;
}
export declare function OrganizationsFilterAllOfFromJSON(json: any): OrganizationsFilterAllOf;
export declare function OrganizationsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsFilterAllOf;
export declare function OrganizationsFilterAllOfToJSON(value?: OrganizationsFilterAllOf | null): any;
