"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountUpdateRequestToJSON = exports.AccountUpdateRequestFromJSONTyped = exports.AccountUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountUpdateRequestFromJSON(json) {
    return AccountUpdateRequestFromJSONTyped(json, false);
}
exports.AccountUpdateRequestFromJSON = AccountUpdateRequestFromJSON;
function AccountUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.AccountStatusFromJSON(json['status']),
        'expirationDate': !runtime_1.exists(json, 'expirationDate') ? undefined : (new Date(json['expirationDate'])),
        'payment': !runtime_1.exists(json, 'payment') ? undefined : json['payment'],
    };
}
exports.AccountUpdateRequestFromJSONTyped = AccountUpdateRequestFromJSONTyped;
function AccountUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.AccountStatusToJSON(value.status),
        'expirationDate': value.expirationDate === undefined ? undefined : (new Date(value.expirationDate).toISOString().substr(0, 10)),
        'payment': value.payment,
    };
}
exports.AccountUpdateRequestToJSON = AccountUpdateRequestToJSON;
