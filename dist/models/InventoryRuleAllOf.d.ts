/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRuleAllOf
 */
export interface InventoryRuleAllOf {
    /**
     * Rule ID
     * @type {string}
     * @memberof InventoryRuleAllOf
     */
    ruleId?: string;
}
export declare function InventoryRuleAllOfFromJSON(json: any): InventoryRuleAllOf;
export declare function InventoryRuleAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleAllOf;
export declare function InventoryRuleAllOfToJSON(value?: InventoryRuleAllOf | null): any;
