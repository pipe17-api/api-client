/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductStatus, ProductType } from './';
/**
 *
 * @export
 * @interface ProductsFilter
 */
export interface ProductsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ProductsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ProductsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ProductsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ProductsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ProductsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ProductsFilter
     */
    count?: number;
    /**
     * Products by list of productId
     * @type {Array<string>}
     * @memberof ProductsFilter
     */
    productId?: Array<string>;
    /**
     * Products by list of extProductId
     * @type {Array<string>}
     * @memberof ProductsFilter
     */
    extProductId?: Array<string>;
    /**
     * All variants of a parent products by list of external ids
     * @type {Array<string>}
     * @memberof ProductsFilter
     */
    parentExtProductId?: Array<string>;
    /**
     * Products with this status
     * @type {Array<ProductStatus>}
     * @memberof ProductsFilter
     */
    status?: Array<ProductStatus>;
    /**
     * Products matching specified types
     * @type {Array<ProductType>}
     * @memberof ProductsFilter
     */
    types?: Array<ProductType>;
    /**
     * Soft deleted products
     * @type {boolean}
     * @memberof ProductsFilter
     */
    deleted?: boolean;
}
export declare function ProductsFilterFromJSON(json: any): ProductsFilter;
export declare function ProductsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsFilter;
export declare function ProductsFilterToJSON(value?: ProductsFilter | null): any;
