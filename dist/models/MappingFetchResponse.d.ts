/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Mapping } from './';
/**
 *
 * @export
 * @interface MappingFetchResponse
 */
export interface MappingFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof MappingFetchResponse
     */
    success: MappingFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof MappingFetchResponse
     */
    code: MappingFetchResponseCodeEnum;
    /**
     *
     * @type {Mapping}
     * @memberof MappingFetchResponse
     */
    mapping?: Mapping;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum MappingFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function MappingFetchResponseFromJSON(json: any): MappingFetchResponse;
export declare function MappingFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingFetchResponse;
export declare function MappingFetchResponseToJSON(value?: MappingFetchResponse | null): any;
