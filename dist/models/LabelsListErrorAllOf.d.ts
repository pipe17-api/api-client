/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LabelsListFilter } from './';
/**
 *
 * @export
 * @interface LabelsListErrorAllOf
 */
export interface LabelsListErrorAllOf {
    /**
     *
     * @type {LabelsListFilter}
     * @memberof LabelsListErrorAllOf
     */
    filters?: LabelsListFilter;
}
export declare function LabelsListErrorAllOfFromJSON(json: any): LabelsListErrorAllOf;
export declare function LabelsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsListErrorAllOf;
export declare function LabelsListErrorAllOfToJSON(value?: LabelsListErrorAllOf | null): any;
