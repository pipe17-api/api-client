/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Account } from './';
/**
 *
 * @export
 * @interface AccountFetchResponse
 */
export interface AccountFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof AccountFetchResponse
     */
    success: AccountFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountFetchResponse
     */
    code: AccountFetchResponseCodeEnum;
    /**
     *
     * @type {Account}
     * @memberof AccountFetchResponse
     */
    account?: Account;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum AccountFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function AccountFetchResponseFromJSON(json: any): AccountFetchResponse;
export declare function AccountFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountFetchResponse;
export declare function AccountFetchResponseToJSON(value?: AccountFetchResponse | null): any;
