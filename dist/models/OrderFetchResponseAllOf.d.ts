/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order } from './';
/**
 *
 * @export
 * @interface OrderFetchResponseAllOf
 */
export interface OrderFetchResponseAllOf {
    /**
     *
     * @type {Order}
     * @memberof OrderFetchResponseAllOf
     */
    order?: Order;
}
export declare function OrderFetchResponseAllOfFromJSON(json: any): OrderFetchResponseAllOf;
export declare function OrderFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderFetchResponseAllOf;
export declare function OrderFetchResponseAllOfToJSON(value?: OrderFetchResponseAllOf | null): any;
