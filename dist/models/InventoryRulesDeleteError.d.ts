/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRulesDeleteFilter } from './';
/**
 *
 * @export
 * @interface InventoryRulesDeleteError
 */
export interface InventoryRulesDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryRulesDeleteError
     */
    success: InventoryRulesDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRulesDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryRulesDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryRulesDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryRulesDeleteFilter}
     * @memberof InventoryRulesDeleteError
     */
    filters?: InventoryRulesDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRulesDeleteErrorSuccessEnum {
    False = "false"
}
export declare function InventoryRulesDeleteErrorFromJSON(json: any): InventoryRulesDeleteError;
export declare function InventoryRulesDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesDeleteError;
export declare function InventoryRulesDeleteErrorToJSON(value?: InventoryRulesDeleteError | null): any;
