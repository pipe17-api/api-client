/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ArrivalLineItem, ArrivalStatus } from './';
/**
 *
 * @export
 * @interface Arrival
 */
export interface Arrival {
    /**
     * Arrival notification ID
     * @type {string}
     * @memberof Arrival
     */
    arrivalId?: string;
    /**
     *
     * @type {ArrivalStatus}
     * @memberof Arrival
     */
    status?: ArrivalStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Arrival
     */
    integration?: string;
    /**
     * Transfer Order ID
     * @type {string}
     * @memberof Arrival
     */
    transferId?: string;
    /**
     * Purchase Order ID
     * @type {string}
     * @memberof Arrival
     */
    purchaseId?: string;
    /**
     * Fulfillment ID
     * @type {string}
     * @memberof Arrival
     */
    fulfillmentId?: string;
    /**
     * External TO or PO ID
     * @type {string}
     * @memberof Arrival
     */
    extOrderId?: string;
    /**
     * Sender's Name
     * @type {string}
     * @memberof Arrival
     */
    senderName: string;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof Arrival
     */
    expectedArrivalDate?: Date;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof Arrival
     */
    toLocationId: string;
    /**
     * Sender Location ID (for TO)
     * @type {string}
     * @memberof Arrival
     */
    fromLocationId?: string;
    /**
     *
     * @type {Address}
     * @memberof Arrival
     */
    fromAddress?: Address;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof Arrival
     */
    shippingCarrier?: string;
    /**
     * Shipping Method
     * @type {string}
     * @memberof Arrival
     */
    shippingMethod?: string;
    /**
     * Shipping Note
     * @type {string}
     * @memberof Arrival
     */
    shippingNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof Arrival
     */
    incoterms?: string;
    /**
     * Total Weight
     * @type {string}
     * @memberof Arrival
     */
    totalWeight?: string;
    /**
     * Weight Unit
     * @type {string}
     * @memberof Arrival
     */
    weightUnit?: string;
    /**
     *
     * @type {Array<ArrivalLineItem>}
     * @memberof Arrival
     */
    lineItems?: Array<ArrivalLineItem>;
    /**
     * Reference to arrival on external system (vendor)
     * @type {string}
     * @memberof Arrival
     */
    extArrivalId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Arrival
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Arrival
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Arrival
     */
    readonly orgKey?: string;
}
export declare function ArrivalFromJSON(json: any): Arrival;
export declare function ArrivalFromJSONTyped(json: any, ignoreDiscriminator: boolean): Arrival;
export declare function ArrivalToJSON(value?: Arrival | null): any;
