"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierCreateErrorToJSON = exports.SupplierCreateErrorFromJSONTyped = exports.SupplierCreateErrorFromJSON = exports.SupplierCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var SupplierCreateErrorSuccessEnum;
(function (SupplierCreateErrorSuccessEnum) {
    SupplierCreateErrorSuccessEnum["False"] = "false";
})(SupplierCreateErrorSuccessEnum = exports.SupplierCreateErrorSuccessEnum || (exports.SupplierCreateErrorSuccessEnum = {}));
function SupplierCreateErrorFromJSON(json) {
    return SupplierCreateErrorFromJSONTyped(json, false);
}
exports.SupplierCreateErrorFromJSON = SupplierCreateErrorFromJSON;
function SupplierCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierCreateDataFromJSON(json['supplier']),
    };
}
exports.SupplierCreateErrorFromJSONTyped = SupplierCreateErrorFromJSONTyped;
function SupplierCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'supplier': _1.SupplierCreateDataToJSON(value.supplier),
    };
}
exports.SupplierCreateErrorToJSON = SupplierCreateErrorToJSON;
