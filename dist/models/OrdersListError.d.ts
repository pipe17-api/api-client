/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrdersListFilter } from './';
/**
 *
 * @export
 * @interface OrdersListError
 */
export interface OrdersListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrdersListError
     */
    success: OrdersListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrdersListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrdersListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrdersListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrdersListFilter}
     * @memberof OrdersListError
     */
    filters?: OrdersListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum OrdersListErrorSuccessEnum {
    False = "false"
}
export declare function OrdersListErrorFromJSON(json: any): OrdersListError;
export declare function OrdersListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersListError;
export declare function OrdersListErrorToJSON(value?: OrdersListError | null): any;
