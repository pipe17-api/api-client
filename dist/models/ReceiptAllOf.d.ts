/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReceiptAllOf
 */
export interface ReceiptAllOf {
    /**
     * Receipt ID
     * @type {string}
     * @memberof ReceiptAllOf
     */
    receiptId?: string;
    /**
     * External Order ID
     * @type {string}
     * @memberof ReceiptAllOf
     */
    extOrderId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof ReceiptAllOf
     */
    integration?: string;
}
export declare function ReceiptAllOfFromJSON(json: any): ReceiptAllOf;
export declare function ReceiptAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptAllOf;
export declare function ReceiptAllOfToJSON(value?: ReceiptAllOf | null): any;
