"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPublishedItemStatusToJSON = exports.ProductPublishedItemStatusFromJSONTyped = exports.ProductPublishedItemStatusFromJSON = exports.ProductPublishedItemStatus = void 0;
/**
 * Product published item status
 * @export
 * @enum {string}
 */
var ProductPublishedItemStatus;
(function (ProductPublishedItemStatus) {
    ProductPublishedItemStatus["Tobepublished"] = "tobepublished";
    ProductPublishedItemStatus["Published"] = "published";
    ProductPublishedItemStatus["Tobeunpublished"] = "tobeunpublished";
    ProductPublishedItemStatus["Unpublished"] = "unpublished";
})(ProductPublishedItemStatus = exports.ProductPublishedItemStatus || (exports.ProductPublishedItemStatus = {}));
function ProductPublishedItemStatusFromJSON(json) {
    return ProductPublishedItemStatusFromJSONTyped(json, false);
}
exports.ProductPublishedItemStatusFromJSON = ProductPublishedItemStatusFromJSON;
function ProductPublishedItemStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ProductPublishedItemStatusFromJSONTyped = ProductPublishedItemStatusFromJSONTyped;
function ProductPublishedItemStatusToJSON(value) {
    return value;
}
exports.ProductPublishedItemStatusToJSON = ProductPublishedItemStatusToJSON;
