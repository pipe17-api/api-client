/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, NameValue, PurchaseCost, PurchaseLineItem, PurchaseStatus } from './';
/**
 *
 * @export
 * @interface Purchase
 */
export interface Purchase {
    /**
     * Purchase ID
     * @type {string}
     * @memberof Purchase
     */
    purchaseId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Purchase
     */
    integration?: string;
    /**
     *
     * @type {PurchaseStatus}
     * @memberof Purchase
     */
    status?: PurchaseStatus;
    /**
     * External Purchase Order ID
     * @type {string}
     * @memberof Purchase
     */
    extOrderId: string;
    /**
     * Purchase Order Date
     * @type {Date}
     * @memberof Purchase
     */
    purchaseOrderDate?: Date;
    /**
     *
     * @type {Array<PurchaseLineItem>}
     * @memberof Purchase
     */
    lineItems?: Array<PurchaseLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof Purchase
     */
    subTotalPrice?: number;
    /**
     * Purchase Tax
     * @type {number}
     * @memberof Purchase
     */
    purchaseTax?: number;
    /**
     * Purchase costs
     * @type {Array<PurchaseCost>}
     * @memberof Purchase
     */
    costs?: Array<PurchaseCost> | null;
    /**
     * Total Price
     * @type {number}
     * @memberof Purchase
     */
    totalPrice?: number;
    /**
     * Purchase order created by
     * @type {string}
     * @memberof Purchase
     */
    employeeName?: string;
    /**
     * Purchase Notes
     * @type {string}
     * @memberof Purchase
     */
    purchaseNotes?: string;
    /**
     *
     * @type {Address}
     * @memberof Purchase
     */
    vendorAddress?: Address;
    /**
     *
     * @type {Address}
     * @memberof Purchase
     */
    toAddress?: Address;
    /**
     * Ship to location ID
     * @type {string}
     * @memberof Purchase
     */
    toLocationId?: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof Purchase
     */
    shippingCarrier?: string;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof Purchase
     */
    shippingService?: string;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof Purchase
     */
    shipByDate?: Date;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof Purchase
     */
    actualArrivalDate?: Date;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof Purchase
     */
    expectedArrivalDate?: Date;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof Purchase
     */
    shippingNotes?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof Purchase
     */
    incoterms?: string;
    /**
     *
     * @type {Address}
     * @memberof Purchase
     */
    billingAddress?: Address;
    /**
     * Purchase order reference number
     * @type {string}
     * @memberof Purchase
     */
    referenceNumber?: string;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof Purchase
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof Purchase
     */
    timestamp?: Date;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Purchase
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Purchase
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Purchase
     */
    readonly orgKey?: string;
}
export declare function PurchaseFromJSON(json: any): Purchase;
export declare function PurchaseFromJSONTyped(json: any, ignoreDiscriminator: boolean): Purchase;
export declare function PurchaseToJSON(value?: Purchase | null): any;
