/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFiltersListFilter } from './';
/**
 *
 * @export
 * @interface EntityFilterListError
 */
export interface EntityFilterListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntityFilterListError
     */
    success: EntityFilterListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntityFilterListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntityFilterListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {EntityFiltersListFilter}
     * @memberof EntityFilterListError
     */
    filters?: EntityFiltersListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterListErrorSuccessEnum {
    False = "false"
}
export declare function EntityFilterListErrorFromJSON(json: any): EntityFilterListError;
export declare function EntityFilterListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterListError;
export declare function EntityFilterListErrorToJSON(value?: EntityFilterListError | null): any;
