"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleCreateResponseAllOfToJSON = exports.RoleCreateResponseAllOfFromJSONTyped = exports.RoleCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleCreateResponseAllOfFromJSON(json) {
    return RoleCreateResponseAllOfFromJSONTyped(json, false);
}
exports.RoleCreateResponseAllOfFromJSON = RoleCreateResponseAllOfFromJSON;
function RoleCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleFromJSON(json['role']),
    };
}
exports.RoleCreateResponseAllOfFromJSONTyped = RoleCreateResponseAllOfFromJSONTyped;
function RoleCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'role': _1.RoleToJSON(value.role),
    };
}
exports.RoleCreateResponseAllOfToJSON = RoleCreateResponseAllOfToJSON;
