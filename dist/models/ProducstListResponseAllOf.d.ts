/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Product, ProductsListFilter } from './';
/**
 *
 * @export
 * @interface ProducstListResponseAllOf
 */
export interface ProducstListResponseAllOf {
    /**
     *
     * @type {ProductsListFilter}
     * @memberof ProducstListResponseAllOf
     */
    filters?: ProductsListFilter;
    /**
     *
     * @type {Array<Product>}
     * @memberof ProducstListResponseAllOf
     */
    products: Array<Product>;
    /**
     *
     * @type {Pagination}
     * @memberof ProducstListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ProducstListResponseAllOfFromJSON(json: any): ProducstListResponseAllOf;
export declare function ProducstListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProducstListResponseAllOf;
export declare function ProducstListResponseAllOfToJSON(value?: ProducstListResponseAllOf | null): any;
