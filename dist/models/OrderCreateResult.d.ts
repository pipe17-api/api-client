/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order } from './';
/**
 *
 * @export
 * @interface OrderCreateResult
 */
export interface OrderCreateResult {
    /**
     *
     * @type {string}
     * @memberof OrderCreateResult
     */
    status?: OrderCreateResultStatusEnum;
    /**
     * Error message if failed
     * @type {string}
     * @memberof OrderCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof OrderCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Order}
     * @memberof OrderCreateResult
     */
    order?: Order;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function OrderCreateResultFromJSON(json: any): OrderCreateResult;
export declare function OrderCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderCreateResult;
export declare function OrderCreateResultToJSON(value?: OrderCreateResult | null): any;
