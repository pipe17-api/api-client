/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Routing } from './';
/**
 *
 * @export
 * @interface RoutingCreateResponse
 */
export interface RoutingCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoutingCreateResponse
     */
    success: RoutingCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingCreateResponse
     */
    code: RoutingCreateResponseCodeEnum;
    /**
     *
     * @type {Routing}
     * @memberof RoutingCreateResponse
     */
    routing?: Routing;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoutingCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoutingCreateResponseFromJSON(json: any): RoutingCreateResponse;
export declare function RoutingCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingCreateResponse;
export declare function RoutingCreateResponseToJSON(value?: RoutingCreateResponse | null): any;
