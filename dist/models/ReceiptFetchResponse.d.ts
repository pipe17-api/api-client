/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Receipt } from './';
/**
 *
 * @export
 * @interface ReceiptFetchResponse
 */
export interface ReceiptFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReceiptFetchResponse
     */
    success: ReceiptFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReceiptFetchResponse
     */
    code: ReceiptFetchResponseCodeEnum;
    /**
     *
     * @type {Receipt}
     * @memberof ReceiptFetchResponse
     */
    receipt?: Receipt;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReceiptFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReceiptFetchResponseFromJSON(json: any): ReceiptFetchResponse;
export declare function ReceiptFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptFetchResponse;
export declare function ReceiptFetchResponseToJSON(value?: ReceiptFetchResponse | null): any;
