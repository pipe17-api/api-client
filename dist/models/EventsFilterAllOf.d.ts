/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestSource, EventRequestStatus } from './';
/**
 * Events Filter
 * @export
 * @interface EventsFilterAllOf
 */
export interface EventsFilterAllOf {
    /**
     * Fetch by list of eventId
     * @type {Array<string>}
     * @memberof EventsFilterAllOf
     */
    eventId?: Array<string>;
    /**
     * Fetch by list of event sources
     * @type {Array<EventRequestSource>}
     * @memberof EventsFilterAllOf
     */
    source?: Array<EventRequestSource>;
    /**
     * Fetch by list of event statuses
     * @type {Array<EventRequestStatus>}
     * @memberof EventsFilterAllOf
     */
    status?: Array<EventRequestStatus>;
    /**
     * Fetch by list of requests
     * @type {Array<string>}
     * @memberof EventsFilterAllOf
     */
    requestId?: Array<string>;
}
export declare function EventsFilterAllOfFromJSON(json: any): EventsFilterAllOf;
export declare function EventsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsFilterAllOf;
export declare function EventsFilterAllOfToJSON(value?: EventsFilterAllOf | null): any;
