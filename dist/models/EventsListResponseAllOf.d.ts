/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Event, EventsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface EventsListResponseAllOf
 */
export interface EventsListResponseAllOf {
    /**
     *
     * @type {EventsListFilter}
     * @memberof EventsListResponseAllOf
     */
    filters?: EventsListFilter;
    /**
     *
     * @type {Array<Event>}
     * @memberof EventsListResponseAllOf
     */
    events?: Array<Event>;
    /**
     *
     * @type {Pagination}
     * @memberof EventsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function EventsListResponseAllOfFromJSON(json: any): EventsListResponseAllOf;
export declare function EventsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsListResponseAllOf;
export declare function EventsListResponseAllOfToJSON(value?: EventsListResponseAllOf | null): any;
