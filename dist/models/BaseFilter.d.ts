/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface BaseFilter
 */
export interface BaseFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof BaseFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof BaseFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof BaseFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof BaseFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof BaseFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof BaseFilter
     */
    count?: number;
}
export declare function BaseFilterFromJSON(json: any): BaseFilter;
export declare function BaseFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): BaseFilter;
export declare function BaseFilterToJSON(value?: BaseFilter | null): any;
