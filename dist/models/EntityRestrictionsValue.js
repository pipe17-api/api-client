"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityRestrictionsValueToJSON = exports.EntityRestrictionsValueFromJSONTyped = exports.EntityRestrictionsValueFromJSON = exports.EntityRestrictionsValueTypeEnum = exports.EntityRestrictionsValuePathEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var EntityRestrictionsValuePathEnum;
(function (EntityRestrictionsValuePathEnum) {
    EntityRestrictionsValuePathEnum["Restricted"] = "restricted";
    EntityRestrictionsValuePathEnum["Since"] = "since";
    EntityRestrictionsValuePathEnum["Until"] = "until";
    EntityRestrictionsValuePathEnum["Content"] = "content";
})(EntityRestrictionsValuePathEnum = exports.EntityRestrictionsValuePathEnum || (exports.EntityRestrictionsValuePathEnum = {})); /**
* @export
* @enum {string}
*/
var EntityRestrictionsValueTypeEnum;
(function (EntityRestrictionsValueTypeEnum) {
    EntityRestrictionsValueTypeEnum["Boolean"] = "boolean";
    EntityRestrictionsValueTypeEnum["Date"] = "date";
    EntityRestrictionsValueTypeEnum["Array"] = "array";
})(EntityRestrictionsValueTypeEnum = exports.EntityRestrictionsValueTypeEnum || (exports.EntityRestrictionsValueTypeEnum = {}));
function EntityRestrictionsValueFromJSON(json) {
    return EntityRestrictionsValueFromJSONTyped(json, false);
}
exports.EntityRestrictionsValueFromJSON = EntityRestrictionsValueFromJSON;
function EntityRestrictionsValueFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'path': json['path'],
        'type': !runtime_1.exists(json, 'type') ? undefined : json['type'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
    };
}
exports.EntityRestrictionsValueFromJSONTyped = EntityRestrictionsValueFromJSONTyped;
function EntityRestrictionsValueToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'path': value.path,
        'type': value.type,
        'value': value.value,
    };
}
exports.EntityRestrictionsValueToJSON = EntityRestrictionsValueToJSON;
