"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleCreateResponseAllOfToJSON = exports.InventoryRuleCreateResponseAllOfFromJSONTyped = exports.InventoryRuleCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryRuleCreateResponseAllOfFromJSON(json) {
    return InventoryRuleCreateResponseAllOfFromJSONTyped(json, false);
}
exports.InventoryRuleCreateResponseAllOfFromJSON = InventoryRuleCreateResponseAllOfFromJSON;
function InventoryRuleCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventoryrule': !runtime_1.exists(json, 'inventoryrule') ? undefined : _1.InventoryRuleFromJSON(json['inventoryrule']),
    };
}
exports.InventoryRuleCreateResponseAllOfFromJSONTyped = InventoryRuleCreateResponseAllOfFromJSONTyped;
function InventoryRuleCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventoryrule': _1.InventoryRuleToJSON(value.inventoryrule),
    };
}
exports.InventoryRuleCreateResponseAllOfToJSON = InventoryRuleCreateResponseAllOfToJSON;
