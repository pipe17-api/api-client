/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchema } from './';
/**
 *
 * @export
 * @interface EntitySchemaCreateResponseAllOf
 */
export interface EntitySchemaCreateResponseAllOf {
    /**
     *
     * @type {EntitySchema}
     * @memberof EntitySchemaCreateResponseAllOf
     */
    entitySchema?: EntitySchema;
}
export declare function EntitySchemaCreateResponseAllOfFromJSON(json: any): EntitySchemaCreateResponseAllOf;
export declare function EntitySchemaCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaCreateResponseAllOf;
export declare function EntitySchemaCreateResponseAllOfToJSON(value?: EntitySchemaCreateResponseAllOf | null): any;
