"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersListResponseToJSON = exports.OrdersListResponseFromJSONTyped = exports.OrdersListResponseFromJSON = exports.OrdersListResponseCodeEnum = exports.OrdersListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrdersListResponseSuccessEnum;
(function (OrdersListResponseSuccessEnum) {
    OrdersListResponseSuccessEnum["True"] = "true";
})(OrdersListResponseSuccessEnum = exports.OrdersListResponseSuccessEnum || (exports.OrdersListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrdersListResponseCodeEnum;
(function (OrdersListResponseCodeEnum) {
    OrdersListResponseCodeEnum[OrdersListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrdersListResponseCodeEnum[OrdersListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrdersListResponseCodeEnum[OrdersListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrdersListResponseCodeEnum = exports.OrdersListResponseCodeEnum || (exports.OrdersListResponseCodeEnum = {}));
function OrdersListResponseFromJSON(json) {
    return OrdersListResponseFromJSONTyped(json, false);
}
exports.OrdersListResponseFromJSON = OrdersListResponseFromJSON;
function OrdersListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersListFilterFromJSON(json['filters']),
        'orders': (json['orders'].map(_1.OrderFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrdersListResponseFromJSONTyped = OrdersListResponseFromJSONTyped;
function OrdersListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.OrdersListFilterToJSON(value.filters),
        'orders': (value.orders.map(_1.OrderToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrdersListResponseToJSON = OrdersListResponseToJSON;
