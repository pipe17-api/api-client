"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSetupDataToJSON = exports.ConnectorSetupDataFromJSONTyped = exports.ConnectorSetupDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function ConnectorSetupDataFromJSON(json) {
    return ConnectorSetupDataFromJSONTyped(json, false);
}
exports.ConnectorSetupDataFromJSON = ConnectorSetupDataFromJSON;
function ConnectorSetupDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'rank': !runtime_1.exists(json, 'rank') ? undefined : json['rank'],
        'orgUnique': !runtime_1.exists(json, 'orgUnique') ? undefined : json['orgUnique'],
    };
}
exports.ConnectorSetupDataFromJSONTyped = ConnectorSetupDataFromJSONTyped;
function ConnectorSetupDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'isPublic': value.isPublic,
        'rank': value.rank,
        'orgUnique': value.orgUnique,
    };
}
exports.ConnectorSetupDataToJSON = ConnectorSetupDataToJSON;
