"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterListResponseAllOfToJSON = exports.ExceptionFilterListResponseAllOfFromJSONTyped = exports.ExceptionFilterListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFilterListResponseAllOfFromJSON(json) {
    return ExceptionFilterListResponseAllOfFromJSONTyped(json, false);
}
exports.ExceptionFilterListResponseAllOfFromJSON = ExceptionFilterListResponseAllOfFromJSON;
function ExceptionFilterListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ExceptionFiltersListFilterFromJSON(json['filters']),
        'exceptionFilters': !runtime_1.exists(json, 'exceptionFilters') ? undefined : (json['exceptionFilters'].map(_1.ExceptionFilterFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ExceptionFilterListResponseAllOfFromJSONTyped = ExceptionFilterListResponseAllOfFromJSONTyped;
function ExceptionFilterListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ExceptionFiltersListFilterToJSON(value.filters),
        'exceptionFilters': value.exceptionFilters === undefined ? undefined : (value.exceptionFilters.map(_1.ExceptionFilterToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ExceptionFilterListResponseAllOfToJSON = ExceptionFilterListResponseAllOfToJSON;
