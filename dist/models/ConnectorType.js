"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorTypeToJSON = exports.ConnectorTypeFromJSONTyped = exports.ConnectorTypeFromJSON = exports.ConnectorType = void 0;
/**
 * Connector Type
 * @export
 * @enum {string}
 */
var ConnectorType;
(function (ConnectorType) {
    ConnectorType["Ecommerce"] = "ecommerce";
    ConnectorType["Finance"] = "finance";
    ConnectorType["Logistics"] = "logistics";
    ConnectorType["Plugin"] = "plugin";
    ConnectorType["Other"] = "other";
})(ConnectorType = exports.ConnectorType || (exports.ConnectorType = {}));
function ConnectorTypeFromJSON(json) {
    return ConnectorTypeFromJSONTyped(json, false);
}
exports.ConnectorTypeFromJSON = ConnectorTypeFromJSON;
function ConnectorTypeFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ConnectorTypeFromJSONTyped = ConnectorTypeFromJSONTyped;
function ConnectorTypeToJSON(value) {
    return value;
}
exports.ConnectorTypeToJSON = ConnectorTypeToJSON;
