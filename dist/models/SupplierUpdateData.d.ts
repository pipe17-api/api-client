/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierAddress, SupplierContact } from './';
/**
 *
 * @export
 * @interface SupplierUpdateData
 */
export interface SupplierUpdateData {
    /**
     * Supplier Name
     * @type {string}
     * @memberof SupplierUpdateData
     */
    name?: string;
    /**
     *
     * @type {SupplierAddress}
     * @memberof SupplierUpdateData
     */
    address?: SupplierAddress;
    /**
     * Supplier Contacts
     * @type {Array<SupplierContact>}
     * @memberof SupplierUpdateData
     */
    contacts?: Array<SupplierContact>;
}
export declare function SupplierUpdateDataFromJSON(json: any): SupplierUpdateData;
export declare function SupplierUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierUpdateData;
export declare function SupplierUpdateDataToJSON(value?: SupplierUpdateData | null): any;
