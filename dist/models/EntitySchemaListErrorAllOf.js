"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaListErrorAllOfToJSON = exports.EntitySchemaListErrorAllOfFromJSONTyped = exports.EntitySchemaListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntitySchemaListErrorAllOfFromJSON(json) {
    return EntitySchemaListErrorAllOfFromJSONTyped(json, false);
}
exports.EntitySchemaListErrorAllOfFromJSON = EntitySchemaListErrorAllOfFromJSON;
function EntitySchemaListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntitySchemasListFilterFromJSON(json['filters']),
    };
}
exports.EntitySchemaListErrorAllOfFromJSONTyped = EntitySchemaListErrorAllOfFromJSONTyped;
function EntitySchemaListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.EntitySchemasListFilterToJSON(value.filters),
    };
}
exports.EntitySchemaListErrorAllOfToJSON = EntitySchemaListErrorAllOfToJSON;
