/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRoutingsListFilter } from './';
/**
 *
 * @export
 * @interface OrderRoutingListErrorAllOf
 */
export interface OrderRoutingListErrorAllOf {
    /**
     *
     * @type {OrderRoutingsListFilter}
     * @memberof OrderRoutingListErrorAllOf
     */
    filters?: OrderRoutingsListFilter;
}
export declare function OrderRoutingListErrorAllOfFromJSON(json: any): OrderRoutingListErrorAllOf;
export declare function OrderRoutingListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingListErrorAllOf;
export declare function OrderRoutingListErrorAllOfToJSON(value?: OrderRoutingListErrorAllOf | null): any;
