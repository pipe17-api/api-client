/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorUpdateData } from './';
/**
 *
 * @export
 * @interface ConnectorUpdateError
 */
export interface ConnectorUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ConnectorUpdateError
     */
    success: ConnectorUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ConnectorUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ConnectorUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ConnectorUpdateData}
     * @memberof ConnectorUpdateError
     */
    connector?: ConnectorUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ConnectorUpdateErrorFromJSON(json: any): ConnectorUpdateError;
export declare function ConnectorUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorUpdateError;
export declare function ConnectorUpdateErrorToJSON(value?: ConnectorUpdateError | null): any;
