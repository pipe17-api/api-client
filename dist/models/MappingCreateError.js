"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingCreateErrorToJSON = exports.MappingCreateErrorFromJSONTyped = exports.MappingCreateErrorFromJSON = exports.MappingCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var MappingCreateErrorSuccessEnum;
(function (MappingCreateErrorSuccessEnum) {
    MappingCreateErrorSuccessEnum["False"] = "false";
})(MappingCreateErrorSuccessEnum = exports.MappingCreateErrorSuccessEnum || (exports.MappingCreateErrorSuccessEnum = {}));
function MappingCreateErrorFromJSON(json) {
    return MappingCreateErrorFromJSONTyped(json, false);
}
exports.MappingCreateErrorFromJSON = MappingCreateErrorFromJSON;
function MappingCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingCreateDataFromJSON(json['mapping']),
    };
}
exports.MappingCreateErrorFromJSONTyped = MappingCreateErrorFromJSONTyped;
function MappingCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'mapping': _1.MappingCreateDataToJSON(value.mapping),
    };
}
exports.MappingCreateErrorToJSON = MappingCreateErrorToJSON;
