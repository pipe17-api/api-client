"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesListErrorAllOfToJSON = exports.RolesListErrorAllOfFromJSONTyped = exports.RolesListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RolesListErrorAllOfFromJSON(json) {
    return RolesListErrorAllOfFromJSONTyped(json, false);
}
exports.RolesListErrorAllOfFromJSON = RolesListErrorAllOfFromJSON;
function RolesListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RolesListFilterFromJSON(json['filters']),
    };
}
exports.RolesListErrorAllOfFromJSONTyped = RolesListErrorAllOfFromJSONTyped;
function RolesListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RolesListFilterToJSON(value.filters),
    };
}
exports.RolesListErrorAllOfToJSON = RolesListErrorAllOfToJSON;
