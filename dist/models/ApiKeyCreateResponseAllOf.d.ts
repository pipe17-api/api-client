/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyCreateResponseAllOf
 */
export interface ApiKeyCreateResponseAllOf {
    /**
     * New Api Key
     * @type {string}
     * @memberof ApiKeyCreateResponseAllOf
     */
    apikey: string;
    /**
     * New Api Key ID
     * @type {string}
     * @memberof ApiKeyCreateResponseAllOf
     */
    apikeyId?: string;
}
export declare function ApiKeyCreateResponseAllOfFromJSON(json: any): ApiKeyCreateResponseAllOf;
export declare function ApiKeyCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyCreateResponseAllOf;
export declare function ApiKeyCreateResponseAllOfToJSON(value?: ApiKeyCreateResponseAllOf | null): any;
