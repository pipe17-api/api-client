/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilterCreateData } from './';
/**
 *
 * @export
 * @interface ExceptionFilterCreateErrorAllOf
 */
export interface ExceptionFilterCreateErrorAllOf {
    /**
     *
     * @type {ExceptionFilterCreateData}
     * @memberof ExceptionFilterCreateErrorAllOf
     */
    exceptionFilter?: ExceptionFilterCreateData;
}
export declare function ExceptionFilterCreateErrorAllOfFromJSON(json: any): ExceptionFilterCreateErrorAllOf;
export declare function ExceptionFilterCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterCreateErrorAllOf;
export declare function ExceptionFilterCreateErrorAllOfToJSON(value?: ExceptionFilterCreateErrorAllOf | null): any;
