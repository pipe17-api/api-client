/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Products Filter
 * @export
 * @interface ProductsListFilterAllOf
 */
export interface ProductsListFilterAllOf {
    /**
     * Products by list sku
     * @type {Array<string>}
     * @memberof ProductsListFilterAllOf
     */
    sku?: Array<string>;
    /**
     * Products matching this name
     * @type {string}
     * @memberof ProductsListFilterAllOf
     */
    name?: string;
    /**
     * Products of the bundle with this SKU
     * @type {string}
     * @memberof ProductsListFilterAllOf
     */
    bundleSKU?: string;
    /**
     * All bundles containing the item with this SKU
     * @type {string}
     * @memberof ProductsListFilterAllOf
     */
    inBundle?: string;
}
export declare function ProductsListFilterAllOfFromJSON(json: any): ProductsListFilterAllOf;
export declare function ProductsListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsListFilterAllOf;
export declare function ProductsListFilterAllOfToJSON(value?: ProductsListFilterAllOf | null): any;
