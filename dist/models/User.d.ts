/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserAddress, UserStatus } from './';
/**
 *
 * @export
 * @interface User
 */
export interface User {
    /**
     * User ID
     * @type {string}
     * @memberof User
     */
    userId?: string;
    /**
     *
     * @type {UserStatus}
     * @memberof User
     */
    status?: UserStatus;
    /**
     * User Name
     * @type {string}
     * @memberof User
     */
    name: string;
    /**
     * User email
     * @type {string}
     * @memberof User
     */
    email: string;
    /**
     * User named Role(s)
     * @type {Array<string>}
     * @memberof User
     */
    roles: Array<string>;
    /**
     * User Phone
     * @type {string}
     * @memberof User
     */
    phone?: string;
    /**
     * User Time Zone
     * @type {string}
     * @memberof User
     */
    timeZone?: string;
    /**
     * User Description
     * @type {string}
     * @memberof User
     */
    description?: string;
    /**
     *
     * @type {UserAddress}
     * @memberof User
     */
    address?: UserAddress;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof User
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof User
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof User
     */
    readonly orgKey?: string;
}
export declare function UserFromJSON(json: any): User;
export declare function UserFromJSONTyped(json: any, ignoreDiscriminator: boolean): User;
export declare function UserToJSON(value?: User | null): any;
