/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule, InventoryRulesListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryRulesListResponseAllOf
 */
export interface InventoryRulesListResponseAllOf {
    /**
     *
     * @type {InventoryRulesListFilter}
     * @memberof InventoryRulesListResponseAllOf
     */
    filters?: InventoryRulesListFilter;
    /**
     *
     * @type {Array<InventoryRule>}
     * @memberof InventoryRulesListResponseAllOf
     */
    rules?: Array<InventoryRule>;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryRulesListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function InventoryRulesListResponseAllOfFromJSON(json: any): InventoryRulesListResponseAllOf;
export declare function InventoryRulesListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesListResponseAllOf;
export declare function InventoryRulesListResponseAllOfToJSON(value?: InventoryRulesListResponseAllOf | null): any;
