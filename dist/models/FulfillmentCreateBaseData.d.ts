/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentCreateBaseData
 */
export interface FulfillmentCreateBaseData {
    /**
     * Internal Pipe17 ID of the Shipment
     * @type {string}
     * @memberof FulfillmentCreateBaseData
     */
    shipmentId?: string;
    /**
     * Tracking number(s)
     * @type {Array<string>}
     * @memberof FulfillmentCreateBaseData
     */
    trackingNumber?: Array<string>;
    /**
     * Actual ship date
     * @type {Date}
     * @memberof FulfillmentCreateBaseData
     */
    actualShipDate?: Date;
    /**
     * Expected arrival date
     * @type {Date}
     * @memberof FulfillmentCreateBaseData
     */
    expectedArrivalDate?: Date;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof FulfillmentCreateBaseData
     */
    shippingCarrier?: string;
    /**
     * Shipping Class
     * @type {string}
     * @memberof FulfillmentCreateBaseData
     */
    shippingClass?: string;
    /**
     * Shipping Charge
     * @type {number}
     * @memberof FulfillmentCreateBaseData
     */
    shippingCharge?: number;
    /**
     * Weight unit
     * @type {string}
     * @memberof FulfillmentCreateBaseData
     */
    weightUnit?: FulfillmentCreateBaseDataWeightUnitEnum;
    /**
     * External customer friendly ID
     * @type {string}
     * @memberof FulfillmentCreateBaseData
     */
    extOrderId?: string;
    /**
     * Weight
     * @type {number}
     * @memberof FulfillmentCreateBaseData
     */
    weight?: number;
    /**
     * Reference to fulfillment on external system (vendor)
     * @type {string}
     * @memberof FulfillmentCreateBaseData
     */
    extFulfillmentId?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentCreateBaseDataWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
}
export declare function FulfillmentCreateBaseDataFromJSON(json: any): FulfillmentCreateBaseData;
export declare function FulfillmentCreateBaseDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentCreateBaseData;
export declare function FulfillmentCreateBaseDataToJSON(value?: FulfillmentCreateBaseData | null): any;
