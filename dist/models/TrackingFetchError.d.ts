/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TrackingFetchError
 */
export interface TrackingFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TrackingFetchError
     */
    success: TrackingFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TrackingFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TrackingFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingFetchErrorSuccessEnum {
    False = "false"
}
export declare function TrackingFetchErrorFromJSON(json: any): TrackingFetchError;
export declare function TrackingFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingFetchError;
export declare function TrackingFetchErrorToJSON(value?: TrackingFetchError | null): any;
