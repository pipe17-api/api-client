/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserStatus } from './';
/**
 *
 * @export
 * @interface UsersFilter
 */
export interface UsersFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof UsersFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof UsersFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof UsersFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof UsersFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof UsersFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof UsersFilter
     */
    count?: number;
    /**
     * Users by list of userId
     * @type {Array<string>}
     * @memberof UsersFilter
     */
    userId?: Array<string>;
    /**
     * Users whose name contains this string
     * @type {string}
     * @memberof UsersFilter
     */
    name?: string;
    /**
     * Users whose email contains this string
     * @type {string}
     * @memberof UsersFilter
     */
    email?: string;
    /**
     * Users by list of statuses
     * @type {Array<UserStatus>}
     * @memberof UsersFilter
     */
    status?: Array<UserStatus>;
    /**
     * Return users of all organizations
     * @type {string}
     * @memberof UsersFilter
     */
    organizations?: string;
}
export declare function UsersFilterFromJSON(json: any): UsersFilter;
export declare function UsersFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersFilter;
export declare function UsersFilterToJSON(value?: UsersFilter | null): any;
