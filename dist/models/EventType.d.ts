/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Type of event triggering inventory change
 * @export
 * @enum {string}
 */
export declare enum EventType {
    Ingest = "ingest",
    Adjust = "adjust",
    Ship = "ship",
    Fulfill = "fulfill",
    Xferin = "xferin",
    Xferout = "xferout",
    Return = "return",
    Receive = "receive",
    Xferfulfill = "xferfulfill",
    FutureShip = "futureShip",
    Release = "release",
    ShipCancel = "shipCancel",
    ShipCancelRestock = "shipCancelRestock",
    FutureShipCancel = "futureShipCancel",
    VirtualCommit = "virtualCommit"
}
export declare function EventTypeFromJSON(json: any): EventType;
export declare function EventTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventType;
export declare function EventTypeToJSON(value?: EventType | null): any;
