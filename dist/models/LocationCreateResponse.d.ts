/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location } from './';
/**
 *
 * @export
 * @interface LocationCreateResponse
 */
export interface LocationCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LocationCreateResponse
     */
    success: LocationCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationCreateResponse
     */
    code: LocationCreateResponseCodeEnum;
    /**
     *
     * @type {Location}
     * @memberof LocationCreateResponse
     */
    location?: Location;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LocationCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LocationCreateResponseFromJSON(json: any): LocationCreateResponse;
export declare function LocationCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationCreateResponse;
export declare function LocationCreateResponseToJSON(value?: LocationCreateResponse | null): any;
