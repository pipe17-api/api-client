"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingCreateResponseToJSON = exports.TrackingCreateResponseFromJSONTyped = exports.TrackingCreateResponseFromJSON = exports.TrackingCreateResponseCodeEnum = exports.TrackingCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TrackingCreateResponseSuccessEnum;
(function (TrackingCreateResponseSuccessEnum) {
    TrackingCreateResponseSuccessEnum["True"] = "true";
})(TrackingCreateResponseSuccessEnum = exports.TrackingCreateResponseSuccessEnum || (exports.TrackingCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TrackingCreateResponseCodeEnum;
(function (TrackingCreateResponseCodeEnum) {
    TrackingCreateResponseCodeEnum[TrackingCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TrackingCreateResponseCodeEnum[TrackingCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TrackingCreateResponseCodeEnum[TrackingCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TrackingCreateResponseCodeEnum = exports.TrackingCreateResponseCodeEnum || (exports.TrackingCreateResponseCodeEnum = {}));
function TrackingCreateResponseFromJSON(json) {
    return TrackingCreateResponseFromJSONTyped(json, false);
}
exports.TrackingCreateResponseFromJSON = TrackingCreateResponseFromJSON;
function TrackingCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'tracking': !runtime_1.exists(json, 'tracking') ? undefined : _1.TrackingFromJSON(json['tracking']),
    };
}
exports.TrackingCreateResponseFromJSONTyped = TrackingCreateResponseFromJSONTyped;
function TrackingCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'tracking': _1.TrackingToJSON(value.tracking),
    };
}
exports.TrackingCreateResponseToJSON = TrackingCreateResponseToJSON;
