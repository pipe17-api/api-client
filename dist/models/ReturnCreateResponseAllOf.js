"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnCreateResponseAllOfToJSON = exports.ReturnCreateResponseAllOfFromJSONTyped = exports.ReturnCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnCreateResponseAllOfFromJSON(json) {
    return ReturnCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ReturnCreateResponseAllOfFromJSON = ReturnCreateResponseAllOfFromJSON;
function ReturnCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        '_return': !runtime_1.exists(json, 'return') ? undefined : _1.ReturnFromJSON(json['return']),
    };
}
exports.ReturnCreateResponseAllOfFromJSONTyped = ReturnCreateResponseAllOfFromJSONTyped;
function ReturnCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'return': _1.ReturnToJSON(value._return),
    };
}
exports.ReturnCreateResponseAllOfToJSON = ReturnCreateResponseAllOfToJSON;
