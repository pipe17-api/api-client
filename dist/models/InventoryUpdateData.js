"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryUpdateDataToJSON = exports.InventoryUpdateDataFromJSONTyped = exports.InventoryUpdateDataFromJSON = exports.InventoryUpdateDataOrderTypeEnum = exports.InventoryUpdateDataEntityTypeEnum = exports.InventoryUpdateDataEventEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryUpdateDataEventEnum;
(function (InventoryUpdateDataEventEnum) {
    InventoryUpdateDataEventEnum["Ship"] = "ship";
    InventoryUpdateDataEventEnum["Fulfill"] = "fulfill";
    InventoryUpdateDataEventEnum["Xferout"] = "xferout";
    InventoryUpdateDataEventEnum["Xferfulfill"] = "xferfulfill";
    InventoryUpdateDataEventEnum["Xferin"] = "xferin";
    InventoryUpdateDataEventEnum["Receive"] = "receive";
    InventoryUpdateDataEventEnum["FutureShip"] = "futureShip";
    InventoryUpdateDataEventEnum["Release"] = "release";
    InventoryUpdateDataEventEnum["ShipCancel"] = "shipCancel";
    InventoryUpdateDataEventEnum["ShipCancelRestock"] = "shipCancelRestock";
    InventoryUpdateDataEventEnum["FutureShipCancel"] = "futureShipCancel";
    InventoryUpdateDataEventEnum["VirtualCommit"] = "virtualCommit";
})(InventoryUpdateDataEventEnum = exports.InventoryUpdateDataEventEnum || (exports.InventoryUpdateDataEventEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryUpdateDataEntityTypeEnum;
(function (InventoryUpdateDataEntityTypeEnum) {
    InventoryUpdateDataEntityTypeEnum["Arrivals"] = "arrivals";
    InventoryUpdateDataEntityTypeEnum["Fulfillments"] = "fulfillments";
    InventoryUpdateDataEntityTypeEnum["Inventory"] = "inventory";
    InventoryUpdateDataEntityTypeEnum["Receipts"] = "receipts";
    InventoryUpdateDataEntityTypeEnum["Shipments"] = "shipments";
    InventoryUpdateDataEntityTypeEnum["Orders"] = "orders";
})(InventoryUpdateDataEntityTypeEnum = exports.InventoryUpdateDataEntityTypeEnum || (exports.InventoryUpdateDataEntityTypeEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryUpdateDataOrderTypeEnum;
(function (InventoryUpdateDataOrderTypeEnum) {
    InventoryUpdateDataOrderTypeEnum["Sales"] = "sales";
    InventoryUpdateDataOrderTypeEnum["Transfer"] = "transfer";
    InventoryUpdateDataOrderTypeEnum["Purchase"] = "purchase";
})(InventoryUpdateDataOrderTypeEnum = exports.InventoryUpdateDataOrderTypeEnum || (exports.InventoryUpdateDataOrderTypeEnum = {}));
function InventoryUpdateDataFromJSON(json) {
    return InventoryUpdateDataFromJSONTyped(json, false);
}
exports.InventoryUpdateDataFromJSON = InventoryUpdateDataFromJSON;
function InventoryUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'event': json['event'],
        'ptype': _1.EventSourceFromJSON(json['ptype']),
        'sku': json['sku'],
        'locationId': json['locationId'],
        'infinite': !runtime_1.exists(json, 'infinite') ? undefined : json['infinite'],
        'quantity': json['quantity'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'entityType': !runtime_1.exists(json, 'entityType') ? undefined : json['entityType'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'orderType': !runtime_1.exists(json, 'orderType') ? undefined : json['orderType'],
    };
}
exports.InventoryUpdateDataFromJSONTyped = InventoryUpdateDataFromJSONTyped;
function InventoryUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'event': value.event,
        'ptype': _1.EventSourceToJSON(value.ptype),
        'sku': value.sku,
        'locationId': value.locationId,
        'infinite': value.infinite,
        'quantity': value.quantity,
        'entityId': value.entityId,
        'entityType': value.entityType,
        'orderId': value.orderId,
        'orderType': value.orderType,
    };
}
exports.InventoryUpdateDataToJSON = InventoryUpdateDataToJSON;
