"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryUpdateResponseToJSON = exports.InventoryUpdateResponseFromJSONTyped = exports.InventoryUpdateResponseFromJSON = exports.InventoryUpdateResponseCodeEnum = exports.InventoryUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var InventoryUpdateResponseSuccessEnum;
(function (InventoryUpdateResponseSuccessEnum) {
    InventoryUpdateResponseSuccessEnum["True"] = "true";
})(InventoryUpdateResponseSuccessEnum = exports.InventoryUpdateResponseSuccessEnum || (exports.InventoryUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryUpdateResponseCodeEnum;
(function (InventoryUpdateResponseCodeEnum) {
    InventoryUpdateResponseCodeEnum[InventoryUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryUpdateResponseCodeEnum[InventoryUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryUpdateResponseCodeEnum[InventoryUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryUpdateResponseCodeEnum = exports.InventoryUpdateResponseCodeEnum || (exports.InventoryUpdateResponseCodeEnum = {}));
function InventoryUpdateResponseFromJSON(json) {
    return InventoryUpdateResponseFromJSONTyped(json, false);
}
exports.InventoryUpdateResponseFromJSON = InventoryUpdateResponseFromJSON;
function InventoryUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.InventoryUpdateResponseFromJSONTyped = InventoryUpdateResponseFromJSONTyped;
function InventoryUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.InventoryUpdateResponseToJSON = InventoryUpdateResponseToJSON;
