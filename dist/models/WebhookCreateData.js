"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookCreateDataToJSON = exports.WebhookCreateDataFromJSONTyped = exports.WebhookCreateDataFromJSON = void 0;
const _1 = require("./");
function WebhookCreateDataFromJSON(json) {
    return WebhookCreateDataFromJSONTyped(json, false);
}
exports.WebhookCreateDataFromJSON = WebhookCreateDataFromJSON;
function WebhookCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'url': json['url'],
        'apikey': json['apikey'],
        'topics': (json['topics'].map(_1.WebhookTopicsFromJSON)),
    };
}
exports.WebhookCreateDataFromJSONTyped = WebhookCreateDataFromJSONTyped;
function WebhookCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'url': value.url,
        'apikey': value.apikey,
        'topics': (value.topics.map(_1.WebhookTopicsToJSON)),
    };
}
exports.WebhookCreateDataToJSON = WebhookCreateDataToJSON;
