/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SuppliersListFilter
 */
export interface SuppliersListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof SuppliersListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof SuppliersListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof SuppliersListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof SuppliersListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof SuppliersListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof SuppliersListFilter
     */
    count?: number;
    /**
     * Suppliers by list of supplierId
     * @type {Array<string>}
     * @memberof SuppliersListFilter
     */
    supplierId?: Array<string>;
    /**
     * Soft deleted suppliers
     * @type {boolean}
     * @memberof SuppliersListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof SuppliersListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof SuppliersListFilter
     */
    keys?: string;
    /**
     * Retrieve suppliers matching name string
     * @type {string}
     * @memberof SuppliersListFilter
     */
    name?: string;
}
export declare function SuppliersListFilterFromJSON(json: any): SuppliersListFilter;
export declare function SuppliersListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersListFilter;
export declare function SuppliersListFilterToJSON(value?: SuppliersListFilter | null): any;
