/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderStatus } from './';
/**
 *
 * @export
 * @interface OrdersFilter
 */
export interface OrdersFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrdersFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrdersFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrdersFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrdersFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrdersFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrdersFilter
     */
    count?: number;
    /**
     * Orders by list of orderId
     * @type {Array<string>}
     * @memberof OrdersFilter
     */
    orderId?: Array<string>;
    /**
     * Orders by list of extOrderId
     * @type {Array<string>}
     * @memberof OrdersFilter
     */
    extOrderId?: Array<string>;
    /**
     * Orders by list of order sources
     * @type {Array<string>}
     * @memberof OrdersFilter
     */
    orderSource?: Array<string>;
    /**
     * Orders by list of statuses
     * @type {Array<OrderStatus>}
     * @memberof OrdersFilter
     */
    status?: Array<OrderStatus>;
    /**
     * Soft deleted orders
     * @type {boolean}
     * @memberof OrdersFilter
     */
    deleted?: boolean;
}
export declare function OrdersFilterFromJSON(json: any): OrdersFilter;
export declare function OrdersFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersFilter;
export declare function OrdersFilterToJSON(value?: OrdersFilter | null): any;
