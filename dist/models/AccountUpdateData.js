"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountUpdateDataToJSON = exports.AccountUpdateDataFromJSONTyped = exports.AccountUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountUpdateDataFromJSON(json) {
    return AccountUpdateDataFromJSONTyped(json, false);
}
exports.AccountUpdateDataFromJSON = AccountUpdateDataFromJSON;
function AccountUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.AccountStatusFromJSON(json['status']),
        'expirationDate': !runtime_1.exists(json, 'expirationDate') ? undefined : (new Date(json['expirationDate'])),
        'payment': !runtime_1.exists(json, 'payment') ? undefined : json['payment'],
    };
}
exports.AccountUpdateDataFromJSONTyped = AccountUpdateDataFromJSONTyped;
function AccountUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.AccountStatusToJSON(value.status),
        'expirationDate': value.expirationDate === undefined ? undefined : (new Date(value.expirationDate).toISOString().substr(0, 10)),
        'payment': value.payment,
    };
}
exports.AccountUpdateDataToJSON = AccountUpdateDataToJSON;
