/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order, OrdersListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrdersListResponseAllOf
 */
export interface OrdersListResponseAllOf {
    /**
     *
     * @type {OrdersListFilter}
     * @memberof OrdersListResponseAllOf
     */
    filters?: OrdersListFilter;
    /**
     *
     * @type {Array<Order>}
     * @memberof OrdersListResponseAllOf
     */
    orders: Array<Order>;
    /**
     *
     * @type {Pagination}
     * @memberof OrdersListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function OrdersListResponseAllOfFromJSON(json: any): OrdersListResponseAllOf;
export declare function OrdersListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersListResponseAllOf;
export declare function OrdersListResponseAllOfToJSON(value?: OrdersListResponseAllOf | null): any;
