"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyFetchErrorToJSON = exports.ApiKeyFetchErrorFromJSONTyped = exports.ApiKeyFetchErrorFromJSON = exports.ApiKeyFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ApiKeyFetchErrorSuccessEnum;
(function (ApiKeyFetchErrorSuccessEnum) {
    ApiKeyFetchErrorSuccessEnum["False"] = "false";
})(ApiKeyFetchErrorSuccessEnum = exports.ApiKeyFetchErrorSuccessEnum || (exports.ApiKeyFetchErrorSuccessEnum = {}));
function ApiKeyFetchErrorFromJSON(json) {
    return ApiKeyFetchErrorFromJSONTyped(json, false);
}
exports.ApiKeyFetchErrorFromJSON = ApiKeyFetchErrorFromJSON;
function ApiKeyFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.ApiKeyFetchErrorFromJSONTyped = ApiKeyFetchErrorFromJSONTyped;
function ApiKeyFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.ApiKeyFetchErrorToJSON = ApiKeyFetchErrorToJSON;
