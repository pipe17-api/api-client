"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferUpdateLineItemToJSON = exports.TransferUpdateLineItemFromJSONTyped = exports.TransferUpdateLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function TransferUpdateLineItemFromJSON(json) {
    return TransferUpdateLineItemFromJSONTyped(json, false);
}
exports.TransferUpdateLineItemFromJSON = TransferUpdateLineItemFromJSON;
function TransferUpdateLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': !runtime_1.exists(json, 'uniqueId') ? undefined : json['uniqueId'],
        'quantity': !runtime_1.exists(json, 'quantity') ? undefined : json['quantity'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'shippedQuantity': !runtime_1.exists(json, 'shippedQuantity') ? undefined : json['shippedQuantity'],
        'receivedQuantity': !runtime_1.exists(json, 'receivedQuantity') ? undefined : json['receivedQuantity'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
    };
}
exports.TransferUpdateLineItemFromJSONTyped = TransferUpdateLineItemFromJSONTyped;
function TransferUpdateLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'quantity': value.quantity,
        'sku': value.sku,
        'name': value.name,
        'upc': value.upc,
        'shippedQuantity': value.shippedQuantity,
        'receivedQuantity': value.receivedQuantity,
        'locationId': value.locationId,
    };
}
exports.TransferUpdateLineItemToJSON = TransferUpdateLineItemToJSON;
