/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus, RefundType } from './';
/**
 *
 * @export
 * @interface RefundCreateData
 */
export interface RefundCreateData {
    /**
     * Return ID
     * @type {string}
     * @memberof RefundCreateData
     */
    returnId: string;
    /**
     * Refund ID on external system
     * @type {string}
     * @memberof RefundCreateData
     */
    extRefundId?: string;
    /**
     * Order ID on external system
     * @type {string}
     * @memberof RefundCreateData
     */
    extOrderId?: string;
    /**
     *
     * @type {RefundType}
     * @memberof RefundCreateData
     */
    type: RefundType;
    /**
     *
     * @type {RefundStatus}
     * @memberof RefundCreateData
     */
    status?: RefundStatus;
    /**
     * Refund currency
     * @type {string}
     * @memberof RefundCreateData
     */
    currency?: string;
    /**
     * Refund amount
     * @type {number}
     * @memberof RefundCreateData
     */
    amount?: number;
    /**
     * Credit issued to customer
     * @type {number}
     * @memberof RefundCreateData
     */
    creditIssued?: number;
    /**
     * Credit spent by customer
     * @type {number}
     * @memberof RefundCreateData
     */
    creditSpent?: number;
    /**
     * Merchant invoice amount
     * @type {number}
     * @memberof RefundCreateData
     */
    merchantInvoiceAmount?: number;
    /**
     * Customer invoice amount
     * @type {number}
     * @memberof RefundCreateData
     */
    customerInvoiceAmount?: number;
}
export declare function RefundCreateDataFromJSON(json: any): RefundCreateData;
export declare function RefundCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundCreateData;
export declare function RefundCreateDataToJSON(value?: RefundCreateData | null): any;
