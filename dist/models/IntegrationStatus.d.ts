/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Integration Status
 * @export
 * @enum {string}
 */
export declare enum IntegrationStatus {
    Created = "created",
    Configured = "configured",
    Initializing = "initializing",
    Connected = "connected",
    Error = "error",
    Disabled = "disabled"
}
export declare function IntegrationStatusFromJSON(json: any): IntegrationStatus;
export declare function IntegrationStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStatus;
export declare function IntegrationStatusToJSON(value?: IntegrationStatus | null): any;
