"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingFilterToJSON = exports.TrackingFilterFromJSONTyped = exports.TrackingFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingFilterFromJSON(json) {
    return TrackingFilterFromJSONTyped(json, false);
}
exports.TrackingFilterFromJSON = TrackingFilterFromJSON;
function TrackingFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'trackingId': !runtime_1.exists(json, 'trackingId') ? undefined : json['trackingId'],
        'trackingNumber': !runtime_1.exists(json, 'trackingNumber') ? undefined : json['trackingNumber'],
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.TrackingStatusFromJSON)),
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
    };
}
exports.TrackingFilterFromJSONTyped = TrackingFilterFromJSONTyped;
function TrackingFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'trackingId': value.trackingId,
        'trackingNumber': value.trackingNumber,
        'fulfillmentId': value.fulfillmentId,
        'shippingCarrier': value.shippingCarrier,
        'status': value.status === undefined ? undefined : (value.status.map(_1.TrackingStatusToJSON)),
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'order': value.order,
        'keys': value.keys,
    };
}
exports.TrackingFilterToJSON = TrackingFilterToJSON;
