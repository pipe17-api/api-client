/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityFilterUpdateRequest
 */
export interface EntityFilterUpdateRequest {
    /**
     * Filter query object
     * @type {object}
     * @memberof EntityFilterUpdateRequest
     */
    query?: object;
    /**
     * Filter name
     * @type {string}
     * @memberof EntityFilterUpdateRequest
     */
    name?: string;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof EntityFilterUpdateRequest
     */
    isPublic?: boolean;
    /**
     *
     * @type {string}
     * @memberof EntityFilterUpdateRequest
     */
    originId?: string;
}
export declare function EntityFilterUpdateRequestFromJSON(json: any): EntityFilterUpdateRequest;
export declare function EntityFilterUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterUpdateRequest;
export declare function EntityFilterUpdateRequestToJSON(value?: EntityFilterUpdateRequest | null): any;
