/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderUpdateData } from './';
/**
 *
 * @export
 * @interface OrderUpdateErrorAllOf
 */
export interface OrderUpdateErrorAllOf {
    /**
     *
     * @type {OrderUpdateData}
     * @memberof OrderUpdateErrorAllOf
     */
    order?: OrderUpdateData;
}
export declare function OrderUpdateErrorAllOfFromJSON(json: any): OrderUpdateErrorAllOf;
export declare function OrderUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderUpdateErrorAllOf;
export declare function OrderUpdateErrorAllOfToJSON(value?: OrderUpdateErrorAllOf | null): any;
