"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationToJSON = exports.OrganizationFromJSONTyped = exports.OrganizationFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationFromJSON(json) {
    return OrganizationFromJSONTyped(json, false);
}
exports.OrganizationFromJSON = OrganizationFromJSON;
function OrganizationFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.OrganizationStatusFromJSON(json['status']),
        'name': json['name'],
        'tier': _1.OrganizationServiceTierFromJSON(json['tier']),
        'email': json['email'],
        'phone': json['phone'],
        'timeZone': json['timeZone'],
        'logoUrl': !runtime_1.exists(json, 'logoUrl') ? undefined : json['logoUrl'],
        'address': _1.OrganizationAddressFromJSON(json['address']),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.OrganizationFromJSONTyped = OrganizationFromJSONTyped;
function OrganizationToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.OrganizationStatusToJSON(value.status),
        'name': value.name,
        'tier': _1.OrganizationServiceTierToJSON(value.tier),
        'email': value.email,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'logoUrl': value.logoUrl,
        'address': _1.OrganizationAddressToJSON(value.address),
    };
}
exports.OrganizationToJSON = OrganizationToJSON;
