"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserFetchResponseToJSON = exports.UserFetchResponseFromJSONTyped = exports.UserFetchResponseFromJSON = exports.UserFetchResponseCodeEnum = exports.UserFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UserFetchResponseSuccessEnum;
(function (UserFetchResponseSuccessEnum) {
    UserFetchResponseSuccessEnum["True"] = "true";
})(UserFetchResponseSuccessEnum = exports.UserFetchResponseSuccessEnum || (exports.UserFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var UserFetchResponseCodeEnum;
(function (UserFetchResponseCodeEnum) {
    UserFetchResponseCodeEnum[UserFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    UserFetchResponseCodeEnum[UserFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    UserFetchResponseCodeEnum[UserFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(UserFetchResponseCodeEnum = exports.UserFetchResponseCodeEnum || (exports.UserFetchResponseCodeEnum = {}));
function UserFetchResponseFromJSON(json) {
    return UserFetchResponseFromJSONTyped(json, false);
}
exports.UserFetchResponseFromJSON = UserFetchResponseFromJSON;
function UserFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserFromJSON(json['user']),
    };
}
exports.UserFetchResponseFromJSONTyped = UserFetchResponseFromJSONTyped;
function UserFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'user': _1.UserToJSON(value.user),
    };
}
exports.UserFetchResponseToJSON = UserFetchResponseToJSON;
