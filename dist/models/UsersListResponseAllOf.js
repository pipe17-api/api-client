"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersListResponseAllOfToJSON = exports.UsersListResponseAllOfFromJSONTyped = exports.UsersListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UsersListResponseAllOfFromJSON(json) {
    return UsersListResponseAllOfFromJSONTyped(json, false);
}
exports.UsersListResponseAllOfFromJSON = UsersListResponseAllOfFromJSON;
function UsersListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.UsersListFilterFromJSON(json['filters']),
        'users': !runtime_1.exists(json, 'users') ? undefined : (json['users'].map(_1.UserFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.UsersListResponseAllOfFromJSONTyped = UsersListResponseAllOfFromJSONTyped;
function UsersListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.UsersListFilterToJSON(value.filters),
        'users': value.users === undefined ? undefined : (value.users.map(_1.UserToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.UsersListResponseAllOfToJSON = UsersListResponseAllOfToJSON;
