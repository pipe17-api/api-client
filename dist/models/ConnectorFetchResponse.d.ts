/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Connector } from './';
/**
 *
 * @export
 * @interface ConnectorFetchResponse
 */
export interface ConnectorFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ConnectorFetchResponse
     */
    success: ConnectorFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorFetchResponse
     */
    code: ConnectorFetchResponseCodeEnum;
    /**
     *
     * @type {Connector}
     * @memberof ConnectorFetchResponse
     */
    connector?: Connector;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ConnectorFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ConnectorFetchResponseFromJSON(json: any): ConnectorFetchResponse;
export declare function ConnectorFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorFetchResponse;
export declare function ConnectorFetchResponseToJSON(value?: ConnectorFetchResponse | null): any;
