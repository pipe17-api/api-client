/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Account } from './';
/**
 *
 * @export
 * @interface AccountCreateResponseAllOf
 */
export interface AccountCreateResponseAllOf {
    /**
     *
     * @type {Account}
     * @memberof AccountCreateResponseAllOf
     */
    account?: Account;
    /**
     * Account Activation key
     * @type {string}
     * @memberof AccountCreateResponseAllOf
     */
    activationKey?: string;
}
export declare function AccountCreateResponseAllOfFromJSON(json: any): AccountCreateResponseAllOf;
export declare function AccountCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountCreateResponseAllOf;
export declare function AccountCreateResponseAllOfToJSON(value?: AccountCreateResponseAllOf | null): any;
