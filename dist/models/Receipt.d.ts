/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptItem } from './';
/**
 *
 * @export
 * @interface Receipt
 */
export interface Receipt {
    /**
     * Receipt ID
     * @type {string}
     * @memberof Receipt
     */
    receiptId?: string;
    /**
     * External Order ID
     * @type {string}
     * @memberof Receipt
     */
    extOrderId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Receipt
     */
    integration?: string;
    /**
     * Arrival ID
     * @type {string}
     * @memberof Receipt
     */
    arrivalId: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof Receipt
     */
    shippingCarrier?: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof Receipt
     */
    shippingClass?: string;
    /**
     * Currency
     * @type {string}
     * @memberof Receipt
     */
    currency?: string;
    /**
     * Actual arrival date
     * @type {Date}
     * @memberof Receipt
     */
    actualArrivalDate?: Date;
    /**
     *
     * @type {Array<ReceiptItem>}
     * @memberof Receipt
     */
    lineItems?: Array<ReceiptItem>;
    /**
     * Reference to receipt on external system (vendor)
     * @type {string}
     * @memberof Receipt
     */
    extReceiptId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Receipt
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Receipt
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Receipt
     */
    readonly orgKey?: string;
}
export declare function ReceiptFromJSON(json: any): Receipt;
export declare function ReceiptFromJSONTyped(json: any, ignoreDiscriminator: boolean): Receipt;
export declare function ReceiptToJSON(value?: Receipt | null): any;
