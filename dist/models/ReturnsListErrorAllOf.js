"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsListErrorAllOfToJSON = exports.ReturnsListErrorAllOfFromJSONTyped = exports.ReturnsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnsListErrorAllOfFromJSON(json) {
    return ReturnsListErrorAllOfFromJSONTyped(json, false);
}
exports.ReturnsListErrorAllOfFromJSON = ReturnsListErrorAllOfFromJSON;
function ReturnsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReturnsFilterFromJSON(json['filters']),
    };
}
exports.ReturnsListErrorAllOfFromJSONTyped = ReturnsListErrorAllOfFromJSONTyped;
function ReturnsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ReturnsFilterToJSON(value.filters),
    };
}
exports.ReturnsListErrorAllOfToJSON = ReturnsListErrorAllOfToJSON;
