"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationToJSON = exports.LocationFromJSONTyped = exports.LocationFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationFromJSON(json) {
    return LocationFromJSONTyped(json, false);
}
exports.LocationFromJSON = LocationFromJSON;
function LocationFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'name': json['name'],
        'address': _1.LocationAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.LocationStatusFromJSON(json['status']),
        'infinite': !runtime_1.exists(json, 'infinite') ? undefined : json['infinite'],
        'preserveBundles': !runtime_1.exists(json, 'preserveBundles') ? undefined : json['preserveBundles'],
        'fulfillmentIntegrationId': !runtime_1.exists(json, 'fulfillmentIntegrationId') ? undefined : json['fulfillmentIntegrationId'],
        'externalSystem': !runtime_1.exists(json, 'externalSystem') ? undefined : (json['externalSystem'].map(_1.LocationExternalSystemFromJSON)),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.LocationFromJSONTyped = LocationFromJSONTyped;
function LocationToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'locationId': value.locationId,
        'integration': value.integration,
        'name': value.name,
        'address': _1.LocationAddressToJSON(value.address),
        'status': _1.LocationStatusToJSON(value.status),
        'infinite': value.infinite,
        'preserveBundles': value.preserveBundles,
        'fulfillmentIntegrationId': value.fulfillmentIntegrationId,
        'externalSystem': value.externalSystem === undefined ? undefined : (value.externalSystem.map(_1.LocationExternalSystemToJSON)),
    };
}
exports.LocationToJSON = LocationToJSON;
