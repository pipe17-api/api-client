/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventSource, EventType } from './';
/**
 *
 * @export
 * @interface InventoryRule
 */
export interface InventoryRule {
    /**
     * Rule ID
     * @type {string}
     * @memberof InventoryRule
     */
    ruleId?: string;
    /**
     *
     * @type {EventType}
     * @memberof InventoryRule
     */
    event: EventType;
    /**
     *
     * @type {EventSource}
     * @memberof InventoryRule
     */
    ptype?: EventSource;
    /**
     * Event source partner name
     * @type {string}
     * @memberof InventoryRule
     */
    partner?: string;
    /**
     * Event source integration id
     * @type {string}
     * @memberof InventoryRule
     */
    integration?: string;
    /**
     * Description of the rule
     * @type {string}
     * @memberof InventoryRule
     */
    description?: string;
    /**
     * Rule's definition as javascript function
     * @type {string}
     * @memberof InventoryRule
     */
    rule: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof InventoryRule
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof InventoryRule
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof InventoryRule
     */
    readonly orgKey?: string;
}
export declare function InventoryRuleFromJSON(json: any): InventoryRule;
export declare function InventoryRuleFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRule;
export declare function InventoryRuleToJSON(value?: InventoryRule | null): any;
