/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntitySchemasListFilter
 */
export interface EntitySchemasListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof EntitySchemasListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof EntitySchemasListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof EntitySchemasListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof EntitySchemasListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof EntitySchemasListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof EntitySchemasListFilter
     */
    count?: number;
    /**
     * Fetch by list of schemaId
     * @type {Array<string>}
     * @memberof EntitySchemasListFilter
     */
    schemaId?: Array<string>;
    /**
     * Fetch by list of entity types
     * @type {Array<EntityName>}
     * @memberof EntitySchemasListFilter
     */
    entity?: Array<EntityName>;
    /**
     * Fetch by list of schema names
     * @type {Array<string>}
     * @memberof EntitySchemasListFilter
     */
    name?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof EntitySchemasListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof EntitySchemasListFilter
     */
    keys?: string;
}
export declare function EntitySchemasListFilterFromJSON(json: any): EntitySchemasListFilter;
export declare function EntitySchemasListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemasListFilter;
export declare function EntitySchemasListFilterToJSON(value?: EntitySchemasListFilter | null): any;
