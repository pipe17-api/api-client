/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface MappingAllOf
 */
export interface MappingAllOf {
    /**
     * Mapping ID
     * @type {string}
     * @memberof MappingAllOf
     */
    mappingId?: string;
    /**
     * Connector that uses this mapping
     * @type {string}
     * @memberof MappingAllOf
     */
    connectorId?: string;
}
export declare function MappingAllOfFromJSON(json: any): MappingAllOf;
export declare function MappingAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingAllOf;
export declare function MappingAllOfToJSON(value?: MappingAllOf | null): any;
