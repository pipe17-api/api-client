/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Arrivals Filter
 * @export
 * @interface ArrivalsDeleteFilterAllOf
 */
export interface ArrivalsDeleteFilterAllOf {
    /**
     * Arrivals of these integrations
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function ArrivalsDeleteFilterAllOfFromJSON(json: any): ArrivalsDeleteFilterAllOf;
export declare function ArrivalsDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsDeleteFilterAllOf;
export declare function ArrivalsDeleteFilterAllOfToJSON(value?: ArrivalsDeleteFilterAllOf | null): any;
