"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountStatusToJSON = exports.AccountStatusFromJSONTyped = exports.AccountStatusFromJSON = exports.AccountStatus = void 0;
/**
 * Account's Status
 * @export
 * @enum {string}
 */
var AccountStatus;
(function (AccountStatus) {
    AccountStatus["Active"] = "active";
    AccountStatus["Disabled"] = "disabled";
})(AccountStatus = exports.AccountStatus || (exports.AccountStatus = {}));
function AccountStatusFromJSON(json) {
    return AccountStatusFromJSONTyped(json, false);
}
exports.AccountStatusFromJSON = AccountStatusFromJSON;
function AccountStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.AccountStatusFromJSONTyped = AccountStatusFromJSONTyped;
function AccountStatusToJSON(value) {
    return value;
}
exports.AccountStatusToJSON = AccountStatusToJSON;
