/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnsFilter
 */
export interface ReturnsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ReturnsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ReturnsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ReturnsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ReturnsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ReturnsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ReturnsFilter
     */
    count?: number;
    /**
     * Returns by list of returnId
     * @type {Array<string>}
     * @memberof ReturnsFilter
     */
    returnId?: Array<string>;
    /**
     * Returns by list of external return ids
     * @type {Array<string>}
     * @memberof ReturnsFilter
     */
    extReturnId?: Array<string>;
    /**
     * Returns by list of statuses
     * @type {Array<ReturnStatus>}
     * @memberof ReturnsFilter
     */
    status?: Array<ReturnStatus>;
    /**
     * Soft deleted returns
     * @type {boolean}
     * @memberof ReturnsFilter
     */
    deleted?: boolean;
}
export declare function ReturnsFilterFromJSON(json: any): ReturnsFilter;
export declare function ReturnsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsFilter;
export declare function ReturnsFilterToJSON(value?: ReturnsFilter | null): any;
