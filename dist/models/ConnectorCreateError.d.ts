/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorCreateData } from './';
/**
 *
 * @export
 * @interface ConnectorCreateError
 */
export interface ConnectorCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ConnectorCreateError
     */
    success: ConnectorCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ConnectorCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ConnectorCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ConnectorCreateData}
     * @memberof ConnectorCreateError
     */
    connector?: ConnectorCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorCreateErrorSuccessEnum {
    False = "false"
}
export declare function ConnectorCreateErrorFromJSON(json: any): ConnectorCreateError;
export declare function ConnectorCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorCreateError;
export declare function ConnectorCreateErrorToJSON(value?: ConnectorCreateError | null): any;
