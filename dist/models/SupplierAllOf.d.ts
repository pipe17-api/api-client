/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SupplierAllOf
 */
export interface SupplierAllOf {
    /**
     * Supplier ID
     * @type {string}
     * @memberof SupplierAllOf
     */
    supplierId?: string;
}
export declare function SupplierAllOfFromJSON(json: any): SupplierAllOf;
export declare function SupplierAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierAllOf;
export declare function SupplierAllOfToJSON(value?: SupplierAllOf | null): any;
