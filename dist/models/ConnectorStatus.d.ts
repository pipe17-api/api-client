/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Connector Status
 * @export
 * @enum {string}
 */
export declare enum ConnectorStatus {
    Created = "created",
    Active = "active",
    Inactive = "inactive",
    Deleted = "deleted"
}
export declare function ConnectorStatusFromJSON(json: any): ConnectorStatus;
export declare function ConnectorStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorStatus;
export declare function ConnectorStatusToJSON(value?: ConnectorStatus | null): any;
