/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductPublishedItemStatus } from './';
/**
 *
 * @export
 * @interface ProductPublishedItem
 */
export interface ProductPublishedItem {
    /**
     *
     * @type {string}
     * @memberof ProductPublishedItem
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof ProductPublishedItem
     */
    integrationId?: string;
    /**
     *
     * @type {ProductPublishedItemStatus}
     * @memberof ProductPublishedItem
     */
    status?: ProductPublishedItemStatus;
    /**
     * reserved buffer inventory quantity when publishing to this channel
     * @type {number}
     * @memberof ProductPublishedItem
     */
    bufferInventory?: number;
}
export declare function ProductPublishedItemFromJSON(json: any): ProductPublishedItem;
export declare function ProductPublishedItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductPublishedItem;
export declare function ProductPublishedItemToJSON(value?: ProductPublishedItem | null): any;
