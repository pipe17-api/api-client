"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderCustomerToJSON = exports.OrderCustomerFromJSONTyped = exports.OrderCustomerFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrderCustomerFromJSON(json) {
    return OrderCustomerFromJSONTyped(json, false);
}
exports.OrderCustomerFromJSON = OrderCustomerFromJSON;
function OrderCustomerFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'extCustomerId': !runtime_1.exists(json, 'extCustomerId') ? undefined : json['extCustomerId'],
        'firstName': !runtime_1.exists(json, 'firstName') ? undefined : json['firstName'],
        'lastName': !runtime_1.exists(json, 'lastName') ? undefined : json['lastName'],
        'company': !runtime_1.exists(json, 'company') ? undefined : json['company'],
    };
}
exports.OrderCustomerFromJSONTyped = OrderCustomerFromJSONTyped;
function OrderCustomerToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'extCustomerId': value.extCustomerId,
        'firstName': value.firstName,
        'lastName': value.lastName,
        'company': value.company,
    };
}
exports.OrderCustomerToJSON = OrderCustomerToJSON;
