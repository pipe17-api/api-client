/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReturnUpdateResponse
 */
export interface ReturnUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReturnUpdateResponse
     */
    success: ReturnUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnUpdateResponse
     */
    code: ReturnUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReturnUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReturnUpdateResponseFromJSON(json: any): ReturnUpdateResponse;
export declare function ReturnUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnUpdateResponse;
export declare function ReturnUpdateResponseToJSON(value?: ReturnUpdateResponse | null): any;
