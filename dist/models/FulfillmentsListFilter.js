"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsListFilterToJSON = exports.FulfillmentsListFilterFromJSONTyped = exports.FulfillmentsListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
function FulfillmentsListFilterFromJSON(json) {
    return FulfillmentsListFilterFromJSONTyped(json, false);
}
exports.FulfillmentsListFilterFromJSON = FulfillmentsListFilterFromJSON;
function FulfillmentsListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'extShipmentId': !runtime_1.exists(json, 'extShipmentId') ? undefined : json['extShipmentId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
    };
}
exports.FulfillmentsListFilterFromJSONTyped = FulfillmentsListFilterFromJSONTyped;
function FulfillmentsListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'fulfillmentId': value.fulfillmentId,
        'shipmentId': value.shipmentId,
        'extShipmentId': value.extShipmentId,
        'extOrderId': value.extOrderId,
        'order': value.order,
        'keys': value.keys,
    };
}
exports.FulfillmentsListFilterToJSON = FulfillmentsListFilterToJSON;
