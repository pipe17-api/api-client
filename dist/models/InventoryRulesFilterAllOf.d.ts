/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Rules Filter
 * @export
 * @interface InventoryRulesFilterAllOf
 */
export interface InventoryRulesFilterAllOf {
    /**
     * Rules by list of ruleId
     * @type {Array<string>}
     * @memberof InventoryRulesFilterAllOf
     */
    ruleId?: Array<string>;
    /**
     * Rules by list of integration ids
     * @type {Array<string>}
     * @memberof InventoryRulesFilterAllOf
     */
    integration?: Array<string>;
    /**
     * Soft deleted rules
     * @type {boolean}
     * @memberof InventoryRulesFilterAllOf
     */
    deleted?: boolean;
}
export declare function InventoryRulesFilterAllOfFromJSON(json: any): InventoryRulesFilterAllOf;
export declare function InventoryRulesFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesFilterAllOf;
export declare function InventoryRulesFilterAllOfToJSON(value?: InventoryRulesFilterAllOf | null): any;
