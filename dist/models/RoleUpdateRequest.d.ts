/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MethodsUpdateData, RoleStatus } from './';
/**
 *
 * @export
 * @interface RoleUpdateRequest
 */
export interface RoleUpdateRequest {
    /**
     * Role Name
     * @type {string}
     * @memberof RoleUpdateRequest
     */
    name?: string;
    /**
     * Role's description
     * @type {string}
     * @memberof RoleUpdateRequest
     */
    description?: string;
    /**
     *
     * @type {MethodsUpdateData}
     * @memberof RoleUpdateRequest
     */
    methods?: MethodsUpdateData;
    /**
     *
     * @type {RoleStatus}
     * @memberof RoleUpdateRequest
     */
    status?: RoleStatus;
}
export declare function RoleUpdateRequestFromJSON(json: any): RoleUpdateRequest;
export declare function RoleUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleUpdateRequest;
export declare function RoleUpdateRequestToJSON(value?: RoleUpdateRequest | null): any;
