"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersListErrorAllOfToJSON = exports.OrdersListErrorAllOfFromJSONTyped = exports.OrdersListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersListErrorAllOfFromJSON(json) {
    return OrdersListErrorAllOfFromJSONTyped(json, false);
}
exports.OrdersListErrorAllOfFromJSON = OrdersListErrorAllOfFromJSON;
function OrdersListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersListFilterFromJSON(json['filters']),
    };
}
exports.OrdersListErrorAllOfFromJSONTyped = OrdersListErrorAllOfFromJSONTyped;
function OrdersListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.OrdersListFilterToJSON(value.filters),
    };
}
exports.OrdersListErrorAllOfToJSON = OrdersListErrorAllOfToJSON;
