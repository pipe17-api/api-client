/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseStatus } from './';
/**
 *
 * @export
 * @interface PurchasesDeleteFilter
 */
export interface PurchasesDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof PurchasesDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof PurchasesDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof PurchasesDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof PurchasesDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof PurchasesDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof PurchasesDeleteFilter
     */
    count?: number;
    /**
     * Purchases by list of purchaseId
     * @type {Array<string>}
     * @memberof PurchasesDeleteFilter
     */
    purchaseId?: Array<string>;
    /**
     * Purchases by list of external purchase IDs
     * @type {Array<string>}
     * @memberof PurchasesDeleteFilter
     */
    extOrderId?: Array<string>;
    /**
     * Purchases by list of statuses
     * @type {Array<PurchaseStatus>}
     * @memberof PurchasesDeleteFilter
     */
    status?: Array<PurchaseStatus>;
    /**
     * Soft deleted purchases
     * @type {boolean}
     * @memberof PurchasesDeleteFilter
     */
    deleted?: boolean;
    /**
     * Purchases of these integrations
     * @type {Array<string>}
     * @memberof PurchasesDeleteFilter
     */
    integration?: Array<string>;
}
export declare function PurchasesDeleteFilterFromJSON(json: any): PurchasesDeleteFilter;
export declare function PurchasesDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesDeleteFilter;
export declare function PurchasesDeleteFilterToJSON(value?: PurchasesDeleteFilter | null): any;
