/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderCreateResult } from './';
/**
 *
 * @export
 * @interface OrdersCreateError
 */
export interface OrdersCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrdersCreateError
     */
    success: OrdersCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrdersCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrdersCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrdersCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<OrderCreateResult>}
     * @memberof OrdersCreateError
     */
    orders?: Array<OrderCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum OrdersCreateErrorSuccessEnum {
    False = "false"
}
export declare function OrdersCreateErrorFromJSON(json: any): OrdersCreateError;
export declare function OrdersCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersCreateError;
export declare function OrdersCreateErrorToJSON(value?: OrdersCreateError | null): any;
