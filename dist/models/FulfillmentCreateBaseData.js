"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentCreateBaseDataToJSON = exports.FulfillmentCreateBaseDataFromJSONTyped = exports.FulfillmentCreateBaseDataFromJSON = exports.FulfillmentCreateBaseDataWeightUnitEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var FulfillmentCreateBaseDataWeightUnitEnum;
(function (FulfillmentCreateBaseDataWeightUnitEnum) {
    FulfillmentCreateBaseDataWeightUnitEnum["Lb"] = "lb";
    FulfillmentCreateBaseDataWeightUnitEnum["Oz"] = "oz";
    FulfillmentCreateBaseDataWeightUnitEnum["Kg"] = "kg";
    FulfillmentCreateBaseDataWeightUnitEnum["G"] = "g";
})(FulfillmentCreateBaseDataWeightUnitEnum = exports.FulfillmentCreateBaseDataWeightUnitEnum || (exports.FulfillmentCreateBaseDataWeightUnitEnum = {}));
function FulfillmentCreateBaseDataFromJSON(json) {
    return FulfillmentCreateBaseDataFromJSONTyped(json, false);
}
exports.FulfillmentCreateBaseDataFromJSON = FulfillmentCreateBaseDataFromJSON;
function FulfillmentCreateBaseDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'trackingNumber': !runtime_1.exists(json, 'trackingNumber') ? undefined : json['trackingNumber'],
        'actualShipDate': !runtime_1.exists(json, 'actualShipDate') ? undefined : (new Date(json['actualShipDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shippingCharge': !runtime_1.exists(json, 'shippingCharge') ? undefined : json['shippingCharge'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'weight': !runtime_1.exists(json, 'weight') ? undefined : json['weight'],
        'extFulfillmentId': !runtime_1.exists(json, 'extFulfillmentId') ? undefined : json['extFulfillmentId'],
    };
}
exports.FulfillmentCreateBaseDataFromJSONTyped = FulfillmentCreateBaseDataFromJSONTyped;
function FulfillmentCreateBaseDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipmentId': value.shipmentId,
        'trackingNumber': value.trackingNumber,
        'actualShipDate': value.actualShipDate === undefined ? undefined : (new Date(value.actualShipDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'shippingCharge': value.shippingCharge,
        'weightUnit': value.weightUnit,
        'extOrderId': value.extOrderId,
        'weight': value.weight,
        'extFulfillmentId': value.extFulfillmentId,
    };
}
exports.FulfillmentCreateBaseDataToJSON = FulfillmentCreateBaseDataToJSON;
