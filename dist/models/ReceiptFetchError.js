"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptFetchErrorToJSON = exports.ReceiptFetchErrorFromJSONTyped = exports.ReceiptFetchErrorFromJSON = exports.ReceiptFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var ReceiptFetchErrorSuccessEnum;
(function (ReceiptFetchErrorSuccessEnum) {
    ReceiptFetchErrorSuccessEnum["False"] = "false";
})(ReceiptFetchErrorSuccessEnum = exports.ReceiptFetchErrorSuccessEnum || (exports.ReceiptFetchErrorSuccessEnum = {}));
function ReceiptFetchErrorFromJSON(json) {
    return ReceiptFetchErrorFromJSONTyped(json, false);
}
exports.ReceiptFetchErrorFromJSON = ReceiptFetchErrorFromJSON;
function ReceiptFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.ReceiptFetchErrorFromJSONTyped = ReceiptFetchErrorFromJSONTyped;
function ReceiptFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.ReceiptFetchErrorToJSON = ReceiptFetchErrorToJSON;
