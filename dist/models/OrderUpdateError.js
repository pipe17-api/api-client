"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderUpdateErrorToJSON = exports.OrderUpdateErrorFromJSONTyped = exports.OrderUpdateErrorFromJSON = exports.OrderUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderUpdateErrorSuccessEnum;
(function (OrderUpdateErrorSuccessEnum) {
    OrderUpdateErrorSuccessEnum["False"] = "false";
})(OrderUpdateErrorSuccessEnum = exports.OrderUpdateErrorSuccessEnum || (exports.OrderUpdateErrorSuccessEnum = {}));
function OrderUpdateErrorFromJSON(json) {
    return OrderUpdateErrorFromJSONTyped(json, false);
}
exports.OrderUpdateErrorFromJSON = OrderUpdateErrorFromJSON;
function OrderUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'order': !runtime_1.exists(json, 'order') ? undefined : _1.OrderUpdateDataFromJSON(json['order']),
    };
}
exports.OrderUpdateErrorFromJSONTyped = OrderUpdateErrorFromJSONTyped;
function OrderUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'order': _1.OrderUpdateDataToJSON(value.order),
    };
}
exports.OrderUpdateErrorToJSON = OrderUpdateErrorToJSON;
