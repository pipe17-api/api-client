"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountToJSON = exports.AccountFromJSONTyped = exports.AccountFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountFromJSON(json) {
    return AccountFromJSONTyped(json, false);
}
exports.AccountFromJSON = AccountFromJSON;
function AccountFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.AccountStatusFromJSON(json['status']),
        'userId': !runtime_1.exists(json, 'userId') ? undefined : json['userId'],
        'roleIds': !runtime_1.exists(json, 'roleIds') ? undefined : json['roleIds'],
        'integrations': !runtime_1.exists(json, 'integrations') ? undefined : json['integrations'],
        'activationKey': !runtime_1.exists(json, 'activationKey') ? undefined : json['activationKey'],
        'expirationDate': !runtime_1.exists(json, 'expirationDate') ? undefined : (new Date(json['expirationDate'])),
        'payment': !runtime_1.exists(json, 'payment') ? undefined : json['payment'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.AccountFromJSONTyped = AccountFromJSONTyped;
function AccountToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.AccountStatusToJSON(value.status),
        'userId': value.userId,
        'roleIds': value.roleIds,
        'integrations': value.integrations,
        'activationKey': value.activationKey,
        'expirationDate': value.expirationDate === undefined ? undefined : (new Date(value.expirationDate).toISOString().substr(0, 10)),
        'payment': value.payment,
    };
}
exports.AccountToJSON = AccountToJSON;
