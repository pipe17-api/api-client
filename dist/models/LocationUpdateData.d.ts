/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationExternalSystem, LocationStatus, LocationUpdateAddress } from './';
/**
 *
 * @export
 * @interface LocationUpdateData
 */
export interface LocationUpdateData {
    /**
     * Location Name
     * @type {string}
     * @memberof LocationUpdateData
     */
    name?: string;
    /**
     *
     * @type {LocationUpdateAddress}
     * @memberof LocationUpdateData
     */
    address?: LocationUpdateAddress;
    /**
     *
     * @type {LocationStatus}
     * @memberof LocationUpdateData
     */
    status?: LocationStatus;
    /**
     * Set if this location has \"infinite\" availability
     * @type {boolean}
     * @memberof LocationUpdateData
     */
    infinite?: boolean;
    /**
     * Set if shipments to this location should have lineItems referring to bundles kept intact
     * @type {boolean}
     * @memberof LocationUpdateData
     */
    preserveBundles?: boolean;
    /**
     * Set if this is a fulfillment location
     * @type {string}
     * @memberof LocationUpdateData
     */
    fulfillmentIntegrationId?: string | null;
    /**
     *
     * @type {Array<LocationExternalSystem>}
     * @memberof LocationUpdateData
     */
    externalSystem?: Array<LocationExternalSystem>;
}
export declare function LocationUpdateDataFromJSON(json: any): LocationUpdateData;
export declare function LocationUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationUpdateData;
export declare function LocationUpdateDataToJSON(value?: LocationUpdateData | null): any;
