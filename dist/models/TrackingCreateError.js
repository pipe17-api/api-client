"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingCreateErrorToJSON = exports.TrackingCreateErrorFromJSONTyped = exports.TrackingCreateErrorFromJSON = exports.TrackingCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TrackingCreateErrorSuccessEnum;
(function (TrackingCreateErrorSuccessEnum) {
    TrackingCreateErrorSuccessEnum["False"] = "false";
})(TrackingCreateErrorSuccessEnum = exports.TrackingCreateErrorSuccessEnum || (exports.TrackingCreateErrorSuccessEnum = {}));
function TrackingCreateErrorFromJSON(json) {
    return TrackingCreateErrorFromJSONTyped(json, false);
}
exports.TrackingCreateErrorFromJSON = TrackingCreateErrorFromJSON;
function TrackingCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'tracking': !runtime_1.exists(json, 'tracking') ? undefined : _1.TrackingCreateDataFromJSON(json['tracking']),
    };
}
exports.TrackingCreateErrorFromJSONTyped = TrackingCreateErrorFromJSONTyped;
function TrackingCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'tracking': _1.TrackingCreateDataToJSON(value.tracking),
    };
}
exports.TrackingCreateErrorToJSON = TrackingCreateErrorToJSON;
