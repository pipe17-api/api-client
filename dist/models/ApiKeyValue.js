"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyValueToJSON = exports.ApiKeyValueFromJSONTyped = exports.ApiKeyValueFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ApiKeyValueFromJSON(json) {
    return ApiKeyValueFromJSONTyped(json, false);
}
exports.ApiKeyValueFromJSON = ApiKeyValueFromJSON;
function ApiKeyValueFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikeyId': !runtime_1.exists(json, 'apikeyId') ? undefined : json['apikeyId'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : json['apikey'],
        'name': json['name'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'connector': !runtime_1.exists(json, 'connector') ? undefined : json['connector'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'tier': !runtime_1.exists(json, 'tier') ? undefined : _1.TierFromJSON(json['tier']),
        'environment': !runtime_1.exists(json, 'environment') ? undefined : _1.EnvFromJSON(json['environment']),
        'methods': _1.MethodsFromJSON(json['methods']),
        'allowedIPs': json['allowedIPs'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.ApiKeyValueFromJSONTyped = ApiKeyValueFromJSONTyped;
function ApiKeyValueToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikeyId': value.apikeyId,
        'apikey': value.apikey,
        'name': value.name,
        'description': value.description,
        'connector': value.connector,
        'integration': value.integration,
        'tier': _1.TierToJSON(value.tier),
        'environment': _1.EnvToJSON(value.environment),
        'methods': _1.MethodsToJSON(value.methods),
        'allowedIPs': value.allowedIPs,
    };
}
exports.ApiKeyValueToJSON = ApiKeyValueToJSON;
