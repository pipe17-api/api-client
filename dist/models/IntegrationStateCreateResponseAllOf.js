"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateCreateResponseAllOfToJSON = exports.IntegrationStateCreateResponseAllOfFromJSONTyped = exports.IntegrationStateCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationStateCreateResponseAllOfFromJSON(json) {
    return IntegrationStateCreateResponseAllOfFromJSONTyped(json, false);
}
exports.IntegrationStateCreateResponseAllOfFromJSON = IntegrationStateCreateResponseAllOfFromJSON;
function IntegrationStateCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integrationState': !runtime_1.exists(json, 'integrationState') ? undefined : _1.IntegrationStateFromJSON(json['integrationState']),
    };
}
exports.IntegrationStateCreateResponseAllOfFromJSONTyped = IntegrationStateCreateResponseAllOfFromJSONTyped;
function IntegrationStateCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integrationState': _1.IntegrationStateToJSON(value.integrationState),
    };
}
exports.IntegrationStateCreateResponseAllOfToJSON = IntegrationStateCreateResponseAllOfToJSON;
