/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Routings Filter
 * @export
 * @interface RoutingsListFilterAllOf
 */
export interface RoutingsListFilterAllOf {
    /**
     * Fetch all routings matching this description
     * @type {string}
     * @memberof RoutingsListFilterAllOf
     */
    desc?: string;
}
export declare function RoutingsListFilterAllOfFromJSON(json: any): RoutingsListFilterAllOf;
export declare function RoutingsListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsListFilterAllOf;
export declare function RoutingsListFilterAllOfToJSON(value?: RoutingsListFilterAllOf | null): any;
