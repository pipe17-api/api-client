/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Organization, OrganizationsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrganizationsListResponse
 */
export interface OrganizationsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrganizationsListResponse
     */
    success: OrganizationsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationsListResponse
     */
    code: OrganizationsListResponseCodeEnum;
    /**
     *
     * @type {OrganizationsListFilter}
     * @memberof OrganizationsListResponse
     */
    filters?: OrganizationsListFilter;
    /**
     *
     * @type {Array<Organization>}
     * @memberof OrganizationsListResponse
     */
    organizations?: Array<Organization>;
    /**
     *
     * @type {Pagination}
     * @memberof OrganizationsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrganizationsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrganizationsListResponseFromJSON(json: any): OrganizationsListResponse;
export declare function OrganizationsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsListResponse;
export declare function OrganizationsListResponseToJSON(value?: OrganizationsListResponse | null): any;
