/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingUpdateData } from './';
/**
 *
 * @export
 * @interface RoutingUpdateErrorAllOf
 */
export interface RoutingUpdateErrorAllOf {
    /**
     *
     * @type {RoutingUpdateData}
     * @memberof RoutingUpdateErrorAllOf
     */
    routing?: RoutingUpdateData;
}
export declare function RoutingUpdateErrorAllOfFromJSON(json: any): RoutingUpdateErrorAllOf;
export declare function RoutingUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingUpdateErrorAllOf;
export declare function RoutingUpdateErrorAllOfToJSON(value?: RoutingUpdateErrorAllOf | null): any;
