"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsFilterAllOfToJSON = exports.EventsFilterAllOfFromJSONTyped = exports.EventsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EventsFilterAllOfFromJSON(json) {
    return EventsFilterAllOfFromJSONTyped(json, false);
}
exports.EventsFilterAllOfFromJSON = EventsFilterAllOfFromJSON;
function EventsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'eventId': !runtime_1.exists(json, 'eventId') ? undefined : json['eventId'],
        'source': !runtime_1.exists(json, 'source') ? undefined : (json['source'].map(_1.EventRequestSourceFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.EventRequestStatusFromJSON)),
        'requestId': !runtime_1.exists(json, 'requestId') ? undefined : json['requestId'],
    };
}
exports.EventsFilterAllOfFromJSONTyped = EventsFilterAllOfFromJSONTyped;
function EventsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'eventId': value.eventId,
        'source': value.source === undefined ? undefined : (value.source.map(_1.EventRequestSourceToJSON)),
        'status': value.status === undefined ? undefined : (value.status.map(_1.EventRequestStatusToJSON)),
        'requestId': value.requestId,
    };
}
exports.EventsFilterAllOfToJSON = EventsFilterAllOfToJSON;
