/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionFilterUpdateResponse
 */
export interface ExceptionFilterUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionFilterUpdateResponse
     */
    success: ExceptionFilterUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterUpdateResponse
     */
    code: ExceptionFilterUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionFilterUpdateResponseFromJSON(json: any): ExceptionFilterUpdateResponse;
export declare function ExceptionFilterUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterUpdateResponse;
export declare function ExceptionFilterUpdateResponseToJSON(value?: ExceptionFilterUpdateResponse | null): any;
