/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalsListFilter } from './';
/**
 *
 * @export
 * @interface ArrivalsListError
 */
export interface ArrivalsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ArrivalsListError
     */
    success: ArrivalsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ArrivalsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ArrivalsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ArrivalsListFilter}
     * @memberof ArrivalsListError
     */
    filters?: ArrivalsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalsListErrorSuccessEnum {
    False = "false"
}
export declare function ArrivalsListErrorFromJSON(json: any): ArrivalsListError;
export declare function ArrivalsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsListError;
export declare function ArrivalsListErrorToJSON(value?: ArrivalsListError | null): any;
