/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferStatus } from './';
/**
 *
 * @export
 * @interface TransfersListFilter
 */
export interface TransfersListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof TransfersListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof TransfersListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof TransfersListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof TransfersListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof TransfersListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof TransfersListFilter
     */
    count?: number;
    /**
     * Transfers by list of transferId
     * @type {Array<string>}
     * @memberof TransfersListFilter
     */
    transferId?: Array<string>;
    /**
     * Transfers by list of external transfer IDs
     * @type {Array<string>}
     * @memberof TransfersListFilter
     */
    extOrderId?: Array<string>;
    /**
     * Transfers by list of statuses
     * @type {Array<TransferStatus>}
     * @memberof TransfersListFilter
     */
    status?: Array<TransferStatus>;
    /**
     * Soft deleted transfers
     * @type {boolean}
     * @memberof TransfersListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof TransfersListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof TransfersListFilter
     */
    keys?: string;
    /**
     * if set to true use timestamp instead of create time
     * @type {boolean}
     * @memberof TransfersListFilter
     */
    timestamp?: boolean;
}
export declare function TransfersListFilterFromJSON(json: any): TransfersListFilter;
export declare function TransfersListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersListFilter;
export declare function TransfersListFilterToJSON(value?: TransfersListFilter | null): any;
