"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferStatusToJSON = exports.TransferStatusFromJSONTyped = exports.TransferStatusFromJSON = exports.TransferStatus = void 0;
/**
 * Transfer Status
 * @export
 * @enum {string}
 */
var TransferStatus;
(function (TransferStatus) {
    TransferStatus["Draft"] = "draft";
    TransferStatus["New"] = "new";
    TransferStatus["OnHold"] = "onHold";
    TransferStatus["ToBeValidated"] = "toBeValidated";
    TransferStatus["ReviewRequired"] = "reviewRequired";
    TransferStatus["ReadyForFulfillment"] = "readyForFulfillment";
    TransferStatus["SentToFulfillment"] = "sentToFulfillment";
    TransferStatus["PartialFulfillment"] = "partialFulfillment";
    TransferStatus["Fulfilled"] = "fulfilled";
    TransferStatus["InTransit"] = "inTransit";
    TransferStatus["PartialReceived"] = "partialReceived";
    TransferStatus["Received"] = "received";
    TransferStatus["Canceled"] = "canceled";
    TransferStatus["Returned"] = "returned";
    TransferStatus["Refunded"] = "refunded";
})(TransferStatus = exports.TransferStatus || (exports.TransferStatus = {}));
function TransferStatusFromJSON(json) {
    return TransferStatusFromJSONTyped(json, false);
}
exports.TransferStatusFromJSON = TransferStatusFromJSON;
function TransferStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.TransferStatusFromJSONTyped = TransferStatusFromJSONTyped;
function TransferStatusToJSON(value) {
    return value;
}
exports.TransferStatusToJSON = TransferStatusToJSON;
