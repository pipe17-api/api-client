/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 *
 * @export
 * @interface AccountAllOf
 */
export interface AccountAllOf {
    /**
     *
     * @type {AccountStatus}
     * @memberof AccountAllOf
     */
    status?: AccountStatus;
    /**
     * Account User ID
     * @type {string}
     * @memberof AccountAllOf
     */
    userId?: string;
    /**
     * Account Role IDs
     * @type {Array<string>}
     * @memberof AccountAllOf
     */
    roleIds?: Array<string>;
    /**
     * Account Integrations
     * @type {Array<string>}
     * @memberof AccountAllOf
     */
    integrations?: Array<string>;
    /**
     * Account Activation Key
     * @type {string}
     * @memberof AccountAllOf
     */
    activationKey?: string;
    /**
     *
     * @type {Date}
     * @memberof AccountAllOf
     */
    expirationDate?: Date;
    /**
     *
     * @type {object}
     * @memberof AccountAllOf
     */
    payment?: object;
}
export declare function AccountAllOfFromJSON(json: any): AccountAllOf;
export declare function AccountAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountAllOf;
export declare function AccountAllOfToJSON(value?: AccountAllOf | null): any;
