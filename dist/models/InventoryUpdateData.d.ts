/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventSource } from './';
/**
 *
 * @export
 * @interface InventoryUpdateData
 */
export interface InventoryUpdateData {
    /**
     * Event updating inventory
     * @type {string}
     * @memberof InventoryUpdateData
     */
    event: InventoryUpdateDataEventEnum;
    /**
     *
     * @type {EventSource}
     * @memberof InventoryUpdateData
     */
    ptype: EventSource;
    /**
     * Item SKU
     * @type {string}
     * @memberof InventoryUpdateData
     */
    sku: string;
    /**
     * Location Id of item
     * @type {string}
     * @memberof InventoryUpdateData
     */
    locationId: string;
    /**
     * Set if item location has \"infinite\" availability
     * @type {boolean}
     * @memberof InventoryUpdateData
     */
    infinite?: boolean;
    /**
     * Number of items affected
     * @type {number}
     * @memberof InventoryUpdateData
     */
    quantity: number;
    /**
     * Entity id of the record which is directly related to this inventory change
     * @type {string}
     * @memberof InventoryUpdateData
     */
    entityId?: string;
    /**
     * Type of the record which is directly related to this inventory change
     * @type {string}
     * @memberof InventoryUpdateData
     */
    entityType?: InventoryUpdateDataEntityTypeEnum;
    /**
     * Pipe17 internal order id that initiated this inventory change
     * @type {string}
     * @memberof InventoryUpdateData
     */
    orderId?: string;
    /**
     * Type of order that initiated this inventory change
     * @type {string}
     * @memberof InventoryUpdateData
     */
    orderType?: InventoryUpdateDataOrderTypeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateDataEventEnum {
    Ship = "ship",
    Fulfill = "fulfill",
    Xferout = "xferout",
    Xferfulfill = "xferfulfill",
    Xferin = "xferin",
    Receive = "receive",
    FutureShip = "futureShip",
    Release = "release",
    ShipCancel = "shipCancel",
    ShipCancelRestock = "shipCancelRestock",
    FutureShipCancel = "futureShipCancel",
    VirtualCommit = "virtualCommit"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateDataEntityTypeEnum {
    Arrivals = "arrivals",
    Fulfillments = "fulfillments",
    Inventory = "inventory",
    Receipts = "receipts",
    Shipments = "shipments",
    Orders = "orders"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateDataOrderTypeEnum {
    Sales = "sales",
    Transfer = "transfer",
    Purchase = "purchase"
}
export declare function InventoryUpdateDataFromJSON(json: any): InventoryUpdateData;
export declare function InventoryUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryUpdateData;
export declare function InventoryUpdateDataToJSON(value?: InventoryUpdateData | null): any;
