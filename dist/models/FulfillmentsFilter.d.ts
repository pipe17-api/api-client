/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface FulfillmentsFilter
 */
export interface FulfillmentsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof FulfillmentsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof FulfillmentsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof FulfillmentsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof FulfillmentsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof FulfillmentsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof FulfillmentsFilter
     */
    count?: number;
    /**
     * Fulfillments by list of fulfillmentId
     * @type {Array<string>}
     * @memberof FulfillmentsFilter
     */
    fulfillmentId?: Array<string>;
    /**
     * Fulfillments by list of shipmentId
     * @type {Array<string>}
     * @memberof FulfillmentsFilter
     */
    shipmentId?: Array<string>;
    /**
     * Fulfillments by list of extShipmentId
     * @type {Array<string>}
     * @memberof FulfillmentsFilter
     */
    extShipmentId?: Array<string>;
    /**
     * Fulfillments by list of extOrderId
     * @type {Array<string>}
     * @memberof FulfillmentsFilter
     */
    extOrderId?: Array<string>;
}
export declare function FulfillmentsFilterFromJSON(json: any): FulfillmentsFilter;
export declare function FulfillmentsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsFilter;
export declare function FulfillmentsFilterToJSON(value?: FulfillmentsFilter | null): any;
