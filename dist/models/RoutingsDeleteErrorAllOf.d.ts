/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RoutingsDeleteErrorAllOf
 */
export interface RoutingsDeleteErrorAllOf {
    /**
     *
     * @type {RoutingsDeleteFilter}
     * @memberof RoutingsDeleteErrorAllOf
     */
    filters?: RoutingsDeleteFilter;
}
export declare function RoutingsDeleteErrorAllOfFromJSON(json: any): RoutingsDeleteErrorAllOf;
export declare function RoutingsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsDeleteErrorAllOf;
export declare function RoutingsDeleteErrorAllOfToJSON(value?: RoutingsDeleteErrorAllOf | null): any;
