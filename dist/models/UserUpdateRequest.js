"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateRequestToJSON = exports.UserUpdateRequestFromJSONTyped = exports.UserUpdateRequestFromJSON = exports.UserUpdateRequestStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UserUpdateRequestStatusEnum;
(function (UserUpdateRequestStatusEnum) {
    UserUpdateRequestStatusEnum["Active"] = "active";
    UserUpdateRequestStatusEnum["Disabled"] = "disabled";
})(UserUpdateRequestStatusEnum = exports.UserUpdateRequestStatusEnum || (exports.UserUpdateRequestStatusEnum = {}));
function UserUpdateRequestFromJSON(json) {
    return UserUpdateRequestFromJSONTyped(json, false);
}
exports.UserUpdateRequestFromJSON = UserUpdateRequestFromJSON;
function UserUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'roles': !runtime_1.exists(json, 'roles') ? undefined : json['roles'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
        'timeZone': !runtime_1.exists(json, 'timeZone') ? undefined : json['timeZone'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'address': !runtime_1.exists(json, 'address') ? undefined : _1.UserAddressFromJSON(json['address']),
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'password': !runtime_1.exists(json, 'password') ? undefined : json['password'],
    };
}
exports.UserUpdateRequestFromJSONTyped = UserUpdateRequestFromJSONTyped;
function UserUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'roles': value.roles,
        'phone': value.phone,
        'timeZone': value.timeZone,
        'description': value.description,
        'address': _1.UserAddressToJSON(value.address),
        'status': value.status,
        'password': value.password,
    };
}
exports.UserUpdateRequestToJSON = UserUpdateRequestToJSON;
