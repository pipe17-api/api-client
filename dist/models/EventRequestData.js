"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventRequestDataToJSON = exports.EventRequestDataFromJSONTyped = exports.EventRequestDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function EventRequestDataFromJSON(json) {
    return EventRequestDataFromJSONTyped(json, false);
}
exports.EventRequestDataFromJSON = EventRequestDataFromJSON;
function EventRequestDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'url': !runtime_1.exists(json, 'url') ? undefined : json['url'],
        'method': !runtime_1.exists(json, 'method') ? undefined : json['method'],
        'headers': !runtime_1.exists(json, 'headers') ? undefined : json['headers'],
        'authobj': !runtime_1.exists(json, 'authobj') ? undefined : json['authobj'],
        'body': !runtime_1.exists(json, 'body') ? undefined : json['body'],
    };
}
exports.EventRequestDataFromJSONTyped = EventRequestDataFromJSONTyped;
function EventRequestDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'url': value.url,
        'method': value.method,
        'headers': value.headers,
        'authobj': value.authobj,
        'body': value.body,
    };
}
exports.EventRequestDataToJSON = EventRequestDataToJSON;
