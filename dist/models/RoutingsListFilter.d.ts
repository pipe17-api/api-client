/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingsListFilter
 */
export interface RoutingsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RoutingsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RoutingsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RoutingsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RoutingsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RoutingsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RoutingsListFilter
     */
    count?: number;
    /**
     * Routings by list of routingId
     * @type {Array<string>}
     * @memberof RoutingsListFilter
     */
    routingId?: Array<string>;
    /**
     * Routings by list of UUIDs
     * @type {Array<string>}
     * @memberof RoutingsListFilter
     */
    uuid?: Array<string>;
    /**
     * Soft deleted routings
     * @type {boolean}
     * @memberof RoutingsListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof RoutingsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof RoutingsListFilter
     */
    keys?: string;
    /**
     * Fetch all routings matching this description
     * @type {string}
     * @memberof RoutingsListFilter
     */
    desc?: string;
}
export declare function RoutingsListFilterFromJSON(json: any): RoutingsListFilter;
export declare function RoutingsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsListFilter;
export declare function RoutingsListFilterToJSON(value?: RoutingsListFilter | null): any;
