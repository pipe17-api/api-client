"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryFetchResponseToJSON = exports.InventoryFetchResponseFromJSONTyped = exports.InventoryFetchResponseFromJSON = exports.InventoryFetchResponseCodeEnum = exports.InventoryFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryFetchResponseSuccessEnum;
(function (InventoryFetchResponseSuccessEnum) {
    InventoryFetchResponseSuccessEnum["True"] = "true";
})(InventoryFetchResponseSuccessEnum = exports.InventoryFetchResponseSuccessEnum || (exports.InventoryFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryFetchResponseCodeEnum;
(function (InventoryFetchResponseCodeEnum) {
    InventoryFetchResponseCodeEnum[InventoryFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryFetchResponseCodeEnum[InventoryFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryFetchResponseCodeEnum[InventoryFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryFetchResponseCodeEnum = exports.InventoryFetchResponseCodeEnum || (exports.InventoryFetchResponseCodeEnum = {}));
function InventoryFetchResponseFromJSON(json) {
    return InventoryFetchResponseFromJSONTyped(json, false);
}
exports.InventoryFetchResponseFromJSON = InventoryFetchResponseFromJSON;
function InventoryFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : _1.InventoryFromJSON(json['inventory']),
    };
}
exports.InventoryFetchResponseFromJSONTyped = InventoryFetchResponseFromJSONTyped;
function InventoryFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'inventory': _1.InventoryToJSON(value.inventory),
    };
}
exports.InventoryFetchResponseToJSON = InventoryFetchResponseToJSON;
