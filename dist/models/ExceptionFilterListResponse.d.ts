/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilter, ExceptionFiltersListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ExceptionFilterListResponse
 */
export interface ExceptionFilterListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionFilterListResponse
     */
    success: ExceptionFilterListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterListResponse
     */
    code: ExceptionFilterListResponseCodeEnum;
    /**
     *
     * @type {ExceptionFiltersListFilter}
     * @memberof ExceptionFilterListResponse
     */
    filters?: ExceptionFiltersListFilter;
    /**
     *
     * @type {Array<ExceptionFilter>}
     * @memberof ExceptionFilterListResponse
     */
    exceptionFilters?: Array<ExceptionFilter>;
    /**
     *
     * @type {Pagination}
     * @memberof ExceptionFilterListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionFilterListResponseFromJSON(json: any): ExceptionFilterListResponse;
export declare function ExceptionFilterListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterListResponse;
export declare function ExceptionFilterListResponseToJSON(value?: ExceptionFilterListResponse | null): any;
