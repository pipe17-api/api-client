/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ShipmentFetchError
 */
export interface ShipmentFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ShipmentFetchError
     */
    success: ShipmentFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ShipmentFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ShipmentFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentFetchErrorSuccessEnum {
    False = "false"
}
export declare function ShipmentFetchErrorFromJSON(json: any): ShipmentFetchError;
export declare function ShipmentFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentFetchError;
export declare function ShipmentFetchErrorToJSON(value?: ShipmentFetchError | null): any;
