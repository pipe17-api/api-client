"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchaseLineItemToJSON = exports.PurchaseLineItemFromJSONTyped = exports.PurchaseLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function PurchaseLineItemFromJSON(json) {
    return PurchaseLineItemFromJSONTyped(json, false);
}
exports.PurchaseLineItemFromJSON = PurchaseLineItemFromJSON;
function PurchaseLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': json['uniqueId'],
        'partId': !runtime_1.exists(json, 'partId') ? undefined : json['partId'],
        'quantity': json['quantity'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'price': !runtime_1.exists(json, 'price') ? undefined : json['price'],
        'shippedQuantity': !runtime_1.exists(json, 'shippedQuantity') ? undefined : json['shippedQuantity'],
        'receivedQuantity': !runtime_1.exists(json, 'receivedQuantity') ? undefined : json['receivedQuantity'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
    };
}
exports.PurchaseLineItemFromJSONTyped = PurchaseLineItemFromJSONTyped;
function PurchaseLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'partId': value.partId,
        'quantity': value.quantity,
        'sku': value.sku,
        'name': value.name,
        'upc': value.upc,
        'price': value.price,
        'shippedQuantity': value.shippedQuantity,
        'receivedQuantity': value.receivedQuantity,
        'locationId': value.locationId,
    };
}
exports.PurchaseLineItemToJSON = PurchaseLineItemToJSON;
