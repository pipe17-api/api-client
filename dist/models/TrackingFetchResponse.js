"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingFetchResponseToJSON = exports.TrackingFetchResponseFromJSONTyped = exports.TrackingFetchResponseFromJSON = exports.TrackingFetchResponseCodeEnum = exports.TrackingFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TrackingFetchResponseSuccessEnum;
(function (TrackingFetchResponseSuccessEnum) {
    TrackingFetchResponseSuccessEnum["True"] = "true";
})(TrackingFetchResponseSuccessEnum = exports.TrackingFetchResponseSuccessEnum || (exports.TrackingFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TrackingFetchResponseCodeEnum;
(function (TrackingFetchResponseCodeEnum) {
    TrackingFetchResponseCodeEnum[TrackingFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TrackingFetchResponseCodeEnum[TrackingFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TrackingFetchResponseCodeEnum[TrackingFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TrackingFetchResponseCodeEnum = exports.TrackingFetchResponseCodeEnum || (exports.TrackingFetchResponseCodeEnum = {}));
function TrackingFetchResponseFromJSON(json) {
    return TrackingFetchResponseFromJSONTyped(json, false);
}
exports.TrackingFetchResponseFromJSON = TrackingFetchResponseFromJSON;
function TrackingFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'tracking': !runtime_1.exists(json, 'tracking') ? undefined : _1.TrackingFromJSON(json['tracking']),
    };
}
exports.TrackingFetchResponseFromJSONTyped = TrackingFetchResponseFromJSONTyped;
function TrackingFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'tracking': _1.TrackingToJSON(value.tracking),
    };
}
exports.TrackingFetchResponseToJSON = TrackingFetchResponseToJSON;
