/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 *
 * @export
 * @interface AccountsListFilter
 */
export interface AccountsListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof AccountsListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof AccountsListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof AccountsListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof AccountsListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof AccountsListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof AccountsListFilter
     */
    count?: number;
    /**
     * Accounts by list of orgKey
     * @type {Array<string>}
     * @memberof AccountsListFilter
     */
    orgKey?: Array<string>;
    /**
     * Accounts by list of status
     * @type {Array<AccountStatus>}
     * @memberof AccountsListFilter
     */
    status?: Array<AccountStatus>;
    /**
     * List sort order
     * @type {string}
     * @memberof AccountsListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof AccountsListFilter
     */
    keys?: string;
}
export declare function AccountsListFilterFromJSON(json: any): AccountsListFilter;
export declare function AccountsListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsListFilter;
export declare function AccountsListFilterToJSON(value?: AccountsListFilter | null): any;
