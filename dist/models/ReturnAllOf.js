"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnAllOfToJSON = exports.ReturnAllOfFromJSONTyped = exports.ReturnAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnAllOfFromJSON(json) {
    return ReturnAllOfFromJSONTyped(json, false);
}
exports.ReturnAllOfFromJSON = ReturnAllOfFromJSON;
function ReturnAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'returnId': !runtime_1.exists(json, 'returnId') ? undefined : json['returnId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ReturnStatusFromJSON(json['status']),
    };
}
exports.ReturnAllOfFromJSONTyped = ReturnAllOfFromJSONTyped;
function ReturnAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'returnId': value.returnId,
        'integration': value.integration,
        'status': _1.ReturnStatusToJSON(value.status),
    };
}
exports.ReturnAllOfToJSON = ReturnAllOfToJSON;
