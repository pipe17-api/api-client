/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Shipment, ShipmentsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsDeleteResponseAllOf
 */
export interface ShipmentsDeleteResponseAllOf {
    /**
     *
     * @type {ShipmentsDeleteFilter}
     * @memberof ShipmentsDeleteResponseAllOf
     */
    filters?: ShipmentsDeleteFilter;
    /**
     *
     * @type {Array<Shipment>}
     * @memberof ShipmentsDeleteResponseAllOf
     */
    shipments?: Array<Shipment>;
    /**
     * Number of deleted shipments
     * @type {number}
     * @memberof ShipmentsDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ShipmentsDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ShipmentsDeleteResponseAllOfFromJSON(json: any): ShipmentsDeleteResponseAllOf;
export declare function ShipmentsDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsDeleteResponseAllOf;
export declare function ShipmentsDeleteResponseAllOfToJSON(value?: ShipmentsDeleteResponseAllOf | null): any;
