"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorUpdateErrorAllOfToJSON = exports.ConnectorUpdateErrorAllOfFromJSONTyped = exports.ConnectorUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConnectorUpdateErrorAllOfFromJSON(json) {
    return ConnectorUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ConnectorUpdateErrorAllOfFromJSON = ConnectorUpdateErrorAllOfFromJSON;
function ConnectorUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'connector': !runtime_1.exists(json, 'connector') ? undefined : _1.ConnectorUpdateDataFromJSON(json['connector']),
    };
}
exports.ConnectorUpdateErrorAllOfFromJSONTyped = ConnectorUpdateErrorAllOfFromJSONTyped;
function ConnectorUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'connector': _1.ConnectorUpdateDataToJSON(value.connector),
    };
}
exports.ConnectorUpdateErrorAllOfToJSON = ConnectorUpdateErrorAllOfToJSON;
