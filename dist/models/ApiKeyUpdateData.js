"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyUpdateDataToJSON = exports.ApiKeyUpdateDataFromJSONTyped = exports.ApiKeyUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ApiKeyUpdateDataFromJSON(json) {
    return ApiKeyUpdateDataFromJSONTyped(json, false);
}
exports.ApiKeyUpdateDataFromJSON = ApiKeyUpdateDataFromJSON;
function ApiKeyUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'methods': !runtime_1.exists(json, 'methods') ? undefined : _1.MethodsUpdateDataFromJSON(json['methods']),
        'allowedIPs': !runtime_1.exists(json, 'allowedIPs') ? undefined : json['allowedIPs'],
    };
}
exports.ApiKeyUpdateDataFromJSONTyped = ApiKeyUpdateDataFromJSONTyped;
function ApiKeyUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'description': value.description,
        'methods': _1.MethodsUpdateDataToJSON(value.methods),
        'allowedIPs': value.allowedIPs,
    };
}
exports.ApiKeyUpdateDataToJSON = ApiKeyUpdateDataToJSON;
