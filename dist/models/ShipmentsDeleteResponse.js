"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsDeleteResponseToJSON = exports.ShipmentsDeleteResponseFromJSONTyped = exports.ShipmentsDeleteResponseFromJSON = exports.ShipmentsDeleteResponseCodeEnum = exports.ShipmentsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentsDeleteResponseSuccessEnum;
(function (ShipmentsDeleteResponseSuccessEnum) {
    ShipmentsDeleteResponseSuccessEnum["True"] = "true";
})(ShipmentsDeleteResponseSuccessEnum = exports.ShipmentsDeleteResponseSuccessEnum || (exports.ShipmentsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ShipmentsDeleteResponseCodeEnum;
(function (ShipmentsDeleteResponseCodeEnum) {
    ShipmentsDeleteResponseCodeEnum[ShipmentsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ShipmentsDeleteResponseCodeEnum[ShipmentsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ShipmentsDeleteResponseCodeEnum[ShipmentsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ShipmentsDeleteResponseCodeEnum = exports.ShipmentsDeleteResponseCodeEnum || (exports.ShipmentsDeleteResponseCodeEnum = {}));
function ShipmentsDeleteResponseFromJSON(json) {
    return ShipmentsDeleteResponseFromJSONTyped(json, false);
}
exports.ShipmentsDeleteResponseFromJSON = ShipmentsDeleteResponseFromJSON;
function ShipmentsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ShipmentsDeleteFilterFromJSON(json['filters']),
        'shipments': !runtime_1.exists(json, 'shipments') ? undefined : (json['shipments'].map(_1.ShipmentFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ShipmentsDeleteResponseFromJSONTyped = ShipmentsDeleteResponseFromJSONTyped;
function ShipmentsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ShipmentsDeleteFilterToJSON(value.filters),
        'shipments': value.shipments === undefined ? undefined : (value.shipments.map(_1.ShipmentToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ShipmentsDeleteResponseToJSON = ShipmentsDeleteResponseToJSON;
