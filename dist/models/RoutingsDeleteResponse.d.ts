/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Routing, RoutingsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RoutingsDeleteResponse
 */
export interface RoutingsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoutingsDeleteResponse
     */
    success: RoutingsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingsDeleteResponse
     */
    code: RoutingsDeleteResponseCodeEnum;
    /**
     *
     * @type {RoutingsDeleteFilter}
     * @memberof RoutingsDeleteResponse
     */
    filters?: RoutingsDeleteFilter;
    /**
     *
     * @type {Array<Routing>}
     * @memberof RoutingsDeleteResponse
     */
    routings?: Array<Routing>;
    /**
     * Number of deleted routings
     * @type {number}
     * @memberof RoutingsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof RoutingsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoutingsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoutingsDeleteResponseFromJSON(json: any): RoutingsDeleteResponse;
export declare function RoutingsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsDeleteResponse;
export declare function RoutingsDeleteResponseToJSON(value?: RoutingsDeleteResponse | null): any;
