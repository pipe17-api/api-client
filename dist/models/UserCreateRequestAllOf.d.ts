/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface UserCreateRequestAllOf
 */
export interface UserCreateRequestAllOf {
    /**
     * User password
     * @type {string}
     * @memberof UserCreateRequestAllOf
     */
    password: string;
}
export declare function UserCreateRequestAllOfFromJSON(json: any): UserCreateRequestAllOf;
export declare function UserCreateRequestAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserCreateRequestAllOf;
export declare function UserCreateRequestAllOfToJSON(value?: UserCreateRequestAllOf | null): any;
