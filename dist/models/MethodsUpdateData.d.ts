/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Access Methods Allowed
 * @export
 * @interface MethodsUpdateData
 */
export interface MethodsUpdateData {
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    accounts?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    apikey?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    arrivals?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    connectors?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    fulfillments?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    integrations?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    inventory?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    inventoryrules?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    labels?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    locations?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    mappings?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    orders?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    organizations?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    products?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    purchases?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    receipts?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    roles?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    routings?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    shipments?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    suppliers?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    transfers?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    trackings?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    users?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    webhooks?: string | null;
    /**
     *
     * @type {string}
     * @memberof MethodsUpdateData
     */
    convert?: MethodsUpdateDataConvertEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum MethodsUpdateDataConvertEnum {
    C = "c"
}
export declare function MethodsUpdateDataFromJSON(json: any): MethodsUpdateData;
export declare function MethodsUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): MethodsUpdateData;
export declare function MethodsUpdateDataToJSON(value?: MethodsUpdateData | null): any;
