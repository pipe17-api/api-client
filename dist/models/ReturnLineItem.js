"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnLineItemToJSON = exports.ReturnLineItemFromJSONTyped = exports.ReturnLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function ReturnLineItemFromJSON(json) {
    return ReturnLineItemFromJSONTyped(json, false);
}
exports.ReturnLineItemFromJSON = ReturnLineItemFromJSON;
function ReturnLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': json['uniqueId'],
        'extOrderId': json['extOrderId'],
        'sku': json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'quantity': json['quantity'],
        'cause': json['cause'],
        'itemCharge': !runtime_1.exists(json, 'itemCharge') ? undefined : json['itemCharge'],
        'itemShippingCharge': !runtime_1.exists(json, 'itemShippingCharge') ? undefined : json['itemShippingCharge'],
        'itemDiscount': !runtime_1.exists(json, 'itemDiscount') ? undefined : json['itemDiscount'],
        'itemTax': !runtime_1.exists(json, 'itemTax') ? undefined : json['itemTax'],
        'itemTotal': !runtime_1.exists(json, 'itemTotal') ? undefined : json['itemTotal'],
        'itemEstimatedTotal': !runtime_1.exists(json, 'itemEstimatedTotal') ? undefined : json['itemEstimatedTotal'],
        'itemRestockingFee': !runtime_1.exists(json, 'itemRestockingFee') ? undefined : json['itemRestockingFee'],
        'itemLabelCost': !runtime_1.exists(json, 'itemLabelCost') ? undefined : json['itemLabelCost'],
        'itemReturnShippingPaid': !runtime_1.exists(json, 'itemReturnShippingPaid') ? undefined : json['itemReturnShippingPaid'],
    };
}
exports.ReturnLineItemFromJSONTyped = ReturnLineItemFromJSONTyped;
function ReturnLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'extOrderId': value.extOrderId,
        'sku': value.sku,
        'name': value.name,
        'quantity': value.quantity,
        'cause': value.cause,
        'itemCharge': value.itemCharge,
        'itemShippingCharge': value.itemShippingCharge,
        'itemDiscount': value.itemDiscount,
        'itemTax': value.itemTax,
        'itemTotal': value.itemTotal,
        'itemEstimatedTotal': value.itemEstimatedTotal,
        'itemRestockingFee': value.itemRestockingFee,
        'itemLabelCost': value.itemLabelCost,
        'itemReturnShippingPaid': value.itemReturnShippingPaid,
    };
}
exports.ReturnLineItemToJSON = ReturnLineItemToJSON;
