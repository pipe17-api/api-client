/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Account, AccountsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface AccountsListResponse
 */
export interface AccountsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof AccountsListResponse
     */
    success: AccountsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountsListResponse
     */
    code: AccountsListResponseCodeEnum;
    /**
     *
     * @type {AccountsListFilter}
     * @memberof AccountsListResponse
     */
    filters?: AccountsListFilter;
    /**
     *
     * @type {Array<Account>}
     * @memberof AccountsListResponse
     */
    accounts?: Array<Account>;
    /**
     *
     * @type {Pagination}
     * @memberof AccountsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum AccountsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function AccountsListResponseFromJSON(json: any): AccountsListResponse;
export declare function AccountsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsListResponse;
export declare function AccountsListResponseToJSON(value?: AccountsListResponse | null): any;
