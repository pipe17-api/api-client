/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationsListFilter } from './';
/**
 *
 * @export
 * @interface IntegrationsListError
 */
export interface IntegrationsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationsListError
     */
    success: IntegrationsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {IntegrationsListFilter}
     * @memberof IntegrationsListError
     */
    filters?: IntegrationsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationsListErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationsListErrorFromJSON(json: any): IntegrationsListError;
export declare function IntegrationsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsListError;
export declare function IntegrationsListErrorToJSON(value?: IntegrationsListError | null): any;
