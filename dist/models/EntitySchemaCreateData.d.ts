/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntitySchemaCreateData
 */
export interface EntitySchemaCreateData {
    /**
     * Schema name
     * @type {string}
     * @memberof EntitySchemaCreateData
     */
    name: string;
    /**
     * Entity schema field descriptors
     * @type {Array<object>}
     * @memberof EntitySchemaCreateData
     */
    fields: Array<object>;
    /**
     *
     * @type {EntityName}
     * @memberof EntitySchemaCreateData
     */
    entity: EntityName;
}
export declare function EntitySchemaCreateDataFromJSON(json: any): EntitySchemaCreateData;
export declare function EntitySchemaCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaCreateData;
export declare function EntitySchemaCreateDataToJSON(value?: EntitySchemaCreateData | null): any;
