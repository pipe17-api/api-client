/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Order Status
 * @export
 * @enum {string}
 */
export declare enum OrderStatus {
    Draft = "draft",
    New = "new",
    OnHold = "onHold",
    ToBeValidated = "toBeValidated",
    ReviewRequired = "reviewRequired",
    ReadyForFulfillment = "readyForFulfillment",
    SentToFulfillment = "sentToFulfillment",
    PartialFulfillment = "partialFulfillment",
    Fulfilled = "fulfilled",
    InTransit = "inTransit",
    PartialReceived = "partialReceived",
    Received = "received",
    Canceled = "canceled",
    Returned = "returned",
    Refunded = "refunded"
}
export declare function OrderStatusFromJSON(json: any): OrderStatus;
export declare function OrderStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderStatus;
export declare function OrderStatusToJSON(value?: OrderStatus | null): any;
