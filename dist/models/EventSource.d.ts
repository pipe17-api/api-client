/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Event source partner type
 * @export
 * @enum {string}
 */
export declare enum EventSource {
    Wms = "wms",
    Ims = "ims",
    Oms = "oms",
    Portal = "portal"
}
export declare function EventSourceFromJSON(json: any): EventSource;
export declare function EventSourceFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventSource;
export declare function EventSourceToJSON(value?: EventSource | null): any;
