/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationStateCreateData
 */
export interface IntegrationStateCreateData {
    /**
     * State Name
     * @type {string}
     * @memberof IntegrationStateCreateData
     */
    name: string;
    /**
     * State Value (if undefined and state exists already, then delete it)
     * @type {string}
     * @memberof IntegrationStateCreateData
     */
    value?: string;
}
export declare function IntegrationStateCreateDataFromJSON(json: any): IntegrationStateCreateData;
export declare function IntegrationStateCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateCreateData;
export declare function IntegrationStateCreateDataToJSON(value?: IntegrationStateCreateData | null): any;
