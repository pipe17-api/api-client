/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsDeleteError
 */
export interface ShipmentsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ShipmentsDeleteError
     */
    success: ShipmentsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ShipmentsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ShipmentsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ShipmentsDeleteFilter}
     * @memberof ShipmentsDeleteError
     */
    filters?: ShipmentsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function ShipmentsDeleteErrorFromJSON(json: any): ShipmentsDeleteError;
export declare function ShipmentsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsDeleteError;
export declare function ShipmentsDeleteErrorToJSON(value?: ShipmentsDeleteError | null): any;
