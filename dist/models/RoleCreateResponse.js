"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleCreateResponseToJSON = exports.RoleCreateResponseFromJSONTyped = exports.RoleCreateResponseFromJSON = exports.RoleCreateResponseCodeEnum = exports.RoleCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoleCreateResponseSuccessEnum;
(function (RoleCreateResponseSuccessEnum) {
    RoleCreateResponseSuccessEnum["True"] = "true";
})(RoleCreateResponseSuccessEnum = exports.RoleCreateResponseSuccessEnum || (exports.RoleCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoleCreateResponseCodeEnum;
(function (RoleCreateResponseCodeEnum) {
    RoleCreateResponseCodeEnum[RoleCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoleCreateResponseCodeEnum[RoleCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoleCreateResponseCodeEnum[RoleCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoleCreateResponseCodeEnum = exports.RoleCreateResponseCodeEnum || (exports.RoleCreateResponseCodeEnum = {}));
function RoleCreateResponseFromJSON(json) {
    return RoleCreateResponseFromJSONTyped(json, false);
}
exports.RoleCreateResponseFromJSON = RoleCreateResponseFromJSON;
function RoleCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleFromJSON(json['role']),
    };
}
exports.RoleCreateResponseFromJSONTyped = RoleCreateResponseFromJSONTyped;
function RoleCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'role': _1.RoleToJSON(value.role),
    };
}
exports.RoleCreateResponseToJSON = RoleCreateResponseToJSON;
