/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransfersListFilter } from './';
/**
 *
 * @export
 * @interface TransfersListError
 */
export interface TransfersListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TransfersListError
     */
    success: TransfersListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransfersListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TransfersListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TransfersListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {TransfersListFilter}
     * @memberof TransfersListError
     */
    filters?: TransfersListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum TransfersListErrorSuccessEnum {
    False = "false"
}
export declare function TransfersListErrorFromJSON(json: any): TransfersListError;
export declare function TransfersListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersListError;
export declare function TransfersListErrorToJSON(value?: TransfersListError | null): any;
