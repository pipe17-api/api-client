"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterCreateErrorToJSON = exports.ExceptionFilterCreateErrorFromJSONTyped = exports.ExceptionFilterCreateErrorFromJSON = exports.ExceptionFilterCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionFilterCreateErrorSuccessEnum;
(function (ExceptionFilterCreateErrorSuccessEnum) {
    ExceptionFilterCreateErrorSuccessEnum["False"] = "false";
})(ExceptionFilterCreateErrorSuccessEnum = exports.ExceptionFilterCreateErrorSuccessEnum || (exports.ExceptionFilterCreateErrorSuccessEnum = {}));
function ExceptionFilterCreateErrorFromJSON(json) {
    return ExceptionFilterCreateErrorFromJSONTyped(json, false);
}
exports.ExceptionFilterCreateErrorFromJSON = ExceptionFilterCreateErrorFromJSON;
function ExceptionFilterCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'exceptionFilter': !runtime_1.exists(json, 'exceptionFilter') ? undefined : _1.ExceptionFilterCreateDataFromJSON(json['exceptionFilter']),
    };
}
exports.ExceptionFilterCreateErrorFromJSONTyped = ExceptionFilterCreateErrorFromJSONTyped;
function ExceptionFilterCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'exceptionFilter': _1.ExceptionFilterCreateDataToJSON(value.exceptionFilter),
    };
}
exports.ExceptionFilterCreateErrorToJSON = ExceptionFilterCreateErrorToJSON;
