/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorSettingsField } from './';
/**
 * 3rd party's credentials
 * @export
 * @interface ConnectorConnection
 */
export interface ConnectorConnection {
    /**
     * Connection Type
     * @type {string}
     * @memberof ConnectorConnection
     */
    type?: ConnectorConnectionTypeEnum;
    /**
     * Connection redirect URL
     * @type {string}
     * @memberof ConnectorConnection
     */
    url?: string;
    /**
     * correspond to ui fields
     * @type {Array<ConnectorSettingsField>}
     * @memberof ConnectorConnection
     */
    fields?: Array<ConnectorSettingsField> | null;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorConnectionTypeEnum {
    Basic = "basic",
    Apikey = "apikey",
    Appid = "appid",
    Oauth2 = "oauth2"
}
export declare function ConnectorConnectionFromJSON(json: any): ConnectorConnection;
export declare function ConnectorConnectionFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorConnection;
export declare function ConnectorConnectionToJSON(value?: ConnectorConnection | null): any;
