/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ArrivalFetchError
 */
export interface ArrivalFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ArrivalFetchError
     */
    success: ArrivalFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ArrivalFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ArrivalFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalFetchErrorSuccessEnum {
    False = "false"
}
export declare function ArrivalFetchErrorFromJSON(json: any): ArrivalFetchError;
export declare function ArrivalFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalFetchError;
export declare function ArrivalFetchErrorToJSON(value?: ArrivalFetchError | null): any;
