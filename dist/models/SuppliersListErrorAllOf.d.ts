/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SuppliersListFilter } from './';
/**
 *
 * @export
 * @interface SuppliersListErrorAllOf
 */
export interface SuppliersListErrorAllOf {
    /**
     *
     * @type {SuppliersListFilter}
     * @memberof SuppliersListErrorAllOf
     */
    filters?: SuppliersListFilter;
}
export declare function SuppliersListErrorAllOfFromJSON(json: any): SuppliersListErrorAllOf;
export declare function SuppliersListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersListErrorAllOf;
export declare function SuppliersListErrorAllOfToJSON(value?: SuppliersListErrorAllOf | null): any;
