/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionType } from './';
/**
 *
 * @export
 * @interface ExceptionFilterUpdateData
 */
export interface ExceptionFilterUpdateData {
    /**
     *
     * @type {ExceptionType}
     * @memberof ExceptionFilterUpdateData
     */
    exceptionType?: ExceptionType;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilterUpdateData
     */
    enabled?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilterUpdateData
     */
    blocking?: boolean;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof ExceptionFilterUpdateData
     */
    isPublic?: boolean;
    /**
     *
     * @type {object}
     * @memberof ExceptionFilterUpdateData
     */
    settings?: object;
    /**
     *
     * @type {string}
     * @memberof ExceptionFilterUpdateData
     */
    originId?: string;
}
export declare function ExceptionFilterUpdateDataFromJSON(json: any): ExceptionFilterUpdateData;
export declare function ExceptionFilterUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterUpdateData;
export declare function ExceptionFilterUpdateDataToJSON(value?: ExceptionFilterUpdateData | null): any;
