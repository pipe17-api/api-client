"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateErrorToJSON = exports.UserUpdateErrorFromJSONTyped = exports.UserUpdateErrorFromJSON = exports.UserUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var UserUpdateErrorSuccessEnum;
(function (UserUpdateErrorSuccessEnum) {
    UserUpdateErrorSuccessEnum["False"] = "false";
})(UserUpdateErrorSuccessEnum = exports.UserUpdateErrorSuccessEnum || (exports.UserUpdateErrorSuccessEnum = {}));
function UserUpdateErrorFromJSON(json) {
    return UserUpdateErrorFromJSONTyped(json, false);
}
exports.UserUpdateErrorFromJSON = UserUpdateErrorFromJSON;
function UserUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserUpdateDataFromJSON(json['user']),
    };
}
exports.UserUpdateErrorFromJSONTyped = UserUpdateErrorFromJSONTyped;
function UserUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'user': _1.UserUpdateDataToJSON(value.user),
    };
}
exports.UserUpdateErrorToJSON = UserUpdateErrorToJSON;
