/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingUpdateError
 */
export interface OrderRoutingUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrderRoutingUpdateError
     */
    success: OrderRoutingUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrderRoutingUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrderRoutingUpdateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingUpdateErrorSuccessEnum {
    False = "false"
}
export declare function OrderRoutingUpdateErrorFromJSON(json: any): OrderRoutingUpdateError;
export declare function OrderRoutingUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingUpdateError;
export declare function OrderRoutingUpdateErrorToJSON(value?: OrderRoutingUpdateError | null): any;
