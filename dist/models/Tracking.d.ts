/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShippingCarrier, TrackingStatus } from './';
/**
 *
 * @export
 * @interface Tracking
 */
export interface Tracking {
    /**
     *
     * @type {string}
     * @memberof Tracking
     */
    trackingId?: string;
    /**
     *
     * @type {TrackingStatus}
     * @memberof Tracking
     */
    status?: TrackingStatus;
    /**
     *
     * @type {Date}
     * @memberof Tracking
     */
    actualShipDate?: Date;
    /**
     *
     * @type {Date}
     * @memberof Tracking
     */
    actualArrivalDate?: Date;
    /**
     *
     * @type {string}
     * @memberof Tracking
     */
    trackingNumber: string;
    /**
     *
     * @type {string}
     * @memberof Tracking
     */
    fulfillmentId: string;
    /**
     *
     * @type {ShippingCarrier}
     * @memberof Tracking
     */
    shippingCarrier: ShippingCarrier;
    /**
     *
     * @type {string}
     * @memberof Tracking
     */
    url?: string;
    /**
     *
     * @type {Date}
     * @memberof Tracking
     */
    expectedArrivalDate?: Date;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Tracking
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Tracking
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Tracking
     */
    readonly orgKey?: string;
}
export declare function TrackingFromJSON(json: any): Tracking;
export declare function TrackingFromJSONTyped(json: any, ignoreDiscriminator: boolean): Tracking;
export declare function TrackingToJSON(value?: Tracking | null): any;
