"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationAddressToJSON = exports.OrganizationAddressFromJSONTyped = exports.OrganizationAddressFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrganizationAddressFromJSON(json) {
    return OrganizationAddressFromJSONTyped(json, false);
}
exports.OrganizationAddressFromJSON = OrganizationAddressFromJSON;
function OrganizationAddressFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'firstName': json['firstName'],
        'lastName': json['lastName'],
        'company': !runtime_1.exists(json, 'company') ? undefined : json['company'],
        'address1': json['address1'],
        'address2': !runtime_1.exists(json, 'address2') ? undefined : json['address2'],
        'city': json['city'],
        'stateOrProvince': !runtime_1.exists(json, 'stateOrProvince') ? undefined : json['stateOrProvince'],
        'zipCodeOrPostalCode': json['zipCodeOrPostalCode'],
        'country': json['country'],
    };
}
exports.OrganizationAddressFromJSONTyped = OrganizationAddressFromJSONTyped;
function OrganizationAddressToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'firstName': value.firstName,
        'lastName': value.lastName,
        'company': value.company,
        'address1': value.address1,
        'address2': value.address2,
        'city': value.city,
        'stateOrProvince': value.stateOrProvince,
        'zipCodeOrPostalCode': value.zipCodeOrPostalCode,
        'country': value.country,
    };
}
exports.OrganizationAddressToJSON = OrganizationAddressToJSON;
