"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingUpdateRuleToJSON = exports.MappingUpdateRuleFromJSONTyped = exports.MappingUpdateRuleFromJSON = exports.MappingUpdateRuleTypeEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var MappingUpdateRuleTypeEnum;
(function (MappingUpdateRuleTypeEnum) {
    MappingUpdateRuleTypeEnum["String"] = "string";
    MappingUpdateRuleTypeEnum["Number"] = "number";
    MappingUpdateRuleTypeEnum["Boolean"] = "boolean";
    MappingUpdateRuleTypeEnum["Date"] = "date";
})(MappingUpdateRuleTypeEnum = exports.MappingUpdateRuleTypeEnum || (exports.MappingUpdateRuleTypeEnum = {}));
function MappingUpdateRuleFromJSON(json) {
    return MappingUpdateRuleFromJSONTyped(json, false);
}
exports.MappingUpdateRuleFromJSON = MappingUpdateRuleFromJSON;
function MappingUpdateRuleFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'enabled': !runtime_1.exists(json, 'enabled') ? undefined : json['enabled'],
        'source': !runtime_1.exists(json, 'source') ? undefined : json['source'],
        'target': !runtime_1.exists(json, 'target') ? undefined : json['target'],
        'type': !runtime_1.exists(json, 'type') ? undefined : json['type'],
        'require': !runtime_1.exists(json, 'require') ? undefined : json['require'],
        'missing': !runtime_1.exists(json, 'missing') ? undefined : json['missing'],
        'default': !runtime_1.exists(json, 'default') ? undefined : json['default'],
        'lookup': !runtime_1.exists(json, 'lookup') ? undefined : json['lookup'],
    };
}
exports.MappingUpdateRuleFromJSONTyped = MappingUpdateRuleFromJSONTyped;
function MappingUpdateRuleToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'enabled': value.enabled,
        'source': value.source,
        'target': value.target,
        'type': value.type,
        'require': value.require,
        'missing': value.missing,
        'default': value.default,
        'lookup': value.lookup,
    };
}
exports.MappingUpdateRuleToJSON = MappingUpdateRuleToJSON;
