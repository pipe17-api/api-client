"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentCreateDataToJSON = exports.FulfillmentCreateDataFromJSONTyped = exports.FulfillmentCreateDataFromJSON = exports.FulfillmentCreateDataWeightUnitEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentCreateDataWeightUnitEnum;
(function (FulfillmentCreateDataWeightUnitEnum) {
    FulfillmentCreateDataWeightUnitEnum["Lb"] = "lb";
    FulfillmentCreateDataWeightUnitEnum["Oz"] = "oz";
    FulfillmentCreateDataWeightUnitEnum["Kg"] = "kg";
    FulfillmentCreateDataWeightUnitEnum["G"] = "g";
})(FulfillmentCreateDataWeightUnitEnum = exports.FulfillmentCreateDataWeightUnitEnum || (exports.FulfillmentCreateDataWeightUnitEnum = {}));
function FulfillmentCreateDataFromJSON(json) {
    return FulfillmentCreateDataFromJSONTyped(json, false);
}
exports.FulfillmentCreateDataFromJSON = FulfillmentCreateDataFromJSON;
function FulfillmentCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.FulfillmentItemFromJSON)),
        'shipmentId': json['shipmentId'],
        'trackingNumber': json['trackingNumber'],
        'actualShipDate': !runtime_1.exists(json, 'actualShipDate') ? undefined : (new Date(json['actualShipDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shippingCharge': !runtime_1.exists(json, 'shippingCharge') ? undefined : json['shippingCharge'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'weight': !runtime_1.exists(json, 'weight') ? undefined : json['weight'],
        'extFulfillmentId': !runtime_1.exists(json, 'extFulfillmentId') ? undefined : json['extFulfillmentId'],
    };
}
exports.FulfillmentCreateDataFromJSONTyped = FulfillmentCreateDataFromJSONTyped;
function FulfillmentCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.FulfillmentItemToJSON)),
        'shipmentId': value.shipmentId,
        'trackingNumber': value.trackingNumber,
        'actualShipDate': value.actualShipDate === undefined ? undefined : (new Date(value.actualShipDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'shippingCharge': value.shippingCharge,
        'weightUnit': value.weightUnit,
        'extOrderId': value.extOrderId,
        'weight': value.weight,
        'extFulfillmentId': value.extFulfillmentId,
    };
}
exports.FulfillmentCreateDataToJSON = FulfillmentCreateDataToJSON;
