"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderUpdateErrorAllOfToJSON = exports.OrderUpdateErrorAllOfFromJSONTyped = exports.OrderUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrderUpdateErrorAllOfFromJSON(json) {
    return OrderUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.OrderUpdateErrorAllOfFromJSON = OrderUpdateErrorAllOfFromJSON;
function OrderUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'order': !runtime_1.exists(json, 'order') ? undefined : _1.OrderUpdateDataFromJSON(json['order']),
    };
}
exports.OrderUpdateErrorAllOfFromJSONTyped = OrderUpdateErrorAllOfFromJSONTyped;
function OrderUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'order': _1.OrderUpdateDataToJSON(value.order),
    };
}
exports.OrderUpdateErrorAllOfToJSON = OrderUpdateErrorAllOfToJSON;
