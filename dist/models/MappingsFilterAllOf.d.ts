/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Mappings Filter
 * @export
 * @interface MappingsFilterAllOf
 */
export interface MappingsFilterAllOf {
    /**
     * Mappings by list of mappingId
     * @type {Array<string>}
     * @memberof MappingsFilterAllOf
     */
    mappingId?: Array<string>;
    /**
     * Mappings by list of UUIDs
     * @type {Array<string>}
     * @memberof MappingsFilterAllOf
     */
    uuid?: Array<string>;
    /**
     * Fetch all mappings matching this description
     * @type {string}
     * @memberof MappingsFilterAllOf
     */
    desc?: string;
}
export declare function MappingsFilterAllOfFromJSON(json: any): MappingsFilterAllOf;
export declare function MappingsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsFilterAllOf;
export declare function MappingsFilterAllOfToJSON(value?: MappingsFilterAllOf | null): any;
