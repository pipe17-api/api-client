"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingsListResponseToJSON = exports.MappingsListResponseFromJSONTyped = exports.MappingsListResponseFromJSON = exports.MappingsListResponseCodeEnum = exports.MappingsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var MappingsListResponseSuccessEnum;
(function (MappingsListResponseSuccessEnum) {
    MappingsListResponseSuccessEnum["True"] = "true";
})(MappingsListResponseSuccessEnum = exports.MappingsListResponseSuccessEnum || (exports.MappingsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var MappingsListResponseCodeEnum;
(function (MappingsListResponseCodeEnum) {
    MappingsListResponseCodeEnum[MappingsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    MappingsListResponseCodeEnum[MappingsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    MappingsListResponseCodeEnum[MappingsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(MappingsListResponseCodeEnum = exports.MappingsListResponseCodeEnum || (exports.MappingsListResponseCodeEnum = {}));
function MappingsListResponseFromJSON(json) {
    return MappingsListResponseFromJSONTyped(json, false);
}
exports.MappingsListResponseFromJSON = MappingsListResponseFromJSON;
function MappingsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.MappingsListFilterFromJSON(json['filters']),
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.MappingsListResponseFromJSONTyped = MappingsListResponseFromJSONTyped;
function MappingsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.MappingsListFilterToJSON(value.filters),
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.MappingsListResponseToJSON = MappingsListResponseToJSON;
