/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SupplierUpdateResponse
 */
export interface SupplierUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof SupplierUpdateResponse
     */
    success: SupplierUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SupplierUpdateResponse
     */
    code: SupplierUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum SupplierUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum SupplierUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function SupplierUpdateResponseFromJSON(json: any): SupplierUpdateResponse;
export declare function SupplierUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierUpdateResponse;
export declare function SupplierUpdateResponseToJSON(value?: SupplierUpdateResponse | null): any;
