/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus, RefundType } from './';
/**
 * Refunds Filter
 * @export
 * @interface RefundsFilterAllOf
 */
export interface RefundsFilterAllOf {
    /**
     * Refunds by list of refundId
     * @type {Array<string>}
     * @memberof RefundsFilterAllOf
     */
    refundId?: Array<string>;
    /**
     * Refunds by list of external refund ids
     * @type {Array<string>}
     * @memberof RefundsFilterAllOf
     */
    extRefundId?: Array<string>;
    /**
     * Refunds by list of external refund ids
     * @type {Array<string>}
     * @memberof RefundsFilterAllOf
     */
    extReturnId?: Array<string>;
    /**
     * Refunds by list of external order ids
     * @type {Array<string>}
     * @memberof RefundsFilterAllOf
     */
    extOrderId?: Array<string>;
    /**
     * Refunds by list of types
     * @type {Array<RefundType>}
     * @memberof RefundsFilterAllOf
     */
    type?: Array<RefundType>;
    /**
     * Refunds by list of statuses
     * @type {Array<RefundStatus>}
     * @memberof RefundsFilterAllOf
     */
    status?: Array<RefundStatus>;
    /**
     * Soft deleted refunds
     * @type {boolean}
     * @memberof RefundsFilterAllOf
     */
    deleted?: boolean;
}
export declare function RefundsFilterAllOfFromJSON(json: any): RefundsFilterAllOf;
export declare function RefundsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsFilterAllOf;
export declare function RefundsFilterAllOfToJSON(value?: RefundsFilterAllOf | null): any;
