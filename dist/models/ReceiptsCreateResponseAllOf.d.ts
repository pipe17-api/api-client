/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptCreateResult } from './';
/**
 *
 * @export
 * @interface ReceiptsCreateResponseAllOf
 */
export interface ReceiptsCreateResponseAllOf {
    /**
     *
     * @type {Array<ReceiptCreateResult>}
     * @memberof ReceiptsCreateResponseAllOf
     */
    receipts?: Array<ReceiptCreateResult>;
}
export declare function ReceiptsCreateResponseAllOfFromJSON(json: any): ReceiptsCreateResponseAllOf;
export declare function ReceiptsCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsCreateResponseAllOf;
export declare function ReceiptsCreateResponseAllOfToJSON(value?: ReceiptsCreateResponseAllOf | null): any;
