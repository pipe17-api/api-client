/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ArrivalLineItem, ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalCreateData
 */
export interface ArrivalCreateData {
    /**
     * Transfer Order ID
     * @type {string}
     * @memberof ArrivalCreateData
     */
    transferId?: string;
    /**
     * Purchase Order ID
     * @type {string}
     * @memberof ArrivalCreateData
     */
    purchaseId?: string;
    /**
     * Fulfillment ID
     * @type {string}
     * @memberof ArrivalCreateData
     */
    fulfillmentId?: string;
    /**
     * External TO or PO ID
     * @type {string}
     * @memberof ArrivalCreateData
     */
    extOrderId?: string;
    /**
     * Sender's Name
     * @type {string}
     * @memberof ArrivalCreateData
     */
    senderName: string;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof ArrivalCreateData
     */
    expectedArrivalDate?: Date;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof ArrivalCreateData
     */
    toLocationId: string;
    /**
     * Sender Location ID (for TO)
     * @type {string}
     * @memberof ArrivalCreateData
     */
    fromLocationId?: string;
    /**
     *
     * @type {Address}
     * @memberof ArrivalCreateData
     */
    fromAddress?: Address;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof ArrivalCreateData
     */
    shippingCarrier?: string;
    /**
     * Shipping Method
     * @type {string}
     * @memberof ArrivalCreateData
     */
    shippingMethod?: string;
    /**
     * Shipping Note
     * @type {string}
     * @memberof ArrivalCreateData
     */
    shippingNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof ArrivalCreateData
     */
    incoterms?: string;
    /**
     * Total Weight
     * @type {string}
     * @memberof ArrivalCreateData
     */
    totalWeight?: string;
    /**
     * Weight Unit
     * @type {string}
     * @memberof ArrivalCreateData
     */
    weightUnit?: string;
    /**
     *
     * @type {Array<ArrivalLineItem>}
     * @memberof ArrivalCreateData
     */
    lineItems?: Array<ArrivalLineItem>;
    /**
     *
     * @type {ArrivalStatus}
     * @memberof ArrivalCreateData
     */
    status?: ArrivalStatus;
    /**
     * Reference to arrival on external system (vendor)
     * @type {string}
     * @memberof ArrivalCreateData
     */
    extArrivalId?: string;
}
export declare function ArrivalCreateDataFromJSON(json: any): ArrivalCreateData;
export declare function ArrivalCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalCreateData;
export declare function ArrivalCreateDataToJSON(value?: ArrivalCreateData | null): any;
