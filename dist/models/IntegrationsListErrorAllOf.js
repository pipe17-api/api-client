"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationsListErrorAllOfToJSON = exports.IntegrationsListErrorAllOfFromJSONTyped = exports.IntegrationsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationsListErrorAllOfFromJSON(json) {
    return IntegrationsListErrorAllOfFromJSONTyped(json, false);
}
exports.IntegrationsListErrorAllOfFromJSON = IntegrationsListErrorAllOfFromJSON;
function IntegrationsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationsListFilterFromJSON(json['filters']),
    };
}
exports.IntegrationsListErrorAllOfFromJSONTyped = IntegrationsListErrorAllOfFromJSONTyped;
function IntegrationsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.IntegrationsListFilterToJSON(value.filters),
    };
}
exports.IntegrationsListErrorAllOfToJSON = IntegrationsListErrorAllOfToJSON;
