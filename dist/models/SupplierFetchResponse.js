"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierFetchResponseToJSON = exports.SupplierFetchResponseFromJSONTyped = exports.SupplierFetchResponseFromJSON = exports.SupplierFetchResponseCodeEnum = exports.SupplierFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var SupplierFetchResponseSuccessEnum;
(function (SupplierFetchResponseSuccessEnum) {
    SupplierFetchResponseSuccessEnum["True"] = "true";
})(SupplierFetchResponseSuccessEnum = exports.SupplierFetchResponseSuccessEnum || (exports.SupplierFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var SupplierFetchResponseCodeEnum;
(function (SupplierFetchResponseCodeEnum) {
    SupplierFetchResponseCodeEnum[SupplierFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    SupplierFetchResponseCodeEnum[SupplierFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    SupplierFetchResponseCodeEnum[SupplierFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(SupplierFetchResponseCodeEnum = exports.SupplierFetchResponseCodeEnum || (exports.SupplierFetchResponseCodeEnum = {}));
function SupplierFetchResponseFromJSON(json) {
    return SupplierFetchResponseFromJSONTyped(json, false);
}
exports.SupplierFetchResponseFromJSON = SupplierFetchResponseFromJSON;
function SupplierFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierFromJSON(json['supplier']),
    };
}
exports.SupplierFetchResponseFromJSONTyped = SupplierFetchResponseFromJSONTyped;
function SupplierFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'supplier': _1.SupplierToJSON(value.supplier),
    };
}
exports.SupplierFetchResponseToJSON = SupplierFetchResponseToJSON;
