/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ListResultOrderKeys
 */
export interface ListResultOrderKeys {
    /**
     * List sort order
     * @type {string}
     * @memberof ListResultOrderKeys
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ListResultOrderKeys
     */
    keys?: string;
}
export declare function ListResultOrderKeysFromJSON(json: any): ListResultOrderKeys;
export declare function ListResultOrderKeysFromJSONTyped(json: any, ignoreDiscriminator: boolean): ListResultOrderKeys;
export declare function ListResultOrderKeysToJSON(value?: ListResultOrderKeys | null): any;
