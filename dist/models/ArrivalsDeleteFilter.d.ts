/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalsDeleteFilter
 */
export interface ArrivalsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ArrivalsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ArrivalsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ArrivalsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ArrivalsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ArrivalsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ArrivalsDeleteFilter
     */
    count?: number;
    /**
     * Arrivals by list of arrivalId
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilter
     */
    arrivalId?: Array<string>;
    /**
     * Arrivals by list transfer orders
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilter
     */
    transferId?: Array<string>;
    /**
     * Arrivals by list purchase orders
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilter
     */
    purchaseId?: Array<string>;
    /**
     * Arrivals by list external orders (PO or TO)
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilter
     */
    extOrderId?: Array<string>;
    /**
     * Arrivals by list fulfillmentId
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilter
     */
    fulfillmentId?: Array<string>;
    /**
     * Arrivals in specific status
     * @type {Array<ArrivalStatus>}
     * @memberof ArrivalsDeleteFilter
     */
    status?: Array<ArrivalStatus>;
    /**
     * Soft deleted arrivals
     * @type {boolean}
     * @memberof ArrivalsDeleteFilter
     */
    deleted?: boolean;
    /**
     * Arrivals of these integrations
     * @type {Array<string>}
     * @memberof ArrivalsDeleteFilter
     */
    integration?: Array<string>;
}
export declare function ArrivalsDeleteFilterFromJSON(json: any): ArrivalsDeleteFilter;
export declare function ArrivalsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsDeleteFilter;
export declare function ArrivalsDeleteFilterToJSON(value?: ArrivalsDeleteFilter | null): any;
