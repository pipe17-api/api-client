"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorCreateErrorAllOfToJSON = exports.ConnectorCreateErrorAllOfFromJSONTyped = exports.ConnectorCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConnectorCreateErrorAllOfFromJSON(json) {
    return ConnectorCreateErrorAllOfFromJSONTyped(json, false);
}
exports.ConnectorCreateErrorAllOfFromJSON = ConnectorCreateErrorAllOfFromJSON;
function ConnectorCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'connector': !runtime_1.exists(json, 'connector') ? undefined : _1.ConnectorCreateDataFromJSON(json['connector']),
    };
}
exports.ConnectorCreateErrorAllOfFromJSONTyped = ConnectorCreateErrorAllOfFromJSONTyped;
function ConnectorCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'connector': _1.ConnectorCreateDataToJSON(value.connector),
    };
}
exports.ConnectorCreateErrorAllOfToJSON = ConnectorCreateErrorAllOfToJSON;
