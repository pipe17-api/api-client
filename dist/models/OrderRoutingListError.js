"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingListErrorToJSON = exports.OrderRoutingListErrorFromJSONTyped = exports.OrderRoutingListErrorFromJSON = exports.OrderRoutingListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderRoutingListErrorSuccessEnum;
(function (OrderRoutingListErrorSuccessEnum) {
    OrderRoutingListErrorSuccessEnum["False"] = "false";
})(OrderRoutingListErrorSuccessEnum = exports.OrderRoutingListErrorSuccessEnum || (exports.OrderRoutingListErrorSuccessEnum = {}));
function OrderRoutingListErrorFromJSON(json) {
    return OrderRoutingListErrorFromJSONTyped(json, false);
}
exports.OrderRoutingListErrorFromJSON = OrderRoutingListErrorFromJSON;
function OrderRoutingListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrderRoutingsListFilterFromJSON(json['filters']),
    };
}
exports.OrderRoutingListErrorFromJSONTyped = OrderRoutingListErrorFromJSONTyped;
function OrderRoutingListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.OrderRoutingsListFilterToJSON(value.filters),
    };
}
exports.OrderRoutingListErrorToJSON = OrderRoutingListErrorToJSON;
