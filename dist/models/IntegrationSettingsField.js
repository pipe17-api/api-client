"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationSettingsFieldToJSON = exports.IntegrationSettingsFieldFromJSONTyped = exports.IntegrationSettingsFieldFromJSON = void 0;
const runtime_1 = require("../runtime");
function IntegrationSettingsFieldFromJSON(json) {
    return IntegrationSettingsFieldFromJSONTyped(json, false);
}
exports.IntegrationSettingsFieldFromJSON = IntegrationSettingsFieldFromJSON;
function IntegrationSettingsFieldFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'path': !runtime_1.exists(json, 'path') ? undefined : json['path'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
    };
}
exports.IntegrationSettingsFieldFromJSONTyped = IntegrationSettingsFieldFromJSONTyped;
function IntegrationSettingsFieldToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'path': value.path,
        'value': value.value,
    };
}
exports.IntegrationSettingsFieldToJSON = IntegrationSettingsFieldToJSON;
