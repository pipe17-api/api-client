"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleToJSON = exports.InventoryRuleFromJSONTyped = exports.InventoryRuleFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryRuleFromJSON(json) {
    return InventoryRuleFromJSONTyped(json, false);
}
exports.InventoryRuleFromJSON = InventoryRuleFromJSON;
function InventoryRuleFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'ruleId': !runtime_1.exists(json, 'ruleId') ? undefined : json['ruleId'],
        'event': _1.EventTypeFromJSON(json['event']),
        'ptype': !runtime_1.exists(json, 'ptype') ? undefined : _1.EventSourceFromJSON(json['ptype']),
        'partner': !runtime_1.exists(json, 'partner') ? undefined : json['partner'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'rule': json['rule'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.InventoryRuleFromJSONTyped = InventoryRuleFromJSONTyped;
function InventoryRuleToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'ruleId': value.ruleId,
        'event': _1.EventTypeToJSON(value.event),
        'ptype': _1.EventSourceToJSON(value.ptype),
        'partner': value.partner,
        'integration': value.integration,
        'description': value.description,
        'rule': value.rule,
    };
}
exports.InventoryRuleToJSON = InventoryRuleToJSON;
