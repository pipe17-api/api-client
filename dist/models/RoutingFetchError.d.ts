/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingFetchError
 */
export interface RoutingFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoutingFetchError
     */
    success: RoutingFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoutingFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoutingFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingFetchErrorSuccessEnum {
    False = "false"
}
export declare function RoutingFetchErrorFromJSON(json: any): RoutingFetchError;
export declare function RoutingFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingFetchError;
export declare function RoutingFetchErrorToJSON(value?: RoutingFetchError | null): any;
