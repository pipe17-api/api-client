/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Purchase, PurchasesListFilter } from './';
/**
 *
 * @export
 * @interface PurchasesListResponseAllOf
 */
export interface PurchasesListResponseAllOf {
    /**
     *
     * @type {PurchasesListFilter}
     * @memberof PurchasesListResponseAllOf
     */
    filters?: PurchasesListFilter;
    /**
     *
     * @type {Array<Purchase>}
     * @memberof PurchasesListResponseAllOf
     */
    purchases: Array<Purchase>;
    /**
     *
     * @type {Pagination}
     * @memberof PurchasesListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function PurchasesListResponseAllOfFromJSON(json: any): PurchasesListResponseAllOf;
export declare function PurchasesListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesListResponseAllOf;
export declare function PurchasesListResponseAllOfToJSON(value?: PurchasesListResponseAllOf | null): any;
