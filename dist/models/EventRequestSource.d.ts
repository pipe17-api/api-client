/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Event Type
 * @export
 * @enum {string}
 */
export declare enum EventRequestSource {
    Api = "api",
    ApiWebhook = "api-webhook",
    Custom = "custom"
}
export declare function EventRequestSourceFromJSON(json: any): EventRequestSource;
export declare function EventRequestSourceFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventRequestSource;
export declare function EventRequestSourceToJSON(value?: EventRequestSource | null): any;
