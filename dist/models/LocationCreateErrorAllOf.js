"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationCreateErrorAllOfToJSON = exports.LocationCreateErrorAllOfFromJSONTyped = exports.LocationCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationCreateErrorAllOfFromJSON(json) {
    return LocationCreateErrorAllOfFromJSONTyped(json, false);
}
exports.LocationCreateErrorAllOfFromJSON = LocationCreateErrorAllOfFromJSON;
function LocationCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationCreateDataFromJSON(json['location']),
    };
}
exports.LocationCreateErrorAllOfFromJSONTyped = LocationCreateErrorAllOfFromJSONTyped;
function LocationCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'location': _1.LocationCreateDataToJSON(value.location),
    };
}
exports.LocationCreateErrorAllOfToJSON = LocationCreateErrorAllOfToJSON;
