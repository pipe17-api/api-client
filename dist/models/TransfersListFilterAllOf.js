"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersListFilterAllOfToJSON = exports.TransfersListFilterAllOfFromJSONTyped = exports.TransfersListFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function TransfersListFilterAllOfFromJSON(json) {
    return TransfersListFilterAllOfFromJSONTyped(json, false);
}
exports.TransfersListFilterAllOfFromJSON = TransfersListFilterAllOfFromJSON;
function TransfersListFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : json['timestamp'],
    };
}
exports.TransfersListFilterAllOfFromJSONTyped = TransfersListFilterAllOfFromJSONTyped;
function TransfersListFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'timestamp': value.timestamp,
    };
}
exports.TransfersListFilterAllOfToJSON = TransfersListFilterAllOfToJSON;
