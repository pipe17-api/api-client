/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationUpdateResponse
 */
export interface IntegrationUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationUpdateResponse
     */
    success: IntegrationUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationUpdateResponse
     */
    code: IntegrationUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationUpdateResponseFromJSON(json: any): IntegrationUpdateResponse;
export declare function IntegrationUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationUpdateResponse;
export declare function IntegrationUpdateResponseToJSON(value?: IntegrationUpdateResponse | null): any;
