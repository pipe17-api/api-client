/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival } from './';
/**
 *
 * @export
 * @interface ArrivalFetchResponse
 */
export interface ArrivalFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ArrivalFetchResponse
     */
    success: ArrivalFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalFetchResponse
     */
    code: ArrivalFetchResponseCodeEnum;
    /**
     *
     * @type {Arrival}
     * @memberof ArrivalFetchResponse
     */
    arrival?: Arrival;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ArrivalFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ArrivalFetchResponseFromJSON(json: any): ArrivalFetchResponse;
export declare function ArrivalFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalFetchResponse;
export declare function ArrivalFetchResponseToJSON(value?: ArrivalFetchResponse | null): any;
