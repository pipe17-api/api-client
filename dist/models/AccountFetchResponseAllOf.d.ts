/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Account } from './';
/**
 *
 * @export
 * @interface AccountFetchResponseAllOf
 */
export interface AccountFetchResponseAllOf {
    /**
     *
     * @type {Account}
     * @memberof AccountFetchResponseAllOf
     */
    account?: Account;
}
export declare function AccountFetchResponseAllOfFromJSON(json: any): AccountFetchResponseAllOf;
export declare function AccountFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountFetchResponseAllOf;
export declare function AccountFetchResponseAllOfToJSON(value?: AccountFetchResponseAllOf | null): any;
