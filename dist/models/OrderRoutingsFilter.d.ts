/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingsFilter
 */
export interface OrderRoutingsFilter {
    /**
     * Fetch routings by list of routingId
     * @type {Array<string>}
     * @memberof OrderRoutingsFilter
     */
    routingId?: Array<string>;
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrderRoutingsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrderRoutingsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrderRoutingsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrderRoutingsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrderRoutingsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrderRoutingsFilter
     */
    count?: number;
}
export declare function OrderRoutingsFilterFromJSON(json: any): OrderRoutingsFilter;
export declare function OrderRoutingsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingsFilter;
export declare function OrderRoutingsFilterToJSON(value?: OrderRoutingsFilter | null): any;
