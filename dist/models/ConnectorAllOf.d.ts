/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorStatus } from './';
/**
 *
 * @export
 * @interface ConnectorAllOf
 */
export interface ConnectorAllOf {
    /**
     * Connector ID
     * @type {string}
     * @memberof ConnectorAllOf
     */
    connectorId?: string;
    /**
     *
     * @type {ConnectorStatus}
     * @memberof ConnectorAllOf
     */
    status?: ConnectorStatus;
    /**
     * Public connector indicator
     * @type {boolean}
     * @memberof ConnectorAllOf
     */
    isPublic?: boolean;
    /**
     * Connector rank in category (higher ranks indicate better match)
     * @type {number}
     * @memberof ConnectorAllOf
     */
    rank?: number;
}
export declare function ConnectorAllOfFromJSON(json: any): ConnectorAllOf;
export declare function ConnectorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorAllOf;
export declare function ConnectorAllOfToJSON(value?: ConnectorAllOf | null): any;
