/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface WebhookUpdateData
 */
export interface WebhookUpdateData {
    /**
     * Webhook URL
     * @type {string}
     * @memberof WebhookUpdateData
     */
    url?: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof WebhookUpdateData
     */
    apikey?: string;
    /**
     * Webhook Topics
     * @type {Array<WebhookTopics>}
     * @memberof WebhookUpdateData
     */
    topics?: Array<WebhookTopics>;
}
export declare function WebhookUpdateDataFromJSON(json: any): WebhookUpdateData;
export declare function WebhookUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookUpdateData;
export declare function WebhookUpdateDataToJSON(value?: WebhookUpdateData | null): any;
