/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductsListFilter } from './';
/**
 *
 * @export
 * @interface ProducstListError
 */
export interface ProducstListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ProducstListError
     */
    success: ProducstListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProducstListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ProducstListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ProducstListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ProductsListFilter}
     * @memberof ProducstListError
     */
    filters?: ProductsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ProducstListErrorSuccessEnum {
    False = "false"
}
export declare function ProducstListErrorFromJSON(json: any): ProducstListError;
export declare function ProducstListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProducstListError;
export declare function ProducstListErrorToJSON(value?: ProducstListError | null): any;
