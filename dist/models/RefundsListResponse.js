"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsListResponseToJSON = exports.RefundsListResponseFromJSONTyped = exports.RefundsListResponseFromJSON = exports.RefundsListResponseCodeEnum = exports.RefundsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RefundsListResponseSuccessEnum;
(function (RefundsListResponseSuccessEnum) {
    RefundsListResponseSuccessEnum["True"] = "true";
})(RefundsListResponseSuccessEnum = exports.RefundsListResponseSuccessEnum || (exports.RefundsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RefundsListResponseCodeEnum;
(function (RefundsListResponseCodeEnum) {
    RefundsListResponseCodeEnum[RefundsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RefundsListResponseCodeEnum[RefundsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RefundsListResponseCodeEnum[RefundsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RefundsListResponseCodeEnum = exports.RefundsListResponseCodeEnum || (exports.RefundsListResponseCodeEnum = {}));
function RefundsListResponseFromJSON(json) {
    return RefundsListResponseFromJSONTyped(json, false);
}
exports.RefundsListResponseFromJSON = RefundsListResponseFromJSON;
function RefundsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RefundsFilterFromJSON(json['filters']),
        'refunds': !runtime_1.exists(json, 'refunds') ? undefined : (json['refunds'].map(_1.RefundFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RefundsListResponseFromJSONTyped = RefundsListResponseFromJSONTyped;
function RefundsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.RefundsFilterToJSON(value.filters),
        'refunds': value.refunds === undefined ? undefined : (value.refunds.map(_1.RefundToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RefundsListResponseToJSON = RefundsListResponseToJSON;
