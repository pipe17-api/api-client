/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShippingCarrier } from './';
/**
 *
 * @export
 * @interface TrackingCreateData
 */
export interface TrackingCreateData {
    /**
     *
     * @type {string}
     * @memberof TrackingCreateData
     */
    trackingNumber: string;
    /**
     *
     * @type {string}
     * @memberof TrackingCreateData
     */
    fulfillmentId: string;
    /**
     *
     * @type {ShippingCarrier}
     * @memberof TrackingCreateData
     */
    shippingCarrier: ShippingCarrier;
    /**
     *
     * @type {string}
     * @memberof TrackingCreateData
     */
    url?: string;
    /**
     *
     * @type {Date}
     * @memberof TrackingCreateData
     */
    expectedArrivalDate?: Date;
}
export declare function TrackingCreateDataFromJSON(json: any): TrackingCreateData;
export declare function TrackingCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingCreateData;
export declare function TrackingCreateDataToJSON(value?: TrackingCreateData | null): any;
