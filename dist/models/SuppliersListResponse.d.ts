/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Supplier, SuppliersListFilter } from './';
/**
 *
 * @export
 * @interface SuppliersListResponse
 */
export interface SuppliersListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof SuppliersListResponse
     */
    success: SuppliersListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SuppliersListResponse
     */
    code: SuppliersListResponseCodeEnum;
    /**
     *
     * @type {SuppliersListFilter}
     * @memberof SuppliersListResponse
     */
    filters?: SuppliersListFilter;
    /**
     *
     * @type {Array<Supplier>}
     * @memberof SuppliersListResponse
     */
    suppliers?: Array<Supplier>;
    /**
     *
     * @type {Pagination}
     * @memberof SuppliersListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum SuppliersListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum SuppliersListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function SuppliersListResponseFromJSON(json: any): SuppliersListResponse;
export declare function SuppliersListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersListResponse;
export declare function SuppliersListResponseToJSON(value?: SuppliersListResponse | null): any;
