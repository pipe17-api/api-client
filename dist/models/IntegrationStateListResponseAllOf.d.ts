/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationState, IntegrationStateListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface IntegrationStateListResponseAllOf
 */
export interface IntegrationStateListResponseAllOf {
    /**
     *
     * @type {IntegrationStateListFilter}
     * @memberof IntegrationStateListResponseAllOf
     */
    filters?: IntegrationStateListFilter;
    /**
     *
     * @type {Array<IntegrationState>}
     * @memberof IntegrationStateListResponseAllOf
     */
    integrationStates?: Array<IntegrationState>;
    /**
     *
     * @type {Pagination}
     * @memberof IntegrationStateListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function IntegrationStateListResponseAllOfFromJSON(json: any): IntegrationStateListResponseAllOf;
export declare function IntegrationStateListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateListResponseAllOf;
export declare function IntegrationStateListResponseAllOfToJSON(value?: IntegrationStateListResponseAllOf | null): any;
