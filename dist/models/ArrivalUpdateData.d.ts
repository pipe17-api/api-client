/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ArrivalLineItem, ArrivalStatus } from './';
/**
 *
 * @export
 * @interface ArrivalUpdateData
 */
export interface ArrivalUpdateData {
    /**
     * Sender's Name
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    senderName?: string;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof ArrivalUpdateData
     */
    expectedArrivalDate?: Date;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    toLocationId?: string;
    /**
     * Sender Location ID (for TO)
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    fromLocationId?: string;
    /**
     *
     * @type {Address}
     * @memberof ArrivalUpdateData
     */
    fromAddress?: Address;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    shippingCarrier?: string;
    /**
     * Shipping Method
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    shippingMethod?: string;
    /**
     * Shipping Note
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    shippingNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    incoterms?: string;
    /**
     * Total Weight
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    totalWeight?: string;
    /**
     * Weight Unit
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    weightUnit?: string;
    /**
     *
     * @type {Array<ArrivalLineItem>}
     * @memberof ArrivalUpdateData
     */
    lineItems?: Array<ArrivalLineItem>;
    /**
     *
     * @type {ArrivalStatus}
     * @memberof ArrivalUpdateData
     */
    status?: ArrivalStatus;
    /**
     * Reference to arrival on external system (vendor)
     * @type {string}
     * @memberof ArrivalUpdateData
     */
    extArrivalId?: string;
}
export declare function ArrivalUpdateDataFromJSON(json: any): ArrivalUpdateData;
export declare function ArrivalUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalUpdateData;
export declare function ArrivalUpdateDataToJSON(value?: ArrivalUpdateData | null): any;
