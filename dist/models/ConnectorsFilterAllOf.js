"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorsFilterAllOfToJSON = exports.ConnectorsFilterAllOfFromJSONTyped = exports.ConnectorsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ConnectorsFilterAllOfFromJSON(json) {
    return ConnectorsFilterAllOfFromJSONTyped(json, false);
}
exports.ConnectorsFilterAllOfFromJSON = ConnectorsFilterAllOfFromJSON;
function ConnectorsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'connectorName': !runtime_1.exists(json, 'connectorName') ? undefined : json['connectorName'],
        'connectorType': !runtime_1.exists(json, 'connectorType') ? undefined : json['connectorType'],
    };
}
exports.ConnectorsFilterAllOfFromJSONTyped = ConnectorsFilterAllOfFromJSONTyped;
function ConnectorsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'connectorId': value.connectorId,
        'connectorName': value.connectorName,
        'connectorType': value.connectorType,
    };
}
exports.ConnectorsFilterAllOfToJSON = ConnectorsFilterAllOfToJSON;
