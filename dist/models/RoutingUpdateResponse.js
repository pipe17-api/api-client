"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingUpdateResponseToJSON = exports.RoutingUpdateResponseFromJSONTyped = exports.RoutingUpdateResponseFromJSON = exports.RoutingUpdateResponseCodeEnum = exports.RoutingUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var RoutingUpdateResponseSuccessEnum;
(function (RoutingUpdateResponseSuccessEnum) {
    RoutingUpdateResponseSuccessEnum["True"] = "true";
})(RoutingUpdateResponseSuccessEnum = exports.RoutingUpdateResponseSuccessEnum || (exports.RoutingUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoutingUpdateResponseCodeEnum;
(function (RoutingUpdateResponseCodeEnum) {
    RoutingUpdateResponseCodeEnum[RoutingUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoutingUpdateResponseCodeEnum[RoutingUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoutingUpdateResponseCodeEnum[RoutingUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoutingUpdateResponseCodeEnum = exports.RoutingUpdateResponseCodeEnum || (exports.RoutingUpdateResponseCodeEnum = {}));
function RoutingUpdateResponseFromJSON(json) {
    return RoutingUpdateResponseFromJSONTyped(json, false);
}
exports.RoutingUpdateResponseFromJSON = RoutingUpdateResponseFromJSON;
function RoutingUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.RoutingUpdateResponseFromJSONTyped = RoutingUpdateResponseFromJSONTyped;
function RoutingUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.RoutingUpdateResponseToJSON = RoutingUpdateResponseToJSON;
