"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptsListErrorAllOfToJSON = exports.ReceiptsListErrorAllOfFromJSONTyped = exports.ReceiptsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReceiptsListErrorAllOfFromJSON(json) {
    return ReceiptsListErrorAllOfFromJSONTyped(json, false);
}
exports.ReceiptsListErrorAllOfFromJSON = ReceiptsListErrorAllOfFromJSON;
function ReceiptsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReceiptsListFilterFromJSON(json['filters']),
    };
}
exports.ReceiptsListErrorAllOfFromJSONTyped = ReceiptsListErrorAllOfFromJSONTyped;
function ReceiptsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ReceiptsListFilterToJSON(value.filters),
    };
}
exports.ReceiptsListErrorAllOfToJSON = ReceiptsListErrorAllOfToJSON;
