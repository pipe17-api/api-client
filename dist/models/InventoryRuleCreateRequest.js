"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleCreateRequestToJSON = exports.InventoryRuleCreateRequestFromJSONTyped = exports.InventoryRuleCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryRuleCreateRequestFromJSON(json) {
    return InventoryRuleCreateRequestFromJSONTyped(json, false);
}
exports.InventoryRuleCreateRequestFromJSON = InventoryRuleCreateRequestFromJSON;
function InventoryRuleCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'event': _1.EventTypeFromJSON(json['event']),
        'ptype': !runtime_1.exists(json, 'ptype') ? undefined : _1.EventSourceFromJSON(json['ptype']),
        'partner': !runtime_1.exists(json, 'partner') ? undefined : json['partner'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'rule': json['rule'],
    };
}
exports.InventoryRuleCreateRequestFromJSONTyped = InventoryRuleCreateRequestFromJSONTyped;
function InventoryRuleCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'event': _1.EventTypeToJSON(value.event),
        'ptype': _1.EventSourceToJSON(value.ptype),
        'partner': value.partner,
        'integration': value.integration,
        'description': value.description,
        'rule': value.rule,
    };
}
exports.InventoryRuleCreateRequestToJSON = InventoryRuleCreateRequestToJSON;
