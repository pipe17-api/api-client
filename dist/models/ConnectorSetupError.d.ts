/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorSetupData } from './';
/**
 *
 * @export
 * @interface ConnectorSetupError
 */
export interface ConnectorSetupError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ConnectorSetupError
     */
    success: ConnectorSetupErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorSetupError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ConnectorSetupError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ConnectorSetupError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ConnectorSetupData}
     * @memberof ConnectorSetupError
     */
    options?: ConnectorSetupData;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorSetupErrorSuccessEnum {
    False = "false"
}
export declare function ConnectorSetupErrorFromJSON(json: any): ConnectorSetupError;
export declare function ConnectorSetupErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSetupError;
export declare function ConnectorSetupErrorToJSON(value?: ConnectorSetupError | null): any;
