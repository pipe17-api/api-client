/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityFilterUpdateError
 */
export interface EntityFilterUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntityFilterUpdateError
     */
    success: EntityFilterUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntityFilterUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntityFilterUpdateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterUpdateErrorSuccessEnum {
    False = "false"
}
export declare function EntityFilterUpdateErrorFromJSON(json: any): EntityFilterUpdateError;
export declare function EntityFilterUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterUpdateError;
export declare function EntityFilterUpdateErrorToJSON(value?: EntityFilterUpdateError | null): any;
