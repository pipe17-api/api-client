"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingListResponseAllOfToJSON = exports.OrderRoutingListResponseAllOfFromJSONTyped = exports.OrderRoutingListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrderRoutingListResponseAllOfFromJSON(json) {
    return OrderRoutingListResponseAllOfFromJSONTyped(json, false);
}
exports.OrderRoutingListResponseAllOfFromJSON = OrderRoutingListResponseAllOfFromJSON;
function OrderRoutingListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrderRoutingsListFilterFromJSON(json['filters']),
        'routings': !runtime_1.exists(json, 'routings') ? undefined : (json['routings'].map(_1.OrderRoutingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrderRoutingListResponseAllOfFromJSONTyped = OrderRoutingListResponseAllOfFromJSONTyped;
function OrderRoutingListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.OrderRoutingsListFilterToJSON(value.filters),
        'routings': value.routings === undefined ? undefined : (value.routings.map(_1.OrderRoutingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrderRoutingListResponseAllOfToJSON = OrderRoutingListResponseAllOfToJSON;
