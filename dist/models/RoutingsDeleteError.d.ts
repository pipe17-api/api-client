/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RoutingsDeleteError
 */
export interface RoutingsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoutingsDeleteError
     */
    success: RoutingsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoutingsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoutingsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RoutingsDeleteFilter}
     * @memberof RoutingsDeleteError
     */
    filters?: RoutingsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function RoutingsDeleteErrorFromJSON(json: any): RoutingsDeleteError;
export declare function RoutingsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsDeleteError;
export declare function RoutingsDeleteErrorToJSON(value?: RoutingsDeleteError | null): any;
