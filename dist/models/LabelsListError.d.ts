/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LabelsListFilter } from './';
/**
 *
 * @export
 * @interface LabelsListError
 */
export interface LabelsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LabelsListError
     */
    success: LabelsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LabelsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LabelsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {LabelsListFilter}
     * @memberof LabelsListError
     */
    filters?: LabelsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelsListErrorSuccessEnum {
    False = "false"
}
export declare function LabelsListErrorFromJSON(json: any): LabelsListError;
export declare function LabelsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsListError;
export declare function LabelsListErrorToJSON(value?: LabelsListError | null): any;
