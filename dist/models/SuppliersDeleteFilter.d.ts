/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SuppliersDeleteFilter
 */
export interface SuppliersDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof SuppliersDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof SuppliersDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof SuppliersDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof SuppliersDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof SuppliersDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof SuppliersDeleteFilter
     */
    count?: number;
    /**
     * Suppliers by list of supplierId
     * @type {Array<string>}
     * @memberof SuppliersDeleteFilter
     */
    supplierId?: Array<string>;
    /**
     * Soft deleted suppliers
     * @type {boolean}
     * @memberof SuppliersDeleteFilter
     */
    deleted?: boolean;
}
export declare function SuppliersDeleteFilterFromJSON(json: any): SuppliersDeleteFilter;
export declare function SuppliersDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersDeleteFilter;
export declare function SuppliersDeleteFilterToJSON(value?: SuppliersDeleteFilter | null): any;
