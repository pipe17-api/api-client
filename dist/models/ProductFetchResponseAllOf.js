"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductFetchResponseAllOfToJSON = exports.ProductFetchResponseAllOfFromJSONTyped = exports.ProductFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductFetchResponseAllOfFromJSON(json) {
    return ProductFetchResponseAllOfFromJSONTyped(json, false);
}
exports.ProductFetchResponseAllOfFromJSON = ProductFetchResponseAllOfFromJSON;
function ProductFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'product': !runtime_1.exists(json, 'product') ? undefined : _1.ProductFromJSON(json['product']),
    };
}
exports.ProductFetchResponseAllOfFromJSONTyped = ProductFetchResponseAllOfFromJSONTyped;
function ProductFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'product': _1.ProductToJSON(value.product),
    };
}
exports.ProductFetchResponseAllOfToJSON = ProductFetchResponseAllOfToJSON;
