/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentStatus } from './';
/**
 *
 * @export
 * @interface ShipmentsFilter
 */
export interface ShipmentsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ShipmentsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ShipmentsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ShipmentsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ShipmentsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ShipmentsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ShipmentsFilter
     */
    count?: number;
    /**
     * Shipments by list of shipmentId
     * @type {Array<string>}
     * @memberof ShipmentsFilter
     */
    shipmentId?: Array<string>;
    /**
     * Shipments by list of orderId
     * @type {Array<string>}
     * @memberof ShipmentsFilter
     */
    orderId?: Array<string>;
    /**
     * Shipments by list of order types
     * @type {Array<string>}
     * @memberof ShipmentsFilter
     */
    orderType?: Array<string>;
    /**
     * Shipments by list of extOrderId
     * @type {Array<string>}
     * @memberof ShipmentsFilter
     */
    extOrderId?: Array<string>;
    /**
     * Shipments by list of statuses
     * @type {Array<ShipmentStatus>}
     * @memberof ShipmentsFilter
     */
    status?: Array<ShipmentStatus>;
    /**
     * Soft deleted shipments
     * @type {boolean}
     * @memberof ShipmentsFilter
     */
    deleted?: boolean;
}
export declare function ShipmentsFilterFromJSON(json: any): ShipmentsFilter;
export declare function ShipmentsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsFilter;
export declare function ShipmentsFilterToJSON(value?: ShipmentsFilter | null): any;
