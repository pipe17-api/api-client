"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionCreateRequestToJSON = exports.ExceptionCreateRequestFromJSONTyped = exports.ExceptionCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionCreateRequestFromJSON(json) {
    return ExceptionCreateRequestFromJSONTyped(json, false);
}
exports.ExceptionCreateRequestFromJSON = ExceptionCreateRequestFromJSON;
function ExceptionCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionType': _1.ExceptionTypeFromJSON(json['exceptionType']),
        'exceptionDetails': !runtime_1.exists(json, 'exceptionDetails') ? undefined : json['exceptionDetails'],
        'entityId': json['entityId'],
        'entityType': _1.EntityNameFromJSON(json['entityType']),
    };
}
exports.ExceptionCreateRequestFromJSONTyped = ExceptionCreateRequestFromJSONTyped;
function ExceptionCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionType': _1.ExceptionTypeToJSON(value.exceptionType),
        'exceptionDetails': value.exceptionDetails,
        'entityId': value.entityId,
        'entityType': _1.EntityNameToJSON(value.entityType),
    };
}
exports.ExceptionCreateRequestToJSON = ExceptionCreateRequestToJSON;
