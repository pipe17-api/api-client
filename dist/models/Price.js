"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PriceToJSON = exports.PriceFromJSONTyped = exports.PriceFromJSON = void 0;
const runtime_1 = require("../runtime");
function PriceFromJSON(json) {
    return PriceFromJSONTyped(json, false);
}
exports.PriceFromJSON = PriceFromJSON;
function PriceFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'value': !runtime_1.exists(json, 'value') ? undefined : json['value'],
        'currency': !runtime_1.exists(json, 'currency') ? undefined : json['currency'],
    };
}
exports.PriceFromJSONTyped = PriceFromJSONTyped;
function PriceToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'value': value.value,
        'currency': value.currency,
    };
}
exports.PriceToJSON = PriceToJSON;
