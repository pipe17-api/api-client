/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Return status
 * @export
 * @enum {string}
 */
export declare enum ReturnStatus {
    Authorized = "authorized",
    InTransit = "inTransit",
    Delivered = "delivered",
    DeliveredLate = "deliveredLate",
    Pending = "pending",
    Refunded = "refunded",
    RefundedExternally = "refundedExternally",
    Canceled = "canceled",
    Deleted = "deleted"
}
export declare function ReturnStatusFromJSON(json: any): ReturnStatus;
export declare function ReturnStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnStatus;
export declare function ReturnStatusToJSON(value?: ReturnStatus | null): any;
