"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundAllOfToJSON = exports.RefundAllOfFromJSONTyped = exports.RefundAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function RefundAllOfFromJSON(json) {
    return RefundAllOfFromJSONTyped(json, false);
}
exports.RefundAllOfFromJSON = RefundAllOfFromJSON;
function RefundAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'refundId': !runtime_1.exists(json, 'refundId') ? undefined : json['refundId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.RefundAllOfFromJSONTyped = RefundAllOfFromJSONTyped;
function RefundAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'refundId': value.refundId,
        'integration': value.integration,
    };
}
exports.RefundAllOfToJSON = RefundAllOfToJSON;
