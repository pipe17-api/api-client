/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressNullable, NameValue, TransferStatus, TransferUpdateLineItem } from './';
/**
 *
 * @export
 * @interface TransferUpdateRequest
 */
export interface TransferUpdateRequest {
    /**
     *
     * @type {TransferStatus}
     * @memberof TransferUpdateRequest
     */
    status?: TransferStatus;
    /**
     *
     * @type {Array<TransferUpdateLineItem>}
     * @memberof TransferUpdateRequest
     */
    lineItems?: Array<TransferUpdateLineItem>;
    /**
     * Shipping Location ID
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    fromLocationId?: string | null;
    /**
     *
     * @type {AddressNullable}
     * @memberof TransferUpdateRequest
     */
    toAddress?: AddressNullable | null;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    toLocationId?: string | null;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    shippingCarrier?: string | null;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    shippingService?: string | null;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof TransferUpdateRequest
     */
    shipByDate?: Date | null;
    /**
     * Expected Ship Date
     * @type {Date}
     * @memberof TransferUpdateRequest
     */
    expectedShipDate?: Date | null;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof TransferUpdateRequest
     */
    expectedArrivalDate?: Date | null;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof TransferUpdateRequest
     */
    actualArrivalDate?: Date | null;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    shippingNotes?: string | null;
    /**
     * Transfer order created by
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    employeeName?: string | null;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    incoterms?: string | null;
    /**
     * Transfer Notes
     * @type {string}
     * @memberof TransferUpdateRequest
     */
    transferNotes?: string | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof TransferUpdateRequest
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof TransferUpdateRequest
     */
    timestamp?: Date | null;
}
export declare function TransferUpdateRequestFromJSON(json: any): TransferUpdateRequest;
export declare function TransferUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferUpdateRequest;
export declare function TransferUpdateRequestToJSON(value?: TransferUpdateRequest | null): any;
