"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookUpdateRequestToJSON = exports.WebhookUpdateRequestFromJSONTyped = exports.WebhookUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function WebhookUpdateRequestFromJSON(json) {
    return WebhookUpdateRequestFromJSONTyped(json, false);
}
exports.WebhookUpdateRequestFromJSON = WebhookUpdateRequestFromJSON;
function WebhookUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'url': !runtime_1.exists(json, 'url') ? undefined : json['url'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : json['apikey'],
        'topics': !runtime_1.exists(json, 'topics') ? undefined : (json['topics'].map(_1.WebhookTopicsFromJSON)),
    };
}
exports.WebhookUpdateRequestFromJSONTyped = WebhookUpdateRequestFromJSONTyped;
function WebhookUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'url': value.url,
        'apikey': value.apikey,
        'topics': value.topics === undefined ? undefined : (value.topics.map(_1.WebhookTopicsToJSON)),
    };
}
exports.WebhookUpdateRequestToJSON = WebhookUpdateRequestToJSON;
