/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShippingCarrier } from './';
/**
 *
 * @export
 * @interface TrackingCreateRequest
 */
export interface TrackingCreateRequest {
    /**
     *
     * @type {string}
     * @memberof TrackingCreateRequest
     */
    trackingNumber: string;
    /**
     *
     * @type {string}
     * @memberof TrackingCreateRequest
     */
    fulfillmentId: string;
    /**
     *
     * @type {ShippingCarrier}
     * @memberof TrackingCreateRequest
     */
    shippingCarrier: ShippingCarrier;
    /**
     *
     * @type {string}
     * @memberof TrackingCreateRequest
     */
    url?: string;
    /**
     *
     * @type {Date}
     * @memberof TrackingCreateRequest
     */
    expectedArrivalDate?: Date;
}
export declare function TrackingCreateRequestFromJSON(json: any): TrackingCreateRequest;
export declare function TrackingCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingCreateRequest;
export declare function TrackingCreateRequestToJSON(value?: TrackingCreateRequest | null): any;
