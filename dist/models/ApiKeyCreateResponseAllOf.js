"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyCreateResponseAllOfToJSON = exports.ApiKeyCreateResponseAllOfFromJSONTyped = exports.ApiKeyCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ApiKeyCreateResponseAllOfFromJSON(json) {
    return ApiKeyCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ApiKeyCreateResponseAllOfFromJSON = ApiKeyCreateResponseAllOfFromJSON;
function ApiKeyCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikey': json['apikey'],
        'apikeyId': !runtime_1.exists(json, 'apikeyId') ? undefined : json['apikeyId'],
    };
}
exports.ApiKeyCreateResponseAllOfFromJSONTyped = ApiKeyCreateResponseAllOfFromJSONTyped;
function ApiKeyCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikey': value.apikey,
        'apikeyId': value.apikeyId,
    };
}
exports.ApiKeyCreateResponseAllOfToJSON = ApiKeyCreateResponseAllOfToJSON;
