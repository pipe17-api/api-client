/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { NameValue, Price, ProductBundleItem, ProductPublishedItem, ProductStatus, ProductType } from './';
/**
 *
 * @export
 * @interface ProductUpdateRequest
 */
export interface ProductUpdateRequest {
    /**
     * Product SKU
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    sku?: string;
    /**
     * Display Name
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    name?: string;
    /**
     *
     * @type {ProductStatus}
     * @memberof ProductUpdateRequest
     */
    status?: ProductStatus;
    /**
     * Description
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    description?: string;
    /**
     * Product types
     * @type {Array<ProductType>}
     * @memberof ProductUpdateRequest
     */
    types?: Array<ProductType>;
    /**
     * External Product API Id
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    extProductApiId?: string;
    /**
     * UPC
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    upc?: string | null;
    /**
     * Manufacturer part number
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    partId?: string | null;
    /**
     * Start off with basePrice
     * @type {Array<Price>}
     * @memberof ProductUpdateRequest
     */
    prices?: Array<Price>;
    /**
     * Image URLs
     * @type {Array<string>}
     * @memberof ProductUpdateRequest
     */
    imageURLs?: Array<string>;
    /**
     * Cost
     * @type {number}
     * @memberof ProductUpdateRequest
     */
    cost?: number | null;
    /**
     * Currency
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    costCurrency?: string | null;
    /**
     * Taxable
     * @type {boolean}
     * @memberof ProductUpdateRequest
     */
    taxable?: boolean | null;
    /**
     * Country of Origin
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    countryOfOrigin?: string | null;
    /**
     * Harmonized System/Tariff Code
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    harmonizedCode?: string | null;
    /**
     * Weight
     * @type {number}
     * @memberof ProductUpdateRequest
     */
    weight?: number | null;
    /**
     * Weight UOM
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    weightUnit?: ProductUpdateRequestWeightUnitEnum;
    /**
     * Height
     * @type {number}
     * @memberof ProductUpdateRequest
     */
    height?: number | null;
    /**
     * Length
     * @type {number}
     * @memberof ProductUpdateRequest
     */
    length?: number | null;
    /**
     * Width
     * @type {number}
     * @memberof ProductUpdateRequest
     */
    width?: number | null;
    /**
     * Dimensions UOM
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    dimensionsUnit?: ProductUpdateRequestDimensionsUnitEnum;
    /**
     * vendor
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    vendor?: string | null;
    /**
     * Tags
     * @type {Array<string>}
     * @memberof ProductUpdateRequest
     */
    tags?: Array<string>;
    /**
     * Variant parent external product Id
     * @type {string}
     * @memberof ProductUpdateRequest
     */
    parentExtProductId?: string;
    /**
     * this let us know the variant description
     * @type {Array<NameValue>}
     * @memberof ProductUpdateRequest
     */
    variantOptions?: Array<NameValue>;
    /**
     *
     * @type {Date}
     * @memberof ProductUpdateRequest
     */
    extProductCreatedAt?: Date | null;
    /**
     *
     * @type {Date}
     * @memberof ProductUpdateRequest
     */
    extProductUpdatedAt?: Date | null;
    /**
     * Bundle Item need to reference the SKU of the actual product
     * @type {Array<ProductBundleItem>}
     * @memberof ProductUpdateRequest
     */
    bundleItems?: Array<ProductBundleItem> | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof ProductUpdateRequest
     */
    customFields?: Array<NameValue>;
    /**
     * Published Channels
     * @type {Array<ProductPublishedItem>}
     * @memberof ProductUpdateRequest
     */
    published?: Array<ProductPublishedItem>;
    /**
     * Do not track inventory flag
     * @type {boolean}
     * @memberof ProductUpdateRequest
     */
    inventoryNotTracked?: boolean | null;
    /**
     * Mark item as hidden in marketplace Storefront
     * @type {boolean}
     * @memberof ProductUpdateRequest
     */
    hideInStore?: boolean | null;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductUpdateRequestWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
} /**
* @export
* @enum {string}
*/
export declare enum ProductUpdateRequestDimensionsUnitEnum {
    Cm = "cm",
    In = "in",
    Ft = "ft"
}
export declare function ProductUpdateRequestFromJSON(json: any): ProductUpdateRequest;
export declare function ProductUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductUpdateRequest;
export declare function ProductUpdateRequestToJSON(value?: ProductUpdateRequest | null): any;
