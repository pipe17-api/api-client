/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 * Webhooks Filter
 * @export
 * @interface WebhooksFilter
 */
export interface WebhooksFilter {
    /**
     * Webhooks by list of webhookId
     * @type {Array<string>}
     * @memberof WebhooksFilter
     */
    webhookId?: Array<string>;
    /**
     *
     * @type {WebhookTopics}
     * @memberof WebhooksFilter
     */
    topic?: WebhookTopics;
    /**
     * Include related webhooks
     * @type {boolean}
     * @memberof WebhooksFilter
     */
    related?: boolean;
}
export declare function WebhooksFilterFromJSON(json: any): WebhooksFilter;
export declare function WebhooksFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhooksFilter;
export declare function WebhooksFilterToJSON(value?: WebhooksFilter | null): any;
