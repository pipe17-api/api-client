/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoleUpdateResponse
 */
export interface RoleUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoleUpdateResponse
     */
    success: RoleUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoleUpdateResponse
     */
    code: RoleUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum RoleUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoleUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoleUpdateResponseFromJSON(json: any): RoleUpdateResponse;
export declare function RoleUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleUpdateResponse;
export declare function RoleUpdateResponseToJSON(value?: RoleUpdateResponse | null): any;
