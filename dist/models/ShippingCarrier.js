"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingCarrierToJSON = exports.ShippingCarrierFromJSONTyped = exports.ShippingCarrierFromJSON = exports.ShippingCarrier = void 0;
/**
 *
 * @export
 * @enum {string}
 */
var ShippingCarrier;
(function (ShippingCarrier) {
    ShippingCarrier["Ups"] = "ups";
    ShippingCarrier["Usps"] = "usps";
    ShippingCarrier["Fedex"] = "fedex";
    ShippingCarrier["Deliverr"] = "deliverr";
    ShippingCarrier["Amazon"] = "amazon";
    ShippingCarrier["Other"] = "other";
})(ShippingCarrier = exports.ShippingCarrier || (exports.ShippingCarrier = {}));
function ShippingCarrierFromJSON(json) {
    return ShippingCarrierFromJSONTyped(json, false);
}
exports.ShippingCarrierFromJSON = ShippingCarrierFromJSON;
function ShippingCarrierFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ShippingCarrierFromJSONTyped = ShippingCarrierFromJSONTyped;
function ShippingCarrierToJSON(value) {
    return value;
}
exports.ShippingCarrierToJSON = ShippingCarrierToJSON;
