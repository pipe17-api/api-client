/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntitySchemaUpdateError
 */
export interface EntitySchemaUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntitySchemaUpdateError
     */
    success: EntitySchemaUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntitySchemaUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntitySchemaUpdateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaUpdateErrorSuccessEnum {
    False = "false"
}
export declare function EntitySchemaUpdateErrorFromJSON(json: any): EntitySchemaUpdateError;
export declare function EntitySchemaUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaUpdateError;
export declare function EntitySchemaUpdateErrorToJSON(value?: EntitySchemaUpdateError | null): any;
