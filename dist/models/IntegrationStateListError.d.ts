/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationStateListFilter } from './';
/**
 *
 * @export
 * @interface IntegrationStateListError
 */
export interface IntegrationStateListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationStateListError
     */
    success: IntegrationStateListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationStateListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationStateListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationStateListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {IntegrationStateListFilter}
     * @memberof IntegrationStateListError
     */
    filters?: IntegrationStateListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationStateListErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationStateListErrorFromJSON(json: any): IntegrationStateListError;
export declare function IntegrationStateListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateListError;
export declare function IntegrationStateListErrorToJSON(value?: IntegrationStateListError | null): any;
