/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationCreateData } from './';
/**
 *
 * @export
 * @interface OrganizationCreateError
 */
export interface OrganizationCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrganizationCreateError
     */
    success: OrganizationCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrganizationCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrganizationCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrganizationCreateData}
     * @memberof OrganizationCreateError
     */
    organization?: OrganizationCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationCreateErrorSuccessEnum {
    False = "false"
}
export declare function OrganizationCreateErrorFromJSON(json: any): OrganizationCreateError;
export declare function OrganizationCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationCreateError;
export declare function OrganizationCreateErrorToJSON(value?: OrganizationCreateError | null): any;
