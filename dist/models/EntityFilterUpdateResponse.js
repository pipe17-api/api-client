"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterUpdateResponseToJSON = exports.EntityFilterUpdateResponseFromJSONTyped = exports.EntityFilterUpdateResponseFromJSON = exports.EntityFilterUpdateResponseCodeEnum = exports.EntityFilterUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var EntityFilterUpdateResponseSuccessEnum;
(function (EntityFilterUpdateResponseSuccessEnum) {
    EntityFilterUpdateResponseSuccessEnum["True"] = "true";
})(EntityFilterUpdateResponseSuccessEnum = exports.EntityFilterUpdateResponseSuccessEnum || (exports.EntityFilterUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntityFilterUpdateResponseCodeEnum;
(function (EntityFilterUpdateResponseCodeEnum) {
    EntityFilterUpdateResponseCodeEnum[EntityFilterUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntityFilterUpdateResponseCodeEnum[EntityFilterUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntityFilterUpdateResponseCodeEnum[EntityFilterUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntityFilterUpdateResponseCodeEnum = exports.EntityFilterUpdateResponseCodeEnum || (exports.EntityFilterUpdateResponseCodeEnum = {}));
function EntityFilterUpdateResponseFromJSON(json) {
    return EntityFilterUpdateResponseFromJSONTyped(json, false);
}
exports.EntityFilterUpdateResponseFromJSON = EntityFilterUpdateResponseFromJSON;
function EntityFilterUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.EntityFilterUpdateResponseFromJSONTyped = EntityFilterUpdateResponseFromJSONTyped;
function EntityFilterUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.EntityFilterUpdateResponseToJSON = EntityFilterUpdateResponseToJSON;
