"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaAllOfToJSON = exports.EntitySchemaAllOfFromJSONTyped = exports.EntitySchemaAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function EntitySchemaAllOfFromJSON(json) {
    return EntitySchemaAllOfFromJSONTyped(json, false);
}
exports.EntitySchemaAllOfFromJSON = EntitySchemaAllOfFromJSON;
function EntitySchemaAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'schemaId': !runtime_1.exists(json, 'schemaId') ? undefined : json['schemaId'],
    };
}
exports.EntitySchemaAllOfFromJSONTyped = EntitySchemaAllOfFromJSONTyped;
function EntitySchemaAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'schemaId': value.schemaId,
    };
}
exports.EntitySchemaAllOfToJSON = EntitySchemaAllOfToJSON;
