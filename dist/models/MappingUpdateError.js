"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingUpdateErrorToJSON = exports.MappingUpdateErrorFromJSONTyped = exports.MappingUpdateErrorFromJSON = exports.MappingUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var MappingUpdateErrorSuccessEnum;
(function (MappingUpdateErrorSuccessEnum) {
    MappingUpdateErrorSuccessEnum["False"] = "false";
})(MappingUpdateErrorSuccessEnum = exports.MappingUpdateErrorSuccessEnum || (exports.MappingUpdateErrorSuccessEnum = {}));
function MappingUpdateErrorFromJSON(json) {
    return MappingUpdateErrorFromJSONTyped(json, false);
}
exports.MappingUpdateErrorFromJSON = MappingUpdateErrorFromJSON;
function MappingUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingUpdateDataFromJSON(json['mapping']),
    };
}
exports.MappingUpdateErrorFromJSONTyped = MappingUpdateErrorFromJSONTyped;
function MappingUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'mapping': _1.MappingUpdateDataToJSON(value.mapping),
    };
}
exports.MappingUpdateErrorToJSON = MappingUpdateErrorToJSON;
