"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorUpdateResponseToJSON = exports.ConnectorUpdateResponseFromJSONTyped = exports.ConnectorUpdateResponseFromJSON = exports.ConnectorUpdateResponseCodeEnum = exports.ConnectorUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ConnectorUpdateResponseSuccessEnum;
(function (ConnectorUpdateResponseSuccessEnum) {
    ConnectorUpdateResponseSuccessEnum["True"] = "true";
})(ConnectorUpdateResponseSuccessEnum = exports.ConnectorUpdateResponseSuccessEnum || (exports.ConnectorUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ConnectorUpdateResponseCodeEnum;
(function (ConnectorUpdateResponseCodeEnum) {
    ConnectorUpdateResponseCodeEnum[ConnectorUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ConnectorUpdateResponseCodeEnum[ConnectorUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ConnectorUpdateResponseCodeEnum[ConnectorUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ConnectorUpdateResponseCodeEnum = exports.ConnectorUpdateResponseCodeEnum || (exports.ConnectorUpdateResponseCodeEnum = {}));
function ConnectorUpdateResponseFromJSON(json) {
    return ConnectorUpdateResponseFromJSONTyped(json, false);
}
exports.ConnectorUpdateResponseFromJSON = ConnectorUpdateResponseFromJSON;
function ConnectorUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ConnectorUpdateResponseFromJSONTyped = ConnectorUpdateResponseFromJSONTyped;
function ConnectorUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ConnectorUpdateResponseToJSON = ConnectorUpdateResponseToJSON;
