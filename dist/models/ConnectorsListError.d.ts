/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConnectorsListFilter } from './';
/**
 *
 * @export
 * @interface ConnectorsListError
 */
export interface ConnectorsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ConnectorsListError
     */
    success: ConnectorsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ConnectorsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ConnectorsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ConnectorsListFilter}
     * @memberof ConnectorsListError
     */
    filters?: ConnectorsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorsListErrorSuccessEnum {
    False = "false"
}
export declare function ConnectorsListErrorFromJSON(json: any): ConnectorsListError;
export declare function ConnectorsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsListError;
export declare function ConnectorsListErrorToJSON(value?: ConnectorsListError | null): any;
