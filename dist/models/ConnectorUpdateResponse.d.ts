/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorUpdateResponse
 */
export interface ConnectorUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ConnectorUpdateResponse
     */
    success: ConnectorUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorUpdateResponse
     */
    code: ConnectorUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ConnectorUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ConnectorUpdateResponseFromJSON(json: any): ConnectorUpdateResponse;
export declare function ConnectorUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorUpdateResponse;
export declare function ConnectorUpdateResponseToJSON(value?: ConnectorUpdateResponse | null): any;
