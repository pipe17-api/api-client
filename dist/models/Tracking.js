"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingToJSON = exports.TrackingFromJSONTyped = exports.TrackingFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingFromJSON(json) {
    return TrackingFromJSONTyped(json, false);
}
exports.TrackingFromJSON = TrackingFromJSON;
function TrackingFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'trackingId': !runtime_1.exists(json, 'trackingId') ? undefined : json['trackingId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.TrackingStatusFromJSON(json['status']),
        'actualShipDate': !runtime_1.exists(json, 'actualShipDate') ? undefined : (new Date(json['actualShipDate'])),
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (new Date(json['actualArrivalDate'])),
        'trackingNumber': json['trackingNumber'],
        'fulfillmentId': json['fulfillmentId'],
        'shippingCarrier': _1.ShippingCarrierFromJSON(json['shippingCarrier']),
        'url': !runtime_1.exists(json, 'url') ? undefined : json['url'],
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.TrackingFromJSONTyped = TrackingFromJSONTyped;
function TrackingToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'trackingId': value.trackingId,
        'status': _1.TrackingStatusToJSON(value.status),
        'actualShipDate': value.actualShipDate === undefined ? undefined : (new Date(value.actualShipDate).toISOString()),
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (new Date(value.actualArrivalDate).toISOString()),
        'trackingNumber': value.trackingNumber,
        'fulfillmentId': value.fulfillmentId,
        'shippingCarrier': _1.ShippingCarrierToJSON(value.shippingCarrier),
        'url': value.url,
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
    };
}
exports.TrackingToJSON = TrackingToJSON;
