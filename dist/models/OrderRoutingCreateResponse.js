"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingCreateResponseToJSON = exports.OrderRoutingCreateResponseFromJSONTyped = exports.OrderRoutingCreateResponseFromJSON = exports.OrderRoutingCreateResponseCodeEnum = exports.OrderRoutingCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderRoutingCreateResponseSuccessEnum;
(function (OrderRoutingCreateResponseSuccessEnum) {
    OrderRoutingCreateResponseSuccessEnum["True"] = "true";
})(OrderRoutingCreateResponseSuccessEnum = exports.OrderRoutingCreateResponseSuccessEnum || (exports.OrderRoutingCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrderRoutingCreateResponseCodeEnum;
(function (OrderRoutingCreateResponseCodeEnum) {
    OrderRoutingCreateResponseCodeEnum[OrderRoutingCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrderRoutingCreateResponseCodeEnum[OrderRoutingCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrderRoutingCreateResponseCodeEnum[OrderRoutingCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrderRoutingCreateResponseCodeEnum = exports.OrderRoutingCreateResponseCodeEnum || (exports.OrderRoutingCreateResponseCodeEnum = {}));
function OrderRoutingCreateResponseFromJSON(json) {
    return OrderRoutingCreateResponseFromJSONTyped(json, false);
}
exports.OrderRoutingCreateResponseFromJSON = OrderRoutingCreateResponseFromJSON;
function OrderRoutingCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.OrderRoutingFromJSON(json['routing']),
    };
}
exports.OrderRoutingCreateResponseFromJSONTyped = OrderRoutingCreateResponseFromJSONTyped;
function OrderRoutingCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'routing': _1.OrderRoutingToJSON(value.routing),
    };
}
exports.OrderRoutingCreateResponseToJSON = OrderRoutingCreateResponseToJSON;
