"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentToJSON = exports.FulfillmentFromJSONTyped = exports.FulfillmentFromJSON = exports.FulfillmentWeightUnitEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentWeightUnitEnum;
(function (FulfillmentWeightUnitEnum) {
    FulfillmentWeightUnitEnum["Lb"] = "lb";
    FulfillmentWeightUnitEnum["Oz"] = "oz";
    FulfillmentWeightUnitEnum["Kg"] = "kg";
    FulfillmentWeightUnitEnum["G"] = "g";
})(FulfillmentWeightUnitEnum = exports.FulfillmentWeightUnitEnum || (exports.FulfillmentWeightUnitEnum = {}));
function FulfillmentFromJSON(json) {
    return FulfillmentFromJSONTyped(json, false);
}
exports.FulfillmentFromJSON = FulfillmentFromJSON;
function FulfillmentFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.FulfillmentItemExtFromJSON)),
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'trackingNumber': !runtime_1.exists(json, 'trackingNumber') ? undefined : json['trackingNumber'],
        'actualShipDate': !runtime_1.exists(json, 'actualShipDate') ? undefined : (new Date(json['actualShipDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'shippingCharge': !runtime_1.exists(json, 'shippingCharge') ? undefined : json['shippingCharge'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'weight': !runtime_1.exists(json, 'weight') ? undefined : json['weight'],
        'extFulfillmentId': !runtime_1.exists(json, 'extFulfillmentId') ? undefined : json['extFulfillmentId'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.FulfillmentFromJSONTyped = FulfillmentFromJSONTyped;
function FulfillmentToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fulfillmentId': value.fulfillmentId,
        'integration': value.integration,
        'locationId': value.locationId,
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.FulfillmentItemExtToJSON)),
        'shipmentId': value.shipmentId,
        'trackingNumber': value.trackingNumber,
        'actualShipDate': value.actualShipDate === undefined ? undefined : (new Date(value.actualShipDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'shippingCharge': value.shippingCharge,
        'weightUnit': value.weightUnit,
        'extOrderId': value.extOrderId,
        'weight': value.weight,
        'extFulfillmentId': value.extFulfillmentId,
    };
}
exports.FulfillmentToJSON = FulfillmentToJSON;
