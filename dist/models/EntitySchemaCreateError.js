"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaCreateErrorToJSON = exports.EntitySchemaCreateErrorFromJSONTyped = exports.EntitySchemaCreateErrorFromJSON = exports.EntitySchemaCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntitySchemaCreateErrorSuccessEnum;
(function (EntitySchemaCreateErrorSuccessEnum) {
    EntitySchemaCreateErrorSuccessEnum["False"] = "false";
})(EntitySchemaCreateErrorSuccessEnum = exports.EntitySchemaCreateErrorSuccessEnum || (exports.EntitySchemaCreateErrorSuccessEnum = {}));
function EntitySchemaCreateErrorFromJSON(json) {
    return EntitySchemaCreateErrorFromJSONTyped(json, false);
}
exports.EntitySchemaCreateErrorFromJSON = EntitySchemaCreateErrorFromJSON;
function EntitySchemaCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'entitySchema': !runtime_1.exists(json, 'entitySchema') ? undefined : _1.EntitySchemaFromJSON(json['entitySchema']),
    };
}
exports.EntitySchemaCreateErrorFromJSONTyped = EntitySchemaCreateErrorFromJSONTyped;
function EntitySchemaCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'entitySchema': _1.EntitySchemaToJSON(value.entitySchema),
    };
}
exports.EntitySchemaCreateErrorToJSON = EntitySchemaCreateErrorToJSON;
