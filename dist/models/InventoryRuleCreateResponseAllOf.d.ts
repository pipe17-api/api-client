/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule } from './';
/**
 *
 * @export
 * @interface InventoryRuleCreateResponseAllOf
 */
export interface InventoryRuleCreateResponseAllOf {
    /**
     *
     * @type {InventoryRule}
     * @memberof InventoryRuleCreateResponseAllOf
     */
    inventoryrule?: InventoryRule;
}
export declare function InventoryRuleCreateResponseAllOfFromJSON(json: any): InventoryRuleCreateResponseAllOf;
export declare function InventoryRuleCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleCreateResponseAllOf;
export declare function InventoryRuleCreateResponseAllOfToJSON(value?: InventoryRuleCreateResponseAllOf | null): any;
