/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Webhook, WebhooksListFilter } from './';
/**
 *
 * @export
 * @interface WebhooksListResponse
 */
export interface WebhooksListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof WebhooksListResponse
     */
    success: WebhooksListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhooksListResponse
     */
    code: WebhooksListResponseCodeEnum;
    /**
     *
     * @type {WebhooksListFilter}
     * @memberof WebhooksListResponse
     */
    filters?: WebhooksListFilter;
    /**
     *
     * @type {Array<Webhook>}
     * @memberof WebhooksListResponse
     */
    webhooks?: Array<Webhook>;
    /**
     *
     * @type {Pagination}
     * @memberof WebhooksListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhooksListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum WebhooksListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function WebhooksListResponseFromJSON(json: any): WebhooksListResponse;
export declare function WebhooksListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhooksListResponse;
export declare function WebhooksListResponseToJSON(value?: WebhooksListResponse | null): any;
