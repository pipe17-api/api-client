/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderStatus } from './';
/**
 *
 * @export
 * @interface OrdersListFilter
 */
export interface OrdersListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrdersListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrdersListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrdersListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrdersListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrdersListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrdersListFilter
     */
    count?: number;
    /**
     * Orders by list of orderId
     * @type {Array<string>}
     * @memberof OrdersListFilter
     */
    orderId?: Array<string>;
    /**
     * Orders by list of extOrderId
     * @type {Array<string>}
     * @memberof OrdersListFilter
     */
    extOrderId?: Array<string>;
    /**
     * Orders by list of order sources
     * @type {Array<string>}
     * @memberof OrdersListFilter
     */
    orderSource?: Array<string>;
    /**
     * Orders by list of statuses
     * @type {Array<OrderStatus>}
     * @memberof OrdersListFilter
     */
    status?: Array<OrderStatus>;
    /**
     * Soft deleted orders
     * @type {boolean}
     * @memberof OrdersListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof OrdersListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof OrdersListFilter
     */
    keys?: string;
    /**
     * Orders matching this recipient email
     * @type {string}
     * @memberof OrdersListFilter
     */
    email?: string;
    /**
     * Orders by shipping labels requirement
     * @type {boolean}
     * @memberof OrdersListFilter
     */
    requireShippingLabels?: boolean;
    /**
     * if set to true use timestamp instead of create time
     * @type {boolean}
     * @memberof OrdersListFilter
     */
    timestamp?: boolean;
}
export declare function OrdersListFilterFromJSON(json: any): OrdersListFilter;
export declare function OrdersListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersListFilter;
export declare function OrdersListFilterToJSON(value?: OrdersListFilter | null): any;
