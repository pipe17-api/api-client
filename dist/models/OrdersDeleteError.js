"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersDeleteErrorToJSON = exports.OrdersDeleteErrorFromJSONTyped = exports.OrdersDeleteErrorFromJSON = exports.OrdersDeleteErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrdersDeleteErrorSuccessEnum;
(function (OrdersDeleteErrorSuccessEnum) {
    OrdersDeleteErrorSuccessEnum["False"] = "false";
})(OrdersDeleteErrorSuccessEnum = exports.OrdersDeleteErrorSuccessEnum || (exports.OrdersDeleteErrorSuccessEnum = {}));
function OrdersDeleteErrorFromJSON(json) {
    return OrdersDeleteErrorFromJSONTyped(json, false);
}
exports.OrdersDeleteErrorFromJSON = OrdersDeleteErrorFromJSON;
function OrdersDeleteErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersDeleteFilterFromJSON(json['filters']),
    };
}
exports.OrdersDeleteErrorFromJSONTyped = OrdersDeleteErrorFromJSONTyped;
function OrdersDeleteErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.OrdersDeleteFilterToJSON(value.filters),
    };
}
exports.OrdersDeleteErrorToJSON = OrdersDeleteErrorToJSON;
