/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionsFilter
 */
export interface ExceptionsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ExceptionsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ExceptionsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ExceptionsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ExceptionsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ExceptionsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ExceptionsFilter
     */
    count?: number;
    /**
     * Exceptions by status-list
     * @type {Array<string>}
     * @memberof ExceptionsFilter
     */
    status?: Array<string>;
    /**
     * Exceptions by exceptionId-list
     * @type {Array<string>}
     * @memberof ExceptionsFilter
     */
    exceptionId?: Array<string>;
    /**
     * Exceptions by exceptionType-list
     * @type {Array<string>}
     * @memberof ExceptionsFilter
     */
    exceptionType?: Array<string>;
    /**
     * Exceptions related to this list of entities
     * @type {Array<string>}
     * @memberof ExceptionsFilter
     */
    entityId?: Array<string>;
    /**
     * Exceptions related to list of entity types
     * @type {Array<string>}
     * @memberof ExceptionsFilter
     */
    entityType?: Array<string>;
}
export declare function ExceptionsFilterFromJSON(json: any): ExceptionsFilter;
export declare function ExceptionsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsFilter;
export declare function ExceptionsFilterToJSON(value?: ExceptionsFilter | null): any;
