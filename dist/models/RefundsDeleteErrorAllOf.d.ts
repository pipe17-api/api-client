/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RefundsDeleteErrorAllOf
 */
export interface RefundsDeleteErrorAllOf {
    /**
     *
     * @type {RefundsDeleteFilter}
     * @memberof RefundsDeleteErrorAllOf
     */
    filters?: RefundsDeleteFilter;
}
export declare function RefundsDeleteErrorAllOfFromJSON(json: any): RefundsDeleteErrorAllOf;
export declare function RefundsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsDeleteErrorAllOf;
export declare function RefundsDeleteErrorAllOfToJSON(value?: RefundsDeleteErrorAllOf | null): any;
