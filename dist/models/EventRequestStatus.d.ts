/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Event Status
 * @export
 * @enum {string}
 */
export declare enum EventRequestStatus {
    Pending = "pending",
    Acknowledged = "acknowledged",
    Completed = "completed",
    Failed = "failed"
}
export declare function EventRequestStatusFromJSON(json: any): EventRequestStatus;
export declare function EventRequestStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventRequestStatus;
export declare function EventRequestStatusToJSON(value?: EventRequestStatus | null): any;
