/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface NotImplementedAllOf
 */
export interface NotImplementedAllOf {
    /**
     *
     * @type {string}
     * @memberof NotImplementedAllOf
     */
    message?: string;
}
export declare function NotImplementedAllOfFromJSON(json: any): NotImplementedAllOf;
export declare function NotImplementedAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): NotImplementedAllOf;
export declare function NotImplementedAllOfToJSON(value?: NotImplementedAllOf | null): any;
