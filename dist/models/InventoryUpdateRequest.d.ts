/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventSource } from './';
/**
 *
 * @export
 * @interface InventoryUpdateRequest
 */
export interface InventoryUpdateRequest {
    /**
     * Event updating inventory
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    event: InventoryUpdateRequestEventEnum;
    /**
     *
     * @type {EventSource}
     * @memberof InventoryUpdateRequest
     */
    ptype: EventSource;
    /**
     * Item SKU
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    sku: string;
    /**
     * Location Id of item
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    locationId: string;
    /**
     * Set if item location has \"infinite\" availability
     * @type {boolean}
     * @memberof InventoryUpdateRequest
     */
    infinite?: boolean;
    /**
     * Number of items affected
     * @type {number}
     * @memberof InventoryUpdateRequest
     */
    quantity: number;
    /**
     * Entity id of the record which is directly related to this inventory change
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    entityId?: string;
    /**
     * Type of the record which is directly related to this inventory change
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    entityType?: InventoryUpdateRequestEntityTypeEnum;
    /**
     * Pipe17 internal order id that initiated this inventory change
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    orderId?: string;
    /**
     * Type of order that initiated this inventory change
     * @type {string}
     * @memberof InventoryUpdateRequest
     */
    orderType?: InventoryUpdateRequestOrderTypeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateRequestEventEnum {
    Ship = "ship",
    Fulfill = "fulfill",
    Xferout = "xferout",
    Xferfulfill = "xferfulfill",
    Xferin = "xferin",
    Receive = "receive",
    FutureShip = "futureShip",
    Release = "release",
    ShipCancel = "shipCancel",
    ShipCancelRestock = "shipCancelRestock",
    FutureShipCancel = "futureShipCancel",
    VirtualCommit = "virtualCommit"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateRequestEntityTypeEnum {
    Arrivals = "arrivals",
    Fulfillments = "fulfillments",
    Inventory = "inventory",
    Receipts = "receipts",
    Shipments = "shipments",
    Orders = "orders"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateRequestOrderTypeEnum {
    Sales = "sales",
    Transfer = "transfer",
    Purchase = "purchase"
}
export declare function InventoryUpdateRequestFromJSON(json: any): InventoryUpdateRequest;
export declare function InventoryUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryUpdateRequest;
export declare function InventoryUpdateRequestToJSON(value?: InventoryUpdateRequest | null): any;
