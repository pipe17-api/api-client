/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingUpdateData
 */
export interface OrderRoutingUpdateData {
    /**
     * Routing rules definition
     * @type {Array<object>}
     * @memberof OrderRoutingUpdateData
     */
    rules?: Array<object>;
}
export declare function OrderRoutingUpdateDataFromJSON(json: any): OrderRoutingUpdateData;
export declare function OrderRoutingUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingUpdateData;
export declare function OrderRoutingUpdateDataToJSON(value?: OrderRoutingUpdateData | null): any;
