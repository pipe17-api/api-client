"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelCreateErrorToJSON = exports.LabelCreateErrorFromJSONTyped = exports.LabelCreateErrorFromJSON = exports.LabelCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var LabelCreateErrorSuccessEnum;
(function (LabelCreateErrorSuccessEnum) {
    LabelCreateErrorSuccessEnum["False"] = "false";
})(LabelCreateErrorSuccessEnum = exports.LabelCreateErrorSuccessEnum || (exports.LabelCreateErrorSuccessEnum = {}));
function LabelCreateErrorFromJSON(json) {
    return LabelCreateErrorFromJSONTyped(json, false);
}
exports.LabelCreateErrorFromJSON = LabelCreateErrorFromJSON;
function LabelCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.LabelCreateErrorFromJSONTyped = LabelCreateErrorFromJSONTyped;
function LabelCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.LabelCreateErrorToJSON = LabelCreateErrorToJSON;
