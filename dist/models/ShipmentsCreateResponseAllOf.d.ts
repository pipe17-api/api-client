/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentCreateResult } from './';
/**
 *
 * @export
 * @interface ShipmentsCreateResponseAllOf
 */
export interface ShipmentsCreateResponseAllOf {
    /**
     *
     * @type {Array<ShipmentCreateResult>}
     * @memberof ShipmentsCreateResponseAllOf
     */
    shipments?: Array<ShipmentCreateResult>;
}
export declare function ShipmentsCreateResponseAllOfFromJSON(json: any): ShipmentsCreateResponseAllOf;
export declare function ShipmentsCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsCreateResponseAllOf;
export declare function ShipmentsCreateResponseAllOfToJSON(value?: ShipmentsCreateResponseAllOf | null): any;
