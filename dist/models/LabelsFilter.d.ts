/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelsFilter
 */
export interface LabelsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof LabelsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof LabelsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof LabelsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof LabelsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof LabelsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof LabelsFilter
     */
    count?: number;
    /**
     * Labels by list of labelId
     * @type {Array<string>}
     * @memberof LabelsFilter
     */
    labelId?: Array<string>;
    /**
     * Soft deleted labels
     * @type {boolean}
     * @memberof LabelsFilter
     */
    deleted?: boolean;
}
export declare function LabelsFilterFromJSON(json: any): LabelsFilter;
export declare function LabelsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsFilter;
export declare function LabelsFilterToJSON(value?: LabelsFilter | null): any;
