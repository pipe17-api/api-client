"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingUpdateDataToJSON = exports.RoutingUpdateDataFromJSONTyped = exports.RoutingUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingUpdateDataFromJSON(json) {
    return RoutingUpdateDataFromJSONTyped(json, false);
}
exports.RoutingUpdateDataFromJSON = RoutingUpdateDataFromJSON;
function RoutingUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'enabled': !runtime_1.exists(json, 'enabled') ? undefined : json['enabled'],
        'filter': !runtime_1.exists(json, 'filter') ? undefined : json['filter'],
    };
}
exports.RoutingUpdateDataFromJSONTyped = RoutingUpdateDataFromJSONTyped;
function RoutingUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'description': value.description,
        'enabled': value.enabled,
        'filter': value.filter,
    };
}
exports.RoutingUpdateDataToJSON = RoutingUpdateDataToJSON;
