"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderUpdateLineItemToJSON = exports.OrderUpdateLineItemFromJSONTyped = exports.OrderUpdateLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrderUpdateLineItemFromJSON(json) {
    return OrderUpdateLineItemFromJSONTyped(json, false);
}
exports.OrderUpdateLineItemFromJSON = OrderUpdateLineItemFromJSON;
function OrderUpdateLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': !runtime_1.exists(json, 'uniqueId') ? undefined : json['uniqueId'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'quantity': !runtime_1.exists(json, 'quantity') ? undefined : json['quantity'],
        'itemPrice': !runtime_1.exists(json, 'itemPrice') ? undefined : json['itemPrice'],
        'itemDiscount': !runtime_1.exists(json, 'itemDiscount') ? undefined : json['itemDiscount'],
        'itemTax': !runtime_1.exists(json, 'itemTax') ? undefined : json['itemTax'],
        'requiresShipping': !runtime_1.exists(json, 'requiresShipping') ? undefined : json['requiresShipping'],
        'taxable': !runtime_1.exists(json, 'taxable') ? undefined : json['taxable'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
    };
}
exports.OrderUpdateLineItemFromJSONTyped = OrderUpdateLineItemFromJSONTyped;
function OrderUpdateLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'sku': value.sku,
        'name': value.name,
        'quantity': value.quantity,
        'itemPrice': value.itemPrice,
        'itemDiscount': value.itemDiscount,
        'itemTax': value.itemTax,
        'requiresShipping': value.requiresShipping,
        'taxable': value.taxable,
        'locationId': value.locationId,
    };
}
exports.OrderUpdateLineItemToJSON = OrderUpdateLineItemToJSON;
