"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsFilterToJSON = exports.EventsFilterFromJSONTyped = exports.EventsFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EventsFilterFromJSON(json) {
    return EventsFilterFromJSONTyped(json, false);
}
exports.EventsFilterFromJSON = EventsFilterFromJSON;
function EventsFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'eventId': !runtime_1.exists(json, 'eventId') ? undefined : json['eventId'],
        'source': !runtime_1.exists(json, 'source') ? undefined : (json['source'].map(_1.EventRequestSourceFromJSON)),
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.EventRequestStatusFromJSON)),
        'requestId': !runtime_1.exists(json, 'requestId') ? undefined : json['requestId'],
    };
}
exports.EventsFilterFromJSONTyped = EventsFilterFromJSONTyped;
function EventsFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'eventId': value.eventId,
        'source': value.source === undefined ? undefined : (value.source.map(_1.EventRequestSourceToJSON)),
        'status': value.status === undefined ? undefined : (value.status.map(_1.EventRequestStatusToJSON)),
        'requestId': value.requestId,
    };
}
exports.EventsFilterToJSON = EventsFilterToJSON;
