"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleCreateDataToJSON = exports.RoleCreateDataFromJSONTyped = exports.RoleCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleCreateDataFromJSON(json) {
    return RoleCreateDataFromJSONTyped(json, false);
}
exports.RoleCreateDataFromJSON = RoleCreateDataFromJSON;
function RoleCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'name': json['name'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'methods': !runtime_1.exists(json, 'methods') ? undefined : _1.MethodsFromJSON(json['methods']),
    };
}
exports.RoleCreateDataFromJSONTyped = RoleCreateDataFromJSONTyped;
function RoleCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'isPublic': value.isPublic,
        'name': value.name,
        'description': value.description,
        'methods': _1.MethodsToJSON(value.methods),
    };
}
exports.RoleCreateDataToJSON = RoleCreateDataToJSON;
