/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Routings Filter
 * @export
 * @interface RoutingsFilterAllOf
 */
export interface RoutingsFilterAllOf {
    /**
     * Routings by list of routingId
     * @type {Array<string>}
     * @memberof RoutingsFilterAllOf
     */
    routingId?: Array<string>;
    /**
     * Routings by list of UUIDs
     * @type {Array<string>}
     * @memberof RoutingsFilterAllOf
     */
    uuid?: Array<string>;
    /**
     * Soft deleted routings
     * @type {boolean}
     * @memberof RoutingsFilterAllOf
     */
    deleted?: boolean;
}
export declare function RoutingsFilterAllOfFromJSON(json: any): RoutingsFilterAllOf;
export declare function RoutingsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsFilterAllOf;
export declare function RoutingsFilterAllOfToJSON(value?: RoutingsFilterAllOf | null): any;
