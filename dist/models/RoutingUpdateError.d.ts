/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingUpdateData } from './';
/**
 *
 * @export
 * @interface RoutingUpdateError
 */
export interface RoutingUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoutingUpdateError
     */
    success: RoutingUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoutingUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoutingUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RoutingUpdateData}
     * @memberof RoutingUpdateError
     */
    routing?: RoutingUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingUpdateErrorSuccessEnum {
    False = "false"
}
export declare function RoutingUpdateErrorFromJSON(json: any): RoutingUpdateError;
export declare function RoutingUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingUpdateError;
export declare function RoutingUpdateErrorToJSON(value?: RoutingUpdateError | null): any;
