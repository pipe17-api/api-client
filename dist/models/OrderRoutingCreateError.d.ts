/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRouting } from './';
/**
 *
 * @export
 * @interface OrderRoutingCreateError
 */
export interface OrderRoutingCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrderRoutingCreateError
     */
    success: OrderRoutingCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrderRoutingCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrderRoutingCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrderRouting}
     * @memberof OrderRoutingCreateError
     */
    routing?: OrderRouting;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingCreateErrorSuccessEnum {
    False = "false"
}
export declare function OrderRoutingCreateErrorFromJSON(json: any): OrderRoutingCreateError;
export declare function OrderRoutingCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingCreateError;
export declare function OrderRoutingCreateErrorToJSON(value?: OrderRoutingCreateError | null): any;
