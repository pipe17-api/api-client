/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorSetupData
 */
export interface ConnectorSetupData {
    /**
     * Public connector indicator
     * @type {boolean}
     * @memberof ConnectorSetupData
     */
    isPublic?: boolean;
    /**
     * Connector rank in category (higher ranks indicate better match)
     * @type {number}
     * @memberof ConnectorSetupData
     */
    rank?: number;
    /**
     * Indicates whether only one integration is allowed to be created in org
     * @type {boolean}
     * @memberof ConnectorSetupData
     */
    orgUnique?: boolean;
}
export declare function ConnectorSetupDataFromJSON(json: any): ConnectorSetupData;
export declare function ConnectorSetupDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorSetupData;
export declare function ConnectorSetupDataToJSON(value?: ConnectorSetupData | null): any;
