"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleUpdateErrorToJSON = exports.InventoryRuleUpdateErrorFromJSONTyped = exports.InventoryRuleUpdateErrorFromJSON = exports.InventoryRuleUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryRuleUpdateErrorSuccessEnum;
(function (InventoryRuleUpdateErrorSuccessEnum) {
    InventoryRuleUpdateErrorSuccessEnum["False"] = "false";
})(InventoryRuleUpdateErrorSuccessEnum = exports.InventoryRuleUpdateErrorSuccessEnum || (exports.InventoryRuleUpdateErrorSuccessEnum = {}));
function InventoryRuleUpdateErrorFromJSON(json) {
    return InventoryRuleUpdateErrorFromJSONTyped(json, false);
}
exports.InventoryRuleUpdateErrorFromJSON = InventoryRuleUpdateErrorFromJSON;
function InventoryRuleUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'inventoryrule': !runtime_1.exists(json, 'inventoryrule') ? undefined : _1.InventoryRuleUpdateDataFromJSON(json['inventoryrule']),
    };
}
exports.InventoryRuleUpdateErrorFromJSONTyped = InventoryRuleUpdateErrorFromJSONTyped;
function InventoryRuleUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'inventoryrule': _1.InventoryRuleUpdateDataToJSON(value.inventoryrule),
    };
}
exports.InventoryRuleUpdateErrorToJSON = InventoryRuleUpdateErrorToJSON;
