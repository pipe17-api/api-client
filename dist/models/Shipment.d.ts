/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ShipmentCreateStatus, ShipmentLineItem, ShipmentOrderType } from './';
/**
 *
 * @export
 * @interface Shipment
 */
export interface Shipment {
    /**
     * Shipment ID
     * @type {string}
     * @memberof Shipment
     */
    shipmentId?: string;
    /**
     *
     * @type {ShipmentCreateStatus}
     * @memberof Shipment
     */
    status?: ShipmentCreateStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Shipment
     */
    integration?: string;
    /**
     * Sent to fulfillment timestamp
     * @type {Date}
     * @memberof Shipment
     */
    sentToFulfillmentAt?: Date;
    /**
     * Destination of this shipment request
     * @type {string}
     * @memberof Shipment
     */
    fulfillmentIntegrationId?: string;
    /**
     * Customer friendly ID on the order object
     * @type {string}
     * @memberof Shipment
     */
    extOrderId: string;
    /**
     * Customer Friendly Internal reference to ShipmentId that they use
     * @type {string}
     * @memberof Shipment
     */
    extShipmentId?: string;
    /**
     * Pipe17 Internal Order's orderId
     * @type {string}
     * @memberof Shipment
     */
    orderId?: string;
    /**
     * Actual Order source, Walmart, Etsy, etc
     * @type {string}
     * @memberof Shipment
     */
    orderSource?: string;
    /**
     *
     * @type {ShipmentOrderType}
     * @memberof Shipment
     */
    orderType?: ShipmentOrderType;
    /**
     * Create time of linked order
     * @type {Date}
     * @memberof Shipment
     */
    orderCreateTime?: Date;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof Shipment
     */
    shipByDate?: Date;
    /**
     * Expected ship date
     * @type {Date}
     * @memberof Shipment
     */
    expectedShipDate?: Date;
    /**
     * First Priority to be respected
     * @type {Date}
     * @memberof Shipment
     */
    expectedDeliveryDate?: Date;
    /**
     *
     * @type {Address}
     * @memberof Shipment
     */
    shippingAddress: Address;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority
     * @type {string}
     * @memberof Shipment
     */
    shippingCarrier?: string;
    /**
     * 3rd Priority
     * @type {string}
     * @memberof Shipment
     */
    shippingCode?: string;
    /**
     * Required with ShippingCarrier, 2nd Priority
     * @type {string}
     * @memberof Shipment
     */
    shippingClass?: string;
    /**
     * Shipping Note
     * @type {string}
     * @memberof Shipment
     */
    shippingNote?: string;
    /**
     * Shipping Labels
     * @type {Array<string>}
     * @memberof Shipment
     */
    shippingLabels?: Array<string>;
    /**
     * Shipping Gift note
     * @type {string}
     * @memberof Shipment
     */
    giftNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof Shipment
     */
    incoterms?: string;
    /**
     * Id of location defined in organization
     * @type {string}
     * @memberof Shipment
     */
    locationId?: string;
    /**
     *
     * @type {Array<ShipmentLineItem>}
     * @memberof Shipment
     */
    lineItems?: Array<ShipmentLineItem>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Shipment
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Shipment
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Shipment
     */
    readonly orgKey?: string;
}
export declare function ShipmentFromJSON(json: any): Shipment;
export declare function ShipmentFromJSONTyped(json: any, ignoreDiscriminator: boolean): Shipment;
export declare function ShipmentToJSON(value?: Shipment | null): any;
