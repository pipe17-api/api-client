/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryDeleteFilter
 */
export interface InventoryDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof InventoryDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof InventoryDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof InventoryDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof InventoryDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof InventoryDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof InventoryDeleteFilter
     */
    count?: number;
    /**
     * Inventory items by list of inventoryId
     * @type {Array<string>}
     * @memberof InventoryDeleteFilter
     */
    inventoryId?: Array<string>;
    /**
     * Return inventory items related to certain event record by record id
     * @type {Array<string>}
     * @memberof InventoryDeleteFilter
     */
    entityId?: Array<string>;
    /**
     * Fetch inventory by its SKU
     * @type {Array<string>}
     * @memberof InventoryDeleteFilter
     */
    sku?: Array<string>;
    /**
     * Fetch inventory by its location Id
     * @type {Array<string>}
     * @memberof InventoryDeleteFilter
     */
    locationId?: Array<string>;
    /**
     * Fetch soft deleted inventory items
     * @type {boolean}
     * @memberof InventoryDeleteFilter
     */
    deleted?: boolean;
}
export declare function InventoryDeleteFilterFromJSON(json: any): InventoryDeleteFilter;
export declare function InventoryDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryDeleteFilter;
export declare function InventoryDeleteFilterToJSON(value?: InventoryDeleteFilter | null): any;
