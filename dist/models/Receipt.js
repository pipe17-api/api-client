"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptToJSON = exports.ReceiptFromJSONTyped = exports.ReceiptFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReceiptFromJSON(json) {
    return ReceiptFromJSONTyped(json, false);
}
exports.ReceiptFromJSON = ReceiptFromJSON;
function ReceiptFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'receiptId': !runtime_1.exists(json, 'receiptId') ? undefined : json['receiptId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'arrivalId': json['arrivalId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingClass': !runtime_1.exists(json, 'shippingClass') ? undefined : json['shippingClass'],
        'currency': !runtime_1.exists(json, 'currency') ? undefined : json['currency'],
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (new Date(json['actualArrivalDate'])),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ReceiptItemFromJSON)),
        'extReceiptId': !runtime_1.exists(json, 'extReceiptId') ? undefined : json['extReceiptId'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.ReceiptFromJSONTyped = ReceiptFromJSONTyped;
function ReceiptToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'receiptId': value.receiptId,
        'extOrderId': value.extOrderId,
        'integration': value.integration,
        'arrivalId': value.arrivalId,
        'shippingCarrier': value.shippingCarrier,
        'shippingClass': value.shippingClass,
        'currency': value.currency,
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (new Date(value.actualArrivalDate).toISOString()),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ReceiptItemToJSON)),
        'extReceiptId': value.extReceiptId,
    };
}
exports.ReceiptToJSON = ReceiptToJSON;
