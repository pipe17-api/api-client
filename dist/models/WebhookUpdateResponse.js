"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookUpdateResponseToJSON = exports.WebhookUpdateResponseFromJSONTyped = exports.WebhookUpdateResponseFromJSON = exports.WebhookUpdateResponseCodeEnum = exports.WebhookUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var WebhookUpdateResponseSuccessEnum;
(function (WebhookUpdateResponseSuccessEnum) {
    WebhookUpdateResponseSuccessEnum["True"] = "true";
})(WebhookUpdateResponseSuccessEnum = exports.WebhookUpdateResponseSuccessEnum || (exports.WebhookUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var WebhookUpdateResponseCodeEnum;
(function (WebhookUpdateResponseCodeEnum) {
    WebhookUpdateResponseCodeEnum[WebhookUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    WebhookUpdateResponseCodeEnum[WebhookUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    WebhookUpdateResponseCodeEnum[WebhookUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(WebhookUpdateResponseCodeEnum = exports.WebhookUpdateResponseCodeEnum || (exports.WebhookUpdateResponseCodeEnum = {}));
function WebhookUpdateResponseFromJSON(json) {
    return WebhookUpdateResponseFromJSONTyped(json, false);
}
exports.WebhookUpdateResponseFromJSON = WebhookUpdateResponseFromJSON;
function WebhookUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.WebhookUpdateResponseFromJSONTyped = WebhookUpdateResponseFromJSONTyped;
function WebhookUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.WebhookUpdateResponseToJSON = WebhookUpdateResponseToJSON;
