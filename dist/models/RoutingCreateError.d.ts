/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingCreateData } from './';
/**
 *
 * @export
 * @interface RoutingCreateError
 */
export interface RoutingCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof RoutingCreateError
     */
    success: RoutingCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof RoutingCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof RoutingCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {RoutingCreateData}
     * @memberof RoutingCreateError
     */
    routing?: RoutingCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingCreateErrorSuccessEnum {
    False = "false"
}
export declare function RoutingCreateErrorFromJSON(json: any): RoutingCreateError;
export declare function RoutingCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingCreateError;
export declare function RoutingCreateErrorToJSON(value?: RoutingCreateError | null): any;
