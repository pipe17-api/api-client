/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Access Methods Allowed
 * @export
 * @interface Methods
 */
export interface Methods {
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    accounts?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    apikey?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    arrivals?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    connectors?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    fulfillments?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    integrations?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    inventory?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    inventoryrules?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    labels?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    locations?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    mappings?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    orders?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    organizations?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    products?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    purchases?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    receipts?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    roles?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    routings?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    shipments?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    suppliers?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    transfers?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    trackings?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    users?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    webhooks?: string;
    /**
     *
     * @type {string}
     * @memberof Methods
     */
    convert?: MethodsConvertEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum MethodsConvertEnum {
    C = "c"
}
export declare function MethodsFromJSON(json: any): Methods;
export declare function MethodsFromJSONTyped(json: any, ignoreDiscriminator: boolean): Methods;
export declare function MethodsToJSON(value?: Methods | null): any;
