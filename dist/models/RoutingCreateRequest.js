"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingCreateRequestToJSON = exports.RoutingCreateRequestFromJSONTyped = exports.RoutingCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingCreateRequestFromJSON(json) {
    return RoutingCreateRequestFromJSONTyped(json, false);
}
exports.RoutingCreateRequestFromJSON = RoutingCreateRequestFromJSON;
function RoutingCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'isPublic': json['isPublic'],
        'description': json['description'],
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'enabled': json['enabled'],
        'filter': json['filter'],
    };
}
exports.RoutingCreateRequestFromJSONTyped = RoutingCreateRequestFromJSONTyped;
function RoutingCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'isPublic': value.isPublic,
        'description': value.description,
        'uuid': value.uuid,
        'enabled': value.enabled,
        'filter': value.filter,
    };
}
exports.RoutingCreateRequestToJSON = RoutingCreateRequestToJSON;
