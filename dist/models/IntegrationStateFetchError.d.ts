/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationStateFetchError
 */
export interface IntegrationStateFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationStateFetchError
     */
    success: IntegrationStateFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationStateFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationStateFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationStateFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationStateFetchErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationStateFetchErrorFromJSON(json: any): IntegrationStateFetchError;
export declare function IntegrationStateFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateFetchError;
export declare function IntegrationStateFetchErrorToJSON(value?: IntegrationStateFetchError | null): any;
