/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestStatus, EventResponseData } from './';
/**
 *
 * @export
 * @interface EventUpdateData
 */
export interface EventUpdateData {
    /**
     *
     * @type {EventRequestStatus}
     * @memberof EventUpdateData
     */
    status?: EventRequestStatus;
    /**
     *
     * @type {EventResponseData}
     * @memberof EventUpdateData
     */
    response?: EventResponseData;
    /**
     * operation duration
     * @type {number}
     * @memberof EventUpdateData
     */
    duration?: number;
}
export declare function EventUpdateDataFromJSON(json: any): EventUpdateData;
export declare function EventUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventUpdateData;
export declare function EventUpdateDataToJSON(value?: EventUpdateData | null): any;
