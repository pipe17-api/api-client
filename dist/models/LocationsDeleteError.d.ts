/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationsDeleteFilter } from './';
/**
 *
 * @export
 * @interface LocationsDeleteError
 */
export interface LocationsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LocationsDeleteError
     */
    success: LocationsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LocationsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LocationsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {LocationsDeleteFilter}
     * @memberof LocationsDeleteError
     */
    filters?: LocationsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function LocationsDeleteErrorFromJSON(json: any): LocationsDeleteError;
export declare function LocationsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsDeleteError;
export declare function LocationsDeleteErrorToJSON(value?: LocationsDeleteError | null): any;
