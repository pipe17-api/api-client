"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleUpdateDataToJSON = exports.RoleUpdateDataFromJSONTyped = exports.RoleUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleUpdateDataFromJSON(json) {
    return RoleUpdateDataFromJSONTyped(json, false);
}
exports.RoleUpdateDataFromJSON = RoleUpdateDataFromJSON;
function RoleUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'methods': !runtime_1.exists(json, 'methods') ? undefined : _1.MethodsUpdateDataFromJSON(json['methods']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.RoleStatusFromJSON(json['status']),
    };
}
exports.RoleUpdateDataFromJSONTyped = RoleUpdateDataFromJSONTyped;
function RoleUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'description': value.description,
        'methods': _1.MethodsUpdateDataToJSON(value.methods),
        'status': _1.RoleStatusToJSON(value.status),
    };
}
exports.RoleUpdateDataToJSON = RoleUpdateDataToJSON;
