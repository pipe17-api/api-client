/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Role, RolesListFilter } from './';
/**
 *
 * @export
 * @interface RolesListResponseAllOf
 */
export interface RolesListResponseAllOf {
    /**
     *
     * @type {RolesListFilter}
     * @memberof RolesListResponseAllOf
     */
    filters?: RolesListFilter;
    /**
     *
     * @type {Array<Role>}
     * @memberof RolesListResponseAllOf
     */
    roles?: Array<Role>;
    /**
     *
     * @type {Pagination}
     * @memberof RolesListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function RolesListResponseAllOfFromJSON(json: any): RolesListResponseAllOf;
export declare function RolesListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesListResponseAllOf;
export declare function RolesListResponseAllOfToJSON(value?: RolesListResponseAllOf | null): any;
