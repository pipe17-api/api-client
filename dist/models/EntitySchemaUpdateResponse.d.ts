/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntitySchemaUpdateResponse
 */
export interface EntitySchemaUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntitySchemaUpdateResponse
     */
    success: EntitySchemaUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaUpdateResponse
     */
    code: EntitySchemaUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntitySchemaUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntitySchemaUpdateResponseFromJSON(json: any): EntitySchemaUpdateResponse;
export declare function EntitySchemaUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaUpdateResponse;
export declare function EntitySchemaUpdateResponseToJSON(value?: EntitySchemaUpdateResponse | null): any;
