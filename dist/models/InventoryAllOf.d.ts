/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventType } from './';
/**
 *
 * @export
 * @interface InventoryAllOf
 */
export interface InventoryAllOf {
    /**
     * Inventory ID
     * @type {string}
     * @memberof InventoryAllOf
     */
    inventoryId?: string;
    /**
     * Related entity ID
     * @type {string}
     * @memberof InventoryAllOf
     */
    entityId?: string;
    /**
     * Related entity ID's type
     * @type {string}
     * @memberof InventoryAllOf
     */
    entityType?: string;
    /**
     *
     * @type {EventType}
     * @memberof InventoryAllOf
     */
    event?: EventType;
    /**
     * Item SKU
     * @type {string}
     * @memberof InventoryAllOf
     */
    sku?: string;
    /**
     * Location of item
     * @type {string}
     * @memberof InventoryAllOf
     */
    locationId?: string;
    /**
     * Number of items on hand
     * @type {number}
     * @memberof InventoryAllOf
     */
    onHand?: number;
    /**
     * Number of items committed
     * @type {number}
     * @memberof InventoryAllOf
     */
    committed?: number;
    /**
     * Number of items committed for pre-order
     * @type {number}
     * @memberof InventoryAllOf
     */
    committedFuture?: number;
    /**
     * Number of items that can be sold for pre-order
     * @type {number}
     * @memberof InventoryAllOf
     */
    future?: number;
    /**
     * Number of items available
     * @type {number}
     * @memberof InventoryAllOf
     */
    available?: number;
    /**
     * Number of items available to promise
     * @type {number}
     * @memberof InventoryAllOf
     */
    availableToPromise?: number;
    /**
     * Number of items affected
     * @type {number}
     * @memberof InventoryAllOf
     */
    quantity?: number;
    /**
     * Number of incoming items in transit (maybe included in available)
     * @type {number}
     * @memberof InventoryAllOf
     */
    incoming?: number;
    /**
     * Number of items being shipped (included in committed)
     * @type {number}
     * @memberof InventoryAllOf
     */
    commitShip?: number;
    /**
     * Number of items being transferred out (included in committed)
     * @type {number}
     * @memberof InventoryAllOf
     */
    commitXfer?: number;
    /**
     * Number of unavailable items
     * @type {number}
     * @memberof InventoryAllOf
     */
    unavailable?: number;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof InventoryAllOf
     */
    integration?: string;
    /**
     * Inventory status (deleted or not)
     * @type {string}
     * @memberof InventoryAllOf
     */
    status?: string;
    /**
     * Pipe17 internal order id that initiated this inventory change
     * @type {string}
     * @memberof InventoryAllOf
     */
    orderId?: string;
    /**
     * Type of order that initiated this inventory change
     * @type {string}
     * @memberof InventoryAllOf
     */
    orderType?: string;
    /**
     * Pipe17 internal id of the user who makes the inventory adjustment via the portal
     * @type {string}
     * @memberof InventoryAllOf
     */
    user?: string;
}
export declare function InventoryAllOfFromJSON(json: any): InventoryAllOf;
export declare function InventoryAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryAllOf;
export declare function InventoryAllOfToJSON(value?: InventoryAllOf | null): any;
