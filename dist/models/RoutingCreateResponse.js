"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingCreateResponseToJSON = exports.RoutingCreateResponseFromJSONTyped = exports.RoutingCreateResponseFromJSON = exports.RoutingCreateResponseCodeEnum = exports.RoutingCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RoutingCreateResponseSuccessEnum;
(function (RoutingCreateResponseSuccessEnum) {
    RoutingCreateResponseSuccessEnum["True"] = "true";
})(RoutingCreateResponseSuccessEnum = exports.RoutingCreateResponseSuccessEnum || (exports.RoutingCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RoutingCreateResponseCodeEnum;
(function (RoutingCreateResponseCodeEnum) {
    RoutingCreateResponseCodeEnum[RoutingCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RoutingCreateResponseCodeEnum[RoutingCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RoutingCreateResponseCodeEnum[RoutingCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RoutingCreateResponseCodeEnum = exports.RoutingCreateResponseCodeEnum || (exports.RoutingCreateResponseCodeEnum = {}));
function RoutingCreateResponseFromJSON(json) {
    return RoutingCreateResponseFromJSONTyped(json, false);
}
exports.RoutingCreateResponseFromJSON = RoutingCreateResponseFromJSON;
function RoutingCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.RoutingFromJSON(json['routing']),
    };
}
exports.RoutingCreateResponseFromJSONTyped = RoutingCreateResponseFromJSONTyped;
function RoutingCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'routing': _1.RoutingToJSON(value.routing),
    };
}
exports.RoutingCreateResponseToJSON = RoutingCreateResponseToJSON;
