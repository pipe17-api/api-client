/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationCreateData } from './';
/**
 *
 * @export
 * @interface OrganizationCreateErrorAllOf
 */
export interface OrganizationCreateErrorAllOf {
    /**
     *
     * @type {OrganizationCreateData}
     * @memberof OrganizationCreateErrorAllOf
     */
    organization?: OrganizationCreateData;
}
export declare function OrganizationCreateErrorAllOfFromJSON(json: any): OrganizationCreateErrorAllOf;
export declare function OrganizationCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationCreateErrorAllOf;
export declare function OrganizationCreateErrorAllOfToJSON(value?: OrganizationCreateErrorAllOf | null): any;
