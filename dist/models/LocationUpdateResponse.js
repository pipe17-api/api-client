"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationUpdateResponseToJSON = exports.LocationUpdateResponseFromJSONTyped = exports.LocationUpdateResponseFromJSON = exports.LocationUpdateResponseCodeEnum = exports.LocationUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var LocationUpdateResponseSuccessEnum;
(function (LocationUpdateResponseSuccessEnum) {
    LocationUpdateResponseSuccessEnum["True"] = "true";
})(LocationUpdateResponseSuccessEnum = exports.LocationUpdateResponseSuccessEnum || (exports.LocationUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LocationUpdateResponseCodeEnum;
(function (LocationUpdateResponseCodeEnum) {
    LocationUpdateResponseCodeEnum[LocationUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LocationUpdateResponseCodeEnum[LocationUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LocationUpdateResponseCodeEnum[LocationUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LocationUpdateResponseCodeEnum = exports.LocationUpdateResponseCodeEnum || (exports.LocationUpdateResponseCodeEnum = {}));
function LocationUpdateResponseFromJSON(json) {
    return LocationUpdateResponseFromJSONTyped(json, false);
}
exports.LocationUpdateResponseFromJSON = LocationUpdateResponseFromJSON;
function LocationUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.LocationUpdateResponseFromJSONTyped = LocationUpdateResponseFromJSONTyped;
function LocationUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.LocationUpdateResponseToJSON = LocationUpdateResponseToJSON;
