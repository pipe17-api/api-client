/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Routing, RoutingsListFilter } from './';
/**
 *
 * @export
 * @interface RoutingsListResponseAllOf
 */
export interface RoutingsListResponseAllOf {
    /**
     *
     * @type {RoutingsListFilter}
     * @memberof RoutingsListResponseAllOf
     */
    filters?: RoutingsListFilter;
    /**
     *
     * @type {Array<Routing>}
     * @memberof RoutingsListResponseAllOf
     */
    routings?: Array<Routing>;
    /**
     *
     * @type {Pagination}
     * @memberof RoutingsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function RoutingsListResponseAllOfFromJSON(json: any): RoutingsListResponseAllOf;
export declare function RoutingsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsListResponseAllOf;
export declare function RoutingsListResponseAllOfToJSON(value?: RoutingsListResponseAllOf | null): any;
