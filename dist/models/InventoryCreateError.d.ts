/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryCreateResult } from './';
/**
 *
 * @export
 * @interface InventoryCreateError
 */
export interface InventoryCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryCreateError
     */
    success: InventoryCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<InventoryCreateResult>}
     * @memberof InventoryCreateError
     */
    inventory?: Array<InventoryCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryCreateErrorSuccessEnum {
    False = "false"
}
export declare function InventoryCreateErrorFromJSON(json: any): InventoryCreateError;
export declare function InventoryCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryCreateError;
export declare function InventoryCreateErrorToJSON(value?: InventoryCreateError | null): any;
