/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryListFilter } from './';
/**
 *
 * @export
 * @interface InventoryListErrorAllOf
 */
export interface InventoryListErrorAllOf {
    /**
     *
     * @type {InventoryListFilter}
     * @memberof InventoryListErrorAllOf
     */
    filters?: InventoryListFilter;
}
export declare function InventoryListErrorAllOfFromJSON(json: any): InventoryListErrorAllOf;
export declare function InventoryListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryListErrorAllOf;
export declare function InventoryListErrorAllOfToJSON(value?: InventoryListErrorAllOf | null): any;
