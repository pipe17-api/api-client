"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersDeleteErrorAllOfToJSON = exports.TransfersDeleteErrorAllOfFromJSONTyped = exports.TransfersDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransfersDeleteErrorAllOfFromJSON(json) {
    return TransfersDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.TransfersDeleteErrorAllOfFromJSON = TransfersDeleteErrorAllOfFromJSON;
function TransfersDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TransfersDeleteFilterFromJSON(json['filters']),
    };
}
exports.TransfersDeleteErrorAllOfFromJSONTyped = TransfersDeleteErrorAllOfFromJSONTyped;
function TransfersDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.TransfersDeleteFilterToJSON(value.filters),
    };
}
exports.TransfersDeleteErrorAllOfToJSON = TransfersDeleteErrorAllOfToJSON;
