/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LocationUpdateResponse
 */
export interface LocationUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LocationUpdateResponse
     */
    success: LocationUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationUpdateResponse
     */
    code: LocationUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LocationUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LocationUpdateResponseFromJSON(json: any): LocationUpdateResponse;
export declare function LocationUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationUpdateResponse;
export declare function LocationUpdateResponseToJSON(value?: LocationUpdateResponse | null): any;
