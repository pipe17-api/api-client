"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressNullableToJSON = exports.AddressNullableFromJSONTyped = exports.AddressNullableFromJSON = void 0;
const runtime_1 = require("../runtime");
function AddressNullableFromJSON(json) {
    return AddressNullableFromJSONTyped(json, false);
}
exports.AddressNullableFromJSON = AddressNullableFromJSON;
function AddressNullableFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'firstName': !runtime_1.exists(json, 'firstName') ? undefined : json['firstName'],
        'lastName': !runtime_1.exists(json, 'lastName') ? undefined : json['lastName'],
        'company': !runtime_1.exists(json, 'company') ? undefined : json['company'],
        'address1': !runtime_1.exists(json, 'address1') ? undefined : json['address1'],
        'address2': !runtime_1.exists(json, 'address2') ? undefined : json['address2'],
        'city': !runtime_1.exists(json, 'city') ? undefined : json['city'],
        'stateOrProvince': !runtime_1.exists(json, 'stateOrProvince') ? undefined : json['stateOrProvince'],
        'zipCodeOrPostalCode': !runtime_1.exists(json, 'zipCodeOrPostalCode') ? undefined : json['zipCodeOrPostalCode'],
        'country': !runtime_1.exists(json, 'country') ? undefined : json['country'],
        'email': !runtime_1.exists(json, 'email') ? undefined : json['email'],
        'phone': !runtime_1.exists(json, 'phone') ? undefined : json['phone'],
    };
}
exports.AddressNullableFromJSONTyped = AddressNullableFromJSONTyped;
function AddressNullableToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'firstName': value.firstName,
        'lastName': value.lastName,
        'company': value.company,
        'address1': value.address1,
        'address2': value.address2,
        'city': value.city,
        'stateOrProvince': value.stateOrProvince,
        'zipCodeOrPostalCode': value.zipCodeOrPostalCode,
        'country': value.country,
        'email': value.email,
        'phone': value.phone,
    };
}
exports.AddressNullableToJSON = AddressNullableToJSON;
