"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsCreateErrorToJSON = exports.ArrivalsCreateErrorFromJSONTyped = exports.ArrivalsCreateErrorFromJSON = exports.ArrivalsCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalsCreateErrorSuccessEnum;
(function (ArrivalsCreateErrorSuccessEnum) {
    ArrivalsCreateErrorSuccessEnum["False"] = "false";
})(ArrivalsCreateErrorSuccessEnum = exports.ArrivalsCreateErrorSuccessEnum || (exports.ArrivalsCreateErrorSuccessEnum = {}));
function ArrivalsCreateErrorFromJSON(json) {
    return ArrivalsCreateErrorFromJSONTyped(json, false);
}
exports.ArrivalsCreateErrorFromJSON = ArrivalsCreateErrorFromJSON;
function ArrivalsCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'arrivals': !runtime_1.exists(json, 'arrivals') ? undefined : (json['arrivals'].map(_1.ArrivalCreateResultFromJSON)),
    };
}
exports.ArrivalsCreateErrorFromJSONTyped = ArrivalsCreateErrorFromJSONTyped;
function ArrivalsCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'arrivals': value.arrivals === undefined ? undefined : (value.arrivals.map(_1.ArrivalCreateResultToJSON)),
    };
}
exports.ArrivalsCreateErrorToJSON = ArrivalsCreateErrorToJSON;
