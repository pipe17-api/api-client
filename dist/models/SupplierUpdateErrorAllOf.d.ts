/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SupplierUpdateData } from './';
/**
 *
 * @export
 * @interface SupplierUpdateErrorAllOf
 */
export interface SupplierUpdateErrorAllOf {
    /**
     *
     * @type {SupplierUpdateData}
     * @memberof SupplierUpdateErrorAllOf
     */
    supplier?: SupplierUpdateData;
}
export declare function SupplierUpdateErrorAllOfFromJSON(json: any): SupplierUpdateErrorAllOf;
export declare function SupplierUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierUpdateErrorAllOf;
export declare function SupplierUpdateErrorAllOfToJSON(value?: SupplierUpdateErrorAllOf | null): any;
