/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingCreateData } from './';
/**
 *
 * @export
 * @interface TrackingCreateError
 */
export interface TrackingCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TrackingCreateError
     */
    success: TrackingCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TrackingCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TrackingCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {TrackingCreateData}
     * @memberof TrackingCreateError
     */
    tracking?: TrackingCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingCreateErrorSuccessEnum {
    False = "false"
}
export declare function TrackingCreateErrorFromJSON(json: any): TrackingCreateError;
export declare function TrackingCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingCreateError;
export declare function TrackingCreateErrorToJSON(value?: TrackingCreateError | null): any;
