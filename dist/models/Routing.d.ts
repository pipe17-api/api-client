/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Routing
 */
export interface Routing {
    /**
     * Routing ID
     * @type {string}
     * @memberof Routing
     */
    routingId?: string;
    /**
     * Public Routing
     * @type {boolean}
     * @memberof Routing
     */
    isPublic: boolean;
    /**
     * Description
     * @type {string}
     * @memberof Routing
     */
    description: string;
    /**
     * UUID
     * @type {string}
     * @memberof Routing
     */
    uuid?: string;
    /**
     * Is Routing Filter Enabled
     * @type {boolean}
     * @memberof Routing
     */
    enabled: boolean;
    /**
     * Routing Filter
     * @type {string}
     * @memberof Routing
     */
    filter: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Routing
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Routing
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Routing
     */
    readonly orgKey?: string;
}
export declare function RoutingFromJSON(json: any): Routing;
export declare function RoutingFromJSONTyped(json: any, ignoreDiscriminator: boolean): Routing;
export declare function RoutingToJSON(value?: Routing | null): any;
