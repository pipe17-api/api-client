/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationBase, IntegrationsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface IntegrationsListResponseAllOf
 */
export interface IntegrationsListResponseAllOf {
    /**
     *
     * @type {IntegrationsListFilter}
     * @memberof IntegrationsListResponseAllOf
     */
    filters?: IntegrationsListFilter;
    /**
     *
     * @type {Array<IntegrationBase>}
     * @memberof IntegrationsListResponseAllOf
     */
    integrations?: Array<IntegrationBase>;
    /**
     *
     * @type {Pagination}
     * @memberof IntegrationsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function IntegrationsListResponseAllOfFromJSON(json: any): IntegrationsListResponseAllOf;
export declare function IntegrationsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsListResponseAllOf;
export declare function IntegrationsListResponseAllOfToJSON(value?: IntegrationsListResponseAllOf | null): any;
