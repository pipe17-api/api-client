"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderCreateResultToJSON = exports.OrderCreateResultFromJSONTyped = exports.OrderCreateResultFromJSON = exports.OrderCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderCreateResultStatusEnum;
(function (OrderCreateResultStatusEnum) {
    OrderCreateResultStatusEnum["Submitted"] = "submitted";
    OrderCreateResultStatusEnum["Failed"] = "failed";
})(OrderCreateResultStatusEnum = exports.OrderCreateResultStatusEnum || (exports.OrderCreateResultStatusEnum = {}));
function OrderCreateResultFromJSON(json) {
    return OrderCreateResultFromJSONTyped(json, false);
}
exports.OrderCreateResultFromJSON = OrderCreateResultFromJSON;
function OrderCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'order': !runtime_1.exists(json, 'order') ? undefined : _1.OrderFromJSON(json['order']),
    };
}
exports.OrderCreateResultFromJSONTyped = OrderCreateResultFromJSONTyped;
function OrderCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'order': _1.OrderToJSON(value.order),
    };
}
exports.OrderCreateResultToJSON = OrderCreateResultToJSON;
