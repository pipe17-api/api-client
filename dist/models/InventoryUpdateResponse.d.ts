/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryUpdateResponse
 */
export interface InventoryUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryUpdateResponse
     */
    success: InventoryUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryUpdateResponse
     */
    code: InventoryUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryUpdateResponseFromJSON(json: any): InventoryUpdateResponse;
export declare function InventoryUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryUpdateResponse;
export declare function InventoryUpdateResponseToJSON(value?: InventoryUpdateResponse | null): any;
