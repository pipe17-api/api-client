"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateListResponseAllOfToJSON = exports.IntegrationStateListResponseAllOfFromJSONTyped = exports.IntegrationStateListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationStateListResponseAllOfFromJSON(json) {
    return IntegrationStateListResponseAllOfFromJSONTyped(json, false);
}
exports.IntegrationStateListResponseAllOfFromJSON = IntegrationStateListResponseAllOfFromJSON;
function IntegrationStateListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationStateListFilterFromJSON(json['filters']),
        'integrationStates': !runtime_1.exists(json, 'integrationStates') ? undefined : (json['integrationStates'].map(_1.IntegrationStateFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.IntegrationStateListResponseAllOfFromJSONTyped = IntegrationStateListResponseAllOfFromJSONTyped;
function IntegrationStateListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.IntegrationStateListFilterToJSON(value.filters),
        'integrationStates': value.integrationStates === undefined ? undefined : (value.integrationStates.map(_1.IntegrationStateToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.IntegrationStateListResponseAllOfToJSON = IntegrationStateListResponseAllOfToJSON;
