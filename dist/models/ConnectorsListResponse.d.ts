/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Connector, ConnectorsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ConnectorsListResponse
 */
export interface ConnectorsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ConnectorsListResponse
     */
    success: ConnectorsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorsListResponse
     */
    code: ConnectorsListResponseCodeEnum;
    /**
     *
     * @type {ConnectorsListFilter}
     * @memberof ConnectorsListResponse
     */
    filters?: ConnectorsListFilter;
    /**
     *
     * @type {Array<Connector>}
     * @memberof ConnectorsListResponse
     */
    connectors?: Array<Connector>;
    /**
     *
     * @type {Pagination}
     * @memberof ConnectorsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ConnectorsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ConnectorsListResponseFromJSON(json: any): ConnectorsListResponse;
export declare function ConnectorsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsListResponse;
export declare function ConnectorsListResponseToJSON(value?: ConnectorsListResponse | null): any;
