/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface WebhookUpdateResponse
 */
export interface WebhookUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof WebhookUpdateResponse
     */
    success: WebhookUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhookUpdateResponse
     */
    code: WebhookUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhookUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum WebhookUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function WebhookUpdateResponseFromJSON(json: any): WebhookUpdateResponse;
export declare function WebhookUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookUpdateResponse;
export declare function WebhookUpdateResponseToJSON(value?: WebhookUpdateResponse | null): any;
