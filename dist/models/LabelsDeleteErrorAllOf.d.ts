/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LabelsDeleteFilter } from './';
/**
 *
 * @export
 * @interface LabelsDeleteErrorAllOf
 */
export interface LabelsDeleteErrorAllOf {
    /**
     *
     * @type {LabelsDeleteFilter}
     * @memberof LabelsDeleteErrorAllOf
     */
    filters?: LabelsDeleteFilter;
}
export declare function LabelsDeleteErrorAllOfFromJSON(json: any): LabelsDeleteErrorAllOf;
export declare function LabelsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsDeleteErrorAllOf;
export declare function LabelsDeleteErrorAllOfToJSON(value?: LabelsDeleteErrorAllOf | null): any;
