/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFiltersListFilter } from './';
/**
 *
 * @export
 * @interface ExceptionFilterListErrorAllOf
 */
export interface ExceptionFilterListErrorAllOf {
    /**
     *
     * @type {ExceptionFiltersListFilter}
     * @memberof ExceptionFilterListErrorAllOf
     */
    filters?: ExceptionFiltersListFilter;
}
export declare function ExceptionFilterListErrorAllOfFromJSON(json: any): ExceptionFilterListErrorAllOf;
export declare function ExceptionFilterListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterListErrorAllOf;
export declare function ExceptionFilterListErrorAllOfToJSON(value?: ExceptionFilterListErrorAllOf | null): any;
