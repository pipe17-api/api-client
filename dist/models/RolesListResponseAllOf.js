"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesListResponseAllOfToJSON = exports.RolesListResponseAllOfFromJSONTyped = exports.RolesListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RolesListResponseAllOfFromJSON(json) {
    return RolesListResponseAllOfFromJSONTyped(json, false);
}
exports.RolesListResponseAllOfFromJSON = RolesListResponseAllOfFromJSON;
function RolesListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RolesListFilterFromJSON(json['filters']),
        'roles': !runtime_1.exists(json, 'roles') ? undefined : (json['roles'].map(_1.RoleFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RolesListResponseAllOfFromJSONTyped = RolesListResponseAllOfFromJSONTyped;
function RolesListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RolesListFilterToJSON(value.filters),
        'roles': value.roles === undefined ? undefined : (value.roles.map(_1.RoleToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RolesListResponseAllOfToJSON = RolesListResponseAllOfToJSON;
