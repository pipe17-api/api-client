"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyUpdateResponseToJSON = exports.ApiKeyUpdateResponseFromJSONTyped = exports.ApiKeyUpdateResponseFromJSON = exports.ApiKeyUpdateResponseCodeEnum = exports.ApiKeyUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ApiKeyUpdateResponseSuccessEnum;
(function (ApiKeyUpdateResponseSuccessEnum) {
    ApiKeyUpdateResponseSuccessEnum["True"] = "true";
})(ApiKeyUpdateResponseSuccessEnum = exports.ApiKeyUpdateResponseSuccessEnum || (exports.ApiKeyUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ApiKeyUpdateResponseCodeEnum;
(function (ApiKeyUpdateResponseCodeEnum) {
    ApiKeyUpdateResponseCodeEnum[ApiKeyUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ApiKeyUpdateResponseCodeEnum[ApiKeyUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ApiKeyUpdateResponseCodeEnum[ApiKeyUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ApiKeyUpdateResponseCodeEnum = exports.ApiKeyUpdateResponseCodeEnum || (exports.ApiKeyUpdateResponseCodeEnum = {}));
function ApiKeyUpdateResponseFromJSON(json) {
    return ApiKeyUpdateResponseFromJSONTyped(json, false);
}
exports.ApiKeyUpdateResponseFromJSON = ApiKeyUpdateResponseFromJSON;
function ApiKeyUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ApiKeyUpdateResponseFromJSONTyped = ApiKeyUpdateResponseFromJSONTyped;
function ApiKeyUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ApiKeyUpdateResponseToJSON = ApiKeyUpdateResponseToJSON;
