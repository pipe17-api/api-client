"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationUpdateErrorAllOfToJSON = exports.IntegrationUpdateErrorAllOfFromJSONTyped = exports.IntegrationUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationUpdateErrorAllOfFromJSON(json) {
    return IntegrationUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.IntegrationUpdateErrorAllOfFromJSON = IntegrationUpdateErrorAllOfFromJSON;
function IntegrationUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : _1.IntegrationUpdateDataFromJSON(json['integration']),
    };
}
exports.IntegrationUpdateErrorAllOfFromJSONTyped = IntegrationUpdateErrorAllOfFromJSONTyped;
function IntegrationUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': _1.IntegrationUpdateDataToJSON(value.integration),
    };
}
exports.IntegrationUpdateErrorAllOfToJSON = IntegrationUpdateErrorAllOfToJSON;
