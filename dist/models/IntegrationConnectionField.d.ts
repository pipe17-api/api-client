/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationConnectionField
 */
export interface IntegrationConnectionField {
    /**
     * Connection Option Name
     * @type {string}
     * @memberof IntegrationConnectionField
     */
    path?: string;
    /**
     * Connection Option Value
     * @type {object}
     * @memberof IntegrationConnectionField
     */
    value?: object;
}
export declare function IntegrationConnectionFieldFromJSON(json: any): IntegrationConnectionField;
export declare function IntegrationConnectionFieldFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationConnectionField;
export declare function IntegrationConnectionFieldToJSON(value?: IntegrationConnectionField | null): any;
