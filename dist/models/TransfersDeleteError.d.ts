/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransfersDeleteFilter } from './';
/**
 *
 * @export
 * @interface TransfersDeleteError
 */
export interface TransfersDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TransfersDeleteError
     */
    success: TransfersDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransfersDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TransfersDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TransfersDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {TransfersDeleteFilter}
     * @memberof TransfersDeleteError
     */
    filters?: TransfersDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum TransfersDeleteErrorSuccessEnum {
    False = "false"
}
export declare function TransfersDeleteErrorFromJSON(json: any): TransfersDeleteError;
export declare function TransfersDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersDeleteError;
export declare function TransfersDeleteErrorToJSON(value?: TransfersDeleteError | null): any;
