/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionStatus } from './';
/**
 *
 * @export
 * @interface ExceptionAllOf
 */
export interface ExceptionAllOf {
    /**
     * Exception ID
     * @type {string}
     * @memberof ExceptionAllOf
     */
    exceptionId?: string;
    /**
     *
     * @type {ExceptionStatus}
     * @memberof ExceptionAllOf
     */
    status?: ExceptionStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof ExceptionAllOf
     */
    integration?: string;
}
export declare function ExceptionAllOfFromJSON(json: any): ExceptionAllOf;
export declare function ExceptionAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionAllOf;
export declare function ExceptionAllOfToJSON(value?: ExceptionAllOf | null): any;
