/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 *
 * @export
 * @interface EntityFiltersFilter
 */
export interface EntityFiltersFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof EntityFiltersFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof EntityFiltersFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof EntityFiltersFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof EntityFiltersFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof EntityFiltersFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof EntityFiltersFilter
     */
    count?: number;
    /**
     * Fetch by list of filterId
     * @type {Array<string>}
     * @memberof EntityFiltersFilter
     */
    filterId?: Array<string>;
    /**
     * Fetch by list of entity types
     * @type {Array<EntityName>}
     * @memberof EntityFiltersFilter
     */
    entity?: Array<EntityName>;
    /**
     * Fetch by list of filter names
     * @type {Array<string>}
     * @memberof EntityFiltersFilter
     */
    name?: Array<string>;
}
export declare function EntityFiltersFilterFromJSON(json: any): EntityFiltersFilter;
export declare function EntityFiltersFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFiltersFilter;
export declare function EntityFiltersFilterToJSON(value?: EntityFiltersFilter | null): any;
