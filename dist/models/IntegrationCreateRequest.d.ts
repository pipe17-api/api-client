/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, IntegrationConnection, IntegrationEntity, IntegrationSettings } from './';
/**
 *
 * @export
 * @interface IntegrationCreateRequest
 */
export interface IntegrationCreateRequest {
    /**
     * Integration Name
     * @type {string}
     * @memberof IntegrationCreateRequest
     */
    integrationName: string;
    /**
     * Connector Name (either Name or ID required)
     * @type {string}
     * @memberof IntegrationCreateRequest
     */
    connectorName?: string;
    /**
     * Connector ID (either Name or ID required)
     * @type {string}
     * @memberof IntegrationCreateRequest
     */
    connectorId?: string;
    /**
     *
     * @type {Env}
     * @memberof IntegrationCreateRequest
     */
    environment?: Env;
    /**
     *
     * @type {Array<IntegrationEntity>}
     * @memberof IntegrationCreateRequest
     */
    entities?: Array<IntegrationEntity>;
    /**
     *
     * @type {IntegrationSettings}
     * @memberof IntegrationCreateRequest
     */
    settings?: IntegrationSettings;
    /**
     *
     * @type {IntegrationConnection}
     * @memberof IntegrationCreateRequest
     */
    connection?: IntegrationConnection;
}
export declare function IntegrationCreateRequestFromJSON(json: any): IntegrationCreateRequest;
export declare function IntegrationCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationCreateRequest;
export declare function IntegrationCreateRequestToJSON(value?: IntegrationCreateRequest | null): any;
