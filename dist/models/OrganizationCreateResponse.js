"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationCreateResponseToJSON = exports.OrganizationCreateResponseFromJSONTyped = exports.OrganizationCreateResponseFromJSON = exports.OrganizationCreateResponseCodeEnum = exports.OrganizationCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrganizationCreateResponseSuccessEnum;
(function (OrganizationCreateResponseSuccessEnum) {
    OrganizationCreateResponseSuccessEnum["True"] = "true";
})(OrganizationCreateResponseSuccessEnum = exports.OrganizationCreateResponseSuccessEnum || (exports.OrganizationCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrganizationCreateResponseCodeEnum;
(function (OrganizationCreateResponseCodeEnum) {
    OrganizationCreateResponseCodeEnum[OrganizationCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrganizationCreateResponseCodeEnum[OrganizationCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrganizationCreateResponseCodeEnum[OrganizationCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrganizationCreateResponseCodeEnum = exports.OrganizationCreateResponseCodeEnum || (exports.OrganizationCreateResponseCodeEnum = {}));
function OrganizationCreateResponseFromJSON(json) {
    return OrganizationCreateResponseFromJSONTyped(json, false);
}
exports.OrganizationCreateResponseFromJSON = OrganizationCreateResponseFromJSON;
function OrganizationCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'organization': !runtime_1.exists(json, 'organization') ? undefined : _1.OrganizationFromJSON(json['organization']),
    };
}
exports.OrganizationCreateResponseFromJSONTyped = OrganizationCreateResponseFromJSONTyped;
function OrganizationCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'organization': _1.OrganizationToJSON(value.organization),
    };
}
exports.OrganizationCreateResponseToJSON = OrganizationCreateResponseToJSON;
