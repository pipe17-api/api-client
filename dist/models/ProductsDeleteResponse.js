"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsDeleteResponseToJSON = exports.ProductsDeleteResponseFromJSONTyped = exports.ProductsDeleteResponseFromJSON = exports.ProductsDeleteResponseCodeEnum = exports.ProductsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductsDeleteResponseSuccessEnum;
(function (ProductsDeleteResponseSuccessEnum) {
    ProductsDeleteResponseSuccessEnum["True"] = "true";
})(ProductsDeleteResponseSuccessEnum = exports.ProductsDeleteResponseSuccessEnum || (exports.ProductsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ProductsDeleteResponseCodeEnum;
(function (ProductsDeleteResponseCodeEnum) {
    ProductsDeleteResponseCodeEnum[ProductsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ProductsDeleteResponseCodeEnum[ProductsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ProductsDeleteResponseCodeEnum[ProductsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ProductsDeleteResponseCodeEnum = exports.ProductsDeleteResponseCodeEnum || (exports.ProductsDeleteResponseCodeEnum = {}));
function ProductsDeleteResponseFromJSON(json) {
    return ProductsDeleteResponseFromJSONTyped(json, false);
}
exports.ProductsDeleteResponseFromJSON = ProductsDeleteResponseFromJSON;
function ProductsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ProductsDeleteFilterFromJSON(json['filters']),
        'products': !runtime_1.exists(json, 'products') ? undefined : (json['products'].map(_1.ProductFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ProductsDeleteResponseFromJSONTyped = ProductsDeleteResponseFromJSONTyped;
function ProductsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ProductsDeleteFilterToJSON(value.filters),
        'products': value.products === undefined ? undefined : (value.products.map(_1.ProductToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ProductsDeleteResponseToJSON = ProductsDeleteResponseToJSON;
