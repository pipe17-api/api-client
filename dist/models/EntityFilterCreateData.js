"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterCreateDataToJSON = exports.EntityFilterCreateDataFromJSONTyped = exports.EntityFilterCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntityFilterCreateDataFromJSON(json) {
    return EntityFilterCreateDataFromJSONTyped(json, false);
}
exports.EntityFilterCreateDataFromJSON = EntityFilterCreateDataFromJSON;
function EntityFilterCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': json['name'],
        'query': json['query'],
        'entity': _1.EntityNameFromJSON(json['entity']),
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
    };
}
exports.EntityFilterCreateDataFromJSONTyped = EntityFilterCreateDataFromJSONTyped;
function EntityFilterCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'query': value.query,
        'entity': _1.EntityNameToJSON(value.entity),
        'isPublic': value.isPublic,
        'originId': value.originId,
    };
}
exports.EntityFilterCreateDataToJSON = EntityFilterCreateDataToJSON;
