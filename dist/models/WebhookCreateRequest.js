"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookCreateRequestToJSON = exports.WebhookCreateRequestFromJSONTyped = exports.WebhookCreateRequestFromJSON = void 0;
const _1 = require("./");
function WebhookCreateRequestFromJSON(json) {
    return WebhookCreateRequestFromJSONTyped(json, false);
}
exports.WebhookCreateRequestFromJSON = WebhookCreateRequestFromJSON;
function WebhookCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'url': json['url'],
        'apikey': json['apikey'],
        'topics': (json['topics'].map(_1.WebhookTopicsFromJSON)),
    };
}
exports.WebhookCreateRequestFromJSONTyped = WebhookCreateRequestFromJSONTyped;
function WebhookCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'url': value.url,
        'apikey': value.apikey,
        'topics': (value.topics.map(_1.WebhookTopicsToJSON)),
    };
}
exports.WebhookCreateRequestToJSON = WebhookCreateRequestToJSON;
