/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityFilterFetchError
 */
export interface EntityFilterFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntityFilterFetchError
     */
    success: EntityFilterFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntityFilterFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntityFilterFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterFetchErrorSuccessEnum {
    False = "false"
}
export declare function EntityFilterFetchErrorFromJSON(json: any): EntityFilterFetchError;
export declare function EntityFilterFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterFetchError;
export declare function EntityFilterFetchErrorToJSON(value?: EntityFilterFetchError | null): any;
