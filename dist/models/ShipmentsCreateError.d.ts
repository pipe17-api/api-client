/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentCreateResult } from './';
/**
 *
 * @export
 * @interface ShipmentsCreateError
 */
export interface ShipmentsCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ShipmentsCreateError
     */
    success: ShipmentsCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentsCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ShipmentsCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ShipmentsCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<ShipmentCreateResult>}
     * @memberof ShipmentsCreateError
     */
    shipments?: Array<ShipmentCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentsCreateErrorSuccessEnum {
    False = "false"
}
export declare function ShipmentsCreateErrorFromJSON(json: any): ShipmentsCreateError;
export declare function ShipmentsCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsCreateError;
export declare function ShipmentsCreateErrorToJSON(value?: ShipmentsCreateError | null): any;
