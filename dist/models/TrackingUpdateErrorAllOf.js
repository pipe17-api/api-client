"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingUpdateErrorAllOfToJSON = exports.TrackingUpdateErrorAllOfFromJSONTyped = exports.TrackingUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingUpdateErrorAllOfFromJSON(json) {
    return TrackingUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.TrackingUpdateErrorAllOfFromJSON = TrackingUpdateErrorAllOfFromJSON;
function TrackingUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'tracking': !runtime_1.exists(json, 'tracking') ? undefined : _1.TrackingUpdateDataFromJSON(json['tracking']),
    };
}
exports.TrackingUpdateErrorAllOfFromJSONTyped = TrackingUpdateErrorAllOfFromJSONTyped;
function TrackingUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'tracking': _1.TrackingUpdateDataToJSON(value.tracking),
    };
}
exports.TrackingUpdateErrorAllOfToJSON = TrackingUpdateErrorAllOfToJSON;
