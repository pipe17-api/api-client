/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ShipmentLineItem
 */
export interface ShipmentLineItem {
    /**
     * Item unique Id (within shipment)
     * @type {string}
     * @memberof ShipmentLineItem
     */
    uniqueId: string;
    /**
     * Item SKU
     * @type {string}
     * @memberof ShipmentLineItem
     */
    sku: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof ShipmentLineItem
     */
    quantity: number;
    /**
     * Item Name
     * @type {string}
     * @memberof ShipmentLineItem
     */
    name?: string;
    /**
     * reference to bundle for bundle components
     * @type {string}
     * @memberof ShipmentLineItem
     */
    bundleSKU?: string;
    /**
     * quantity of item in bundle for bundle components
     * @type {number}
     * @memberof ShipmentLineItem
     */
    bundleQuantity?: number;
    /**
     * Item Shipped Quantity
     * @type {number}
     * @memberof ShipmentLineItem
     */
    shippedQuantity?: number;
}
export declare function ShipmentLineItemFromJSON(json: any): ShipmentLineItem;
export declare function ShipmentLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentLineItem;
export declare function ShipmentLineItemToJSON(value?: ShipmentLineItem | null): any;
