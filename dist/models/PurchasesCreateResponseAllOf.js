"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesCreateResponseAllOfToJSON = exports.PurchasesCreateResponseAllOfFromJSONTyped = exports.PurchasesCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchasesCreateResponseAllOfFromJSON(json) {
    return PurchasesCreateResponseAllOfFromJSONTyped(json, false);
}
exports.PurchasesCreateResponseAllOfFromJSON = PurchasesCreateResponseAllOfFromJSON;
function PurchasesCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'purchases': !runtime_1.exists(json, 'purchases') ? undefined : (json['purchases'].map(_1.PurchaseCreateResultFromJSON)),
    };
}
exports.PurchasesCreateResponseAllOfFromJSONTyped = PurchasesCreateResponseAllOfFromJSONTyped;
function PurchasesCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'purchases': value.purchases === undefined ? undefined : (value.purchases.map(_1.PurchaseCreateResultToJSON)),
    };
}
exports.PurchasesCreateResponseAllOfToJSON = PurchasesCreateResponseAllOfToJSON;
