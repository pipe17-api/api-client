/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SuppliersDeleteFilter } from './';
/**
 *
 * @export
 * @interface SuppliersDeleteError
 */
export interface SuppliersDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof SuppliersDeleteError
     */
    success: SuppliersDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SuppliersDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof SuppliersDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof SuppliersDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {SuppliersDeleteFilter}
     * @memberof SuppliersDeleteError
     */
    filters?: SuppliersDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum SuppliersDeleteErrorSuccessEnum {
    False = "false"
}
export declare function SuppliersDeleteErrorFromJSON(json: any): SuppliersDeleteError;
export declare function SuppliersDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersDeleteError;
export declare function SuppliersDeleteErrorToJSON(value?: SuppliersDeleteError | null): any;
