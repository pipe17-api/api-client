"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingToJSON = exports.RoutingFromJSONTyped = exports.RoutingFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingFromJSON(json) {
    return RoutingFromJSONTyped(json, false);
}
exports.RoutingFromJSON = RoutingFromJSON;
function RoutingFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routingId': !runtime_1.exists(json, 'routingId') ? undefined : json['routingId'],
        'isPublic': json['isPublic'],
        'description': json['description'],
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'enabled': json['enabled'],
        'filter': json['filter'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.RoutingFromJSONTyped = RoutingFromJSONTyped;
function RoutingToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routingId': value.routingId,
        'isPublic': value.isPublic,
        'description': value.description,
        'uuid': value.uuid,
        'enabled': value.enabled,
        'filter': value.filter,
    };
}
exports.RoutingToJSON = RoutingToJSON;
