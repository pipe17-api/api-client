"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductBundleItemToJSON = exports.ProductBundleItemFromJSONTyped = exports.ProductBundleItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function ProductBundleItemFromJSON(json) {
    return ProductBundleItemFromJSONTyped(json, false);
}
exports.ProductBundleItemFromJSON = ProductBundleItemFromJSON;
function ProductBundleItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'quantity': !runtime_1.exists(json, 'quantity') ? undefined : json['quantity'],
    };
}
exports.ProductBundleItemFromJSONTyped = ProductBundleItemFromJSONTyped;
function ProductBundleItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'quantity': value.quantity,
    };
}
exports.ProductBundleItemToJSON = ProductBundleItemToJSON;
