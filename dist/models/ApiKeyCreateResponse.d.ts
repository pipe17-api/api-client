/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyCreateResponse
 */
export interface ApiKeyCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ApiKeyCreateResponse
     */
    success: ApiKeyCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyCreateResponse
     */
    code: ApiKeyCreateResponseCodeEnum;
    /**
     * New Api Key
     * @type {string}
     * @memberof ApiKeyCreateResponse
     */
    apikey: string;
    /**
     * New Api Key ID
     * @type {string}
     * @memberof ApiKeyCreateResponse
     */
    apikeyId?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ApiKeyCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ApiKeyCreateResponseFromJSON(json: any): ApiKeyCreateResponse;
export declare function ApiKeyCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyCreateResponse;
export declare function ApiKeyCreateResponseToJSON(value?: ApiKeyCreateResponse | null): any;
