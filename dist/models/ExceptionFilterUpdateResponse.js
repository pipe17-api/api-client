"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterUpdateResponseToJSON = exports.ExceptionFilterUpdateResponseFromJSONTyped = exports.ExceptionFilterUpdateResponseFromJSON = exports.ExceptionFilterUpdateResponseCodeEnum = exports.ExceptionFilterUpdateResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ExceptionFilterUpdateResponseSuccessEnum;
(function (ExceptionFilterUpdateResponseSuccessEnum) {
    ExceptionFilterUpdateResponseSuccessEnum["True"] = "true";
})(ExceptionFilterUpdateResponseSuccessEnum = exports.ExceptionFilterUpdateResponseSuccessEnum || (exports.ExceptionFilterUpdateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionFilterUpdateResponseCodeEnum;
(function (ExceptionFilterUpdateResponseCodeEnum) {
    ExceptionFilterUpdateResponseCodeEnum[ExceptionFilterUpdateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionFilterUpdateResponseCodeEnum[ExceptionFilterUpdateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionFilterUpdateResponseCodeEnum[ExceptionFilterUpdateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionFilterUpdateResponseCodeEnum = exports.ExceptionFilterUpdateResponseCodeEnum || (exports.ExceptionFilterUpdateResponseCodeEnum = {}));
function ExceptionFilterUpdateResponseFromJSON(json) {
    return ExceptionFilterUpdateResponseFromJSONTyped(json, false);
}
exports.ExceptionFilterUpdateResponseFromJSON = ExceptionFilterUpdateResponseFromJSON;
function ExceptionFilterUpdateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ExceptionFilterUpdateResponseFromJSONTyped = ExceptionFilterUpdateResponseFromJSONTyped;
function ExceptionFilterUpdateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ExceptionFilterUpdateResponseToJSON = ExceptionFilterUpdateResponseToJSON;
