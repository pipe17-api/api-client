/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AccountCreateRequestAllOf
 */
export interface AccountCreateRequestAllOf {
    /**
     * Account Owner password
     * @type {string}
     * @memberof AccountCreateRequestAllOf
     */
    password: string;
}
export declare function AccountCreateRequestAllOfFromJSON(json: any): AccountCreateRequestAllOf;
export declare function AccountCreateRequestAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountCreateRequestAllOf;
export declare function AccountCreateRequestAllOfToJSON(value?: AccountCreateRequestAllOf | null): any;
