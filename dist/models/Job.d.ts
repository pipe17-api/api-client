/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobStatus, JobSubType, JobType } from './';
/**
 *
 * @export
 * @interface Job
 */
export interface Job {
    /**
     * Job ID
     * @type {string}
     * @memberof Job
     */
    jobId?: string;
    /**
     * Job progress (0-100)
     * @type {number}
     * @memberof Job
     */
    progress?: number;
    /**
     *
     * @type {JobStatus}
     * @memberof Job
     */
    status?: JobStatus;
    /**
     *
     * @type {JobType}
     * @memberof Job
     */
    type: JobType;
    /**
     *
     * @type {JobSubType}
     * @memberof Job
     */
    subType: JobSubType;
    /**
     * Job type specific parameters
     * @type {object}
     * @memberof Job
     */
    params?: object;
    /**
     * Job result content type
     * @type {string}
     * @memberof Job
     */
    contentType?: string;
    /**
     * Job timeout (in milliseconds)
     * @type {number}
     * @memberof Job
     */
    timeout?: number;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Job
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Job
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Job
     */
    readonly orgKey?: string;
}
export declare function JobFromJSON(json: any): Job;
export declare function JobFromJSONTyped(json: any, ignoreDiscriminator: boolean): Job;
export declare function JobToJSON(value?: Job | null): any;
