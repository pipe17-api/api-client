/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionFilterFetchError
 */
export interface ExceptionFilterFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionFilterFetchError
     */
    success: ExceptionFilterFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionFilterFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionFilterFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterFetchErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionFilterFetchErrorFromJSON(json: any): ExceptionFilterFetchError;
export declare function ExceptionFilterFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterFetchError;
export declare function ExceptionFilterFetchErrorToJSON(value?: ExceptionFilterFetchError | null): any;
