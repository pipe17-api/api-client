"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersCreateResponseAllOfToJSON = exports.OrdersCreateResponseAllOfFromJSONTyped = exports.OrdersCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersCreateResponseAllOfFromJSON(json) {
    return OrdersCreateResponseAllOfFromJSONTyped(json, false);
}
exports.OrdersCreateResponseAllOfFromJSON = OrdersCreateResponseAllOfFromJSON;
function OrdersCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'orders': !runtime_1.exists(json, 'orders') ? undefined : (json['orders'].map(_1.OrderCreateResultFromJSON)),
    };
}
exports.OrdersCreateResponseAllOfFromJSONTyped = OrdersCreateResponseAllOfFromJSONTyped;
function OrdersCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'orders': value.orders === undefined ? undefined : (value.orders.map(_1.OrderCreateResultToJSON)),
    };
}
exports.OrdersCreateResponseAllOfToJSON = OrdersCreateResponseAllOfToJSON;
