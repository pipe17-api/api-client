/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, Methods, Tier } from './';
/**
 *
 * @export
 * @interface ApiKey
 */
export interface ApiKey {
    /**
     * API key Id
     * @type {string}
     * @memberof ApiKey
     */
    apikeyId?: string;
    /**
     * API Key Name
     * @type {string}
     * @memberof ApiKey
     */
    name: string;
    /**
     * API Key Description
     * @type {string}
     * @memberof ApiKey
     */
    description?: string;
    /**
     * Connector ID
     * @type {string}
     * @memberof ApiKey
     */
    connector?: string;
    /**
     * Integration ID
     * @type {string}
     * @memberof ApiKey
     */
    integration?: string;
    /**
     *
     * @type {Tier}
     * @memberof ApiKey
     */
    tier?: Tier;
    /**
     *
     * @type {Env}
     * @memberof ApiKey
     */
    environment?: Env;
    /**
     *
     * @type {Methods}
     * @memberof ApiKey
     */
    methods: Methods;
    /**
     * IP/CIDR Whitelist
     * @type {Array<string>}
     * @memberof ApiKey
     */
    allowedIPs: Array<string>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof ApiKey
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof ApiKey
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof ApiKey
     */
    readonly orgKey?: string;
}
export declare function ApiKeyFromJSON(json: any): ApiKey;
export declare function ApiKeyFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKey;
export declare function ApiKeyToJSON(value?: ApiKey | null): any;
