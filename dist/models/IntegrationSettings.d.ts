/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationSettingsField } from './';
/**
 * Connector specific
 * @export
 * @interface IntegrationSettings
 */
export interface IntegrationSettings {
    /**
     * correspond to ui fields
     * @type {Array<IntegrationSettingsField>}
     * @memberof IntegrationSettings
     */
    fields?: Array<IntegrationSettingsField> | null;
}
export declare function IntegrationSettingsFromJSON(json: any): IntegrationSettings;
export declare function IntegrationSettingsFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationSettings;
export declare function IntegrationSettingsToJSON(value?: IntegrationSettings | null): any;
