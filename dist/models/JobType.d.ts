/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Job type
 * @export
 * @enum {string}
 */
export declare enum JobType {
    Report = "report",
    Import = "import"
}
export declare function JobTypeFromJSON(json: any): JobType;
export declare function JobTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobType;
export declare function JobTypeToJSON(value?: JobType | null): any;
