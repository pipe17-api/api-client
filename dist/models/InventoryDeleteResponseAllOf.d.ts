/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory, InventoryDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryDeleteResponseAllOf
 */
export interface InventoryDeleteResponseAllOf {
    /**
     *
     * @type {InventoryDeleteFilter}
     * @memberof InventoryDeleteResponseAllOf
     */
    filters?: InventoryDeleteFilter;
    /**
     *
     * @type {Array<Inventory>}
     * @memberof InventoryDeleteResponseAllOf
     */
    inventory?: Array<Inventory>;
    /**
     * Number of deleted inventory items
     * @type {number}
     * @memberof InventoryDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function InventoryDeleteResponseAllOfFromJSON(json: any): InventoryDeleteResponseAllOf;
export declare function InventoryDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryDeleteResponseAllOf;
export declare function InventoryDeleteResponseAllOfToJSON(value?: InventoryDeleteResponseAllOf | null): any;
