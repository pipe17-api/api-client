"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountFetchResponseAllOfToJSON = exports.AccountFetchResponseAllOfFromJSONTyped = exports.AccountFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function AccountFetchResponseAllOfFromJSON(json) {
    return AccountFetchResponseAllOfFromJSONTyped(json, false);
}
exports.AccountFetchResponseAllOfFromJSON = AccountFetchResponseAllOfFromJSON;
function AccountFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'account': !runtime_1.exists(json, 'account') ? undefined : _1.AccountFromJSON(json['account']),
    };
}
exports.AccountFetchResponseAllOfFromJSONTyped = AccountFetchResponseAllOfFromJSONTyped;
function AccountFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'account': _1.AccountToJSON(value.account),
    };
}
exports.AccountFetchResponseAllOfToJSON = AccountFetchResponseAllOfToJSON;
