"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryUpdateErrorToJSON = exports.InventoryUpdateErrorFromJSONTyped = exports.InventoryUpdateErrorFromJSON = exports.InventoryUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryUpdateErrorSuccessEnum;
(function (InventoryUpdateErrorSuccessEnum) {
    InventoryUpdateErrorSuccessEnum["False"] = "false";
})(InventoryUpdateErrorSuccessEnum = exports.InventoryUpdateErrorSuccessEnum || (exports.InventoryUpdateErrorSuccessEnum = {}));
function InventoryUpdateErrorFromJSON(json) {
    return InventoryUpdateErrorFromJSONTyped(json, false);
}
exports.InventoryUpdateErrorFromJSON = InventoryUpdateErrorFromJSON;
function InventoryUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : _1.InventoryUpdateDataFromJSON(json['inventory']),
    };
}
exports.InventoryUpdateErrorFromJSONTyped = InventoryUpdateErrorFromJSONTyped;
function InventoryUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'inventory': _1.InventoryUpdateDataToJSON(value.inventory),
    };
}
exports.InventoryUpdateErrorToJSON = InventoryUpdateErrorToJSON;
