/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionType } from './';
/**
 *
 * @export
 * @interface ExceptionFilterCreateData
 */
export interface ExceptionFilterCreateData {
    /**
     *
     * @type {ExceptionType}
     * @memberof ExceptionFilterCreateData
     */
    exceptionType: ExceptionType;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilterCreateData
     */
    enabled?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ExceptionFilterCreateData
     */
    blocking?: boolean;
    /**
     * Public filter indicator
     * @type {boolean}
     * @memberof ExceptionFilterCreateData
     */
    isPublic?: boolean;
    /**
     *
     * @type {object}
     * @memberof ExceptionFilterCreateData
     */
    settings?: object;
    /**
     *
     * @type {string}
     * @memberof ExceptionFilterCreateData
     */
    originId?: string;
}
export declare function ExceptionFilterCreateDataFromJSON(json: any): ExceptionFilterCreateData;
export declare function ExceptionFilterCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterCreateData;
export declare function ExceptionFilterCreateDataToJSON(value?: ExceptionFilterCreateData | null): any;
