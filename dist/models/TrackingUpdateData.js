"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingUpdateDataToJSON = exports.TrackingUpdateDataFromJSONTyped = exports.TrackingUpdateDataFromJSON = exports.TrackingUpdateDataStatusEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var TrackingUpdateDataStatusEnum;
(function (TrackingUpdateDataStatusEnum) {
    TrackingUpdateDataStatusEnum["Shipped"] = "shipped";
    TrackingUpdateDataStatusEnum["Delivered"] = "delivered";
})(TrackingUpdateDataStatusEnum = exports.TrackingUpdateDataStatusEnum || (exports.TrackingUpdateDataStatusEnum = {}));
function TrackingUpdateDataFromJSON(json) {
    return TrackingUpdateDataFromJSONTyped(json, false);
}
exports.TrackingUpdateDataFromJSON = TrackingUpdateDataFromJSON;
function TrackingUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'status': json['status'],
    };
}
exports.TrackingUpdateDataFromJSONTyped = TrackingUpdateDataFromJSONTyped;
function TrackingUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'status': value.status,
    };
}
exports.TrackingUpdateDataToJSON = TrackingUpdateDataToJSON;
