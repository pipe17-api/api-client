/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchemasListFilter } from './';
/**
 *
 * @export
 * @interface EntitySchemaListError
 */
export interface EntitySchemaListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntitySchemaListError
     */
    success: EntitySchemaListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntitySchemaListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntitySchemaListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {EntitySchemasListFilter}
     * @memberof EntitySchemaListError
     */
    filters?: EntitySchemasListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaListErrorSuccessEnum {
    False = "false"
}
export declare function EntitySchemaListErrorFromJSON(json: any): EntitySchemaListError;
export declare function EntitySchemaListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaListError;
export declare function EntitySchemaListErrorToJSON(value?: EntitySchemaListError | null): any;
