/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRuleUpdateData } from './';
/**
 *
 * @export
 * @interface InventoryRuleUpdateErrorAllOf
 */
export interface InventoryRuleUpdateErrorAllOf {
    /**
     *
     * @type {InventoryRuleUpdateData}
     * @memberof InventoryRuleUpdateErrorAllOf
     */
    inventoryrule?: InventoryRuleUpdateData;
}
export declare function InventoryRuleUpdateErrorAllOfFromJSON(json: any): InventoryRuleUpdateErrorAllOf;
export declare function InventoryRuleUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleUpdateErrorAllOf;
export declare function InventoryRuleUpdateErrorAllOfToJSON(value?: InventoryRuleUpdateErrorAllOf | null): any;
