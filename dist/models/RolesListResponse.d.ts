/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Role, RolesListFilter } from './';
/**
 *
 * @export
 * @interface RolesListResponse
 */
export interface RolesListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RolesListResponse
     */
    success: RolesListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RolesListResponse
     */
    code: RolesListResponseCodeEnum;
    /**
     *
     * @type {RolesListFilter}
     * @memberof RolesListResponse
     */
    filters?: RolesListFilter;
    /**
     *
     * @type {Array<Role>}
     * @memberof RolesListResponse
     */
    roles?: Array<Role>;
    /**
     *
     * @type {Pagination}
     * @memberof RolesListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum RolesListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RolesListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RolesListResponseFromJSON(json: any): RolesListResponse;
export declare function RolesListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesListResponse;
export declare function RolesListResponseToJSON(value?: RolesListResponse | null): any;
