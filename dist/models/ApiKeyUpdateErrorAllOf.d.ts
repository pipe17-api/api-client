/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKeyUpdateData } from './';
/**
 *
 * @export
 * @interface ApiKeyUpdateErrorAllOf
 */
export interface ApiKeyUpdateErrorAllOf {
    /**
     *
     * @type {ApiKeyUpdateData}
     * @memberof ApiKeyUpdateErrorAllOf
     */
    apikey?: ApiKeyUpdateData;
}
export declare function ApiKeyUpdateErrorAllOfFromJSON(json: any): ApiKeyUpdateErrorAllOf;
export declare function ApiKeyUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyUpdateErrorAllOf;
export declare function ApiKeyUpdateErrorAllOfToJSON(value?: ApiKeyUpdateErrorAllOf | null): any;
