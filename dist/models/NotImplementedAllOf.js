"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotImplementedAllOfToJSON = exports.NotImplementedAllOfFromJSONTyped = exports.NotImplementedAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function NotImplementedAllOfFromJSON(json) {
    return NotImplementedAllOfFromJSONTyped(json, false);
}
exports.NotImplementedAllOfFromJSON = NotImplementedAllOfFromJSON;
function NotImplementedAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
    };
}
exports.NotImplementedAllOfFromJSONTyped = NotImplementedAllOfFromJSONTyped;
function NotImplementedAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'message': value.message,
    };
}
exports.NotImplementedAllOfToJSON = NotImplementedAllOfToJSON;
