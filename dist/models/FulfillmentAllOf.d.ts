/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentItemExt } from './';
/**
 *
 * @export
 * @interface FulfillmentAllOf
 */
export interface FulfillmentAllOf {
    /**
     * Fulfillment ID
     * @type {string}
     * @memberof FulfillmentAllOf
     */
    fulfillmentId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof FulfillmentAllOf
     */
    integration?: string;
    /**
     * locationId of the shipment
     * @type {string}
     * @memberof FulfillmentAllOf
     */
    locationId?: string;
    /**
     *
     * @type {Array<FulfillmentItemExt>}
     * @memberof FulfillmentAllOf
     */
    lineItems?: Array<FulfillmentItemExt>;
}
export declare function FulfillmentAllOfFromJSON(json: any): FulfillmentAllOf;
export declare function FulfillmentAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentAllOf;
export declare function FulfillmentAllOfToJSON(value?: FulfillmentAllOf | null): any;
