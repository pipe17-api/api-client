/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKeyValue } from './';
/**
 *
 * @export
 * @interface ApiKeyValueFetchResponseAllOf
 */
export interface ApiKeyValueFetchResponseAllOf {
    /**
     *
     * @type {ApiKeyValue}
     * @memberof ApiKeyValueFetchResponseAllOf
     */
    apikey?: ApiKeyValue;
}
export declare function ApiKeyValueFetchResponseAllOfFromJSON(json: any): ApiKeyValueFetchResponseAllOf;
export declare function ApiKeyValueFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyValueFetchResponseAllOf;
export declare function ApiKeyValueFetchResponseAllOfToJSON(value?: ApiKeyValueFetchResponseAllOf | null): any;
