/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConverterResponseAllOfResult } from './';
/**
 *
 * @export
 * @interface ConverterResponse
 */
export interface ConverterResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ConverterResponse
     */
    success: ConverterResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConverterResponse
     */
    code: ConverterResponseCodeEnum;
    /**
     *
     * @type {string}
     * @memberof ConverterResponse
     */
    message?: string;
    /**
     *
     * @type {ConverterResponseAllOfResult}
     * @memberof ConverterResponse
     */
    result?: ConverterResponseAllOfResult;
    /**
     *
     * @type {object}
     * @memberof ConverterResponse
     */
    info?: object;
}
/**
* @export
* @enum {string}
*/
export declare enum ConverterResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ConverterResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ConverterResponseFromJSON(json: any): ConverterResponse;
export declare function ConverterResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConverterResponse;
export declare function ConverterResponseToJSON(value?: ConverterResponse | null): any;
