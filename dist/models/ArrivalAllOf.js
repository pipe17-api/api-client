"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalAllOfToJSON = exports.ArrivalAllOfFromJSONTyped = exports.ArrivalAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalAllOfFromJSON(json) {
    return ArrivalAllOfFromJSONTyped(json, false);
}
exports.ArrivalAllOfFromJSON = ArrivalAllOfFromJSON;
function ArrivalAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrivalId': !runtime_1.exists(json, 'arrivalId') ? undefined : json['arrivalId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ArrivalStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.ArrivalAllOfFromJSONTyped = ArrivalAllOfFromJSONTyped;
function ArrivalAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrivalId': value.arrivalId,
        'status': _1.ArrivalStatusToJSON(value.status),
        'integration': value.integration,
    };
}
exports.ArrivalAllOfToJSON = ArrivalAllOfToJSON;
