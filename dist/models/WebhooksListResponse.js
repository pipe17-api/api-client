"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhooksListResponseToJSON = exports.WebhooksListResponseFromJSONTyped = exports.WebhooksListResponseFromJSON = exports.WebhooksListResponseCodeEnum = exports.WebhooksListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var WebhooksListResponseSuccessEnum;
(function (WebhooksListResponseSuccessEnum) {
    WebhooksListResponseSuccessEnum["True"] = "true";
})(WebhooksListResponseSuccessEnum = exports.WebhooksListResponseSuccessEnum || (exports.WebhooksListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var WebhooksListResponseCodeEnum;
(function (WebhooksListResponseCodeEnum) {
    WebhooksListResponseCodeEnum[WebhooksListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    WebhooksListResponseCodeEnum[WebhooksListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    WebhooksListResponseCodeEnum[WebhooksListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(WebhooksListResponseCodeEnum = exports.WebhooksListResponseCodeEnum || (exports.WebhooksListResponseCodeEnum = {}));
function WebhooksListResponseFromJSON(json) {
    return WebhooksListResponseFromJSONTyped(json, false);
}
exports.WebhooksListResponseFromJSON = WebhooksListResponseFromJSON;
function WebhooksListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.WebhooksListFilterFromJSON(json['filters']),
        'webhooks': !runtime_1.exists(json, 'webhooks') ? undefined : (json['webhooks'].map(_1.WebhookFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.WebhooksListResponseFromJSONTyped = WebhooksListResponseFromJSONTyped;
function WebhooksListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.WebhooksListFilterToJSON(value.filters),
        'webhooks': value.webhooks === undefined ? undefined : (value.webhooks.map(_1.WebhookToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.WebhooksListResponseToJSON = WebhooksListResponseToJSON;
