"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationUpdateErrorAllOfToJSON = exports.LocationUpdateErrorAllOfFromJSONTyped = exports.LocationUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationUpdateErrorAllOfFromJSON(json) {
    return LocationUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.LocationUpdateErrorAllOfFromJSON = LocationUpdateErrorAllOfFromJSON;
function LocationUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationUpdateDataFromJSON(json['location']),
    };
}
exports.LocationUpdateErrorAllOfFromJSONTyped = LocationUpdateErrorAllOfFromJSONTyped;
function LocationUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'location': _1.LocationUpdateDataToJSON(value.location),
    };
}
exports.LocationUpdateErrorAllOfToJSON = LocationUpdateErrorAllOfToJSON;
