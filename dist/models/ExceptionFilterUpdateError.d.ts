/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionFilterUpdateError
 */
export interface ExceptionFilterUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionFilterUpdateError
     */
    success: ExceptionFilterUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionFilterUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionFilterUpdateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionFilterUpdateErrorFromJSON(json: any): ExceptionFilterUpdateError;
export declare function ExceptionFilterUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterUpdateError;
export declare function ExceptionFilterUpdateErrorToJSON(value?: ExceptionFilterUpdateError | null): any;
