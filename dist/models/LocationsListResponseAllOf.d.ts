/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location, LocationsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface LocationsListResponseAllOf
 */
export interface LocationsListResponseAllOf {
    /**
     *
     * @type {LocationsListFilter}
     * @memberof LocationsListResponseAllOf
     */
    filters?: LocationsListFilter;
    /**
     *
     * @type {Array<Location>}
     * @memberof LocationsListResponseAllOf
     */
    locations?: Array<Location>;
    /**
     *
     * @type {Pagination}
     * @memberof LocationsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function LocationsListResponseAllOfFromJSON(json: any): LocationsListResponseAllOf;
export declare function LocationsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsListResponseAllOf;
export declare function LocationsListResponseAllOfToJSON(value?: LocationsListResponseAllOf | null): any;
