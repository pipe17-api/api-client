/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRulesListFilter } from './';
/**
 *
 * @export
 * @interface InventoryRulesListError
 */
export interface InventoryRulesListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryRulesListError
     */
    success: InventoryRulesListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRulesListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryRulesListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryRulesListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryRulesListFilter}
     * @memberof InventoryRulesListError
     */
    filters?: InventoryRulesListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRulesListErrorSuccessEnum {
    False = "false"
}
export declare function InventoryRulesListErrorFromJSON(json: any): InventoryRulesListError;
export declare function InventoryRulesListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesListError;
export declare function InventoryRulesListErrorToJSON(value?: InventoryRulesListError | null): any;
