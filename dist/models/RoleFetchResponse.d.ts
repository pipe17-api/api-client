/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Role } from './';
/**
 *
 * @export
 * @interface RoleFetchResponse
 */
export interface RoleFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoleFetchResponse
     */
    success: RoleFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoleFetchResponse
     */
    code: RoleFetchResponseCodeEnum;
    /**
     *
     * @type {Role}
     * @memberof RoleFetchResponse
     */
    role?: Role;
}
/**
* @export
* @enum {string}
*/
export declare enum RoleFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoleFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoleFetchResponseFromJSON(json: any): RoleFetchResponse;
export declare function RoleFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleFetchResponse;
export declare function RoleFetchResponseToJSON(value?: RoleFetchResponse | null): any;
