/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Supplier } from './';
/**
 *
 * @export
 * @interface SupplierFetchResponse
 */
export interface SupplierFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof SupplierFetchResponse
     */
    success: SupplierFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SupplierFetchResponse
     */
    code: SupplierFetchResponseCodeEnum;
    /**
     *
     * @type {Supplier}
     * @memberof SupplierFetchResponse
     */
    supplier?: Supplier;
}
/**
* @export
* @enum {string}
*/
export declare enum SupplierFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum SupplierFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function SupplierFetchResponseFromJSON(json: any): SupplierFetchResponse;
export declare function SupplierFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierFetchResponse;
export declare function SupplierFetchResponseToJSON(value?: SupplierFetchResponse | null): any;
