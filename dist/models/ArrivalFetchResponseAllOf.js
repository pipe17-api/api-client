"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalFetchResponseAllOfToJSON = exports.ArrivalFetchResponseAllOfFromJSONTyped = exports.ArrivalFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ArrivalFetchResponseAllOfFromJSON(json) {
    return ArrivalFetchResponseAllOfFromJSONTyped(json, false);
}
exports.ArrivalFetchResponseAllOfFromJSON = ArrivalFetchResponseAllOfFromJSON;
function ArrivalFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'arrival': !runtime_1.exists(json, 'arrival') ? undefined : _1.ArrivalFromJSON(json['arrival']),
    };
}
exports.ArrivalFetchResponseAllOfFromJSONTyped = ArrivalFetchResponseAllOfFromJSONTyped;
function ArrivalFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'arrival': _1.ArrivalToJSON(value.arrival),
    };
}
exports.ArrivalFetchResponseAllOfToJSON = ArrivalFetchResponseAllOfToJSON;
