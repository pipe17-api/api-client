/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnUpdateData } from './';
/**
 *
 * @export
 * @interface ReturnUpdateError
 */
export interface ReturnUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReturnUpdateError
     */
    success: ReturnUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReturnUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReturnUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ReturnUpdateData}
     * @memberof ReturnUpdateError
     */
    _return?: ReturnUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ReturnUpdateErrorFromJSON(json: any): ReturnUpdateError;
export declare function ReturnUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnUpdateError;
export declare function ReturnUpdateErrorToJSON(value?: ReturnUpdateError | null): any;
