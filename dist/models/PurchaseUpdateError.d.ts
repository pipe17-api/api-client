/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseUpdateData } from './';
/**
 *
 * @export
 * @interface PurchaseUpdateError
 */
export interface PurchaseUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof PurchaseUpdateError
     */
    success: PurchaseUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchaseUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof PurchaseUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof PurchaseUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {PurchaseUpdateData}
     * @memberof PurchaseUpdateError
     */
    purchase?: PurchaseUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchaseUpdateErrorSuccessEnum {
    False = "false"
}
export declare function PurchaseUpdateErrorFromJSON(json: any): PurchaseUpdateError;
export declare function PurchaseUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseUpdateError;
export declare function PurchaseUpdateErrorToJSON(value?: PurchaseUpdateError | null): any;
