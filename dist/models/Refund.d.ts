/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundStatus, RefundType } from './';
/**
 *
 * @export
 * @interface Refund
 */
export interface Refund {
    /**
     * Refund ID
     * @type {string}
     * @memberof Refund
     */
    refundId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Refund
     */
    integration?: string;
    /**
     * Return ID
     * @type {string}
     * @memberof Refund
     */
    returnId: string;
    /**
     * Refund ID on external system
     * @type {string}
     * @memberof Refund
     */
    extRefundId?: string;
    /**
     * Order ID on external system
     * @type {string}
     * @memberof Refund
     */
    extOrderId?: string;
    /**
     *
     * @type {RefundType}
     * @memberof Refund
     */
    type: RefundType;
    /**
     *
     * @type {RefundStatus}
     * @memberof Refund
     */
    status?: RefundStatus;
    /**
     * Refund currency
     * @type {string}
     * @memberof Refund
     */
    currency?: string;
    /**
     * Refund amount
     * @type {number}
     * @memberof Refund
     */
    amount?: number;
    /**
     * Credit issued to customer
     * @type {number}
     * @memberof Refund
     */
    creditIssued?: number;
    /**
     * Credit spent by customer
     * @type {number}
     * @memberof Refund
     */
    creditSpent?: number;
    /**
     * Merchant invoice amount
     * @type {number}
     * @memberof Refund
     */
    merchantInvoiceAmount?: number;
    /**
     * Customer invoice amount
     * @type {number}
     * @memberof Refund
     */
    customerInvoiceAmount?: number;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Refund
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Refund
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Refund
     */
    readonly orgKey?: string;
}
export declare function RefundFromJSON(json: any): Refund;
export declare function RefundFromJSONTyped(json: any, ignoreDiscriminator: boolean): Refund;
export declare function RefundToJSON(value?: Refund | null): any;
