/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Purchase Status
 * @export
 * @enum {string}
 */
export declare enum PurchaseStatus {
    Draft = "draft",
    New = "new",
    OnHold = "onHold",
    ToBeValidated = "toBeValidated",
    ReviewRequired = "reviewRequired",
    ReadyForFulfillment = "readyForFulfillment",
    SentToFulfillment = "sentToFulfillment",
    PartialFulfillment = "partialFulfillment",
    Fulfilled = "fulfilled",
    InTransit = "inTransit",
    PartialReceived = "partialReceived",
    Received = "received",
    Canceled = "canceled",
    Returned = "returned",
    Refunded = "refunded"
}
export declare function PurchaseStatusFromJSON(json: any): PurchaseStatus;
export declare function PurchaseStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseStatus;
export declare function PurchaseStatusToJSON(value?: PurchaseStatus | null): any;
