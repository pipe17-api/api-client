/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnAllOf
 */
export interface ReturnAllOf {
    /**
     * Return ID
     * @type {string}
     * @memberof ReturnAllOf
     */
    returnId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof ReturnAllOf
     */
    integration?: string;
    /**
     *
     * @type {ReturnStatus}
     * @memberof ReturnAllOf
     */
    status?: ReturnStatus;
}
export declare function ReturnAllOfFromJSON(json: any): ReturnAllOf;
export declare function ReturnAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnAllOf;
export declare function ReturnAllOfToJSON(value?: ReturnAllOf | null): any;
