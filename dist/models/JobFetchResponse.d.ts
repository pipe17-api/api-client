/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job } from './';
/**
 *
 * @export
 * @interface JobFetchResponse
 */
export interface JobFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof JobFetchResponse
     */
    success: JobFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof JobFetchResponse
     */
    code: JobFetchResponseCodeEnum;
    /**
     *
     * @type {Job}
     * @memberof JobFetchResponse
     */
    job?: Job;
}
/**
* @export
* @enum {string}
*/
export declare enum JobFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum JobFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function JobFetchResponseFromJSON(json: any): JobFetchResponse;
export declare function JobFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobFetchResponse;
export declare function JobFetchResponseToJSON(value?: JobFetchResponse | null): any;
