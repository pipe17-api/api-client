/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, IntegrationConnection, IntegrationEntity, IntegrationSettings, IntegrationStatus } from './';
/**
 *
 * @export
 * @interface IntegrationBase
 */
export interface IntegrationBase {
    /**
     * Integration version
     * @type {string}
     * @memberof IntegrationBase
     */
    version?: string;
    /**
     * Integration Id
     * @type {string}
     * @memberof IntegrationBase
     */
    integrationId?: string;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationBase
     */
    errorText?: string;
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationBase
     */
    status?: IntegrationStatus;
    /**
     * Integration Name
     * @type {string}
     * @memberof IntegrationBase
     */
    integrationName: string;
    /**
     * Connector Name (either Name or ID required)
     * @type {string}
     * @memberof IntegrationBase
     */
    connectorName?: string;
    /**
     * Connector ID (either Name or ID required)
     * @type {string}
     * @memberof IntegrationBase
     */
    connectorId?: string;
    /**
     *
     * @type {Env}
     * @memberof IntegrationBase
     */
    environment?: Env;
    /**
     *
     * @type {Array<IntegrationEntity>}
     * @memberof IntegrationBase
     */
    entities?: Array<IntegrationEntity>;
    /**
     *
     * @type {IntegrationSettings}
     * @memberof IntegrationBase
     */
    settings?: IntegrationSettings;
    /**
     *
     * @type {IntegrationConnection}
     * @memberof IntegrationBase
     */
    connection?: IntegrationConnection;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof IntegrationBase
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof IntegrationBase
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof IntegrationBase
     */
    readonly orgKey?: string;
}
export declare function IntegrationBaseFromJSON(json: any): IntegrationBase;
export declare function IntegrationBaseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationBase;
export declare function IntegrationBaseToJSON(value?: IntegrationBase | null): any;
