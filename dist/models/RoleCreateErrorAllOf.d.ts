/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleCreateData } from './';
/**
 *
 * @export
 * @interface RoleCreateErrorAllOf
 */
export interface RoleCreateErrorAllOf {
    /**
     *
     * @type {RoleCreateData}
     * @memberof RoleCreateErrorAllOf
     */
    role?: RoleCreateData;
}
export declare function RoleCreateErrorAllOfFromJSON(json: any): RoleCreateErrorAllOf;
export declare function RoleCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleCreateErrorAllOf;
export declare function RoleCreateErrorAllOfToJSON(value?: RoleCreateErrorAllOf | null): any;
