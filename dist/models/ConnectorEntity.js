"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorEntityToJSON = exports.ConnectorEntityFromJSONTyped = exports.ConnectorEntityFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConnectorEntityFromJSON(json) {
    return ConnectorEntityFromJSONTyped(json, false);
}
exports.ConnectorEntityFromJSON = ConnectorEntityFromJSON;
function ConnectorEntityFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': _1.EntityNameFromJSON(json['name']),
        'direction': _1.DirectionFromJSON(json['direction']),
        'mappingUuid': !runtime_1.exists(json, 'mappingUuid') ? undefined : json['mappingUuid'],
        'routingUuid': !runtime_1.exists(json, 'routingUuid') ? undefined : json['routingUuid'],
        'restrictions': !runtime_1.exists(json, 'restrictions') ? undefined : (json['restrictions'].map(_1.EntityRestrictionsItemFromJSON)),
    };
}
exports.ConnectorEntityFromJSONTyped = ConnectorEntityFromJSONTyped;
function ConnectorEntityToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': _1.EntityNameToJSON(value.name),
        'direction': _1.DirectionToJSON(value.direction),
        'mappingUuid': value.mappingUuid,
        'routingUuid': value.routingUuid,
        'restrictions': value.restrictions === undefined ? undefined : (value.restrictions.map(_1.EntityRestrictionsItemToJSON)),
    };
}
exports.ConnectorEntityToJSON = ConnectorEntityToJSON;
