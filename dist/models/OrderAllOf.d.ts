/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderStatus } from './';
/**
 *
 * @export
 * @interface OrderAllOf
 */
export interface OrderAllOf {
    /**
     * Auto Generated
     * @type {string}
     * @memberof OrderAllOf
     */
    orderId?: string;
    /**
     *
     * @type {OrderStatus}
     * @memberof OrderAllOf
     */
    status?: OrderStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof OrderAllOf
     */
    integration?: string;
}
export declare function OrderAllOfFromJSON(json: any): OrderAllOf;
export declare function OrderAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderAllOf;
export declare function OrderAllOfToJSON(value?: OrderAllOf | null): any;
