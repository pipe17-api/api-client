/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorFetchError
 */
export interface ConnectorFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ConnectorFetchError
     */
    success: ConnectorFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ConnectorFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ConnectorFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorFetchErrorSuccessEnum {
    False = "false"
}
export declare function ConnectorFetchErrorFromJSON(json: any): ConnectorFetchError;
export declare function ConnectorFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorFetchError;
export declare function ConnectorFetchErrorToJSON(value?: ConnectorFetchError | null): any;
