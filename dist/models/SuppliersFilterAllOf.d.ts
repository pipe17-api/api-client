/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Suppliers Filter
 * @export
 * @interface SuppliersFilterAllOf
 */
export interface SuppliersFilterAllOf {
    /**
     * Suppliers by list of supplierId
     * @type {Array<string>}
     * @memberof SuppliersFilterAllOf
     */
    supplierId?: Array<string>;
    /**
     * Soft deleted suppliers
     * @type {boolean}
     * @memberof SuppliersFilterAllOf
     */
    deleted?: boolean;
}
export declare function SuppliersFilterAllOfFromJSON(json: any): SuppliersFilterAllOf;
export declare function SuppliersFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersFilterAllOf;
export declare function SuppliersFilterAllOfToJSON(value?: SuppliersFilterAllOf | null): any;
