"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductPublishedItemToJSON = exports.ProductPublishedItemFromJSONTyped = exports.ProductPublishedItemFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductPublishedItemFromJSON(json) {
    return ProductPublishedItemFromJSONTyped(json, false);
}
exports.ProductPublishedItemFromJSON = ProductPublishedItemFromJSON;
function ProductPublishedItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'integrationId': !runtime_1.exists(json, 'integrationId') ? undefined : json['integrationId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ProductPublishedItemStatusFromJSON(json['status']),
        'bufferInventory': !runtime_1.exists(json, 'bufferInventory') ? undefined : json['bufferInventory'],
    };
}
exports.ProductPublishedItemFromJSONTyped = ProductPublishedItemFromJSONTyped;
function ProductPublishedItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': value.name,
        'integrationId': value.integrationId,
        'status': _1.ProductPublishedItemStatusToJSON(value.status),
        'bufferInventory': value.bufferInventory,
    };
}
exports.ProductPublishedItemToJSON = ProductPublishedItemToJSON;
