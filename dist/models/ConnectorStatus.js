"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorStatusToJSON = exports.ConnectorStatusFromJSONTyped = exports.ConnectorStatusFromJSON = exports.ConnectorStatus = void 0;
/**
 * Connector Status
 * @export
 * @enum {string}
 */
var ConnectorStatus;
(function (ConnectorStatus) {
    ConnectorStatus["Created"] = "created";
    ConnectorStatus["Active"] = "active";
    ConnectorStatus["Inactive"] = "inactive";
    ConnectorStatus["Deleted"] = "deleted";
})(ConnectorStatus = exports.ConnectorStatus || (exports.ConnectorStatus = {}));
function ConnectorStatusFromJSON(json) {
    return ConnectorStatusFromJSONTyped(json, false);
}
exports.ConnectorStatusFromJSON = ConnectorStatusFromJSON;
function ConnectorStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ConnectorStatusFromJSONTyped = ConnectorStatusFromJSONTyped;
function ConnectorStatusToJSON(value) {
    return value;
}
exports.ConnectorStatusToJSON = ConnectorStatusToJSON;
