/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleStatus } from './';
/**
 *
 * @export
 * @interface RolesFilter
 */
export interface RolesFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RolesFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RolesFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RolesFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RolesFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RolesFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RolesFilter
     */
    count?: number;
    /**
     * Roles by list of roleId
     * @type {Array<string>}
     * @memberof RolesFilter
     */
    roleId?: Array<string>;
    /**
     * Roles whose name contains this string
     * @type {string}
     * @memberof RolesFilter
     */
    name?: string;
    /**
     * Roles by list of statuses
     * @type {Array<RoleStatus>}
     * @memberof RolesFilter
     */
    status?: Array<RoleStatus>;
}
export declare function RolesFilterFromJSON(json: any): RolesFilter;
export declare function RolesFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesFilter;
export declare function RolesFilterToJSON(value?: RolesFilter | null): any;
