/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Address
 */
export interface Address {
    /**
     * First Name
     * @type {string}
     * @memberof Address
     */
    firstName?: string;
    /**
     * Last Name
     * @type {string}
     * @memberof Address
     */
    lastName?: string;
    /**
     * Company
     * @type {string}
     * @memberof Address
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof Address
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof Address
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof Address
     */
    city?: string;
    /**
     * If within the US, required 2 letter State Code
     * @type {string}
     * @memberof Address
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof Address
     */
    zipCodeOrPostalCode?: string;
    /**
     * ISO 3 letter code only
     * @type {string}
     * @memberof Address
     */
    country?: string;
    /**
     * email
     * @type {string}
     * @memberof Address
     */
    email?: string;
    /**
     * Phone
     * @type {string}
     * @memberof Address
     */
    phone?: string;
}
export declare function AddressFromJSON(json: any): Address;
export declare function AddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): Address;
export declare function AddressToJSON(value?: Address | null): any;
