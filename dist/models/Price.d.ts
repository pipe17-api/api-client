/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Price
 */
export interface Price {
    /**
     * Price Name
     * @type {string}
     * @memberof Price
     */
    name?: string;
    /**
     * Price Amount
     * @type {number}
     * @memberof Price
     */
    value?: number;
    /**
     * ISO Currency
     * @type {string}
     * @memberof Price
     */
    currency?: string;
}
export declare function PriceFromJSON(json: any): Price;
export declare function PriceFromJSONTyped(json: any, ignoreDiscriminator: boolean): Price;
export declare function PriceToJSON(value?: Price | null): any;
