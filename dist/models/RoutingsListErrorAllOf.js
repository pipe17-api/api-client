"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsListErrorAllOfToJSON = exports.RoutingsListErrorAllOfFromJSONTyped = exports.RoutingsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoutingsListErrorAllOfFromJSON(json) {
    return RoutingsListErrorAllOfFromJSONTyped(json, false);
}
exports.RoutingsListErrorAllOfFromJSON = RoutingsListErrorAllOfFromJSON;
function RoutingsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RoutingsListFilterFromJSON(json['filters']),
    };
}
exports.RoutingsListErrorAllOfFromJSONTyped = RoutingsListErrorAllOfFromJSONTyped;
function RoutingsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RoutingsListFilterToJSON(value.filters),
    };
}
exports.RoutingsListErrorAllOfToJSON = RoutingsListErrorAllOfToJSON;
