"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesListResponseAllOfToJSON = exports.PurchasesListResponseAllOfFromJSONTyped = exports.PurchasesListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchasesListResponseAllOfFromJSON(json) {
    return PurchasesListResponseAllOfFromJSONTyped(json, false);
}
exports.PurchasesListResponseAllOfFromJSON = PurchasesListResponseAllOfFromJSON;
function PurchasesListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesListFilterFromJSON(json['filters']),
        'purchases': (json['purchases'].map(_1.PurchaseFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.PurchasesListResponseAllOfFromJSONTyped = PurchasesListResponseAllOfFromJSONTyped;
function PurchasesListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.PurchasesListFilterToJSON(value.filters),
        'purchases': (value.purchases.map(_1.PurchaseToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.PurchasesListResponseAllOfToJSON = PurchasesListResponseAllOfToJSON;
