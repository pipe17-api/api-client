/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnCreateData } from './';
/**
 *
 * @export
 * @interface ReturnCreateErrorAllOf
 */
export interface ReturnCreateErrorAllOf {
    /**
     *
     * @type {ReturnCreateData}
     * @memberof ReturnCreateErrorAllOf
     */
    _return?: ReturnCreateData;
}
export declare function ReturnCreateErrorAllOfFromJSON(json: any): ReturnCreateErrorAllOf;
export declare function ReturnCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnCreateErrorAllOf;
export declare function ReturnCreateErrorAllOfToJSON(value?: ReturnCreateErrorAllOf | null): any;
