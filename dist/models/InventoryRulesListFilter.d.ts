/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRulesListFilter
 */
export interface InventoryRulesListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof InventoryRulesListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof InventoryRulesListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof InventoryRulesListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof InventoryRulesListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof InventoryRulesListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof InventoryRulesListFilter
     */
    count?: number;
    /**
     * Rules by list of ruleId
     * @type {Array<string>}
     * @memberof InventoryRulesListFilter
     */
    ruleId?: Array<string>;
    /**
     * Rules by list of integration ids
     * @type {Array<string>}
     * @memberof InventoryRulesListFilter
     */
    integration?: Array<string>;
    /**
     * Soft deleted rules
     * @type {boolean}
     * @memberof InventoryRulesListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof InventoryRulesListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof InventoryRulesListFilter
     */
    keys?: string;
}
export declare function InventoryRulesListFilterFromJSON(json: any): InventoryRulesListFilter;
export declare function InventoryRulesListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesListFilter;
export declare function InventoryRulesListFilterToJSON(value?: InventoryRulesListFilter | null): any;
