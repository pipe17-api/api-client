"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationFetchResponseToJSON = exports.OrganizationFetchResponseFromJSONTyped = exports.OrganizationFetchResponseFromJSON = exports.OrganizationFetchResponseCodeEnum = exports.OrganizationFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrganizationFetchResponseSuccessEnum;
(function (OrganizationFetchResponseSuccessEnum) {
    OrganizationFetchResponseSuccessEnum["True"] = "true";
})(OrganizationFetchResponseSuccessEnum = exports.OrganizationFetchResponseSuccessEnum || (exports.OrganizationFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrganizationFetchResponseCodeEnum;
(function (OrganizationFetchResponseCodeEnum) {
    OrganizationFetchResponseCodeEnum[OrganizationFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrganizationFetchResponseCodeEnum[OrganizationFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrganizationFetchResponseCodeEnum[OrganizationFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrganizationFetchResponseCodeEnum = exports.OrganizationFetchResponseCodeEnum || (exports.OrganizationFetchResponseCodeEnum = {}));
function OrganizationFetchResponseFromJSON(json) {
    return OrganizationFetchResponseFromJSONTyped(json, false);
}
exports.OrganizationFetchResponseFromJSON = OrganizationFetchResponseFromJSON;
function OrganizationFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'organization': !runtime_1.exists(json, 'organization') ? undefined : _1.OrganizationFromJSON(json['organization']),
    };
}
exports.OrganizationFetchResponseFromJSONTyped = OrganizationFetchResponseFromJSONTyped;
function OrganizationFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'organization': _1.OrganizationToJSON(value.organization),
    };
}
exports.OrganizationFetchResponseToJSON = OrganizationFetchResponseToJSON;
