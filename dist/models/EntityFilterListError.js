"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterListErrorToJSON = exports.EntityFilterListErrorFromJSONTyped = exports.EntityFilterListErrorFromJSON = exports.EntityFilterListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntityFilterListErrorSuccessEnum;
(function (EntityFilterListErrorSuccessEnum) {
    EntityFilterListErrorSuccessEnum["False"] = "false";
})(EntityFilterListErrorSuccessEnum = exports.EntityFilterListErrorSuccessEnum || (exports.EntityFilterListErrorSuccessEnum = {}));
function EntityFilterListErrorFromJSON(json) {
    return EntityFilterListErrorFromJSONTyped(json, false);
}
exports.EntityFilterListErrorFromJSON = EntityFilterListErrorFromJSON;
function EntityFilterListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntityFiltersListFilterFromJSON(json['filters']),
    };
}
exports.EntityFilterListErrorFromJSONTyped = EntityFilterListErrorFromJSONTyped;
function EntityFilterListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.EntityFiltersListFilterToJSON(value.filters),
    };
}
exports.EntityFilterListErrorToJSON = EntityFilterListErrorToJSON;
