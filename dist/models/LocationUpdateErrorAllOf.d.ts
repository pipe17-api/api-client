/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationUpdateData } from './';
/**
 *
 * @export
 * @interface LocationUpdateErrorAllOf
 */
export interface LocationUpdateErrorAllOf {
    /**
     *
     * @type {LocationUpdateData}
     * @memberof LocationUpdateErrorAllOf
     */
    location?: LocationUpdateData;
}
export declare function LocationUpdateErrorAllOfFromJSON(json: any): LocationUpdateErrorAllOf;
export declare function LocationUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationUpdateErrorAllOf;
export declare function LocationUpdateErrorAllOfToJSON(value?: LocationUpdateErrorAllOf | null): any;
