/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRulesDeleteFilter
 */
export interface InventoryRulesDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof InventoryRulesDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof InventoryRulesDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof InventoryRulesDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof InventoryRulesDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof InventoryRulesDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof InventoryRulesDeleteFilter
     */
    count?: number;
    /**
     * Rules by list of ruleId
     * @type {Array<string>}
     * @memberof InventoryRulesDeleteFilter
     */
    ruleId?: Array<string>;
    /**
     * Rules by list of integration ids
     * @type {Array<string>}
     * @memberof InventoryRulesDeleteFilter
     */
    integration?: Array<string>;
    /**
     * Soft deleted rules
     * @type {boolean}
     * @memberof InventoryRulesDeleteFilter
     */
    deleted?: boolean;
}
export declare function InventoryRulesDeleteFilterFromJSON(json: any): InventoryRulesDeleteFilter;
export declare function InventoryRulesDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesDeleteFilter;
export declare function InventoryRulesDeleteFilterToJSON(value?: InventoryRulesDeleteFilter | null): any;
