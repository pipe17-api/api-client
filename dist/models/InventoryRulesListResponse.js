"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRulesListResponseToJSON = exports.InventoryRulesListResponseFromJSONTyped = exports.InventoryRulesListResponseFromJSON = exports.InventoryRulesListResponseCodeEnum = exports.InventoryRulesListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryRulesListResponseSuccessEnum;
(function (InventoryRulesListResponseSuccessEnum) {
    InventoryRulesListResponseSuccessEnum["True"] = "true";
})(InventoryRulesListResponseSuccessEnum = exports.InventoryRulesListResponseSuccessEnum || (exports.InventoryRulesListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var InventoryRulesListResponseCodeEnum;
(function (InventoryRulesListResponseCodeEnum) {
    InventoryRulesListResponseCodeEnum[InventoryRulesListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    InventoryRulesListResponseCodeEnum[InventoryRulesListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    InventoryRulesListResponseCodeEnum[InventoryRulesListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(InventoryRulesListResponseCodeEnum = exports.InventoryRulesListResponseCodeEnum || (exports.InventoryRulesListResponseCodeEnum = {}));
function InventoryRulesListResponseFromJSON(json) {
    return InventoryRulesListResponseFromJSONTyped(json, false);
}
exports.InventoryRulesListResponseFromJSON = InventoryRulesListResponseFromJSON;
function InventoryRulesListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryRulesListFilterFromJSON(json['filters']),
        'rules': !runtime_1.exists(json, 'rules') ? undefined : (json['rules'].map(_1.InventoryRuleFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryRulesListResponseFromJSONTyped = InventoryRulesListResponseFromJSONTyped;
function InventoryRulesListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.InventoryRulesListFilterToJSON(value.filters),
        'rules': value.rules === undefined ? undefined : (value.rules.map(_1.InventoryRuleToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryRulesListResponseToJSON = InventoryRulesListResponseToJSON;
