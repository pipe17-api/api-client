/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ReturnLineItem, ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnUpdateRequest
 */
export interface ReturnUpdateRequest {
    /**
     *
     * @type {ReturnStatus}
     * @memberof ReturnUpdateRequest
     */
    status?: ReturnStatus;
    /**
     *
     * @type {Array<ReturnLineItem>}
     * @memberof ReturnUpdateRequest
     */
    lineItems?: Array<ReturnLineItem>;
    /**
     *
     * @type {Address}
     * @memberof ReturnUpdateRequest
     */
    shippingAddress?: Address;
    /**
     * Currency value, e.g. 'USD'
     * @type {string}
     * @memberof ReturnUpdateRequest
     */
    currency?: string;
    /**
     * Customer notes
     * @type {string}
     * @memberof ReturnUpdateRequest
     */
    customerNotes?: string;
    /**
     * Merchant notes
     * @type {string}
     * @memberof ReturnUpdateRequest
     */
    notes?: string;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    tax?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    discount?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    subTotal?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    total?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    shippingPrice?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    shippingRefund?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    shippingQuote?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    shippingLabelFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    restockingFee?: number;
    /**
     *
     * @type {number}
     * @memberof ReturnUpdateRequest
     */
    estimatedTotal?: number;
    /**
     *
     * @type {boolean}
     * @memberof ReturnUpdateRequest
     */
    isExchange?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnUpdateRequest
     */
    isGift?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof ReturnUpdateRequest
     */
    requiresShipping?: boolean;
    /**
     *
     * @type {Date}
     * @memberof ReturnUpdateRequest
     */
    refundedAt?: Date;
}
export declare function ReturnUpdateRequestFromJSON(json: any): ReturnUpdateRequest;
export declare function ReturnUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnUpdateRequest;
export declare function ReturnUpdateRequestToJSON(value?: ReturnUpdateRequest | null): any;
