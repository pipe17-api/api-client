"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingsFilterAllOfToJSON = exports.MappingsFilterAllOfFromJSONTyped = exports.MappingsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function MappingsFilterAllOfFromJSON(json) {
    return MappingsFilterAllOfFromJSONTyped(json, false);
}
exports.MappingsFilterAllOfFromJSON = MappingsFilterAllOfFromJSON;
function MappingsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mappingId': !runtime_1.exists(json, 'mappingId') ? undefined : json['mappingId'],
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'desc': !runtime_1.exists(json, 'desc') ? undefined : json['desc'],
    };
}
exports.MappingsFilterAllOfFromJSONTyped = MappingsFilterAllOfFromJSONTyped;
function MappingsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mappingId': value.mappingId,
        'uuid': value.uuid,
        'desc': value.desc,
    };
}
exports.MappingsFilterAllOfToJSON = MappingsFilterAllOfToJSON;
