/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * WebhookTopic
 * @export
 * @enum {string}
 */
export declare enum WebhookTopics {
    Arrivals = "arrivals",
    Fulfillments = "fulfillments",
    Integrations = "integrations",
    Inventory = "inventory",
    Labels = "labels",
    Locations = "locations",
    Orders = "orders",
    Products = "products",
    Purchases = "purchases",
    Receipts = "receipts",
    Refunds = "refunds",
    Returns = "returns",
    Shipments = "shipments",
    Suppliers = "suppliers",
    Transfers = "transfers",
    Trackings = "trackings",
    Exceptions = "exceptions"
}
export declare function WebhookTopicsFromJSON(json: any): WebhookTopics;
export declare function WebhookTopicsFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookTopics;
export declare function WebhookTopicsToJSON(value?: WebhookTopics | null): any;
