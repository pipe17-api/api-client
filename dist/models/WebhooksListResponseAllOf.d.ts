/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Webhook, WebhooksListFilter } from './';
/**
 *
 * @export
 * @interface WebhooksListResponseAllOf
 */
export interface WebhooksListResponseAllOf {
    /**
     *
     * @type {WebhooksListFilter}
     * @memberof WebhooksListResponseAllOf
     */
    filters?: WebhooksListFilter;
    /**
     *
     * @type {Array<Webhook>}
     * @memberof WebhooksListResponseAllOf
     */
    webhooks?: Array<Webhook>;
    /**
     *
     * @type {Pagination}
     * @memberof WebhooksListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function WebhooksListResponseAllOfFromJSON(json: any): WebhooksListResponseAllOf;
export declare function WebhooksListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhooksListResponseAllOf;
export declare function WebhooksListResponseAllOfToJSON(value?: WebhooksListResponseAllOf | null): any;
