/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferStatus } from './';
/**
 *
 * @export
 * @interface TransfersFilter
 */
export interface TransfersFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof TransfersFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof TransfersFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof TransfersFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof TransfersFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof TransfersFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof TransfersFilter
     */
    count?: number;
    /**
     * Transfers by list of transferId
     * @type {Array<string>}
     * @memberof TransfersFilter
     */
    transferId?: Array<string>;
    /**
     * Transfers by list of external transfer IDs
     * @type {Array<string>}
     * @memberof TransfersFilter
     */
    extOrderId?: Array<string>;
    /**
     * Transfers by list of statuses
     * @type {Array<TransferStatus>}
     * @memberof TransfersFilter
     */
    status?: Array<TransferStatus>;
    /**
     * Soft deleted transfers
     * @type {boolean}
     * @memberof TransfersFilter
     */
    deleted?: boolean;
}
export declare function TransfersFilterFromJSON(json: any): TransfersFilter;
export declare function TransfersFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersFilter;
export declare function TransfersFilterToJSON(value?: TransfersFilter | null): any;
