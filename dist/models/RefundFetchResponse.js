"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundFetchResponseToJSON = exports.RefundFetchResponseFromJSONTyped = exports.RefundFetchResponseFromJSON = exports.RefundFetchResponseCodeEnum = exports.RefundFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var RefundFetchResponseSuccessEnum;
(function (RefundFetchResponseSuccessEnum) {
    RefundFetchResponseSuccessEnum["True"] = "true";
})(RefundFetchResponseSuccessEnum = exports.RefundFetchResponseSuccessEnum || (exports.RefundFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var RefundFetchResponseCodeEnum;
(function (RefundFetchResponseCodeEnum) {
    RefundFetchResponseCodeEnum[RefundFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    RefundFetchResponseCodeEnum[RefundFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    RefundFetchResponseCodeEnum[RefundFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(RefundFetchResponseCodeEnum = exports.RefundFetchResponseCodeEnum || (exports.RefundFetchResponseCodeEnum = {}));
function RefundFetchResponseFromJSON(json) {
    return RefundFetchResponseFromJSONTyped(json, false);
}
exports.RefundFetchResponseFromJSON = RefundFetchResponseFromJSON;
function RefundFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'refund': !runtime_1.exists(json, 'refund') ? undefined : _1.RefundFromJSON(json['refund']),
    };
}
exports.RefundFetchResponseFromJSONTyped = RefundFetchResponseFromJSONTyped;
function RefundFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'refund': _1.RefundToJSON(value.refund),
    };
}
exports.RefundFetchResponseToJSON = RefundFetchResponseToJSON;
