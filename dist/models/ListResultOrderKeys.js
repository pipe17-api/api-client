"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListResultOrderKeysToJSON = exports.ListResultOrderKeysFromJSONTyped = exports.ListResultOrderKeysFromJSON = void 0;
const runtime_1 = require("../runtime");
function ListResultOrderKeysFromJSON(json) {
    return ListResultOrderKeysFromJSONTyped(json, false);
}
exports.ListResultOrderKeysFromJSON = ListResultOrderKeysFromJSON;
function ListResultOrderKeysFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
    };
}
exports.ListResultOrderKeysFromJSONTyped = ListResultOrderKeysFromJSONTyped;
function ListResultOrderKeysToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'order': value.order,
        'keys': value.keys,
    };
}
exports.ListResultOrderKeysToJSON = ListResultOrderKeysToJSON;
