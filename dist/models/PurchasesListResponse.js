"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesListResponseToJSON = exports.PurchasesListResponseFromJSONTyped = exports.PurchasesListResponseFromJSON = exports.PurchasesListResponseCodeEnum = exports.PurchasesListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var PurchasesListResponseSuccessEnum;
(function (PurchasesListResponseSuccessEnum) {
    PurchasesListResponseSuccessEnum["True"] = "true";
})(PurchasesListResponseSuccessEnum = exports.PurchasesListResponseSuccessEnum || (exports.PurchasesListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var PurchasesListResponseCodeEnum;
(function (PurchasesListResponseCodeEnum) {
    PurchasesListResponseCodeEnum[PurchasesListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    PurchasesListResponseCodeEnum[PurchasesListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    PurchasesListResponseCodeEnum[PurchasesListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(PurchasesListResponseCodeEnum = exports.PurchasesListResponseCodeEnum || (exports.PurchasesListResponseCodeEnum = {}));
function PurchasesListResponseFromJSON(json) {
    return PurchasesListResponseFromJSONTyped(json, false);
}
exports.PurchasesListResponseFromJSON = PurchasesListResponseFromJSON;
function PurchasesListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesListFilterFromJSON(json['filters']),
        'purchases': (json['purchases'].map(_1.PurchaseFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.PurchasesListResponseFromJSONTyped = PurchasesListResponseFromJSONTyped;
function PurchasesListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.PurchasesListFilterToJSON(value.filters),
        'purchases': (value.purchases.map(_1.PurchaseToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.PurchasesListResponseToJSON = PurchasesListResponseToJSON;
