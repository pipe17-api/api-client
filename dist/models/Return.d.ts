/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, ReturnLineItem, ReturnStatus } from './';
/**
 *
 * @export
 * @interface Return
 */
export interface Return {
    /**
     * Return ID
     * @type {string}
     * @memberof Return
     */
    returnId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Return
     */
    integration?: string;
    /**
     *
     * @type {ReturnStatus}
     * @memberof Return
     */
    status?: ReturnStatus;
    /**
     * Return ID on external system
     * @type {string}
     * @memberof Return
     */
    extReturnId?: string;
    /**
     *
     * @type {Array<ReturnLineItem>}
     * @memberof Return
     */
    lineItems?: Array<ReturnLineItem>;
    /**
     *
     * @type {Address}
     * @memberof Return
     */
    shippingAddress?: Address;
    /**
     * Currency value, e.g. 'USD'
     * @type {string}
     * @memberof Return
     */
    currency?: string;
    /**
     * Customer notes
     * @type {string}
     * @memberof Return
     */
    customerNotes?: string;
    /**
     * Merchant notes
     * @type {string}
     * @memberof Return
     */
    notes?: string;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    tax?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    discount?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    subTotal?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    total?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    shippingPrice?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    shippingRefund?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    shippingQuote?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    shippingLabelFee?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    restockingFee?: number;
    /**
     *
     * @type {number}
     * @memberof Return
     */
    estimatedTotal?: number;
    /**
     *
     * @type {boolean}
     * @memberof Return
     */
    isExchange?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Return
     */
    isGift?: boolean;
    /**
     *
     * @type {boolean}
     * @memberof Return
     */
    requiresShipping?: boolean;
    /**
     *
     * @type {Date}
     * @memberof Return
     */
    refundedAt?: Date;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Return
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Return
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Return
     */
    readonly orgKey?: string;
}
export declare function ReturnFromJSON(json: any): Return;
export declare function ReturnFromJSONTyped(json: any, ignoreDiscriminator: boolean): Return;
export declare function ReturnToJSON(value?: Return | null): any;
