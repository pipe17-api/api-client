/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface NameValue
 */
export interface NameValue {
    /**
     *
     * @type {string}
     * @memberof NameValue
     */
    name?: string;
    /**
     *
     * @type {string}
     * @memberof NameValue
     */
    value?: string;
}
export declare function NameValueFromJSON(json: any): NameValue;
export declare function NameValueFromJSONTyped(json: any, ignoreDiscriminator: boolean): NameValue;
export declare function NameValueToJSON(value?: NameValue | null): any;
