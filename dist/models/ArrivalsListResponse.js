"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrivalsListResponseToJSON = exports.ArrivalsListResponseFromJSONTyped = exports.ArrivalsListResponseFromJSON = exports.ArrivalsListResponseCodeEnum = exports.ArrivalsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ArrivalsListResponseSuccessEnum;
(function (ArrivalsListResponseSuccessEnum) {
    ArrivalsListResponseSuccessEnum["True"] = "true";
})(ArrivalsListResponseSuccessEnum = exports.ArrivalsListResponseSuccessEnum || (exports.ArrivalsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ArrivalsListResponseCodeEnum;
(function (ArrivalsListResponseCodeEnum) {
    ArrivalsListResponseCodeEnum[ArrivalsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ArrivalsListResponseCodeEnum[ArrivalsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ArrivalsListResponseCodeEnum[ArrivalsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ArrivalsListResponseCodeEnum = exports.ArrivalsListResponseCodeEnum || (exports.ArrivalsListResponseCodeEnum = {}));
function ArrivalsListResponseFromJSON(json) {
    return ArrivalsListResponseFromJSONTyped(json, false);
}
exports.ArrivalsListResponseFromJSON = ArrivalsListResponseFromJSON;
function ArrivalsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ArrivalsListFilterFromJSON(json['filters']),
        'arrivals': (json['arrivals'].map(_1.ArrivalFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ArrivalsListResponseFromJSONTyped = ArrivalsListResponseFromJSONTyped;
function ArrivalsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ArrivalsListFilterToJSON(value.filters),
        'arrivals': (value.arrivals.map(_1.ArrivalToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ArrivalsListResponseToJSON = ArrivalsListResponseToJSON;
