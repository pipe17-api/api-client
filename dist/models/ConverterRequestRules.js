"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConverterRequestRulesToJSON = exports.ConverterRequestRulesFromJSONTyped = exports.ConverterRequestRulesFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConverterRequestRulesFromJSON(json) {
    return ConverterRequestRulesFromJSONTyped(json, false);
}
exports.ConverterRequestRulesFromJSON = ConverterRequestRulesFromJSON;
function ConverterRequestRulesFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingRuleFromJSON)),
        'mappingUuid': !runtime_1.exists(json, 'mappingUuid') ? undefined : json['mappingUuid'],
    };
}
exports.ConverterRequestRulesFromJSONTyped = ConverterRequestRulesFromJSONTyped;
function ConverterRequestRulesToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingRuleToJSON)),
        'mappingUuid': value.mappingUuid,
    };
}
exports.ConverterRequestRulesToJSON = ConverterRequestRulesToJSON;
