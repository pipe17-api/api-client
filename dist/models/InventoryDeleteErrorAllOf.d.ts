/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryDeleteFilter } from './';
/**
 *
 * @export
 * @interface InventoryDeleteErrorAllOf
 */
export interface InventoryDeleteErrorAllOf {
    /**
     *
     * @type {InventoryDeleteFilter}
     * @memberof InventoryDeleteErrorAllOf
     */
    filters?: InventoryDeleteFilter;
}
export declare function InventoryDeleteErrorAllOfFromJSON(json: any): InventoryDeleteErrorAllOf;
export declare function InventoryDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryDeleteErrorAllOf;
export declare function InventoryDeleteErrorAllOfToJSON(value?: InventoryDeleteErrorAllOf | null): any;
