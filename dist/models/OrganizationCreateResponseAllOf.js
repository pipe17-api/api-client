"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationCreateResponseAllOfToJSON = exports.OrganizationCreateResponseAllOfFromJSONTyped = exports.OrganizationCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationCreateResponseAllOfFromJSON(json) {
    return OrganizationCreateResponseAllOfFromJSONTyped(json, false);
}
exports.OrganizationCreateResponseAllOfFromJSON = OrganizationCreateResponseAllOfFromJSON;
function OrganizationCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'organization': !runtime_1.exists(json, 'organization') ? undefined : _1.OrganizationFromJSON(json['organization']),
    };
}
exports.OrganizationCreateResponseAllOfFromJSONTyped = OrganizationCreateResponseAllOfFromJSONTyped;
function OrganizationCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'organization': _1.OrganizationToJSON(value.organization),
    };
}
exports.OrganizationCreateResponseAllOfToJSON = OrganizationCreateResponseAllOfToJSON;
