/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Delete Refunds Filter
 * @export
 * @interface RefundsDeleteFilterAllOf
 */
export interface RefundsDeleteFilterAllOf {
    /**
     * Refunds of these integrations
     * @type {Array<string>}
     * @memberof RefundsDeleteFilterAllOf
     */
    integration?: Array<string>;
}
export declare function RefundsDeleteFilterAllOfFromJSON(json: any): RefundsDeleteFilterAllOf;
export declare function RefundsDeleteFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsDeleteFilterAllOf;
export declare function RefundsDeleteFilterAllOfToJSON(value?: RefundsDeleteFilterAllOf | null): any;
