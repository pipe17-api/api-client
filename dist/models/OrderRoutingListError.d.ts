/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRoutingsListFilter } from './';
/**
 *
 * @export
 * @interface OrderRoutingListError
 */
export interface OrderRoutingListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrderRoutingListError
     */
    success: OrderRoutingListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrderRoutingListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrderRoutingListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {OrderRoutingsListFilter}
     * @memberof OrderRoutingListError
     */
    filters?: OrderRoutingsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingListErrorSuccessEnum {
    False = "false"
}
export declare function OrderRoutingListErrorFromJSON(json: any): OrderRoutingListError;
export declare function OrderRoutingListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingListError;
export declare function OrderRoutingListErrorToJSON(value?: OrderRoutingListError | null): any;
