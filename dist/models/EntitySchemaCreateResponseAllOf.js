"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaCreateResponseAllOfToJSON = exports.EntitySchemaCreateResponseAllOfFromJSONTyped = exports.EntitySchemaCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntitySchemaCreateResponseAllOfFromJSON(json) {
    return EntitySchemaCreateResponseAllOfFromJSONTyped(json, false);
}
exports.EntitySchemaCreateResponseAllOfFromJSON = EntitySchemaCreateResponseAllOfFromJSON;
function EntitySchemaCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'entitySchema': !runtime_1.exists(json, 'entitySchema') ? undefined : _1.EntitySchemaFromJSON(json['entitySchema']),
    };
}
exports.EntitySchemaCreateResponseAllOfFromJSONTyped = EntitySchemaCreateResponseAllOfFromJSONTyped;
function EntitySchemaCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'entitySchema': _1.EntitySchemaToJSON(value.entitySchema),
    };
}
exports.EntitySchemaCreateResponseAllOfToJSON = EntitySchemaCreateResponseAllOfToJSON;
