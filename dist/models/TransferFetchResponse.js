"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferFetchResponseToJSON = exports.TransferFetchResponseFromJSONTyped = exports.TransferFetchResponseFromJSON = exports.TransferFetchResponseCodeEnum = exports.TransferFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransferFetchResponseSuccessEnum;
(function (TransferFetchResponseSuccessEnum) {
    TransferFetchResponseSuccessEnum["True"] = "true";
})(TransferFetchResponseSuccessEnum = exports.TransferFetchResponseSuccessEnum || (exports.TransferFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TransferFetchResponseCodeEnum;
(function (TransferFetchResponseCodeEnum) {
    TransferFetchResponseCodeEnum[TransferFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TransferFetchResponseCodeEnum[TransferFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TransferFetchResponseCodeEnum[TransferFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TransferFetchResponseCodeEnum = exports.TransferFetchResponseCodeEnum || (exports.TransferFetchResponseCodeEnum = {}));
function TransferFetchResponseFromJSON(json) {
    return TransferFetchResponseFromJSONTyped(json, false);
}
exports.TransferFetchResponseFromJSON = TransferFetchResponseFromJSON;
function TransferFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'transfer': !runtime_1.exists(json, 'transfer') ? undefined : _1.TransferFromJSON(json['transfer']),
    };
}
exports.TransferFetchResponseFromJSONTyped = TransferFetchResponseFromJSONTyped;
function TransferFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'transfer': _1.TransferToJSON(value.transfer),
    };
}
exports.TransferFetchResponseToJSON = TransferFetchResponseToJSON;
