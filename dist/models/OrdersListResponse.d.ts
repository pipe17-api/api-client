/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order, OrdersListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrdersListResponse
 */
export interface OrdersListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrdersListResponse
     */
    success: OrdersListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrdersListResponse
     */
    code: OrdersListResponseCodeEnum;
    /**
     *
     * @type {OrdersListFilter}
     * @memberof OrdersListResponse
     */
    filters?: OrdersListFilter;
    /**
     *
     * @type {Array<Order>}
     * @memberof OrdersListResponse
     */
    orders: Array<Order>;
    /**
     *
     * @type {Pagination}
     * @memberof OrdersListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum OrdersListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrdersListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrdersListResponseFromJSON(json: any): OrdersListResponse;
export declare function OrdersListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersListResponse;
export declare function OrdersListResponseToJSON(value?: OrdersListResponse | null): any;
