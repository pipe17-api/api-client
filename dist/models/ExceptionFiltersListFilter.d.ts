/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ExceptionFiltersListFilter
 */
export interface ExceptionFiltersListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ExceptionFiltersListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ExceptionFiltersListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ExceptionFiltersListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ExceptionFiltersListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ExceptionFiltersListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ExceptionFiltersListFilter
     */
    count?: number;
    /**
     * Filters by filterId-list
     * @type {Array<string>}
     * @memberof ExceptionFiltersListFilter
     */
    filterId?: Array<string>;
    /**
     * Filters by exceptionType-list
     * @type {Array<string>}
     * @memberof ExceptionFiltersListFilter
     */
    exceptionType?: Array<string>;
    /**
     * List sort order
     * @type {string}
     * @memberof ExceptionFiltersListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof ExceptionFiltersListFilter
     */
    keys?: string;
}
export declare function ExceptionFiltersListFilterFromJSON(json: any): ExceptionFiltersListFilter;
export declare function ExceptionFiltersListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFiltersListFilter;
export declare function ExceptionFiltersListFilterToJSON(value?: ExceptionFiltersListFilter | null): any;
