/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LocationUpdateAddress
 */
export interface LocationUpdateAddress {
    /**
     * Company
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    company?: string;
    /**
     * Address
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    address1?: string;
    /**
     * Address Line 2
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    address2?: string;
    /**
     * City
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    city?: string;
    /**
     * State or Province
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    stateOrProvince?: string;
    /**
     * zipcode or postal code
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    zipCodeOrPostalCode?: string;
    /**
     * Country
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    country?: string;
    /**
     * email
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    email?: string;
    /**
     * Phone
     * @type {string}
     * @memberof LocationUpdateAddress
     */
    phone?: string;
}
export declare function LocationUpdateAddressFromJSON(json: any): LocationUpdateAddress;
export declare function LocationUpdateAddressFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationUpdateAddress;
export declare function LocationUpdateAddressToJSON(value?: LocationUpdateAddress | null): any;
