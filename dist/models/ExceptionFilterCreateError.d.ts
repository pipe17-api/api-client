/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionFilterCreateData } from './';
/**
 *
 * @export
 * @interface ExceptionFilterCreateError
 */
export interface ExceptionFilterCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ExceptionFilterCreateError
     */
    success: ExceptionFilterCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionFilterCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ExceptionFilterCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ExceptionFilterCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ExceptionFilterCreateData}
     * @memberof ExceptionFilterCreateError
     */
    exceptionFilter?: ExceptionFilterCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionFilterCreateErrorSuccessEnum {
    False = "false"
}
export declare function ExceptionFilterCreateErrorFromJSON(json: any): ExceptionFilterCreateError;
export declare function ExceptionFilterCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFilterCreateError;
export declare function ExceptionFilterCreateErrorToJSON(value?: ExceptionFilterCreateError | null): any;
