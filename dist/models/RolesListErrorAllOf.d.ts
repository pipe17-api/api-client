/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RolesListFilter } from './';
/**
 *
 * @export
 * @interface RolesListErrorAllOf
 */
export interface RolesListErrorAllOf {
    /**
     *
     * @type {RolesListFilter}
     * @memberof RolesListErrorAllOf
     */
    filters?: RolesListFilter;
}
export declare function RolesListErrorAllOfFromJSON(json: any): RolesListErrorAllOf;
export declare function RolesListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RolesListErrorAllOf;
export declare function RolesListErrorAllOfToJSON(value?: RolesListErrorAllOf | null): any;
