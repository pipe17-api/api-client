/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label, LabelsListFilter } from './';
/**
 *
 * @export
 * @interface LabelsListResponse
 */
export interface LabelsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LabelsListResponse
     */
    success: LabelsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelsListResponse
     */
    code: LabelsListResponseCodeEnum;
    /**
     *
     * @type {LabelsListFilter}
     * @memberof LabelsListResponse
     */
    filters?: LabelsListFilter;
    /**
     *
     * @type {Array<Label>}
     * @memberof LabelsListResponse
     */
    labels?: Array<Label>;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LabelsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LabelsListResponseFromJSON(json: any): LabelsListResponse;
export declare function LabelsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsListResponse;
export declare function LabelsListResponseToJSON(value?: LabelsListResponse | null): any;
