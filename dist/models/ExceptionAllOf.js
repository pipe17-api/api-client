"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionAllOfToJSON = exports.ExceptionAllOfFromJSONTyped = exports.ExceptionAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionAllOfFromJSON(json) {
    return ExceptionAllOfFromJSONTyped(json, false);
}
exports.ExceptionAllOfFromJSON = ExceptionAllOfFromJSON;
function ExceptionAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionId': !runtime_1.exists(json, 'exceptionId') ? undefined : json['exceptionId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ExceptionStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.ExceptionAllOfFromJSONTyped = ExceptionAllOfFromJSONTyped;
function ExceptionAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionId': value.exceptionId,
        'status': _1.ExceptionStatusToJSON(value.status),
        'integration': value.integration,
    };
}
exports.ExceptionAllOfToJSON = ExceptionAllOfToJSON;
