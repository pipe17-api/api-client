/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Refund, RefundsFilter } from './';
/**
 *
 * @export
 * @interface RefundsListResponse
 */
export interface RefundsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RefundsListResponse
     */
    success: RefundsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundsListResponse
     */
    code: RefundsListResponseCodeEnum;
    /**
     *
     * @type {RefundsFilter}
     * @memberof RefundsListResponse
     */
    filters?: RefundsFilter;
    /**
     *
     * @type {Array<Refund>}
     * @memberof RefundsListResponse
     */
    refunds?: Array<Refund>;
    /**
     *
     * @type {Pagination}
     * @memberof RefundsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RefundsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RefundsListResponseFromJSON(json: any): RefundsListResponse;
export declare function RefundsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsListResponse;
export declare function RefundsListResponseToJSON(value?: RefundsListResponse | null): any;
