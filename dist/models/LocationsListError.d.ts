/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationsListFilter } from './';
/**
 *
 * @export
 * @interface LocationsListError
 */
export interface LocationsListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LocationsListError
     */
    success: LocationsListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationsListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LocationsListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LocationsListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {LocationsListFilter}
     * @memberof LocationsListError
     */
    filters?: LocationsListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationsListErrorSuccessEnum {
    False = "false"
}
export declare function LocationsListErrorFromJSON(json: any): LocationsListError;
export declare function LocationsListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsListError;
export declare function LocationsListErrorToJSON(value?: LocationsListError | null): any;
