/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKeysListFilter } from './';
/**
 *
 * @export
 * @interface ApiKeysListErrorAllOf
 */
export interface ApiKeysListErrorAllOf {
    /**
     *
     * @type {ApiKeysListFilter}
     * @memberof ApiKeysListErrorAllOf
     */
    filters?: ApiKeysListFilter;
}
export declare function ApiKeysListErrorAllOfFromJSON(json: any): ApiKeysListErrorAllOf;
export declare function ApiKeysListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysListErrorAllOf;
export declare function ApiKeysListErrorAllOfToJSON(value?: ApiKeysListErrorAllOf | null): any;
