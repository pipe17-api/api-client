"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationBaseAllOfToJSON = exports.IntegrationBaseAllOfFromJSONTyped = exports.IntegrationBaseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationBaseAllOfFromJSON(json) {
    return IntegrationBaseAllOfFromJSONTyped(json, false);
}
exports.IntegrationBaseAllOfFromJSON = IntegrationBaseAllOfFromJSON;
function IntegrationBaseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'version': !runtime_1.exists(json, 'version') ? undefined : json['version'],
        'integrationId': !runtime_1.exists(json, 'integrationId') ? undefined : json['integrationId'],
        'errorText': !runtime_1.exists(json, 'errorText') ? undefined : json['errorText'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.IntegrationStatusFromJSON(json['status']),
    };
}
exports.IntegrationBaseAllOfFromJSONTyped = IntegrationBaseAllOfFromJSONTyped;
function IntegrationBaseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'version': value.version,
        'integrationId': value.integrationId,
        'errorText': value.errorText,
        'status': _1.IntegrationStatusToJSON(value.status),
    };
}
exports.IntegrationBaseAllOfToJSON = IntegrationBaseAllOfToJSON;
