/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Account, AccountsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface AccountsListResponseAllOf
 */
export interface AccountsListResponseAllOf {
    /**
     *
     * @type {AccountsListFilter}
     * @memberof AccountsListResponseAllOf
     */
    filters?: AccountsListFilter;
    /**
     *
     * @type {Array<Account>}
     * @memberof AccountsListResponseAllOf
     */
    accounts?: Array<Account>;
    /**
     *
     * @type {Pagination}
     * @memberof AccountsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function AccountsListResponseAllOfFromJSON(json: any): AccountsListResponseAllOf;
export declare function AccountsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsListResponseAllOf;
export declare function AccountsListResponseAllOfToJSON(value?: AccountsListResponseAllOf | null): any;
