/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ArrivalsDeleteError
 */
export interface ArrivalsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ArrivalsDeleteError
     */
    success: ArrivalsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ArrivalsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ArrivalsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ArrivalsDeleteFilter}
     * @memberof ArrivalsDeleteError
     */
    filters?: ArrivalsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function ArrivalsDeleteErrorFromJSON(json: any): ArrivalsDeleteError;
export declare function ArrivalsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsDeleteError;
export declare function ArrivalsDeleteErrorToJSON(value?: ArrivalsDeleteError | null): any;
