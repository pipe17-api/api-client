"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventRequestSourceToJSON = exports.EventRequestSourceFromJSONTyped = exports.EventRequestSourceFromJSON = exports.EventRequestSource = void 0;
/**
 * Event Type
 * @export
 * @enum {string}
 */
var EventRequestSource;
(function (EventRequestSource) {
    EventRequestSource["Api"] = "api";
    EventRequestSource["ApiWebhook"] = "api-webhook";
    EventRequestSource["Custom"] = "custom";
})(EventRequestSource = exports.EventRequestSource || (exports.EventRequestSource = {}));
function EventRequestSourceFromJSON(json) {
    return EventRequestSourceFromJSONTyped(json, false);
}
exports.EventRequestSourceFromJSON = EventRequestSourceFromJSON;
function EventRequestSourceFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.EventRequestSourceFromJSONTyped = EventRequestSourceFromJSONTyped;
function EventRequestSourceToJSON(value) {
    return value;
}
exports.EventRequestSourceToJSON = EventRequestSourceToJSON;
