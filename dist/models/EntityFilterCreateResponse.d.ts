/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFilter } from './';
/**
 *
 * @export
 * @interface EntityFilterCreateResponse
 */
export interface EntityFilterCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntityFilterCreateResponse
     */
    success: EntityFilterCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterCreateResponse
     */
    code: EntityFilterCreateResponseCodeEnum;
    /**
     *
     * @type {EntityFilter}
     * @memberof EntityFilterCreateResponse
     */
    entityFilter?: EntityFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntityFilterCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntityFilterCreateResponseFromJSON(json: any): EntityFilterCreateResponse;
export declare function EntityFilterCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterCreateResponse;
export declare function EntityFilterCreateResponseToJSON(value?: EntityFilterCreateResponse | null): any;
