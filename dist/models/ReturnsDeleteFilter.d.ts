/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnStatus } from './';
/**
 *
 * @export
 * @interface ReturnsDeleteFilter
 */
export interface ReturnsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ReturnsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ReturnsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ReturnsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ReturnsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ReturnsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ReturnsDeleteFilter
     */
    count?: number;
    /**
     * Returns by list of returnId
     * @type {Array<string>}
     * @memberof ReturnsDeleteFilter
     */
    returnId?: Array<string>;
    /**
     * Returns by list of external return ids
     * @type {Array<string>}
     * @memberof ReturnsDeleteFilter
     */
    extReturnId?: Array<string>;
    /**
     * Returns by list of statuses
     * @type {Array<ReturnStatus>}
     * @memberof ReturnsDeleteFilter
     */
    status?: Array<ReturnStatus>;
    /**
     * Soft deleted returns
     * @type {boolean}
     * @memberof ReturnsDeleteFilter
     */
    deleted?: boolean;
    /**
     *
     * @type {Array<string>}
     * @memberof ReturnsDeleteFilter
     */
    integration?: Array<string>;
}
export declare function ReturnsDeleteFilterFromJSON(json: any): ReturnsDeleteFilter;
export declare function ReturnsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsDeleteFilter;
export declare function ReturnsDeleteFilterToJSON(value?: ReturnsDeleteFilter | null): any;
