/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface IntegrationState
 */
export interface IntegrationState {
    /**
     * Integration ID
     * @type {string}
     * @memberof IntegrationState
     */
    integration?: string;
    /**
     * State Name
     * @type {string}
     * @memberof IntegrationState
     */
    name: string;
    /**
     * State Value (if undefined and state exists already, then delete it)
     * @type {string}
     * @memberof IntegrationState
     */
    value?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof IntegrationState
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof IntegrationState
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof IntegrationState
     */
    readonly orgKey?: string;
}
export declare function IntegrationStateFromJSON(json: any): IntegrationState;
export declare function IntegrationStateFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationState;
export declare function IntegrationStateToJSON(value?: IntegrationState | null): any;
