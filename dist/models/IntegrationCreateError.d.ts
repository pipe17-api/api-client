/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationCreateData } from './';
/**
 *
 * @export
 * @interface IntegrationCreateError
 */
export interface IntegrationCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof IntegrationCreateError
     */
    success: IntegrationCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof IntegrationCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {IntegrationCreateData}
     * @memberof IntegrationCreateError
     */
    integration?: IntegrationCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationCreateErrorSuccessEnum {
    False = "false"
}
export declare function IntegrationCreateErrorFromJSON(json: any): IntegrationCreateError;
export declare function IntegrationCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationCreateError;
export declare function IntegrationCreateErrorToJSON(value?: IntegrationCreateError | null): any;
