/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ConverterRequestRules } from './';
/**
 *
 * @export
 * @interface ConverterRequest
 */
export interface ConverterRequest {
    /**
     *
     * @type {ConverterRequestRules}
     * @memberof ConverterRequest
     */
    rules: ConverterRequestRules;
    /**
     * Source object to convert
     * @type {object}
     * @memberof ConverterRequest
     */
    source: object;
}
export declare function ConverterRequestFromJSON(json: any): ConverterRequest;
export declare function ConverterRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConverterRequest;
export declare function ConverterRequestToJSON(value?: ConverterRequest | null): any;
