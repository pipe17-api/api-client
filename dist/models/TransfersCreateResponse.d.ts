/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferCreateResult } from './';
/**
 *
 * @export
 * @interface TransfersCreateResponse
 */
export interface TransfersCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TransfersCreateResponse
     */
    success: TransfersCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransfersCreateResponse
     */
    code: TransfersCreateResponseCodeEnum;
    /**
     *
     * @type {Array<TransferCreateResult>}
     * @memberof TransfersCreateResponse
     */
    transfers?: Array<TransferCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum TransfersCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TransfersCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TransfersCreateResponseFromJSON(json: any): TransfersCreateResponse;
export declare function TransfersCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersCreateResponse;
export declare function TransfersCreateResponseToJSON(value?: TransfersCreateResponse | null): any;
