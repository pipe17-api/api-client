/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingUpdateResponse
 */
export interface OrderRoutingUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrderRoutingUpdateResponse
     */
    success: OrderRoutingUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingUpdateResponse
     */
    code: OrderRoutingUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrderRoutingUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrderRoutingUpdateResponseFromJSON(json: any): OrderRoutingUpdateResponse;
export declare function OrderRoutingUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingUpdateResponse;
export declare function OrderRoutingUpdateResponseToJSON(value?: OrderRoutingUpdateResponse | null): any;
