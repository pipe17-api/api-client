/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFilter } from './';
/**
 *
 * @export
 * @interface EntityFilterCreateResponseAllOf
 */
export interface EntityFilterCreateResponseAllOf {
    /**
     *
     * @type {EntityFilter}
     * @memberof EntityFilterCreateResponseAllOf
     */
    entityFilter?: EntityFilter;
}
export declare function EntityFilterCreateResponseAllOfFromJSON(json: any): EntityFilterCreateResponseAllOf;
export declare function EntityFilterCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterCreateResponseAllOf;
export declare function EntityFilterCreateResponseAllOfToJSON(value?: EntityFilterCreateResponseAllOf | null): any;
