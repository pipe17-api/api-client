"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesListFilterToJSON = exports.PurchasesListFilterFromJSONTyped = exports.PurchasesListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchasesListFilterFromJSON(json) {
    return PurchasesListFilterFromJSONTyped(json, false);
}
exports.PurchasesListFilterFromJSON = PurchasesListFilterFromJSON;
function PurchasesListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'purchaseId': !runtime_1.exists(json, 'purchaseId') ? undefined : json['purchaseId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.PurchaseStatusFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : json['timestamp'],
    };
}
exports.PurchasesListFilterFromJSONTyped = PurchasesListFilterFromJSONTyped;
function PurchasesListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'purchaseId': value.purchaseId,
        'extOrderId': value.extOrderId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.PurchaseStatusToJSON)),
        'deleted': value.deleted,
        'order': value.order,
        'keys': value.keys,
        'timestamp': value.timestamp,
    };
}
exports.PurchasesListFilterToJSON = PurchasesListFilterToJSON;
