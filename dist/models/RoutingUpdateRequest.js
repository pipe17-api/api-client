"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingUpdateRequestToJSON = exports.RoutingUpdateRequestFromJSONTyped = exports.RoutingUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
function RoutingUpdateRequestFromJSON(json) {
    return RoutingUpdateRequestFromJSONTyped(json, false);
}
exports.RoutingUpdateRequestFromJSON = RoutingUpdateRequestFromJSON;
function RoutingUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'enabled': !runtime_1.exists(json, 'enabled') ? undefined : json['enabled'],
        'filter': !runtime_1.exists(json, 'filter') ? undefined : json['filter'],
    };
}
exports.RoutingUpdateRequestFromJSONTyped = RoutingUpdateRequestFromJSONTyped;
function RoutingUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'description': value.description,
        'enabled': value.enabled,
        'filter': value.filter,
    };
}
exports.RoutingUpdateRequestToJSON = RoutingUpdateRequestToJSON;
