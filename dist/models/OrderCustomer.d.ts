/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderCustomer
 */
export interface OrderCustomer {
    /**
     *
     * @type {string}
     * @memberof OrderCustomer
     */
    extCustomerId?: string;
    /**
     *
     * @type {string}
     * @memberof OrderCustomer
     */
    firstName?: string;
    /**
     *
     * @type {string}
     * @memberof OrderCustomer
     */
    lastName?: string;
    /**
     *
     * @type {string}
     * @memberof OrderCustomer
     */
    company?: string;
}
export declare function OrderCustomerFromJSON(json: any): OrderCustomer;
export declare function OrderCustomerFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderCustomer;
export declare function OrderCustomerToJSON(value?: OrderCustomer | null): any;
