"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationsListResponseToJSON = exports.OrganizationsListResponseFromJSONTyped = exports.OrganizationsListResponseFromJSON = exports.OrganizationsListResponseCodeEnum = exports.OrganizationsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrganizationsListResponseSuccessEnum;
(function (OrganizationsListResponseSuccessEnum) {
    OrganizationsListResponseSuccessEnum["True"] = "true";
})(OrganizationsListResponseSuccessEnum = exports.OrganizationsListResponseSuccessEnum || (exports.OrganizationsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrganizationsListResponseCodeEnum;
(function (OrganizationsListResponseCodeEnum) {
    OrganizationsListResponseCodeEnum[OrganizationsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrganizationsListResponseCodeEnum[OrganizationsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrganizationsListResponseCodeEnum[OrganizationsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrganizationsListResponseCodeEnum = exports.OrganizationsListResponseCodeEnum || (exports.OrganizationsListResponseCodeEnum = {}));
function OrganizationsListResponseFromJSON(json) {
    return OrganizationsListResponseFromJSONTyped(json, false);
}
exports.OrganizationsListResponseFromJSON = OrganizationsListResponseFromJSON;
function OrganizationsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrganizationsListFilterFromJSON(json['filters']),
        'organizations': !runtime_1.exists(json, 'organizations') ? undefined : (json['organizations'].map(_1.OrganizationFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrganizationsListResponseFromJSONTyped = OrganizationsListResponseFromJSONTyped;
function OrganizationsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.OrganizationsListFilterToJSON(value.filters),
        'organizations': value.organizations === undefined ? undefined : (value.organizations.map(_1.OrganizationToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrganizationsListResponseToJSON = OrganizationsListResponseToJSON;
