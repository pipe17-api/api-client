/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductUpdateData } from './';
/**
 *
 * @export
 * @interface ProductUpdateErrorAllOf
 */
export interface ProductUpdateErrorAllOf {
    /**
     *
     * @type {ProductUpdateData}
     * @memberof ProductUpdateErrorAllOf
     */
    product?: ProductUpdateData;
}
export declare function ProductUpdateErrorAllOfFromJSON(json: any): ProductUpdateErrorAllOf;
export declare function ProductUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductUpdateErrorAllOf;
export declare function ProductUpdateErrorAllOfToJSON(value?: ProductUpdateErrorAllOf | null): any;
