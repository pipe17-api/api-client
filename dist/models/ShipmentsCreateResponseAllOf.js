"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsCreateResponseAllOfToJSON = exports.ShipmentsCreateResponseAllOfFromJSONTyped = exports.ShipmentsCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentsCreateResponseAllOfFromJSON(json) {
    return ShipmentsCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ShipmentsCreateResponseAllOfFromJSON = ShipmentsCreateResponseAllOfFromJSON;
function ShipmentsCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipments': !runtime_1.exists(json, 'shipments') ? undefined : (json['shipments'].map(_1.ShipmentCreateResultFromJSON)),
    };
}
exports.ShipmentsCreateResponseAllOfFromJSONTyped = ShipmentsCreateResponseAllOfFromJSONTyped;
function ShipmentsCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipments': value.shipments === undefined ? undefined : (value.shipments.map(_1.ShipmentCreateResultToJSON)),
    };
}
exports.ShipmentsCreateResponseAllOfToJSON = ShipmentsCreateResponseAllOfToJSON;
