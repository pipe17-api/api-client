/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface NotImplemented
 */
export interface NotImplemented {
    /**
     * Always true
     * @type {boolean}
     * @memberof NotImplemented
     */
    success: NotImplementedSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof NotImplemented
     */
    code: NotImplementedCodeEnum;
    /**
     *
     * @type {string}
     * @memberof NotImplemented
     */
    message?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum NotImplementedSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum NotImplementedCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function NotImplementedFromJSON(json: any): NotImplemented;
export declare function NotImplementedFromJSONTyped(json: any, ignoreDiscriminator: boolean): NotImplemented;
export declare function NotImplementedToJSON(value?: NotImplemented | null): any;
