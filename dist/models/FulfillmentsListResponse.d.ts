/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Fulfillment, FulfillmentsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface FulfillmentsListResponse
 */
export interface FulfillmentsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof FulfillmentsListResponse
     */
    success: FulfillmentsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof FulfillmentsListResponse
     */
    code: FulfillmentsListResponseCodeEnum;
    /**
     *
     * @type {FulfillmentsListFilter}
     * @memberof FulfillmentsListResponse
     */
    filters?: FulfillmentsListFilter;
    /**
     *
     * @type {Array<Fulfillment>}
     * @memberof FulfillmentsListResponse
     */
    fulfillments: Array<Fulfillment>;
    /**
     *
     * @type {Pagination}
     * @memberof FulfillmentsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum FulfillmentsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function FulfillmentsListResponseFromJSON(json: any): FulfillmentsListResponse;
export declare function FulfillmentsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsListResponse;
export declare function FulfillmentsListResponseToJSON(value?: FulfillmentsListResponse | null): any;
