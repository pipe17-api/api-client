/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchasesDeleteFilter } from './';
/**
 *
 * @export
 * @interface PurchasesDeleteErrorAllOf
 */
export interface PurchasesDeleteErrorAllOf {
    /**
     *
     * @type {PurchasesDeleteFilter}
     * @memberof PurchasesDeleteErrorAllOf
     */
    filters?: PurchasesDeleteFilter;
}
export declare function PurchasesDeleteErrorAllOfFromJSON(json: any): PurchasesDeleteErrorAllOf;
export declare function PurchasesDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesDeleteErrorAllOf;
export declare function PurchasesDeleteErrorAllOfToJSON(value?: PurchasesDeleteErrorAllOf | null): any;
