/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchasesDeleteFilter } from './';
/**
 *
 * @export
 * @interface PurchasesDeleteError
 */
export interface PurchasesDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof PurchasesDeleteError
     */
    success: PurchasesDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchasesDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof PurchasesDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof PurchasesDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {PurchasesDeleteFilter}
     * @memberof PurchasesDeleteError
     */
    filters?: PurchasesDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchasesDeleteErrorSuccessEnum {
    False = "false"
}
export declare function PurchasesDeleteErrorFromJSON(json: any): PurchasesDeleteError;
export declare function PurchasesDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesDeleteError;
export declare function PurchasesDeleteErrorToJSON(value?: PurchasesDeleteError | null): any;
