/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyCreateError
 */
export interface ApiKeyCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ApiKeyCreateError
     */
    success: ApiKeyCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ApiKeyCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ApiKeyCreateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyCreateErrorSuccessEnum {
    False = "false"
}
export declare function ApiKeyCreateErrorFromJSON(json: any): ApiKeyCreateError;
export declare function ApiKeyCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyCreateError;
export declare function ApiKeyCreateErrorToJSON(value?: ApiKeyCreateError | null): any;
