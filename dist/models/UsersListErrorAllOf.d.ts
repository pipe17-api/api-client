/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UsersListFilter } from './';
/**
 *
 * @export
 * @interface UsersListErrorAllOf
 */
export interface UsersListErrorAllOf {
    /**
     *
     * @type {UsersListFilter}
     * @memberof UsersListErrorAllOf
     */
    filters?: UsersListFilter;
}
export declare function UsersListErrorAllOfFromJSON(json: any): UsersListErrorAllOf;
export declare function UsersListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UsersListErrorAllOf;
export declare function UsersListErrorAllOfToJSON(value?: UsersListErrorAllOf | null): any;
