"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentUpdateErrorToJSON = exports.ShipmentUpdateErrorFromJSONTyped = exports.ShipmentUpdateErrorFromJSON = exports.ShipmentUpdateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ShipmentUpdateErrorSuccessEnum;
(function (ShipmentUpdateErrorSuccessEnum) {
    ShipmentUpdateErrorSuccessEnum["False"] = "false";
})(ShipmentUpdateErrorSuccessEnum = exports.ShipmentUpdateErrorSuccessEnum || (exports.ShipmentUpdateErrorSuccessEnum = {}));
function ShipmentUpdateErrorFromJSON(json) {
    return ShipmentUpdateErrorFromJSONTyped(json, false);
}
exports.ShipmentUpdateErrorFromJSON = ShipmentUpdateErrorFromJSON;
function ShipmentUpdateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'shipment': !runtime_1.exists(json, 'shipment') ? undefined : _1.ShipmentUpdateDataFromJSON(json['shipment']),
    };
}
exports.ShipmentUpdateErrorFromJSONTyped = ShipmentUpdateErrorFromJSONTyped;
function ShipmentUpdateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'shipment': _1.ShipmentUpdateDataToJSON(value.shipment),
    };
}
exports.ShipmentUpdateErrorToJSON = ShipmentUpdateErrorToJSON;
