"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersDeleteErrorAllOfToJSON = exports.OrdersDeleteErrorAllOfFromJSONTyped = exports.OrdersDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrdersDeleteErrorAllOfFromJSON(json) {
    return OrdersDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.OrdersDeleteErrorAllOfFromJSON = OrdersDeleteErrorAllOfFromJSON;
function OrdersDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersDeleteFilterFromJSON(json['filters']),
    };
}
exports.OrdersDeleteErrorAllOfFromJSONTyped = OrdersDeleteErrorAllOfFromJSONTyped;
function OrdersDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.OrdersDeleteFilterToJSON(value.filters),
    };
}
exports.OrdersDeleteErrorAllOfToJSON = OrdersDeleteErrorAllOfToJSON;
