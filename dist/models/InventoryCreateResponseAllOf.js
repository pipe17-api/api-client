"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryCreateResponseAllOfToJSON = exports.InventoryCreateResponseAllOfFromJSONTyped = exports.InventoryCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryCreateResponseAllOfFromJSON(json) {
    return InventoryCreateResponseAllOfFromJSONTyped(json, false);
}
exports.InventoryCreateResponseAllOfFromJSON = InventoryCreateResponseAllOfFromJSON;
function InventoryCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : (json['inventory'].map(_1.InventoryCreateResultFromJSON)),
    };
}
exports.InventoryCreateResponseAllOfFromJSONTyped = InventoryCreateResponseAllOfFromJSONTyped;
function InventoryCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventory': value.inventory === undefined ? undefined : (value.inventory.map(_1.InventoryCreateResultToJSON)),
    };
}
exports.InventoryCreateResponseAllOfToJSON = InventoryCreateResponseAllOfToJSON;
