/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, NameValue, OrderCustomer, OrderLineItem, OrderStatus, Payment } from './';
/**
 *
 * @export
 * @interface Order
 */
export interface Order {
    /**
     * Auto Generated
     * @type {string}
     * @memberof Order
     */
    orderId?: string;
    /**
     *
     * @type {OrderStatus}
     * @memberof Order
     */
    status?: OrderStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Order
     */
    integration?: string;
    /**
     * External Order ID
     * @type {string}
     * @memberof Order
     */
    extOrderId: string;
    /**
     * Order Url in External System
     * @type {string}
     * @memberof Order
     */
    extOrderUrl?: string;
    /**
     * The original order platform, walmart, etsy, etc
     * @type {string}
     * @memberof Order
     */
    orderSource?: string;
    /**
     *
     * @type {Array<OrderLineItem>}
     * @memberof Order
     */
    lineItems?: Array<OrderLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof Order
     */
    subTotalPrice?: number;
    /**
     * Order Discount
     * @type {number}
     * @memberof Order
     */
    orderDiscount?: number;
    /**
     * Order Tax
     * @type {number}
     * @memberof Order
     */
    orderTax?: number;
    /**
     * Total Price
     * @type {number}
     * @memberof Order
     */
    totalPrice?: number;
    /**
     * Order Notes
     * @type {string}
     * @memberof Order
     */
    orderNote?: string;
    /**
     * Order Gift note
     * @type {string}
     * @memberof Order
     */
    giftNote?: string;
    /**
     *
     * @type {Address}
     * @memberof Order
     */
    shippingAddress?: Address;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority
     * @type {string}
     * @memberof Order
     */
    shippingCarrier?: string;
    /**
     * Required with ShippingCarrier, 2nd Priority
     * @type {string}
     * @memberof Order
     */
    shippingClass?: string;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof Order
     */
    shipByDate?: Date;
    /**
     * Ship After Date
     * @type {Date}
     * @memberof Order
     */
    shipAfterDate?: Date;
    /**
     * First Priority to be respected
     * @type {Date}
     * @memberof Order
     */
    expectedDeliveryDate?: Date;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof Order
     */
    shippingNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof Order
     */
    incoterms?: string;
    /**
     *
     * @type {Address}
     * @memberof Order
     */
    billingAddress?: Address;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof Order
     */
    timestamp?: Date;
    /**
     * 3rd Priority
     * @type {string}
     * @memberof Order
     */
    shippingCode?: string;
    /**
     *
     * @type {OrderCustomer}
     * @memberof Order
     */
    customer?: OrderCustomer;
    /**
     * status of the payment
     * @type {string}
     * @memberof Order
     */
    paymentStatus?: string;
    /**
     * status of the fulfillment
     * @type {string}
     * @memberof Order
     */
    fulfillmentStatus?: string;
    /**
     * External System Order API ID
     * @type {string}
     * @memberof Order
     */
    extOrderApiId?: string;
    /**
     * When the order was created at source
     * @type {Date}
     * @memberof Order
     */
    extOrderCreatedAt?: Date;
    /**
     * When the order was updated at source
     * @type {Date}
     * @memberof Order
     */
    extOrderUpdatedAt?: Date;
    /**
     * Id of location defined in organization. Used as item origin for shipment.
     * @type {string}
     * @memberof Order
     */
    locationId?: string;
    /**
     * Shipping price
     * @type {number}
     * @memberof Order
     */
    shippingPrice?: number;
    /**
     * Indicates if fetching of shipping labels is required
     * @type {boolean}
     * @memberof Order
     */
    requireShippingLabels?: boolean;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof Order
     */
    customFields?: Array<NameValue>;
    /**
     * Order tags
     * @type {Array<string>}
     * @memberof Order
     */
    tags?: Array<string>;
    /**
     * Payments
     * @type {Array<Payment>}
     * @memberof Order
     */
    payments?: Array<Payment>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Order
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Order
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Order
     */
    readonly orgKey?: string;
}
export declare function OrderFromJSON(json: any): Order;
export declare function OrderFromJSONTyped(json: any, ignoreDiscriminator: boolean): Order;
export declare function OrderToJSON(value?: Order | null): any;
