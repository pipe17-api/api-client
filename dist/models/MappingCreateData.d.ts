/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MappingRule } from './';
/**
 *
 * @export
 * @interface MappingCreateData
 */
export interface MappingCreateData {
    /**
     * Public mapping indicator
     * @type {boolean}
     * @memberof MappingCreateData
     */
    isPublic: boolean;
    /**
     * Description
     * @type {string}
     * @memberof MappingCreateData
     */
    description: string;
    /**
     *
     * @type {Array<MappingRule>}
     * @memberof MappingCreateData
     */
    mappings?: Array<MappingRule>;
    /**
     * UUID (generated if not provided)
     * @type {string}
     * @memberof MappingCreateData
     */
    uuid?: string;
    /**
     *
     * @type {string}
     * @memberof MappingCreateData
     */
    originId?: string;
}
export declare function MappingCreateDataFromJSON(json: any): MappingCreateData;
export declare function MappingCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingCreateData;
export declare function MappingCreateDataToJSON(value?: MappingCreateData | null): any;
