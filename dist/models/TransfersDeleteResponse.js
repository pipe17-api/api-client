"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransfersDeleteResponseToJSON = exports.TransfersDeleteResponseFromJSONTyped = exports.TransfersDeleteResponseFromJSON = exports.TransfersDeleteResponseCodeEnum = exports.TransfersDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TransfersDeleteResponseSuccessEnum;
(function (TransfersDeleteResponseSuccessEnum) {
    TransfersDeleteResponseSuccessEnum["True"] = "true";
})(TransfersDeleteResponseSuccessEnum = exports.TransfersDeleteResponseSuccessEnum || (exports.TransfersDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TransfersDeleteResponseCodeEnum;
(function (TransfersDeleteResponseCodeEnum) {
    TransfersDeleteResponseCodeEnum[TransfersDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TransfersDeleteResponseCodeEnum[TransfersDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TransfersDeleteResponseCodeEnum[TransfersDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TransfersDeleteResponseCodeEnum = exports.TransfersDeleteResponseCodeEnum || (exports.TransfersDeleteResponseCodeEnum = {}));
function TransfersDeleteResponseFromJSON(json) {
    return TransfersDeleteResponseFromJSONTyped(json, false);
}
exports.TransfersDeleteResponseFromJSON = TransfersDeleteResponseFromJSON;
function TransfersDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TransfersDeleteFilterFromJSON(json['filters']),
        'transfers': !runtime_1.exists(json, 'transfers') ? undefined : (json['transfers'].map(_1.TransferFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.TransfersDeleteResponseFromJSONTyped = TransfersDeleteResponseFromJSONTyped;
function TransfersDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.TransfersDeleteFilterToJSON(value.filters),
        'transfers': value.transfers === undefined ? undefined : (value.transfers.map(_1.TransferToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.TransfersDeleteResponseToJSON = TransfersDeleteResponseToJSON;
