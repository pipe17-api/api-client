/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingsFilter
 */
export interface RoutingsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof RoutingsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof RoutingsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof RoutingsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof RoutingsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof RoutingsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof RoutingsFilter
     */
    count?: number;
    /**
     * Routings by list of routingId
     * @type {Array<string>}
     * @memberof RoutingsFilter
     */
    routingId?: Array<string>;
    /**
     * Routings by list of UUIDs
     * @type {Array<string>}
     * @memberof RoutingsFilter
     */
    uuid?: Array<string>;
    /**
     * Soft deleted routings
     * @type {boolean}
     * @memberof RoutingsFilter
     */
    deleted?: boolean;
}
export declare function RoutingsFilterFromJSON(json: any): RoutingsFilter;
export declare function RoutingsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingsFilter;
export declare function RoutingsFilterToJSON(value?: RoutingsFilter | null): any;
