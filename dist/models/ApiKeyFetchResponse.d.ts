/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKey } from './';
/**
 *
 * @export
 * @interface ApiKeyFetchResponse
 */
export interface ApiKeyFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ApiKeyFetchResponse
     */
    success: ApiKeyFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyFetchResponse
     */
    code: ApiKeyFetchResponseCodeEnum;
    /**
     *
     * @type {ApiKey}
     * @memberof ApiKeyFetchResponse
     */
    apikey?: ApiKey;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ApiKeyFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ApiKeyFetchResponseFromJSON(json: any): ApiKeyFetchResponse;
export declare function ApiKeyFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyFetchResponse;
export declare function ApiKeyFetchResponseToJSON(value?: ApiKeyFetchResponse | null): any;
