"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentUpdateErrorAllOfToJSON = exports.ShipmentUpdateErrorAllOfFromJSONTyped = exports.ShipmentUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentUpdateErrorAllOfFromJSON(json) {
    return ShipmentUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ShipmentUpdateErrorAllOfFromJSON = ShipmentUpdateErrorAllOfFromJSON;
function ShipmentUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'shipment': !runtime_1.exists(json, 'shipment') ? undefined : _1.ShipmentUpdateDataFromJSON(json['shipment']),
    };
}
exports.ShipmentUpdateErrorAllOfFromJSONTyped = ShipmentUpdateErrorAllOfFromJSONTyped;
function ShipmentUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'shipment': _1.ShipmentUpdateDataToJSON(value.shipment),
    };
}
exports.ShipmentUpdateErrorAllOfToJSON = ShipmentUpdateErrorAllOfToJSON;
