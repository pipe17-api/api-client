/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Refund, RefundsDeleteFilter } from './';
/**
 *
 * @export
 * @interface RefundsDeleteResponse
 */
export interface RefundsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RefundsDeleteResponse
     */
    success: RefundsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundsDeleteResponse
     */
    code: RefundsDeleteResponseCodeEnum;
    /**
     *
     * @type {RefundsDeleteFilter}
     * @memberof RefundsDeleteResponse
     */
    filters?: RefundsDeleteFilter;
    /**
     *
     * @type {Array<Refund>}
     * @memberof RefundsDeleteResponse
     */
    refunds?: Array<Refund>;
    /**
     * Number of deleted refunds
     * @type {number}
     * @memberof RefundsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof RefundsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RefundsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RefundsDeleteResponseFromJSON(json: any): RefundsDeleteResponse;
export declare function RefundsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundsDeleteResponse;
export declare function RefundsDeleteResponseToJSON(value?: RefundsDeleteResponse | null): any;
