/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Job } from './';
/**
 *
 * @export
 * @interface JobCreateResponseAllOf
 */
export interface JobCreateResponseAllOf {
    /**
     *
     * @type {Job}
     * @memberof JobCreateResponseAllOf
     */
    job?: Job;
}
export declare function JobCreateResponseAllOfFromJSON(json: any): JobCreateResponseAllOf;
export declare function JobCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobCreateResponseAllOf;
export declare function JobCreateResponseAllOfToJSON(value?: JobCreateResponseAllOf | null): any;
