"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobFetchResultErrorToJSON = exports.JobFetchResultErrorFromJSONTyped = exports.JobFetchResultErrorFromJSON = exports.JobFetchResultErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var JobFetchResultErrorSuccessEnum;
(function (JobFetchResultErrorSuccessEnum) {
    JobFetchResultErrorSuccessEnum["False"] = "false";
})(JobFetchResultErrorSuccessEnum = exports.JobFetchResultErrorSuccessEnum || (exports.JobFetchResultErrorSuccessEnum = {}));
function JobFetchResultErrorFromJSON(json) {
    return JobFetchResultErrorFromJSONTyped(json, false);
}
exports.JobFetchResultErrorFromJSON = JobFetchResultErrorFromJSON;
function JobFetchResultErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.JobFetchResultErrorFromJSONTyped = JobFetchResultErrorFromJSONTyped;
function JobFetchResultErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.JobFetchResultErrorToJSON = JobFetchResultErrorToJSON;
