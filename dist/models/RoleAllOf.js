"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleAllOfToJSON = exports.RoleAllOfFromJSONTyped = exports.RoleAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleAllOfFromJSON(json) {
    return RoleAllOfFromJSONTyped(json, false);
}
exports.RoleAllOfFromJSON = RoleAllOfFromJSON;
function RoleAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'roleId': !runtime_1.exists(json, 'roleId') ? undefined : json['roleId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.RoleStatusFromJSON(json['status']),
    };
}
exports.RoleAllOfFromJSONTyped = RoleAllOfFromJSONTyped;
function RoleAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'roleId': value.roleId,
        'status': _1.RoleStatusToJSON(value.status),
    };
}
exports.RoleAllOfToJSON = RoleAllOfToJSON;
