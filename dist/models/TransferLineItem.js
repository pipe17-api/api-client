"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferLineItemToJSON = exports.TransferLineItemFromJSONTyped = exports.TransferLineItemFromJSON = void 0;
const runtime_1 = require("../runtime");
function TransferLineItemFromJSON(json) {
    return TransferLineItemFromJSONTyped(json, false);
}
exports.TransferLineItemFromJSON = TransferLineItemFromJSON;
function TransferLineItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'uniqueId': json['uniqueId'],
        'quantity': json['quantity'],
        'sku': json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'shippedQuantity': !runtime_1.exists(json, 'shippedQuantity') ? undefined : json['shippedQuantity'],
        'receivedQuantity': !runtime_1.exists(json, 'receivedQuantity') ? undefined : json['receivedQuantity'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
    };
}
exports.TransferLineItemFromJSONTyped = TransferLineItemFromJSONTyped;
function TransferLineItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'uniqueId': value.uniqueId,
        'quantity': value.quantity,
        'sku': value.sku,
        'name': value.name,
        'upc': value.upc,
        'shippedQuantity': value.shippedQuantity,
        'receivedQuantity': value.receivedQuantity,
        'locationId': value.locationId,
    };
}
exports.TransferLineItemToJSON = TransferLineItemToJSON;
