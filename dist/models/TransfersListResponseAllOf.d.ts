/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Transfer, TransfersListFilter } from './';
/**
 *
 * @export
 * @interface TransfersListResponseAllOf
 */
export interface TransfersListResponseAllOf {
    /**
     *
     * @type {TransfersListFilter}
     * @memberof TransfersListResponseAllOf
     */
    filters?: TransfersListFilter;
    /**
     *
     * @type {Array<Transfer>}
     * @memberof TransfersListResponseAllOf
     */
    transfers: Array<Transfer>;
    /**
     *
     * @type {Pagination}
     * @memberof TransfersListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function TransfersListResponseAllOfFromJSON(json: any): TransfersListResponseAllOf;
export declare function TransfersListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersListResponseAllOf;
export declare function TransfersListResponseAllOfToJSON(value?: TransfersListResponseAllOf | null): any;
