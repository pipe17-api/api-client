"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingListResponseToJSON = exports.OrderRoutingListResponseFromJSONTyped = exports.OrderRoutingListResponseFromJSON = exports.OrderRoutingListResponseCodeEnum = exports.OrderRoutingListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrderRoutingListResponseSuccessEnum;
(function (OrderRoutingListResponseSuccessEnum) {
    OrderRoutingListResponseSuccessEnum["True"] = "true";
})(OrderRoutingListResponseSuccessEnum = exports.OrderRoutingListResponseSuccessEnum || (exports.OrderRoutingListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrderRoutingListResponseCodeEnum;
(function (OrderRoutingListResponseCodeEnum) {
    OrderRoutingListResponseCodeEnum[OrderRoutingListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrderRoutingListResponseCodeEnum[OrderRoutingListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrderRoutingListResponseCodeEnum[OrderRoutingListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrderRoutingListResponseCodeEnum = exports.OrderRoutingListResponseCodeEnum || (exports.OrderRoutingListResponseCodeEnum = {}));
function OrderRoutingListResponseFromJSON(json) {
    return OrderRoutingListResponseFromJSONTyped(json, false);
}
exports.OrderRoutingListResponseFromJSON = OrderRoutingListResponseFromJSON;
function OrderRoutingListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrderRoutingsListFilterFromJSON(json['filters']),
        'routings': !runtime_1.exists(json, 'routings') ? undefined : (json['routings'].map(_1.OrderRoutingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrderRoutingListResponseFromJSONTyped = OrderRoutingListResponseFromJSONTyped;
function OrderRoutingListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.OrderRoutingsListFilterToJSON(value.filters),
        'routings': value.routings === undefined ? undefined : (value.routings.map(_1.OrderRoutingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrderRoutingListResponseToJSON = OrderRoutingListResponseToJSON;
