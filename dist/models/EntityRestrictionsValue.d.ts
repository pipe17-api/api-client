/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityRestrictionsValue
 */
export interface EntityRestrictionsValue {
    /**
     *
     * @type {string}
     * @memberof EntityRestrictionsValue
     */
    path: EntityRestrictionsValuePathEnum;
    /**
     *
     * @type {string}
     * @memberof EntityRestrictionsValue
     */
    type?: EntityRestrictionsValueTypeEnum;
    /**
     *
     * @type {object}
     * @memberof EntityRestrictionsValue
     */
    value?: object;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityRestrictionsValuePathEnum {
    Restricted = "restricted",
    Since = "since",
    Until = "until",
    Content = "content"
} /**
* @export
* @enum {string}
*/
export declare enum EntityRestrictionsValueTypeEnum {
    Boolean = "boolean",
    Date = "date",
    Array = "array"
}
export declare function EntityRestrictionsValueFromJSON(json: any): EntityRestrictionsValue;
export declare function EntityRestrictionsValueFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityRestrictionsValue;
export declare function EntityRestrictionsValueToJSON(value?: EntityRestrictionsValue | null): any;
