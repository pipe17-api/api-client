/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Mapping Rule
 * @export
 * @interface MappingUpdateRule
 */
export interface MappingUpdateRule {
    /**
     * Mapping Rule Name
     * @type {string}
     * @memberof MappingUpdateRule
     */
    name?: string;
    /**
     * Is Mapping Rule Enabled
     * @type {boolean}
     * @memberof MappingUpdateRule
     */
    enabled?: boolean;
    /**
     * Mapping Rule Source
     * @type {string}
     * @memberof MappingUpdateRule
     */
    source?: string;
    /**
     * Mapping Rule Target
     * @type {string}
     * @memberof MappingUpdateRule
     */
    target?: string;
    /**
     * Mapping Rule Target Value Type
     * @type {string}
     * @memberof MappingUpdateRule
     */
    type?: MappingUpdateRuleTypeEnum;
    /**
     * Is Mapping Rule Source Required
     * @type {string}
     * @memberof MappingUpdateRule
     */
    require?: string;
    /**
     * Mapping Rule Source Value if Source is Missing
     * @type {string}
     * @memberof MappingUpdateRule
     */
    missing?: string;
    /**
     * Mapping Rule Source Default Value if Source is Missing
     * @type {string}
     * @memberof MappingUpdateRule
     */
    default?: string;
    /**
     * Mapping rule lookup definition
     * @type {object}
     * @memberof MappingUpdateRule
     */
    lookup?: object;
}
/**
* @export
* @enum {string}
*/
export declare enum MappingUpdateRuleTypeEnum {
    String = "string",
    Number = "number",
    Boolean = "boolean",
    Date = "date"
}
export declare function MappingUpdateRuleFromJSON(json: any): MappingUpdateRule;
export declare function MappingUpdateRuleFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingUpdateRule;
export declare function MappingUpdateRuleToJSON(value?: MappingUpdateRule | null): any;
