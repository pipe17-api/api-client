/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Tracking, TrackingFilter } from './';
/**
 *
 * @export
 * @interface TrackingListResponse
 */
export interface TrackingListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TrackingListResponse
     */
    success: TrackingListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingListResponse
     */
    code: TrackingListResponseCodeEnum;
    /**
     *
     * @type {TrackingFilter}
     * @memberof TrackingListResponse
     */
    filters?: TrackingFilter;
    /**
     *
     * @type {Array<Tracking>}
     * @memberof TrackingListResponse
     */
    trackings?: Array<Tracking>;
    /**
     *
     * @type {Pagination}
     * @memberof TrackingListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TrackingListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TrackingListResponseFromJSON(json: any): TrackingListResponse;
export declare function TrackingListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingListResponse;
export declare function TrackingListResponseToJSON(value?: TrackingListResponse | null): any;
