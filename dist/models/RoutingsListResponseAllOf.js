"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingsListResponseAllOfToJSON = exports.RoutingsListResponseAllOfFromJSONTyped = exports.RoutingsListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoutingsListResponseAllOfFromJSON(json) {
    return RoutingsListResponseAllOfFromJSONTyped(json, false);
}
exports.RoutingsListResponseAllOfFromJSON = RoutingsListResponseAllOfFromJSON;
function RoutingsListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RoutingsListFilterFromJSON(json['filters']),
        'routings': !runtime_1.exists(json, 'routings') ? undefined : (json['routings'].map(_1.RoutingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.RoutingsListResponseAllOfFromJSONTyped = RoutingsListResponseAllOfFromJSONTyped;
function RoutingsListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RoutingsListFilterToJSON(value.filters),
        'routings': value.routings === undefined ? undefined : (value.routings.map(_1.RoutingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.RoutingsListResponseAllOfToJSON = RoutingsListResponseAllOfToJSON;
