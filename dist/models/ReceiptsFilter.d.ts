/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReceiptsFilter
 */
export interface ReceiptsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ReceiptsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ReceiptsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ReceiptsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ReceiptsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ReceiptsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ReceiptsFilter
     */
    count?: number;
    /**
     * Receipts by list of receiptId
     * @type {Array<string>}
     * @memberof ReceiptsFilter
     */
    receiptId?: Array<string>;
    /**
     * Receipts by list of arrivalId
     * @type {Array<string>}
     * @memberof ReceiptsFilter
     */
    arrivalId?: Array<string>;
    /**
     * Receipts by list of extOrderId
     * @type {Array<string>}
     * @memberof ReceiptsFilter
     */
    extOrderId?: Array<string>;
}
export declare function ReceiptsFilterFromJSON(json: any): ReceiptsFilter;
export declare function ReceiptsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsFilter;
export declare function ReceiptsFilterToJSON(value?: ReceiptsFilter | null): any;
