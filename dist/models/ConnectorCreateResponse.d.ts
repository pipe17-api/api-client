/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Connector } from './';
/**
 *
 * @export
 * @interface ConnectorCreateResponse
 */
export interface ConnectorCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ConnectorCreateResponse
     */
    success: ConnectorCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ConnectorCreateResponse
     */
    code: ConnectorCreateResponseCodeEnum;
    /**
     *
     * @type {Connector}
     * @memberof ConnectorCreateResponse
     */
    connector?: Connector;
}
/**
* @export
* @enum {string}
*/
export declare enum ConnectorCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ConnectorCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ConnectorCreateResponseFromJSON(json: any): ConnectorCreateResponse;
export declare function ConnectorCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorCreateResponse;
export declare function ConnectorCreateResponseToJSON(value?: ConnectorCreateResponse | null): any;
