/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseStatus } from './';
/**
 *
 * @export
 * @interface PurchasesFilter
 */
export interface PurchasesFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof PurchasesFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof PurchasesFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof PurchasesFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof PurchasesFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof PurchasesFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof PurchasesFilter
     */
    count?: number;
    /**
     * Purchases by list of purchaseId
     * @type {Array<string>}
     * @memberof PurchasesFilter
     */
    purchaseId?: Array<string>;
    /**
     * Purchases by list of external purchase IDs
     * @type {Array<string>}
     * @memberof PurchasesFilter
     */
    extOrderId?: Array<string>;
    /**
     * Purchases by list of statuses
     * @type {Array<PurchaseStatus>}
     * @memberof PurchasesFilter
     */
    status?: Array<PurchaseStatus>;
    /**
     * Soft deleted purchases
     * @type {boolean}
     * @memberof PurchasesFilter
     */
    deleted?: boolean;
}
export declare function PurchasesFilterFromJSON(json: any): PurchasesFilter;
export declare function PurchasesFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesFilter;
export declare function PurchasesFilterToJSON(value?: PurchasesFilter | null): any;
