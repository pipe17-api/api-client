/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseCreateResult } from './';
/**
 *
 * @export
 * @interface PurchasesCreateError
 */
export interface PurchasesCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof PurchasesCreateError
     */
    success: PurchasesCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchasesCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof PurchasesCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof PurchasesCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {Array<PurchaseCreateResult>}
     * @memberof PurchasesCreateError
     */
    purchases?: Array<PurchaseCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchasesCreateErrorSuccessEnum {
    False = "false"
}
export declare function PurchasesCreateErrorFromJSON(json: any): PurchasesCreateError;
export declare function PurchasesCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesCreateError;
export declare function PurchasesCreateErrorToJSON(value?: PurchasesCreateError | null): any;
