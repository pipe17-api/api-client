/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { UserUpdateData } from './';
/**
 *
 * @export
 * @interface UserUpdateErrorAllOf
 */
export interface UserUpdateErrorAllOf {
    /**
     *
     * @type {UserUpdateData}
     * @memberof UserUpdateErrorAllOf
     */
    user?: UserUpdateData;
}
export declare function UserUpdateErrorAllOfFromJSON(json: any): UserUpdateErrorAllOf;
export declare function UserUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): UserUpdateErrorAllOf;
export declare function UserUpdateErrorAllOfToJSON(value?: UserUpdateErrorAllOf | null): any;
