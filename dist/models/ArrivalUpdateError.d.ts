/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalUpdateData } from './';
/**
 *
 * @export
 * @interface ArrivalUpdateError
 */
export interface ArrivalUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ArrivalUpdateError
     */
    success: ArrivalUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ArrivalUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ArrivalUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ArrivalUpdateData}
     * @memberof ArrivalUpdateError
     */
    arrival?: ArrivalUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalUpdateErrorSuccessEnum {
    False = "false"
}
export declare function ArrivalUpdateErrorFromJSON(json: any): ArrivalUpdateError;
export declare function ArrivalUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalUpdateError;
export declare function ArrivalUpdateErrorToJSON(value?: ArrivalUpdateError | null): any;
