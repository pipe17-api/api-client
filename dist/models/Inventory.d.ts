/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventType } from './';
/**
 *
 * @export
 * @interface Inventory
 */
export interface Inventory {
    /**
     * Inventory ID
     * @type {string}
     * @memberof Inventory
     */
    inventoryId?: string;
    /**
     * Related entity ID
     * @type {string}
     * @memberof Inventory
     */
    entityId?: string;
    /**
     * Related entity ID's type
     * @type {string}
     * @memberof Inventory
     */
    entityType?: string;
    /**
     *
     * @type {EventType}
     * @memberof Inventory
     */
    event?: EventType;
    /**
     * Item SKU
     * @type {string}
     * @memberof Inventory
     */
    sku?: string;
    /**
     * Location of item
     * @type {string}
     * @memberof Inventory
     */
    locationId?: string;
    /**
     * Number of items on hand
     * @type {number}
     * @memberof Inventory
     */
    onHand?: number;
    /**
     * Number of items committed
     * @type {number}
     * @memberof Inventory
     */
    committed?: number;
    /**
     * Number of items committed for pre-order
     * @type {number}
     * @memberof Inventory
     */
    committedFuture?: number;
    /**
     * Number of items that can be sold for pre-order
     * @type {number}
     * @memberof Inventory
     */
    future?: number;
    /**
     * Number of items available
     * @type {number}
     * @memberof Inventory
     */
    available?: number;
    /**
     * Number of items available to promise
     * @type {number}
     * @memberof Inventory
     */
    availableToPromise?: number;
    /**
     * Number of items affected
     * @type {number}
     * @memberof Inventory
     */
    quantity?: number;
    /**
     * Number of incoming items in transit (maybe included in available)
     * @type {number}
     * @memberof Inventory
     */
    incoming?: number;
    /**
     * Number of items being shipped (included in committed)
     * @type {number}
     * @memberof Inventory
     */
    commitShip?: number;
    /**
     * Number of items being transferred out (included in committed)
     * @type {number}
     * @memberof Inventory
     */
    commitXfer?: number;
    /**
     * Number of unavailable items
     * @type {number}
     * @memberof Inventory
     */
    unavailable?: number;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Inventory
     */
    integration?: string;
    /**
     * Inventory status (deleted or not)
     * @type {string}
     * @memberof Inventory
     */
    status?: string;
    /**
     * Pipe17 internal order id that initiated this inventory change
     * @type {string}
     * @memberof Inventory
     */
    orderId?: string;
    /**
     * Type of order that initiated this inventory change
     * @type {string}
     * @memberof Inventory
     */
    orderType?: string;
    /**
     * Pipe17 internal id of the user who makes the inventory adjustment via the portal
     * @type {string}
     * @memberof Inventory
     */
    user?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Inventory
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Inventory
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Inventory
     */
    readonly orgKey?: string;
}
export declare function InventoryFromJSON(json: any): Inventory;
export declare function InventoryFromJSONTyped(json: any, ignoreDiscriminator: boolean): Inventory;
export declare function InventoryToJSON(value?: Inventory | null): any;
