/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchaseStatus } from './';
/**
 *
 * @export
 * @interface PurchasesListFilter
 */
export interface PurchasesListFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof PurchasesListFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof PurchasesListFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof PurchasesListFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof PurchasesListFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof PurchasesListFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof PurchasesListFilter
     */
    count?: number;
    /**
     * Purchases by list of purchaseId
     * @type {Array<string>}
     * @memberof PurchasesListFilter
     */
    purchaseId?: Array<string>;
    /**
     * Purchases by list of external purchase IDs
     * @type {Array<string>}
     * @memberof PurchasesListFilter
     */
    extOrderId?: Array<string>;
    /**
     * Purchases by list of statuses
     * @type {Array<PurchaseStatus>}
     * @memberof PurchasesListFilter
     */
    status?: Array<PurchaseStatus>;
    /**
     * Soft deleted purchases
     * @type {boolean}
     * @memberof PurchasesListFilter
     */
    deleted?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof PurchasesListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof PurchasesListFilter
     */
    keys?: string;
    /**
     * if set to true use timestamp instead of create time
     * @type {boolean}
     * @memberof PurchasesListFilter
     */
    timestamp?: boolean;
}
export declare function PurchasesListFilterFromJSON(json: any): PurchasesListFilter;
export declare function PurchasesListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesListFilter;
export declare function PurchasesListFilterToJSON(value?: PurchasesListFilter | null): any;
