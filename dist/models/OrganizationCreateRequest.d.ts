/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationAddress, OrganizationServiceTier } from './';
/**
 *
 * @export
 * @interface OrganizationCreateRequest
 */
export interface OrganizationCreateRequest {
    /**
     * Organization Name
     * @type {string}
     * @memberof OrganizationCreateRequest
     */
    name: string;
    /**
     *
     * @type {OrganizationServiceTier}
     * @memberof OrganizationCreateRequest
     */
    tier: OrganizationServiceTier;
    /**
     * Organization Contact email
     * @type {string}
     * @memberof OrganizationCreateRequest
     */
    email: string;
    /**
     * Organization Contact Phone
     * @type {string}
     * @memberof OrganizationCreateRequest
     */
    phone: string;
    /**
     * Organization Time Zone
     * @type {string}
     * @memberof OrganizationCreateRequest
     */
    timeZone: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof OrganizationCreateRequest
     */
    logoUrl?: string;
    /**
     *
     * @type {OrganizationAddress}
     * @memberof OrganizationCreateRequest
     */
    address: OrganizationAddress;
}
export declare function OrganizationCreateRequestFromJSON(json: any): OrganizationCreateRequest;
export declare function OrganizationCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationCreateRequest;
export declare function OrganizationCreateRequestToJSON(value?: OrganizationCreateRequest | null): any;
