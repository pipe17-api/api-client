"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobCreateDataToJSON = exports.JobCreateDataFromJSONTyped = exports.JobCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobCreateDataFromJSON(json) {
    return JobCreateDataFromJSONTyped(json, false);
}
exports.JobCreateDataFromJSON = JobCreateDataFromJSON;
function JobCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'type': _1.JobTypeFromJSON(json['type']),
        'subType': _1.JobSubTypeFromJSON(json['subType']),
        'params': !runtime_1.exists(json, 'params') ? undefined : json['params'],
        'contentType': !runtime_1.exists(json, 'contentType') ? undefined : json['contentType'],
        'timeout': !runtime_1.exists(json, 'timeout') ? undefined : json['timeout'],
    };
}
exports.JobCreateDataFromJSONTyped = JobCreateDataFromJSONTyped;
function JobCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'type': _1.JobTypeToJSON(value.type),
        'subType': _1.JobSubTypeToJSON(value.subType),
        'params': value.params,
        'contentType': value.contentType,
        'timeout': value.timeout,
    };
}
exports.JobCreateDataToJSON = JobCreateDataToJSON;
