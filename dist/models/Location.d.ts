/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationAddress, LocationExternalSystem, LocationStatus } from './';
/**
 *
 * @export
 * @interface Location
 */
export interface Location {
    /**
     * Location ID
     * @type {string}
     * @memberof Location
     */
    locationId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Location
     */
    integration?: string;
    /**
     * Location Name
     * @type {string}
     * @memberof Location
     */
    name: string;
    /**
     *
     * @type {LocationAddress}
     * @memberof Location
     */
    address: LocationAddress;
    /**
     *
     * @type {LocationStatus}
     * @memberof Location
     */
    status?: LocationStatus;
    /**
     * Set if this location has \"infinite\" availability
     * @type {boolean}
     * @memberof Location
     */
    infinite?: boolean;
    /**
     * Set if shipments to this location should have lineItems referring to bundles kept intact
     * @type {boolean}
     * @memberof Location
     */
    preserveBundles?: boolean;
    /**
     * Set if this is a fulfillment location
     * @type {string}
     * @memberof Location
     */
    fulfillmentIntegrationId?: string;
    /**
     *
     * @type {Array<LocationExternalSystem>}
     * @memberof Location
     */
    externalSystem?: Array<LocationExternalSystem>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Location
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Location
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Location
     */
    readonly orgKey?: string;
}
export declare function LocationFromJSON(json: any): Location;
export declare function LocationFromJSONTyped(json: any, ignoreDiscriminator: boolean): Location;
export declare function LocationToJSON(value?: Location | null): any;
