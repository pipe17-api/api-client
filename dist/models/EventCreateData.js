"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventCreateDataToJSON = exports.EventCreateDataFromJSONTyped = exports.EventCreateDataFromJSON = exports.EventCreateDataOperationEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EventCreateDataOperationEnum;
(function (EventCreateDataOperationEnum) {
    EventCreateDataOperationEnum["Create"] = "create";
    EventCreateDataOperationEnum["Update"] = "update";
    EventCreateDataOperationEnum["Delete"] = "delete";
})(EventCreateDataOperationEnum = exports.EventCreateDataOperationEnum || (exports.EventCreateDataOperationEnum = {}));
function EventCreateDataFromJSON(json) {
    return EventCreateDataFromJSONTyped(json, false);
}
exports.EventCreateDataFromJSON = EventCreateDataFromJSON;
function EventCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'source': !runtime_1.exists(json, 'source') ? undefined : _1.EventRequestSourcePublicFromJSON(json['source']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.EventRequestStatusFromJSON(json['status']),
        'requestId': !runtime_1.exists(json, 'requestId') ? undefined : json['requestId'],
        'request': !runtime_1.exists(json, 'request') ? undefined : _1.EventRequestDataFromJSON(json['request']),
        'response': !runtime_1.exists(json, 'response') ? undefined : _1.EventResponseDataFromJSON(json['response']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'operation': !runtime_1.exists(json, 'operation') ? undefined : json['operation'],
        'entityType': !runtime_1.exists(json, 'entityType') ? undefined : json['entityType'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (new Date(json['timestamp'])),
        'duration': !runtime_1.exists(json, 'duration') ? undefined : json['duration'],
        'userId': !runtime_1.exists(json, 'userId') ? undefined : json['userId'],
    };
}
exports.EventCreateDataFromJSONTyped = EventCreateDataFromJSONTyped;
function EventCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'source': _1.EventRequestSourcePublicToJSON(value.source),
        'status': _1.EventRequestStatusToJSON(value.status),
        'requestId': value.requestId,
        'request': _1.EventRequestDataToJSON(value.request),
        'response': _1.EventResponseDataToJSON(value.response),
        'integration': value.integration,
        'operation': value.operation,
        'entityType': value.entityType,
        'entityId': value.entityId,
        'timestamp': value.timestamp === undefined ? undefined : (new Date(value.timestamp).toISOString()),
        'duration': value.duration,
        'userId': value.userId,
    };
}
exports.EventCreateDataToJSON = EventCreateDataToJSON;
