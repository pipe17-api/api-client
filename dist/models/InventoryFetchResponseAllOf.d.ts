/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Inventory } from './';
/**
 *
 * @export
 * @interface InventoryFetchResponseAllOf
 */
export interface InventoryFetchResponseAllOf {
    /**
     *
     * @type {Inventory}
     * @memberof InventoryFetchResponseAllOf
     */
    inventory?: Inventory;
}
export declare function InventoryFetchResponseAllOfFromJSON(json: any): InventoryFetchResponseAllOf;
export declare function InventoryFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryFetchResponseAllOf;
export declare function InventoryFetchResponseAllOfToJSON(value?: InventoryFetchResponseAllOf | null): any;
