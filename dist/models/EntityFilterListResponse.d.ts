/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityFilter, EntityFiltersListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface EntityFilterListResponse
 */
export interface EntityFilterListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntityFilterListResponse
     */
    success: EntityFilterListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterListResponse
     */
    code: EntityFilterListResponseCodeEnum;
    /**
     *
     * @type {EntityFiltersListFilter}
     * @memberof EntityFilterListResponse
     */
    filters?: EntityFiltersListFilter;
    /**
     *
     * @type {Array<EntityFilter>}
     * @memberof EntityFilterListResponse
     */
    entityFilters?: Array<EntityFilter>;
    /**
     *
     * @type {Pagination}
     * @memberof EntityFilterListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntityFilterListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntityFilterListResponseFromJSON(json: any): EntityFilterListResponse;
export declare function EntityFilterListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterListResponse;
export declare function EntityFilterListResponseToJSON(value?: EntityFilterListResponse | null): any;
