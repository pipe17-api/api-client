/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntitySchemaUpdateData
 */
export interface EntitySchemaUpdateData {
    /**
     * Schema name
     * @type {string}
     * @memberof EntitySchemaUpdateData
     */
    name?: string;
    /**
     * Entity schema field descriptors
     * @type {Array<object>}
     * @memberof EntitySchemaUpdateData
     */
    fields?: Array<object>;
}
export declare function EntitySchemaUpdateDataFromJSON(json: any): EntitySchemaUpdateData;
export declare function EntitySchemaUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaUpdateData;
export declare function EntitySchemaUpdateDataToJSON(value?: EntitySchemaUpdateData | null): any;
