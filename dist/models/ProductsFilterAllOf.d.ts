/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductStatus, ProductType } from './';
/**
 * Products Filter
 * @export
 * @interface ProductsFilterAllOf
 */
export interface ProductsFilterAllOf {
    /**
     * Products by list of productId
     * @type {Array<string>}
     * @memberof ProductsFilterAllOf
     */
    productId?: Array<string>;
    /**
     * Products by list of extProductId
     * @type {Array<string>}
     * @memberof ProductsFilterAllOf
     */
    extProductId?: Array<string>;
    /**
     * All variants of a parent products by list of external ids
     * @type {Array<string>}
     * @memberof ProductsFilterAllOf
     */
    parentExtProductId?: Array<string>;
    /**
     * Products with this status
     * @type {Array<ProductStatus>}
     * @memberof ProductsFilterAllOf
     */
    status?: Array<ProductStatus>;
    /**
     * Products matching specified types
     * @type {Array<ProductType>}
     * @memberof ProductsFilterAllOf
     */
    types?: Array<ProductType>;
    /**
     * Soft deleted products
     * @type {boolean}
     * @memberof ProductsFilterAllOf
     */
    deleted?: boolean;
}
export declare function ProductsFilterAllOfFromJSON(json: any): ProductsFilterAllOf;
export declare function ProductsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsFilterAllOf;
export declare function ProductsFilterAllOfToJSON(value?: ProductsFilterAllOf | null): any;
