/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationUpdateData } from './';
/**
 *
 * @export
 * @interface LocationUpdateError
 */
export interface LocationUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LocationUpdateError
     */
    success: LocationUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LocationUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LocationUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {LocationUpdateData}
     * @memberof LocationUpdateError
     */
    location?: LocationUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationUpdateErrorSuccessEnum {
    False = "false"
}
export declare function LocationUpdateErrorFromJSON(json: any): LocationUpdateError;
export declare function LocationUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationUpdateError;
export declare function LocationUpdateErrorToJSON(value?: LocationUpdateError | null): any;
