/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Integration } from './';
/**
 *
 * @export
 * @interface IntegrationFetchResponse
 */
export interface IntegrationFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof IntegrationFetchResponse
     */
    success: IntegrationFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof IntegrationFetchResponse
     */
    code: IntegrationFetchResponseCodeEnum;
    /**
     *
     * @type {Integration}
     * @memberof IntegrationFetchResponse
     */
    integration?: Integration;
}
/**
* @export
* @enum {string}
*/
export declare enum IntegrationFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum IntegrationFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function IntegrationFetchResponseFromJSON(json: any): IntegrationFetchResponse;
export declare function IntegrationFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationFetchResponse;
export declare function IntegrationFetchResponseToJSON(value?: IntegrationFetchResponse | null): any;
