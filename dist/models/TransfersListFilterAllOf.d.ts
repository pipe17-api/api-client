/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Transfers Filter
 * @export
 * @interface TransfersListFilterAllOf
 */
export interface TransfersListFilterAllOf {
    /**
     * if set to true use timestamp instead of create time
     * @type {boolean}
     * @memberof TransfersListFilterAllOf
     */
    timestamp?: boolean;
}
export declare function TransfersListFilterAllOfFromJSON(json: any): TransfersListFilterAllOf;
export declare function TransfersListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersListFilterAllOf;
export declare function TransfersListFilterAllOfToJSON(value?: TransfersListFilterAllOf | null): any;
