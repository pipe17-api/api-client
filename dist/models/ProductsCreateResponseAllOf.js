"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsCreateResponseAllOfToJSON = exports.ProductsCreateResponseAllOfFromJSONTyped = exports.ProductsCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductsCreateResponseAllOfFromJSON(json) {
    return ProductsCreateResponseAllOfFromJSONTyped(json, false);
}
exports.ProductsCreateResponseAllOfFromJSON = ProductsCreateResponseAllOfFromJSON;
function ProductsCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'products': !runtime_1.exists(json, 'products') ? undefined : (json['products'].map(_1.ProductCreateResultFromJSON)),
    };
}
exports.ProductsCreateResponseAllOfFromJSONTyped = ProductsCreateResponseAllOfFromJSONTyped;
function ProductsCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'products': value.products === undefined ? undefined : (value.products.map(_1.ProductCreateResultToJSON)),
    };
}
exports.ProductsCreateResponseAllOfToJSON = ProductsCreateResponseAllOfToJSON;
