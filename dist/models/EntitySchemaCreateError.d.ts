/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntitySchema } from './';
/**
 *
 * @export
 * @interface EntitySchemaCreateError
 */
export interface EntitySchemaCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EntitySchemaCreateError
     */
    success: EntitySchemaCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntitySchemaCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EntitySchemaCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EntitySchemaCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {EntitySchema}
     * @memberof EntitySchemaCreateError
     */
    entitySchema?: EntitySchema;
}
/**
* @export
* @enum {string}
*/
export declare enum EntitySchemaCreateErrorSuccessEnum {
    False = "false"
}
export declare function EntitySchemaCreateErrorFromJSON(json: any): EntitySchemaCreateError;
export declare function EntitySchemaCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemaCreateError;
export declare function EntitySchemaCreateErrorToJSON(value?: EntitySchemaCreateError | null): any;
