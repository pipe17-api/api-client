/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransfersListFilter } from './';
/**
 *
 * @export
 * @interface TransfersListErrorAllOf
 */
export interface TransfersListErrorAllOf {
    /**
     *
     * @type {TransfersListFilter}
     * @memberof TransfersListErrorAllOf
     */
    filters?: TransfersListFilter;
}
export declare function TransfersListErrorAllOfFromJSON(json: any): TransfersListErrorAllOf;
export declare function TransfersListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersListErrorAllOf;
export declare function TransfersListErrorAllOfToJSON(value?: TransfersListErrorAllOf | null): any;
