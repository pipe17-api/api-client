/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, NameValue, OrderCustomer, OrderLineItem, OrderStatus, Payment } from './';
/**
 *
 * @export
 * @interface OrderCreateData
 */
export interface OrderCreateData {
    /**
     *
     * @type {OrderStatus}
     * @memberof OrderCreateData
     */
    status?: OrderStatus;
    /**
     * External Order ID
     * @type {string}
     * @memberof OrderCreateData
     */
    extOrderId: string;
    /**
     * Order Url in External System
     * @type {string}
     * @memberof OrderCreateData
     */
    extOrderUrl?: string;
    /**
     * The original order platform, walmart, etsy, etc
     * @type {string}
     * @memberof OrderCreateData
     */
    orderSource?: string;
    /**
     *
     * @type {Array<OrderLineItem>}
     * @memberof OrderCreateData
     */
    lineItems?: Array<OrderLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof OrderCreateData
     */
    subTotalPrice?: number;
    /**
     * Order Discount
     * @type {number}
     * @memberof OrderCreateData
     */
    orderDiscount?: number;
    /**
     * Order Tax
     * @type {number}
     * @memberof OrderCreateData
     */
    orderTax?: number;
    /**
     * Total Price
     * @type {number}
     * @memberof OrderCreateData
     */
    totalPrice?: number;
    /**
     * Order Notes
     * @type {string}
     * @memberof OrderCreateData
     */
    orderNote?: string;
    /**
     * Order Gift note
     * @type {string}
     * @memberof OrderCreateData
     */
    giftNote?: string;
    /**
     *
     * @type {Address}
     * @memberof OrderCreateData
     */
    shippingAddress?: Address;
    /**
     * Required with ShippingClass, Pipe17 Approved List, 2nd Priority
     * @type {string}
     * @memberof OrderCreateData
     */
    shippingCarrier?: string;
    /**
     * Required with ShippingCarrier, 2nd Priority
     * @type {string}
     * @memberof OrderCreateData
     */
    shippingClass?: string;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof OrderCreateData
     */
    shipByDate?: Date;
    /**
     * Ship After Date
     * @type {Date}
     * @memberof OrderCreateData
     */
    shipAfterDate?: Date;
    /**
     * First Priority to be respected
     * @type {Date}
     * @memberof OrderCreateData
     */
    expectedDeliveryDate?: Date;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof OrderCreateData
     */
    shippingNote?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof OrderCreateData
     */
    incoterms?: string;
    /**
     *
     * @type {Address}
     * @memberof OrderCreateData
     */
    billingAddress?: Address;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof OrderCreateData
     */
    timestamp?: Date;
    /**
     * 3rd Priority
     * @type {string}
     * @memberof OrderCreateData
     */
    shippingCode?: string;
    /**
     *
     * @type {OrderCustomer}
     * @memberof OrderCreateData
     */
    customer?: OrderCustomer;
    /**
     * status of the payment
     * @type {string}
     * @memberof OrderCreateData
     */
    paymentStatus?: string;
    /**
     * status of the fulfillment
     * @type {string}
     * @memberof OrderCreateData
     */
    fulfillmentStatus?: string;
    /**
     * External System Order API ID
     * @type {string}
     * @memberof OrderCreateData
     */
    extOrderApiId?: string;
    /**
     * When the order was created at source
     * @type {Date}
     * @memberof OrderCreateData
     */
    extOrderCreatedAt?: Date;
    /**
     * When the order was updated at source
     * @type {Date}
     * @memberof OrderCreateData
     */
    extOrderUpdatedAt?: Date;
    /**
     * Id of location defined in organization. Used as item origin for shipment.
     * @type {string}
     * @memberof OrderCreateData
     */
    locationId?: string;
    /**
     * Shipping price
     * @type {number}
     * @memberof OrderCreateData
     */
    shippingPrice?: number;
    /**
     * Indicates if fetching of shipping labels is required
     * @type {boolean}
     * @memberof OrderCreateData
     */
    requireShippingLabels?: boolean;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof OrderCreateData
     */
    customFields?: Array<NameValue>;
    /**
     * Order tags
     * @type {Array<string>}
     * @memberof OrderCreateData
     */
    tags?: Array<string>;
    /**
     * Payments
     * @type {Array<Payment>}
     * @memberof OrderCreateData
     */
    payments?: Array<Payment>;
}
export declare function OrderCreateDataFromJSON(json: any): OrderCreateData;
export declare function OrderCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderCreateData;
export declare function OrderCreateDataToJSON(value?: OrderCreateData | null): any;
