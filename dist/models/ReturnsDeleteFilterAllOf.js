"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsDeleteFilterAllOfToJSON = exports.ReturnsDeleteFilterAllOfFromJSONTyped = exports.ReturnsDeleteFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ReturnsDeleteFilterAllOfFromJSON(json) {
    return ReturnsDeleteFilterAllOfFromJSONTyped(json, false);
}
exports.ReturnsDeleteFilterAllOfFromJSON = ReturnsDeleteFilterAllOfFromJSON;
function ReturnsDeleteFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.ReturnsDeleteFilterAllOfFromJSONTyped = ReturnsDeleteFilterAllOfFromJSONTyped;
function ReturnsDeleteFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': value.integration,
    };
}
exports.ReturnsDeleteFilterAllOfToJSON = ReturnsDeleteFilterAllOfToJSON;
