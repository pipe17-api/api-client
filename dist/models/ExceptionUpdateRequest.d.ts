/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ExceptionStatus } from './';
/**
 *
 * @export
 * @interface ExceptionUpdateRequest
 */
export interface ExceptionUpdateRequest {
    /**
     *
     * @type {ExceptionStatus}
     * @memberof ExceptionUpdateRequest
     */
    status: ExceptionStatus;
}
export declare function ExceptionUpdateRequestFromJSON(json: any): ExceptionUpdateRequest;
export declare function ExceptionUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionUpdateRequest;
export declare function ExceptionUpdateRequestToJSON(value?: ExceptionUpdateRequest | null): any;
