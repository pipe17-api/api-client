/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ProductUpdateResponse
 */
export interface ProductUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ProductUpdateResponse
     */
    success: ProductUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductUpdateResponse
     */
    code: ProductUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ProductUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ProductUpdateResponseFromJSON(json: any): ProductUpdateResponse;
export declare function ProductUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductUpdateResponse;
export declare function ProductUpdateResponseToJSON(value?: ProductUpdateResponse | null): any;
