"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReceiptFetchResponseToJSON = exports.ReceiptFetchResponseFromJSONTyped = exports.ReceiptFetchResponseFromJSON = exports.ReceiptFetchResponseCodeEnum = exports.ReceiptFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReceiptFetchResponseSuccessEnum;
(function (ReceiptFetchResponseSuccessEnum) {
    ReceiptFetchResponseSuccessEnum["True"] = "true";
})(ReceiptFetchResponseSuccessEnum = exports.ReceiptFetchResponseSuccessEnum || (exports.ReceiptFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReceiptFetchResponseCodeEnum;
(function (ReceiptFetchResponseCodeEnum) {
    ReceiptFetchResponseCodeEnum[ReceiptFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReceiptFetchResponseCodeEnum[ReceiptFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReceiptFetchResponseCodeEnum[ReceiptFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReceiptFetchResponseCodeEnum = exports.ReceiptFetchResponseCodeEnum || (exports.ReceiptFetchResponseCodeEnum = {}));
function ReceiptFetchResponseFromJSON(json) {
    return ReceiptFetchResponseFromJSONTyped(json, false);
}
exports.ReceiptFetchResponseFromJSON = ReceiptFetchResponseFromJSON;
function ReceiptFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'receipt': !runtime_1.exists(json, 'receipt') ? undefined : _1.ReceiptFromJSON(json['receipt']),
    };
}
exports.ReceiptFetchResponseFromJSONTyped = ReceiptFetchResponseFromJSONTyped;
function ReceiptFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'receipt': _1.ReceiptToJSON(value.receipt),
    };
}
exports.ReceiptFetchResponseToJSON = ReceiptFetchResponseToJSON;
