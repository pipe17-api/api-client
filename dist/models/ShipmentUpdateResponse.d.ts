/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ShipmentUpdateResponse
 */
export interface ShipmentUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ShipmentUpdateResponse
     */
    success: ShipmentUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentUpdateResponse
     */
    code: ShipmentUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ShipmentUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ShipmentUpdateResponseFromJSON(json: any): ShipmentUpdateResponse;
export declare function ShipmentUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentUpdateResponse;
export declare function ShipmentUpdateResponseToJSON(value?: ShipmentUpdateResponse | null): any;
