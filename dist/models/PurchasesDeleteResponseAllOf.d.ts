/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Purchase, PurchasesDeleteFilter } from './';
/**
 *
 * @export
 * @interface PurchasesDeleteResponseAllOf
 */
export interface PurchasesDeleteResponseAllOf {
    /**
     *
     * @type {PurchasesDeleteFilter}
     * @memberof PurchasesDeleteResponseAllOf
     */
    filters?: PurchasesDeleteFilter;
    /**
     *
     * @type {Array<Purchase>}
     * @memberof PurchasesDeleteResponseAllOf
     */
    purchases?: Array<Purchase>;
    /**
     * Number of deleted purchases
     * @type {number}
     * @memberof PurchasesDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof PurchasesDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function PurchasesDeleteResponseAllOfFromJSON(json: any): PurchasesDeleteResponseAllOf;
export declare function PurchasesDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesDeleteResponseAllOf;
export declare function PurchasesDeleteResponseAllOfToJSON(value?: PurchasesDeleteResponseAllOf | null): any;
