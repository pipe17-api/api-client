/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Exception, ExceptionsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ExceptionsListResponse
 */
export interface ExceptionsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ExceptionsListResponse
     */
    success: ExceptionsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ExceptionsListResponse
     */
    code: ExceptionsListResponseCodeEnum;
    /**
     *
     * @type {ExceptionsListFilter}
     * @memberof ExceptionsListResponse
     */
    filters?: ExceptionsListFilter;
    /**
     *
     * @type {Array<Exception>}
     * @memberof ExceptionsListResponse
     */
    exceptions?: Array<Exception>;
    /**
     *
     * @type {Pagination}
     * @memberof ExceptionsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ExceptionsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ExceptionsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ExceptionsListResponseFromJSON(json: any): ExceptionsListResponse;
export declare function ExceptionsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionsListResponse;
export declare function ExceptionsListResponseToJSON(value?: ExceptionsListResponse | null): any;
