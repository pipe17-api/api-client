"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoutingUpdateErrorAllOfToJSON = exports.RoutingUpdateErrorAllOfFromJSONTyped = exports.RoutingUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoutingUpdateErrorAllOfFromJSON(json) {
    return RoutingUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.RoutingUpdateErrorAllOfFromJSON = RoutingUpdateErrorAllOfFromJSON;
function RoutingUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'routing': !runtime_1.exists(json, 'routing') ? undefined : _1.RoutingUpdateDataFromJSON(json['routing']),
    };
}
exports.RoutingUpdateErrorAllOfFromJSONTyped = RoutingUpdateErrorAllOfFromJSONTyped;
function RoutingUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'routing': _1.RoutingUpdateDataToJSON(value.routing),
    };
}
exports.RoutingUpdateErrorAllOfToJSON = RoutingUpdateErrorAllOfToJSON;
