/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * List Suppliers Filter
 * @export
 * @interface SuppliersListFilterAllOf
 */
export interface SuppliersListFilterAllOf {
    /**
     * Retrieve suppliers matching name string
     * @type {string}
     * @memberof SuppliersListFilterAllOf
     */
    name?: string;
}
export declare function SuppliersListFilterAllOfFromJSON(json: any): SuppliersListFilterAllOf;
export declare function SuppliersListFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersListFilterAllOf;
export declare function SuppliersListFilterAllOfToJSON(value?: SuppliersListFilterAllOf | null): any;
