/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Organization } from './';
/**
 *
 * @export
 * @interface OrganizationFetchResponse
 */
export interface OrganizationFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrganizationFetchResponse
     */
    success: OrganizationFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrganizationFetchResponse
     */
    code: OrganizationFetchResponseCodeEnum;
    /**
     *
     * @type {Organization}
     * @memberof OrganizationFetchResponse
     */
    organization?: Organization;
}
/**
* @export
* @enum {string}
*/
export declare enum OrganizationFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrganizationFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrganizationFetchResponseFromJSON(json: any): OrganizationFetchResponse;
export declare function OrganizationFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationFetchResponse;
export declare function OrganizationFetchResponseToJSON(value?: OrganizationFetchResponse | null): any;
