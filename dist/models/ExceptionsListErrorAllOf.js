"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionsListErrorAllOfToJSON = exports.ExceptionsListErrorAllOfFromJSONTyped = exports.ExceptionsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionsListErrorAllOfFromJSON(json) {
    return ExceptionsListErrorAllOfFromJSONTyped(json, false);
}
exports.ExceptionsListErrorAllOfFromJSON = ExceptionsListErrorAllOfFromJSON;
function ExceptionsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ExceptionsListFilterFromJSON(json['filters']),
    };
}
exports.ExceptionsListErrorAllOfFromJSONTyped = ExceptionsListErrorAllOfFromJSONTyped;
function ExceptionsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ExceptionsListFilterToJSON(value.filters),
    };
}
exports.ExceptionsListErrorAllOfToJSON = ExceptionsListErrorAllOfToJSON;
