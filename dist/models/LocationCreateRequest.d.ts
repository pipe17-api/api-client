/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationAddress, LocationExternalSystem, LocationStatus } from './';
/**
 *
 * @export
 * @interface LocationCreateRequest
 */
export interface LocationCreateRequest {
    /**
     * Location Name
     * @type {string}
     * @memberof LocationCreateRequest
     */
    name: string;
    /**
     *
     * @type {LocationAddress}
     * @memberof LocationCreateRequest
     */
    address: LocationAddress;
    /**
     *
     * @type {LocationStatus}
     * @memberof LocationCreateRequest
     */
    status?: LocationStatus;
    /**
     * Set if this location has \"infinite\" availability
     * @type {boolean}
     * @memberof LocationCreateRequest
     */
    infinite?: boolean;
    /**
     * Set if shipments to this location should have lineItems referring to bundles kept intact
     * @type {boolean}
     * @memberof LocationCreateRequest
     */
    preserveBundles?: boolean;
    /**
     * Set if this is a fulfillment location
     * @type {string}
     * @memberof LocationCreateRequest
     */
    fulfillmentIntegrationId?: string;
    /**
     *
     * @type {Array<LocationExternalSystem>}
     * @memberof LocationCreateRequest
     */
    externalSystem?: Array<LocationExternalSystem>;
}
export declare function LocationCreateRequestFromJSON(json: any): LocationCreateRequest;
export declare function LocationCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationCreateRequest;
export declare function LocationCreateRequestToJSON(value?: LocationCreateRequest | null): any;
