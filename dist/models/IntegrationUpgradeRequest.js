"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationUpgradeRequestToJSON = exports.IntegrationUpgradeRequestFromJSONTyped = exports.IntegrationUpgradeRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
function IntegrationUpgradeRequestFromJSON(json) {
    return IntegrationUpgradeRequestFromJSONTyped(json, false);
}
exports.IntegrationUpgradeRequestFromJSON = IntegrationUpgradeRequestFromJSON;
function IntegrationUpgradeRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'version': !runtime_1.exists(json, 'version') ? undefined : json['version'],
    };
}
exports.IntegrationUpgradeRequestFromJSONTyped = IntegrationUpgradeRequestFromJSONTyped;
function IntegrationUpgradeRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'version': value.version,
    };
}
exports.IntegrationUpgradeRequestToJSON = IntegrationUpgradeRequestToJSON;
