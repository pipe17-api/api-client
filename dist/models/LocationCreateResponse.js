"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationCreateResponseToJSON = exports.LocationCreateResponseFromJSONTyped = exports.LocationCreateResponseFromJSON = exports.LocationCreateResponseCodeEnum = exports.LocationCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationCreateResponseSuccessEnum;
(function (LocationCreateResponseSuccessEnum) {
    LocationCreateResponseSuccessEnum["True"] = "true";
})(LocationCreateResponseSuccessEnum = exports.LocationCreateResponseSuccessEnum || (exports.LocationCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LocationCreateResponseCodeEnum;
(function (LocationCreateResponseCodeEnum) {
    LocationCreateResponseCodeEnum[LocationCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LocationCreateResponseCodeEnum[LocationCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LocationCreateResponseCodeEnum[LocationCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LocationCreateResponseCodeEnum = exports.LocationCreateResponseCodeEnum || (exports.LocationCreateResponseCodeEnum = {}));
function LocationCreateResponseFromJSON(json) {
    return LocationCreateResponseFromJSONTyped(json, false);
}
exports.LocationCreateResponseFromJSON = LocationCreateResponseFromJSON;
function LocationCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationFromJSON(json['location']),
    };
}
exports.LocationCreateResponseFromJSONTyped = LocationCreateResponseFromJSONTyped;
function LocationCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'location': _1.LocationToJSON(value.location),
    };
}
exports.LocationCreateResponseToJSON = LocationCreateResponseToJSON;
