"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUpdateErrorAllOfToJSON = exports.UserUpdateErrorAllOfFromJSONTyped = exports.UserUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function UserUpdateErrorAllOfFromJSON(json) {
    return UserUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.UserUpdateErrorAllOfFromJSON = UserUpdateErrorAllOfFromJSON;
function UserUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'user': !runtime_1.exists(json, 'user') ? undefined : _1.UserUpdateDataFromJSON(json['user']),
    };
}
exports.UserUpdateErrorAllOfFromJSONTyped = UserUpdateErrorAllOfFromJSONTyped;
function UserUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'user': _1.UserUpdateDataToJSON(value.user),
    };
}
exports.UserUpdateErrorAllOfToJSON = UserUpdateErrorAllOfToJSON;
