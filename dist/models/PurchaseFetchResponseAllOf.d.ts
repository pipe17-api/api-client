/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Purchase } from './';
/**
 *
 * @export
 * @interface PurchaseFetchResponseAllOf
 */
export interface PurchaseFetchResponseAllOf {
    /**
     *
     * @type {Purchase}
     * @memberof PurchaseFetchResponseAllOf
     */
    purchase?: Purchase;
}
export declare function PurchaseFetchResponseAllOfFromJSON(json: any): PurchaseFetchResponseAllOf;
export declare function PurchaseFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseFetchResponseAllOf;
export declare function PurchaseFetchResponseAllOfToJSON(value?: PurchaseFetchResponseAllOf | null): any;
