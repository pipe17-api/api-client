/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Event Type
 * @export
 * @enum {string}
 */
export declare enum EventRequestSourcePublic {
    Custom = "custom"
}
export declare function EventRequestSourcePublicFromJSON(json: any): EventRequestSourcePublic;
export declare function EventRequestSourcePublicFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventRequestSourcePublic;
export declare function EventRequestSourcePublicToJSON(value?: EventRequestSourcePublic | null): any;
