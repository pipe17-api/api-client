"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderAllOfToJSON = exports.OrderAllOfFromJSONTyped = exports.OrderAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrderAllOfFromJSON(json) {
    return OrderAllOfFromJSONTyped(json, false);
}
exports.OrderAllOfFromJSON = OrderAllOfFromJSON;
function OrderAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.OrderStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.OrderAllOfFromJSONTyped = OrderAllOfFromJSONTyped;
function OrderAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'orderId': value.orderId,
        'status': _1.OrderStatusToJSON(value.status),
        'integration': value.integration,
    };
}
exports.OrderAllOfToJSON = OrderAllOfToJSON;
