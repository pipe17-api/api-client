/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ReturnsDeleteErrorAllOf
 */
export interface ReturnsDeleteErrorAllOf {
    /**
     *
     * @type {ReturnsDeleteFilter}
     * @memberof ReturnsDeleteErrorAllOf
     */
    filters?: ReturnsDeleteFilter;
}
export declare function ReturnsDeleteErrorAllOfFromJSON(json: any): ReturnsDeleteErrorAllOf;
export declare function ReturnsDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsDeleteErrorAllOf;
export declare function ReturnsDeleteErrorAllOfToJSON(value?: ReturnsDeleteErrorAllOf | null): any;
