/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ReturnFetchError
 */
export interface ReturnFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ReturnFetchError
     */
    success: ReturnFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ReturnFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ReturnFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnFetchErrorSuccessEnum {
    False = "false"
}
export declare function ReturnFetchErrorFromJSON(json: any): ReturnFetchError;
export declare function ReturnFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnFetchError;
export declare function ReturnFetchErrorToJSON(value?: ReturnFetchError | null): any;
