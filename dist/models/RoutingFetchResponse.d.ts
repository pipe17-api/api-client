/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Routing } from './';
/**
 *
 * @export
 * @interface RoutingFetchResponse
 */
export interface RoutingFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RoutingFetchResponse
     */
    success: RoutingFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RoutingFetchResponse
     */
    code: RoutingFetchResponseCodeEnum;
    /**
     *
     * @type {Routing}
     * @memberof RoutingFetchResponse
     */
    routing?: Routing;
}
/**
* @export
* @enum {string}
*/
export declare enum RoutingFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RoutingFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RoutingFetchResponseFromJSON(json: any): RoutingFetchResponse;
export declare function RoutingFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingFetchResponse;
export declare function RoutingFetchResponseToJSON(value?: RoutingFetchResponse | null): any;
