"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryCreateResultToJSON = exports.InventoryCreateResultFromJSONTyped = exports.InventoryCreateResultFromJSON = exports.InventoryCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var InventoryCreateResultStatusEnum;
(function (InventoryCreateResultStatusEnum) {
    InventoryCreateResultStatusEnum["Submitted"] = "submitted";
    InventoryCreateResultStatusEnum["Failed"] = "failed";
})(InventoryCreateResultStatusEnum = exports.InventoryCreateResultStatusEnum || (exports.InventoryCreateResultStatusEnum = {}));
function InventoryCreateResultFromJSON(json) {
    return InventoryCreateResultFromJSONTyped(json, false);
}
exports.InventoryCreateResultFromJSON = InventoryCreateResultFromJSON;
function InventoryCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'inventory': !runtime_1.exists(json, 'inventory') ? undefined : _1.InventoryFromJSON(json['inventory']),
    };
}
exports.InventoryCreateResultFromJSONTyped = InventoryCreateResultFromJSONTyped;
function InventoryCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'message': value.message,
        'errors': value.errors,
        'inventory': _1.InventoryToJSON(value.inventory),
    };
}
exports.InventoryCreateResultToJSON = InventoryCreateResultToJSON;
