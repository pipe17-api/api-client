"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingCreateResponseToJSON = exports.MappingCreateResponseFromJSONTyped = exports.MappingCreateResponseFromJSON = exports.MappingCreateResponseCodeEnum = exports.MappingCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var MappingCreateResponseSuccessEnum;
(function (MappingCreateResponseSuccessEnum) {
    MappingCreateResponseSuccessEnum["True"] = "true";
})(MappingCreateResponseSuccessEnum = exports.MappingCreateResponseSuccessEnum || (exports.MappingCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var MappingCreateResponseCodeEnum;
(function (MappingCreateResponseCodeEnum) {
    MappingCreateResponseCodeEnum[MappingCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    MappingCreateResponseCodeEnum[MappingCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    MappingCreateResponseCodeEnum[MappingCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(MappingCreateResponseCodeEnum = exports.MappingCreateResponseCodeEnum || (exports.MappingCreateResponseCodeEnum = {}));
function MappingCreateResponseFromJSON(json) {
    return MappingCreateResponseFromJSONTyped(json, false);
}
exports.MappingCreateResponseFromJSON = MappingCreateResponseFromJSON;
function MappingCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'mapping': !runtime_1.exists(json, 'mapping') ? undefined : _1.MappingFromJSON(json['mapping']),
    };
}
exports.MappingCreateResponseFromJSONTyped = MappingCreateResponseFromJSONTyped;
function MappingCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'mapping': _1.MappingToJSON(value.mapping),
    };
}
exports.MappingCreateResponseToJSON = MappingCreateResponseToJSON;
