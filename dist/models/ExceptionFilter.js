"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterToJSON = exports.ExceptionFilterFromJSONTyped = exports.ExceptionFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFilterFromJSON(json) {
    return ExceptionFilterFromJSONTyped(json, false);
}
exports.ExceptionFilterFromJSON = ExceptionFilterFromJSON;
function ExceptionFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filterId': !runtime_1.exists(json, 'filterId') ? undefined : json['filterId'],
        'exceptionType': _1.ExceptionTypeFromJSON(json['exceptionType']),
        'enabled': !runtime_1.exists(json, 'enabled') ? undefined : json['enabled'],
        'blocking': !runtime_1.exists(json, 'blocking') ? undefined : json['blocking'],
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'settings': !runtime_1.exists(json, 'settings') ? undefined : json['settings'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.ExceptionFilterFromJSONTyped = ExceptionFilterFromJSONTyped;
function ExceptionFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filterId': value.filterId,
        'exceptionType': _1.ExceptionTypeToJSON(value.exceptionType),
        'enabled': value.enabled,
        'blocking': value.blocking,
        'isPublic': value.isPublic,
        'settings': value.settings,
        'originId': value.originId,
    };
}
exports.ExceptionFilterToJSON = ExceptionFilterToJSON;
