/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestData, EventRequestSourcePublic, EventRequestStatus, EventResponseData } from './';
/**
 *
 * @export
 * @interface Event
 */
export interface Event {
    /**
     * Event ID
     * @type {string}
     * @memberof Event
     */
    eventId?: string;
    /**
     *
     * @type {EventRequestSourcePublic}
     * @memberof Event
     */
    source?: EventRequestSourcePublic;
    /**
     *
     * @type {EventRequestStatus}
     * @memberof Event
     */
    status?: EventRequestStatus;
    /**
     * API request Id
     * @type {string}
     * @memberof Event
     */
    requestId?: string;
    /**
     *
     * @type {EventRequestData}
     * @memberof Event
     */
    request?: EventRequestData;
    /**
     *
     * @type {EventResponseData}
     * @memberof Event
     */
    response?: EventResponseData;
    /**
     * integration id
     * @type {string}
     * @memberof Event
     */
    integration?: string;
    /**
     * operation
     * @type {string}
     * @memberof Event
     */
    operation?: EventOperationEnum;
    /**
     * entity type
     * @type {string}
     * @memberof Event
     */
    entityType?: string;
    /**
     * entityId
     * @type {string}
     * @memberof Event
     */
    entityId?: string;
    /**
     * When the event was triggered
     * @type {Date}
     * @memberof Event
     */
    timestamp?: Date;
    /**
     * operation duration
     * @type {number}
     * @memberof Event
     */
    duration?: number;
    /**
     * API user id
     * @type {string}
     * @memberof Event
     */
    userId?: string;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Event
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Event
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Event
     */
    readonly orgKey?: string;
}
/**
* @export
* @enum {string}
*/
export declare enum EventOperationEnum {
    Create = "create",
    Update = "update",
    Delete = "delete"
}
export declare function EventFromJSON(json: any): Event;
export declare function EventFromJSONTyped(json: any, ignoreDiscriminator: boolean): Event;
export declare function EventToJSON(value?: Event | null): any;
