/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingAllOf
 */
export interface OrderRoutingAllOf {
    /**
     * Routing ID
     * @type {string}
     * @memberof OrderRoutingAllOf
     */
    routingId?: string;
}
export declare function OrderRoutingAllOfFromJSON(json: any): OrderRoutingAllOf;
export declare function OrderRoutingAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingAllOf;
export declare function OrderRoutingAllOfToJSON(value?: OrderRoutingAllOf | null): any;
