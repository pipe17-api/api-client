/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Exception Filters Filter
 * @export
 * @interface ExceptionFiltersFilterAllOf
 */
export interface ExceptionFiltersFilterAllOf {
    /**
     * Filters by filterId-list
     * @type {Array<string>}
     * @memberof ExceptionFiltersFilterAllOf
     */
    filterId?: Array<string>;
    /**
     * Filters by exceptionType-list
     * @type {Array<string>}
     * @memberof ExceptionFiltersFilterAllOf
     */
    exceptionType?: Array<string>;
}
export declare function ExceptionFiltersFilterAllOfFromJSON(json: any): ExceptionFiltersFilterAllOf;
export declare function ExceptionFiltersFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ExceptionFiltersFilterAllOf;
export declare function ExceptionFiltersFilterAllOfToJSON(value?: ExceptionFiltersFilterAllOf | null): any;
