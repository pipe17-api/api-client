"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobCreateResponseToJSON = exports.JobCreateResponseFromJSONTyped = exports.JobCreateResponseFromJSON = exports.JobCreateResponseCodeEnum = exports.JobCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var JobCreateResponseSuccessEnum;
(function (JobCreateResponseSuccessEnum) {
    JobCreateResponseSuccessEnum["True"] = "true";
})(JobCreateResponseSuccessEnum = exports.JobCreateResponseSuccessEnum || (exports.JobCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var JobCreateResponseCodeEnum;
(function (JobCreateResponseCodeEnum) {
    JobCreateResponseCodeEnum[JobCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    JobCreateResponseCodeEnum[JobCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    JobCreateResponseCodeEnum[JobCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(JobCreateResponseCodeEnum = exports.JobCreateResponseCodeEnum || (exports.JobCreateResponseCodeEnum = {}));
function JobCreateResponseFromJSON(json) {
    return JobCreateResponseFromJSONTyped(json, false);
}
exports.JobCreateResponseFromJSON = JobCreateResponseFromJSON;
function JobCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'job': !runtime_1.exists(json, 'job') ? undefined : _1.JobFromJSON(json['job']),
    };
}
exports.JobCreateResponseFromJSONTyped = JobCreateResponseFromJSONTyped;
function JobCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'job': _1.JobToJSON(value.job),
    };
}
exports.JobCreateResponseToJSON = JobCreateResponseToJSON;
