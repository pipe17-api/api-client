"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationCreateResponseAllOfToJSON = exports.IntegrationCreateResponseAllOfFromJSONTyped = exports.IntegrationCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationCreateResponseAllOfFromJSON(json) {
    return IntegrationCreateResponseAllOfFromJSONTyped(json, false);
}
exports.IntegrationCreateResponseAllOfFromJSON = IntegrationCreateResponseAllOfFromJSON;
function IntegrationCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integration': !runtime_1.exists(json, 'integration') ? undefined : _1.IntegrationFromJSON(json['integration']),
    };
}
exports.IntegrationCreateResponseAllOfFromJSONTyped = IntegrationCreateResponseAllOfFromJSONTyped;
function IntegrationCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integration': _1.IntegrationToJSON(value.integration),
    };
}
exports.IntegrationCreateResponseAllOfToJSON = IntegrationCreateResponseAllOfToJSON;
