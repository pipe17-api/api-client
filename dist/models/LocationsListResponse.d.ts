/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location, LocationsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface LocationsListResponse
 */
export interface LocationsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof LocationsListResponse
     */
    success: LocationsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LocationsListResponse
     */
    code: LocationsListResponseCodeEnum;
    /**
     *
     * @type {LocationsListFilter}
     * @memberof LocationsListResponse
     */
    filters?: LocationsListFilter;
    /**
     *
     * @type {Array<Location>}
     * @memberof LocationsListResponse
     */
    locations?: Array<Location>;
    /**
     *
     * @type {Pagination}
     * @memberof LocationsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum LocationsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum LocationsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function LocationsListResponseFromJSON(json: any): LocationsListResponse;
export declare function LocationsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsListResponse;
export declare function LocationsListResponseToJSON(value?: LocationsListResponse | null): any;
