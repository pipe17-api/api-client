/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyValueAllOf
 */
export interface ApiKeyValueAllOf {
    /**
     * API key Id
     * @type {string}
     * @memberof ApiKeyValueAllOf
     */
    apikeyId?: string;
    /**
     * API key value
     * @type {string}
     * @memberof ApiKeyValueAllOf
     */
    apikey?: string;
}
export declare function ApiKeyValueAllOfFromJSON(json: any): ApiKeyValueAllOf;
export declare function ApiKeyValueAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyValueAllOf;
export declare function ApiKeyValueAllOfToJSON(value?: ApiKeyValueAllOf | null): any;
