/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface Success
 */
export interface Success {
    /**
     * Always true
     * @type {boolean}
     * @memberof Success
     */
    success: SuccessSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof Success
     */
    code: SuccessCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum SuccessSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum SuccessCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function SuccessFromJSON(json: any): Success;
export declare function SuccessFromJSONTyped(json: any, ignoreDiscriminator: boolean): Success;
export declare function SuccessToJSON(value?: Success | null): any;
