/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LocationAllOf
 */
export interface LocationAllOf {
    /**
     * Location ID
     * @type {string}
     * @memberof LocationAllOf
     */
    locationId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof LocationAllOf
     */
    integration?: string;
}
export declare function LocationAllOfFromJSON(json: any): LocationAllOf;
export declare function LocationAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationAllOf;
export declare function LocationAllOfToJSON(value?: LocationAllOf | null): any;
