/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptCreateResult } from './';
/**
 *
 * @export
 * @interface ReceiptsCreateResponse
 */
export interface ReceiptsCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReceiptsCreateResponse
     */
    success: ReceiptsCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReceiptsCreateResponse
     */
    code: ReceiptsCreateResponseCodeEnum;
    /**
     *
     * @type {Array<ReceiptCreateResult>}
     * @memberof ReceiptsCreateResponse
     */
    receipts?: Array<ReceiptCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ReceiptsCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReceiptsCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReceiptsCreateResponseFromJSON(json: any): ReceiptsCreateResponse;
export declare function ReceiptsCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsCreateResponse;
export declare function ReceiptsCreateResponseToJSON(value?: ReceiptsCreateResponse | null): any;
