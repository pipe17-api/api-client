"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundUpdateErrorAllOfToJSON = exports.RefundUpdateErrorAllOfFromJSONTyped = exports.RefundUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundUpdateErrorAllOfFromJSON(json) {
    return RefundUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.RefundUpdateErrorAllOfFromJSON = RefundUpdateErrorAllOfFromJSON;
function RefundUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'refund': !runtime_1.exists(json, 'refund') ? undefined : _1.RefundUpdateDataFromJSON(json['refund']),
    };
}
exports.RefundUpdateErrorAllOfFromJSONTyped = RefundUpdateErrorAllOfFromJSONTyped;
function RefundUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'refund': _1.RefundUpdateDataToJSON(value.refund),
    };
}
exports.RefundUpdateErrorAllOfToJSON = RefundUpdateErrorAllOfToJSON;
