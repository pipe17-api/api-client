/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RefundCreateData } from './';
/**
 *
 * @export
 * @interface RefundCreateErrorAllOf
 */
export interface RefundCreateErrorAllOf {
    /**
     *
     * @type {RefundCreateData}
     * @memberof RefundCreateErrorAllOf
     */
    refund?: RefundCreateData;
}
export declare function RefundCreateErrorAllOfFromJSON(json: any): RefundCreateErrorAllOf;
export declare function RefundCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundCreateErrorAllOf;
export declare function RefundCreateErrorAllOfToJSON(value?: RefundCreateErrorAllOf | null): any;
