/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Fulfillment } from './';
/**
 *
 * @export
 * @interface FulfillmentCreateResult
 */
export interface FulfillmentCreateResult {
    /**
     *
     * @type {string}
     * @memberof FulfillmentCreateResult
     */
    status?: FulfillmentCreateResultStatusEnum;
    /**
     * HTTP status code
     * @type {number}
     * @memberof FulfillmentCreateResult
     */
    code?: number;
    /**
     * Error message if failed
     * @type {string}
     * @memberof FulfillmentCreateResult
     */
    message?: string;
    /**
     * Error details if failed
     * @type {Array<string>}
     * @memberof FulfillmentCreateResult
     */
    errors?: Array<string>;
    /**
     *
     * @type {Fulfillment}
     * @memberof FulfillmentCreateResult
     */
    fulfillment?: Fulfillment;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentCreateResultStatusEnum {
    Submitted = "submitted",
    Failed = "failed"
}
export declare function FulfillmentCreateResultFromJSON(json: any): FulfillmentCreateResult;
export declare function FulfillmentCreateResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentCreateResult;
export declare function FulfillmentCreateResultToJSON(value?: FulfillmentCreateResult | null): any;
