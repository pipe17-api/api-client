"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateListErrorAllOfToJSON = exports.IntegrationStateListErrorAllOfFromJSONTyped = exports.IntegrationStateListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationStateListErrorAllOfFromJSON(json) {
    return IntegrationStateListErrorAllOfFromJSONTyped(json, false);
}
exports.IntegrationStateListErrorAllOfFromJSON = IntegrationStateListErrorAllOfFromJSON;
function IntegrationStateListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationStateListFilterFromJSON(json['filters']),
    };
}
exports.IntegrationStateListErrorAllOfFromJSONTyped = IntegrationStateListErrorAllOfFromJSONTyped;
function IntegrationStateListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.IntegrationStateListFilterToJSON(value.filters),
    };
}
exports.IntegrationStateListErrorAllOfToJSON = IntegrationStateListErrorAllOfToJSON;
