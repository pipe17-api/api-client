/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Webhook } from './';
/**
 *
 * @export
 * @interface WebhookCreateResponseAllOf
 */
export interface WebhookCreateResponseAllOf {
    /**
     *
     * @type {Webhook}
     * @memberof WebhookCreateResponseAllOf
     */
    webhook?: Webhook;
}
export declare function WebhookCreateResponseAllOfFromJSON(json: any): WebhookCreateResponseAllOf;
export declare function WebhookCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookCreateResponseAllOf;
export declare function WebhookCreateResponseAllOfToJSON(value?: WebhookCreateResponseAllOf | null): any;
