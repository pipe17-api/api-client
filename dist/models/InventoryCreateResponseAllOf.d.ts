/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryCreateResult } from './';
/**
 *
 * @export
 * @interface InventoryCreateResponseAllOf
 */
export interface InventoryCreateResponseAllOf {
    /**
     *
     * @type {Array<InventoryCreateResult>}
     * @memberof InventoryCreateResponseAllOf
     */
    inventory?: Array<InventoryCreateResult>;
}
export declare function InventoryCreateResponseAllOfFromJSON(json: any): InventoryCreateResponseAllOf;
export declare function InventoryCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryCreateResponseAllOf;
export declare function InventoryCreateResponseAllOfToJSON(value?: InventoryCreateResponseAllOf | null): any;
