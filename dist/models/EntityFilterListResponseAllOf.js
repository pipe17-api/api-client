"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterListResponseAllOfToJSON = exports.EntityFilterListResponseAllOfFromJSONTyped = exports.EntityFilterListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function EntityFilterListResponseAllOfFromJSON(json) {
    return EntityFilterListResponseAllOfFromJSONTyped(json, false);
}
exports.EntityFilterListResponseAllOfFromJSON = EntityFilterListResponseAllOfFromJSON;
function EntityFilterListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntityFiltersListFilterFromJSON(json['filters']),
        'entityFilters': !runtime_1.exists(json, 'entityFilters') ? undefined : (json['entityFilters'].map(_1.EntityFilterFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.EntityFilterListResponseAllOfFromJSONTyped = EntityFilterListResponseAllOfFromJSONTyped;
function EntityFilterListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.EntityFiltersListFilterToJSON(value.filters),
        'entityFilters': value.entityFilters === undefined ? undefined : (value.entityFilters.map(_1.EntityFilterToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.EntityFilterListResponseAllOfToJSON = EntityFilterListResponseAllOfToJSON;
