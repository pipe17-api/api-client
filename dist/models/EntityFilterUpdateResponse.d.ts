/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EntityFilterUpdateResponse
 */
export interface EntityFilterUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof EntityFilterUpdateResponse
     */
    success: EntityFilterUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EntityFilterUpdateResponse
     */
    code: EntityFilterUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum EntityFilterUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum EntityFilterUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function EntityFilterUpdateResponseFromJSON(json: any): EntityFilterUpdateResponse;
export declare function EntityFilterUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntityFilterUpdateResponse;
export declare function EntityFilterUpdateResponseToJSON(value?: EntityFilterUpdateResponse | null): any;
