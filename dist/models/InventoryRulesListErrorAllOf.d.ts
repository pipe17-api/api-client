/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRulesListFilter } from './';
/**
 *
 * @export
 * @interface InventoryRulesListErrorAllOf
 */
export interface InventoryRulesListErrorAllOf {
    /**
     *
     * @type {InventoryRulesListFilter}
     * @memberof InventoryRulesListErrorAllOf
     */
    filters?: InventoryRulesListFilter;
}
export declare function InventoryRulesListErrorAllOfFromJSON(json: any): InventoryRulesListErrorAllOf;
export declare function InventoryRulesListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesListErrorAllOf;
export declare function InventoryRulesListErrorAllOfToJSON(value?: InventoryRulesListErrorAllOf | null): any;
