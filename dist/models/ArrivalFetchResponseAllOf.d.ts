/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Arrival } from './';
/**
 *
 * @export
 * @interface ArrivalFetchResponseAllOf
 */
export interface ArrivalFetchResponseAllOf {
    /**
     *
     * @type {Arrival}
     * @memberof ArrivalFetchResponseAllOf
     */
    arrival?: Arrival;
}
export declare function ArrivalFetchResponseAllOfFromJSON(json: any): ArrivalFetchResponseAllOf;
export declare function ArrivalFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalFetchResponseAllOf;
export declare function ArrivalFetchResponseAllOfToJSON(value?: ArrivalFetchResponseAllOf | null): any;
