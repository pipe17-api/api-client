"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelCreateDataToJSON = exports.LabelCreateDataFromJSONTyped = exports.LabelCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function LabelCreateDataFromJSON(json) {
    return LabelCreateDataFromJSONTyped(json, false);
}
exports.LabelCreateDataFromJSON = LabelCreateDataFromJSON;
function LabelCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fileName': json['fileName'],
        'contentType': json['contentType'],
        'trackingNumber': !runtime_1.exists(json, 'trackingNumber') ? undefined : json['trackingNumber'],
        'shippingMethod': !runtime_1.exists(json, 'shippingMethod') ? undefined : json['shippingMethod'],
    };
}
exports.LabelCreateDataFromJSONTyped = LabelCreateDataFromJSONTyped;
function LabelCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fileName': value.fileName,
        'contentType': value.contentType,
        'trackingNumber': value.trackingNumber,
        'shippingMethod': value.shippingMethod,
    };
}
exports.LabelCreateDataToJSON = LabelCreateDataToJSON;
