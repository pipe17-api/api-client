"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderFetchResponseAllOfToJSON = exports.OrderFetchResponseAllOfFromJSONTyped = exports.OrderFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrderFetchResponseAllOfFromJSON(json) {
    return OrderFetchResponseAllOfFromJSONTyped(json, false);
}
exports.OrderFetchResponseAllOfFromJSON = OrderFetchResponseAllOfFromJSON;
function OrderFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'order': !runtime_1.exists(json, 'order') ? undefined : _1.OrderFromJSON(json['order']),
    };
}
exports.OrderFetchResponseAllOfFromJSONTyped = OrderFetchResponseAllOfFromJSONTyped;
function OrderFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'order': _1.OrderToJSON(value.order),
    };
}
exports.OrderFetchResponseAllOfToJSON = OrderFetchResponseAllOfToJSON;
