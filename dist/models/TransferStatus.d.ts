/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Transfer Status
 * @export
 * @enum {string}
 */
export declare enum TransferStatus {
    Draft = "draft",
    New = "new",
    OnHold = "onHold",
    ToBeValidated = "toBeValidated",
    ReviewRequired = "reviewRequired",
    ReadyForFulfillment = "readyForFulfillment",
    SentToFulfillment = "sentToFulfillment",
    PartialFulfillment = "partialFulfillment",
    Fulfilled = "fulfilled",
    InTransit = "inTransit",
    PartialReceived = "partialReceived",
    Received = "received",
    Canceled = "canceled",
    Returned = "returned",
    Refunded = "refunded"
}
export declare function TransferStatusFromJSON(json: any): TransferStatus;
export declare function TransferStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferStatus;
export declare function TransferStatusToJSON(value?: TransferStatus | null): any;
