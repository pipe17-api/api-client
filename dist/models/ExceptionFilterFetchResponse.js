"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionFilterFetchResponseToJSON = exports.ExceptionFilterFetchResponseFromJSONTyped = exports.ExceptionFilterFetchResponseFromJSON = exports.ExceptionFilterFetchResponseCodeEnum = exports.ExceptionFilterFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ExceptionFilterFetchResponseSuccessEnum;
(function (ExceptionFilterFetchResponseSuccessEnum) {
    ExceptionFilterFetchResponseSuccessEnum["True"] = "true";
})(ExceptionFilterFetchResponseSuccessEnum = exports.ExceptionFilterFetchResponseSuccessEnum || (exports.ExceptionFilterFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ExceptionFilterFetchResponseCodeEnum;
(function (ExceptionFilterFetchResponseCodeEnum) {
    ExceptionFilterFetchResponseCodeEnum[ExceptionFilterFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ExceptionFilterFetchResponseCodeEnum[ExceptionFilterFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ExceptionFilterFetchResponseCodeEnum[ExceptionFilterFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ExceptionFilterFetchResponseCodeEnum = exports.ExceptionFilterFetchResponseCodeEnum || (exports.ExceptionFilterFetchResponseCodeEnum = {}));
function ExceptionFilterFetchResponseFromJSON(json) {
    return ExceptionFilterFetchResponseFromJSONTyped(json, false);
}
exports.ExceptionFilterFetchResponseFromJSON = ExceptionFilterFetchResponseFromJSON;
function ExceptionFilterFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'exceptionFilter': !runtime_1.exists(json, 'exceptionFilter') ? undefined : _1.ExceptionFilterFromJSON(json['exceptionFilter']),
    };
}
exports.ExceptionFilterFetchResponseFromJSONTyped = ExceptionFilterFetchResponseFromJSONTyped;
function ExceptionFilterFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'exceptionFilter': _1.ExceptionFilterToJSON(value.exceptionFilter),
    };
}
exports.ExceptionFilterFetchResponseToJSON = ExceptionFilterFetchResponseToJSON;
