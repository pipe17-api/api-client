"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.NotImplementedToJSON = exports.NotImplementedFromJSONTyped = exports.NotImplementedFromJSON = exports.NotImplementedCodeEnum = exports.NotImplementedSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var NotImplementedSuccessEnum;
(function (NotImplementedSuccessEnum) {
    NotImplementedSuccessEnum["True"] = "true";
})(NotImplementedSuccessEnum = exports.NotImplementedSuccessEnum || (exports.NotImplementedSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var NotImplementedCodeEnum;
(function (NotImplementedCodeEnum) {
    NotImplementedCodeEnum[NotImplementedCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    NotImplementedCodeEnum[NotImplementedCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    NotImplementedCodeEnum[NotImplementedCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(NotImplementedCodeEnum = exports.NotImplementedCodeEnum || (exports.NotImplementedCodeEnum = {}));
function NotImplementedFromJSON(json) {
    return NotImplementedFromJSONTyped(json, false);
}
exports.NotImplementedFromJSON = NotImplementedFromJSON;
function NotImplementedFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
    };
}
exports.NotImplementedFromJSONTyped = NotImplementedFromJSONTyped;
function NotImplementedToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
    };
}
exports.NotImplementedToJSON = NotImplementedToJSON;
