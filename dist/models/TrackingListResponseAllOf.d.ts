/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Tracking, TrackingFilter } from './';
/**
 *
 * @export
 * @interface TrackingListResponseAllOf
 */
export interface TrackingListResponseAllOf {
    /**
     *
     * @type {TrackingFilter}
     * @memberof TrackingListResponseAllOf
     */
    filters?: TrackingFilter;
    /**
     *
     * @type {Array<Tracking>}
     * @memberof TrackingListResponseAllOf
     */
    trackings?: Array<Tracking>;
    /**
     *
     * @type {Pagination}
     * @memberof TrackingListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function TrackingListResponseAllOfFromJSON(json: any): TrackingListResponseAllOf;
export declare function TrackingListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingListResponseAllOf;
export declare function TrackingListResponseAllOfToJSON(value?: TrackingListResponseAllOf | null): any;
