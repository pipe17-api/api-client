/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobStatus, JobSubType, JobType } from './';
/**
 * Jobs Filter
 * @export
 * @interface JobsFilterAllOf
 */
export interface JobsFilterAllOf {
    /**
     * Jobs by list of jobId
     * @type {Array<string>}
     * @memberof JobsFilterAllOf
     */
    jobId?: Array<string>;
    /**
     * Jobs with given type
     * @type {Array<JobType>}
     * @memberof JobsFilterAllOf
     */
    type?: Array<JobType>;
    /**
     * Jobs with given sub-type
     * @type {Array<JobSubType>}
     * @memberof JobsFilterAllOf
     */
    subType?: Array<JobSubType>;
    /**
     * Jobs with given status
     * @type {Array<JobStatus>}
     * @memberof JobsFilterAllOf
     */
    status?: Array<JobStatus>;
}
export declare function JobsFilterAllOfFromJSON(json: any): JobsFilterAllOf;
export declare function JobsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobsFilterAllOf;
export declare function JobsFilterAllOfToJSON(value?: JobsFilterAllOf | null): any;
