/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface WebhooksListFilter
 */
export interface WebhooksListFilter {
    /**
     * Webhooks by list of webhookId
     * @type {Array<string>}
     * @memberof WebhooksListFilter
     */
    webhookId?: Array<string>;
    /**
     *
     * @type {WebhookTopics}
     * @memberof WebhooksListFilter
     */
    topic?: WebhookTopics;
    /**
     * Include related webhooks
     * @type {boolean}
     * @memberof WebhooksListFilter
     */
    related?: boolean;
    /**
     * List sort order
     * @type {string}
     * @memberof WebhooksListFilter
     */
    order?: string;
    /**
     * Fields to retrieve
     * @type {string}
     * @memberof WebhooksListFilter
     */
    keys?: string;
}
export declare function WebhooksListFilterFromJSON(json: any): WebhooksListFilter;
export declare function WebhooksListFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhooksListFilter;
export declare function WebhooksListFilterToJSON(value?: WebhooksListFilter | null): any;
