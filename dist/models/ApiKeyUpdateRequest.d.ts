/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { MethodsUpdateData } from './';
/**
 *
 * @export
 * @interface ApiKeyUpdateRequest
 */
export interface ApiKeyUpdateRequest {
    /**
     * API Key Name
     * @type {string}
     * @memberof ApiKeyUpdateRequest
     */
    name?: string;
    /**
     * API Key Description
     * @type {string}
     * @memberof ApiKeyUpdateRequest
     */
    description?: string;
    /**
     *
     * @type {MethodsUpdateData}
     * @memberof ApiKeyUpdateRequest
     */
    methods?: MethodsUpdateData;
    /**
     * IP/CIDR Whitelist
     * @type {Array<string>}
     * @memberof ApiKeyUpdateRequest
     */
    allowedIPs?: Array<string>;
}
export declare function ApiKeyUpdateRequestFromJSON(json: any): ApiKeyUpdateRequest;
export declare function ApiKeyUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyUpdateRequest;
export declare function ApiKeyUpdateRequestToJSON(value?: ApiKeyUpdateRequest | null): any;
