/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelsDeleteFilter
 */
export interface LabelsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof LabelsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof LabelsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof LabelsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof LabelsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof LabelsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof LabelsDeleteFilter
     */
    count?: number;
    /**
     * Labels by list of labelId
     * @type {Array<string>}
     * @memberof LabelsDeleteFilter
     */
    labelId?: Array<string>;
    /**
     * Soft deleted labels
     * @type {boolean}
     * @memberof LabelsDeleteFilter
     */
    deleted?: boolean;
}
export declare function LabelsDeleteFilterFromJSON(json: any): LabelsDeleteFilter;
export declare function LabelsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelsDeleteFilter;
export declare function LabelsDeleteFilterToJSON(value?: LabelsDeleteFilter | null): any;
