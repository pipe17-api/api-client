/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentsListFilter } from './';
/**
 *
 * @export
 * @interface FulfillmentsListErrorAllOf
 */
export interface FulfillmentsListErrorAllOf {
    /**
     *
     * @type {FulfillmentsListFilter}
     * @memberof FulfillmentsListErrorAllOf
     */
    filters?: FulfillmentsListFilter;
}
export declare function FulfillmentsListErrorAllOfFromJSON(json: any): FulfillmentsListErrorAllOf;
export declare function FulfillmentsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsListErrorAllOf;
export declare function FulfillmentsListErrorAllOfToJSON(value?: FulfillmentsListErrorAllOf | null): any;
