/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TransferFetchError
 */
export interface TransferFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof TransferFetchError
     */
    success: TransferFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TransferFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof TransferFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof TransferFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum TransferFetchErrorSuccessEnum {
    False = "false"
}
export declare function TransferFetchErrorFromJSON(json: any): TransferFetchError;
export declare function TransferFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferFetchError;
export declare function TransferFetchErrorToJSON(value?: TransferFetchError | null): any;
