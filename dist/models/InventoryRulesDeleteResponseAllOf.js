"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRulesDeleteResponseAllOfToJSON = exports.InventoryRulesDeleteResponseAllOfFromJSONTyped = exports.InventoryRulesDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryRulesDeleteResponseAllOfFromJSON(json) {
    return InventoryRulesDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.InventoryRulesDeleteResponseAllOfFromJSON = InventoryRulesDeleteResponseAllOfFromJSON;
function InventoryRulesDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.InventoryRulesDeleteFilterFromJSON(json['filters']),
        'rules': !runtime_1.exists(json, 'rules') ? undefined : (json['rules'].map(_1.InventoryRuleFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.InventoryRulesDeleteResponseAllOfFromJSONTyped = InventoryRulesDeleteResponseAllOfFromJSONTyped;
function InventoryRulesDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.InventoryRulesDeleteFilterToJSON(value.filters),
        'rules': value.rules === undefined ? undefined : (value.rules.map(_1.InventoryRuleToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.InventoryRulesDeleteResponseAllOfToJSON = InventoryRulesDeleteResponseAllOfToJSON;
