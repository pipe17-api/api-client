/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Return, ReturnsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ReturnsDeleteResponse
 */
export interface ReturnsDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ReturnsDeleteResponse
     */
    success: ReturnsDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ReturnsDeleteResponse
     */
    code: ReturnsDeleteResponseCodeEnum;
    /**
     *
     * @type {ReturnsDeleteFilter}
     * @memberof ReturnsDeleteResponse
     */
    filters?: ReturnsDeleteFilter;
    /**
     *
     * @type {Array<Return>}
     * @memberof ReturnsDeleteResponse
     */
    suppliers?: Array<Return>;
    /**
     * Number of deleted suppliers
     * @type {number}
     * @memberof ReturnsDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof ReturnsDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ReturnsDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ReturnsDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ReturnsDeleteResponseFromJSON(json: any): ReturnsDeleteResponse;
export declare function ReturnsDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsDeleteResponse;
export declare function ReturnsDeleteResponseToJSON(value?: ReturnsDeleteResponse | null): any;
