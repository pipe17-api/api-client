"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersDeleteResponseToJSON = exports.OrdersDeleteResponseFromJSONTyped = exports.OrdersDeleteResponseFromJSON = exports.OrdersDeleteResponseCodeEnum = exports.OrdersDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrdersDeleteResponseSuccessEnum;
(function (OrdersDeleteResponseSuccessEnum) {
    OrdersDeleteResponseSuccessEnum["True"] = "true";
})(OrdersDeleteResponseSuccessEnum = exports.OrdersDeleteResponseSuccessEnum || (exports.OrdersDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrdersDeleteResponseCodeEnum;
(function (OrdersDeleteResponseCodeEnum) {
    OrdersDeleteResponseCodeEnum[OrdersDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrdersDeleteResponseCodeEnum[OrdersDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrdersDeleteResponseCodeEnum[OrdersDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrdersDeleteResponseCodeEnum = exports.OrdersDeleteResponseCodeEnum || (exports.OrdersDeleteResponseCodeEnum = {}));
function OrdersDeleteResponseFromJSON(json) {
    return OrdersDeleteResponseFromJSONTyped(json, false);
}
exports.OrdersDeleteResponseFromJSON = OrdersDeleteResponseFromJSON;
function OrdersDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.OrdersDeleteFilterFromJSON(json['filters']),
        'orders': !runtime_1.exists(json, 'orders') ? undefined : (json['orders'].map(_1.OrderFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.OrdersDeleteResponseFromJSONTyped = OrdersDeleteResponseFromJSONTyped;
function OrdersDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.OrdersDeleteFilterToJSON(value.filters),
        'orders': value.orders === undefined ? undefined : (value.orders.map(_1.OrderToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.OrdersDeleteResponseToJSON = OrdersDeleteResponseToJSON;
