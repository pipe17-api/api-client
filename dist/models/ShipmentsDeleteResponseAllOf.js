"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShipmentsDeleteResponseAllOfToJSON = exports.ShipmentsDeleteResponseAllOfFromJSONTyped = exports.ShipmentsDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ShipmentsDeleteResponseAllOfFromJSON(json) {
    return ShipmentsDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.ShipmentsDeleteResponseAllOfFromJSON = ShipmentsDeleteResponseAllOfFromJSON;
function ShipmentsDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ShipmentsDeleteFilterFromJSON(json['filters']),
        'shipments': !runtime_1.exists(json, 'shipments') ? undefined : (json['shipments'].map(_1.ShipmentFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ShipmentsDeleteResponseAllOfFromJSONTyped = ShipmentsDeleteResponseAllOfFromJSONTyped;
function ShipmentsDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ShipmentsDeleteFilterToJSON(value.filters),
        'shipments': value.shipments === undefined ? undefined : (value.shipments.map(_1.ShipmentToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ShipmentsDeleteResponseAllOfToJSON = ShipmentsDeleteResponseAllOfToJSON;
