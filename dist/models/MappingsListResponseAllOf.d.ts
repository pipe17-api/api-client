/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Mapping, MappingsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface MappingsListResponseAllOf
 */
export interface MappingsListResponseAllOf {
    /**
     *
     * @type {MappingsListFilter}
     * @memberof MappingsListResponseAllOf
     */
    filters?: MappingsListFilter;
    /**
     *
     * @type {Array<Mapping>}
     * @memberof MappingsListResponseAllOf
     */
    mappings?: Array<Mapping>;
    /**
     *
     * @type {Pagination}
     * @memberof MappingsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function MappingsListResponseAllOfFromJSON(json: any): MappingsListResponseAllOf;
export declare function MappingsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): MappingsListResponseAllOf;
export declare function MappingsListResponseAllOfToJSON(value?: MappingsListResponseAllOf | null): any;
