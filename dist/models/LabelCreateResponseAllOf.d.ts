/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Label } from './';
/**
 *
 * @export
 * @interface LabelCreateResponseAllOf
 */
export interface LabelCreateResponseAllOf {
    /**
     *
     * @type {Label}
     * @memberof LabelCreateResponseAllOf
     */
    label?: Label;
}
export declare function LabelCreateResponseAllOfFromJSON(json: any): LabelCreateResponseAllOf;
export declare function LabelCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelCreateResponseAllOf;
export declare function LabelCreateResponseAllOfToJSON(value?: LabelCreateResponseAllOf | null): any;
