/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationStatus } from './';
/**
 *
 * @export
 * @interface LocationsFilter
 */
export interface LocationsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof LocationsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof LocationsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof LocationsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof LocationsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof LocationsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof LocationsFilter
     */
    count?: number;
    /**
     * Locations by list of locationId
     * @type {Array<string>}
     * @memberof LocationsFilter
     */
    locationId?: Array<string>;
    /**
     *
     * @type {Array<LocationStatus>}
     * @memberof LocationsFilter
     */
    status?: Array<LocationStatus>;
    /**
     * Soft deleted locations
     * @type {boolean}
     * @memberof LocationsFilter
     */
    deleted?: boolean;
}
export declare function LocationsFilterFromJSON(json: any): LocationsFilter;
export declare function LocationsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsFilter;
export declare function LocationsFilterToJSON(value?: LocationsFilter | null): any;
