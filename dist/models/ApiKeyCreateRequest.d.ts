/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Env, Methods, Tier } from './';
/**
 *
 * @export
 * @interface ApiKeyCreateRequest
 */
export interface ApiKeyCreateRequest {
    /**
     * API Key Name
     * @type {string}
     * @memberof ApiKeyCreateRequest
     */
    name: string;
    /**
     * API Key Description
     * @type {string}
     * @memberof ApiKeyCreateRequest
     */
    description?: string;
    /**
     * Connector ID
     * @type {string}
     * @memberof ApiKeyCreateRequest
     */
    connector?: string;
    /**
     * Integration ID
     * @type {string}
     * @memberof ApiKeyCreateRequest
     */
    integration?: string;
    /**
     *
     * @type {Tier}
     * @memberof ApiKeyCreateRequest
     */
    tier?: Tier;
    /**
     *
     * @type {Env}
     * @memberof ApiKeyCreateRequest
     */
    environment?: Env;
    /**
     *
     * @type {Methods}
     * @memberof ApiKeyCreateRequest
     */
    methods: Methods;
    /**
     * IP/CIDR Whitelist
     * @type {Array<string>}
     * @memberof ApiKeyCreateRequest
     */
    allowedIPs: Array<string>;
}
export declare function ApiKeyCreateRequestFromJSON(json: any): ApiKeyCreateRequest;
export declare function ApiKeyCreateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyCreateRequest;
export declare function ApiKeyCreateRequestToJSON(value?: ApiKeyCreateRequest | null): any;
