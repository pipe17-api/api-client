"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FailureToJSON = exports.FailureFromJSONTyped = exports.FailureFromJSON = exports.FailureSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var FailureSuccessEnum;
(function (FailureSuccessEnum) {
    FailureSuccessEnum["False"] = "false";
})(FailureSuccessEnum = exports.FailureSuccessEnum || (exports.FailureSuccessEnum = {}));
function FailureFromJSON(json) {
    return FailureFromJSONTyped(json, false);
}
exports.FailureFromJSON = FailureFromJSON;
function FailureFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.FailureFromJSONTyped = FailureFromJSONTyped;
function FailureToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.FailureToJSON = FailureToJSON;
