/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName, ExceptionStatus, ExceptionType } from './';
/**
 *
 * @export
 * @interface Exception
 */
export interface Exception {
    /**
     * Exception ID
     * @type {string}
     * @memberof Exception
     */
    exceptionId?: string;
    /**
     *
     * @type {ExceptionStatus}
     * @memberof Exception
     */
    status?: ExceptionStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof Exception
     */
    integration?: string;
    /**
     *
     * @type {ExceptionType}
     * @memberof Exception
     */
    exceptionType: ExceptionType;
    /**
     * Exception specific error message/details
     * @type {string}
     * @memberof Exception
     */
    exceptionDetails?: string;
    /**
     * Entity ID
     * @type {string}
     * @memberof Exception
     */
    entityId: string;
    /**
     *
     * @type {EntityName}
     * @memberof Exception
     */
    entityType: EntityName;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Exception
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Exception
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Exception
     */
    readonly orgKey?: string;
}
export declare function ExceptionFromJSON(json: any): Exception;
export declare function ExceptionFromJSONTyped(json: any, ignoreDiscriminator: boolean): Exception;
export declare function ExceptionToJSON(value?: Exception | null): any;
