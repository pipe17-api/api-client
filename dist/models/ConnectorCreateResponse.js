"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorCreateResponseToJSON = exports.ConnectorCreateResponseFromJSONTyped = exports.ConnectorCreateResponseFromJSON = exports.ConnectorCreateResponseCodeEnum = exports.ConnectorCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ConnectorCreateResponseSuccessEnum;
(function (ConnectorCreateResponseSuccessEnum) {
    ConnectorCreateResponseSuccessEnum["True"] = "true";
})(ConnectorCreateResponseSuccessEnum = exports.ConnectorCreateResponseSuccessEnum || (exports.ConnectorCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ConnectorCreateResponseCodeEnum;
(function (ConnectorCreateResponseCodeEnum) {
    ConnectorCreateResponseCodeEnum[ConnectorCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ConnectorCreateResponseCodeEnum[ConnectorCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ConnectorCreateResponseCodeEnum[ConnectorCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ConnectorCreateResponseCodeEnum = exports.ConnectorCreateResponseCodeEnum || (exports.ConnectorCreateResponseCodeEnum = {}));
function ConnectorCreateResponseFromJSON(json) {
    return ConnectorCreateResponseFromJSONTyped(json, false);
}
exports.ConnectorCreateResponseFromJSON = ConnectorCreateResponseFromJSON;
function ConnectorCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'connector': !runtime_1.exists(json, 'connector') ? undefined : _1.ConnectorFromJSON(json['connector']),
    };
}
exports.ConnectorCreateResponseFromJSONTyped = ConnectorCreateResponseFromJSONTyped;
function ConnectorCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'connector': _1.ConnectorToJSON(value.connector),
    };
}
exports.ConnectorCreateResponseToJSON = ConnectorCreateResponseToJSON;
