/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Tracking } from './';
/**
 *
 * @export
 * @interface TrackingCreateResponse
 */
export interface TrackingCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof TrackingCreateResponse
     */
    success: TrackingCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof TrackingCreateResponse
     */
    code: TrackingCreateResponseCodeEnum;
    /**
     *
     * @type {Tracking}
     * @memberof TrackingCreateResponse
     */
    tracking?: Tracking;
}
/**
* @export
* @enum {string}
*/
export declare enum TrackingCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum TrackingCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function TrackingCreateResponseFromJSON(json: any): TrackingCreateResponse;
export declare function TrackingCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingCreateResponse;
export declare function TrackingCreateResponseToJSON(value?: TrackingCreateResponse | null): any;
