/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderRoutingUpdateRequest
 */
export interface OrderRoutingUpdateRequest {
    /**
     * Routing rules definition
     * @type {Array<object>}
     * @memberof OrderRoutingUpdateRequest
     */
    rules?: Array<object>;
}
export declare function OrderRoutingUpdateRequestFromJSON(json: any): OrderRoutingUpdateRequest;
export declare function OrderRoutingUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingUpdateRequest;
export declare function OrderRoutingUpdateRequestToJSON(value?: OrderRoutingUpdateRequest | null): any;
