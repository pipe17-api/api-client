/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductsDeleteFilter } from './';
/**
 *
 * @export
 * @interface ProductsDeleteError
 */
export interface ProductsDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof ProductsDeleteError
     */
    success: ProductsDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ProductsDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof ProductsDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof ProductsDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {ProductsDeleteFilter}
     * @memberof ProductsDeleteError
     */
    filters?: ProductsDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductsDeleteErrorSuccessEnum {
    False = "false"
}
export declare function ProductsDeleteErrorFromJSON(json: any): ProductsDeleteError;
export declare function ProductsDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsDeleteError;
export declare function ProductsDeleteErrorToJSON(value?: ProductsDeleteError | null): any;
