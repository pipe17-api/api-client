"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundToJSON = exports.RefundFromJSONTyped = exports.RefundFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundFromJSON(json) {
    return RefundFromJSONTyped(json, false);
}
exports.RefundFromJSON = RefundFromJSON;
function RefundFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'refundId': !runtime_1.exists(json, 'refundId') ? undefined : json['refundId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'returnId': json['returnId'],
        'extRefundId': !runtime_1.exists(json, 'extRefundId') ? undefined : json['extRefundId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
        'type': _1.RefundTypeFromJSON(json['type']),
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.RefundStatusFromJSON(json['status']),
        'currency': !runtime_1.exists(json, 'currency') ? undefined : json['currency'],
        'amount': !runtime_1.exists(json, 'amount') ? undefined : json['amount'],
        'creditIssued': !runtime_1.exists(json, 'creditIssued') ? undefined : json['creditIssued'],
        'creditSpent': !runtime_1.exists(json, 'creditSpent') ? undefined : json['creditSpent'],
        'merchantInvoiceAmount': !runtime_1.exists(json, 'merchantInvoiceAmount') ? undefined : json['merchantInvoiceAmount'],
        'customerInvoiceAmount': !runtime_1.exists(json, 'customerInvoiceAmount') ? undefined : json['customerInvoiceAmount'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.RefundFromJSONTyped = RefundFromJSONTyped;
function RefundToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'refundId': value.refundId,
        'integration': value.integration,
        'returnId': value.returnId,
        'extRefundId': value.extRefundId,
        'extOrderId': value.extOrderId,
        'type': _1.RefundTypeToJSON(value.type),
        'status': _1.RefundStatusToJSON(value.status),
        'currency': value.currency,
        'amount': value.amount,
        'creditIssued': value.creditIssued,
        'creditSpent': value.creditSpent,
        'merchantInvoiceAmount': value.merchantInvoiceAmount,
        'customerInvoiceAmount': value.customerInvoiceAmount,
    };
}
exports.RefundToJSON = RefundToJSON;
