"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferCreateDataToJSON = exports.TransferCreateDataFromJSONTyped = exports.TransferCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransferCreateDataFromJSON(json) {
    return TransferCreateDataFromJSONTyped(json, false);
}
exports.TransferCreateDataFromJSON = TransferCreateDataFromJSON;
function TransferCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.TransferStatusFromJSON(json['status']),
        'extOrderId': json['extOrderId'],
        'transferOrderDate': !runtime_1.exists(json, 'transferOrderDate') ? undefined : (new Date(json['transferOrderDate'])),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.TransferLineItemFromJSON)),
        'fromLocationId': !runtime_1.exists(json, 'fromLocationId') ? undefined : json['fromLocationId'],
        'toAddress': !runtime_1.exists(json, 'toAddress') ? undefined : _1.AddressFromJSON(json['toAddress']),
        'toLocationId': !runtime_1.exists(json, 'toLocationId') ? undefined : json['toLocationId'],
        'shippingCarrier': !runtime_1.exists(json, 'shippingCarrier') ? undefined : json['shippingCarrier'],
        'shippingService': !runtime_1.exists(json, 'shippingService') ? undefined : json['shippingService'],
        'shipByDate': !runtime_1.exists(json, 'shipByDate') ? undefined : (new Date(json['shipByDate'])),
        'expectedShipDate': !runtime_1.exists(json, 'expectedShipDate') ? undefined : (new Date(json['expectedShipDate'])),
        'expectedArrivalDate': !runtime_1.exists(json, 'expectedArrivalDate') ? undefined : (new Date(json['expectedArrivalDate'])),
        'actualArrivalDate': !runtime_1.exists(json, 'actualArrivalDate') ? undefined : (new Date(json['actualArrivalDate'])),
        'shippingNotes': !runtime_1.exists(json, 'shippingNotes') ? undefined : json['shippingNotes'],
        'employeeName': !runtime_1.exists(json, 'employeeName') ? undefined : json['employeeName'],
        'incoterms': !runtime_1.exists(json, 'incoterms') ? undefined : json['incoterms'],
        'transferNotes': !runtime_1.exists(json, 'transferNotes') ? undefined : json['transferNotes'],
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'timestamp': !runtime_1.exists(json, 'timestamp') ? undefined : (new Date(json['timestamp'])),
    };
}
exports.TransferCreateDataFromJSONTyped = TransferCreateDataFromJSONTyped;
function TransferCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': _1.TransferStatusToJSON(value.status),
        'extOrderId': value.extOrderId,
        'transferOrderDate': value.transferOrderDate === undefined ? undefined : (new Date(value.transferOrderDate).toISOString()),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.TransferLineItemToJSON)),
        'fromLocationId': value.fromLocationId,
        'toAddress': _1.AddressToJSON(value.toAddress),
        'toLocationId': value.toLocationId,
        'shippingCarrier': value.shippingCarrier,
        'shippingService': value.shippingService,
        'shipByDate': value.shipByDate === undefined ? undefined : (new Date(value.shipByDate).toISOString()),
        'expectedShipDate': value.expectedShipDate === undefined ? undefined : (new Date(value.expectedShipDate).toISOString()),
        'expectedArrivalDate': value.expectedArrivalDate === undefined ? undefined : (new Date(value.expectedArrivalDate).toISOString()),
        'actualArrivalDate': value.actualArrivalDate === undefined ? undefined : (new Date(value.actualArrivalDate).toISOString()),
        'shippingNotes': value.shippingNotes,
        'employeeName': value.employeeName,
        'incoterms': value.incoterms,
        'transferNotes': value.transferNotes,
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'timestamp': value.timestamp === undefined ? undefined : (new Date(value.timestamp).toISOString()),
    };
}
exports.TransferCreateDataToJSON = TransferCreateDataToJSON;
