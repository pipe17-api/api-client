"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierCreateResponseAllOfToJSON = exports.SupplierCreateResponseAllOfFromJSONTyped = exports.SupplierCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SupplierCreateResponseAllOfFromJSON(json) {
    return SupplierCreateResponseAllOfFromJSONTyped(json, false);
}
exports.SupplierCreateResponseAllOfFromJSON = SupplierCreateResponseAllOfFromJSON;
function SupplierCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierFromJSON(json['supplier']),
    };
}
exports.SupplierCreateResponseAllOfFromJSONTyped = SupplierCreateResponseAllOfFromJSONTyped;
function SupplierCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'supplier': _1.SupplierToJSON(value.supplier),
    };
}
exports.SupplierCreateResponseAllOfToJSON = SupplierCreateResponseAllOfToJSON;
