"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobFetchResponseToJSON = exports.JobFetchResponseFromJSONTyped = exports.JobFetchResponseFromJSON = exports.JobFetchResponseCodeEnum = exports.JobFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var JobFetchResponseSuccessEnum;
(function (JobFetchResponseSuccessEnum) {
    JobFetchResponseSuccessEnum["True"] = "true";
})(JobFetchResponseSuccessEnum = exports.JobFetchResponseSuccessEnum || (exports.JobFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var JobFetchResponseCodeEnum;
(function (JobFetchResponseCodeEnum) {
    JobFetchResponseCodeEnum[JobFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    JobFetchResponseCodeEnum[JobFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    JobFetchResponseCodeEnum[JobFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(JobFetchResponseCodeEnum = exports.JobFetchResponseCodeEnum || (exports.JobFetchResponseCodeEnum = {}));
function JobFetchResponseFromJSON(json) {
    return JobFetchResponseFromJSONTyped(json, false);
}
exports.JobFetchResponseFromJSON = JobFetchResponseFromJSON;
function JobFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'job': !runtime_1.exists(json, 'job') ? undefined : _1.JobFromJSON(json['job']),
    };
}
exports.JobFetchResponseFromJSONTyped = JobFetchResponseFromJSONTyped;
function JobFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'job': _1.JobToJSON(value.job),
    };
}
exports.JobFetchResponseToJSON = JobFetchResponseToJSON;
