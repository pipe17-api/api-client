/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AddressNullable, AddressUpdateData, NameValue, PurchaseCost, PurchaseStatus, PurchaseUpdateLineItem } from './';
/**
 *
 * @export
 * @interface PurchaseUpdateData
 */
export interface PurchaseUpdateData {
    /**
     *
     * @type {PurchaseStatus}
     * @memberof PurchaseUpdateData
     */
    status?: PurchaseStatus;
    /**
     *
     * @type {Array<PurchaseUpdateLineItem>}
     * @memberof PurchaseUpdateData
     */
    lineItems?: Array<PurchaseUpdateLineItem>;
    /**
     * Sub Total Price
     * @type {number}
     * @memberof PurchaseUpdateData
     */
    subTotalPrice?: number | null;
    /**
     * Purchase Tax
     * @type {number}
     * @memberof PurchaseUpdateData
     */
    purchaseTax?: number | null;
    /**
     * Total Price
     * @type {number}
     * @memberof PurchaseUpdateData
     */
    totalPrice?: number | null;
    /**
     * Purchase costs
     * @type {Array<PurchaseCost>}
     * @memberof PurchaseUpdateData
     */
    costs?: Array<PurchaseCost> | null;
    /**
     * Purchase order created by
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    employeeName?: string | null;
    /**
     * Purchase Notes
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    purchaseNotes?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof PurchaseUpdateData
     */
    vendorAddress?: AddressUpdateData;
    /**
     *
     * @type {AddressNullable}
     * @memberof PurchaseUpdateData
     */
    toAddress?: AddressNullable | null;
    /**
     * Ship to location ID
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    toLocationId?: string | null;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    shippingCarrier?: string | null;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    shippingService?: string | null;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof PurchaseUpdateData
     */
    shipByDate?: Date | null;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof PurchaseUpdateData
     */
    actualArrivalDate?: Date | null;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof PurchaseUpdateData
     */
    expectedArrivalDate?: Date | null;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    shippingNotes?: string | null;
    /**
     *
     * @type {AddressUpdateData}
     * @memberof PurchaseUpdateData
     */
    billingAddress?: AddressUpdateData;
    /**
     * Purchase order reference number
     * @type {string}
     * @memberof PurchaseUpdateData
     */
    referenceNumber?: string | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof PurchaseUpdateData
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof PurchaseUpdateData
     */
    timestamp?: Date | null;
}
export declare function PurchaseUpdateDataFromJSON(json: any): PurchaseUpdateData;
export declare function PurchaseUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseUpdateData;
export declare function PurchaseUpdateDataToJSON(value?: PurchaseUpdateData | null): any;
