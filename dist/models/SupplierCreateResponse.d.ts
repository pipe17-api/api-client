/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Supplier } from './';
/**
 *
 * @export
 * @interface SupplierCreateResponse
 */
export interface SupplierCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof SupplierCreateResponse
     */
    success: SupplierCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SupplierCreateResponse
     */
    code: SupplierCreateResponseCodeEnum;
    /**
     *
     * @type {Supplier}
     * @memberof SupplierCreateResponse
     */
    supplier?: Supplier;
}
/**
* @export
* @enum {string}
*/
export declare enum SupplierCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum SupplierCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function SupplierCreateResponseFromJSON(json: any): SupplierCreateResponse;
export declare function SupplierCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierCreateResponse;
export declare function SupplierCreateResponseToJSON(value?: SupplierCreateResponse | null): any;
