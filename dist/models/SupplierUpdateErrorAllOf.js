"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.SupplierUpdateErrorAllOfToJSON = exports.SupplierUpdateErrorAllOfFromJSONTyped = exports.SupplierUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function SupplierUpdateErrorAllOfFromJSON(json) {
    return SupplierUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.SupplierUpdateErrorAllOfFromJSON = SupplierUpdateErrorAllOfFromJSON;
function SupplierUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'supplier': !runtime_1.exists(json, 'supplier') ? undefined : _1.SupplierUpdateDataFromJSON(json['supplier']),
    };
}
exports.SupplierUpdateErrorAllOfFromJSONTyped = SupplierUpdateErrorAllOfFromJSONTyped;
function SupplierUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'supplier': _1.SupplierUpdateDataToJSON(value.supplier),
    };
}
exports.SupplierUpdateErrorAllOfToJSON = SupplierUpdateErrorAllOfToJSON;
