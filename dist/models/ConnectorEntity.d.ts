/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Direction, EntityName, EntityRestrictionsItem } from './';
/**
 * Mapping or Routing Entity
 * @export
 * @interface ConnectorEntity
 */
export interface ConnectorEntity {
    /**
     *
     * @type {EntityName}
     * @memberof ConnectorEntity
     */
    name: EntityName;
    /**
     *
     * @type {Direction}
     * @memberof ConnectorEntity
     */
    direction: Direction;
    /**
     * Mapping Id
     * @type {string}
     * @memberof ConnectorEntity
     */
    mappingUuid?: string;
    /**
     * Routing Id
     * @type {string}
     * @memberof ConnectorEntity
     */
    routingUuid?: string;
    /**
     *
     * @type {Array<EntityRestrictionsItem>}
     * @memberof ConnectorEntity
     */
    restrictions?: Array<EntityRestrictionsItem>;
}
export declare function ConnectorEntityFromJSON(json: any): ConnectorEntity;
export declare function ConnectorEntityFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorEntity;
export declare function ConnectorEntityToJSON(value?: ConnectorEntity | null): any;
