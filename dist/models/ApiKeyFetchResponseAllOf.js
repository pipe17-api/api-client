"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyFetchResponseAllOfToJSON = exports.ApiKeyFetchResponseAllOfFromJSONTyped = exports.ApiKeyFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ApiKeyFetchResponseAllOfFromJSON(json) {
    return ApiKeyFetchResponseAllOfFromJSONTyped(json, false);
}
exports.ApiKeyFetchResponseAllOfFromJSON = ApiKeyFetchResponseAllOfFromJSON;
function ApiKeyFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : _1.ApiKeyFromJSON(json['apikey']),
    };
}
exports.ApiKeyFetchResponseAllOfFromJSONTyped = ApiKeyFetchResponseAllOfFromJSONTyped;
function ApiKeyFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikey': _1.ApiKeyToJSON(value.apikey),
    };
}
exports.ApiKeyFetchResponseAllOfToJSON = ApiKeyFetchResponseAllOfToJSON;
