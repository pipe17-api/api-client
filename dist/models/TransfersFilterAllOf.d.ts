/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferStatus } from './';
/**
 * Transfers Filter
 * @export
 * @interface TransfersFilterAllOf
 */
export interface TransfersFilterAllOf {
    /**
     * Transfers by list of transferId
     * @type {Array<string>}
     * @memberof TransfersFilterAllOf
     */
    transferId?: Array<string>;
    /**
     * Transfers by list of external transfer IDs
     * @type {Array<string>}
     * @memberof TransfersFilterAllOf
     */
    extOrderId?: Array<string>;
    /**
     * Transfers by list of statuses
     * @type {Array<TransferStatus>}
     * @memberof TransfersFilterAllOf
     */
    status?: Array<TransferStatus>;
    /**
     * Soft deleted transfers
     * @type {boolean}
     * @memberof TransfersFilterAllOf
     */
    deleted?: boolean;
}
export declare function TransfersFilterAllOfFromJSON(json: any): TransfersFilterAllOf;
export declare function TransfersFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersFilterAllOf;
export declare function TransfersFilterAllOfToJSON(value?: TransfersFilterAllOf | null): any;
