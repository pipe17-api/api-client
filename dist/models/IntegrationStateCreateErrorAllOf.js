"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateCreateErrorAllOfToJSON = exports.IntegrationStateCreateErrorAllOfFromJSONTyped = exports.IntegrationStateCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationStateCreateErrorAllOfFromJSON(json) {
    return IntegrationStateCreateErrorAllOfFromJSONTyped(json, false);
}
exports.IntegrationStateCreateErrorAllOfFromJSON = IntegrationStateCreateErrorAllOfFromJSON;
function IntegrationStateCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integrationState': !runtime_1.exists(json, 'integrationState') ? undefined : _1.IntegrationStateCreateDataFromJSON(json['integrationState']),
    };
}
exports.IntegrationStateCreateErrorAllOfFromJSONTyped = IntegrationStateCreateErrorAllOfFromJSONTyped;
function IntegrationStateCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integrationState': _1.IntegrationStateCreateDataToJSON(value.integrationState),
    };
}
exports.IntegrationStateCreateErrorAllOfToJSON = IntegrationStateCreateErrorAllOfToJSON;
