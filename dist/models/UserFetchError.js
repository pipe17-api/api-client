"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserFetchErrorToJSON = exports.UserFetchErrorFromJSONTyped = exports.UserFetchErrorFromJSON = exports.UserFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var UserFetchErrorSuccessEnum;
(function (UserFetchErrorSuccessEnum) {
    UserFetchErrorSuccessEnum["False"] = "false";
})(UserFetchErrorSuccessEnum = exports.UserFetchErrorSuccessEnum || (exports.UserFetchErrorSuccessEnum = {}));
function UserFetchErrorFromJSON(json) {
    return UserFetchErrorFromJSONTyped(json, false);
}
exports.UserFetchErrorFromJSON = UserFetchErrorFromJSON;
function UserFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.UserFetchErrorFromJSONTyped = UserFetchErrorFromJSONTyped;
function UserFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.UserFetchErrorToJSON = UserFetchErrorToJSON;
