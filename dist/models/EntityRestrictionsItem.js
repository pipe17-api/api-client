"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityRestrictionsItemToJSON = exports.EntityRestrictionsItemFromJSONTyped = exports.EntityRestrictionsItemFromJSON = exports.EntityRestrictionsItemPathEnum = void 0;
/**
* @export
* @enum {string}
*/
var EntityRestrictionsItemPathEnum;
(function (EntityRestrictionsItemPathEnum) {
    EntityRestrictionsItemPathEnum["Restricted"] = "restricted";
    EntityRestrictionsItemPathEnum["Since"] = "since";
    EntityRestrictionsItemPathEnum["Until"] = "until";
    EntityRestrictionsItemPathEnum["Content"] = "content";
})(EntityRestrictionsItemPathEnum = exports.EntityRestrictionsItemPathEnum || (exports.EntityRestrictionsItemPathEnum = {}));
function EntityRestrictionsItemFromJSON(json) {
    return EntityRestrictionsItemFromJSONTyped(json, false);
}
exports.EntityRestrictionsItemFromJSON = EntityRestrictionsItemFromJSON;
function EntityRestrictionsItemFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'path': json['path'],
    };
}
exports.EntityRestrictionsItemFromJSONTyped = EntityRestrictionsItemFromJSONTyped;
function EntityRestrictionsItemToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'path': value.path,
    };
}
exports.EntityRestrictionsItemToJSON = EntityRestrictionsItemToJSON;
