/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Receipt, ReceiptsListFilter } from './';
/**
 *
 * @export
 * @interface ReceiptsListResponseAllOf
 */
export interface ReceiptsListResponseAllOf {
    /**
     *
     * @type {ReceiptsListFilter}
     * @memberof ReceiptsListResponseAllOf
     */
    filters?: ReceiptsListFilter;
    /**
     *
     * @type {Array<Receipt>}
     * @memberof ReceiptsListResponseAllOf
     */
    receipts: Array<Receipt>;
    /**
     *
     * @type {Pagination}
     * @memberof ReceiptsListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ReceiptsListResponseAllOfFromJSON(json: any): ReceiptsListResponseAllOf;
export declare function ReceiptsListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsListResponseAllOf;
export declare function ReceiptsListResponseAllOfToJSON(value?: ReceiptsListResponseAllOf | null): any;
