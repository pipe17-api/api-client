/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoutingCreateData } from './';
/**
 *
 * @export
 * @interface RoutingCreateErrorAllOf
 */
export interface RoutingCreateErrorAllOf {
    /**
     *
     * @type {RoutingCreateData}
     * @memberof RoutingCreateErrorAllOf
     */
    routing?: RoutingCreateData;
}
export declare function RoutingCreateErrorAllOfFromJSON(json: any): RoutingCreateErrorAllOf;
export declare function RoutingCreateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingCreateErrorAllOf;
export declare function RoutingCreateErrorAllOfToJSON(value?: RoutingCreateErrorAllOf | null): any;
