/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface TransferLineItem
 */
export interface TransferLineItem {
    /**
     * Item unique Id (within transfer)
     * @type {string}
     * @memberof TransferLineItem
     */
    uniqueId: string;
    /**
     * Item Quantity
     * @type {number}
     * @memberof TransferLineItem
     */
    quantity: number;
    /**
     * Item SKU
     * @type {string}
     * @memberof TransferLineItem
     */
    sku: string;
    /**
     * Item Name or Description
     * @type {string}
     * @memberof TransferLineItem
     */
    name?: string;
    /**
     * Item UPC
     * @type {string}
     * @memberof TransferLineItem
     */
    upc?: string;
    /**
     * Shipped quantity
     * @type {number}
     * @memberof TransferLineItem
     */
    shippedQuantity?: number;
    /**
     * Received quantity
     * @type {number}
     * @memberof TransferLineItem
     */
    receivedQuantity?: number;
    /**
     *
     * @type {string}
     * @memberof TransferLineItem
     */
    locationId?: string;
}
export declare function TransferLineItemFromJSON(json: any): TransferLineItem;
export declare function TransferLineItemFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferLineItem;
export declare function TransferLineItemToJSON(value?: TransferLineItem | null): any;
