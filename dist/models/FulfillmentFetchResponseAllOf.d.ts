/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Fulfillment } from './';
/**
 *
 * @export
 * @interface FulfillmentFetchResponseAllOf
 */
export interface FulfillmentFetchResponseAllOf {
    /**
     *
     * @type {Fulfillment}
     * @memberof FulfillmentFetchResponseAllOf
     */
    fulfillment?: Fulfillment;
}
export declare function FulfillmentFetchResponseAllOfFromJSON(json: any): FulfillmentFetchResponseAllOf;
export declare function FulfillmentFetchResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentFetchResponseAllOf;
export declare function FulfillmentFetchResponseAllOfToJSON(value?: FulfillmentFetchResponseAllOf | null): any;
