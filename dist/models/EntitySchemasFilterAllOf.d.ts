/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EntityName } from './';
/**
 * Entity Schemas Filter
 * @export
 * @interface EntitySchemasFilterAllOf
 */
export interface EntitySchemasFilterAllOf {
    /**
     * Fetch by list of schemaId
     * @type {Array<string>}
     * @memberof EntitySchemasFilterAllOf
     */
    schemaId?: Array<string>;
    /**
     * Fetch by list of entity types
     * @type {Array<EntityName>}
     * @memberof EntitySchemasFilterAllOf
     */
    entity?: Array<EntityName>;
    /**
     * Fetch by list of schema names
     * @type {Array<string>}
     * @memberof EntitySchemasFilterAllOf
     */
    name?: Array<string>;
}
export declare function EntitySchemasFilterAllOfFromJSON(json: any): EntitySchemasFilterAllOf;
export declare function EntitySchemasFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): EntitySchemasFilterAllOf;
export declare function EntitySchemasFilterAllOfToJSON(value?: EntitySchemasFilterAllOf | null): any;
