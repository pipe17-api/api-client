"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookTopicsToJSON = exports.WebhookTopicsFromJSONTyped = exports.WebhookTopicsFromJSON = exports.WebhookTopics = void 0;
/**
 * WebhookTopic
 * @export
 * @enum {string}
 */
var WebhookTopics;
(function (WebhookTopics) {
    WebhookTopics["Arrivals"] = "arrivals";
    WebhookTopics["Fulfillments"] = "fulfillments";
    WebhookTopics["Integrations"] = "integrations";
    WebhookTopics["Inventory"] = "inventory";
    WebhookTopics["Labels"] = "labels";
    WebhookTopics["Locations"] = "locations";
    WebhookTopics["Orders"] = "orders";
    WebhookTopics["Products"] = "products";
    WebhookTopics["Purchases"] = "purchases";
    WebhookTopics["Receipts"] = "receipts";
    WebhookTopics["Refunds"] = "refunds";
    WebhookTopics["Returns"] = "returns";
    WebhookTopics["Shipments"] = "shipments";
    WebhookTopics["Suppliers"] = "suppliers";
    WebhookTopics["Transfers"] = "transfers";
    WebhookTopics["Trackings"] = "trackings";
    WebhookTopics["Exceptions"] = "exceptions";
})(WebhookTopics = exports.WebhookTopics || (exports.WebhookTopics = {}));
function WebhookTopicsFromJSON(json) {
    return WebhookTopicsFromJSONTyped(json, false);
}
exports.WebhookTopicsFromJSON = WebhookTopicsFromJSON;
function WebhookTopicsFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.WebhookTopicsFromJSONTyped = WebhookTopicsFromJSONTyped;
function WebhookTopicsToJSON(value) {
    return value;
}
exports.WebhookTopicsToJSON = WebhookTopicsToJSON;
