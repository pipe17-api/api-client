"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntitySchemaListResponseToJSON = exports.EntitySchemaListResponseFromJSONTyped = exports.EntitySchemaListResponseFromJSON = exports.EntitySchemaListResponseCodeEnum = exports.EntitySchemaListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntitySchemaListResponseSuccessEnum;
(function (EntitySchemaListResponseSuccessEnum) {
    EntitySchemaListResponseSuccessEnum["True"] = "true";
})(EntitySchemaListResponseSuccessEnum = exports.EntitySchemaListResponseSuccessEnum || (exports.EntitySchemaListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntitySchemaListResponseCodeEnum;
(function (EntitySchemaListResponseCodeEnum) {
    EntitySchemaListResponseCodeEnum[EntitySchemaListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntitySchemaListResponseCodeEnum[EntitySchemaListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntitySchemaListResponseCodeEnum[EntitySchemaListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntitySchemaListResponseCodeEnum = exports.EntitySchemaListResponseCodeEnum || (exports.EntitySchemaListResponseCodeEnum = {}));
function EntitySchemaListResponseFromJSON(json) {
    return EntitySchemaListResponseFromJSONTyped(json, false);
}
exports.EntitySchemaListResponseFromJSON = EntitySchemaListResponseFromJSON;
function EntitySchemaListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.EntitySchemasListFilterFromJSON(json['filters']),
        'entitySchemas': !runtime_1.exists(json, 'entitySchemas') ? undefined : (json['entitySchemas'].map(_1.EntitySchemaFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.EntitySchemaListResponseFromJSONTyped = EntitySchemaListResponseFromJSONTyped;
function EntitySchemaListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.EntitySchemasListFilterToJSON(value.filters),
        'entitySchemas': value.entitySchemas === undefined ? undefined : (value.entitySchemas.map(_1.EntitySchemaToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.EntitySchemaListResponseToJSON = EntitySchemaListResponseToJSON;
