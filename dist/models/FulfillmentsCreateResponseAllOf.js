"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsCreateResponseAllOfToJSON = exports.FulfillmentsCreateResponseAllOfFromJSONTyped = exports.FulfillmentsCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function FulfillmentsCreateResponseAllOfFromJSON(json) {
    return FulfillmentsCreateResponseAllOfFromJSONTyped(json, false);
}
exports.FulfillmentsCreateResponseAllOfFromJSON = FulfillmentsCreateResponseAllOfFromJSON;
function FulfillmentsCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fulfillments': !runtime_1.exists(json, 'fulfillments') ? undefined : (json['fulfillments'].map(_1.FulfillmentCreateResultFromJSON)),
    };
}
exports.FulfillmentsCreateResponseAllOfFromJSONTyped = FulfillmentsCreateResponseAllOfFromJSONTyped;
function FulfillmentsCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fulfillments': value.fulfillments === undefined ? undefined : (value.fulfillments.map(_1.FulfillmentCreateResultToJSON)),
    };
}
exports.FulfillmentsCreateResponseAllOfToJSON = FulfillmentsCreateResponseAllOfToJSON;
