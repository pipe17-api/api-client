"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorsListErrorAllOfToJSON = exports.ConnectorsListErrorAllOfFromJSONTyped = exports.ConnectorsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConnectorsListErrorAllOfFromJSON(json) {
    return ConnectorsListErrorAllOfFromJSONTyped(json, false);
}
exports.ConnectorsListErrorAllOfFromJSON = ConnectorsListErrorAllOfFromJSON;
function ConnectorsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ConnectorsListFilterFromJSON(json['filters']),
    };
}
exports.ConnectorsListErrorAllOfFromJSONTyped = ConnectorsListErrorAllOfFromJSONTyped;
function ConnectorsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.ConnectorsListFilterToJSON(value.filters),
    };
}
exports.ConnectorsListErrorAllOfToJSON = ConnectorsListErrorAllOfToJSON;
