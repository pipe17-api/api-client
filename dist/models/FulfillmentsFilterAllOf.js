"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentsFilterAllOfToJSON = exports.FulfillmentsFilterAllOfFromJSONTyped = exports.FulfillmentsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function FulfillmentsFilterAllOfFromJSON(json) {
    return FulfillmentsFilterAllOfFromJSONTyped(json, false);
}
exports.FulfillmentsFilterAllOfFromJSON = FulfillmentsFilterAllOfFromJSON;
function FulfillmentsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fulfillmentId': !runtime_1.exists(json, 'fulfillmentId') ? undefined : json['fulfillmentId'],
        'shipmentId': !runtime_1.exists(json, 'shipmentId') ? undefined : json['shipmentId'],
        'extShipmentId': !runtime_1.exists(json, 'extShipmentId') ? undefined : json['extShipmentId'],
        'extOrderId': !runtime_1.exists(json, 'extOrderId') ? undefined : json['extOrderId'],
    };
}
exports.FulfillmentsFilterAllOfFromJSONTyped = FulfillmentsFilterAllOfFromJSONTyped;
function FulfillmentsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fulfillmentId': value.fulfillmentId,
        'shipmentId': value.shipmentId,
        'extShipmentId': value.extShipmentId,
        'extOrderId': value.extOrderId,
    };
}
exports.FulfillmentsFilterAllOfToJSON = FulfillmentsFilterAllOfToJSON;
