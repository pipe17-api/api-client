"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnsDeleteResponseToJSON = exports.ReturnsDeleteResponseFromJSONTyped = exports.ReturnsDeleteResponseFromJSON = exports.ReturnsDeleteResponseCodeEnum = exports.ReturnsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ReturnsDeleteResponseSuccessEnum;
(function (ReturnsDeleteResponseSuccessEnum) {
    ReturnsDeleteResponseSuccessEnum["True"] = "true";
})(ReturnsDeleteResponseSuccessEnum = exports.ReturnsDeleteResponseSuccessEnum || (exports.ReturnsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ReturnsDeleteResponseCodeEnum;
(function (ReturnsDeleteResponseCodeEnum) {
    ReturnsDeleteResponseCodeEnum[ReturnsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ReturnsDeleteResponseCodeEnum[ReturnsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ReturnsDeleteResponseCodeEnum[ReturnsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ReturnsDeleteResponseCodeEnum = exports.ReturnsDeleteResponseCodeEnum || (exports.ReturnsDeleteResponseCodeEnum = {}));
function ReturnsDeleteResponseFromJSON(json) {
    return ReturnsDeleteResponseFromJSONTyped(json, false);
}
exports.ReturnsDeleteResponseFromJSON = ReturnsDeleteResponseFromJSON;
function ReturnsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.ReturnsDeleteFilterFromJSON(json['filters']),
        'suppliers': !runtime_1.exists(json, 'suppliers') ? undefined : (json['suppliers'].map(_1.ReturnFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.ReturnsDeleteResponseFromJSONTyped = ReturnsDeleteResponseFromJSONTyped;
function ReturnsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.ReturnsDeleteFilterToJSON(value.filters),
        'suppliers': value.suppliers === undefined ? undefined : (value.suppliers.map(_1.ReturnToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.ReturnsDeleteResponseToJSON = ReturnsDeleteResponseToJSON;
