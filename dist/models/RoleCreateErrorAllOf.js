"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleCreateErrorAllOfToJSON = exports.RoleCreateErrorAllOfFromJSONTyped = exports.RoleCreateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RoleCreateErrorAllOfFromJSON(json) {
    return RoleCreateErrorAllOfFromJSONTyped(json, false);
}
exports.RoleCreateErrorAllOfFromJSON = RoleCreateErrorAllOfFromJSON;
function RoleCreateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'role': !runtime_1.exists(json, 'role') ? undefined : _1.RoleCreateDataFromJSON(json['role']),
    };
}
exports.RoleCreateErrorAllOfFromJSONTyped = RoleCreateErrorAllOfFromJSONTyped;
function RoleCreateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'role': _1.RoleCreateDataToJSON(value.role),
    };
}
exports.RoleCreateErrorAllOfToJSON = RoleCreateErrorAllOfToJSON;
