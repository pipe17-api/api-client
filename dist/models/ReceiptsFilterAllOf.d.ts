/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Receipts Filter
 * @export
 * @interface ReceiptsFilterAllOf
 */
export interface ReceiptsFilterAllOf {
    /**
     * Receipts by list of receiptId
     * @type {Array<string>}
     * @memberof ReceiptsFilterAllOf
     */
    receiptId?: Array<string>;
    /**
     * Receipts by list of arrivalId
     * @type {Array<string>}
     * @memberof ReceiptsFilterAllOf
     */
    arrivalId?: Array<string>;
    /**
     * Receipts by list of extOrderId
     * @type {Array<string>}
     * @memberof ReceiptsFilterAllOf
     */
    extOrderId?: Array<string>;
}
export declare function ReceiptsFilterAllOfFromJSON(json: any): ReceiptsFilterAllOf;
export declare function ReceiptsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptsFilterAllOf;
export declare function ReceiptsFilterAllOfToJSON(value?: ReceiptsFilterAllOf | null): any;
