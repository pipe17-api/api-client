"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingUpdateRequestToJSON = exports.MappingUpdateRequestFromJSONTyped = exports.MappingUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingUpdateRequestFromJSON(json) {
    return MappingUpdateRequestFromJSONTyped(json, false);
}
exports.MappingUpdateRequestFromJSON = MappingUpdateRequestFromJSON;
function MappingUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'isPublic': !runtime_1.exists(json, 'isPublic') ? undefined : json['isPublic'],
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingUpdateRuleFromJSON)),
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
    };
}
exports.MappingUpdateRequestFromJSONTyped = MappingUpdateRequestFromJSONTyped;
function MappingUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'isPublic': value.isPublic,
        'description': value.description,
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingUpdateRuleToJSON)),
        'originId': value.originId,
    };
}
exports.MappingUpdateRequestToJSON = MappingUpdateRequestToJSON;
