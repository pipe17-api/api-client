/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface LabelCreateError
 */
export interface LabelCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof LabelCreateError
     */
    success: LabelCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof LabelCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof LabelCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof LabelCreateError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum LabelCreateErrorSuccessEnum {
    False = "false"
}
export declare function LabelCreateErrorFromJSON(json: any): LabelCreateError;
export declare function LabelCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): LabelCreateError;
export declare function LabelCreateErrorToJSON(value?: LabelCreateError | null): any;
