/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConnectorsFilter
 */
export interface ConnectorsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ConnectorsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ConnectorsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ConnectorsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ConnectorsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ConnectorsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ConnectorsFilter
     */
    count?: number;
    /**
     * Retrieve connectors by list of connectorId
     * @type {Array<string>}
     * @memberof ConnectorsFilter
     */
    connectorId?: Array<string>;
    /**
     * Retrieve connector by list of connector names
     * @type {Array<string>}
     * @memberof ConnectorsFilter
     */
    connectorName?: Array<string>;
    /**
     * Retrieve connector by list of connector types
     * @type {Array<string>}
     * @memberof ConnectorsFilter
     */
    connectorType?: Array<string>;
}
export declare function ConnectorsFilterFromJSON(json: any): ConnectorsFilter;
export declare function ConnectorsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConnectorsFilter;
export declare function ConnectorsFilterToJSON(value?: ConnectorsFilter | null): any;
