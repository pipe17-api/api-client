/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookUpdateData } from './';
/**
 *
 * @export
 * @interface WebhookUpdateErrorAllOf
 */
export interface WebhookUpdateErrorAllOf {
    /**
     *
     * @type {WebhookUpdateData}
     * @memberof WebhookUpdateErrorAllOf
     */
    webhook?: WebhookUpdateData;
}
export declare function WebhookUpdateErrorAllOfFromJSON(json: any): WebhookUpdateErrorAllOf;
export declare function WebhookUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookUpdateErrorAllOf;
export declare function WebhookUpdateErrorAllOfToJSON(value?: WebhookUpdateErrorAllOf | null): any;
