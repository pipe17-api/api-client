"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingListResponseAllOfToJSON = exports.TrackingListResponseAllOfFromJSONTyped = exports.TrackingListResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TrackingListResponseAllOfFromJSON(json) {
    return TrackingListResponseAllOfFromJSONTyped(json, false);
}
exports.TrackingListResponseAllOfFromJSON = TrackingListResponseAllOfFromJSON;
function TrackingListResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TrackingFilterFromJSON(json['filters']),
        'trackings': !runtime_1.exists(json, 'trackings') ? undefined : (json['trackings'].map(_1.TrackingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.TrackingListResponseAllOfFromJSONTyped = TrackingListResponseAllOfFromJSONTyped;
function TrackingListResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.TrackingFilterToJSON(value.filters),
        'trackings': value.trackings === undefined ? undefined : (value.trackings.map(_1.TrackingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.TrackingListResponseAllOfToJSON = TrackingListResponseAllOfToJSON;
