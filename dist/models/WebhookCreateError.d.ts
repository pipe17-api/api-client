/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookCreateData } from './';
/**
 *
 * @export
 * @interface WebhookCreateError
 */
export interface WebhookCreateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof WebhookCreateError
     */
    success: WebhookCreateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhookCreateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof WebhookCreateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof WebhookCreateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {WebhookCreateData}
     * @memberof WebhookCreateError
     */
    webhook?: WebhookCreateData;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhookCreateErrorSuccessEnum {
    False = "false"
}
export declare function WebhookCreateErrorFromJSON(json: any): WebhookCreateError;
export declare function WebhookCreateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhookCreateError;
export declare function WebhookCreateErrorToJSON(value?: WebhookCreateError | null): any;
