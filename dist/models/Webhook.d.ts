/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhookTopics } from './';
/**
 *
 * @export
 * @interface Webhook
 */
export interface Webhook {
    /**
     * Webhook ID
     * @type {string}
     * @memberof Webhook
     */
    webhookId?: string;
    /**
     * Integration ID
     * @type {string}
     * @memberof Webhook
     */
    integration?: string;
    /**
     * Connector ID
     * @type {string}
     * @memberof Webhook
     */
    connectorId?: string;
    /**
     * Webhook URL
     * @type {string}
     * @memberof Webhook
     */
    url: string;
    /**
     * Webhook api key
     * @type {string}
     * @memberof Webhook
     */
    apikey: string;
    /**
     * Webhook topics
     * @type {Array<WebhookTopics>}
     * @memberof Webhook
     */
    topics: Array<WebhookTopics>;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Webhook
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Webhook
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Webhook
     */
    readonly orgKey?: string;
}
export declare function WebhookFromJSON(json: any): Webhook;
export declare function WebhookFromJSONTyped(json: any, ignoreDiscriminator: boolean): Webhook;
export declare function WebhookToJSON(value?: Webhook | null): any;
