/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TrackingStatus } from './';
/**
 *
 * @export
 * @interface TrackingAllOf
 */
export interface TrackingAllOf {
    /**
     *
     * @type {string}
     * @memberof TrackingAllOf
     */
    trackingId?: string;
    /**
     *
     * @type {TrackingStatus}
     * @memberof TrackingAllOf
     */
    status?: TrackingStatus;
    /**
     *
     * @type {Date}
     * @memberof TrackingAllOf
     */
    actualShipDate?: Date;
    /**
     *
     * @type {Date}
     * @memberof TrackingAllOf
     */
    actualArrivalDate?: Date;
}
export declare function TrackingAllOfFromJSON(json: any): TrackingAllOf;
export declare function TrackingAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TrackingAllOf;
export declare function TrackingAllOfToJSON(value?: TrackingAllOf | null): any;
