/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Shipment } from './';
/**
 *
 * @export
 * @interface ShipmentFetchResponse
 */
export interface ShipmentFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ShipmentFetchResponse
     */
    success: ShipmentFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentFetchResponse
     */
    code: ShipmentFetchResponseCodeEnum;
    /**
     *
     * @type {Shipment}
     * @memberof ShipmentFetchResponse
     */
    shipment?: Shipment;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ShipmentFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ShipmentFetchResponseFromJSON(json: any): ShipmentFetchResponse;
export declare function ShipmentFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentFetchResponse;
export declare function ShipmentFetchResponseToJSON(value?: ShipmentFetchResponse | null): any;
