"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyAllOfToJSON = exports.ApiKeyAllOfFromJSONTyped = exports.ApiKeyAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ApiKeyAllOfFromJSON(json) {
    return ApiKeyAllOfFromJSONTyped(json, false);
}
exports.ApiKeyAllOfFromJSON = ApiKeyAllOfFromJSON;
function ApiKeyAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikeyId': !runtime_1.exists(json, 'apikeyId') ? undefined : json['apikeyId'],
    };
}
exports.ApiKeyAllOfFromJSONTyped = ApiKeyAllOfFromJSONTyped;
function ApiKeyAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikeyId': value.apikeyId,
    };
}
exports.ApiKeyAllOfToJSON = ApiKeyAllOfToJSON;
