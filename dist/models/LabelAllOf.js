"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelAllOfToJSON = exports.LabelAllOfFromJSONTyped = exports.LabelAllOfFromJSON = exports.LabelAllOfStatusEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var LabelAllOfStatusEnum;
(function (LabelAllOfStatusEnum) {
    LabelAllOfStatusEnum["New"] = "new";
    LabelAllOfStatusEnum["Ready"] = "ready";
})(LabelAllOfStatusEnum = exports.LabelAllOfStatusEnum || (exports.LabelAllOfStatusEnum = {}));
function LabelAllOfFromJSON(json) {
    return LabelAllOfFromJSONTyped(json, false);
}
exports.LabelAllOfFromJSON = LabelAllOfFromJSON;
function LabelAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'labelId': !runtime_1.exists(json, 'labelId') ? undefined : json['labelId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
    };
}
exports.LabelAllOfFromJSONTyped = LabelAllOfFromJSONTyped;
function LabelAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'labelId': value.labelId,
        'status': value.status,
    };
}
exports.LabelAllOfToJSON = LabelAllOfToJSON;
