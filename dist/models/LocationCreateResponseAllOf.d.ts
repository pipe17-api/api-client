/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Location } from './';
/**
 *
 * @export
 * @interface LocationCreateResponseAllOf
 */
export interface LocationCreateResponseAllOf {
    /**
     *
     * @type {Location}
     * @memberof LocationCreateResponseAllOf
     */
    location?: Location;
}
export declare function LocationCreateResponseAllOfFromJSON(json: any): LocationCreateResponseAllOf;
export declare function LocationCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationCreateResponseAllOf;
export declare function LocationCreateResponseAllOfToJSON(value?: LocationCreateResponseAllOf | null): any;
