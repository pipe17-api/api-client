"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrdersCreateResponseToJSON = exports.OrdersCreateResponseFromJSONTyped = exports.OrdersCreateResponseFromJSON = exports.OrdersCreateResponseCodeEnum = exports.OrdersCreateResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var OrdersCreateResponseSuccessEnum;
(function (OrdersCreateResponseSuccessEnum) {
    OrdersCreateResponseSuccessEnum["True"] = "true";
})(OrdersCreateResponseSuccessEnum = exports.OrdersCreateResponseSuccessEnum || (exports.OrdersCreateResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var OrdersCreateResponseCodeEnum;
(function (OrdersCreateResponseCodeEnum) {
    OrdersCreateResponseCodeEnum[OrdersCreateResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    OrdersCreateResponseCodeEnum[OrdersCreateResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    OrdersCreateResponseCodeEnum[OrdersCreateResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(OrdersCreateResponseCodeEnum = exports.OrdersCreateResponseCodeEnum || (exports.OrdersCreateResponseCodeEnum = {}));
function OrdersCreateResponseFromJSON(json) {
    return OrdersCreateResponseFromJSONTyped(json, false);
}
exports.OrdersCreateResponseFromJSON = OrdersCreateResponseFromJSON;
function OrdersCreateResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'orders': !runtime_1.exists(json, 'orders') ? undefined : (json['orders'].map(_1.OrderCreateResultFromJSON)),
    };
}
exports.OrdersCreateResponseFromJSONTyped = OrdersCreateResponseFromJSONTyped;
function OrdersCreateResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'orders': value.orders === undefined ? undefined : (value.orders.map(_1.OrderCreateResultToJSON)),
    };
}
exports.OrdersCreateResponseToJSON = OrdersCreateResponseToJSON;
