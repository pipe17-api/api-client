/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { WebhooksListFilter } from './';
/**
 *
 * @export
 * @interface WebhooksListError
 */
export interface WebhooksListError {
    /**
     * Always false
     * @type {boolean}
     * @memberof WebhooksListError
     */
    success: WebhooksListErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof WebhooksListError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof WebhooksListError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof WebhooksListError
     */
    errors?: Array<string>;
    /**
     *
     * @type {WebhooksListFilter}
     * @memberof WebhooksListError
     */
    filters?: WebhooksListFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum WebhooksListErrorSuccessEnum {
    False = "false"
}
export declare function WebhooksListErrorFromJSON(json: any): WebhooksListError;
export declare function WebhooksListErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): WebhooksListError;
export declare function WebhooksListErrorToJSON(value?: WebhooksListError | null): any;
