/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationStatus } from './';
/**
 *
 * @export
 * @interface OrganizationsFilter
 */
export interface OrganizationsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof OrganizationsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof OrganizationsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof OrganizationsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof OrganizationsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof OrganizationsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof OrganizationsFilter
     */
    count?: number;
    /**
     * Organizations by list of orgKey
     * @type {Array<string>}
     * @memberof OrganizationsFilter
     */
    orgKey?: Array<string>;
    /**
     * Organizations whose name contains this string
     * @type {string}
     * @memberof OrganizationsFilter
     */
    name?: string;
    /**
     * Organizations whose phone number contains this string
     * @type {string}
     * @memberof OrganizationsFilter
     */
    phone?: string;
    /**
     * Organizations whose email contains this string
     * @type {string}
     * @memberof OrganizationsFilter
     */
    email?: string;
    /**
     * Organizations by list of statuses
     * @type {Array<OrganizationStatus>}
     * @memberof OrganizationsFilter
     */
    status?: Array<OrganizationStatus>;
}
export declare function OrganizationsFilterFromJSON(json: any): OrganizationsFilter;
export declare function OrganizationsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationsFilter;
export declare function OrganizationsFilterToJSON(value?: OrganizationsFilter | null): any;
