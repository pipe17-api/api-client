"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsListFilterToJSON = exports.ProductsListFilterFromJSONTyped = exports.ProductsListFilterFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductsListFilterFromJSON(json) {
    return ProductsListFilterFromJSONTyped(json, false);
}
exports.ProductsListFilterFromJSON = ProductsListFilterFromJSON;
function ProductsListFilterFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'since': !runtime_1.exists(json, 'since') ? undefined : (new Date(json['since'])),
        'until': !runtime_1.exists(json, 'until') ? undefined : (new Date(json['until'])),
        'updatedSince': !runtime_1.exists(json, 'updatedSince') ? undefined : (new Date(json['updatedSince'])),
        'updatedUntil': !runtime_1.exists(json, 'updatedUntil') ? undefined : (new Date(json['updatedUntil'])),
        'skip': !runtime_1.exists(json, 'skip') ? undefined : json['skip'],
        'count': !runtime_1.exists(json, 'count') ? undefined : json['count'],
        'productId': !runtime_1.exists(json, 'productId') ? undefined : json['productId'],
        'extProductId': !runtime_1.exists(json, 'extProductId') ? undefined : json['extProductId'],
        'parentExtProductId': !runtime_1.exists(json, 'parentExtProductId') ? undefined : json['parentExtProductId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ProductStatusFromJSON)),
        'types': !runtime_1.exists(json, 'types') ? undefined : (json['types'].map(_1.ProductTypeFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'order': !runtime_1.exists(json, 'order') ? undefined : json['order'],
        'keys': !runtime_1.exists(json, 'keys') ? undefined : json['keys'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'bundleSKU': !runtime_1.exists(json, 'bundleSKU') ? undefined : json['bundleSKU'],
        'inBundle': !runtime_1.exists(json, 'inBundle') ? undefined : json['inBundle'],
    };
}
exports.ProductsListFilterFromJSONTyped = ProductsListFilterFromJSONTyped;
function ProductsListFilterToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'since': value.since === undefined ? undefined : (new Date(value.since).toISOString()),
        'until': value.until === undefined ? undefined : (new Date(value.until).toISOString()),
        'updatedSince': value.updatedSince === undefined ? undefined : (new Date(value.updatedSince).toISOString()),
        'updatedUntil': value.updatedUntil === undefined ? undefined : (new Date(value.updatedUntil).toISOString()),
        'skip': value.skip,
        'count': value.count,
        'productId': value.productId,
        'extProductId': value.extProductId,
        'parentExtProductId': value.parentExtProductId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ProductStatusToJSON)),
        'types': value.types === undefined ? undefined : (value.types.map(_1.ProductTypeToJSON)),
        'deleted': value.deleted,
        'order': value.order,
        'keys': value.keys,
        'sku': value.sku,
        'name': value.name,
        'bundleSKU': value.bundleSKU,
        'inBundle': value.inBundle,
    };
}
exports.ProductsListFilterToJSON = ProductsListFilterToJSON;
