/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountsListFilter } from './';
/**
 *
 * @export
 * @interface AccountsListErrorAllOf
 */
export interface AccountsListErrorAllOf {
    /**
     *
     * @type {AccountsListFilter}
     * @memberof AccountsListErrorAllOf
     */
    filters?: AccountsListFilter;
}
export declare function AccountsListErrorAllOfFromJSON(json: any): AccountsListErrorAllOf;
export declare function AccountsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountsListErrorAllOf;
export declare function AccountsListErrorAllOfToJSON(value?: AccountsListErrorAllOf | null): any;
