/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RefundUpdateResponse
 */
export interface RefundUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof RefundUpdateResponse
     */
    success: RefundUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof RefundUpdateResponse
     */
    code: RefundUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum RefundUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum RefundUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function RefundUpdateResponseFromJSON(json: any): RefundUpdateResponse;
export declare function RefundUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundUpdateResponse;
export declare function RefundUpdateResponseToJSON(value?: RefundUpdateResponse | null): any;
