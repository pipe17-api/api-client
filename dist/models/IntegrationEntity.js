"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationEntityToJSON = exports.IntegrationEntityFromJSONTyped = exports.IntegrationEntityFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationEntityFromJSON(json) {
    return IntegrationEntityFromJSONTyped(json, false);
}
exports.IntegrationEntityFromJSON = IntegrationEntityFromJSON;
function IntegrationEntityFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'name': !runtime_1.exists(json, 'name') ? undefined : _1.EntityNameFromJSON(json['name']),
        'direction': !runtime_1.exists(json, 'direction') ? undefined : _1.DirectionFromJSON(json['direction']),
        'mappingUuid': !runtime_1.exists(json, 'mappingUuid') ? undefined : json['mappingUuid'],
        'routingUuid': !runtime_1.exists(json, 'routingUuid') ? undefined : json['routingUuid'],
        'restrictions': !runtime_1.exists(json, 'restrictions') ? undefined : (json['restrictions'].map(_1.EntityRestrictionsValueFromJSON)),
    };
}
exports.IntegrationEntityFromJSONTyped = IntegrationEntityFromJSONTyped;
function IntegrationEntityToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'name': _1.EntityNameToJSON(value.name),
        'direction': _1.DirectionToJSON(value.direction),
        'mappingUuid': value.mappingUuid,
        'routingUuid': value.routingUuid,
        'restrictions': value.restrictions === undefined ? undefined : (value.restrictions.map(_1.EntityRestrictionsValueToJSON)),
    };
}
exports.IntegrationEntityToJSON = IntegrationEntityToJSON;
