/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingCreateData
 */
export interface RoutingCreateData {
    /**
     * Public Routing
     * @type {boolean}
     * @memberof RoutingCreateData
     */
    isPublic: boolean;
    /**
     * Description
     * @type {string}
     * @memberof RoutingCreateData
     */
    description: string;
    /**
     * UUID
     * @type {string}
     * @memberof RoutingCreateData
     */
    uuid?: string;
    /**
     * Is Routing Filter Enabled
     * @type {boolean}
     * @memberof RoutingCreateData
     */
    enabled: boolean;
    /**
     * Routing Filter
     * @type {string}
     * @memberof RoutingCreateData
     */
    filter: string;
}
export declare function RoutingCreateDataFromJSON(json: any): RoutingCreateData;
export declare function RoutingCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingCreateData;
export declare function RoutingCreateDataToJSON(value?: RoutingCreateData | null): any;
