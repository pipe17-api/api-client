/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountStatus } from './';
/**
 *
 * @export
 * @interface Account
 */
export interface Account {
    /**
     *
     * @type {AccountStatus}
     * @memberof Account
     */
    status?: AccountStatus;
    /**
     * Account User ID
     * @type {string}
     * @memberof Account
     */
    userId?: string;
    /**
     * Account Role IDs
     * @type {Array<string>}
     * @memberof Account
     */
    roleIds?: Array<string>;
    /**
     * Account Integrations
     * @type {Array<string>}
     * @memberof Account
     */
    integrations?: Array<string>;
    /**
     * Account Activation Key
     * @type {string}
     * @memberof Account
     */
    activationKey?: string;
    /**
     *
     * @type {Date}
     * @memberof Account
     */
    expirationDate?: Date;
    /**
     *
     * @type {object}
     * @memberof Account
     */
    payment?: object;
    /**
     * When the object was created within Pipe17
     * @type {Date}
     * @memberof Account
     */
    readonly createdAt?: Date;
    /**
     * When the object was updated within Pipe17
     * @type {Date}
     * @memberof Account
     */
    readonly updatedAt?: Date;
    /**
     * Organization this object belong to
     * @type {string}
     * @memberof Account
     */
    readonly orgKey?: string;
}
export declare function AccountFromJSON(json: any): Account;
export declare function AccountFromJSONTyped(json: any, ignoreDiscriminator: boolean): Account;
export declare function AccountToJSON(value?: Account | null): any;
