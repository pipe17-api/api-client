/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReceiptItem } from './';
/**
 *
 * @export
 * @interface ReceiptCreateData
 */
export interface ReceiptCreateData {
    /**
     * Arrival ID
     * @type {string}
     * @memberof ReceiptCreateData
     */
    arrivalId: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof ReceiptCreateData
     */
    shippingCarrier?: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof ReceiptCreateData
     */
    shippingClass?: string;
    /**
     * Currency
     * @type {string}
     * @memberof ReceiptCreateData
     */
    currency?: string;
    /**
     * Actual arrival date
     * @type {Date}
     * @memberof ReceiptCreateData
     */
    actualArrivalDate?: Date;
    /**
     *
     * @type {Array<ReceiptItem>}
     * @memberof ReceiptCreateData
     */
    lineItems?: Array<ReceiptItem>;
    /**
     * Reference to receipt on external system (vendor)
     * @type {string}
     * @memberof ReceiptCreateData
     */
    extReceiptId?: string;
}
export declare function ReceiptCreateDataFromJSON(json: any): ReceiptCreateData;
export declare function ReceiptCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReceiptCreateData;
export declare function ReceiptCreateDataToJSON(value?: ReceiptCreateData | null): any;
