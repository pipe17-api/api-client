/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentStatus } from './';
/**
 *
 * @export
 * @interface ShipmentsDeleteFilter
 */
export interface ShipmentsDeleteFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ShipmentsDeleteFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ShipmentsDeleteFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ShipmentsDeleteFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ShipmentsDeleteFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ShipmentsDeleteFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ShipmentsDeleteFilter
     */
    count?: number;
    /**
     * Shipments by list of shipmentId
     * @type {Array<string>}
     * @memberof ShipmentsDeleteFilter
     */
    shipmentId?: Array<string>;
    /**
     * Shipments by list of orderId
     * @type {Array<string>}
     * @memberof ShipmentsDeleteFilter
     */
    orderId?: Array<string>;
    /**
     * Shipments by list of order types
     * @type {Array<string>}
     * @memberof ShipmentsDeleteFilter
     */
    orderType?: Array<string>;
    /**
     * Shipments by list of extOrderId
     * @type {Array<string>}
     * @memberof ShipmentsDeleteFilter
     */
    extOrderId?: Array<string>;
    /**
     * Shipments by list of statuses
     * @type {Array<ShipmentStatus>}
     * @memberof ShipmentsDeleteFilter
     */
    status?: Array<ShipmentStatus>;
    /**
     * Soft deleted shipments
     * @type {boolean}
     * @memberof ShipmentsDeleteFilter
     */
    deleted?: boolean;
    /**
     * Shipments of these integrations
     * @type {Array<string>}
     * @memberof ShipmentsDeleteFilter
     */
    integration?: Array<string>;
}
export declare function ShipmentsDeleteFilterFromJSON(json: any): ShipmentsDeleteFilter;
export declare function ShipmentsDeleteFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsDeleteFilter;
export declare function ShipmentsDeleteFilterToJSON(value?: ShipmentsDeleteFilter | null): any;
