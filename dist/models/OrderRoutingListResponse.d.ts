/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRouting, OrderRoutingsListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrderRoutingListResponse
 */
export interface OrderRoutingListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrderRoutingListResponse
     */
    success: OrderRoutingListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingListResponse
     */
    code: OrderRoutingListResponseCodeEnum;
    /**
     *
     * @type {OrderRoutingsListFilter}
     * @memberof OrderRoutingListResponse
     */
    filters?: OrderRoutingsListFilter;
    /**
     *
     * @type {Array<OrderRouting>}
     * @memberof OrderRoutingListResponse
     */
    routings?: Array<OrderRouting>;
    /**
     *
     * @type {Pagination}
     * @memberof OrderRoutingListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrderRoutingListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrderRoutingListResponseFromJSON(json: any): OrderRoutingListResponse;
export declare function OrderRoutingListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingListResponse;
export declare function OrderRoutingListResponseToJSON(value?: OrderRoutingListResponse | null): any;
