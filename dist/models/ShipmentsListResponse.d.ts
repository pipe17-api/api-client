/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Shipment, ShipmentsFilter } from './';
/**
 *
 * @export
 * @interface ShipmentsListResponse
 */
export interface ShipmentsListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ShipmentsListResponse
     */
    success: ShipmentsListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ShipmentsListResponse
     */
    code: ShipmentsListResponseCodeEnum;
    /**
     *
     * @type {ShipmentsFilter}
     * @memberof ShipmentsListResponse
     */
    filters?: ShipmentsFilter;
    /**
     *
     * @type {Array<Shipment>}
     * @memberof ShipmentsListResponse
     */
    shipments: Array<Shipment>;
    /**
     *
     * @type {Pagination}
     * @memberof ShipmentsListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum ShipmentsListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ShipmentsListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ShipmentsListResponseFromJSON(json: any): ShipmentsListResponse;
export declare function ShipmentsListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentsListResponse;
export declare function ShipmentsListResponseToJSON(value?: ShipmentsListResponse | null): any;
