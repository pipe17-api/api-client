/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { FulfillmentCreateResult } from './';
/**
 *
 * @export
 * @interface FulfillmentsCreateResponse
 */
export interface FulfillmentsCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof FulfillmentsCreateResponse
     */
    success: FulfillmentsCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof FulfillmentsCreateResponse
     */
    code: FulfillmentsCreateResponseCodeEnum;
    /**
     *
     * @type {Array<FulfillmentCreateResult>}
     * @memberof FulfillmentsCreateResponse
     */
    fulfillments?: Array<FulfillmentCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum FulfillmentsCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum FulfillmentsCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function FulfillmentsCreateResponseFromJSON(json: any): FulfillmentsCreateResponse;
export declare function FulfillmentsCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): FulfillmentsCreateResponse;
export declare function FulfillmentsCreateResponseToJSON(value?: FulfillmentsCreateResponse | null): any;
