"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiKeyValueAllOfToJSON = exports.ApiKeyValueAllOfFromJSONTyped = exports.ApiKeyValueAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function ApiKeyValueAllOfFromJSON(json) {
    return ApiKeyValueAllOfFromJSONTyped(json, false);
}
exports.ApiKeyValueAllOfFromJSON = ApiKeyValueAllOfFromJSON;
function ApiKeyValueAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'apikeyId': !runtime_1.exists(json, 'apikeyId') ? undefined : json['apikeyId'],
        'apikey': !runtime_1.exists(json, 'apikey') ? undefined : json['apikey'],
    };
}
exports.ApiKeyValueAllOfFromJSONTyped = ApiKeyValueAllOfFromJSONTyped;
function ApiKeyValueAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'apikeyId': value.apikeyId,
        'apikey': value.apikey,
    };
}
exports.ApiKeyValueAllOfToJSON = ApiKeyValueAllOfToJSON;
