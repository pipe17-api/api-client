/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Product Status
 * @export
 * @enum {string}
 */
export declare enum ProductStatus {
    Active = "active",
    Inactive = "inactive"
}
export declare function ProductStatusFromJSON(json: any): ProductStatus;
export declare function ProductStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductStatus;
export declare function ProductStatusToJSON(value?: ProductStatus | null): any;
