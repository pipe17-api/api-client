"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationUpdateErrorAllOfToJSON = exports.OrganizationUpdateErrorAllOfFromJSONTyped = exports.OrganizationUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function OrganizationUpdateErrorAllOfFromJSON(json) {
    return OrganizationUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.OrganizationUpdateErrorAllOfFromJSON = OrganizationUpdateErrorAllOfFromJSON;
function OrganizationUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'organization': !runtime_1.exists(json, 'organization') ? undefined : _1.OrganizationUpdateDataFromJSON(json['organization']),
    };
}
exports.OrganizationUpdateErrorAllOfFromJSONTyped = OrganizationUpdateErrorAllOfFromJSONTyped;
function OrganizationUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'organization': _1.OrganizationUpdateDataToJSON(value.organization),
    };
}
exports.OrganizationUpdateErrorAllOfToJSON = OrganizationUpdateErrorAllOfToJSON;
