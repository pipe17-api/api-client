/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationAddress, LocationExternalSystem, LocationStatus } from './';
/**
 *
 * @export
 * @interface LocationCreateData
 */
export interface LocationCreateData {
    /**
     * Location Name
     * @type {string}
     * @memberof LocationCreateData
     */
    name: string;
    /**
     *
     * @type {LocationAddress}
     * @memberof LocationCreateData
     */
    address: LocationAddress;
    /**
     *
     * @type {LocationStatus}
     * @memberof LocationCreateData
     */
    status?: LocationStatus;
    /**
     * Set if this location has \"infinite\" availability
     * @type {boolean}
     * @memberof LocationCreateData
     */
    infinite?: boolean;
    /**
     * Set if shipments to this location should have lineItems referring to bundles kept intact
     * @type {boolean}
     * @memberof LocationCreateData
     */
    preserveBundles?: boolean;
    /**
     * Set if this is a fulfillment location
     * @type {string}
     * @memberof LocationCreateData
     */
    fulfillmentIntegrationId?: string;
    /**
     *
     * @type {Array<LocationExternalSystem>}
     * @memberof LocationCreateData
     */
    externalSystem?: Array<LocationExternalSystem>;
}
export declare function LocationCreateDataFromJSON(json: any): LocationCreateData;
export declare function LocationCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationCreateData;
export declare function LocationCreateDataToJSON(value?: LocationCreateData | null): any;
