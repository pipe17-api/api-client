/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationsListFilter } from './';
/**
 *
 * @export
 * @interface IntegrationsListErrorAllOf
 */
export interface IntegrationsListErrorAllOf {
    /**
     *
     * @type {IntegrationsListFilter}
     * @memberof IntegrationsListErrorAllOf
     */
    filters?: IntegrationsListFilter;
}
export declare function IntegrationsListErrorAllOfFromJSON(json: any): IntegrationsListErrorAllOf;
export declare function IntegrationsListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationsListErrorAllOf;
export declare function IntegrationsListErrorAllOfToJSON(value?: IntegrationsListErrorAllOf | null): any;
