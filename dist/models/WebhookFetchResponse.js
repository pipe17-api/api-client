"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebhookFetchResponseToJSON = exports.WebhookFetchResponseFromJSONTyped = exports.WebhookFetchResponseFromJSON = exports.WebhookFetchResponseCodeEnum = exports.WebhookFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var WebhookFetchResponseSuccessEnum;
(function (WebhookFetchResponseSuccessEnum) {
    WebhookFetchResponseSuccessEnum["True"] = "true";
})(WebhookFetchResponseSuccessEnum = exports.WebhookFetchResponseSuccessEnum || (exports.WebhookFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var WebhookFetchResponseCodeEnum;
(function (WebhookFetchResponseCodeEnum) {
    WebhookFetchResponseCodeEnum[WebhookFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    WebhookFetchResponseCodeEnum[WebhookFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    WebhookFetchResponseCodeEnum[WebhookFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(WebhookFetchResponseCodeEnum = exports.WebhookFetchResponseCodeEnum || (exports.WebhookFetchResponseCodeEnum = {}));
function WebhookFetchResponseFromJSON(json) {
    return WebhookFetchResponseFromJSONTyped(json, false);
}
exports.WebhookFetchResponseFromJSON = WebhookFetchResponseFromJSON;
function WebhookFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'webhook': !runtime_1.exists(json, 'webhook') ? undefined : _1.WebhookFromJSON(json['webhook']),
    };
}
exports.WebhookFetchResponseFromJSONTyped = WebhookFetchResponseFromJSONTyped;
function WebhookFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'webhook': _1.WebhookToJSON(value.webhook),
    };
}
exports.WebhookFetchResponseToJSON = WebhookFetchResponseToJSON;
