/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface PurchaseFetchError
 */
export interface PurchaseFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof PurchaseFetchError
     */
    success: PurchaseFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchaseFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof PurchaseFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof PurchaseFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchaseFetchErrorSuccessEnum {
    False = "false"
}
export declare function PurchaseFetchErrorFromJSON(json: any): PurchaseFetchError;
export declare function PurchaseFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseFetchError;
export declare function PurchaseFetchErrorToJSON(value?: PurchaseFetchError | null): any;
