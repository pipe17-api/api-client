/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryRule, InventoryRulesListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface InventoryRulesListResponse
 */
export interface InventoryRulesListResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof InventoryRulesListResponse
     */
    success: InventoryRulesListResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryRulesListResponse
     */
    code: InventoryRulesListResponseCodeEnum;
    /**
     *
     * @type {InventoryRulesListFilter}
     * @memberof InventoryRulesListResponse
     */
    filters?: InventoryRulesListFilter;
    /**
     *
     * @type {Array<InventoryRule>}
     * @memberof InventoryRulesListResponse
     */
    rules?: Array<InventoryRule>;
    /**
     *
     * @type {Pagination}
     * @memberof InventoryRulesListResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryRulesListResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum InventoryRulesListResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function InventoryRulesListResponseFromJSON(json: any): InventoryRulesListResponse;
export declare function InventoryRulesListResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRulesListResponse;
export declare function InventoryRulesListResponseToJSON(value?: InventoryRulesListResponse | null): any;
