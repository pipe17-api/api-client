"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EntityFilterFetchResponseToJSON = exports.EntityFilterFetchResponseFromJSONTyped = exports.EntityFilterFetchResponseFromJSON = exports.EntityFilterFetchResponseCodeEnum = exports.EntityFilterFetchResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EntityFilterFetchResponseSuccessEnum;
(function (EntityFilterFetchResponseSuccessEnum) {
    EntityFilterFetchResponseSuccessEnum["True"] = "true";
})(EntityFilterFetchResponseSuccessEnum = exports.EntityFilterFetchResponseSuccessEnum || (exports.EntityFilterFetchResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var EntityFilterFetchResponseCodeEnum;
(function (EntityFilterFetchResponseCodeEnum) {
    EntityFilterFetchResponseCodeEnum[EntityFilterFetchResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    EntityFilterFetchResponseCodeEnum[EntityFilterFetchResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    EntityFilterFetchResponseCodeEnum[EntityFilterFetchResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(EntityFilterFetchResponseCodeEnum = exports.EntityFilterFetchResponseCodeEnum || (exports.EntityFilterFetchResponseCodeEnum = {}));
function EntityFilterFetchResponseFromJSON(json) {
    return EntityFilterFetchResponseFromJSONTyped(json, false);
}
exports.EntityFilterFetchResponseFromJSON = EntityFilterFetchResponseFromJSON;
function EntityFilterFetchResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'entityFilter': !runtime_1.exists(json, 'entityFilter') ? undefined : _1.EntityFilterFromJSON(json['entityFilter']),
    };
}
exports.EntityFilterFetchResponseFromJSONTyped = EntityFilterFetchResponseFromJSONTyped;
function EntityFilterFetchResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'entityFilter': _1.EntityFilterToJSON(value.entityFilter),
    };
}
exports.EntityFilterFetchResponseToJSON = EntityFilterFetchResponseToJSON;
