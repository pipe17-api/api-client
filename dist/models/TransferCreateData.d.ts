/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Address, NameValue, TransferLineItem, TransferStatus } from './';
/**
 *
 * @export
 * @interface TransferCreateData
 */
export interface TransferCreateData {
    /**
     *
     * @type {TransferStatus}
     * @memberof TransferCreateData
     */
    status?: TransferStatus;
    /**
     * External Transfer Order ID
     * @type {string}
     * @memberof TransferCreateData
     */
    extOrderId: string;
    /**
     * Transfer Order Date
     * @type {Date}
     * @memberof TransferCreateData
     */
    transferOrderDate?: Date;
    /**
     *
     * @type {Array<TransferLineItem>}
     * @memberof TransferCreateData
     */
    lineItems?: Array<TransferLineItem>;
    /**
     * Shipping Location ID
     * @type {string}
     * @memberof TransferCreateData
     */
    fromLocationId?: string;
    /**
     *
     * @type {Address}
     * @memberof TransferCreateData
     */
    toAddress?: Address;
    /**
     * Arrival Location ID
     * @type {string}
     * @memberof TransferCreateData
     */
    toLocationId?: string;
    /**
     * Shipping Carrier
     * @type {string}
     * @memberof TransferCreateData
     */
    shippingCarrier?: string;
    /**
     * Shipping Service Type
     * @type {string}
     * @memberof TransferCreateData
     */
    shippingService?: string;
    /**
     * Ship By Date
     * @type {Date}
     * @memberof TransferCreateData
     */
    shipByDate?: Date;
    /**
     * Expected Ship Date
     * @type {Date}
     * @memberof TransferCreateData
     */
    expectedShipDate?: Date;
    /**
     * Expected Arrival Date
     * @type {Date}
     * @memberof TransferCreateData
     */
    expectedArrivalDate?: Date;
    /**
     * Actual Arrival Date (from most recent Arrival)
     * @type {Date}
     * @memberof TransferCreateData
     */
    actualArrivalDate?: Date;
    /**
     * Shipping Notes
     * @type {string}
     * @memberof TransferCreateData
     */
    shippingNotes?: string;
    /**
     * Transfer order created by
     * @type {string}
     * @memberof TransferCreateData
     */
    employeeName?: string;
    /**
     * International Commercial Terms
     * @type {string}
     * @memberof TransferCreateData
     */
    incoterms?: string;
    /**
     * Transfer Notes
     * @type {string}
     * @memberof TransferCreateData
     */
    transferNotes?: string;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof TransferCreateData
     */
    customFields?: Array<NameValue>;
    /**
     * General Purpose Time Stamp
     * @type {Date}
     * @memberof TransferCreateData
     */
    timestamp?: Date;
}
export declare function TransferCreateDataFromJSON(json: any): TransferCreateData;
export declare function TransferCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferCreateData;
export declare function TransferCreateDataToJSON(value?: TransferCreateData | null): any;
