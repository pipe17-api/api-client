/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyAllOf
 */
export interface ApiKeyAllOf {
    /**
     * API key Id
     * @type {string}
     * @memberof ApiKeyAllOf
     */
    apikeyId?: string;
}
export declare function ApiKeyAllOfFromJSON(json: any): ApiKeyAllOf;
export declare function ApiKeyAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyAllOf;
export declare function ApiKeyAllOfToJSON(value?: ApiKeyAllOf | null): any;
