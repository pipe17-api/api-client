/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryRuleUpdateData
 */
export interface InventoryRuleUpdateData {
    /**
     * Description of the rule
     * @type {string}
     * @memberof InventoryRuleUpdateData
     */
    description?: string;
    /**
     * Rule's definition as javascript function
     * @type {string}
     * @memberof InventoryRuleUpdateData
     */
    rule: string;
}
export declare function InventoryRuleUpdateDataFromJSON(json: any): InventoryRuleUpdateData;
export declare function InventoryRuleUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryRuleUpdateData;
export declare function InventoryRuleUpdateDataToJSON(value?: InventoryRuleUpdateData | null): any;
