/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface EventFetchError
 */
export interface EventFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof EventFetchError
     */
    success: EventFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof EventFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof EventFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof EventFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum EventFetchErrorSuccessEnum {
    False = "false"
}
export declare function EventFetchErrorFromJSON(json: any): EventFetchError;
export declare function EventFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventFetchError;
export declare function EventFetchErrorToJSON(value?: EventFetchError | null): any;
