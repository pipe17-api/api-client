/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeysFilter
 */
export interface ApiKeysFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof ApiKeysFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof ApiKeysFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof ApiKeysFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof ApiKeysFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof ApiKeysFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof ApiKeysFilter
     */
    count?: number;
    /**
     * List API Keys by matching connector id(s)
     * @type {Array<string>}
     * @memberof ApiKeysFilter
     */
    connector?: Array<string>;
    /**
     * List API Keys by matching integration id(s)
     * @type {Array<string>}
     * @memberof ApiKeysFilter
     */
    integration?: Array<string>;
    /**
     * List API Keys by matching name(s)
     * @type {string}
     * @memberof ApiKeysFilter
     */
    name?: string;
}
export declare function ApiKeysFilterFromJSON(json: any): ApiKeysFilter;
export declare function ApiKeysFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysFilter;
export declare function ApiKeysFilterToJSON(value?: ApiKeysFilter | null): any;
