/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Supplier, SuppliersDeleteFilter } from './';
/**
 *
 * @export
 * @interface SuppliersDeleteResponseAllOf
 */
export interface SuppliersDeleteResponseAllOf {
    /**
     *
     * @type {SuppliersDeleteFilter}
     * @memberof SuppliersDeleteResponseAllOf
     */
    filters?: SuppliersDeleteFilter;
    /**
     *
     * @type {Array<Supplier>}
     * @memberof SuppliersDeleteResponseAllOf
     */
    suppliers?: Array<Supplier>;
    /**
     * Number of deleted suppliers
     * @type {number}
     * @memberof SuppliersDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof SuppliersDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function SuppliersDeleteResponseAllOfFromJSON(json: any): SuppliersDeleteResponseAllOf;
export declare function SuppliersDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersDeleteResponseAllOf;
export declare function SuppliersDeleteResponseAllOfToJSON(value?: SuppliersDeleteResponseAllOf | null): any;
