"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsListErrorAllOfToJSON = exports.RefundsListErrorAllOfFromJSONTyped = exports.RefundsListErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundsListErrorAllOfFromJSON(json) {
    return RefundsListErrorAllOfFromJSONTyped(json, false);
}
exports.RefundsListErrorAllOfFromJSON = RefundsListErrorAllOfFromJSON;
function RefundsListErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RefundsFilterFromJSON(json['filters']),
    };
}
exports.RefundsListErrorAllOfFromJSONTyped = RefundsListErrorAllOfFromJSONTyped;
function RefundsListErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RefundsFilterToJSON(value.filters),
    };
}
exports.RefundsListErrorAllOfToJSON = RefundsListErrorAllOfToJSON;
