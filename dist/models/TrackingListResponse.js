"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackingListResponseToJSON = exports.TrackingListResponseFromJSONTyped = exports.TrackingListResponseFromJSON = exports.TrackingListResponseCodeEnum = exports.TrackingListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var TrackingListResponseSuccessEnum;
(function (TrackingListResponseSuccessEnum) {
    TrackingListResponseSuccessEnum["True"] = "true";
})(TrackingListResponseSuccessEnum = exports.TrackingListResponseSuccessEnum || (exports.TrackingListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var TrackingListResponseCodeEnum;
(function (TrackingListResponseCodeEnum) {
    TrackingListResponseCodeEnum[TrackingListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    TrackingListResponseCodeEnum[TrackingListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    TrackingListResponseCodeEnum[TrackingListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(TrackingListResponseCodeEnum = exports.TrackingListResponseCodeEnum || (exports.TrackingListResponseCodeEnum = {}));
function TrackingListResponseFromJSON(json) {
    return TrackingListResponseFromJSONTyped(json, false);
}
exports.TrackingListResponseFromJSON = TrackingListResponseFromJSON;
function TrackingListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.TrackingFilterFromJSON(json['filters']),
        'trackings': !runtime_1.exists(json, 'trackings') ? undefined : (json['trackings'].map(_1.TrackingFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.TrackingListResponseFromJSONTyped = TrackingListResponseFromJSONTyped;
function TrackingListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.TrackingFilterToJSON(value.filters),
        'trackings': value.trackings === undefined ? undefined : (value.trackings.map(_1.TrackingToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.TrackingListResponseToJSON = TrackingListResponseToJSON;
