"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryRuleUpdateRequestToJSON = exports.InventoryRuleUpdateRequestFromJSONTyped = exports.InventoryRuleUpdateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryRuleUpdateRequestFromJSON(json) {
    return InventoryRuleUpdateRequestFromJSONTyped(json, false);
}
exports.InventoryRuleUpdateRequestFromJSON = InventoryRuleUpdateRequestFromJSON;
function InventoryRuleUpdateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'rule': json['rule'],
    };
}
exports.InventoryRuleUpdateRequestFromJSONTyped = InventoryRuleUpdateRequestFromJSONTyped;
function InventoryRuleUpdateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'description': value.description,
        'rule': value.rule,
    };
}
exports.InventoryRuleUpdateRequestToJSON = InventoryRuleUpdateRequestToJSON;
