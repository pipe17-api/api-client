/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ShipmentStatus } from './';
/**
 *
 * @export
 * @interface ShipmentAllOf
 */
export interface ShipmentAllOf {
    /**
     * Shipment ID
     * @type {string}
     * @memberof ShipmentAllOf
     */
    shipmentId?: string;
    /**
     *
     * @type {ShipmentStatus}
     * @memberof ShipmentAllOf
     */
    status?: ShipmentStatus;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof ShipmentAllOf
     */
    integration?: string;
    /**
     * Sent to fulfillment timestamp
     * @type {Date}
     * @memberof ShipmentAllOf
     */
    sentToFulfillmentAt?: Date;
}
export declare function ShipmentAllOfFromJSON(json: any): ShipmentAllOf;
export declare function ShipmentAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentAllOf;
export declare function ShipmentAllOfToJSON(value?: ShipmentAllOf | null): any;
