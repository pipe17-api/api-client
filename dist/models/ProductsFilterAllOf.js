"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductsFilterAllOfToJSON = exports.ProductsFilterAllOfFromJSONTyped = exports.ProductsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ProductsFilterAllOfFromJSON(json) {
    return ProductsFilterAllOfFromJSONTyped(json, false);
}
exports.ProductsFilterAllOfFromJSON = ProductsFilterAllOfFromJSON;
function ProductsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'productId': !runtime_1.exists(json, 'productId') ? undefined : json['productId'],
        'extProductId': !runtime_1.exists(json, 'extProductId') ? undefined : json['extProductId'],
        'parentExtProductId': !runtime_1.exists(json, 'parentExtProductId') ? undefined : json['parentExtProductId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : (json['status'].map(_1.ProductStatusFromJSON)),
        'types': !runtime_1.exists(json, 'types') ? undefined : (json['types'].map(_1.ProductTypeFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.ProductsFilterAllOfFromJSONTyped = ProductsFilterAllOfFromJSONTyped;
function ProductsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'productId': value.productId,
        'extProductId': value.extProductId,
        'parentExtProductId': value.parentExtProductId,
        'status': value.status === undefined ? undefined : (value.status.map(_1.ProductStatusToJSON)),
        'types': value.types === undefined ? undefined : (value.types.map(_1.ProductTypeToJSON)),
        'deleted': value.deleted,
    };
}
exports.ProductsFilterAllOfToJSON = ProductsFilterAllOfToJSON;
