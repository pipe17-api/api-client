/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface OrderFetchError
 */
export interface OrderFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof OrderFetchError
     */
    success: OrderFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof OrderFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof OrderFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderFetchErrorSuccessEnum {
    False = "false"
}
export declare function OrderFetchErrorFromJSON(json: any): OrderFetchError;
export declare function OrderFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderFetchError;
export declare function OrderFetchErrorToJSON(value?: OrderFetchError | null): any;
