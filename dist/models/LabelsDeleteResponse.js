"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LabelsDeleteResponseToJSON = exports.LabelsDeleteResponseFromJSONTyped = exports.LabelsDeleteResponseFromJSON = exports.LabelsDeleteResponseCodeEnum = exports.LabelsDeleteResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LabelsDeleteResponseSuccessEnum;
(function (LabelsDeleteResponseSuccessEnum) {
    LabelsDeleteResponseSuccessEnum["True"] = "true";
})(LabelsDeleteResponseSuccessEnum = exports.LabelsDeleteResponseSuccessEnum || (exports.LabelsDeleteResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LabelsDeleteResponseCodeEnum;
(function (LabelsDeleteResponseCodeEnum) {
    LabelsDeleteResponseCodeEnum[LabelsDeleteResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LabelsDeleteResponseCodeEnum[LabelsDeleteResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LabelsDeleteResponseCodeEnum[LabelsDeleteResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LabelsDeleteResponseCodeEnum = exports.LabelsDeleteResponseCodeEnum || (exports.LabelsDeleteResponseCodeEnum = {}));
function LabelsDeleteResponseFromJSON(json) {
    return LabelsDeleteResponseFromJSONTyped(json, false);
}
exports.LabelsDeleteResponseFromJSON = LabelsDeleteResponseFromJSON;
function LabelsDeleteResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LabelsDeleteFilterFromJSON(json['filters']),
        'labels': !runtime_1.exists(json, 'labels') ? undefined : (json['labels'].map(_1.LabelFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.LabelsDeleteResponseFromJSONTyped = LabelsDeleteResponseFromJSONTyped;
function LabelsDeleteResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.LabelsDeleteFilterToJSON(value.filters),
        'labels': value.labels === undefined ? undefined : (value.labels.map(_1.LabelToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.LabelsDeleteResponseToJSON = LabelsDeleteResponseToJSON;
