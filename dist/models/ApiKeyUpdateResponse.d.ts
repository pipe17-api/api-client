/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ApiKeyUpdateResponse
 */
export interface ApiKeyUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ApiKeyUpdateResponse
     */
    success: ApiKeyUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ApiKeyUpdateResponse
     */
    code: ApiKeyUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum ApiKeyUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ApiKeyUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ApiKeyUpdateResponseFromJSON(json: any): ApiKeyUpdateResponse;
export declare function ApiKeyUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeyUpdateResponse;
export declare function ApiKeyUpdateResponseToJSON(value?: ApiKeyUpdateResponse | null): any;
