/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationConnection, IntegrationEntity, IntegrationSettings, IntegrationStatus } from './';
/**
 *
 * @export
 * @interface IntegrationUpdateRequest
 */
export interface IntegrationUpdateRequest {
    /**
     *
     * @type {IntegrationStatus}
     * @memberof IntegrationUpdateRequest
     */
    status?: IntegrationStatus;
    /**
     * Update integration api key
     * @type {boolean}
     * @memberof IntegrationUpdateRequest
     */
    apikey?: boolean;
    /**
     * Integration Name
     * @type {string}
     * @memberof IntegrationUpdateRequest
     */
    integrationName?: string;
    /**
     *
     * @type {Array<IntegrationEntity>}
     * @memberof IntegrationUpdateRequest
     */
    entities?: Array<IntegrationEntity>;
    /**
     *
     * @type {IntegrationSettings}
     * @memberof IntegrationUpdateRequest
     */
    settings?: IntegrationSettings;
    /**
     *
     * @type {IntegrationConnection}
     * @memberof IntegrationUpdateRequest
     */
    connection?: IntegrationConnection;
    /**
     * Error message
     * @type {string}
     * @memberof IntegrationUpdateRequest
     */
    errorText?: string | null;
}
export declare function IntegrationUpdateRequestFromJSON(json: any): IntegrationUpdateRequest;
export declare function IntegrationUpdateRequestFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationUpdateRequest;
export declare function IntegrationUpdateRequestToJSON(value?: IntegrationUpdateRequest | null): any;
