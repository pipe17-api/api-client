"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductUpdateDataToJSON = exports.ProductUpdateDataFromJSONTyped = exports.ProductUpdateDataFromJSON = exports.ProductUpdateDataDimensionsUnitEnum = exports.ProductUpdateDataWeightUnitEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var ProductUpdateDataWeightUnitEnum;
(function (ProductUpdateDataWeightUnitEnum) {
    ProductUpdateDataWeightUnitEnum["Lb"] = "lb";
    ProductUpdateDataWeightUnitEnum["Oz"] = "oz";
    ProductUpdateDataWeightUnitEnum["Kg"] = "kg";
    ProductUpdateDataWeightUnitEnum["G"] = "g";
})(ProductUpdateDataWeightUnitEnum = exports.ProductUpdateDataWeightUnitEnum || (exports.ProductUpdateDataWeightUnitEnum = {})); /**
* @export
* @enum {string}
*/
var ProductUpdateDataDimensionsUnitEnum;
(function (ProductUpdateDataDimensionsUnitEnum) {
    ProductUpdateDataDimensionsUnitEnum["Cm"] = "cm";
    ProductUpdateDataDimensionsUnitEnum["In"] = "in";
    ProductUpdateDataDimensionsUnitEnum["Ft"] = "ft";
})(ProductUpdateDataDimensionsUnitEnum = exports.ProductUpdateDataDimensionsUnitEnum || (exports.ProductUpdateDataDimensionsUnitEnum = {}));
function ProductUpdateDataFromJSON(json) {
    return ProductUpdateDataFromJSONTyped(json, false);
}
exports.ProductUpdateDataFromJSON = ProductUpdateDataFromJSON;
function ProductUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'name': !runtime_1.exists(json, 'name') ? undefined : json['name'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ProductStatusFromJSON(json['status']),
        'description': !runtime_1.exists(json, 'description') ? undefined : json['description'],
        'types': !runtime_1.exists(json, 'types') ? undefined : (json['types'].map(_1.ProductTypeFromJSON)),
        'extProductApiId': !runtime_1.exists(json, 'extProductApiId') ? undefined : json['extProductApiId'],
        'upc': !runtime_1.exists(json, 'upc') ? undefined : json['upc'],
        'partId': !runtime_1.exists(json, 'partId') ? undefined : json['partId'],
        'prices': !runtime_1.exists(json, 'prices') ? undefined : (json['prices'].map(_1.PriceFromJSON)),
        'imageURLs': !runtime_1.exists(json, 'imageURLs') ? undefined : json['imageURLs'],
        'cost': !runtime_1.exists(json, 'cost') ? undefined : json['cost'],
        'costCurrency': !runtime_1.exists(json, 'costCurrency') ? undefined : json['costCurrency'],
        'taxable': !runtime_1.exists(json, 'taxable') ? undefined : json['taxable'],
        'countryOfOrigin': !runtime_1.exists(json, 'countryOfOrigin') ? undefined : json['countryOfOrigin'],
        'harmonizedCode': !runtime_1.exists(json, 'harmonizedCode') ? undefined : json['harmonizedCode'],
        'weight': !runtime_1.exists(json, 'weight') ? undefined : json['weight'],
        'weightUnit': !runtime_1.exists(json, 'weightUnit') ? undefined : json['weightUnit'],
        'height': !runtime_1.exists(json, 'height') ? undefined : json['height'],
        'length': !runtime_1.exists(json, 'length') ? undefined : json['length'],
        'width': !runtime_1.exists(json, 'width') ? undefined : json['width'],
        'dimensionsUnit': !runtime_1.exists(json, 'dimensionsUnit') ? undefined : json['dimensionsUnit'],
        'vendor': !runtime_1.exists(json, 'vendor') ? undefined : json['vendor'],
        'tags': !runtime_1.exists(json, 'tags') ? undefined : json['tags'],
        'parentExtProductId': !runtime_1.exists(json, 'parentExtProductId') ? undefined : json['parentExtProductId'],
        'variantOptions': !runtime_1.exists(json, 'variantOptions') ? undefined : (json['variantOptions'].map(_1.NameValueFromJSON)),
        'extProductCreatedAt': !runtime_1.exists(json, 'extProductCreatedAt') ? undefined : (json['extProductCreatedAt'] === null ? null : new Date(json['extProductCreatedAt'])),
        'extProductUpdatedAt': !runtime_1.exists(json, 'extProductUpdatedAt') ? undefined : (json['extProductUpdatedAt'] === null ? null : new Date(json['extProductUpdatedAt'])),
        'bundleItems': !runtime_1.exists(json, 'bundleItems') ? undefined : (json['bundleItems'] === null ? null : json['bundleItems'].map(_1.ProductBundleItemFromJSON)),
        'customFields': !runtime_1.exists(json, 'customFields') ? undefined : (json['customFields'].map(_1.NameValueFromJSON)),
        'published': !runtime_1.exists(json, 'published') ? undefined : (json['published'].map(_1.ProductPublishedItemFromJSON)),
        'inventoryNotTracked': !runtime_1.exists(json, 'inventoryNotTracked') ? undefined : json['inventoryNotTracked'],
        'hideInStore': !runtime_1.exists(json, 'hideInStore') ? undefined : json['hideInStore'],
    };
}
exports.ProductUpdateDataFromJSONTyped = ProductUpdateDataFromJSONTyped;
function ProductUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'sku': value.sku,
        'name': value.name,
        'status': _1.ProductStatusToJSON(value.status),
        'description': value.description,
        'types': value.types === undefined ? undefined : (value.types.map(_1.ProductTypeToJSON)),
        'extProductApiId': value.extProductApiId,
        'upc': value.upc,
        'partId': value.partId,
        'prices': value.prices === undefined ? undefined : (value.prices.map(_1.PriceToJSON)),
        'imageURLs': value.imageURLs,
        'cost': value.cost,
        'costCurrency': value.costCurrency,
        'taxable': value.taxable,
        'countryOfOrigin': value.countryOfOrigin,
        'harmonizedCode': value.harmonizedCode,
        'weight': value.weight,
        'weightUnit': value.weightUnit,
        'height': value.height,
        'length': value.length,
        'width': value.width,
        'dimensionsUnit': value.dimensionsUnit,
        'vendor': value.vendor,
        'tags': value.tags,
        'parentExtProductId': value.parentExtProductId,
        'variantOptions': value.variantOptions === undefined ? undefined : (value.variantOptions.map(_1.NameValueToJSON)),
        'extProductCreatedAt': value.extProductCreatedAt === undefined ? undefined : (value.extProductCreatedAt === null ? null : new Date(value.extProductCreatedAt).toISOString()),
        'extProductUpdatedAt': value.extProductUpdatedAt === undefined ? undefined : (value.extProductUpdatedAt === null ? null : new Date(value.extProductUpdatedAt).toISOString()),
        'bundleItems': value.bundleItems === undefined ? undefined : (value.bundleItems === null ? null : value.bundleItems.map(_1.ProductBundleItemToJSON)),
        'customFields': value.customFields === undefined ? undefined : (value.customFields.map(_1.NameValueToJSON)),
        'published': value.published === undefined ? undefined : (value.published.map(_1.ProductPublishedItemToJSON)),
        'inventoryNotTracked': value.inventoryNotTracked,
        'hideInStore': value.hideInStore,
    };
}
exports.ProductUpdateDataToJSON = ProductUpdateDataToJSON;
