"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentCreateResultToJSON = exports.FulfillmentCreateResultFromJSONTyped = exports.FulfillmentCreateResultFromJSON = exports.FulfillmentCreateResultStatusEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var FulfillmentCreateResultStatusEnum;
(function (FulfillmentCreateResultStatusEnum) {
    FulfillmentCreateResultStatusEnum["Submitted"] = "submitted";
    FulfillmentCreateResultStatusEnum["Failed"] = "failed";
})(FulfillmentCreateResultStatusEnum = exports.FulfillmentCreateResultStatusEnum || (exports.FulfillmentCreateResultStatusEnum = {}));
function FulfillmentCreateResultFromJSON(json) {
    return FulfillmentCreateResultFromJSONTyped(json, false);
}
exports.FulfillmentCreateResultFromJSON = FulfillmentCreateResultFromJSON;
function FulfillmentCreateResultFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'code': !runtime_1.exists(json, 'code') ? undefined : json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'fulfillment': !runtime_1.exists(json, 'fulfillment') ? undefined : _1.FulfillmentFromJSON(json['fulfillment']),
    };
}
exports.FulfillmentCreateResultFromJSONTyped = FulfillmentCreateResultFromJSONTyped;
function FulfillmentCreateResultToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'status': value.status,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'fulfillment': _1.FulfillmentToJSON(value.fulfillment),
    };
}
exports.FulfillmentCreateResultToJSON = FulfillmentCreateResultToJSON;
