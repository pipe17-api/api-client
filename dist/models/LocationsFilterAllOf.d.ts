/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { LocationStatus } from './';
/**
 * Locations Filter
 * @export
 * @interface LocationsFilterAllOf
 */
export interface LocationsFilterAllOf {
    /**
     * Locations by list of locationId
     * @type {Array<string>}
     * @memberof LocationsFilterAllOf
     */
    locationId?: Array<string>;
    /**
     *
     * @type {Array<LocationStatus>}
     * @memberof LocationsFilterAllOf
     */
    status?: Array<LocationStatus>;
    /**
     * Soft deleted locations
     * @type {boolean}
     * @memberof LocationsFilterAllOf
     */
    deleted?: boolean;
}
export declare function LocationsFilterAllOfFromJSON(json: any): LocationsFilterAllOf;
export declare function LocationsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): LocationsFilterAllOf;
export declare function LocationsFilterAllOfToJSON(value?: LocationsFilterAllOf | null): any;
