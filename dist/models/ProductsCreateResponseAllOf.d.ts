/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductCreateResult } from './';
/**
 *
 * @export
 * @interface ProductsCreateResponseAllOf
 */
export interface ProductsCreateResponseAllOf {
    /**
     *
     * @type {Array<ProductCreateResult>}
     * @memberof ProductsCreateResponseAllOf
     */
    products?: Array<ProductCreateResult>;
}
export declare function ProductsCreateResponseAllOfFromJSON(json: any): ProductsCreateResponseAllOf;
export declare function ProductsCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductsCreateResponseAllOf;
export declare function ProductsCreateResponseAllOfToJSON(value?: ProductsCreateResponseAllOf | null): any;
