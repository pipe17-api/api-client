/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RefundAllOf
 */
export interface RefundAllOf {
    /**
     * Refund ID
     * @type {string}
     * @memberof RefundAllOf
     */
    refundId?: string;
    /**
     * Reference to integration that created an item
     * @type {string}
     * @memberof RefundAllOf
     */
    integration?: string;
}
export declare function RefundAllOfFromJSON(json: any): RefundAllOf;
export declare function RefundAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundAllOf;
export declare function RefundAllOfToJSON(value?: RefundAllOf | null): any;
