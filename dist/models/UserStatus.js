"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserStatusToJSON = exports.UserStatusFromJSONTyped = exports.UserStatusFromJSON = exports.UserStatus = void 0;
/**
 * User Status
 * @export
 * @enum {string}
 */
var UserStatus;
(function (UserStatus) {
    UserStatus["Pending"] = "pending";
    UserStatus["Active"] = "active";
    UserStatus["Disabled"] = "disabled";
})(UserStatus = exports.UserStatus || (exports.UserStatus = {}));
function UserStatusFromJSON(json) {
    return UserStatusFromJSONTyped(json, false);
}
exports.UserStatusFromJSON = UserStatusFromJSON;
function UserStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.UserStatusFromJSONTyped = UserStatusFromJSONTyped;
function UserStatusToJSON(value) {
    return value;
}
exports.UserStatusToJSON = UserStatusToJSON;
