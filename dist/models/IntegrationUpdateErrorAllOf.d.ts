/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationUpdateData } from './';
/**
 *
 * @export
 * @interface IntegrationUpdateErrorAllOf
 */
export interface IntegrationUpdateErrorAllOf {
    /**
     *
     * @type {IntegrationUpdateData}
     * @memberof IntegrationUpdateErrorAllOf
     */
    integration?: IntegrationUpdateData;
}
export declare function IntegrationUpdateErrorAllOfFromJSON(json: any): IntegrationUpdateErrorAllOf;
export declare function IntegrationUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationUpdateErrorAllOf;
export declare function IntegrationUpdateErrorAllOfToJSON(value?: IntegrationUpdateErrorAllOf | null): any;
