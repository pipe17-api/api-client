"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionUpdateErrorAllOfToJSON = exports.ExceptionUpdateErrorAllOfFromJSONTyped = exports.ExceptionUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionUpdateErrorAllOfFromJSON(json) {
    return ExceptionUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.ExceptionUpdateErrorAllOfFromJSON = ExceptionUpdateErrorAllOfFromJSON;
function ExceptionUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exception': !runtime_1.exists(json, 'exception') ? undefined : _1.ExceptionUpdateDataFromJSON(json['exception']),
    };
}
exports.ExceptionUpdateErrorAllOfFromJSONTyped = ExceptionUpdateErrorAllOfFromJSONTyped;
function ExceptionUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exception': _1.ExceptionUpdateDataToJSON(value.exception),
    };
}
exports.ExceptionUpdateErrorAllOfToJSON = ExceptionUpdateErrorAllOfToJSON;
