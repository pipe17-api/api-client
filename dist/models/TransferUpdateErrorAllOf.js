"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferUpdateErrorAllOfToJSON = exports.TransferUpdateErrorAllOfFromJSONTyped = exports.TransferUpdateErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function TransferUpdateErrorAllOfFromJSON(json) {
    return TransferUpdateErrorAllOfFromJSONTyped(json, false);
}
exports.TransferUpdateErrorAllOfFromJSON = TransferUpdateErrorAllOfFromJSON;
function TransferUpdateErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'transfer': !runtime_1.exists(json, 'transfer') ? undefined : _1.TransferUpdateDataFromJSON(json['transfer']),
    };
}
exports.TransferUpdateErrorAllOfFromJSONTyped = TransferUpdateErrorAllOfFromJSONTyped;
function TransferUpdateErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'transfer': _1.TransferUpdateDataToJSON(value.transfer),
    };
}
exports.TransferUpdateErrorAllOfToJSON = TransferUpdateErrorAllOfToJSON;
