/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { SuppliersDeleteFilter } from './';
/**
 *
 * @export
 * @interface SuppliersDeleteErrorAllOf
 */
export interface SuppliersDeleteErrorAllOf {
    /**
     *
     * @type {SuppliersDeleteFilter}
     * @memberof SuppliersDeleteErrorAllOf
     */
    filters?: SuppliersDeleteFilter;
}
export declare function SuppliersDeleteErrorAllOfFromJSON(json: any): SuppliersDeleteErrorAllOf;
export declare function SuppliersDeleteErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): SuppliersDeleteErrorAllOf;
export declare function SuppliersDeleteErrorAllOfToJSON(value?: SuppliersDeleteErrorAllOf | null): any;
