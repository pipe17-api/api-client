/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Role status
 * @export
 * @enum {string}
 */
export declare enum RoleStatus {
    Active = "active",
    Disabled = "disabled"
}
export declare function RoleStatusFromJSON(json: any): RoleStatus;
export declare function RoleStatusFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleStatus;
export declare function RoleStatusToJSON(value?: RoleStatus | null): any;
