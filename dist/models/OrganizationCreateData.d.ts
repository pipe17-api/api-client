/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrganizationAddress, OrganizationServiceTier } from './';
/**
 *
 * @export
 * @interface OrganizationCreateData
 */
export interface OrganizationCreateData {
    /**
     * Organization Name
     * @type {string}
     * @memberof OrganizationCreateData
     */
    name: string;
    /**
     *
     * @type {OrganizationServiceTier}
     * @memberof OrganizationCreateData
     */
    tier: OrganizationServiceTier;
    /**
     * Organization Contact email
     * @type {string}
     * @memberof OrganizationCreateData
     */
    email: string;
    /**
     * Organization Contact Phone
     * @type {string}
     * @memberof OrganizationCreateData
     */
    phone: string;
    /**
     * Organization Time Zone
     * @type {string}
     * @memberof OrganizationCreateData
     */
    timeZone: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof OrganizationCreateData
     */
    logoUrl?: string;
    /**
     *
     * @type {OrganizationAddress}
     * @memberof OrganizationCreateData
     */
    address: OrganizationAddress;
}
export declare function OrganizationCreateDataFromJSON(json: any): OrganizationCreateData;
export declare function OrganizationCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrganizationCreateData;
export declare function OrganizationCreateDataToJSON(value?: OrganizationCreateData | null): any;
