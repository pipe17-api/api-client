"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConvertRequestRulesToJSON = exports.ConvertRequestRulesFromJSONTyped = exports.ConvertRequestRulesFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ConvertRequestRulesFromJSON(json) {
    return ConvertRequestRulesFromJSONTyped(json, false);
}
exports.ConvertRequestRulesFromJSON = ConvertRequestRulesFromJSON;
function ConvertRequestRulesFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingRuleFromJSON)),
        'mappingId': !runtime_1.exists(json, 'mappingId') ? undefined : json['mappingId'],
    };
}
exports.ConvertRequestRulesFromJSONTyped = ConvertRequestRulesFromJSONTyped;
function ConvertRequestRulesToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingRuleToJSON)),
        'mappingId': value.mappingId,
    };
}
exports.ConvertRequestRulesToJSON = ConvertRequestRulesToJSON;
