"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.MappingCreateRequestToJSON = exports.MappingCreateRequestFromJSONTyped = exports.MappingCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function MappingCreateRequestFromJSON(json) {
    return MappingCreateRequestFromJSONTyped(json, false);
}
exports.MappingCreateRequestFromJSON = MappingCreateRequestFromJSON;
function MappingCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'isPublic': json['isPublic'],
        'description': json['description'],
        'mappings': !runtime_1.exists(json, 'mappings') ? undefined : (json['mappings'].map(_1.MappingRuleFromJSON)),
        'uuid': !runtime_1.exists(json, 'uuid') ? undefined : json['uuid'],
        'originId': !runtime_1.exists(json, 'originId') ? undefined : json['originId'],
    };
}
exports.MappingCreateRequestFromJSONTyped = MappingCreateRequestFromJSONTyped;
function MappingCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'isPublic': value.isPublic,
        'description': value.description,
        'mappings': value.mappings === undefined ? undefined : (value.mappings.map(_1.MappingRuleToJSON)),
        'uuid': value.uuid,
        'originId': value.originId,
    };
}
exports.MappingCreateRequestToJSON = MappingCreateRequestToJSON;
