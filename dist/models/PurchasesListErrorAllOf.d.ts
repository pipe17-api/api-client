/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { PurchasesListFilter } from './';
/**
 *
 * @export
 * @interface PurchasesListErrorAllOf
 */
export interface PurchasesListErrorAllOf {
    /**
     *
     * @type {PurchasesListFilter}
     * @memberof PurchasesListErrorAllOf
     */
    filters?: PurchasesListFilter;
}
export declare function PurchasesListErrorAllOfFromJSON(json: any): PurchasesListErrorAllOf;
export declare function PurchasesListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchasesListErrorAllOf;
export declare function PurchasesListErrorAllOfToJSON(value?: PurchasesListErrorAllOf | null): any;
