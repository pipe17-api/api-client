"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventCreateErrorToJSON = exports.EventCreateErrorFromJSONTyped = exports.EventCreateErrorFromJSON = exports.EventCreateErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var EventCreateErrorSuccessEnum;
(function (EventCreateErrorSuccessEnum) {
    EventCreateErrorSuccessEnum["False"] = "false";
})(EventCreateErrorSuccessEnum = exports.EventCreateErrorSuccessEnum || (exports.EventCreateErrorSuccessEnum = {}));
function EventCreateErrorFromJSON(json) {
    return EventCreateErrorFromJSONTyped(json, false);
}
exports.EventCreateErrorFromJSON = EventCreateErrorFromJSON;
function EventCreateErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'event': !runtime_1.exists(json, 'event') ? undefined : _1.EventFromJSON(json['event']),
    };
}
exports.EventCreateErrorFromJSONTyped = EventCreateErrorFromJSONTyped;
function EventCreateErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'event': _1.EventToJSON(value.event),
    };
}
exports.EventCreateErrorToJSON = EventCreateErrorToJSON;
