"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReturnCreateRequestToJSON = exports.ReturnCreateRequestFromJSONTyped = exports.ReturnCreateRequestFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ReturnCreateRequestFromJSON(json) {
    return ReturnCreateRequestFromJSONTyped(json, false);
}
exports.ReturnCreateRequestFromJSON = ReturnCreateRequestFromJSON;
function ReturnCreateRequestFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'extReturnId': !runtime_1.exists(json, 'extReturnId') ? undefined : json['extReturnId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ReturnStatusFromJSON(json['status']),
        'lineItems': !runtime_1.exists(json, 'lineItems') ? undefined : (json['lineItems'].map(_1.ReturnLineItemFromJSON)),
        'shippingAddress': !runtime_1.exists(json, 'shippingAddress') ? undefined : _1.AddressFromJSON(json['shippingAddress']),
        'currency': !runtime_1.exists(json, 'currency') ? undefined : json['currency'],
        'customerNotes': !runtime_1.exists(json, 'customerNotes') ? undefined : json['customerNotes'],
        'notes': !runtime_1.exists(json, 'notes') ? undefined : json['notes'],
        'tax': !runtime_1.exists(json, 'tax') ? undefined : json['tax'],
        'discount': !runtime_1.exists(json, 'discount') ? undefined : json['discount'],
        'subTotal': !runtime_1.exists(json, 'subTotal') ? undefined : json['subTotal'],
        'total': !runtime_1.exists(json, 'total') ? undefined : json['total'],
        'shippingPrice': !runtime_1.exists(json, 'shippingPrice') ? undefined : json['shippingPrice'],
        'shippingRefund': !runtime_1.exists(json, 'shippingRefund') ? undefined : json['shippingRefund'],
        'shippingQuote': !runtime_1.exists(json, 'shippingQuote') ? undefined : json['shippingQuote'],
        'shippingLabelFee': !runtime_1.exists(json, 'shippingLabelFee') ? undefined : json['shippingLabelFee'],
        'restockingFee': !runtime_1.exists(json, 'restockingFee') ? undefined : json['restockingFee'],
        'estimatedTotal': !runtime_1.exists(json, 'estimatedTotal') ? undefined : json['estimatedTotal'],
        'isExchange': !runtime_1.exists(json, 'isExchange') ? undefined : json['isExchange'],
        'isGift': !runtime_1.exists(json, 'isGift') ? undefined : json['isGift'],
        'requiresShipping': !runtime_1.exists(json, 'requiresShipping') ? undefined : json['requiresShipping'],
        'refundedAt': !runtime_1.exists(json, 'refundedAt') ? undefined : (new Date(json['refundedAt'])),
    };
}
exports.ReturnCreateRequestFromJSONTyped = ReturnCreateRequestFromJSONTyped;
function ReturnCreateRequestToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'extReturnId': value.extReturnId,
        'status': _1.ReturnStatusToJSON(value.status),
        'lineItems': value.lineItems === undefined ? undefined : (value.lineItems.map(_1.ReturnLineItemToJSON)),
        'shippingAddress': _1.AddressToJSON(value.shippingAddress),
        'currency': value.currency,
        'customerNotes': value.customerNotes,
        'notes': value.notes,
        'tax': value.tax,
        'discount': value.discount,
        'subTotal': value.subTotal,
        'total': value.total,
        'shippingPrice': value.shippingPrice,
        'shippingRefund': value.shippingRefund,
        'shippingQuote': value.shippingQuote,
        'shippingLabelFee': value.shippingLabelFee,
        'restockingFee': value.restockingFee,
        'estimatedTotal': value.estimatedTotal,
        'isExchange': value.isExchange,
        'isGift': value.isGift,
        'requiresShipping': value.requiresShipping,
        'refundedAt': value.refundedAt === undefined ? undefined : (new Date(value.refundedAt).toISOString()),
    };
}
exports.ReturnCreateRequestToJSON = ReturnCreateRequestToJSON;
