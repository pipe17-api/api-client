"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderRoutingUpdateDataToJSON = exports.OrderRoutingUpdateDataFromJSONTyped = exports.OrderRoutingUpdateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
function OrderRoutingUpdateDataFromJSON(json) {
    return OrderRoutingUpdateDataFromJSONTyped(json, false);
}
exports.OrderRoutingUpdateDataFromJSON = OrderRoutingUpdateDataFromJSON;
function OrderRoutingUpdateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'rules': !runtime_1.exists(json, 'rules') ? undefined : json['rules'],
    };
}
exports.OrderRoutingUpdateDataFromJSONTyped = OrderRoutingUpdateDataFromJSONTyped;
function OrderRoutingUpdateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'rules': value.rules,
    };
}
exports.OrderRoutingUpdateDataToJSON = OrderRoutingUpdateDataToJSON;
