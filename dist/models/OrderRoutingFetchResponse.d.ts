/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRouting } from './';
/**
 *
 * @export
 * @interface OrderRoutingFetchResponse
 */
export interface OrderRoutingFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrderRoutingFetchResponse
     */
    success: OrderRoutingFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingFetchResponse
     */
    code: OrderRoutingFetchResponseCodeEnum;
    /**
     *
     * @type {OrderRouting}
     * @memberof OrderRoutingFetchResponse
     */
    routing?: OrderRouting;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrderRoutingFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrderRoutingFetchResponseFromJSON(json: any): OrderRoutingFetchResponse;
export declare function OrderRoutingFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingFetchResponse;
export declare function OrderRoutingFetchResponseToJSON(value?: OrderRoutingFetchResponse | null): any;
