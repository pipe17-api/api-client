/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Type of order that initiated this shipment
 * @export
 * @enum {string}
 */
export declare enum ShipmentOrderType {
    Sales = "sales",
    Transfer = "transfer",
    Purchase = "purchase"
}
export declare function ShipmentOrderTypeFromJSON(json: any): ShipmentOrderType;
export declare function ShipmentOrderTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShipmentOrderType;
export declare function ShipmentOrderTypeToJSON(value?: ShipmentOrderType | null): any;
