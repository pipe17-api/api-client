/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { InventoryDeleteFilter } from './';
/**
 *
 * @export
 * @interface InventoryDeleteError
 */
export interface InventoryDeleteError {
    /**
     * Always false
     * @type {boolean}
     * @memberof InventoryDeleteError
     */
    success: InventoryDeleteErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof InventoryDeleteError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof InventoryDeleteError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof InventoryDeleteError
     */
    errors?: Array<string>;
    /**
     *
     * @type {InventoryDeleteFilter}
     * @memberof InventoryDeleteError
     */
    filters?: InventoryDeleteFilter;
}
/**
* @export
* @enum {string}
*/
export declare enum InventoryDeleteErrorSuccessEnum {
    False = "false"
}
export declare function InventoryDeleteErrorFromJSON(json: any): InventoryDeleteError;
export declare function InventoryDeleteErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryDeleteError;
export declare function InventoryDeleteErrorToJSON(value?: InventoryDeleteError | null): any;
