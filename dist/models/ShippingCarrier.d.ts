/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @enum {string}
 */
export declare enum ShippingCarrier {
    Ups = "ups",
    Usps = "usps",
    Fedex = "fedex",
    Deliverr = "deliverr",
    Amazon = "amazon",
    Other = "other"
}
export declare function ShippingCarrierFromJSON(json: any): ShippingCarrier;
export declare function ShippingCarrierFromJSONTyped(json: any, ignoreDiscriminator: boolean): ShippingCarrier;
export declare function ShippingCarrierToJSON(value?: ShippingCarrier | null): any;
