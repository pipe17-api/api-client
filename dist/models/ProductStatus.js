"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductStatusToJSON = exports.ProductStatusFromJSONTyped = exports.ProductStatusFromJSON = exports.ProductStatus = void 0;
/**
 * Product Status
 * @export
 * @enum {string}
 */
var ProductStatus;
(function (ProductStatus) {
    ProductStatus["Active"] = "active";
    ProductStatus["Inactive"] = "inactive";
})(ProductStatus = exports.ProductStatus || (exports.ProductStatus = {}));
function ProductStatusFromJSON(json) {
    return ProductStatusFromJSONTyped(json, false);
}
exports.ProductStatusFromJSON = ProductStatusFromJSON;
function ProductStatusFromJSONTyped(json, ignoreDiscriminator) {
    return json;
}
exports.ProductStatusFromJSONTyped = ProductStatusFromJSONTyped;
function ProductStatusToJSON(value) {
    return value;
}
exports.ProductStatusToJSON = ProductStatusToJSON;
