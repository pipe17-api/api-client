"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConnectorSetupResponseToJSON = exports.ConnectorSetupResponseFromJSONTyped = exports.ConnectorSetupResponseFromJSON = exports.ConnectorSetupResponseCodeEnum = exports.ConnectorSetupResponseSuccessEnum = void 0;
/**
* @export
* @enum {string}
*/
var ConnectorSetupResponseSuccessEnum;
(function (ConnectorSetupResponseSuccessEnum) {
    ConnectorSetupResponseSuccessEnum["True"] = "true";
})(ConnectorSetupResponseSuccessEnum = exports.ConnectorSetupResponseSuccessEnum || (exports.ConnectorSetupResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var ConnectorSetupResponseCodeEnum;
(function (ConnectorSetupResponseCodeEnum) {
    ConnectorSetupResponseCodeEnum[ConnectorSetupResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    ConnectorSetupResponseCodeEnum[ConnectorSetupResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    ConnectorSetupResponseCodeEnum[ConnectorSetupResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(ConnectorSetupResponseCodeEnum = exports.ConnectorSetupResponseCodeEnum || (exports.ConnectorSetupResponseCodeEnum = {}));
function ConnectorSetupResponseFromJSON(json) {
    return ConnectorSetupResponseFromJSONTyped(json, false);
}
exports.ConnectorSetupResponseFromJSON = ConnectorSetupResponseFromJSON;
function ConnectorSetupResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
    };
}
exports.ConnectorSetupResponseFromJSONTyped = ConnectorSetupResponseFromJSONTyped;
function ConnectorSetupResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
    };
}
exports.ConnectorSetupResponseToJSON = ConnectorSetupResponseToJSON;
