/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order, OrdersDeleteFilter, Pagination } from './';
/**
 *
 * @export
 * @interface OrdersDeleteResponse
 */
export interface OrdersDeleteResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrdersDeleteResponse
     */
    success: OrdersDeleteResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrdersDeleteResponse
     */
    code: OrdersDeleteResponseCodeEnum;
    /**
     *
     * @type {OrdersDeleteFilter}
     * @memberof OrdersDeleteResponse
     */
    filters?: OrdersDeleteFilter;
    /**
     *
     * @type {Array<Order>}
     * @memberof OrdersDeleteResponse
     */
    orders?: Array<Order>;
    /**
     * Number of deleted orders
     * @type {number}
     * @memberof OrdersDeleteResponse
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof OrdersDeleteResponse
     */
    pagination?: Pagination;
}
/**
* @export
* @enum {string}
*/
export declare enum OrdersDeleteResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrdersDeleteResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrdersDeleteResponseFromJSON(json: any): OrdersDeleteResponse;
export declare function OrdersDeleteResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrdersDeleteResponse;
export declare function OrdersDeleteResponseToJSON(value?: OrdersDeleteResponse | null): any;
