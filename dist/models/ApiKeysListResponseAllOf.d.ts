/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ApiKey, ApiKeysListFilter, Pagination } from './';
/**
 *
 * @export
 * @interface ApiKeysListResponseAllOf
 */
export interface ApiKeysListResponseAllOf {
    /**
     *
     * @type {ApiKeysListFilter}
     * @memberof ApiKeysListResponseAllOf
     */
    filters?: ApiKeysListFilter;
    /**
     *
     * @type {Array<ApiKey>}
     * @memberof ApiKeysListResponseAllOf
     */
    apikeys?: Array<ApiKey>;
    /**
     *
     * @type {Pagination}
     * @memberof ApiKeysListResponseAllOf
     */
    pagination?: Pagination;
}
export declare function ApiKeysListResponseAllOfFromJSON(json: any): ApiKeysListResponseAllOf;
export declare function ApiKeysListResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ApiKeysListResponseAllOf;
export declare function ApiKeysListResponseAllOfToJSON(value?: ApiKeysListResponseAllOf | null): any;
