/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { OrderRouting } from './';
/**
 *
 * @export
 * @interface OrderRoutingCreateResponse
 */
export interface OrderRoutingCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrderRoutingCreateResponse
     */
    success: OrderRoutingCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderRoutingCreateResponse
     */
    code: OrderRoutingCreateResponseCodeEnum;
    /**
     *
     * @type {OrderRouting}
     * @memberof OrderRoutingCreateResponse
     */
    routing?: OrderRouting;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderRoutingCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrderRoutingCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrderRoutingCreateResponseFromJSON(json: any): OrderRoutingCreateResponse;
export declare function OrderRoutingCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderRoutingCreateResponse;
export declare function OrderRoutingCreateResponseToJSON(value?: OrderRoutingCreateResponse | null): any;
