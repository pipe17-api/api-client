"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.JobAllOfToJSON = exports.JobAllOfFromJSONTyped = exports.JobAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function JobAllOfFromJSON(json) {
    return JobAllOfFromJSONTyped(json, false);
}
exports.JobAllOfFromJSON = JobAllOfFromJSON;
function JobAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'jobId': !runtime_1.exists(json, 'jobId') ? undefined : json['jobId'],
        'progress': !runtime_1.exists(json, 'progress') ? undefined : json['progress'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.JobStatusFromJSON(json['status']),
    };
}
exports.JobAllOfFromJSONTyped = JobAllOfFromJSONTyped;
function JobAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'jobId': value.jobId,
        'progress': value.progress,
        'status': _1.JobStatusToJSON(value.status),
    };
}
exports.JobAllOfToJSON = JobAllOfToJSON;
