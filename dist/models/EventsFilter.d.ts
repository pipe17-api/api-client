/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { EventRequestSource, EventRequestStatus } from './';
/**
 *
 * @export
 * @interface EventsFilter
 */
export interface EventsFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof EventsFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof EventsFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof EventsFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof EventsFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof EventsFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof EventsFilter
     */
    count?: number;
    /**
     * Fetch by list of eventId
     * @type {Array<string>}
     * @memberof EventsFilter
     */
    eventId?: Array<string>;
    /**
     * Fetch by list of event sources
     * @type {Array<EventRequestSource>}
     * @memberof EventsFilter
     */
    source?: Array<EventRequestSource>;
    /**
     * Fetch by list of event statuses
     * @type {Array<EventRequestStatus>}
     * @memberof EventsFilter
     */
    status?: Array<EventRequestStatus>;
    /**
     * Fetch by list of requests
     * @type {Array<string>}
     * @memberof EventsFilter
     */
    requestId?: Array<string>;
}
export declare function EventsFilterFromJSON(json: any): EventsFilter;
export declare function EventsFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): EventsFilter;
export declare function EventsFilterToJSON(value?: EventsFilter | null): any;
