/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { RoleUpdateData } from './';
/**
 *
 * @export
 * @interface RoleUpdateErrorAllOf
 */
export interface RoleUpdateErrorAllOf {
    /**
     *
     * @type {RoleUpdateData}
     * @memberof RoleUpdateErrorAllOf
     */
    role?: RoleUpdateData;
}
export declare function RoleUpdateErrorAllOfFromJSON(json: any): RoleUpdateErrorAllOf;
export declare function RoleUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoleUpdateErrorAllOf;
export declare function RoleUpdateErrorAllOfToJSON(value?: RoleUpdateErrorAllOf | null): any;
