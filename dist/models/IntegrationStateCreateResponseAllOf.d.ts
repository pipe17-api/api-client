/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { IntegrationState } from './';
/**
 *
 * @export
 * @interface IntegrationStateCreateResponseAllOf
 */
export interface IntegrationStateCreateResponseAllOf {
    /**
     *
     * @type {IntegrationState}
     * @memberof IntegrationStateCreateResponseAllOf
     */
    integrationState?: IntegrationState;
}
export declare function IntegrationStateCreateResponseAllOfFromJSON(json: any): IntegrationStateCreateResponseAllOf;
export declare function IntegrationStateCreateResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationStateCreateResponseAllOf;
export declare function IntegrationStateCreateResponseAllOfToJSON(value?: IntegrationStateCreateResponseAllOf | null): any;
