"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.FulfillmentFetchResponseAllOfToJSON = exports.FulfillmentFetchResponseAllOfFromJSONTyped = exports.FulfillmentFetchResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function FulfillmentFetchResponseAllOfFromJSON(json) {
    return FulfillmentFetchResponseAllOfFromJSONTyped(json, false);
}
exports.FulfillmentFetchResponseAllOfFromJSON = FulfillmentFetchResponseAllOfFromJSON;
function FulfillmentFetchResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fulfillment': !runtime_1.exists(json, 'fulfillment') ? undefined : _1.FulfillmentFromJSON(json['fulfillment']),
    };
}
exports.FulfillmentFetchResponseAllOfFromJSONTyped = FulfillmentFetchResponseAllOfFromJSONTyped;
function FulfillmentFetchResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fulfillment': _1.FulfillmentToJSON(value.fulfillment),
    };
}
exports.FulfillmentFetchResponseAllOfToJSON = FulfillmentFetchResponseAllOfToJSON;
