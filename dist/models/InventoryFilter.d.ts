/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface InventoryFilter
 */
export interface InventoryFilter {
    /**
     * Items created after this date-time
     * @type {Date}
     * @memberof InventoryFilter
     */
    since?: Date;
    /**
     * Items created before this date-time
     * @type {Date}
     * @memberof InventoryFilter
     */
    until?: Date;
    /**
     * Items updated after this date-time
     * @type {Date}
     * @memberof InventoryFilter
     */
    updatedSince?: Date;
    /**
     * Items updated before this date-time
     * @type {Date}
     * @memberof InventoryFilter
     */
    updatedUntil?: Date;
    /**
     * Skip this many items
     * @type {number}
     * @memberof InventoryFilter
     */
    skip?: number;
    /**
     * Return at most this many items
     * @type {number}
     * @memberof InventoryFilter
     */
    count?: number;
    /**
     * Inventory items by list of inventoryId
     * @type {Array<string>}
     * @memberof InventoryFilter
     */
    inventoryId?: Array<string>;
    /**
     * Return inventory items related to certain event record by record id
     * @type {Array<string>}
     * @memberof InventoryFilter
     */
    entityId?: Array<string>;
    /**
     * Fetch inventory by its SKU
     * @type {Array<string>}
     * @memberof InventoryFilter
     */
    sku?: Array<string>;
    /**
     * Fetch inventory by its location Id
     * @type {Array<string>}
     * @memberof InventoryFilter
     */
    locationId?: Array<string>;
    /**
     * Fetch soft deleted inventory items
     * @type {boolean}
     * @memberof InventoryFilter
     */
    deleted?: boolean;
}
export declare function InventoryFilterFromJSON(json: any): InventoryFilter;
export declare function InventoryFilterFromJSONTyped(json: any, ignoreDiscriminator: boolean): InventoryFilter;
export declare function InventoryFilterToJSON(value?: InventoryFilter | null): any;
