"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.PurchasesDeleteResponseAllOfToJSON = exports.PurchasesDeleteResponseAllOfFromJSONTyped = exports.PurchasesDeleteResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function PurchasesDeleteResponseAllOfFromJSON(json) {
    return PurchasesDeleteResponseAllOfFromJSONTyped(json, false);
}
exports.PurchasesDeleteResponseAllOfFromJSON = PurchasesDeleteResponseAllOfFromJSON;
function PurchasesDeleteResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.PurchasesDeleteFilterFromJSON(json['filters']),
        'purchases': !runtime_1.exists(json, 'purchases') ? undefined : (json['purchases'].map(_1.PurchaseFromJSON)),
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.PurchasesDeleteResponseAllOfFromJSONTyped = PurchasesDeleteResponseAllOfFromJSONTyped;
function PurchasesDeleteResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.PurchasesDeleteFilterToJSON(value.filters),
        'purchases': value.purchases === undefined ? undefined : (value.purchases.map(_1.PurchaseToJSON)),
        'deleted': value.deleted,
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.PurchasesDeleteResponseAllOfToJSON = PurchasesDeleteResponseAllOfToJSON;
