/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 * Refund type
 * @export
 * @enum {string}
 */
export declare enum RefundType {
    Refund = "refund",
    Voucher = "voucher"
}
export declare function RefundTypeFromJSON(json: any): RefundType;
export declare function RefundTypeFromJSONTyped(json: any, ignoreDiscriminator: boolean): RefundType;
export declare function RefundTypeToJSON(value?: RefundType | null): any;
