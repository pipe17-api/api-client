"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateListErrorToJSON = exports.IntegrationStateListErrorFromJSONTyped = exports.IntegrationStateListErrorFromJSON = exports.IntegrationStateListErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationStateListErrorSuccessEnum;
(function (IntegrationStateListErrorSuccessEnum) {
    IntegrationStateListErrorSuccessEnum["False"] = "false";
})(IntegrationStateListErrorSuccessEnum = exports.IntegrationStateListErrorSuccessEnum || (exports.IntegrationStateListErrorSuccessEnum = {}));
function IntegrationStateListErrorFromJSON(json) {
    return IntegrationStateListErrorFromJSONTyped(json, false);
}
exports.IntegrationStateListErrorFromJSON = IntegrationStateListErrorFromJSON;
function IntegrationStateListErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationStateListFilterFromJSON(json['filters']),
    };
}
exports.IntegrationStateListErrorFromJSONTyped = IntegrationStateListErrorFromJSONTyped;
function IntegrationStateListErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
        'filters': _1.IntegrationStateListFilterToJSON(value.filters),
    };
}
exports.IntegrationStateListErrorToJSON = IntegrationStateListErrorToJSON;
