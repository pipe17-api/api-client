/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { TransferUpdateData } from './';
/**
 *
 * @export
 * @interface TransferUpdateErrorAllOf
 */
export interface TransferUpdateErrorAllOf {
    /**
     *
     * @type {TransferUpdateData}
     * @memberof TransferUpdateErrorAllOf
     */
    transfer?: TransferUpdateData;
}
export declare function TransferUpdateErrorAllOfFromJSON(json: any): TransferUpdateErrorAllOf;
export declare function TransferUpdateErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransferUpdateErrorAllOf;
export declare function TransferUpdateErrorAllOfToJSON(value?: TransferUpdateErrorAllOf | null): any;
