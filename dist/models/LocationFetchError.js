"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationFetchErrorToJSON = exports.LocationFetchErrorFromJSONTyped = exports.LocationFetchErrorFromJSON = exports.LocationFetchErrorSuccessEnum = void 0;
const runtime_1 = require("../runtime");
/**
* @export
* @enum {string}
*/
var LocationFetchErrorSuccessEnum;
(function (LocationFetchErrorSuccessEnum) {
    LocationFetchErrorSuccessEnum["False"] = "false";
})(LocationFetchErrorSuccessEnum = exports.LocationFetchErrorSuccessEnum || (exports.LocationFetchErrorSuccessEnum = {}));
function LocationFetchErrorFromJSON(json) {
    return LocationFetchErrorFromJSONTyped(json, false);
}
exports.LocationFetchErrorFromJSON = LocationFetchErrorFromJSON;
function LocationFetchErrorFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'message': !runtime_1.exists(json, 'message') ? undefined : json['message'],
        'errors': !runtime_1.exists(json, 'errors') ? undefined : json['errors'],
    };
}
exports.LocationFetchErrorFromJSONTyped = LocationFetchErrorFromJSONTyped;
function LocationFetchErrorToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'message': value.message,
        'errors': value.errors,
    };
}
exports.LocationFetchErrorToJSON = LocationFetchErrorToJSON;
