"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefundsDeleteErrorAllOfToJSON = exports.RefundsDeleteErrorAllOfFromJSONTyped = exports.RefundsDeleteErrorAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function RefundsDeleteErrorAllOfFromJSON(json) {
    return RefundsDeleteErrorAllOfFromJSONTyped(json, false);
}
exports.RefundsDeleteErrorAllOfFromJSON = RefundsDeleteErrorAllOfFromJSON;
function RefundsDeleteErrorAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.RefundsDeleteFilterFromJSON(json['filters']),
    };
}
exports.RefundsDeleteErrorAllOfFromJSONTyped = RefundsDeleteErrorAllOfFromJSONTyped;
function RefundsDeleteErrorAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'filters': _1.RefundsDeleteFilterToJSON(value.filters),
    };
}
exports.RefundsDeleteErrorAllOfToJSON = RefundsDeleteErrorAllOfToJSON;
