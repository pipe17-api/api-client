"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionCreateDataToJSON = exports.ExceptionCreateDataFromJSONTyped = exports.ExceptionCreateDataFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionCreateDataFromJSON(json) {
    return ExceptionCreateDataFromJSONTyped(json, false);
}
exports.ExceptionCreateDataFromJSON = ExceptionCreateDataFromJSON;
function ExceptionCreateDataFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionType': _1.ExceptionTypeFromJSON(json['exceptionType']),
        'exceptionDetails': !runtime_1.exists(json, 'exceptionDetails') ? undefined : json['exceptionDetails'],
        'entityId': json['entityId'],
        'entityType': _1.EntityNameFromJSON(json['entityType']),
    };
}
exports.ExceptionCreateDataFromJSONTyped = ExceptionCreateDataFromJSONTyped;
function ExceptionCreateDataToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionType': _1.ExceptionTypeToJSON(value.exceptionType),
        'exceptionDetails': value.exceptionDetails,
        'entityId': value.entityId,
        'entityType': _1.EntityNameToJSON(value.entityType),
    };
}
exports.ExceptionCreateDataToJSON = ExceptionCreateDataToJSON;
