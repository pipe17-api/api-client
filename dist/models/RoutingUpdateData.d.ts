/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface RoutingUpdateData
 */
export interface RoutingUpdateData {
    /**
     * Description
     * @type {string}
     * @memberof RoutingUpdateData
     */
    description?: string;
    /**
     * Is Routing Filter Enabled
     * @type {boolean}
     * @memberof RoutingUpdateData
     */
    enabled?: boolean;
    /**
     * Routing Filter
     * @type {string}
     * @memberof RoutingUpdateData
     */
    filter?: string | null;
}
export declare function RoutingUpdateDataFromJSON(json: any): RoutingUpdateData;
export declare function RoutingUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): RoutingUpdateData;
export declare function RoutingUpdateDataToJSON(value?: RoutingUpdateData | null): any;
