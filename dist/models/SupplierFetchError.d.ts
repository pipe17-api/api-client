/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface SupplierFetchError
 */
export interface SupplierFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof SupplierFetchError
     */
    success: SupplierFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof SupplierFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof SupplierFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof SupplierFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum SupplierFetchErrorSuccessEnum {
    False = "false"
}
export declare function SupplierFetchErrorFromJSON(json: any): SupplierFetchError;
export declare function SupplierFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): SupplierFetchError;
export declare function SupplierFetchErrorToJSON(value?: SupplierFetchError | null): any;
