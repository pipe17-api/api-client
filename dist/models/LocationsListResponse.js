"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationsListResponseToJSON = exports.LocationsListResponseFromJSONTyped = exports.LocationsListResponseFromJSON = exports.LocationsListResponseCodeEnum = exports.LocationsListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var LocationsListResponseSuccessEnum;
(function (LocationsListResponseSuccessEnum) {
    LocationsListResponseSuccessEnum["True"] = "true";
})(LocationsListResponseSuccessEnum = exports.LocationsListResponseSuccessEnum || (exports.LocationsListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var LocationsListResponseCodeEnum;
(function (LocationsListResponseCodeEnum) {
    LocationsListResponseCodeEnum[LocationsListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    LocationsListResponseCodeEnum[LocationsListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    LocationsListResponseCodeEnum[LocationsListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(LocationsListResponseCodeEnum = exports.LocationsListResponseCodeEnum || (exports.LocationsListResponseCodeEnum = {}));
function LocationsListResponseFromJSON(json) {
    return LocationsListResponseFromJSONTyped(json, false);
}
exports.LocationsListResponseFromJSON = LocationsListResponseFromJSON;
function LocationsListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.LocationsListFilterFromJSON(json['filters']),
        'locations': !runtime_1.exists(json, 'locations') ? undefined : (json['locations'].map(_1.LocationFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.LocationsListResponseFromJSONTyped = LocationsListResponseFromJSONTyped;
function LocationsListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.LocationsListFilterToJSON(value.filters),
        'locations': value.locations === undefined ? undefined : (value.locations.map(_1.LocationToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.LocationsListResponseToJSON = LocationsListResponseToJSON;
