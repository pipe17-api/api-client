/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountUpdateData } from './';
/**
 *
 * @export
 * @interface AccountUpdateError
 */
export interface AccountUpdateError {
    /**
     * Always false
     * @type {boolean}
     * @memberof AccountUpdateError
     */
    success: AccountUpdateErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountUpdateError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof AccountUpdateError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof AccountUpdateError
     */
    errors?: Array<string>;
    /**
     *
     * @type {AccountUpdateData}
     * @memberof AccountUpdateError
     */
    account?: AccountUpdateData;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountUpdateErrorSuccessEnum {
    False = "false"
}
export declare function AccountUpdateErrorFromJSON(json: any): AccountUpdateError;
export declare function AccountUpdateErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountUpdateError;
export declare function AccountUpdateErrorToJSON(value?: AccountUpdateError | null): any;
