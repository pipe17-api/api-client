"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationsFilterAllOfToJSON = exports.IntegrationsFilterAllOfFromJSONTyped = exports.IntegrationsFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationsFilterAllOfFromJSON(json) {
    return IntegrationsFilterAllOfFromJSONTyped(json, false);
}
exports.IntegrationsFilterAllOfFromJSON = IntegrationsFilterAllOfFromJSON;
function IntegrationsFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'integrationId': !runtime_1.exists(json, 'integrationId') ? undefined : json['integrationId'],
        'connectorName': !runtime_1.exists(json, 'connectorName') ? undefined : json['connectorName'],
        'connectorId': !runtime_1.exists(json, 'connectorId') ? undefined : json['connectorId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.IntegrationStatusFromJSON(json['status']),
    };
}
exports.IntegrationsFilterAllOfFromJSONTyped = IntegrationsFilterAllOfFromJSONTyped;
function IntegrationsFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'integrationId': value.integrationId,
        'connectorName': value.connectorName,
        'connectorId': value.connectorId,
        'status': _1.IntegrationStatusToJSON(value.status),
    };
}
exports.IntegrationsFilterAllOfToJSON = IntegrationsFilterAllOfToJSON;
