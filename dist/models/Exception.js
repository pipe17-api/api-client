"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExceptionToJSON = exports.ExceptionFromJSONTyped = exports.ExceptionFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function ExceptionFromJSON(json) {
    return ExceptionFromJSONTyped(json, false);
}
exports.ExceptionFromJSON = ExceptionFromJSON;
function ExceptionFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'exceptionId': !runtime_1.exists(json, 'exceptionId') ? undefined : json['exceptionId'],
        'status': !runtime_1.exists(json, 'status') ? undefined : _1.ExceptionStatusFromJSON(json['status']),
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'exceptionType': _1.ExceptionTypeFromJSON(json['exceptionType']),
        'exceptionDetails': !runtime_1.exists(json, 'exceptionDetails') ? undefined : json['exceptionDetails'],
        'entityId': json['entityId'],
        'entityType': _1.EntityNameFromJSON(json['entityType']),
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.ExceptionFromJSONTyped = ExceptionFromJSONTyped;
function ExceptionToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'exceptionId': value.exceptionId,
        'status': _1.ExceptionStatusToJSON(value.status),
        'integration': value.integration,
        'exceptionType': _1.ExceptionTypeToJSON(value.exceptionType),
        'exceptionDetails': value.exceptionDetails,
        'entityId': value.entityId,
        'entityType': _1.EntityNameToJSON(value.entityType),
    };
}
exports.ExceptionToJSON = ExceptionToJSON;
