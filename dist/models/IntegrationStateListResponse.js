"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationStateListResponseToJSON = exports.IntegrationStateListResponseFromJSONTyped = exports.IntegrationStateListResponseFromJSON = exports.IntegrationStateListResponseCodeEnum = exports.IntegrationStateListResponseSuccessEnum = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
/**
* @export
* @enum {string}
*/
var IntegrationStateListResponseSuccessEnum;
(function (IntegrationStateListResponseSuccessEnum) {
    IntegrationStateListResponseSuccessEnum["True"] = "true";
})(IntegrationStateListResponseSuccessEnum = exports.IntegrationStateListResponseSuccessEnum || (exports.IntegrationStateListResponseSuccessEnum = {})); /**
* @export
* @enum {string}
*/
var IntegrationStateListResponseCodeEnum;
(function (IntegrationStateListResponseCodeEnum) {
    IntegrationStateListResponseCodeEnum[IntegrationStateListResponseCodeEnum["NUMBER_200"] = 200] = "NUMBER_200";
    IntegrationStateListResponseCodeEnum[IntegrationStateListResponseCodeEnum["NUMBER_201"] = 201] = "NUMBER_201";
    IntegrationStateListResponseCodeEnum[IntegrationStateListResponseCodeEnum["NUMBER_202"] = 202] = "NUMBER_202";
})(IntegrationStateListResponseCodeEnum = exports.IntegrationStateListResponseCodeEnum || (exports.IntegrationStateListResponseCodeEnum = {}));
function IntegrationStateListResponseFromJSON(json) {
    return IntegrationStateListResponseFromJSONTyped(json, false);
}
exports.IntegrationStateListResponseFromJSON = IntegrationStateListResponseFromJSON;
function IntegrationStateListResponseFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'success': json['success'],
        'code': json['code'],
        'filters': !runtime_1.exists(json, 'filters') ? undefined : _1.IntegrationStateListFilterFromJSON(json['filters']),
        'integrationStates': !runtime_1.exists(json, 'integrationStates') ? undefined : (json['integrationStates'].map(_1.IntegrationStateFromJSON)),
        'pagination': !runtime_1.exists(json, 'pagination') ? undefined : _1.PaginationFromJSON(json['pagination']),
    };
}
exports.IntegrationStateListResponseFromJSONTyped = IntegrationStateListResponseFromJSONTyped;
function IntegrationStateListResponseToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'success': value.success,
        'code': value.code,
        'filters': _1.IntegrationStateListFilterToJSON(value.filters),
        'integrationStates': value.integrationStates === undefined ? undefined : (value.integrationStates.map(_1.IntegrationStateToJSON)),
        'pagination': _1.PaginationToJSON(value.pagination),
    };
}
exports.IntegrationStateListResponseToJSON = IntegrationStateListResponseToJSON;
