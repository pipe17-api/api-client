/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { JobStatus } from './';
/**
 *
 * @export
 * @interface JobAllOf
 */
export interface JobAllOf {
    /**
     * Job ID
     * @type {string}
     * @memberof JobAllOf
     */
    jobId?: string;
    /**
     * Job progress (0-100)
     * @type {number}
     * @memberof JobAllOf
     */
    progress?: number;
    /**
     *
     * @type {JobStatus}
     * @memberof JobAllOf
     */
    status?: JobStatus;
}
export declare function JobAllOfFromJSON(json: any): JobAllOf;
export declare function JobAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): JobAllOf;
export declare function JobAllOfToJSON(value?: JobAllOf | null): any;
