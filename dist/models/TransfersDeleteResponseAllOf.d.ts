/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Pagination, Transfer, TransfersDeleteFilter } from './';
/**
 *
 * @export
 * @interface TransfersDeleteResponseAllOf
 */
export interface TransfersDeleteResponseAllOf {
    /**
     *
     * @type {TransfersDeleteFilter}
     * @memberof TransfersDeleteResponseAllOf
     */
    filters?: TransfersDeleteFilter;
    /**
     *
     * @type {Array<Transfer>}
     * @memberof TransfersDeleteResponseAllOf
     */
    transfers?: Array<Transfer>;
    /**
     * Number of deleted transfers
     * @type {number}
     * @memberof TransfersDeleteResponseAllOf
     */
    deleted?: number;
    /**
     *
     * @type {Pagination}
     * @memberof TransfersDeleteResponseAllOf
     */
    pagination?: Pagination;
}
export declare function TransfersDeleteResponseAllOfFromJSON(json: any): TransfersDeleteResponseAllOf;
export declare function TransfersDeleteResponseAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): TransfersDeleteResponseAllOf;
export declare function TransfersDeleteResponseAllOfToJSON(value?: TransfersDeleteResponseAllOf | null): any;
