"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransferAllOfToJSON = exports.TransferAllOfFromJSONTyped = exports.TransferAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function TransferAllOfFromJSON(json) {
    return TransferAllOfFromJSONTyped(json, false);
}
exports.TransferAllOfFromJSON = TransferAllOfFromJSON;
function TransferAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'transferId': !runtime_1.exists(json, 'transferId') ? undefined : json['transferId'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
    };
}
exports.TransferAllOfFromJSONTyped = TransferAllOfFromJSONTyped;
function TransferAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'transferId': value.transferId,
        'integration': value.integration,
    };
}
exports.TransferAllOfToJSON = TransferAllOfToJSON;
