/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface PurchaseUpdateResponse
 */
export interface PurchaseUpdateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof PurchaseUpdateResponse
     */
    success: PurchaseUpdateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof PurchaseUpdateResponse
     */
    code: PurchaseUpdateResponseCodeEnum;
}
/**
* @export
* @enum {string}
*/
export declare enum PurchaseUpdateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum PurchaseUpdateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function PurchaseUpdateResponseFromJSON(json: any): PurchaseUpdateResponse;
export declare function PurchaseUpdateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): PurchaseUpdateResponse;
export declare function PurchaseUpdateResponseToJSON(value?: PurchaseUpdateResponse | null): any;
