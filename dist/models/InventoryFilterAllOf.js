"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryFilterAllOfToJSON = exports.InventoryFilterAllOfFromJSONTyped = exports.InventoryFilterAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
function InventoryFilterAllOfFromJSON(json) {
    return InventoryFilterAllOfFromJSONTyped(json, false);
}
exports.InventoryFilterAllOfFromJSON = InventoryFilterAllOfFromJSON;
function InventoryFilterAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventoryId': !runtime_1.exists(json, 'inventoryId') ? undefined : json['inventoryId'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'deleted': !runtime_1.exists(json, 'deleted') ? undefined : json['deleted'],
    };
}
exports.InventoryFilterAllOfFromJSONTyped = InventoryFilterAllOfFromJSONTyped;
function InventoryFilterAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventoryId': value.inventoryId,
        'entityId': value.entityId,
        'sku': value.sku,
        'locationId': value.locationId,
        'deleted': value.deleted,
    };
}
exports.InventoryFilterAllOfToJSON = InventoryFilterAllOfToJSON;
