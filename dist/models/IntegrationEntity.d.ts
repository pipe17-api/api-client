/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Direction, EntityName, EntityRestrictionsValue } from './';
/**
 * Mapping or Routing Entity
 * @export
 * @interface IntegrationEntity
 */
export interface IntegrationEntity {
    /**
     *
     * @type {EntityName}
     * @memberof IntegrationEntity
     */
    name?: EntityName;
    /**
     *
     * @type {Direction}
     * @memberof IntegrationEntity
     */
    direction?: Direction;
    /**
     * Mapping Id
     * @type {string}
     * @memberof IntegrationEntity
     */
    mappingUuid?: string;
    /**
     * Routing Id
     * @type {string}
     * @memberof IntegrationEntity
     */
    routingUuid?: string;
    /**
     *
     * @type {Array<EntityRestrictionsValue>}
     * @memberof IntegrationEntity
     */
    restrictions?: Array<EntityRestrictionsValue>;
}
export declare function IntegrationEntityFromJSON(json: any): IntegrationEntity;
export declare function IntegrationEntityFromJSONTyped(json: any, ignoreDiscriminator: boolean): IntegrationEntity;
export declare function IntegrationEntityToJSON(value?: IntegrationEntity | null): any;
