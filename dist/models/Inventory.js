"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.InventoryToJSON = exports.InventoryFromJSONTyped = exports.InventoryFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function InventoryFromJSON(json) {
    return InventoryFromJSONTyped(json, false);
}
exports.InventoryFromJSON = InventoryFromJSON;
function InventoryFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'inventoryId': !runtime_1.exists(json, 'inventoryId') ? undefined : json['inventoryId'],
        'entityId': !runtime_1.exists(json, 'entityId') ? undefined : json['entityId'],
        'entityType': !runtime_1.exists(json, 'entityType') ? undefined : json['entityType'],
        'event': !runtime_1.exists(json, 'event') ? undefined : _1.EventTypeFromJSON(json['event']),
        'sku': !runtime_1.exists(json, 'sku') ? undefined : json['sku'],
        'locationId': !runtime_1.exists(json, 'locationId') ? undefined : json['locationId'],
        'onHand': !runtime_1.exists(json, 'onHand') ? undefined : json['onHand'],
        'committed': !runtime_1.exists(json, 'committed') ? undefined : json['committed'],
        'committedFuture': !runtime_1.exists(json, 'committedFuture') ? undefined : json['committedFuture'],
        'future': !runtime_1.exists(json, 'future') ? undefined : json['future'],
        'available': !runtime_1.exists(json, 'available') ? undefined : json['available'],
        'availableToPromise': !runtime_1.exists(json, 'availableToPromise') ? undefined : json['availableToPromise'],
        'quantity': !runtime_1.exists(json, 'quantity') ? undefined : json['quantity'],
        'incoming': !runtime_1.exists(json, 'incoming') ? undefined : json['incoming'],
        'commitShip': !runtime_1.exists(json, 'commitShip') ? undefined : json['commitShip'],
        'commitXfer': !runtime_1.exists(json, 'commitXfer') ? undefined : json['commitXfer'],
        'unavailable': !runtime_1.exists(json, 'unavailable') ? undefined : json['unavailable'],
        'integration': !runtime_1.exists(json, 'integration') ? undefined : json['integration'],
        'status': !runtime_1.exists(json, 'status') ? undefined : json['status'],
        'orderId': !runtime_1.exists(json, 'orderId') ? undefined : json['orderId'],
        'orderType': !runtime_1.exists(json, 'orderType') ? undefined : json['orderType'],
        'user': !runtime_1.exists(json, 'user') ? undefined : json['user'],
        'createdAt': !runtime_1.exists(json, 'createdAt') ? undefined : (new Date(json['createdAt'])),
        'updatedAt': !runtime_1.exists(json, 'updatedAt') ? undefined : (new Date(json['updatedAt'])),
        'orgKey': !runtime_1.exists(json, 'orgKey') ? undefined : json['orgKey'],
    };
}
exports.InventoryFromJSONTyped = InventoryFromJSONTyped;
function InventoryToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'inventoryId': value.inventoryId,
        'entityId': value.entityId,
        'entityType': value.entityType,
        'event': _1.EventTypeToJSON(value.event),
        'sku': value.sku,
        'locationId': value.locationId,
        'onHand': value.onHand,
        'committed': value.committed,
        'committedFuture': value.committedFuture,
        'future': value.future,
        'available': value.available,
        'availableToPromise': value.availableToPromise,
        'quantity': value.quantity,
        'incoming': value.incoming,
        'commitShip': value.commitShip,
        'commitXfer': value.commitXfer,
        'unavailable': value.unavailable,
        'integration': value.integration,
        'status': value.status,
        'orderId': value.orderId,
        'orderType': value.orderType,
        'user': value.user,
    };
}
exports.InventoryToJSON = InventoryToJSON;
