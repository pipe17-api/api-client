/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ArrivalCreateResult } from './';
/**
 *
 * @export
 * @interface ArrivalsCreateResponse
 */
export interface ArrivalsCreateResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof ArrivalsCreateResponse
     */
    success: ArrivalsCreateResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof ArrivalsCreateResponse
     */
    code: ArrivalsCreateResponseCodeEnum;
    /**
     *
     * @type {Array<ArrivalCreateResult>}
     * @memberof ArrivalsCreateResponse
     */
    arrivals?: Array<ArrivalCreateResult>;
}
/**
* @export
* @enum {string}
*/
export declare enum ArrivalsCreateResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum ArrivalsCreateResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function ArrivalsCreateResponseFromJSON(json: any): ArrivalsCreateResponse;
export declare function ArrivalsCreateResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): ArrivalsCreateResponse;
export declare function ArrivalsCreateResponseToJSON(value?: ArrivalsCreateResponse | null): any;
