"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.IntegrationConnectionToJSON = exports.IntegrationConnectionFromJSONTyped = exports.IntegrationConnectionFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function IntegrationConnectionFromJSON(json) {
    return IntegrationConnectionFromJSONTyped(json, false);
}
exports.IntegrationConnectionFromJSON = IntegrationConnectionFromJSON;
function IntegrationConnectionFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'fields': !runtime_1.exists(json, 'fields') ? undefined : (json['fields'] === null ? null : json['fields'].map(_1.IntegrationConnectionFieldFromJSON)),
    };
}
exports.IntegrationConnectionFromJSONTyped = IntegrationConnectionFromJSONTyped;
function IntegrationConnectionToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'fields': value.fields === undefined ? undefined : (value.fields === null ? null : value.fields.map(_1.IntegrationConnectionFieldToJSON)),
    };
}
exports.IntegrationConnectionToJSON = IntegrationConnectionToJSON;
