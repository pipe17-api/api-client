/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ProductsListFilter } from './';
/**
 *
 * @export
 * @interface ProducstListErrorAllOf
 */
export interface ProducstListErrorAllOf {
    /**
     *
     * @type {ProductsListFilter}
     * @memberof ProducstListErrorAllOf
     */
    filters?: ProductsListFilter;
}
export declare function ProducstListErrorAllOfFromJSON(json: any): ProducstListErrorAllOf;
export declare function ProducstListErrorAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProducstListErrorAllOf;
export declare function ProducstListErrorAllOfToJSON(value?: ProducstListErrorAllOf | null): any;
