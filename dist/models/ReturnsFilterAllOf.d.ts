/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { ReturnStatus } from './';
/**
 * Returns Filter
 * @export
 * @interface ReturnsFilterAllOf
 */
export interface ReturnsFilterAllOf {
    /**
     * Returns by list of returnId
     * @type {Array<string>}
     * @memberof ReturnsFilterAllOf
     */
    returnId?: Array<string>;
    /**
     * Returns by list of external return ids
     * @type {Array<string>}
     * @memberof ReturnsFilterAllOf
     */
    extReturnId?: Array<string>;
    /**
     * Returns by list of statuses
     * @type {Array<ReturnStatus>}
     * @memberof ReturnsFilterAllOf
     */
    status?: Array<ReturnStatus>;
    /**
     * Soft deleted returns
     * @type {boolean}
     * @memberof ReturnsFilterAllOf
     */
    deleted?: boolean;
}
export declare function ReturnsFilterAllOfFromJSON(json: any): ReturnsFilterAllOf;
export declare function ReturnsFilterAllOfFromJSONTyped(json: any, ignoreDiscriminator: boolean): ReturnsFilterAllOf;
export declare function ReturnsFilterAllOfToJSON(value?: ReturnsFilterAllOf | null): any;
