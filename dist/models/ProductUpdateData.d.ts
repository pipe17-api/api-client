/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { NameValue, Price, ProductBundleItem, ProductPublishedItem, ProductStatus, ProductType } from './';
/**
 *
 * @export
 * @interface ProductUpdateData
 */
export interface ProductUpdateData {
    /**
     * Product SKU
     * @type {string}
     * @memberof ProductUpdateData
     */
    sku?: string;
    /**
     * Display Name
     * @type {string}
     * @memberof ProductUpdateData
     */
    name?: string;
    /**
     *
     * @type {ProductStatus}
     * @memberof ProductUpdateData
     */
    status?: ProductStatus;
    /**
     * Description
     * @type {string}
     * @memberof ProductUpdateData
     */
    description?: string;
    /**
     * Product types
     * @type {Array<ProductType>}
     * @memberof ProductUpdateData
     */
    types?: Array<ProductType>;
    /**
     * External Product API Id
     * @type {string}
     * @memberof ProductUpdateData
     */
    extProductApiId?: string;
    /**
     * UPC
     * @type {string}
     * @memberof ProductUpdateData
     */
    upc?: string | null;
    /**
     * Manufacturer part number
     * @type {string}
     * @memberof ProductUpdateData
     */
    partId?: string | null;
    /**
     * Start off with basePrice
     * @type {Array<Price>}
     * @memberof ProductUpdateData
     */
    prices?: Array<Price>;
    /**
     * Image URLs
     * @type {Array<string>}
     * @memberof ProductUpdateData
     */
    imageURLs?: Array<string>;
    /**
     * Cost
     * @type {number}
     * @memberof ProductUpdateData
     */
    cost?: number | null;
    /**
     * Currency
     * @type {string}
     * @memberof ProductUpdateData
     */
    costCurrency?: string | null;
    /**
     * Taxable
     * @type {boolean}
     * @memberof ProductUpdateData
     */
    taxable?: boolean | null;
    /**
     * Country of Origin
     * @type {string}
     * @memberof ProductUpdateData
     */
    countryOfOrigin?: string | null;
    /**
     * Harmonized System/Tariff Code
     * @type {string}
     * @memberof ProductUpdateData
     */
    harmonizedCode?: string | null;
    /**
     * Weight
     * @type {number}
     * @memberof ProductUpdateData
     */
    weight?: number | null;
    /**
     * Weight UOM
     * @type {string}
     * @memberof ProductUpdateData
     */
    weightUnit?: ProductUpdateDataWeightUnitEnum;
    /**
     * Height
     * @type {number}
     * @memberof ProductUpdateData
     */
    height?: number | null;
    /**
     * Length
     * @type {number}
     * @memberof ProductUpdateData
     */
    length?: number | null;
    /**
     * Width
     * @type {number}
     * @memberof ProductUpdateData
     */
    width?: number | null;
    /**
     * Dimensions UOM
     * @type {string}
     * @memberof ProductUpdateData
     */
    dimensionsUnit?: ProductUpdateDataDimensionsUnitEnum;
    /**
     * vendor
     * @type {string}
     * @memberof ProductUpdateData
     */
    vendor?: string | null;
    /**
     * Tags
     * @type {Array<string>}
     * @memberof ProductUpdateData
     */
    tags?: Array<string>;
    /**
     * Variant parent external product Id
     * @type {string}
     * @memberof ProductUpdateData
     */
    parentExtProductId?: string;
    /**
     * this let us know the variant description
     * @type {Array<NameValue>}
     * @memberof ProductUpdateData
     */
    variantOptions?: Array<NameValue>;
    /**
     *
     * @type {Date}
     * @memberof ProductUpdateData
     */
    extProductCreatedAt?: Date | null;
    /**
     *
     * @type {Date}
     * @memberof ProductUpdateData
     */
    extProductUpdatedAt?: Date | null;
    /**
     * Bundle Item need to reference the SKU of the actual product
     * @type {Array<ProductBundleItem>}
     * @memberof ProductUpdateData
     */
    bundleItems?: Array<ProductBundleItem> | null;
    /**
     * Custom Fields
     * @type {Array<NameValue>}
     * @memberof ProductUpdateData
     */
    customFields?: Array<NameValue>;
    /**
     * Published Channels
     * @type {Array<ProductPublishedItem>}
     * @memberof ProductUpdateData
     */
    published?: Array<ProductPublishedItem>;
    /**
     * Do not track inventory flag
     * @type {boolean}
     * @memberof ProductUpdateData
     */
    inventoryNotTracked?: boolean | null;
    /**
     * Mark item as hidden in marketplace Storefront
     * @type {boolean}
     * @memberof ProductUpdateData
     */
    hideInStore?: boolean | null;
}
/**
* @export
* @enum {string}
*/
export declare enum ProductUpdateDataWeightUnitEnum {
    Lb = "lb",
    Oz = "oz",
    Kg = "kg",
    G = "g"
} /**
* @export
* @enum {string}
*/
export declare enum ProductUpdateDataDimensionsUnitEnum {
    Cm = "cm",
    In = "in",
    Ft = "ft"
}
export declare function ProductUpdateDataFromJSON(json: any): ProductUpdateData;
export declare function ProductUpdateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): ProductUpdateData;
export declare function ProductUpdateDataToJSON(value?: ProductUpdateData | null): any;
