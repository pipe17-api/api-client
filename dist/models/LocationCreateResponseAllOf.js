"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationCreateResponseAllOfToJSON = exports.LocationCreateResponseAllOfFromJSONTyped = exports.LocationCreateResponseAllOfFromJSON = void 0;
const runtime_1 = require("../runtime");
const _1 = require("./");
function LocationCreateResponseAllOfFromJSON(json) {
    return LocationCreateResponseAllOfFromJSONTyped(json, false);
}
exports.LocationCreateResponseAllOfFromJSON = LocationCreateResponseAllOfFromJSON;
function LocationCreateResponseAllOfFromJSONTyped(json, ignoreDiscriminator) {
    if ((json === undefined) || (json === null)) {
        return json;
    }
    return {
        'location': !runtime_1.exists(json, 'location') ? undefined : _1.LocationFromJSON(json['location']),
    };
}
exports.LocationCreateResponseAllOfFromJSONTyped = LocationCreateResponseAllOfFromJSONTyped;
function LocationCreateResponseAllOfToJSON(value) {
    if (value === undefined) {
        return undefined;
    }
    if (value === null) {
        return null;
    }
    return {
        'location': _1.LocationToJSON(value.location),
    };
}
exports.LocationCreateResponseAllOfToJSON = LocationCreateResponseAllOfToJSON;
