/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface AccountFetchError
 */
export interface AccountFetchError {
    /**
     * Always false
     * @type {boolean}
     * @memberof AccountFetchError
     */
    success: AccountFetchErrorSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof AccountFetchError
     */
    code: number;
    /**
     * Error message
     * @type {string}
     * @memberof AccountFetchError
     */
    message?: string;
    /**
     * Error detail
     * @type {Array<string>}
     * @memberof AccountFetchError
     */
    errors?: Array<string>;
}
/**
* @export
* @enum {string}
*/
export declare enum AccountFetchErrorSuccessEnum {
    False = "false"
}
export declare function AccountFetchErrorFromJSON(json: any): AccountFetchError;
export declare function AccountFetchErrorFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountFetchError;
export declare function AccountFetchErrorToJSON(value?: AccountFetchError | null): any;
