/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
/**
 *
 * @export
 * @interface ConvertResponseResult
 */
export interface ConvertResponseResult {
    /**
     * 200 - Ok, otherwise - failure
     * @type {number}
     * @memberof ConvertResponseResult
     */
    status?: number;
    /**
     * Conversion result
     * @type {object}
     * @memberof ConvertResponseResult
     */
    body?: object;
}
export declare function ConvertResponseResultFromJSON(json: any): ConvertResponseResult;
export declare function ConvertResponseResultFromJSONTyped(json: any, ignoreDiscriminator: boolean): ConvertResponseResult;
export declare function ConvertResponseResultToJSON(value?: ConvertResponseResult | null): any;
