/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { Order } from './';
/**
 *
 * @export
 * @interface OrderFetchResponse
 */
export interface OrderFetchResponse {
    /**
     * Always true
     * @type {boolean}
     * @memberof OrderFetchResponse
     */
    success: OrderFetchResponseSuccessEnum;
    /**
     * Copy of HTTP status
     * @type {number}
     * @memberof OrderFetchResponse
     */
    code: OrderFetchResponseCodeEnum;
    /**
     *
     * @type {Order}
     * @memberof OrderFetchResponse
     */
    order?: Order;
}
/**
* @export
* @enum {string}
*/
export declare enum OrderFetchResponseSuccessEnum {
    True = "true"
} /**
* @export
* @enum {string}
*/
export declare enum OrderFetchResponseCodeEnum {
    NUMBER_200 = 200,
    NUMBER_201 = 201,
    NUMBER_202 = 202
}
export declare function OrderFetchResponseFromJSON(json: any): OrderFetchResponse;
export declare function OrderFetchResponseFromJSONTyped(json: any, ignoreDiscriminator: boolean): OrderFetchResponse;
export declare function OrderFetchResponseToJSON(value?: OrderFetchResponse | null): any;
