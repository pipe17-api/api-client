/**
 * Pipe17 API
 * Generic interface to Pipe17 services
 *
 * Contact: support@pipe17.com
 *
 * NOTE: This class is auto generated.
 * Do not edit the class manually.
 */
import { AccountAddress } from './';
/**
 *
 * @export
 * @interface AccountCreateData
 */
export interface AccountCreateData {
    /**
     * Account Owner email
     * @type {string}
     * @memberof AccountCreateData
     */
    email: string;
    /**
     * Account Owner First Name
     * @type {string}
     * @memberof AccountCreateData
     */
    ownerFirstName: string;
    /**
     * Account Owner Last Name
     * @type {string}
     * @memberof AccountCreateData
     */
    ownerLastName: string;
    /**
     * Account Name
     * @type {string}
     * @memberof AccountCreateData
     */
    accountName?: string;
    /**
     * Company Name
     * @type {string}
     * @memberof AccountCreateData
     */
    companyName?: string;
    /**
     * Account Phone
     * @type {string}
     * @memberof AccountCreateData
     */
    phone: string;
    /**
     * Account Time Zone
     * @type {string}
     * @memberof AccountCreateData
     */
    timeZone: string;
    /**
     * Logo URL
     * @type {string}
     * @memberof AccountCreateData
     */
    logoUrl?: string;
    /**
     *
     * @type {AccountAddress}
     * @memberof AccountCreateData
     */
    address: AccountAddress;
    /**
     * Account Integrations
     * @type {Array<string>}
     * @memberof AccountCreateData
     */
    integrations?: Array<string>;
    /**
     *
     * @type {object}
     * @memberof AccountCreateData
     */
    payment?: object;
}
export declare function AccountCreateDataFromJSON(json: any): AccountCreateData;
export declare function AccountCreateDataFromJSONTyped(json: any, ignoreDiscriminator: boolean): AccountCreateData;
export declare function AccountCreateDataToJSON(value?: AccountCreateData | null): any;
